package cl.tinet.common.seguridad.dao.jdbc;

import java.util.Locale;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.jumbo.ldap.LDAPJumbo;
import cl.tinet.common.seguridad.model.Clave;
import cl.tinet.common.seguridad.model.Usuario;
import cl.tinet.common.util.resource.ResourceLeakUtil;

/**
 * TODO Falta descripcion de clase SeguridadDAOJDBCLDAP.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Oct 11, 2010
 */
public class SeguridadDAOJDBCLDAP extends SeguridadDAOJDBC {

    /**
     * TODO Describir atributo ESTADO_CLAVE_ACTIVA_KEY.
     */
    public static final String ESTADO_CLAVE_ACTIVA_KEY =
        "cl.tinet.common.seguridad.validacion.ESTADO_CLAVE_ACTIVA";
    /**
     * TODO Describir atributo logger.
     */
    private static Log logger = LogFactory.getLog(SeguridadDAOJDBCLDAP.class);

    /**
     * TODO Describir m�todo getUsuario.
     * @param username
     * @param password
     * @param tipo
     * @param locale
     * @return
     */
    @Override
    public Usuario getUsuario(String username, String password, int tipo,
        Locale locale) {
        Context context = null;
        try {
            logger.debug("Inicializando datos llamada LDAP.");
                context = new InitialContext();

            String url1 = (String) context.lookup("java:comp/env/url/ldap1");
            String url2 = (String) context.lookup("java:comp/env/url/ldap2");
            String userGroup = (String) context.lookup("java:comp/env/username/ldap");
            String passGroup = (String) context.lookup("java:comp/env/password/ldap");
            String group = (String) context.lookup("java:comp/env/group/ldap");
            LDAPJumbo jumbo =
                new LDAPJumbo(url1, url2, userGroup, passGroup, "5000", group);
            logger.debug("Validando LDAP...");
            if (jumbo.validaUsuario(username, password, 1)) {
                logger
                    .debug("LDAP validado. Obteniendo datos de usuario desde base de datos.");
                return super.getUsuario(username, password, tipo, locale);
            } else {
                logger.debug("Usuario no encontrado en LDAP: " + username);
            }
        } catch (NamingException ne) {
            logger.debug("Error buscando usuario LDAP.", ne);
        } finally {
            ResourceLeakUtil.close(context);
        }
        return null;
    }

    /**
     * TODO Describir m�todo getUltimaClave.
     * 
     * @param idUsuario
     * @return
     */
    @Override
    public Clave getUltimaClave(int idUsuario) {
        Clave clave = new Clave();
        int estadoActiva =
            this.getConfigurator().getInt(ESTADO_CLAVE_ACTIVA_KEY, getLocale());
        clave.setEstado(estadoActiva);
        clave.setIntentos(0);
        return clave;
    }
}
