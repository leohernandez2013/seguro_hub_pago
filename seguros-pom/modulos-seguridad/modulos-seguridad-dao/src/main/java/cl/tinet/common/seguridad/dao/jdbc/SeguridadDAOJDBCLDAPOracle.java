package cl.tinet.common.seguridad.dao.jdbc;

import org.apache.commons.dbutils.ResultSetHandler;

import cl.tinet.common.dao.jdbc.oracle.OracleJDBCUtil;

/**
 * TODO Falta descripcion de clase SeguridadDAOJDBCLDAPOracle.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Oct 12, 2010
 */
public class SeguridadDAOJDBCLDAPOracle extends SeguridadDAOJDBCLDAP {

    /**
     * TODO Describir m�todo getResultSetHandler.
     * @param resultClass
     * @return
     */
    @Override
    @SuppressWarnings("unchecked")
    protected ResultSetHandler getResultSetHandler(Class resultClass) {
        return OracleJDBCUtil.getResultSetHandler(resultClass);
    }

}
