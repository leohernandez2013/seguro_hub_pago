package cl.tinet.common.seguridad.dao;

import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cl.tinet.common.dao.jdbc.BaseDAO;
import cl.tinet.common.seguridad.model.Clave;
import cl.tinet.common.seguridad.model.Evento;
import cl.tinet.common.seguridad.model.Funcionalidad;
import cl.tinet.common.seguridad.model.Usuario;
import cl.tinet.common.seguridad.model.UsuarioExterno;
import cl.tinet.common.seguridad.model.UsuarioInterno;

/**
 * TODO Falta descripcion de clase SeguridadDAO.
 * <br/>
 * @author Ricardo
 * @version 1.0
 * @created Aug 23, 2010
 */
public interface SeguridadDAO extends BaseDAO {

    /**
     * TODO Obtiene el usuario, con los datos obtenidos desde la jsp
     * Si no se obtiene, se invalida el login
     * @param usuario
     * @param clave
     * @param tipoUsuario
     * @param locale
     * @return
     */
    public Usuario obtenerUsuario(String usuario, String clave,
        int tipoUsuario, Long locale);

    /**
     * Obtiene los roles y permisos asociados a un usuario
     * @param usuario
     * @return List
     */
    public List obtenerRoles(Usuario usuario);

    /**
     * TODO Describir m�todo autentificar.
     * @param rut
     * @param dv
     * @param clave
     * @return
     */
    public boolean autentificar(String rut, String dv, String clave);

    /**
     * TODO Describir m�todo bloquearCuentaUsuario.
     * @param userName
     * @return
     */
    public int bloquearCuentaUsuario(String userName);

    public Integer loginFallido(boolean b, Usuario usuario);

    public Integer obtenerEstadoUsuario(Usuario usuario);

    public Integer obtenerEstadoUsuario(String userName);

    public Map obtenerFechas(String usuario, String clave, int tipo);

    public boolean cambiarEstadoUsuario(String userName, long locale, int estado);

    public boolean cambiarEstadoUsuario(Usuario usuarioTO, long locale,
        int estado);

    public boolean usuarioInscrito(Usuario userTO, String clave);

    public Map getUsuarioExterno(String rut, String dv, long locale);

    public boolean cambiarClaveUsuario(String userName, long locale,
        String clave);

    public boolean cambiarClaveTemporal(String username, String claveTemporal,
        String claveNueva);

    public UsuarioExterno getUsuarioExterno(Usuario userTO);

    public UsuarioInterno getUsuarioInterno(Usuario userTO);

    public boolean existenciaUsuario(String userName);

    public void registrarEventoAuditoria(int idUsuario,
        Integer id_funcionalidad, String descripcion, Date date, int i);

    public int obtenerIdUsuario(String usuario);

    public boolean validacionLDAP(String userName, String password);

    /**
     * TODO Describir m�todo getUsuario.
     * @param username
     * @param password TODO
     * @param tipo
     * @param locale
     * @return
     */
    public Usuario getUsuario(String username, String password, int tipo, Locale locale);

    /**
     * TODO Describir m�todo getUltimaClave.
     * @param idUsuario
     * @return
     */
    public Clave getUltimaClave(int idUsuario);

    /**
     * TODO Describir m�todo updateClave.
     * @param clave
     */
    public void updateClave(Clave clave);

    /**
     * TODO Describir m�todo obtenerFuncionalidades.
     * @param usuario
     * @return
     */
    public List < Funcionalidad > getFuncionalidades(Usuario usuario);
    
    /**
     * TODO Describir m�todo obtenerDatosEmail.
     * @param nombre
     * @return
     */
    public String obtenerDatosEmail(String nombre);
    
    /**
     * TODO Describir m�todo registrarEvento.
     * @param evento
     */
    public void registrarEvento(Evento evento);
    
    /**
     * TODO Describir m�todo obtenerCuerpoEmail.
     * @param nombre
     * @return
     */
    InputStream obtenerCuerpoEmail(String nombre);

}
