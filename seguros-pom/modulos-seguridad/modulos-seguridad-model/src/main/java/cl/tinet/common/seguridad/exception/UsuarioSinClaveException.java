package cl.tinet.common.seguridad.exception;

/**
 * TODO Falta descripcion de clase UsuarioSinClaveException.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Oct 9, 2010
 */
public class UsuarioSinClaveException extends AutenticacionException {
    
    /**
     * TODO Describir atributo USUARIO_SIN_CLAVE_KEY.
     */
    public static final String USUARIO_SIN_CLAVE_KEY =
        "cl.tinet.common.seguridad.exception.USUARIO_SIN_CLAVE";

    /**
     * TODO Describir atributo serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * TODO Describir constructor de UsuarioSinClaveException.
     * @param username
     * @param tipo
     */
    public UsuarioSinClaveException(String username, int tipo) {
        super(USUARIO_SIN_CLAVE_KEY, username, tipo);
    }
}
