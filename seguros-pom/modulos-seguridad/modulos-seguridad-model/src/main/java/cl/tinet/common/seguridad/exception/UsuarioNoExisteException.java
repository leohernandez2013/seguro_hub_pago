package cl.tinet.common.seguridad.exception;

/**
 * TODO Falta descripcion de clase UsuarioNoExisteException.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Oct 9, 2010
 */
public class UsuarioNoExisteException extends AutenticacionException {
    
    /**
     * Key para indicar fallo en la autenticación.
     */
    public static final String AUTENTICACION_FALLIDA =
        "cl.tinet.common.seguridad.exception.AUTENTICACION_FALLIDA";
    
    /**
     * TODO Describir atributo serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    public UsuarioNoExisteException(String username, int tipo) {
        super(AUTENTICACION_FALLIDA, username, tipo);
    }
}
