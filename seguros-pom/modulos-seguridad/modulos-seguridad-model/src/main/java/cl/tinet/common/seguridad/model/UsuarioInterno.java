package cl.tinet.common.seguridad.model;

import java.io.Serializable;
import java.util.Date;

/**
 * TODO Falta descripcion de clase UsuarioInterno.
 * <br/>
 * @author Ricardo
 * @version 1.0
 * @created Sep 6, 2010
 */
public class UsuarioInterno extends Usuario implements Serializable {

    /**
     * Atributo para serial serializable.
     */
    private static final long serialVersionUID = 2028047091925951297L;

    /**
     * Id usuario Interno en la BD
     */
    private int IdUsuarioInt;

    /**
     *Id usuario, foreing en la base de datos
     */
    private int IdUsuario;

    /**
     * correo electronico del usuario
     */
    private String email;
    /**
     * Login
     */
    private String login;
    /**
     * Fecha Creacion del usuario Interno
     */
    private Date fechaCreacion;
    /**
     * Fecha de la ultima modificacion
     */
    private Date fechaModifiacion;

    private int IdPais;

    public int getIdPais() {
        return IdPais;
    }

    public void setIdPais(int idPais) {
        IdPais = idPais;
    }

    public int getIdUsuarioInt() {
        return IdUsuarioInt;
    }

    public void setIdUsuarioInt(int idUsuarioInt) {
        IdUsuarioInt = idUsuarioInt;
    }

    public int getIdUsuario() {
        return IdUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        IdUsuario = idUsuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Date getFechaCreacion() {
        return isNull(fechaCreacion);
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = isNull(fechaCreacion);
    }

    public Date getFechaModifiacion() {
        return isNull(fechaModifiacion);
    }

    public void setFechaModifiacion(Date fechaModifiacion) {
        this.fechaModifiacion = isNull(fechaModifiacion);
    }

}
