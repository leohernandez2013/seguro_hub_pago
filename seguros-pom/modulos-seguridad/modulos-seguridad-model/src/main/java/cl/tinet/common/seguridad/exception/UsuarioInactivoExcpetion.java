package cl.tinet.common.seguridad.exception;

/**
 * TODO Falta descripcion de clase UsuarioInactivoExcpetion.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Oct 8, 2010
 */
public class UsuarioInactivoExcpetion extends AutenticacionException {

    /**
     * TODO Describir atributo USUARIO_INACTIVO_KEY.
     */
    public static final String USUARIO_INACTIVO_KEY =
        "cl.tinet.common.seguridad.exception.USUARIO_INACTIVO";

    /**
     * TODO Describir atributo serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * TODO Describir constructor de UsuarioInactivoExcpetion.
     * @param username
     * @param tipo
     */
    public UsuarioInactivoExcpetion(String username, int tipo) {
        super(USUARIO_INACTIVO_KEY, username, tipo);
    }
}
