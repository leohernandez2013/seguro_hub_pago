package cl.tinet.common.seguridad.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Clase de trasporte de usuarioExterno <br/>
 * 
 * @author Ricardo
 * @version 1.0
 * @created Sep 3, 2010
 */
public class UsuarioExterno extends Usuario implements Serializable {

    /**
     * Atributo para serial serializable.
     * 
     */
    private static final long serialVersionUID = -8201510950240653440L;

    public UsuarioExterno() {

    }

    /**
     * Nombre del usuario.
     */
    private String nombre;
    /**
     * Apellido paterno del usuario.
     */
    private String apellido_paterno;
    /**
     * Apellido materno del usuario.
     */
    private String apellido_materno;
    /**
     * Telefono1.
     */
    private String Telefono_1;
    /**
     * Telefono2.
     */
    private String Telefono_2;
    /**
     * email.
     */
    private String email;
    /**
     * numero.
     */
    private String numero;
    /**
     * rut cliente.
     */
    private String rut_cliente;
    /**
     * rut-dv Cliente.
     */
    private String dv_cliente;
    /**
     * Numero del departamento, usuario.
     */
    private String numero_departamento;
    /**
     * Calle, (direccion) del usuario.
     */
    private String calle;

    /**
     * Tipo de telefono (celular, fijo).
     */
    private String tipo_telefono_1;

    /**
     * Tipo de telefono (celular, fijo).
     */
    private String tipo_telefono_2;

    /**
     * Estado civil del usuario.
     */
    private String estado_civil;

    /**
     * Sexo del usuario.
     */
    private String sexo;

    /**
     * Id de ciudad.
     */
    private String id_ciudad;

    /**
     * Fecha de nacimiento del cliente.
     */
    private Date fecha_nacimiento;

    /**
         * Numero de serie relacionado a la cedula de identidad.
         */
    private String numero_serie;

    /**
     * @return retorna el valor del atributo numero_serie
     */
    public String getNumero_serie() {
        return numero_serie;
    }

    /**
     * @param numeroSerie a establecer en el atributo numero_serie.
     */
    public void setNumero_serie(String numeroSerie) {
        numero_serie = numeroSerie;
    }

    /**
     * obtener nombre de usuario.
     * 
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Setear nombre de usuario.
     * 
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Obtener apellido paterno de usuario.
     * 
     * @return
     */
    public String getApellido_paterno() {
        return apellido_paterno;
    }

    /**
     * Obtener apellido materno del usuario.
     * 
     * @param apellido_paterno
     */
    public void setApellido_paterno(String apellido_paterno) {
        this.apellido_paterno = apellido_paterno;
    }

    /**
     * Obtener apellido materno del usuario,
     * 
     * @return
     */
    public String getApellido_materno() {
        return apellido_materno;
    }

    /**
     * Setear apellido materno del usuario.
     * 
     * @param apellido_materno
     */
    public void setApellido_materno(String apellido_materno) {
        this.apellido_materno = apellido_materno;
    }

    /**
     * Obtener el telefono de uusuario.
     * 
     * @return
     */
    public String getTelefono_1() {
        return Telefono_1;
    }

    /**
     * Setear telefono del usuario.
     * 
     * @param telefono_1
     */
    public void setTelefono_1(String telefono_1) {
        Telefono_1 = telefono_1;
    }

    /**
     * Obtener segundo numero de telefono.
     * 
     * @return
     */
    public String getTelefono_2() {
        return Telefono_2;
    }

    /**
     * Setear el segundo numero de telefono.
     * 
     * @param telefono_2
     */
    public void setTelefono_2(String telefono_2) {
        Telefono_2 = telefono_2;
    }

    /**
     * Obtener mail del usuario.
     * 
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setear mail de uusuario.
     * 
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Obtener numero de usuario.
     * 
     * @return
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Setear numero de usuario.
     * 
     * @param numero
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * obtener rutl del cliente.
     * 
     * @return
     */
    public String getRut_cliente() {
        return rut_cliente;
    }

    /**
     * setear rut_cliente.
     * 
     * @param rut_cliente
     */
    public void setRut_cliente(String rut_cliente) {
        this.rut_cliente = rut_cliente;
    }

    /**
     * Obtener dv cliente.
     * 
     * @return
     */
    public String getDv_cliente() {
        return dv_cliente;
    }

    /**
     * Setear dv cliente.
     * 
     * @param dv_cliente
     */
    public void setDv_cliente(String dv_cliente) {
        this.dv_cliente = dv_cliente;
    }

    /**
     * Obtener numer de departamento.
     * 
     * @return
     */
    public String getNumero_departamento() {
        return numero_departamento;
    }

    /**
     * setear numero de departamento.
     * 
     * @param numero_departamento
     */
    public void setNumero_departamento(String numero_departamento) {
        this.numero_departamento = numero_departamento;
    }

    /**
     * Obtener calle del usuario.
     * 
     * @return
     */
    public String getCalle() {
        return calle;
    }

    /**
     * Setear calle del usuario.
     * 
     * @param calle
     */
    public void setCalle(String calle) {
        this.calle = calle;
    }

    /**
     * @return the tipo_telefono_1
     */
    public String getTipo_telefono_1() {
        return tipo_telefono_1;
    }

    /**
     * @param tipo_telefono_1
     *            the tipo_telefono_1 to set
     */
    public void setTipo_telefono_1(String tipo_telefono_1) {
        this.tipo_telefono_1 = tipo_telefono_1;
    }

    /**
     * @return the tipo_telefono_2
     */
    public String getTipo_telefono_2() {
        return tipo_telefono_2;
    }

    /**
     * @param tipo_telefono_2
     *            the tipo_telefono_2 to set
     */
    public void setTipo_telefono_2(String tipo_telefono_2) {
        this.tipo_telefono_2 = tipo_telefono_2;
    }

    /**
     * @return the estado_civil
     */
    public String getEstado_civil() {
        return estado_civil;
    }

    /**
     * @param estado_civil
     *            the estado_civil to set
     */
    public void setEstado_civil(String estado_civil) {
        this.estado_civil = estado_civil;
    }

    /**
     * @return the sexo
     */
    public String getSexo() {
        return sexo;
    }

    /**
     * @param sexo
     *            the sexo to set
     */
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    /**
     * @return the id_ciudad
     */
    public String getId_ciudad() {
        return id_ciudad;
    }

    /**
     * @param id_ciudad
     *            the id_ciudad to set
     */
    public void setId_ciudad(String id_ciudad) {
        this.id_ciudad = id_ciudad;
    }

    /**
     * @return the fecha_nacimiento
     */
    public Date getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    /**
     * @param fecha_nacimiento
     *            the fecha_nacimiento to set
     */
    public void setFecha_nacimiento(Date fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

}
