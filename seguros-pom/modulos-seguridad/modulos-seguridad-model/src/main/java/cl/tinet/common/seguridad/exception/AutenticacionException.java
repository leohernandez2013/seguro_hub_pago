package cl.tinet.common.seguridad.exception;

import cl.tinet.common.model.exception.BusinessException;

/**
 * TODO Falta descripcion de clase AutenticacionException.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Oct 7, 2010
 */
public class AutenticacionException extends SeguridadException {

    /**
     * TODO Describir atributo AUTENTICACION_FALLIDA_KEY.
     */
    public static final String AUTENTICACION_FALLIDA_KEY =
        "cl.tinet.common.seguridad.exception.AUTENTICACION_FALLIDA";

    /**
     * TODO Describir atributo serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * TODO Describir atributo username.
     */
    private String username;

    /**
     * TODO Describir atributo tipo.
     */
    private int tipo;
    
    private int intentos;

    /**
     * TODO Describir constructor de AutenticacionException.
     * @param username
     * @param tipo
     */
    public AutenticacionException(String username, int tipo) {
        this(AUTENTICACION_FALLIDA_KEY, username, tipo);
    }

    /**
     * TODO Describir constructor de AutenticacionException.
     * @param messageKey
     * @param username
     * @param tipo
     */
    public AutenticacionException(String messageKey, String username, int tipo) {
        super(messageKey, new Object[] { username, tipo });
        this.username = username;
        this.tipo = tipo;
    }
    
    /**
     * TODO Describir constructor de AutenticacionException.
     * @param messageKey
     * @param username
     * @param tipo
     */
    public AutenticacionException(String messageKey, String username, int tipo, int intentos) {
        super(messageKey, new Object[] { username, tipo, intentos });
        this.username = username;
        this.tipo = tipo;
        this.intentos = intentos; 
    }

    /**
     * TODO Describir constructor de AutenticacionException.
     * @param exceptions
     */
    public AutenticacionException(BusinessException[] exceptions) {
        super(exceptions);
    }

    public String getUsername() {
        return username;
    }

    public int getTipo() {
        return tipo;
    }
}
