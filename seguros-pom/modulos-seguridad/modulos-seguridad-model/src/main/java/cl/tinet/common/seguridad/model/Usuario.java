package cl.tinet.common.seguridad.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author tinet
 * @version 1.0
 * @created 27-Ene-2010 16:38:20
 */
/**
 * Clase de trasporte, del Usuario
 * <br/>
 * @author Ricardo
 * @version 1.0
 * @created Sep 3, 2010
 */
public class Usuario implements Serializable {

    /**
     * TODO Describir atributo USUARIO_INTERNO.
     */
    public static final int USUARIO_INTERNO = 2;

    /**
     * TODO Describir atributo USUARIO_EXTERNO.
     */
    public static final int USUARIO_EXTERNO = 1;

    /**
     *  Atributo para serial serializable.
     */
    private static final long serialVersionUID = 4777836381513992040L;

    /**
     * FIXME Eliminar atributo resultadoValidacion.
     * 
     * Atributo que se llenara, en relacion a la validacion:
     * 1: usuario normalmente logeado
     * 2: cliente no registrado
     * 3: PSW no activa
     * 4: Intento fallido de autenticacion
     *   (posiblemente por ingreso mal de clave)
     * 5: cambio de clave: temporal
     * 
     */
    private int resultadoValidacion;

    /**
     * Atributo, que representa el estado del usuario.
     */
    private int estado;

    /**
     * Atributo que representa el identificador del usuario de la base de
     * datos.
     */
    private int idUsuario;

    /**
     * Atributo que representa el identificador del pais al que pertenece el
     * usuario.
     */
    private int idPais;

    /**
     * Atributo que representa el userName del usuario, es el nombre con el
     * cual se identifica el usuario en el sistema.
     */
    private String username;

    /**
     * Atributo que representa el tipo de usuario.
     * <li> 1: Interno </li>
     * <li> 2: Externo </li>
     */
    private int tipo;

    /**
     * Atributo que representa la fecha de
     * ingreso del usuario.
     */
    private Date fechaIngreso;

    /**
     * FIXME Modificar javadoc.
     * Atributo que representa el estado del usuario.
     */
    private int activo;

    /**
     * TODO Describir atributo email.
     */
    private String email;

    /**
     * TODO Describir atributo cambioClaveRequerido.
     */
    private boolean cambioClaveRequerido;

    /**
     * Atributo que contiene la lista de roles de un usuario.
     */
    private List roles;

    /**
     * Atributo que representa un usuario Externo.
     */
    private UsuarioExterno usuarioExterno;

    /**
     * Atributo que representa un usuario Interno.
     */
    private UsuarioInterno usuarioInterno;

    /**
     * Constructor vacio.
     */
    public Usuario() {

    }

    /**
     * Metodo que obiene el estado del usuario.
     * @return
     */
    public int getActivo() {
        return activo;
    }

    /**
     * TODO Describir m�todo getEmail.
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     * Metodo que obtiene el estado del usuario.
     * @return
     */
    public int getEstado() {
        return estado;
    }

    /**
     * Metodo que devuelve la fecha de ingreso de un usuario.
     * @return
     */
    public Date getFechaIngreso() {
        return isNull(fechaIngreso);
    }

    public int getIdPais() {
        return idPais;
    }

    /**
     * Metodo que obtiene el Id del usuario.
     * @return
     */
    public int getIdUsuario() {
        return idUsuario;
    }

    /**
     * Metodo que retorna el resultado de la validacion.
     * @return
     */
    public int getResultadoValidacion() {
        return resultadoValidacion;
    }

    /**
     * Metodo que obtiene una lista de roles.
     * @return
     */
    public List getRoles() {
        return roles;
    }

    /**
     * Metodo, que devuelve el tipo de usuario.
     * @return
     */
    public int getTipo() {
        return tipo;
    }

    /**
     * Metodo, que obtiene un objeto userName.
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     * Metodo que retorna un usuarioExterno.
     * @return
     */
    public UsuarioExterno getUsuarioExterno() {
        if (this instanceof UsuarioExterno) {
            return (UsuarioExterno) this;
        }
        throw new IllegalStateException(
            "El tipo de usuario no corresponde a un usuario externo.");
    }

    /**
     * Metodo que retorna un usuarioInterno.
     * @return
     */
    public UsuarioInterno getUsuarioInterno() {
        //if (this instanceof UsuarioInterno) {
        return (UsuarioInterno) this.usuarioInterno;
        //}
        //throw new IllegalStateException(
        //    "El tipo de usuario no corresponde a un usuario interno.");
    }

    /**
     * TODO Describir m�todo isCambioClaveRequerido.
     * @return
     */
    public boolean isCambioClaveRequerido() {
        return cambioClaveRequerido;
    }

    /**
     * TODO Describir m�todo isNull.
     * @param newVal
     * @return
     */
    protected Date isNull(Date newVal) {
        if (newVal == null) {
            return null;
        }
        return new Date(newVal.getTime());
    }

    /**
     * Metodo que devuelve el estado del usuario.
     * @return
     */
    public boolean isUsuarioActivo() {
        return activo != 0;
    }

    /**
     * Metodo que setea el estado del usuario.
     * @param newVal
     */
    public void setActivo(int newVal) {
        activo = newVal;
    }

    /**
     * TODO Describir m�todo setCambioClaveRequerido.
     * @param cambioClaveRequerido
     */
    public void setCambioClaveRequerido(boolean cambioClaveRequerido) {
        this.cambioClaveRequerido = cambioClaveRequerido;
    }

    /**
     * TODO Describir m�todo setEmail.
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Metodo que setea el estado del usuario.
     * @param estado
     */
    public void setEstado(int estado) {
        this.estado = estado;
    }

    /**
     * Metodo que setea la fecha de ingreso de un usuario.
     * @param newVal
     */
    public void setFechaIngreso(Date newVal) {
        fechaIngreso = isNull(newVal);
    }

    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }

    /**
     * Metodo que setea el atributo el id del usuario.
     * @param idUsuario
     */
    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * Metodo que setea el resultado de la validacion.
     * @param resultadoValidacion
     */
    public void setResultadoValidacion(int resultadoValidacion) {
        this.resultadoValidacion = resultadoValidacion;
    }

    /**
     * Metodo que setea una lista de roles.
     * @param newVal
     */
    public void setRoles(List newVal) {
        roles = newVal;
    }

    /**
     * Metodo que setea el tipo de usuario.
     * @param newVal
     */
    public void setTipo(int newVal) {
        tipo = newVal;
    }

    /**
     * Metodo que setea, el   userName.
     * @param newVal
     */
    public void setUsername(String newVal) {
        username = newVal;
    }

    /**
     * Metodo que setea un usuario Externo.
     * @param usuarioExterno
     */
    public void setUsuarioExterno(UsuarioExterno usuarioExterno) {
        this.usuarioExterno = usuarioExterno;
    }

    /**
     * Metodo que setea un usuario interno.
     * @param usuarioInterno
     */
    public void setUsuarioInterno(UsuarioInterno usuarioInterno) {
        this.usuarioInterno = usuarioInterno;
    }

    @Override
    public String toString() {
        return this.username;
    }
}