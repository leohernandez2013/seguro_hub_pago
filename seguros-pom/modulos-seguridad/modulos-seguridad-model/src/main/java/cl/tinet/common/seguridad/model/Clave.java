package cl.tinet.common.seguridad.model;

import java.io.Serializable;
import java.util.Date;

/**
 * TODO Falta descripcion de clase Clave.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Oct 9, 2010
 */
public class Clave implements Serializable {

    /**
     * TODO Describir atributo serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Identificador de la clave.
     */
    private long idClave;

    /**
     * Estado de la clave. Valor numérico que debe ser
     * interpretado mediante la configuración del sistema.
     */
    private int estado;

    /**
     * TODO Describir atributo intentos.
     */
    private int intentos;

    /**
     * TODO Describir atributo fechaCreacion.
     */
    private Date fechaCreacion;

    /**
     * TODO Describir atributo fechaModificacion.
     */
    private Date fechaModificacion;

    /**
     * TODO Describir atributo fechaExpiracion.
     */
    private Date fechaExpiracion;

    /**
     * TODO Describir atributo fechaBloqueo.
     */
    private Date fechaBloqueo;

    /**
     * TODO Describir atributo password.
     */
    private String password;

    public Clave() {
        fechaCreacion = new Date();
    }

    public int getEstado() {
        return estado;
    }

    public Date getFechaBloqueo() {
        return isNull(this.fechaBloqueo);
    }

    public Date getFechaCreacion() {
        return isNull(this.fechaCreacion);
    }

    public Date getFechaExpiracion() {
        return isNull(this.fechaExpiracion);
    }

    public Date getFechaModificacion() {
        return isNull(this.fechaModificacion);
    }

    public long getIdClave() {
        return idClave;
    }

    public int getIntentos() {
        return intentos;
    }

    public String getPassword() {
        return password;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public void setFechaBloqueo(Date fechaBloqueo) {
        this.fechaBloqueo = isNull(fechaBloqueo);
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = isNull(fechaCreacion);
    }

    public void setFechaExpiracion(Date fechaExpiracion) {
        this.fechaExpiracion = isNull(fechaExpiracion);
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = isNull(fechaModificacion);
    }

    public void setIdClave(long idClave) {
        this.idClave = idClave;
    }

    public void setIntentos(int intentos) {
        this.intentos = intentos;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private Date isNull(Date fecha) {
        if (fecha == null) {
            return null;
        }
        return new Date(fecha.getTime());
    }
}
