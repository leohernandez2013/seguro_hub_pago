package cl.tinet.common.seguridad.model;

import java.io.Serializable;

/**
 * @author tinet
 * @version 1.0
 * @created 27-Ene-2010 16:38:20
 */
public class TipoPermiso implements Serializable {

    /**
     * TODO Describir atributo serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * TODO Describir atributo id.
     */
    private int id;

    /**
     * TODO Describir atributo nombre.
     */
    private String nombre;

    public TipoPermiso() {
    }

    public int getId() {
        return id;
    }

    /**
     * 
     * @param newVal
     */
    public void setId(int newVal) {
        id = newVal;
    }

    public String getNombre() {
        return nombre;
    }

    /**
     * 
     * @param newVal
     */
    public void setNombre(String newVal) {
        nombre = newVal;
    }
}