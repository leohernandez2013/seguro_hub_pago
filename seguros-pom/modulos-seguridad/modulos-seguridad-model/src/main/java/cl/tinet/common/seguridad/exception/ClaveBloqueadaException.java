package cl.tinet.common.seguridad.exception;

/**
 * TODO Falta descripcion de clase ClaveBloqueadaException.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Oct 9, 2010
 */
public class ClaveBloqueadaException extends AutenticacionException {

    /**
     * TODO Describir atributo serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * TODO Describir atributo CLAVE_BLOQUEADA_KEY.
     */
    public static final String CLAVE_BLOQUEADA_KEY =
        "cl.tinet.common.seguridad.exception.CLAVE_BLOQUEADA";

    /**
     * TODO Describir constructor de ClaveBloqueadaException.
     * @param username
     * @param tipo
     */
    public ClaveBloqueadaException(String username, int tipo) {
        super(CLAVE_BLOQUEADA_KEY, username, tipo);
    }
}
