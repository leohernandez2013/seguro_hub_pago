package cl.tinet.common.seguridad.exception;

/**
 * TODO Falta descripcion de clase BloqueoCuentaException.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Oct 7, 2010
 */
public class BloqueoCuentaException extends SeguridadException {

    /**
     * TODO Describir atributo FALLO_BLOQUEO_CUENTA.
     */
    public static final String FALLO_BLOQUEO_CUENTA =
        "cl.tinet.common.seguridad.exception.FALLO_BLOQUEO_CUENTA";

    /**
     * TODO Describir atributo serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * TODO Describir constructor de BloqueoCuentaException.
     * @param motivo
     */
    public BloqueoCuentaException(String motivo) {
        super(FALLO_BLOQUEO_CUENTA, new Object[] { motivo });
    }
}
