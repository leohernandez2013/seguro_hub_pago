package cl.tinet.common.seguridad.exception;

/**
 * TODO Falta descripcion de clase CredencialInvalidaException.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Oct 9, 2010
 */
public class CredencialInvalidaException extends SeguridadException {

    /**
     * TODO Describir atributo serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * TODO Describir atributo username.
     */
    private String username;

    /**
     * TODO Describir constructor de CredencialInvalidaException.
     * @param messageKey
     */
    public CredencialInvalidaException(String messageKey) {
        super(messageKey, SIN_ARGUMENTOS);
    }

    /**
     * TODO Describir constructor de CredencialInvalidaException.
     * @param messageKey
     * @param username
     */
    public CredencialInvalidaException(String messageKey, String username) {
        super(messageKey, new Object[] { username });
        this.username = username;
    }

    /**
     * @return retorna el valor del atributo username
     */
    public String getUsername() {
        return username;
    }
}
