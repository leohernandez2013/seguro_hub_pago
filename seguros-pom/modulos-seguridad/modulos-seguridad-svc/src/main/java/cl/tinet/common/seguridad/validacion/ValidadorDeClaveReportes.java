package cl.tinet.common.seguridad.validacion;

import cl.tinet.common.seguridad.exception.AutenticacionException;
import cl.tinet.common.seguridad.exception.ClaveIncorrectaException;
import cl.tinet.common.seguridad.model.Clave;
import cl.tinet.common.seguridad.model.Usuario;

/**
 * TODO Falta descripcion de clase ValidadorDeClaveDummy.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Oct 9, 2010
 */
public class ValidadorDeClaveReportes extends ValidadorDeClave {

    /**
     * TODO Describir m�todo validarClave.
     * @param usuario
     * @param password
     * @param clave
     * @throws AutenticacionException
     */
    @Override
    public void validarClave(Usuario usuario, String password, Clave clave)
        throws AutenticacionException {

        //Validar clave contra base de datos.
        if (!password.equals(clave.getPassword())) {
            String sIdUsuario = String.valueOf(clave.getIdClave());
            usuario.setIdUsuario(Integer.parseInt(sIdUsuario));
            usuario.setTipo(1);
            throw new ClaveIncorrectaException(usuario.getUsername(), usuario
                .getTipo(), 1);
        }

    }
}
