package cl.tinet.common.seguridad.validacion;

import java.util.Locale;

import cl.tinet.common.config.AbstractConfigurator;
import cl.tinet.common.seguridad.config.SeguridadConfig;
import cl.tinet.common.seguridad.dao.SeguridadDAO;
import cl.tinet.common.seguridad.exception.AutenticacionException;
import cl.tinet.common.seguridad.model.Clave;
import cl.tinet.common.seguridad.model.Usuario;

import com.tinet.exceptions.system.SystemException;

/**
 * TODO Falta descripcion de clase ValidadorDeClave.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Oct 9, 2010
 */
public abstract class ValidadorDeClave {

    /**
     * TODO Describir atributo VALIDADOR_CLAVE_CLASS_KEY.
     */
    public static final String VALIDADOR_CLAVE_CLASS_KEY =
        "cl.tinet.common.seguridad.validacion.VALIDADOR_CLAVE_CLASS";

    /**
     * TODO Describir m�todo createKey.
     * @param llave
     * @param tipo
     * @return
     */
    protected static String createKey(String llave, int tipo) {
        if (llave == null) {
            throw new NullPointerException("La llave no debe ser nula.");
        }
        return llave + "_" + tipo;
    }

    /**
     * TODO Describir m�todo getValidador.
     * @param tipo
     * @param dao
     * @param locale
     * @return
     */
    public static ValidadorDeClave getValidador(int tipo, SeguridadDAO dao,
        Locale locale) {
        AbstractConfigurator config = SeguridadConfig.getInstance();
        String className = config.getString(createKey(VALIDADOR_CLAVE_CLASS_KEY, tipo), locale);
        if (className == null) {
            throw new NullPointerException(
                "La clase validadora no fue encontrada.");
        }
        try {
            Class < ? > clase = Class.forName(className);
            ValidadorDeClave validador = (ValidadorDeClave) clase.newInstance();
            validador.setLocale(locale);
            validador.setSeguridadDAO(dao);
            return validador;
        } catch (ClassNotFoundException cnfe) {
            throw new SystemException(cnfe);
        } catch (IllegalAccessException iae) {
            throw new SystemException(iae);
        } catch (InstantiationException ie) {
            throw new SystemException(ie);
        }
    }

    /**
     * TODO Describir atributo locale.
     */
    private Locale locale;

    /**
     * TODO Describir atributo seguridadDAO.
     */
    private SeguridadDAO seguridadDAO;

    /**
     * @return retorna el valor del atributo locale
     */
    protected Locale getLocale() {
        return locale;
    }

    /**
     * TODO Describir m�todo getSeguridadDAO.
     * @return
     */
    protected SeguridadDAO getSeguridadDAO() {
        return seguridadDAO;
    }

    /**
     * @param locale a establecer en el atributo locale.
     */
    protected void setLocale(Locale locale) {
        this.locale = locale;
    }

    /**
     * TODO Describir m�todo setSeguridadDAO.
     * @param seguridadDAO
     */
    protected void setSeguridadDAO(SeguridadDAO seguridadDAO) {
        this.seguridadDAO = seguridadDAO;
    }

    /**
     * TODO Describir m�todo validarClave.
     * @param usuario
     * @param password
     * @param clave
     * @throws AutenticacionException
     */
    public abstract void validarClave(Usuario usuario, String password,
        Clave clave) throws AutenticacionException;
}
