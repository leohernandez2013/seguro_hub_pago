package cl.tinet.common.seguridad.validacion;

import java.util.Date;

import cl.tinet.common.seguridad.config.SeguridadConfig;
import cl.tinet.common.seguridad.exception.AutenticacionException;
import cl.tinet.common.seguridad.exception.ClaveBloqueadaException;
import cl.tinet.common.seguridad.exception.ClaveIncorrectaException;
import cl.tinet.common.seguridad.model.Clave;
import cl.tinet.common.seguridad.model.Usuario;
import cl.tinet.common.util.crypto.CryptoUtil;

/**
 * TODO Falta descripcion de clase ValidadorClaveCifrada.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Oct 9, 2010
 */
public class ValidadorDeClaveNormal extends ValidadorDeClave {

    /**
     * TODO Describir atributo CANTIDAD_MAXIMA_INTENTOS_KEY.
     */
    public static final String CANTIDAD_MAXIMA_INTENTOS_KEY =
        "cl.tinet.common.seguridad.validacion.CANTIDAD_MAXIMA_INTENTOS";

    /**
     * TODO Describir atributo ESTADO_CLAVE_ACTIVA_KEY.
     */
    public static final String ESTADO_CLAVE_ACTIVA_KEY =
        "cl.tinet.common.seguridad.validacion.ESTADO_CLAVE_ACTIVA";

    /**
     * TODO Describir atributo ESTADO_CLAVE_BLOQUEADA_KEY.
     */
    public static final String ESTADO_CLAVE_BLOQUEADA_KEY =
        "cl.tinet.common.seguridad.validacion.ESTADO_CLAVE_BLOQUEADA";

    /**
     * TODO Describir atributo ESTADO_CLAVE_TEMPORAL_KEY.
     */
    public static final String ESTADO_CLAVE_TEMPORAL_KEY =
        "cl.tinet.common.seguridad.validacion.ESTADO_CLAVE_TEMPORAL";
    

    /**
     * TODO Describir m�todo validarClave.
     * @param usuario
     * @param password
     * @param clave
     * @throws AutenticacionException
     */
    @Override
    public void validarClave(Usuario usuario, String password, Clave clave)
        throws AutenticacionException {
        validarClaveBloqueada(usuario, clave);
        validarClaveEncpritada(usuario, password, clave);
        validarClaveTemporal(usuario, password, clave);
    }

    /**
     * TODO Describir m�todo validarClaveTemporal.
     * @param usuario
     * @param password
     * @param clave
     */
    protected void validarClaveTemporal(Usuario usuario, String password,
        Clave clave) {
        int estadoTemporal =
            SeguridadConfig.getInstance().getInt(ESTADO_CLAVE_TEMPORAL_KEY,
                getLocale());
        if (clave.getEstado() == estadoTemporal) {
            usuario.setCambioClaveRequerido(true);
        }
    }

    /**
     * TODO Describir m�todo validarClaveBloqueada.
     * @param usuario
     * @param clave
     * @throws ClaveBloqueadaException 
     */
    protected void validarClaveBloqueada(Usuario usuario, Clave clave)
        throws ClaveBloqueadaException {
        if (isClaveBloqueada(usuario, clave)) {
            throw new ClaveBloqueadaException(usuario.getUsername(), usuario
                .getTipo());
        }
    }

    /**
     * TODO Describir m�todo isClaveBloqueada.
     * @param usuario
     * @param clave
     * @return
     */
    protected boolean isClaveBloqueada(Usuario usuario, Clave clave) {
        int estadoBloqueo =
            SeguridadConfig.getInstance().getInt(ESTADO_CLAVE_BLOQUEADA_KEY,
                getLocale());
        boolean condicion = clave.getEstado() == estadoBloqueo;
        return condicion;
    }

    /**
     * TODO Describir m�todo validarClaveEncpritada.
     * @param usuario
     * @param password
     * @param clave
     * @throws ClaveIncorrectaException
     */
    protected void validarClaveEncpritada(Usuario usuario, String password,
        Clave clave) throws ClaveIncorrectaException {
        SeguridadConfig config = SeguridadConfig.getInstance();
        CryptoUtil cryptoUtil = new CryptoUtil(config);
        //        byte[] semilla = cryptoUtil.generateEncryptionSalt(usuario.getUsername());
        byte[] datos = cryptoUtil.getEncrypted(password);
        String encriptada = CryptoUtil.toBase64String(datos, true);
        if (!encriptada.equals(clave.getPassword())) {
            
            incrementarIntentos(usuario, clave);
            int nroItentos = config.getInt(CANTIDAD_MAXIMA_INTENTOS_KEY, getLocale()) - clave.getIntentos();
            if (nroItentos < 1){
            	nroItentos = 0;
            }
            throw new ClaveIncorrectaException(usuario.getUsername(), usuario
                .getTipo(), nroItentos);
        }
        int estadoActivo =
            config.getInt(ESTADO_CLAVE_ACTIVA_KEY, getLocale());
        int estadoBloqueo =
            config.getInt(ESTADO_CLAVE_BLOQUEADA_KEY, getLocale());
        if ((clave.getEstado() == estadoBloqueo) || (clave.getIntentos() >= 0)) {
            
            if (clave.getEstado() == estadoBloqueo) {
                clave.setEstado(estadoActivo);
            }
            clave.setIntentos(0);
            getSeguridadDAO().updateClave(clave);
        }
    }

    /**
     * TODO Describir m�todo incrementarIntentos.
     * @param usuario
     * @param clave
     */
    protected int incrementarIntentos(Usuario usuario, Clave clave) {
        int intentos = clave.getIntentos() + 1;
        Date fechaActual = new Date();
        clave.setFechaModificacion(fechaActual);
        SeguridadConfig config = SeguridadConfig.getInstance();
        int maximoIntentos =
            config.getInt(CANTIDAD_MAXIMA_INTENTOS_KEY, getLocale());
        if (intentos > maximoIntentos) {
        	intentos = 4;
        }
        if (intentos > maximoIntentos) {
            int estadoBloqueo =
                config.getInt(ESTADO_CLAVE_BLOQUEADA_KEY, getLocale());
            clave.setEstado(estadoBloqueo);
            clave.setFechaBloqueo(fechaActual);
        }
        clave.setIntentos(intentos);
        getSeguridadDAO().updateClave(clave);
        return intentos;
    }
}
