package cl.tinet.common.seguridad.validacion;

import java.util.Locale;

import com.tinet.exceptions.system.SystemException;

import cl.tinet.common.model.exception.BusinessException;
import cl.tinet.common.seguridad.config.SeguridadConfig;
import cl.tinet.common.seguridad.exception.AutenticacionException;

/**
 * TODO Falta descripcion de clase ValidadorDinamico.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Oct 9, 2010
 */
public abstract class ValidadorDinamico {

    public static final String VALIDADOR_DINAMICO_CLASS_KEY =
        "cl.tinet.common.seguridad.validacion.VALIDADOR_DINAMICO_CLASS";

    /**
     * TODO Describir m�todo getValidador.
     * @param tipo
     * @param locale
     * @return
     */
    public static ValidadorDinamico getValidador(int tipo, Locale locale) {
        String className =
            SeguridadConfig.getInstance().getString(
                VALIDADOR_DINAMICO_CLASS_KEY + "_" + tipo, locale);
        if (className == null) {
            throw new NullPointerException(
                "La clase validadora no fue encontrada.");
        }
        try {
            Class < ? > clase = Class.forName(className);
            ValidadorDinamico validador =
                (ValidadorDinamico) clase.newInstance();
            validador.setLocale(locale);
            return validador;
        } catch (ClassNotFoundException cnfe) {
            throw new SystemException(cnfe);
        } catch (IllegalAccessException iae) {
            throw new SystemException(iae);
        } catch (InstantiationException ie) {
            throw new SystemException(ie);
        }
    }

    /**
     * TODO Describir atributo locale.
     */
    private Locale locale;

    /**
     * @return retorna el valor del atributo locale
     */
    protected Locale getLocale() {
        return locale;
    }

    /**
     * @param locale a establecer en el atributo locale.
     */
    protected void setLocale(Locale locale) {
        this.locale = locale;
    }

    /**
     * TODO Describir m�todo validarUsuario.
     * @param username
     * @throws BusinessException
     */
    public abstract void validar(String username, String password)
        throws AutenticacionException;
}
