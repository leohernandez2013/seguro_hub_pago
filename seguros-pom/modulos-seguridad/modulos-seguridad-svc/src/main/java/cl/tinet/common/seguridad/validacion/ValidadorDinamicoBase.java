package cl.tinet.common.seguridad.validacion;

import java.util.ArrayList;
import java.util.List;

import cl.tinet.common.model.exception.BusinessException;
import cl.tinet.common.seguridad.exception.AutenticacionException;
import cl.tinet.common.seguridad.exception.CredencialInvalidaException;

public class ValidadorDinamicoBase extends ValidadorDinamico {

    /**
     * TODO Describir atributo USERNAME_NULO_KEY.
     */
    public static final String USERNAME_NULO_KEY =
        "cl.tinet.common.seguridad.exception.USERNAME_NULO";

    /**
     * TODO Describir atributo PASSWORD_NULO_KEY.
     */
    public static final String PASSWORD_NULO_KEY =
        "cl.tinet.common.seguridad.exception.PASSWORD_NULO";

    /**
     * TODO Describir m�todo validarUsuario.
     * @param username
     * @throws BusinessException
     */
    @Override
    public void validar(String username, String password)
        throws AutenticacionException {
        List < CredencialInvalidaException > errores =
            new ArrayList < CredencialInvalidaException >();
        validarUsername(username, errores);
        validarPassword(password, errores);
        if (errores.size() > 0) {
            throw new AutenticacionException(errores
                .toArray(new BusinessException[errores.size()]));
        }
    }

    /**
     * TODO Describir m�todo validarPassword.
     * @param password
     * @param errores
     */
    protected void validarPassword(String password,
        List < CredencialInvalidaException > errores) {
        if (password == null) {
            errores.add(new CredencialInvalidaException(PASSWORD_NULO_KEY));
        }
    }

    /**
     * TODO Describir m�todo validarUsername.
     * @param username
     * @param errores
     */
    protected void validarUsername(String username,
        List < CredencialInvalidaException > errores) {
        if (username == null) {
            errores.add(new CredencialInvalidaException(USERNAME_NULO_KEY));
        }
    }
}