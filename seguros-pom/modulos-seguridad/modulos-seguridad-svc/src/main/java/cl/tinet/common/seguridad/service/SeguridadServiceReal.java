package cl.tinet.common.seguridad.service;

import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cl.tinet.common.seguridad.config.SeguridadConfig;
import cl.tinet.common.seguridad.dao.SeguridadDAO;
import cl.tinet.common.seguridad.dao.SeguridadDAOFactory;
import cl.tinet.common.seguridad.exception.AutenticacionException;
import cl.tinet.common.seguridad.exception.UsuarioInactivoExcpetion;
import cl.tinet.common.seguridad.exception.UsuarioNoExisteException;
import cl.tinet.common.seguridad.exception.UsuarioSinClaveException;
import cl.tinet.common.seguridad.model.Clave;
import cl.tinet.common.seguridad.model.Evento;
import cl.tinet.common.seguridad.model.Funcionalidad;
import cl.tinet.common.seguridad.model.Rol;
import cl.tinet.common.seguridad.model.Usuario;
import cl.tinet.common.seguridad.validacion.ValidadorDeClave;
import cl.tinet.common.seguridad.validacion.ValidadorDinamico;

/**
 * TODO Falta descripcion de clase SeguridadServiceReal.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Oct 8, 2010
 */
public class SeguridadServiceReal implements SeguridadService {

    /**
     * TODO Describir atributo LLAVE_MENSAJE_AUTENTICACION_EXITOSA.
     */
    public static final String LLAVE_MENSAJE_AUTENTICACION_EXITOSA =
        "cl.tinet.common.seguridad.MENSAJE_AUTENTICACION_EXITOSA";

    /**
     * TODO Describir atributo LLAVE_MENSAJE_AUTENTICACION_FALLIDA.
     */
    public static final String LLAVE_MENSAJE_AUTENTICACION_FALLIDA =
        "cl.tinet.common.seguridad.MENSAJE_AUTENTICACION_FALLIDA";

    /**
     * TODO Describir m�todo autenticar.
     * @param username
     * @param passsword
     * @param tipoUsuario
     * @return
     * @throws AutenticacionException 
     */
    public Usuario autenticar(String username, String passsword, int tipoUsuario)
        throws AutenticacionException {
        return autenticar(username, passsword, tipoUsuario, Locale.getDefault());
    }

    /**
     * TODO Describir m�todo autenticar.
     * @param username
     * @param password
     * @param tipo
     * @param locale
     * @return
     * @throws AutenticacionException 
     */
    public Usuario autenticar(String username, String password, int tipo,
        Locale locale) throws AutenticacionException {
        ValidadorDinamico.getValidador(tipo, locale).validar(username, password);

        SeguridadConfig config = SeguridadConfig.getInstance();
        Usuario usuario = null;
        String descripcion = config.getString(LLAVE_MENSAJE_AUTENTICACION_FALLIDA, locale);
        SeguridadDAO seguridadDAO = getSeguridadDAO(tipo, locale);
        try {
            usuario = seguridadDAO.getUsuario(username, password, tipo, locale);
            if (usuario == null) {
                throw new UsuarioNoExisteException(username, tipo);
            } else if (!usuario.isUsuarioActivo()) {
                throw new UsuarioInactivoExcpetion(username, tipo);
            }
            Clave clave = seguridadDAO.getUltimaClave(usuario.getIdUsuario());
            if (clave == null) {
                throw new UsuarioSinClaveException(username, tipo);
            }
            ValidadorDeClave.getValidador(tipo, seguridadDAO, locale).validarClave(usuario, password, clave);

            //FIXME: USUARIOS REPORTE: Para log.
            if(tipo == 3) {
                String sIdUsuario = String.valueOf(clave.getIdClave());
                usuario.setIdUsuario(Integer.parseInt(sIdUsuario));
                usuario.setTipo(1);
            }
            
            descripcion =
                config.getString(LLAVE_MENSAJE_AUTENTICACION_EXITOSA, locale);
            descripcion = MessageFormat.format(descripcion, usuario);
            seguridadDAO.registrarEvento(new Evento(usuario, descripcion));
            return usuario;
        } catch (AutenticacionException ae) {
            // Solo se registra el evento en caso de que el usuario exista.
            if (usuario != null) {
                descripcion =
                    MessageFormat.format(descripcion, usuario, ae
                        .getMessage(locale));
                registrarEvento(new Evento(usuario, descripcion), locale);
            }
            throw ae;
        } finally {
            seguridadDAO.close();
        }
    }
    
    /**
     * TODO Describir m�todo autenticar.
     * @param username
     * @param tipo
     * @return
     * @throws AutenticacionException 
     */
    public Usuario getUsuario(String username, int tipo) throws AutenticacionException {
    	
        Locale locale = Locale.getDefault();
    	SeguridadDAO dao = getSeguridadDAO(tipo, Locale.getDefault());
        SeguridadConfig config = SeguridadConfig.getInstance();
        Usuario usuario = null;
        String descripcion = config.getString(LLAVE_MENSAJE_AUTENTICACION_FALLIDA, locale);

        try {
            usuario = dao.getUsuario(username, "", tipo, locale);
            if (usuario == null) {
                throw new UsuarioNoExisteException(username, tipo);
            } else if (!usuario.isUsuarioActivo()) {
                throw new UsuarioInactivoExcpetion(username, tipo);
            }
                                    
            descripcion = config.getString(LLAVE_MENSAJE_AUTENTICACION_EXITOSA, locale);
            descripcion = MessageFormat.format(descripcion, usuario);
            dao.registrarEvento(new Evento(usuario, descripcion));
            return usuario;
        } catch (AutenticacionException ae) {
            // Solo se registra el evento en caso de que el usuario exista.
            if (usuario != null) {
                descripcion = MessageFormat.format(descripcion, usuario, ae.getMessage(locale));
                registrarEvento(new Evento(usuario, descripcion), locale);
            }
            throw ae;
        } finally {
            dao.close();
        }
    }
        
    /**
     * TODO Describir m�todo obtenerFuncionalidades.
     * @param usuario
     * @return
     */
    public List < Funcionalidad > obtenerFuncionalidades(Usuario usuario) {
        return obtenerFuncionalidades(usuario, Locale.getDefault());
    }

    /**
     * TODO Describir m�todo obtenerFuncionalidades.
     * @param usuario
     * @param locale
     * @return
     */
    public List < Funcionalidad > obtenerFuncionalidades(Usuario usuario,
        Locale locale) {
        if (usuario == null) {
            throw new NullPointerException(
                "El usuario especificado no debe ser nulo.");
        }
        if (locale == null) {
            throw new NullPointerException(
                "El locale especificado no debe ser nulo.");
        }
        SeguridadDAO seguridadDAO = getSeguridadDAO(usuario.getTipo(), locale);
        try {
            return seguridadDAO.getFuncionalidades(usuario);
        } finally {
            seguridadDAO.close();
        }
    }

    /**
     * TODO Describir m�todo obtenerRoles.
     * @param usuario
     * @return
     */
    public List < Rol > obtenerRoles(Usuario usuario) {
        return obtenerRoles(usuario, Locale.getDefault());
    }

    /**
     * TODO Describir m�todo obtenerRoles.
     * @param usuario
     * @param locale
     * @return
     */
    public List < Rol > obtenerRoles(Usuario usuario, Locale locale) {
        throw new UnsupportedOperationException(
            "La operacion obtenerRoles no se encuentra implementada.");
    }

    /**
     * TODO Describir m�todo registrarEvento.
     * @param evento
     */
    public void registrarEvento(Evento evento) {
        registrarEvento(evento, Locale.getDefault());
    }

    /**
     * TODO Describir m�todo registrarEvento.
     * @param evento
     * @param locale
     */
    public void registrarEvento(Evento evento, Locale locale) {
        if (evento == null) {
            throw new NullPointerException(
                "El evento especificado no debe ser nulo.");
        }
        SeguridadDAO seguridadDAO =
            getSeguridadDAO(evento.getTipoUsuario(), locale);
        try {
            seguridadDAO.registrarEvento(evento);
        } finally {
            seguridadDAO.close();
        }
    }

    /**
     * TODO Describir m�todo getSeguridadDAO.
     * @param tipoUsuario
     * @param locale TODO
     * @return
     */
    private SeguridadDAO getSeguridadDAO(int tipoUsuario, Locale locale) {
        return SeguridadDAOFactory.getInstance(locale).getSeguridadDAO(
            tipoUsuario);
    }
}
