package cl.tinet.common.seguridad.interfaces;

import com.ibm.ejs.container.*;

/**
 * EJSStatelessGestorDeSeguridadHomeBean_913792aa
 */
public class EJSStatelessGestorDeSeguridadHomeBean_913792aa extends EJSHome {
	static final long serialVersionUID = 61;
	/**
	 * EJSStatelessGestorDeSeguridadHomeBean_913792aa
	 */
	public EJSStatelessGestorDeSeguridadHomeBean_913792aa() throws java.rmi.RemoteException {
		super();	}
	/**
	 * create
	 */
	public cl.tinet.common.seguridad.interfaces.GestorDeSeguridad create() throws javax.ejb.CreateException, java.rmi.RemoteException {
BeanO beanO = null;
cl.tinet.common.seguridad.interfaces.GestorDeSeguridad result = null;
boolean createFailed = false;
try {
	result = (cl.tinet.common.seguridad.interfaces.GestorDeSeguridad) super.createWrapper(null);
}
catch (javax.ejb.CreateException ex) {
	createFailed = true;
	throw ex;
} catch (java.rmi.RemoteException ex) {
	createFailed = true;
	throw ex;
} catch (Throwable ex) {
	createFailed = true;
	throw new CreateFailureException(ex);
} finally {
	if (createFailed) {
		super.createFailure(beanO);
	}
}
return result;	}
}
