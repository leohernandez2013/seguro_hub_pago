package cl.tinet.common.seguridad.interfaces;

import com.ibm.ejs.container.*;

/**
 * EJSRemoteStatelessGestorDeSeguridad_913792aa
 */
public class EJSRemoteStatelessGestorDeSeguridad_913792aa extends EJSWrapper implements GestorDeSeguridad {
	/**
	 * EJSRemoteStatelessGestorDeSeguridad_913792aa
	 */
	public EJSRemoteStatelessGestorDeSeguridad_913792aa() throws java.rmi.RemoteException {
		super();	}
	/**
	 * autenticar
	 */
	public boolean autenticar(java.lang.String rut, java.lang.String dv, java.lang.String clave, int tipoUsuario) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		boolean _EJS_result = false;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[4];
				_jacc_parms[0] = rut;
				_jacc_parms[1] = dv;
				_jacc_parms[2] = clave;
				_jacc_parms[3] = new java.lang.Integer(tipoUsuario);
			}
	cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean beanRef = (cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean)container.preInvoke(this, 0, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.autenticar(rut, dv, clave, tipoUsuario);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 0, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * cambiarClaveTemporal
	 */
	public boolean cambiarClaveTemporal(cl.tinet.common.seguridad.model.Usuario usuario, java.lang.String claveTemporal, java.lang.String claveNueva, long locale) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		boolean _EJS_result = false;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[4];
				_jacc_parms[0] = usuario;
				_jacc_parms[1] = claveTemporal;
				_jacc_parms[2] = claveNueva;
				_jacc_parms[3] = new java.lang.Long(locale);
			}
	cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean beanRef = (cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean)container.preInvoke(this, 1, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.cambiarClaveTemporal(usuario, claveTemporal, claveNueva, locale);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 1, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * generarClaveTemporal
	 */
	public boolean generarClaveTemporal(java.lang.String rut, java.lang.String dv, long locale, int tipoUsuario) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		boolean _EJS_result = false;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[4];
				_jacc_parms[0] = rut;
				_jacc_parms[1] = dv;
				_jacc_parms[2] = new java.lang.Long(locale);
				_jacc_parms[3] = new java.lang.Integer(tipoUsuario);
			}
	cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean beanRef = (cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean)container.preInvoke(this, 2, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.generarClaveTemporal(rut, dv, locale, tipoUsuario);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 2, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * validacionLDAP
	 */
	public boolean validacionLDAP(java.lang.String userName, java.lang.String password, int tipoUsuario) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		boolean _EJS_result = false;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[3];
				_jacc_parms[0] = userName;
				_jacc_parms[1] = password;
				_jacc_parms[2] = new java.lang.Integer(tipoUsuario);
			}
	cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean beanRef = (cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean)container.preInvoke(this, 3, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.validacionLDAP(userName, password, tipoUsuario);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 3, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * autenticar
	 */
	public cl.tinet.common.seguridad.model.Usuario autenticar(java.lang.String usuario, java.lang.String clave, int tipoUsuario) throws cl.tinet.common.seguridad.exception.AutenticacionException, java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.tinet.common.seguridad.model.Usuario _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[3];
				_jacc_parms[0] = usuario;
				_jacc_parms[1] = clave;
				_jacc_parms[2] = new java.lang.Integer(tipoUsuario);
			}
	cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean beanRef = (cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean)container.preInvoke(this, 4, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.autenticar(usuario, clave, tipoUsuario);
		}
		catch (cl.tinet.common.seguridad.exception.AutenticacionException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 4, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * autenticar
	 */
	public cl.tinet.common.seguridad.model.Usuario autenticar(java.lang.String usuario, java.lang.String clave, int tipoUsuario, java.lang.Long locale) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.tinet.common.seguridad.model.Usuario _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[4];
				_jacc_parms[0] = usuario;
				_jacc_parms[1] = clave;
				_jacc_parms[2] = new java.lang.Integer(tipoUsuario);
				_jacc_parms[3] = locale;
			}
	cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean beanRef = (cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean)container.preInvoke(this, 5, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.autenticar(usuario, clave, tipoUsuario, locale);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 5, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * autenticar
	 */
	public cl.tinet.common.seguridad.model.Usuario autenticar(java.lang.String usuario, java.lang.String clave, int tipoUsuario, java.util.Locale locale) throws cl.tinet.common.seguridad.exception.AutenticacionException, java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.tinet.common.seguridad.model.Usuario _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[4];
				_jacc_parms[0] = usuario;
				_jacc_parms[1] = clave;
				_jacc_parms[2] = new java.lang.Integer(tipoUsuario);
				_jacc_parms[3] = locale;
			}
	cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean beanRef = (cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean)container.preInvoke(this, 6, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.autenticar(usuario, clave, tipoUsuario, locale);
		}
		catch (cl.tinet.common.seguridad.exception.AutenticacionException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 6, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerUsuarioExterno
	 */
	public cl.tinet.common.seguridad.model.UsuarioExterno obtenerUsuarioExterno(java.lang.String userName, int i) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.tinet.common.seguridad.model.UsuarioExterno _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = userName;
				_jacc_parms[1] = new java.lang.Integer(i);
			}
	cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean beanRef = (cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean)container.preInvoke(this, 7, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerUsuarioExterno(userName, i);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 7, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * loginFallido
	 */
	public java.lang.Integer loginFallido(boolean b, cl.tinet.common.seguridad.model.Usuario usuario) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.lang.Integer _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = new java.lang.Boolean(b);
				_jacc_parms[1] = usuario;
			}
	cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean beanRef = (cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean)container.preInvoke(this, 8, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.loginFallido(b, usuario);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 8, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * autorizar
	 */
	public java.util.List autorizar(cl.tinet.common.seguridad.model.Usuario usuario) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = usuario;
			}
	cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean beanRef = (cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean)container.preInvoke(this, 9, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.autorizar(usuario);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 9, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * autorizar
	 */
	public java.util.List autorizar(cl.tinet.common.seguridad.model.Usuario usuario, java.util.Locale locale) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = usuario;
				_jacc_parms[1] = locale;
			}
	cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean beanRef = (cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean)container.preInvoke(this, 10, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.autorizar(usuario, locale);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 10, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerFuncionalidades
	 */
	public java.util.List obtenerFuncionalidades(cl.tinet.common.seguridad.model.Usuario usuario) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = usuario;
			}
	cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean beanRef = (cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean)container.preInvoke(this, 11, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerFuncionalidades(usuario);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 11, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerFuncionalidades
	 */
	public java.util.List obtenerFuncionalidades(cl.tinet.common.seguridad.model.Usuario usuario, java.util.Locale locale) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = usuario;
				_jacc_parms[1] = locale;
			}
	cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean beanRef = (cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean)container.preInvoke(this, 12, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerFuncionalidades(usuario, locale);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 12, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerRoles
	 */
	public java.util.List obtenerRoles(cl.tinet.common.seguridad.model.Usuario usuario) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = usuario;
			}
	cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean beanRef = (cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean)container.preInvoke(this, 13, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerRoles(usuario);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 13, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerRoles
	 */
	public java.util.List obtenerRoles(cl.tinet.common.seguridad.model.Usuario usuario, java.util.Locale locale) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = usuario;
				_jacc_parms[1] = locale;
			}
	cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean beanRef = (cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean)container.preInvoke(this, 14, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerRoles(usuario, locale);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 14, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * bloquearCuentaUsuario
	 */
	public void bloquearCuentaUsuario(java.lang.String userName, int tipo) throws cl.tinet.common.seguridad.exception.BloqueoCuentaException, java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = userName;
				_jacc_parms[1] = new java.lang.Integer(tipo);
			}
	cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean beanRef = (cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean)container.preInvoke(this, 15, _EJS_s, _jacc_parms);
			beanRef.bloquearCuentaUsuario(userName, tipo);
		}
		catch (cl.tinet.common.seguridad.exception.BloqueoCuentaException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 15, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * registrarEvento
	 */
	public void registrarEvento(cl.tinet.common.seguridad.model.Evento evento) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = evento;
			}
	cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean beanRef = (cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean)container.preInvoke(this, 16, _EJS_s, _jacc_parms);
			beanRef.registrarEvento(evento);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 16, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * registrarEvento
	 */
	public void registrarEvento(cl.tinet.common.seguridad.model.Evento evento, java.util.Locale locale) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = evento;
				_jacc_parms[1] = locale;
			}
	cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean beanRef = (cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean)container.preInvoke(this, 17, _EJS_s, _jacc_parms);
			beanRef.registrarEvento(evento, locale);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 17, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * registrarEventoAuditoria
	 */
	public void registrarEventoAuditoria(int tipo, int idUsuario, java.lang.Integer id_funcionalidad, java.lang.String descripcion, java.util.Date date, int i) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[6];
				_jacc_parms[0] = new java.lang.Integer(tipo);
				_jacc_parms[1] = new java.lang.Integer(idUsuario);
				_jacc_parms[2] = id_funcionalidad;
				_jacc_parms[3] = descripcion;
				_jacc_parms[4] = date;
				_jacc_parms[5] = new java.lang.Integer(i);
			}
	cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean beanRef = (cl.tinet.common.seguridad.ejb.GestorDeSeguridadBean)container.preInvoke(this, 18, _EJS_s, _jacc_parms);
			beanRef.registrarEventoAuditoria(tipo, idUsuario, id_funcionalidad, descripcion, date, i);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 18, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
}
