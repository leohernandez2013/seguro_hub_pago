package cl.tinet.common.seguridad.ejb;

import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.tinet.common.mail.Mail;
import cl.tinet.common.seguridad.config.SeguridadConfig;
import cl.tinet.common.seguridad.dao.SeguridadDAO;
import cl.tinet.common.seguridad.dao.SeguridadDAOFactory;
import cl.tinet.common.seguridad.exception.AutenticacionException;
import cl.tinet.common.seguridad.exception.BloqueoCuentaException;
import cl.tinet.common.seguridad.model.Evento;
import cl.tinet.common.seguridad.model.Funcionalidad;
import cl.tinet.common.seguridad.model.Rol;
import cl.tinet.common.seguridad.model.Usuario;
import cl.tinet.common.seguridad.model.UsuarioExterno;
import cl.tinet.common.seguridad.service.SeguridadService;
import cl.tinet.common.seguridad.service.SeguridadServiceFactory;
import cl.tinet.common.util.crypto.CryptoUtil;

/**
 * @author tinet
 * @version 1.0
 * @created 27-Ene-2010 17:12:09
 * 
 * @ejb.bean name="GestorDeSeguridad" display-name="Gestor de seguridad"
 *           description="Expone servicios de seguridad"
 *           jndi-name="ejb/ventaseguros/GestorDeSeguridad" type="Stateless"
 *           view-type="remote" transaction-type="Container"
 * 
 * @ejb.transaction type="Supports"
 * 
 * @ejb.resource-ref res-ref-name="jdbc/seguridadDS"
 *                   res-type="javax.sql.DataSource"
 *                   res-sharing-scope="Shareable" res-auth="Container"
 *                   jndi-name="jdbc/seguridadDS"
 * 
 * @ejb.resource-ref res-ref-name="jdbc/VSPDS"
 *                   res-type="javax.sql.DataSource"
 *                   res-sharing-scope="Shareable" res-auth="Container"
 *                   jndi-name="jdbc/VSPDS"
 *                   
 * @ejb.env-entry name="url/ldap1" type = "java.lang.String"
 *                value="ldap://192.168.50.132:389"
 * 
 * @ejb.env-entry name="url/ldap2" type = "java.lang.String"
 *                value="ldap://192.168.50.133:389"
 * 
 * @ejb.env-entry name="username/ldap" type = "java.lang.String" value=""
 * 
 * @ejb.env-entry name="password/ldap" type = "java.lang.String" value=""
 * 
 * @ejb.env-entry name="group/ldap" type = "java.lang.String" value=""
 * 
 */
public class GestorDeSeguridadBean implements SessionBean, SeguridadService {

    /**
     * Constante para para clave password.
     */
    public static final String MESSAGE_RECOVERY_PASSWORD_KEY =
        "cl.tinet.common.seguridad.MESSAGE_RECOVERY_PASSWORD";

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * sessionContext.
     */
    private SessionContext sessionContext;

    private static Log logger = LogFactory.getLog(GestorDeSeguridadBean.class);

    /**
     * Constructor de la clase.
     */
    public GestorDeSeguridadBean() {
    }

    /**
     * Permite realizar la autenticaci�n del usuario. En caso de que la
     * autenticaci�n no sea exitosa se lanza una AutenticacionFallidaException.
     * 
     * @ejb.interface-method view-type="remote"
     * @ejb.transaction type="Required"
     * 
     * @param usuario
     *            login del usuario.
     * @param clave
     *            clave del usuario.
     * @param tipoUsuario
     *            tipo de usuario.
     * @return usuario.
     * @throws AutenticacionException
     */
    public Usuario autenticar(String usuario, String clave, int tipoUsuario)
        throws AutenticacionException {
        return this
            .autenticar(usuario, clave, tipoUsuario, Locale.getDefault());
    }

    /**
     * Permite realizar la autenticaci�n del usuario. En caso de que la
     * autenticaci�n no sea exitosa regresa un objeto Usuario NULO.
     * 
     * @ejb.interface-method view-type="remote"
     * @ejb.transaction type="Required"
     * 
     * @param usuario
     *            login del usuario.
     * @param clave
     *            clave del usuario.
     * @param tipoUsuario
     *            tipo de usuario.
     * @param locale
     *            variable de localizaci�n.
     * @return Usuario.
     */
    public Usuario autenticar(String usuario, String clave, int tipoUsuario,
        Long locale) {

        SeguridadDAOFactory daoFactory = SeguridadDAOFactory.getInstance();
        SeguridadDAO dao = daoFactory.getSeguridadDAO(tipoUsuario);

        Usuario usuarioTO;
        Usuario devolverUsuario = null;

        try {

            Usuario userTO = new Usuario();
            userTO.setUsername(usuario);
            userTO.setTipo(tipoUsuario);

            if (!dao.usuarioInscrito(userTO, clave)) {
                userTO.setResultadoValidacion(2);
                devolverUsuario = userTO;

            } else {
                userTO =
                    dao.obtenerUsuario(usuario, clave, tipoUsuario, locale);

                if (userTO == null) {
                    // se equivoco en la contrase�a
                    usuarioTO = new Usuario();
                    usuarioTO.setUsername(usuario);
                    usuarioTO.setTipo(tipoUsuario);

                    Integer cantIntentos = dao.loginFallido(true, usuarioTO);
                    // true para aumentar
                    if (cantIntentos > 2) {
                        // Parte desde el cero 0,1,2 (tres veces)
                        if (dao.obtenerEstadoUsuario(usuarioTO) != 2) {
                            // Si no tiene el estado 2, significa
                            // que no esta bloqueado

                            if (!dao.cambiarEstadoUsuario(usuarioTO
                                .getUsername(), locale, 2)) {
                                // saltar una excepcion.. .
                            }
                            usuarioTO.setResultadoValidacion(3);
                            usuarioTO.setIdUsuario(dao
                                .obtenerIdUsuario(usuario));
                            devolverUsuario = usuarioTO;
                        } else {
                            usuarioTO.setResultadoValidacion(3);
                            usuarioTO.setIdUsuario(dao
                                .obtenerIdUsuario(usuario));
                            devolverUsuario = usuarioTO;
                        }
                    } else {
                        usuarioTO.setResultadoValidacion(4);
                        usuarioTO.setIdUsuario(dao.obtenerIdUsuario(usuario));
                        devolverUsuario = usuarioTO;
                    }
                } else {
                    Integer estadoPsw = dao.obtenerEstadoUsuario(userTO);

                    if (estadoPsw == 3) {
                        // Esta Temporal
                        userTO.setResultadoValidacion(5);
                        devolverUsuario = userTO;
                    } else {
                        if (estadoPsw == 2) { // usuario bloqueado
                            if (validarFecha(dao.obtenerFechas(userTO
                                .getUsername(), clave, tipoUsuario))) {
                                if (dao.cambiarEstadoUsuario(userTO, locale, 1)) {
                                    // 1== activo.
                                    dao.loginFallido(false, userTO);

                                    // vuelvo a cero los intentos.
                                    userTO.setRoles(dao.obtenerRoles(userTO));
                                    userTO.setUsuarioExterno(dao
                                        .getUsuarioExterno(userTO));
                                    userTO.setUsuarioInterno(dao
                                        .getUsuarioInterno(userTO));
                                    userTO.setResultadoValidacion(1);
                                    devolverUsuario = userTO;
                                }

                            } else {
                                userTO.setResultadoValidacion(3);
                                devolverUsuario = userTO;
                            }
                        } else {

                            dao.loginFallido(false, userTO);
                            // vuelvo a cero los intentos, para
                            // que se pueda volver a logear

                            if (userTO.getTipo() == 1) {
                                userTO.setResultadoValidacion(1);
                                userTO.setUsuarioExterno(dao
                                    .getUsuarioExterno(userTO));
                                userTO.setUsuarioInterno(dao
                                    .getUsuarioInterno(userTO));
                                userTO.setRoles(dao.obtenerRoles(userTO));
                                devolverUsuario = userTO;
                            } else {
                                userTO.setResultadoValidacion(1);
                                userTO.setUsuarioInterno(dao
                                    .getUsuarioInterno(userTO));
                                userTO.setRoles(dao.obtenerRoles(userTO));
                                devolverUsuario = userTO;

                            }

                        }
                    }

                }

            }

        } finally {
            dao.close();
        }
        return devolverUsuario;
    }

    /**
     * Permite realizar la autenticaci�n del usuario. Devuelve un booleano true,
     * si la autentifacion fue correcta.
     * 
     * @param rut
     *            rut del usuario.
     * @param dv
     *            digito verificador.
     * @param clave
     *            clave del usuario.
     * @param tipoUsuario
     *            tipo de usuario.
     * @return indicador de autenticaci�n.
     * 
     * @ejb.interface-method view-type="remote"
     */
    public boolean autenticar(String rut, String dv, String clave,
        int tipoUsuario) {

        SeguridadDAOFactory daoFactory = SeguridadDAOFactory.getInstance();
        SeguridadDAO dao = daoFactory.getSeguridadDAO(tipoUsuario);
        try {
            return dao.autentificar(rut, dv, clave);
        } finally {
            dao.close();
        }

    }

    /**
     * Obtiene la lista de funciones que tiene un usuario en particular.
     * 
     * @ejb.interface-method view-type="remote"
     * @ejb.transaction type="Supports"
     * 
     * @param usuario
     *            usuario para autorizar.
     * @return lista de funciones asociadas.
     */
    public List autorizar(Usuario usuario) {

        SeguridadDAOFactory daoFactory = SeguridadDAOFactory.getInstance();
        SeguridadDAO dao = daoFactory.getSeguridadDAO(usuario.getTipo());
        try {
            return dao.obtenerRoles(usuario);
        } finally {
            dao.close();
        }
    }

    /**
     * Metodo para autorizar, un usuario.
     * 
     * @ejb.interface-method view-type="remote"
     * @ejb.transaction type="Supports"
     * @param usuario
     *            Usuariopara autorizar.
     * @param locale
     *            variable de localizaci�n.
     * 
     * @return lista de funciones asociadas.
     * 
     * 
     */
    public List autorizar(Usuario usuario, Locale locale) {
        return null;
    }

    /**
     * Permite bloquear (PSW) un usuario.
     * 
     * @ejb.interface-method view-type="remote"
     * @ejb.transaction type="Supports"
     * 
     * @param userName
     *            login de usuario.
     * @param tipo
     *            tipo de Usuario.
     * 
     * @throws BloqueoCuentaException
     *             Exception interno en caso de bloqueo.
     * 
     */
    public void bloquearCuentaUsuario(String userName, int tipo)
        throws BloqueoCuentaException {
        Boolean resultado = false;
        SeguridadDAOFactory daoFactory = SeguridadDAOFactory.getInstance();
        SeguridadDAO dao = daoFactory.getSeguridadDAO(tipo);
        try {
            // resultado = dao.bloquearCuentaUsuario(userName);

            if (!resultado) {
                throw new BloqueoCuentaException(
                    "Problema en bloquear la cuenta de usuario");
            }
        } finally {
            dao.close();
        }
    }

    /**
     * 
     * Metodo para validar, login fallidos.
     * 
     * @ejb.interface-method view-type="remote"
     * @ejb.transaction type="Supports"
     * 
     * @param b
     *            indicador si se reinicia el conteo.
     * @param usuario
     *            usuario del login.
     * @return numero de intendos del login.
     */
    public Integer loginFallido(boolean b, Usuario usuario) {

        Integer resultado;
        SeguridadDAOFactory daoFactory = SeguridadDAOFactory.getInstance();
        SeguridadDAO dao = daoFactory.getSeguridadDAO(usuario.getTipo());
        try {

            resultado = dao.loginFallido(b, usuario);

            if (resultado == null) {
                resultado = new Integer(-1);
            } else {

                if (resultado >= 3) {

                    if (!dao.cambiarEstadoUsuario(usuario.getUsername(), 1, 2)) {
                        // saltar una excepcion...
                    }
                    resultado = 1;
                }
            }

        } finally {
            dao.close();
        }
        return resultado;
    }

    /**
     * Genera una clave temporal y envia el correo al correo del usuario En caso
     * de error, devuelve false.
     * 
     * @param rut
     *            rut del usuario.
     * @param dv
     *            digito verificador
     * @param locale
     *            variable de localizaci�n.
     * @param tipoUsuario
     *            tipo de usuario.
     * @return indicador si se genero la nueva clave.
     * 
     * @ejb.interface-method view-type="remote"
     * @ejb.t|ransaction type="Supports"
     */
    public boolean generarClaveTemporal(String rut, String dv, long locale,
        int tipoUsuario) {
        boolean devuelta = false;
        SeguridadDAOFactory daoFactory = SeguridadDAOFactory.getInstance();
        CryptoUtil cryptoUtil = new CryptoUtil(SeguridadConfig.getInstance());
        SeguridadDAO dao = daoFactory.getSeguridadDAO(tipoUsuario);
        Map mapUsuarioExterno;
        boolean resultado;
        String userName = rut + "-" + dv;
        String cadenaClave = "";
        String clave = "";
        int x;

        try {
            if (dao.existenciaUsuario(userName)) {
                if (dao.obtenerEstadoUsuario(userName) != 2) {
                    // Usuario no bloqueado
                    cadenaClave = this.generarCadenaAleatoria();
                    clave =
                        CryptoUtil.toBase64String(cryptoUtil
                            .getEncrypted(cadenaClave), true);

                    mapUsuarioExterno = dao.getUsuarioExterno(rut, dv, locale);
                    if (dao.cambiarEstadoUsuario(userName, locale, 3)) {
                        // 3 == temporal
                        if (dao.cambiarClaveUsuario(userName, locale, clave)) {
                            this.enviarCorreo(cadenaClave, mapUsuarioExterno,
                                tipoUsuario);
                            devuelta = true;
                        }

                    }

                } else {
                    devuelta = false;
                }

            } else {
                devuelta = false;
            }

        } finally {
            dao.close();
        }
        return devuelta;
    }

    /**
     * Registra la accion, realizada por el usuario.
     * 
     * @param tipo
     *            tipo de evento.
     * @param idUsuario
     *            identificador de usuario.
     * @param id_funcionalidad
     *            identidicador de funcionalidad.
     * @param descripcion
     *            descripcion de la acci�n.
     * @param date
     *            fecha.
     * @param i
     *            localizaci�n.
     * 
     * @ejb.interface-method view-type="remote"
     */
    public void registrarEventoAuditoria(int tipo, int idUsuario,
        Integer id_funcionalidad, String descripcion, Date date, int i) {
        SeguridadDAOFactory daoFactory = SeguridadDAOFactory.getInstance();
        SeguridadDAO dao = daoFactory.getSeguridadDAO(tipo);

        try {
            dao.registrarEventoAuditoria(idUsuario, id_funcionalidad,
                descripcion, date, i);
        } finally {
            dao.close();
        }

    }

    /**
     * Metodo relacionado con ObtenerUsuario.
     * 
     * @return Usuario.
     */
    public Usuario getUsuario() {
        return null;
    }

    /**
     * Valida la fecha, para desbloquear cuenta (PSW) devuelve true en caso que
     * la fecha sea igual o sobre pase la fecha actual devuelve false en caso
     * que la fecha sea menor a la fecha actual.
     * 
     * @param fechas
     *            fechas para validar.
     * @return indicador de la validaci�n.
     * 
     * 
     */
    private boolean validarFecha(Map fechas) {

        Integer tiempoBloqueo = (Integer) fechas.get("tiempoBloqueo");
        boolean resultado = false;
        Date fechaB = (Date) fechas.get("fechabloqueo");
        Calendar fechaBloqueo = Calendar.getInstance();
        fechaBloqueo.setTimeInMillis(fechaB.getTime());
        fechaBloqueo.add(Calendar.DAY_OF_YEAR, tiempoBloqueo);

        GregorianCalendar fechaActual = new GregorianCalendar();

        return fechaActual.after(fechaBloqueo);
    }

    /**
     * Envia el correo de peticion de cambio de clave.
     * 
     * @param cadenaClave
     *            clave asociada.
     * @param mapUsuarioExterno
     *            usuario.
     */
    private void enviarCorreo(String cadenaClave, Map mapUsuarioExterno,
        int tipoUsuario) {
        SeguridadDAOFactory daoFactory = SeguridadDAOFactory.getInstance();
        SeguridadDAO dao = daoFactory.getSeguridadDAO(tipoUsuario);

        String nombre = (String) mapUsuarioExterno.get("nombre");

        String mensaje = "";

        String email = (String) mapUsuarioExterno.get("email");
        String server = dao.obtenerDatosEmail("SERVER_CORREO");
        String from = dao.obtenerDatosEmail("MAIL_FROM");
        String asunto = dao.obtenerDatosEmail("MAIL_SUBJECT_CLAVE");

        Mail mail = new Mail(from, email, asunto, "", server);

        try {
            InputStream is = dao.obtenerCuerpoEmail("HTML_CLAVE");

            String archivo = mail.convertStreamToString(is);

            //Reemplazar nombre y clave.
            mensaje = archivo.replace("{0}", nombre);
            mensaje = mensaje.replace("{1}", cadenaClave);

            mail.addContenido(mensaje);
            mail.sendMail();

        } catch (Exception e) {
            logger.error("Error al enviar email", e);
        } finally {
            dao.close();
        }
    }

    /**
     * Realiza el cambio de clave, para pasar de la clave temporal a la
     * permamente.
     * 
     * @param usuario
     *            usuario asociado.
     * @param claveTemporal
     *            clave temporal.
     * @param claveNueva
     *            clave nueva asociada.
     * @param locale
     *            variable de localizaci�n.
     * @return indicador del cambio de clave.
     * 
     * @ejb.interface-method view-type="remote"
     */
    public boolean cambiarClaveTemporal(Usuario usuario, String claveTemporal,
        String claveNueva, long locale) {

        boolean resultado = false;
        SeguridadDAOFactory daoFactory = SeguridadDAOFactory.getInstance();
        SeguridadDAO dao = daoFactory.getSeguridadDAO(usuario.getTipo());
        try {
            if (!dao.cambiarClaveTemporal(usuario.getUsername(), claveTemporal,
                claveNueva)) {
                resultado = false;
            } else {
                if (dao.cambiarEstadoUsuario(usuario.getUsername(), locale, 1)) {
                    // se activa el usuario
                    dao.loginFallido(false, usuario);
                    resultado = true;
                }
            }

        } finally {
            dao.close();
        }
        return resultado;
    }

    /**
     * Obtiene un objeto de tipo UsuarioExterno.
     * 
     * @param userName
     *            nomrbe de usuario
     * @param i
     *            tipo de usuario.
     * @return Usuario externo.
     * 
     * @ejb.interface-method view-type="remote"
     */

    public UsuarioExterno obtenerUsuarioExterno(String userName, int i) {

        SeguridadDAOFactory daoFactory = SeguridadDAOFactory.getInstance();
        SeguridadDAO dao = daoFactory.getSeguridadDAO(i);
        UsuarioExterno usuarioExterno;

        Usuario userTO = new Usuario();
        userTO.setUsername(userName);
        userTO.setTipo(i);
        try {
            usuarioExterno = dao.getUsuarioExterno(userTO);
        } finally {
            dao.close();
        }

        return usuarioExterno;
    }

    /**
     * Genera una cadena con cuadro digitos (numericos) en forma aleatoria.
     * 
     * @return nueva cadena.
     */
    private String generarCadenaAleatoria() {

        String clave = "";
        int x;
        Random r = new Random();

        for (int i = 0; i < 4; i++) {
            x = (int) (r.nextDouble() * 10.0);
            clave += x;
        }
        return clave;
    }

    /**
     * valida la existencia de un usuario contra LDAP.
     * 
     * @param userName
     *            usuario a validar
     * @param tipoUsuario
     *            tipo de usuario.
     * @return indicador de validaci�n LDAP
     * 
     * @ejb.interface-method view-type="remote"
     */
    public boolean validacionLDAP(String userName, String password,
        int tipoUsuario) {
        SeguridadDAOFactory daoFactory = SeguridadDAOFactory.getInstance();
        SeguridadDAO dao = daoFactory.getSeguridadDAO(tipoUsuario);
        try {
            return dao.validacionLDAP(userName, password);
        } finally {
            dao.close();
        }
    }

    /**
     * Metodo create.
     */
    public void ejbCreate() {

    }

    /**
     * metodo Activate.
     */
    public void ejbActivate() {

    }

    /**
     * Metodo passivate.
     */
    public void ejbPassivate() {

    }

    /**
     * Metodo remove.
     */
    public void ejbRemove() {

    }

    /**
     * Metodo setSessionContext.
     * 
     * @param sc
     *            session context.
     */
    public void setSessionContext(SessionContext sc) {
        this.sessionContext = sc;
    }

    /**
     * TODO Describir m�todo autenticar.
     * 
     * @param usuario
     * @param clave
     * @param tipoUsuario
     * @param locale
     * @return
     * @throws AutenticacionException
     * 
     * @ejb.interface-method view-type="remote"
     * @ejb.transaction type="NotSupported"
     */
    public Usuario autenticar(String usuario, String clave, int tipoUsuario,
        Locale locale) throws AutenticacionException {
        SeguridadServiceFactory factory = SeguridadServiceFactory.getInstance();
        SeguridadService service =
            factory.getSeguridadService(
                SeguridadServiceFactory.AUTH_SERVICE_REAL_KEY, locale);
        return service.autenticar(usuario, clave, tipoUsuario, locale);
    }

    /**
     * TODO Describir m�todo obtenerFuncionalidades.
     * 
     * @param usuario
     * @return
     * 
     * @ejb.interface-method view-type="remote"
     */
    public List < Funcionalidad > obtenerFuncionalidades(Usuario usuario) {
        SeguridadServiceFactory factory = SeguridadServiceFactory.getInstance();
        SeguridadService service =
            factory.getSeguridadService(
                SeguridadServiceFactory.AUTH_SERVICE_REAL_KEY, Locale
                    .getDefault());
        return service.obtenerFuncionalidades(usuario);
    }

    /**
     * TODO Describir m�todo obtenerFuncionalidades.
     * 
     * @param usuario
     * @param locale
     * @return
     * 
     * @ejb.interface-method view-type="remote"
     */
    public List < Funcionalidad > obtenerFuncionalidades(Usuario usuario,
        Locale locale) {
        SeguridadServiceFactory factory = SeguridadServiceFactory.getInstance();
        SeguridadService service =
            factory.getSeguridadService(
                SeguridadServiceFactory.AUTH_SERVICE_REAL_KEY, locale);
        return service.obtenerFuncionalidades(usuario, locale);
    }

    /**
     * TODO Describir m�todo obtenerRoles.
     * 
     * @param usuario
     * @return
     * 
     * @ejb.interface-method view-type="remote"
     */
    public List < Rol > obtenerRoles(Usuario usuario) {
        SeguridadServiceFactory factory = SeguridadServiceFactory.getInstance();
        SeguridadService service =
            factory.getSeguridadService(
                SeguridadServiceFactory.AUTH_SERVICE_REAL_KEY, Locale
                    .getDefault());
        return service.obtenerRoles(usuario);
    }

    /**
     * TODO Describir m�todo obtenerRoles.
     * 
     * @param usuario
     * @param locale
     * @return
     * 
     * @ejb.interface-method view-type="remote"
     */
    public List < Rol > obtenerRoles(Usuario usuario, Locale locale) {
        SeguridadServiceFactory factory = SeguridadServiceFactory.getInstance();
        SeguridadService service =
            factory.getSeguridadService(
                SeguridadServiceFactory.AUTH_SERVICE_REAL_KEY, locale);
        return service.obtenerRoles(usuario, locale);
    }

    /**
     * TODO Describir m�todo registrarEvento.
     * @param evento
     * 
     * @ejb.interface-method view-type="remote"
     */
    public void registrarEvento(Evento evento) {
        SeguridadServiceFactory factory = SeguridadServiceFactory.getInstance();
        SeguridadService service =
            factory.getSeguridadService(
                SeguridadServiceFactory.AUTH_SERVICE_REAL_KEY, Locale
                    .getDefault());
        service.registrarEvento(evento);
    }

    /**
     * TODO Describir m�todo registrarEvento.
     * @param evento
     * @param locale
     * 
     * @ejb.interface-method view-type="remote"
     */
    public void registrarEvento(Evento evento, Locale locale) {
        SeguridadServiceFactory factory = SeguridadServiceFactory.getInstance();
        SeguridadService service =
            factory.getSeguridadService(
                SeguridadServiceFactory.AUTH_SERVICE_REAL_KEY, locale);
        service.registrarEvento(evento, locale);
    }

	public Usuario getUsuario(String username, int tipo)
			throws AutenticacionException {
		// TODO Auto-generated method stub
		return null;
	}
	
}
