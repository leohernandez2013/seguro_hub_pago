package cl.tinet.common.seguridad.servlet;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.tinet.common.seguridad.config.SeguridadConfig;
import cl.tinet.common.seguridad.model.Evento;
import cl.tinet.common.seguridad.model.Usuario;
import cl.tinet.common.seguridad.service.SeguridadService;
import cl.tinet.common.seguridad.service.SeguridadServiceFactory;
import cl.tinet.common.seguridad.util.SeguridadUtil;

/**
 * Servlet de cierre de sesi�n.
 * <p>
 * Invalida la sesi�n del usuario con el fin de que no pueda
 * volver a realizar operaciones en la aplicaci�n a menos que
 * se autentique nuevamente.
 * </p>
 * @author Roberto San Mart�n
 * @version 1.0
 * @created Oct 5, 2010
 */
public class CierreSesionServlet extends HttpServlet {

    /**
     * Llave de configuraci�n en el archivo web.xml de la p�gina
     * destino donde se debe redirigir la solicitud una vez que
     * la sesi�n es cerrada.
     */
    public static final String LLAVE_PAGINA_DESTINO =
        "cl.tinet.common.seguridad.PAGINA_DESTINO";

    /**
     * TODO Describir atributo LLAVE_AUDITORIA_ACTIVADA.
     */
    public static final String LLAVE_AUDITORIA_ACTIVADA =
        "cl.tinet.common.seguridad.AUDITORIA_ACTIVADA";

    /**
     * TODO Describir atributo LLAVE_MENSAJE_CIERRE_SESION.
     */
    public static final String LLAVE_MENSAJE_CIERRE_SESION =
        "cl.tinet.common.seguridad.MENSAJE_CIERRE_SESION";

    /**
     * Versi�n de la clase para serializaci�n.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Separador de path web.
     */
    private static final String SEPARADOR_PATH_WEB = "/";

    /**
     * Variable de acceso al log de la aplicaci�n.
     */
    private static Log logger = LogFactory.getLog(CierreSesionServlet.class);

    /**
     * Ruta relativa de destino donde se redirige la solicitud una vez que la
     * sesi�n se ha cerrado.
     */
    private String destino;

    /**
     * TODO Describir atributo auditoriaActivada.
     */
    private boolean auditoriaActivada = true;

    /**
     * TODO Describir m�todo init.
     * @throws ServletException 
     */
    @Override
    public void init() throws ServletException {
        logger.info("Inicializando servlet de cierre de sesi�n.");
        String parametro = getInitParameter(LLAVE_PAGINA_DESTINO);
        if (parametro == null) {
            throw new ServletException("Falta parametro de inicio: "
                + LLAVE_PAGINA_DESTINO);
        }
        if (!parametro.startsWith(SEPARADOR_PATH_WEB)) {
            parametro = SEPARADOR_PATH_WEB + parametro;
        }
        this.destino = parametro;
        parametro = getInitParameter(LLAVE_AUDITORIA_ACTIVADA);
        if (parametro != null) {
            this.auditoriaActivada = Boolean.parseBoolean(parametro);
        }
        logger.info("Servlet de cierre de sesi�n inicializado.");
    }

    /**
     * TODO Describir m�todo service.
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {

        Locale locale = req.getLocale();
        SeguridadServiceFactory factory = SeguridadServiceFactory.getInstance();
        SeguridadService service =
            factory.getSeguridadService(
                SeguridadServiceFactory.AUTH_SERVICE_DEFAULT_KEY, locale);

        Usuario usuario = SeguridadUtil.getUsuario(req);
        if (this.auditoriaActivada) {
            if (usuario != null) {
                SeguridadConfig config = SeguridadConfig.getInstance();
                String descripcion =
                    config.getString(LLAVE_MENSAJE_CIERRE_SESION, locale);
                descripcion = MessageFormat.format(descripcion, usuario);
                service.registrarEvento(new Evento(usuario, descripcion));
            } else {
                logger
                    .debug("Usuario no autenticado. No se registra en auditor�a.");
            }
        }
        SeguridadUtil.logout(req);

        // FIXME Variar en funcion de si se desea un redirect absoluto o relativo.
        resp.sendRedirect(req.getContextPath() + this.destino);
    }
}