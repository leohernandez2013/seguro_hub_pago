package cl.tinet.common.seguridad.util;

import javax.servlet.http.HttpServletRequest;

import nl.bitwalker.useragentutils.DeviceType;
import nl.bitwalker.useragentutils.OperatingSystem;
import nl.bitwalker.useragentutils.UserAgent;

/**
 * Utilitario que permite discriminar si es que el dispositivo conectado a la
 * aplicacion debe ser atendido utilizando la interfaz especial para
 * dispositivos móviles o no.
 * 
 * @author rsanmartin
 */
public class UserAgentUtil {

	/**
	 * Identificador del sistema operativo IOS.
	 */
	public static final String OS_IOS = "IOS";
	
	/**
	 * Identificador de IPAD.
	 */
	public static final String OS_IOS_IPAD = "IPAD";
	
	/**
	 * Identificador del sistema operativo Android.
	 */
	public static final String OS_ANDROID = "ANDROID";
	
	/**
	 * Identificador del sistema operativo Symbian.
	 */
	public static final String OS_SYMBIAN = "SYMBIAN";
	
	/**
	 * Identificador del sistema operativo Symbian.
	 */
	public static final String OS_DESCONOCIDO = "UNKNOW";
	
	/**
	 * Constructor privado sin argumentos.
	 * <p>
	 * Se define privado para evitar que la clase sea instanciada.
	 */
	private UserAgentUtil() {
	}

	/**
	 * Discrimina según los datos de la solicitud especificada si el tipo de
	 * dispositivo conectado debe ser atendido utilizando la interfaz
	 * especializada para dispositivos móviles o no.
	 * <p>
	 * La version actual considera los siguentes dispositivos:
	 * <ul>
	 * <li>Smartphones.</li>
	 * <li>Palms.</li>
	 * <li>Tablets.</li>
	 * </ul>
	 * </p>
	 * 
	 * @param request
	 *            los datos de la solicitud.
	 * @return <code>true</code> si es que el dispositivo debe ser
	 *         direccionado a la version móvil de páginas, <code>false</code>
	 *         en caso contrario.
	 */
	public static boolean isMobileDevice(HttpServletRequest request) {
		
		String header = request.getHeader("User-Agent");
		
		if (header != null) {
			
			UserAgent agent = UserAgent.parseUserAgentString(header);
			OperatingSystem os = agent.getOperatingSystem();
			
			return os.isMobileDevice()
			    || os.getDeviceType().equals(DeviceType.TABLET)
			    || (os.name().toUpperCase().indexOf(OS_DESCONOCIDO)>=0) 
			    || (os.name().toUpperCase().indexOf(OS_SYMBIAN)>=0);
		} else {
			return false;
		}
	}
	
	public static boolean isOSAccepted(HttpServletRequest request) {
		String header = request.getHeader("User-Agent");
		if(header !=null) {
			UserAgent agent = UserAgent.parseUserAgentString(header);
			OperatingSystem os = agent.getOperatingSystem();
			 if(os.name().toUpperCase().indexOf(OS_ANDROID)>= 0) {
				 return true;
			 }else {
				 if(os.name().toUpperCase().indexOf(OS_IOS)>= 0) {
					 return true;
				 } else {
					 if(os.name().toUpperCase().indexOf(OS_IOS_IPAD)>= 0){
						 return true;
					 }else {
						 if(isBlackBerry(header)) {
							 return true;
						 }
					 }
					 return false;
				 }
					 
			 }
		}
		return false;

	}
	
	public static boolean isBlackBerry(String header){
		if(header.toUpperCase().indexOf("BLACKBERRY")>= 0){
			if (header.indexOf("Version/") >= 0) { // ***User Agent in BlackBerry 6 and BlackBerry 7
	            int a = header.indexOf("Version/") + 8; //sumar 8 espacio para saber que versi�n es
	            if(header.substring(a, a+1).equals("7")){
	            	return true;
	            }else{
	            	//OS 6
	            	return false;
	            }
	        }
	        else {// ***User Agent in BlackBerry Device Software 4.2 to 5.0
	            return false;
	        }

		}
		return false;
	}
}
