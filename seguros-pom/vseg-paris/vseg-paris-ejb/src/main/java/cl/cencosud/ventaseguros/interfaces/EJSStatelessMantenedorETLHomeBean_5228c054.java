package cl.cencosud.ventaseguros.interfaces;

import com.ibm.ejs.container.*;

/**
 * EJSStatelessMantenedorETLHomeBean_5228c054
 */
public class EJSStatelessMantenedorETLHomeBean_5228c054 extends EJSHome {
	static final long serialVersionUID = 61;
	/**
	 * EJSStatelessMantenedorETLHomeBean_5228c054
	 */
	public EJSStatelessMantenedorETLHomeBean_5228c054() throws java.rmi.RemoteException {
		super();	}
	/**
	 * create
	 */
	public cl.cencosud.ventaseguros.interfaces.MantenedorETL create() throws javax.ejb.CreateException, java.rmi.RemoteException {
BeanO beanO = null;
cl.cencosud.ventaseguros.interfaces.MantenedorETL result = null;
boolean createFailed = false;
try {
	result = (cl.cencosud.ventaseguros.interfaces.MantenedorETL) super.createWrapper(null);
}
catch (javax.ejb.CreateException ex) {
	createFailed = true;
	throw ex;
} catch (java.rmi.RemoteException ex) {
	createFailed = true;
	throw ex;
} catch (Throwable ex) {
	createFailed = true;
	throw new CreateFailureException(ex);
} finally {
	if (createFailed) {
		super.createFailure(beanO);
	}
}
return result;	}
}
