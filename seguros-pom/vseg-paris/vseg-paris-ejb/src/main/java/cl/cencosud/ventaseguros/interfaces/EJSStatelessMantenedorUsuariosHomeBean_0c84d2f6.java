package cl.cencosud.ventaseguros.interfaces;

import com.ibm.ejs.container.*;

/**
 * EJSStatelessMantenedorUsuariosHomeBean_0c84d2f6
 */
public class EJSStatelessMantenedorUsuariosHomeBean_0c84d2f6 extends EJSHome {
	static final long serialVersionUID = 61;
	/**
	 * EJSStatelessMantenedorUsuariosHomeBean_0c84d2f6
	 */
	public EJSStatelessMantenedorUsuariosHomeBean_0c84d2f6() throws java.rmi.RemoteException {
		super();	}
	/**
	 * create
	 */
	public cl.cencosud.ventaseguros.interfaces.MantenedorUsuarios create() throws javax.ejb.CreateException, java.rmi.RemoteException {
BeanO beanO = null;
cl.cencosud.ventaseguros.interfaces.MantenedorUsuarios result = null;
boolean createFailed = false;
try {
	result = (cl.cencosud.ventaseguros.interfaces.MantenedorUsuarios) super.createWrapper(null);
}
catch (javax.ejb.CreateException ex) {
	createFailed = true;
	throw ex;
} catch (java.rmi.RemoteException ex) {
	createFailed = true;
	throw ex;
} catch (Throwable ex) {
	createFailed = true;
	throw new CreateFailureException(ex);
} finally {
	if (createFailed) {
		super.createFailure(beanO);
	}
}
return result;	}
}
