package cl.cencosud.ventaseguros.delegate;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cl.cencosud.ventaseguros.common.ProductoLeg;
import cl.cencosud.ventaseguros.common.Rama;
import cl.cencosud.ventaseguros.common.Subcategoria;
import cl.cencosud.ventaseguros.common.TipoRama;
import cl.cencosud.ventaseguros.interfaces.PaginaIntermedia;
import cl.cencosud.ventaseguros.interfaces.PaginaIntermediaHome;
import cl.tinet.common.model.exception.BusinessException;

import com.tinet.comun.jndi.ServiceLocator;
import com.tinet.exceptions.system.SystemException;

/**
 * Delegate encargado de manejar lo referente a la pagina intermedia.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 23/08/2010
 */
public class PaginaIntermediaDelegate {

    /**
     * Instancia de la clase paginaIntermedia.
     */
    private PaginaIntermedia paginaIntermedia;

    /**
     * Home de PaginaIntermedia.
     */
    private PaginaIntermediaHome paginaIntermediaHome;

    /**
     * Constructor de PaginaIntermediaDelgate.
     */
    public PaginaIntermediaDelegate() {
        try {
            this.paginaIntermediaHome =
                (PaginaIntermediaHome) ServiceLocator.singleton()
                    .getRemoteHome(PaginaIntermediaHome.JNDI_NAME,
                        PaginaIntermediaHome.class);

            this.paginaIntermedia = this.paginaIntermediaHome.create();
        } catch (Exception e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene un listado de ramas de productos.
     * @return Listado de ramas.
     * @ejb.interface-method "remote"
     */
    public List < Rama > obtenerRamas() {
        try {
            return this.paginaIntermedia.obtenerRamas();
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    /**
     * Obtiene un listado de subcategorias de productos.
     * @return Listado de subcategorias.
     * @param idRama Identificador de la rama.
     * @ejb.interface-method "remote"
     */
    public List < Subcategoria > obtenerSubcategorias(int idRama) {
        try {
            return this.paginaIntermedia.obtenerSubcategorias(idRama);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }
    
    /**
     * Obtiene un listado de subcategorias de productos.
     * @return Listado de subcategorias.
     * @param idRama Identificador de la rama.
     * @param idTipo Identificador del tipo
     * @ejb.interface-method "remote"
     */
    public List < Subcategoria > obtenerSubcategoriasTipo(int idRama, int idTipo) {
        try {
            return this.paginaIntermedia.obtenerSubcategoriasTipo(idRama, idTipo);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }
    
    /**
     * Obtiene un listado de tipo de rama.
     * @return Listado de tipos de rama.
     * @param idRama Identificador de la rama.
     * @ejb.interface-method "remote"
     */
    public List < TipoRama > obtenerTiposRama(int idRama) {
        try {
            return this.paginaIntermedia.obtenerTiposRama(idRama);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    /**
     * Obtiene un listado de subcategorias asociadas a una rama.
     * @param idRama Identificador de la rama.
     * @param idSubcategoria Identificador de la subcategoria.
     * @return listado de subcategorias.
     */
    public List < Map < String, ? > > obtenerListaPaginaIntermedia(int idRama,
        int idSubcategoria) {
        try {
            return this.paginaIntermedia.obtenerListaPaginaIntermedia(idRama,
                idSubcategoria);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    /**
     * Obtiene los datos de una subcatrgoria a partir del id especificado.
     * @param idSubcategoria identificador de subcategoria.
     * @return Subcatogoria.
     */
    public Subcategoria obtenerSubcategoria(int idSubcategoria) {
        try {
            return this.paginaIntermedia.obtenerSubcategoria(idSubcategoria);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    /**
     * Obtiene un listado de producto que tienen relacion con la subcategoria.
     * @param idSubcategoria Identificador de la subcategoria.
     * @param asignado Si es verdadero, se obtienen los productos asociados a 
     * la subcategoria, si es falso, se obtienen los productos disponibles 
     * para asociar a la subcategoria.
     * @param nombre Nombre del producto a buscar.
     * @return Listado de productos.
     */
    public List < ProductoLeg > obtenerListaProductos(int idSubcategoria,
        boolean asignado, String nombre) {
        try {
            return this.paginaIntermedia.obtenerListaProductos(idSubcategoria,
                asignado, nombre);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    /**
     * Agrega un producto a una subcategoria.
     * @param idSubcategoria identificador de subcategoria.
     * @param idProductoLeg identificador de producto legacy.
     * @return identificador de insercion.
     */
    public long agregarProducto(int idSubcategoria, int idProductoLeg) {
        try {
            return this.paginaIntermedia.agregarProducto(idSubcategoria,
                idProductoLeg);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    /**
     * Elimina un producto de una subcategoria.
     * @param idSubcategoria identificador de subcategoria.
     * @param idProductoLeg identificador de producto legacy.
     */
    public void eliminarProducto(int idSubcategoria, int idProductoLeg) {
        try {
            this.paginaIntermedia.eliminarProducto(idSubcategoria,
                idProductoLeg);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    /**
     * Modifica el estado de una subcategoria.
     * @param idSubcategoria Subcategoria a modificar.
     * @param estado Estado actual.
     */
    public void modificarSubcategoria(int idSubcategoria, String titulo,
        int estado) {
        try {
            this.paginaIntermedia.modificarSubcategoria(idSubcategoria, titulo,
                estado);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    /**
     * Obtiene listado de subcategorias asociados a una rama en particular.
     * @param idRama identificador de la rama.
     * @return listado de subcategorias.
     */
    public List < Subcategoria > obtenerSubcategoriasPaginaIntermedia(int idRama) {
        try {
            return this.paginaIntermedia
                .obtenerSubcategoriasPaginaIntermedia(idRama);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    /**
     * Agrega una nueva subcategoria.
     * @param idRama identificador de la rma.
     * @param subcategoria titulo de la subcategoria.
     * @return identificador de la nueva subcategoria.
     * @throws BusinessException Problema si ya existe la subcategoria.
     */
    public long agregarSubcategoria(int idRama, String subcategoria, int idTipo)
        throws BusinessException {
        try {
            return this.paginaIntermedia.agregarSubcategoria(idRama,
                subcategoria, idTipo);

        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

	public List<HashMap<String, Object>> obtenerPromociones(String tipo) 
		throws BusinessException {
        try {
            return this.paginaIntermedia.obtenerPromociones(tipo);

        } catch (RemoteException re) {
            throw new SystemException(re);
        }
	}

	public List<HashMap<String, Object>> obtenerPromocionesSecundarias(String idRama) 
	throws BusinessException {
        try {
            return this.paginaIntermedia.obtenerPromocionesSecundarias(idRama);

        } catch (RemoteException re) {
            throw new SystemException(re);
        }
	}

}
