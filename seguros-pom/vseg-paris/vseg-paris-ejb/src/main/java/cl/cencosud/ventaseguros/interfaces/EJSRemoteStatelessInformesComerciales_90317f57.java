package cl.cencosud.ventaseguros.interfaces;

import com.ibm.ejs.container.*;

/**
 * EJSRemoteStatelessInformesComerciales_90317f57
 */
public class EJSRemoteStatelessInformesComerciales_90317f57 extends EJSWrapper implements InformesComerciales {
	/**
	 * EJSRemoteStatelessInformesComerciales_90317f57
	 */
	public EJSRemoteStatelessInformesComerciales_90317f57() throws java.rmi.RemoteException {
		super();	}
	/**
	 * ingresarPromocionPrincipal
	 */
	public boolean ingresarPromocionPrincipal(java.lang.String principal, java.lang.String tipo, java.lang.String posicion, java.lang.String nombre, java.lang.String linkVer, java.lang.String tracker, java.lang.String imagen, java.util.Date fechaDesde, java.util.Date fechaHasta, java.lang.String tipoBanner, java.lang.String rama, java.lang.String linkConocer, java.lang.String linkCotizar, java.lang.String idPlan) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		boolean _EJS_result = false;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[14];
				_jacc_parms[0] = principal;
				_jacc_parms[1] = tipo;
				_jacc_parms[2] = posicion;
				_jacc_parms[3] = nombre;
				_jacc_parms[4] = linkVer;
				_jacc_parms[5] = tracker;
				_jacc_parms[6] = imagen;
				_jacc_parms[7] = fechaDesde;
				_jacc_parms[8] = fechaHasta;
				_jacc_parms[9] = tipoBanner;
				_jacc_parms[10] = rama;
				_jacc_parms[11] = linkConocer;
				_jacc_parms[12] = linkCotizar;
				_jacc_parms[13] = idPlan;
			}
	cl.cencosud.ventaseguros.ejb.InformesComercialesBean beanRef = (cl.cencosud.ventaseguros.ejb.InformesComercialesBean)container.preInvoke(this, 0, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.ingresarPromocionPrincipal(principal, tipo, posicion, nombre, linkVer, tracker, imagen, fechaDesde, fechaHasta, tipoBanner, rama, linkConocer, linkCotizar, idPlan);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 0, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * ingresarPromocionSecundaria
	 */
	public boolean ingresarPromocionSecundaria(java.lang.String principal, java.lang.String tipo, java.lang.String ordinal, java.lang.String nombre, java.lang.String linkVer, java.lang.String tracker, java.lang.String imagen, java.util.Date fechaDesde, java.util.Date fechaHasta, java.lang.String tipoBanner, java.lang.String rama, java.lang.String linkConocer, java.lang.String linkCotizar, java.lang.String idPlan) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		boolean _EJS_result = false;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[14];
				_jacc_parms[0] = principal;
				_jacc_parms[1] = tipo;
				_jacc_parms[2] = ordinal;
				_jacc_parms[3] = nombre;
				_jacc_parms[4] = linkVer;
				_jacc_parms[5] = tracker;
				_jacc_parms[6] = imagen;
				_jacc_parms[7] = fechaDesde;
				_jacc_parms[8] = fechaHasta;
				_jacc_parms[9] = tipoBanner;
				_jacc_parms[10] = rama;
				_jacc_parms[11] = linkConocer;
				_jacc_parms[12] = linkCotizar;
				_jacc_parms[13] = idPlan;
			}
	cl.cencosud.ventaseguros.ejb.InformesComercialesBean beanRef = (cl.cencosud.ventaseguros.ejb.InformesComercialesBean)container.preInvoke(this, 1, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.ingresarPromocionSecundaria(principal, tipo, ordinal, nombre, linkVer, tracker, imagen, fechaDesde, fechaHasta, tipoBanner, rama, linkConocer, linkCotizar, idPlan);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 1, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerInformeComercialCotizaciones
	 */
	public java.util.List obtenerInformeComercialCotizaciones(java.lang.String fechaDesde, java.lang.String fechaHasta, boolean esPaginado, int numeroPagina, int rama, int subcategoria, int id_plan, java.lang.String compannia) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[8];
				_jacc_parms[0] = fechaDesde;
				_jacc_parms[1] = fechaHasta;
				_jacc_parms[2] = new java.lang.Boolean(esPaginado);
				_jacc_parms[3] = new java.lang.Integer(numeroPagina);
				_jacc_parms[4] = new java.lang.Integer(rama);
				_jacc_parms[5] = new java.lang.Integer(subcategoria);
				_jacc_parms[6] = new java.lang.Integer(id_plan);
				_jacc_parms[7] = compannia;
			}
	cl.cencosud.ventaseguros.ejb.InformesComercialesBean beanRef = (cl.cencosud.ventaseguros.ejb.InformesComercialesBean)container.preInvoke(this, 2, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerInformeComercialCotizaciones(fechaDesde, fechaHasta, esPaginado, numeroPagina, rama, subcategoria, id_plan, compannia);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 2, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerInformeComercialTarjetas
	 */
	public java.util.List obtenerInformeComercialTarjetas(java.util.Date fechaDesde, java.util.Date fechaHasta, boolean esPaginado, int numeroPagina) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[4];
				_jacc_parms[0] = fechaDesde;
				_jacc_parms[1] = fechaHasta;
				_jacc_parms[2] = new java.lang.Boolean(esPaginado);
				_jacc_parms[3] = new java.lang.Integer(numeroPagina);
			}
	cl.cencosud.ventaseguros.ejb.InformesComercialesBean beanRef = (cl.cencosud.ventaseguros.ejb.InformesComercialesBean)container.preInvoke(this, 3, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerInformeComercialTarjetas(fechaDesde, fechaHasta, esPaginado, numeroPagina);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 3, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerInformeComercialVehiculo
	 */
	public java.util.List obtenerInformeComercialVehiculo(java.util.Date fechaDesde, java.util.Date fechaHasta, boolean esPaginado, int numeroPagina) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[4];
				_jacc_parms[0] = fechaDesde;
				_jacc_parms[1] = fechaHasta;
				_jacc_parms[2] = new java.lang.Boolean(esPaginado);
				_jacc_parms[3] = new java.lang.Integer(numeroPagina);
			}
	cl.cencosud.ventaseguros.ejb.InformesComercialesBean beanRef = (cl.cencosud.ventaseguros.ejb.InformesComercialesBean)container.preInvoke(this, 4, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerInformeComercialVehiculo(fechaDesde, fechaHasta, esPaginado, numeroPagina);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 4, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerInformeComercialVentas
	 */
	public java.util.List obtenerInformeComercialVentas(java.lang.String fechaDesde, java.lang.String fechaHasta, boolean esPaginado, int numeroPagina, int rama, int subcategoria, int id_plan, java.lang.String compannia) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[8];
				_jacc_parms[0] = fechaDesde;
				_jacc_parms[1] = fechaHasta;
				_jacc_parms[2] = new java.lang.Boolean(esPaginado);
				_jacc_parms[3] = new java.lang.Integer(numeroPagina);
				_jacc_parms[4] = new java.lang.Integer(rama);
				_jacc_parms[5] = new java.lang.Integer(subcategoria);
				_jacc_parms[6] = new java.lang.Integer(id_plan);
				_jacc_parms[7] = compannia;
			}
	cl.cencosud.ventaseguros.ejb.InformesComercialesBean beanRef = (cl.cencosud.ventaseguros.ejb.InformesComercialesBean)container.preInvoke(this, 5, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerInformeComercialVentas(fechaDesde, fechaHasta, esPaginado, numeroPagina, rama, subcategoria, id_plan, compannia);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 5, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerPromocionesBO
	 */
	public java.util.List obtenerPromocionesBO() throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[0];
			}
	cl.cencosud.ventaseguros.ejb.InformesComercialesBean beanRef = (cl.cencosud.ventaseguros.ejb.InformesComercialesBean)container.preInvoke(this, 6, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerPromocionesBO();
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 6, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerPromocionesBOPrincipal
	 */
	public java.util.List obtenerPromocionesBOPrincipal() throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[0];
			}
	cl.cencosud.ventaseguros.ejb.InformesComercialesBean beanRef = (cl.cencosud.ventaseguros.ejb.InformesComercialesBean)container.preInvoke(this, 7, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerPromocionesBOPrincipal();
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 7, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerPromocionesPrincipalBP
	 */
	public java.util.List obtenerPromocionesPrincipalBP(java.lang.String tipoS) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = tipoS;
			}
	cl.cencosud.ventaseguros.ejb.InformesComercialesBean beanRef = (cl.cencosud.ventaseguros.ejb.InformesComercialesBean)container.preInvoke(this, 8, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerPromocionesPrincipalBP(tipoS);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 8, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerVitrineoGeneral
	 */
	public java.util.List obtenerVitrineoGeneral(java.lang.String fechaDesde, java.lang.String fechaHasta, boolean esPaginado, int numeroPagina, int rama, int subcategoria) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[6];
				_jacc_parms[0] = fechaDesde;
				_jacc_parms[1] = fechaHasta;
				_jacc_parms[2] = new java.lang.Boolean(esPaginado);
				_jacc_parms[3] = new java.lang.Integer(numeroPagina);
				_jacc_parms[4] = new java.lang.Integer(rama);
				_jacc_parms[5] = new java.lang.Integer(subcategoria);
			}
	cl.cencosud.ventaseguros.ejb.InformesComercialesBean beanRef = (cl.cencosud.ventaseguros.ejb.InformesComercialesBean)container.preInvoke(this, 9, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerVitrineoGeneral(fechaDesde, fechaHasta, esPaginado, numeroPagina, rama, subcategoria);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 9, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerArchivoFactura
	 */
	public java.util.Map obtenerArchivoFactura(long id_factura) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.Map _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = new java.lang.Long(id_factura);
			}
	cl.cencosud.ventaseguros.ejb.InformesComercialesBean beanRef = (cl.cencosud.ventaseguros.ejb.InformesComercialesBean)container.preInvoke(this, 10, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerArchivoFactura(id_factura);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 10, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
}
