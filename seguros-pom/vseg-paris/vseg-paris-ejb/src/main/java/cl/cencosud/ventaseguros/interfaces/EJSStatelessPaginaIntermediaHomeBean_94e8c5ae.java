package cl.cencosud.ventaseguros.interfaces;

import com.ibm.ejs.container.*;

/**
 * EJSStatelessPaginaIntermediaHomeBean_94e8c5ae
 */
public class EJSStatelessPaginaIntermediaHomeBean_94e8c5ae extends EJSHome {
	static final long serialVersionUID = 61;
	/**
	 * EJSStatelessPaginaIntermediaHomeBean_94e8c5ae
	 */
	public EJSStatelessPaginaIntermediaHomeBean_94e8c5ae() throws java.rmi.RemoteException {
		super();	}
	/**
	 * create
	 */
	public cl.cencosud.ventaseguros.interfaces.PaginaIntermedia create() throws javax.ejb.CreateException, java.rmi.RemoteException {
BeanO beanO = null;
cl.cencosud.ventaseguros.interfaces.PaginaIntermedia result = null;
boolean createFailed = false;
try {
	result = (cl.cencosud.ventaseguros.interfaces.PaginaIntermedia) super.createWrapper(null);
}
catch (javax.ejb.CreateException ex) {
	createFailed = true;
	throw ex;
} catch (java.rmi.RemoteException ex) {
	createFailed = true;
	throw ex;
} catch (Throwable ex) {
	createFailed = true;
	throw new CreateFailureException(ex);
} finally {
	if (createFailed) {
		super.createFailure(beanO);
	}
}
return result;	}
}
