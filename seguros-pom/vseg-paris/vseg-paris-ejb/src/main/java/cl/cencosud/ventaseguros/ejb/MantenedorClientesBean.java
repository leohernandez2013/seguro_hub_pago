package cl.cencosud.ventaseguros.ejb;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.cencosud.ventaseguros.asesorvirtual.model.SolicitudCotizacion;
import cl.cencosud.ventaseguros.common.EstadoCivil;
import cl.cencosud.ventaseguros.common.Parametro;
import cl.cencosud.ventaseguros.common.exception.ValidacionUsuarioException;
import cl.cencosud.ventaseguros.common.model.Solicitud;
import cl.cencosud.ventaseguros.dao.VSPDAOFactory;
import cl.cencosud.ventaseguros.mantenedorclientes.dao.MantenedorClientesDAO;
import cl.cencosud.ventaseguros.mantenedorclientes.dao.MantenedorClientesWSDAO;
import cl.tinet.common.mail.Mail;
import cl.tinet.common.model.exception.BusinessException;
import cl.tinet.common.seguridad.model.UsuarioExterno;



/**
 * @author Roberto San Martín (TInet Soluciones Informáticas).
 * @version 1.0
 * @created 26-Jul-2010 15:32:03
 * 
 * 
 * @ejb.bean name="MantenedorClientes" display-name="MantenedorClientes"
 *           description="Servicios relacionado con el mantenedor de clientes"
 *           jndi-name="ejb/ventaseguros/MantenedorClientes" type="Stateless"
 *           view-type="remote"
 * 
 * @ejb.resource-ref res-ref-name="jdbc/seguridadDS"
 *                   res-type="javax.sql.DataSource"
 *                   res-sharing-scope="Shareable" res-auth="Container"
 *                   jndi-name="jdbc/seguridadDS"
 *                   
 * @ejb.resource-ref res-ref-name="jdbc/VSPDS"
 *                   res-type="javax.sql.DataSource"
 *                   res-sharing-scope="Shareable" res-auth="Container"
 *                   jndi-name="jdbc/VSPDS"
 *
 * @ejb.transaction type="Supports"
 * 
 * @ejb.ejb-service-ref name="service/WsBigsaPolizaPDFService"
 *           interface="cl.cencosud.bigsa.poliza.consultapdf.webservice.client.was.WsBigsaPolizaPDFService"
 *           jaxrpc-mapping-file="META-INF/WsBigsaPolizaPDFPort_mapping.xml"
 * 
 * @ejb.ejb-service-ref name="service/WsBigsaPolizaHtml"
 *           interface="cl.cencosud.bigsa.poliza.consultahtml.webservice.client.was.WsBigsaPolizaHtml"
 *           jaxrpc-mapping-file="META-INF/WsBigsaPolizaHtmlImplPort_mapping.xml"
 * 
 * @ejb.ejb-service-ref name="service/IComercialService"
 *           interface="cl.cencosud.equifax.infocomercial.webservice.client.was.services.IComercialService"
 *           jaxrpc-mapping-file="META-INF/IComercial_mapping.xml"
 *           
 * @ejb.ejb-service-ref name="service/WsBigsaActulizaFormaPagoService"
 *           interface="cl.cencosud.bigsa.gestion.formapago.grabar.webservice.client.was.WsBigsaActulizaFormaPagoService"
 *           jaxrpc-mapping-file="META-INF/WsBigsaActulizaFormaPagoService_mapping.xml"
 *           
 * @ejb.ejb-service-ref name="service/WsBigsaSolicitudes"
 *           interface="cl.cencosud.bigsa.gestion.solicitud.webservice.client.was.WsBigsaSolicitudes"
 *           jaxrpc-mapping-file="META-INF/WsBigsaSolicitudesImplPort_mapping.xml"
 */
public class MantenedorClientesBean implements SessionBean {

    /**
     * Logger de la clase.
     */
    private static final Log logger =
        LogFactory.getLog(MantenedorClientesBean.class);

    /**
     * Identificador de la clase para serialización.
     */
    private static final long serialVersionUID = 5188894893963304437L;

    /**
     * Metodo create.
     */
    public void ejbCreate() {
        // Create.
    }

    public void ejbActivate() throws EJBException, RemoteException {
        // TODO Auto-generated method stub

    }

    public void ejbPassivate() throws EJBException, RemoteException {
        // TODO Auto-generated method stub

    }

    public void ejbRemove() throws EJBException, RemoteException {
        // TODO Auto-generated method stub

    }

    public void setSessionContext(SessionContext arg0) throws EJBException,
        RemoteException {
        // TODO Auto-generated method stub

    }

    /**
     * Obtiene Parametros segrun grupo.
     * @ejb.interface-method "remote"
     */
    public List < Map < String, ? >> obtenerParametro(String idGrupo) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorClientesDAO dao =
            factory.getVentaSegurosDAO(MantenedorClientesDAO.class);
        try {
            List < Map < String, ? >> result = dao.obtenerParametro(idGrupo);
            return result;
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene Parametros segrun grupo.
     * @ejb.interface-method "remote"
     */
    public List < Map < String, ? >> obtenerParametro(String idGrupo,
        String valorParametro) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorClientesDAO dao =
            factory.getVentaSegurosDAO(MantenedorClientesDAO.class);
        try {
            List < Map < String, ? >> result =
                dao.obtenerParametro(idGrupo, valorParametro);
            return result;
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene listado de Estados Civiles disponibles.
     * @ejb.interface-method "remote"
     */
    public EstadoCivil[] obtenerEstadosCiviles() {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorClientesDAO dao =
            factory.getVentaSegurosDAO(MantenedorClientesDAO.class);
        try {
            return dao.obtenerEstadosCiviles();
        } finally {
            dao.close();
        }

    }

    /**
     * Obtiene listdo de Regiones dispobiles.
     * @ejb.interface-method "remote"
     */
    public Parametro[] obtenerRegiones() {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorClientesDAO dao =
            factory.getVentaSegurosDAO(MantenedorClientesDAO.class);
        try {
            return dao.obtenerRegiones();
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene listdo de Comunas dispobiles.
     * @ejb.interface-method "remote"
     */
    public Parametro[] obtenerComunas(String idRegion) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorClientesDAO dao =
            factory.getVentaSegurosDAO(MantenedorClientesDAO.class);
        try {
            return dao.obtenerComunas(idRegion);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene listdo de Comunas dispobiles.
     * @ejb.interface-method "remote"
     */
    public Parametro[] obtenerCiudades(String idComuna) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorClientesDAO dao =
            factory.getVentaSegurosDAO(MantenedorClientesDAO.class);
        try {
            return dao.obtenerCiudades(idComuna);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene la comuna a la que pertenece una ciudad.
     * @ejb.interface-method "remote"
     */
    public Parametro[] obtenerComunaPorCiudad(String idCiudad) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorClientesDAO dao =
            factory.getVentaSegurosDAO(MantenedorClientesDAO.class);
        try {
            return dao.obtenerComunaPorCiudad(idCiudad);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene la region a la que pertenece una comuna.
     * @ejb.interface-method "remote"
     */
    public Parametro[] obtenerRegionPorComuna(String idComuna) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorClientesDAO dao =
            factory.getVentaSegurosDAO(MantenedorClientesDAO.class);
        try {
            return dao.obtenerRegionPorComuna(idComuna);
        } finally {
            dao.close();
        }
    }

    /**
     * Actualiza los datos de un cliente.
     * @ejb.interface-method "remote"
     */
    public void actualizarCliente(Map < String, String > datos) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorClientesDAO dao =
            factory.getVentaSegurosDAO(MantenedorClientesDAO.class);
        try {
            dao.actualizarCliente(datos);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtener solicitudes de un usuario.
     * @param rutUsuario rut del usuario
     * @return listado de solicitudes.
     * @ejb.interface-method "remote"
     */
    public List < Solicitud > obtenerSolicitudes(Long rutUsuario) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorClientesDAO dao =
            factory.getVentaSegurosDAO(MantenedorClientesDAO.class);
        try {
            return dao.obtenerSolicitudes(rutUsuario);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtener solicitudes de un usuario en BIGSA.
     * @param rutCliente rut del usuario
     * @return listado de solicitudes.
     * @ejb.interface-method "remote"
     */
    public SolicitudCotizacion[] obtenerSolicitudesWS(Long rutCliente)
        throws BusinessException {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        //        MantenedorClientesDAO dao =
        //            factory.getVentaSegurosDAO(MantenedorClientesDAO.class);
        MantenedorClientesWSDAO dao =
            factory.getVentaSegurosDAO(MantenedorClientesWSDAO.class);
        try {
            //            return dao.obtenerSolicitudesWS(rutCliente);
            return dao.obtenerSolicitudes(rutCliente);
        } finally {
            dao.close();
        }

    }

    /**
     * Obtener la poliza en formato pdf.
     * @param rutCliente rut del usuario
     * @param numeroSolicitud numero de solicitud en bigsa.
     * @return PDF.
     * @ejb.interface-method "remote"
     */
    public byte[] obtenerPolizaPdf(int rutCliente, int numeroSolicitud)
        throws BusinessException {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorClientesWSDAO dao =
            factory.getVentaSegurosDAO(MantenedorClientesWSDAO.class);
        try {
            return dao.obtenerPolizaPdf(rutCliente, numeroSolicitud);
        } finally {
            dao.close();
        }
    }

    /**
     * TODO Describir método obtenerPolizaHtml.
     * @param rutCliente
     * @param numeroSolicitud
     * @return
     * @ejb.interface-method "remote"
     */
    public byte[] obtenerPolizaHtml(int rutCliente, int numeroSolicitud)
        throws BusinessException {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorClientesWSDAO dao =
            factory.getVentaSegurosDAO(MantenedorClientesWSDAO.class);
        try {
            return dao.obtenerPolizaHtml(rutCliente, numeroSolicitud);
        } finally {
            dao.close();
        }
    }

    /**
     * Inserta un cliente que no se encuentra registrado.
     * @throws ValidacionUsuarioException 
     * 
     * @ejb.interface-method "remote"
     */
    public void registrarCliente(UsuarioExterno usuario, String clave)
        throws ValidacionUsuarioException {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorClientesDAO dao = factory.getVentaSegurosDAO(MantenedorClientesDAO.class);

        MantenedorClientesWSDAO wsDao = factory.getVentaSegurosDAO(MantenedorClientesWSDAO.class);
        String rutCliente = usuario.getRut_cliente() + usuario.getDv_cliente();
      
        if (!wsDao.validarSerieRut(rutCliente, usuario.getNumero_serie())) {
            logger.info("Nro de serie de la cedula de identidad no valido");
            throw new ValidacionUsuarioException(ValidacionUsuarioException.NRO_SERIE_NO_VALIDO, new Object[] {});
        }

        UsuarioExterno user = dao.getCliente(usuario.getRut_cliente(), usuario.getDv_cliente());

        if (user == null) {
            Long idUsuario = dao.registrarUsuario(usuario);

            if (idUsuario > 0) {
                usuario.setIdUsuario(idUsuario.intValue());
                dao.registrarUsuarioExterno(usuario);
                dao.registrarClaveUsuarioExterno(idUsuario, clave);
                dao.actualizarRolUsuario(idUsuario);
                this.enviarEmail(usuario, clave);
            }
        } else {
            logger.info("Usuario ya se encuentra registrado");
            throw new ValidacionUsuarioException(ValidacionUsuarioException.USUARIO_EXISTE,
                new Object[] { usuario.getRut_cliente() + "-"+ usuario.getDv_cliente() });
        }
    }

    /**
     * TODO Describir método obtenerNroOrdenCompra.
     * @param idSolicitud
     * @return
     * @throws BusinessException
     * @ejb.interface-method "remote"
     */
    public int obtenerNroOrdenCompra(String idSolicitud)
        throws BusinessException {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorClientesDAO dao =
            factory.getVentaSegurosDAO(MantenedorClientesDAO.class);
        try {
            return dao.obtenerNroOrdenCompra(idSolicitud);
        } finally {
            dao.close();
        }
    }

    /**
     * TODO Describir método enviarEmail.
     * 
     * @param datosSeguro
     */
    private void enviarEmail(UsuarioExterno usuario, String clave) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorClientesDAO dao =
            factory.getVentaSegurosDAO(MantenedorClientesDAO.class);

        try {

            String mensaje = "";
            String email = usuario.getEmail();
            String nombre = usuario.getNombre();
            String server = dao.obtenerDatosEmail("SERVER_CORREO");
            String from = dao.obtenerDatosEmail("MAIL_FROM");
            String asunto = dao.obtenerDatosEmail("MAIL_SUBJECT_CREAR_CUENTA");

            // Enviar mail.
            String cuerpoEmail = "HTML_CREAR_CUENTA";

            Mail mail = new Mail(from, email, asunto, "", server);

            try {
                logger.debug(cuerpoEmail);
                InputStream is = dao.obtenerCuerpoEmail(cuerpoEmail);

                String archivo = mail.convertStreamToString(is);
                logger.debug(archivo);

                // Reemplazar nombre y clave.
                mensaje = archivo.replace("{0}", nombre);
                mail.addContenido(mensaje);

                logger.info("enviar email");
                mail.sendMail();
            } catch (Exception e) {
                logger.error("Excepcion general al generar email", e);
            }
        } catch (Exception e) {
            logger.error("Excepcion general al enviar correo", e);
        } finally {
            dao.close();
        }
    }

    /**
     * TODO Describir método actualizarFormaPago.
     * @param rutCliente
     * @param numeroCotizacion
     * @param codigoFormaPago
     * @return
     * @throws BusinessException
     * @ejb.interface-method "remote"
     */
    public boolean actualizarFormaPago(int rutCliente, int numeroCotizacion,
        int codigoFormaPago) throws BusinessException {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorClientesWSDAO dao =
            factory.getVentaSegurosDAO(MantenedorClientesWSDAO.class);
        try {
            return dao.actualizarFormaPago(rutCliente, numeroCotizacion,
                codigoFormaPago);
        } finally {
            dao.close();
        }
    }

    /**
     * TODO Describir método obtenerCoberturas.
     * @param idPlan
     * @return
     * @throws BusinessException
     * @ejb.interface-method "remote"
     */
    public List < Map < String, Object > > obtenerCoberturas(int idPlan)
        throws BusinessException {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorClientesDAO dao =
            factory.getVentaSegurosDAO(MantenedorClientesDAO.class);
        try {
            return dao.obtenerCoberturas(idPlan);
        } finally {
            dao.close();
        }
    }

    /**
     * TODO Describir método enviarEmail.
     * 
     * @param datosSeguro
     * @ejb.interface-method "remote"
     */
    public void enviarEmailFormularioContacto(UsuarioExterno usuario,
        String tipoContacto, String mensajeContacto) {

        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorClientesDAO dao =
            factory.getVentaSegurosDAO(MantenedorClientesDAO.class);

        try {

            String mensaje = "";
            String email = dao.obtenerDatosEmail("MAIL_CONTACTO_DEST");
            String server = dao.obtenerDatosEmail("SERVER_CORREO");
            String from = usuario.getEmail();
            String asunto = dao.obtenerDatosEmail("MAIL_SUBJECT_CONTACTO");

            // Enviar mail.
            String cuerpoEmail = "HTML_FORMULARIO_CONTACTO";

            Mail mail = new Mail(from, email, asunto, "", server);

            try {
                logger.debug(cuerpoEmail);
                InputStream is = dao.obtenerCuerpoEmail(cuerpoEmail);

                String archivo = mail.convertStreamToString(is);
                logger.debug(archivo);

                mensaje =
                    archivo.replace("{rut_cliente}", usuario.getRut_cliente()
                        + "-" + usuario.getDv_cliente());
                mensaje =
                    mensaje.replace("{nombre_cliente}", usuario
                        .getNombre());
                mensaje =
                    mensaje.replace("{apellido_paterno}", usuario
                        .getApellido_paterno());
                mensaje =
                    mensaje.replace("{apellido_materno}", usuario
                        .getApellido_materno());
                mensaje =
                    mensaje.replace("{tipo_telefono1}", usuario
                        .getTipo_telefono_1());
                mensaje =
                    mensaje.replace("{telefono1}", usuario.getTelefono_1());
                mensaje =
                    mensaje.replace("{tipo_telefono2}", usuario
                        .getTipo_telefono_2());
                mensaje =
                    mensaje.replace("{telefono2}", usuario.getTelefono_2());
                mensaje = mensaje.replace("{email}", usuario.getEmail());
                mensaje = mensaje.replace("{tipo_contacto}", tipoContacto);
                mensaje = mensaje.replace("{comentario}", mensajeContacto);

                mail.addContenido(mensaje);

                logger.info("enviar email contacto");
                mail.sendMail();
            } catch (Exception e) {
                logger.error("Excepcion general al generar email contacto", e);
            }
        } catch (Exception e) {
            logger.error("Excepcion general al enviar correo contacto", e);
        } finally {
            dao.close();
        }
    }
    
    /**
     * Obtiene el nombre de la subcategoria.
     * @ejb.interface-method "remote"
     */
    public String obtenerSubcategoria(String idSubcategoria) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorClientesDAO dao = factory.getVentaSegurosDAO(MantenedorClientesDAO.class);
        try {
            return dao.obtenerSubcategoria(idSubcategoria);
        } finally {
            dao.close();
        }
    }
    
    /**
     * Guarda datos del formulario de cotizacion.
     * @ejb.interface-method "remote"
     */
    public void registrarCotizacion(String rut, String nombre, String telefono, String descSubcategoria) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorClientesDAO dao =
            factory.getVentaSegurosDAO(MantenedorClientesDAO.class);
        try {
            dao.registrarCotizacion(rut, nombre, telefono, descSubcategoria);
            this.enviarEmailCotizador(rut, nombre, telefono, descSubcategoria);
        } finally {
            dao.close();
        }
    }
    
    /**
     * Envia mail con registros ingresados en el formulario del cotizador.
     * @ejb.interface-method "remote"
     */
    public void enviarEmailCotizador(String rut, String nombre, String telefono, String descSubcategoria) {

            VSPDAOFactory factory = VSPDAOFactory.getInstance();
            MantenedorClientesDAO dao = factory.getVentaSegurosDAO(MantenedorClientesDAO.class);

            try {

                String mensaje = "";
                String email = dao.obtenerDatosEmail("MAIL_COTIZADOR_DEST");
                String server = dao.obtenerDatosEmail("SERVER_CORREO");
                String from = dao.obtenerDatosEmail("MAIL_COTIZADOR_FROM");
                String asunto = dao.obtenerDatosEmail("MAIL_SUBJECT_COTIZADOR"); 
                String cuerpoEmail = "HTML_FORMULARIO_COTIZADOR";

                Mail mail = new Mail(from, email, asunto, "", server);
                
                Date fechaActual = new Date();
                SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
                String fecha = formato.format(fechaActual);

                try {
                    InputStream is = dao.obtenerCuerpoEmail(cuerpoEmail);

                    String archivo = mail.convertStreamToString(is);

                    mensaje = archivo.replace("{rut_cliente}",rut);
                    mensaje = mensaje.replace("{nombre_cliente}", nombre);
                    mensaje = mensaje.replace("{telefono}", telefono);
                    mensaje = mensaje.replace("{seguro}", descSubcategoria);
                    mensaje = mensaje.replace("{fecha}", fecha);
                    
                    mail.addContenido(mensaje);

                    logger.info("Enviar email formulario cotizacion");
                    mail.sendMail();
                    
                } catch (Exception e) {
                    logger.error("Excepcion general al generar email formulario cotizacion", e);
                }
            } catch (Exception e) {
                logger.error("Excepcion general al enviar email formulario cotizacion", e);
            } finally {
                dao.close();
            }
        }
    // metodo que obtiene secuencia para denuncias
    /**
     * Genera secuencia para formulario de denuncias.
     * @ejb.interface-method "remote"
     */
    
    public String generarSecuenciaDenuncia(){ 	
    	 VSPDAOFactory factory = VSPDAOFactory.getInstance();
         MantenedorClientesDAO dao = factory.getVentaSegurosDAO(MantenedorClientesDAO.class);

         try {
             return dao.generarSecuenciaDenuncias();
         } finally {
             dao.close();
         }
    }
    
    /**
     * Envia mail denuncias.
     * @ejb.interface-method "remote"
     */
       
    public void enviarEmailDenuncias(String ruta,String folio,String rut,String nombre,String apellidoPat,String apellidoMat,String direccion,String comuna,String ciudad,String fonoFijo,String fonoCelular,String detalle) {

        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorClientesDAO dao = factory.getVentaSegurosDAO(MantenedorClientesDAO.class);

        try {

            String mensaje = "";
            String email = dao.obtenerDatosEmail("MAIL_DENUNCIA_DEST");
            String server = dao.obtenerDatosEmail("SERVER_CORREO");
            String cuentaEmailDestino = dao.obtenerDatosEmail("MAIL_DENUNCIA_FROM");
            String asunto = dao.obtenerDatosEmail("MAIL_SUBJECT_DENUNCIA"); 
            String cuerpoEmail = dao.obtenerDatosEmail("HTML_FORMULARIO_DENUNCIA"); 
            String from= null;
            String [] copia = null;
            
            String rutaArchivo = ruta + cuerpoEmail ;
            
            System.out.println("RUTA DENUNCIAS EJB  :  " + ruta);
            
           // URL url = new URL( rutaArchivo );
		   
            if(cuentaEmailDestino != null ){
        	    
         	   copia = cuentaEmailDestino.split(";");
         	    
         	   for (int i = 0; i < copia.length; i++) {
         			if (copia[i] != null){
         			    from = copia[i];

			            Mail mail = new Mail(from, email, asunto, "", server);
			            
			            Date fechaActual = new Date();
			            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
			            String fecha = formato.format(fechaActual);
			
			          
			               // InputStream is = dao.obtenerCuerpoEmail(cuerpoEmail);
			
			            	InputStreamReader input = new InputStreamReader(new FileInputStream(cuerpoEmail));
			           	    BufferedReader in = new BufferedReader(input);             
			                
			            	StringBuffer body = new StringBuffer();
			              
			               	    String fileLine = in.readLine();
			
			               	    while (fileLine != null) {
			               		
			
			               		if (fileLine.indexOf("$imagen") >= 0) {
			               		    fileLine = StringUtils.replace(fileLine, "$imagen",
			               		    		ruta + "images");
			               		}
			
			               		if (fileLine.indexOf("$folio") >= 0) {
			               		    fileLine = StringUtils.replace(fileLine, "$folio",
			               			 folio);
			               		}
			
			               		if (fileLine.indexOf("$rut") >= 0) {
			               		    fileLine = StringUtils.replace(fileLine, "$rut",
			               			 rut );
			               		}
			               		if (fileLine.indexOf("$nombre") >= 0) {
			               		    fileLine = StringUtils.replace(fileLine, "$nombre",
			               			nombre);
			               		}
			
			               		if (fileLine.indexOf("$apellidoPat") >= 0) {
			               		    fileLine = StringUtils.replace(fileLine, "$apellidoPat",
			               		    apellidoPat);
			               		}
			               		if (fileLine.indexOf("$apellidoMat") >= 0) {
			               		    fileLine = StringUtils.replace(fileLine, "$apellidoMat", apellidoMat);
			               		}
			
			               		if (fileLine.indexOf("$direccion") >= 0) {
			               		    fileLine = StringUtils.replace(fileLine, "$direccion",
			               		    		direccion);
			               		}
			               		if (fileLine.indexOf("$comuna") >= 0) {
			               		    fileLine = StringUtils.replace(fileLine, "$comuna",
			               		    		comuna);
			               		}
			               		if (fileLine.indexOf("$ciudad") >= 0) {
			               		    fileLine = StringUtils.replace(fileLine, "$ciudad",
			               		    		ciudad);
			               		}
			               		if (fileLine.indexOf("$fonoFijo") >= 0) {
			               		    fileLine = StringUtils.replace(fileLine, "$fonoFijo",
			               		    		fonoFijo);
			               		}
			               		if (fileLine.indexOf("$fonoCelular") >= 0) {
			               		    fileLine = StringUtils.replace(fileLine, "$fonoCelular",
			               		    		fonoCelular);
			               		}
			               		if (fileLine.indexOf("$detalle") >= 0) {
			               		    fileLine = StringUtils.replace(fileLine, "$detalle",
			               		    		detalle);
			               		}
			
			               		body.append(fileLine);
			               		fileLine = in.readLine();
			               	    }
			                
			                
			               /* mensaje = archivo.replace("$folio",folio);
			                mensaje = archivo.replace("$rut",rut);
			                mensaje = archivo.replace("$nombre",nombre);
			                mensaje = archivo.replace("$apellidoPat",apellidoPat);
			                mensaje = archivo.replace("$apellidoMat",apellidoMat);
			                mensaje = archivo.replace("$direccion",direccion);
			                mensaje = archivo.replace("$comuna",comuna);
			                mensaje = mensaje.replace("$ciudad", ciudad);
			                mensaje = mensaje.replace("$fonoFijo", fonoFijo);
			                mensaje = mensaje.replace("$fonoCelular", fonoCelular);
			                mensaje = mensaje.replace("$detalle", detalle);*/
			                
			               	mensaje = body.toString();
			               	    
			                mail.addContenido(mensaje);
			
			                logger.info("Enviar email formulario contacto denuncias");
			                mail.sendMail();
			            }
         			}
            }
                
         
        } catch (Exception e) {
            logger.error("Excepcion general al enviar email formulario cotizacion", e);
        } finally {
            dao.close();
        }
    }
    
    
}
