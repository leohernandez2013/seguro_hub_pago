package cl.cencosud.ventaseguros.interfaces;

import com.ibm.ejs.container.*;

/**
 * EJSStatelessMantenedorClientesHomeBean_06b9b79b
 */
public class EJSStatelessMantenedorClientesHomeBean_06b9b79b extends EJSHome {
	static final long serialVersionUID = 61;
	/**
	 * EJSStatelessMantenedorClientesHomeBean_06b9b79b
	 */
	public EJSStatelessMantenedorClientesHomeBean_06b9b79b() throws java.rmi.RemoteException {
		super();	}
	/**
	 * create
	 */
	public cl.cencosud.ventaseguros.interfaces.MantenedorClientes create() throws javax.ejb.CreateException, java.rmi.RemoteException {
BeanO beanO = null;
cl.cencosud.ventaseguros.interfaces.MantenedorClientes result = null;
boolean createFailed = false;
try {
	result = (cl.cencosud.ventaseguros.interfaces.MantenedorClientes) super.createWrapper(null);
}
catch (javax.ejb.CreateException ex) {
	createFailed = true;
	throw ex;
} catch (java.rmi.RemoteException ex) {
	createFailed = true;
	throw ex;
} catch (Throwable ex) {
	createFailed = true;
	throw new CreateFailureException(ex);
} finally {
	if (createFailed) {
		super.createFailure(beanO);
	}
}
return result;	}
}
