// Tie class generated by rmic, do not edit.
// Contents subject to change without notice.

package cl.cencosud.ventaseguros.interfaces;

import cl.cencosud.ventaseguros.asesorvirtual.model.Alternativa;
import cl.cencosud.ventaseguros.asesorvirtual.model.Pregunta;
import cl.cencosud.ventaseguros.asesorvirtual.model.Resultado;
import cl.cencosud.ventaseguros.asesorvirtual.model.Sitio;
import cl.tinet.common.model.exception.BusinessException;
import java.io.Serializable;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.rmi.Remote;
import java.util.List;
import javax.ejb.EJBHome;
import javax.ejb.EJBObject;
import javax.ejb.Handle;
import javax.ejb.RemoveException;
import javax.rmi.CORBA.Tie;
import javax.rmi.CORBA.Util;
import org.omg.CORBA.BAD_OPERATION;
import org.omg.CORBA.ORB;
import org.omg.CORBA.SystemException;
import org.omg.CORBA.portable.Delegate;
import org.omg.CORBA.portable.InputStream;
import org.omg.CORBA.portable.OutputStream;
import org.omg.CORBA.portable.ResponseHandler;
import org.omg.CORBA.portable.UnknownException;

public class _EJSRemoteStatelessAsesorVirtual_2ae6f2b4_Tie extends org.omg.CORBA_2_3.portable.ObjectImpl implements Tie {
    
    private EJSRemoteStatelessAsesorVirtual_2ae6f2b4 target = null;
    private ORB orb = null;
    
    private static final String[] _type_ids = {
        "RMI:cl.cencosud.ventaseguros.interfaces.AsesorVirtual:0000000000000000", 
        "RMI:javax.ejb.EJBObject:0000000000000000", 
        "RMI:com.ibm.websphere.csi.CSIServant:0000000000000000", 
        "RMI:com.ibm.websphere.csi.TransactionalObject:0000000000000000"
    };
    
    public void setTarget(Remote target) {
        this.target = (EJSRemoteStatelessAsesorVirtual_2ae6f2b4) target;
    }
    
    public Remote getTarget() {
        return target;
    }
    
    public org.omg.CORBA.Object thisObject() {
        return this;
    }
    
    public void deactivate() {
        if (orb != null) {
            orb.disconnect(this);
            _set_delegate(null);
        }
    }
    
    public ORB orb() {
        return _orb();
    }
    
    public void orb(ORB orb) {
        orb.connect(this);
    }
    
    public void _set_delegate(Delegate del) {
        super._set_delegate(del);
        if (del != null)
            orb = _orb();
        else
            orb = null;
    }
    
    public String[] _ids() { 
        return _type_ids;
    }
    
    public OutputStream _invoke(String method, InputStream _in, ResponseHandler reply) throws SystemException {
        try {
            org.omg.CORBA_2_3.portable.InputStream in = 
                (org.omg.CORBA_2_3.portable.InputStream) _in;
            switch (method.charAt(0)) {
                case 95: 
                    if (method.equals("_get_primaryKey")) {
                        return _get_primaryKey(in, reply);
                    } else if (method.equals("_get_handle")) {
                        return _get_handle(in, reply);
                    } else if (method.equals("_get_EJBHome")) {
                        return _get_EJBHome(in, reply);
                    }
                case 97: 
                    if (method.equals("actualizarAlternativa")) {
                        return actualizarAlternativa(in, reply);
                    } else if (method.equals("actualizarProximaPregunta")) {
                        return actualizarProximaPregunta(in, reply);
                    }
                case 98: 
                    if (method.equals("buscarArbol")) {
                        return buscarArbol(in, reply);
                    }
                case 99: 
                    if (method.equals("cargarArbol")) {
                        return cargarArbol(in, reply);
                    }
                case 103: 
                    if (method.equals("grabarArbol")) {
                        return grabarArbol(in, reply);
                    } else if (method.equals("grabarAlternativa")) {
                        return grabarAlternativa(in, reply);
                    } else if (method.equals("grabarPregunta")) {
                        return grabarPregunta(in, reply);
                    }
                case 105: 
                    if (method.equals("isIdentical")) {
                        return isIdentical(in, reply);
                    }
                case 114: 
                    if (method.equals("remove")) {
                        return remove(in, reply);
                    } else if (method.equals("resultadoGrabar")) {
                        return resultadoGrabar(in, reply);
                    }
                case 115: 
                    if (method.equals("sincronizarXML")) {
                        return sincronizarXML(in, reply);
                    }
            }
            throw new BAD_OPERATION();
        } catch (SystemException ex) {
            throw ex;
        } catch (Throwable ex) {
            throw new UnknownException(ex);
        }
    }
    
    private OutputStream _get_EJBHome(org.omg.CORBA_2_3.portable.InputStream in , ResponseHandler reply) throws Throwable {
        EJBHome result = target.getEJBHome();
        OutputStream out = reply.createReply();
        Util.writeRemoteObject(out,result);
        return out;
    }
    
    private OutputStream _get_primaryKey(org.omg.CORBA_2_3.portable.InputStream in , ResponseHandler reply) throws Throwable {
        Object result = target.getPrimaryKey();
        OutputStream out = reply.createReply();
        Util.writeAny(out,result);
        return out;
    }
    
    private OutputStream remove(org.omg.CORBA_2_3.portable.InputStream in , ResponseHandler reply) throws Throwable {
        try {
            target.remove();
        } catch (RemoveException ex) {
            String id = "IDL:javax/ejb/RemoveEx:1.0";
            org.omg.CORBA_2_3.portable.OutputStream out = 
                (org.omg.CORBA_2_3.portable.OutputStream) reply.createExceptionReply();
            out.write_string(id);
            out.write_value(ex,RemoveException.class);
            return out;
        }
        OutputStream out = reply.createReply();
        return out;
    }
    
    private OutputStream _get_handle(org.omg.CORBA_2_3.portable.InputStream in , ResponseHandler reply) throws Throwable {
        Handle result = target.getHandle();
        OutputStream out = reply.createReply();
        Util.writeAbstractObject(out,result);
        return out;
    }
    
    private OutputStream isIdentical(org.omg.CORBA_2_3.portable.InputStream in , ResponseHandler reply) throws Throwable {
        EJBObject arg0 = (EJBObject) in.read_Object(EJBObject.class);
        boolean result = target.isIdentical(arg0);
        OutputStream out = reply.createReply();
        out.write_boolean(result);
        return out;
    }
    
    private OutputStream grabarArbol(org.omg.CORBA_2_3.portable.InputStream in , ResponseHandler reply) throws Throwable {
        Sitio arg0 = (Sitio) in.read_value(Sitio.class);
        String arg1 = (String) in.read_value(String.class);
        int arg2 = in.read_long();
        try {
            target.grabarArbol(arg0, arg1, arg2);
        } catch (BusinessException ex) {
            String id = "IDL:cl/tinet/common/model/_exception/BusinessEx:1.0";
            org.omg.CORBA_2_3.portable.OutputStream out = 
                (org.omg.CORBA_2_3.portable.OutputStream) reply.createExceptionReply();
            out.write_string(id);
            out.write_value(ex,BusinessException.class);
            return out;
        }
        OutputStream out = reply.createReply();
        return out;
    }
    
    private OutputStream cargarArbol(org.omg.CORBA_2_3.portable.InputStream in , ResponseHandler reply) throws Throwable {
        int arg0 = in.read_long();
        Sitio result = target.cargarArbol(arg0);
        org.omg.CORBA_2_3.portable.OutputStream out = 
            (org.omg.CORBA_2_3.portable.OutputStream) reply.createReply();
        out.write_value(result,Sitio.class);
        return out;
    }
    
    private OutputStream buscarArbol(org.omg.CORBA_2_3.portable.InputStream in , ResponseHandler reply) throws Throwable {
        int arg0 = in.read_long();
        List result = target.buscarArbol(arg0);
        org.omg.CORBA_2_3.portable.OutputStream out = 
            (org.omg.CORBA_2_3.portable.OutputStream) reply.createReply();
        out.write_value((Serializable)result,List.class);
        return out;
    }
    
    private OutputStream resultadoGrabar(org.omg.CORBA_2_3.portable.InputStream in , ResponseHandler reply) throws Throwable {
        Resultado arg0 = (Resultado) in.read_value(Resultado.class);
        int arg1 = in.read_long();
        String arg2 = (String) in.read_value(String.class);
        target.resultadoGrabar(arg0, arg1, arg2);
        OutputStream out = reply.createReply();
        return out;
    }
    
    private OutputStream actualizarAlternativa(org.omg.CORBA_2_3.portable.InputStream in , ResponseHandler reply) throws Throwable {
        int arg0 = in.read_long();
        int arg1 = in.read_long();
        int arg2 = in.read_long();
        int arg3 = in.read_long();
        String arg4 = (String) in.read_value(String.class);
        target.actualizarAlternativa(arg0, arg1, arg2, arg3, arg4);
        OutputStream out = reply.createReply();
        return out;
    }
    
    private OutputStream grabarAlternativa(org.omg.CORBA_2_3.portable.InputStream in , ResponseHandler reply) throws Throwable {
        Alternativa arg0 = (Alternativa) in.read_value(Alternativa.class);
        int arg1 = in.read_long();
        int arg2 = in.read_long();
        String arg3 = (String) in.read_value(String.class);
        target.grabarAlternativa(arg0, arg1, arg2, arg3);
        OutputStream out = reply.createReply();
        return out;
    }
    
    private OutputStream grabarPregunta(org.omg.CORBA_2_3.portable.InputStream in , ResponseHandler reply) throws Throwable {
        Pregunta arg0 = (Pregunta) in.read_value(Pregunta.class);
        int arg1 = in.read_long();
        String arg2 = (String) in.read_value(String.class);
        target.grabarPregunta(arg0, arg1, arg2);
        OutputStream out = reply.createReply();
        return out;
    }
    
    private OutputStream actualizarProximaPregunta(org.omg.CORBA_2_3.portable.InputStream in , ResponseHandler reply) throws Throwable {
        int arg0 = in.read_long();
        int arg1 = in.read_long();
        int arg2 = in.read_long();
        int arg3 = in.read_long();
        String arg4 = (String) in.read_value(String.class);
        target.actualizarProximaPregunta(arg0, arg1, arg2, arg3, arg4);
        OutputStream out = reply.createReply();
        return out;
    }
    
    private OutputStream sincronizarXML(org.omg.CORBA_2_3.portable.InputStream in , ResponseHandler reply) throws Throwable {
        String arg0 = (String) in.read_value(String.class);
        boolean result = target.sincronizarXML(arg0);
        OutputStream out = reply.createReply();
        out.write_boolean(result);
        return out;
    }
}
