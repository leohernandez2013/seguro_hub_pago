package cl.cencosud.ventaseguros.interfaces;

import com.ibm.ejs.container.*;

/**
 * EJSStatelessInformesComercialesHomeBean_90317f57
 */
public class EJSStatelessInformesComercialesHomeBean_90317f57 extends EJSHome {
	static final long serialVersionUID = 61;
	/**
	 * EJSStatelessInformesComercialesHomeBean_90317f57
	 */
	public EJSStatelessInformesComercialesHomeBean_90317f57() throws java.rmi.RemoteException {
		super();	}
	/**
	 * create
	 */
	public cl.cencosud.ventaseguros.interfaces.InformesComerciales create() throws javax.ejb.CreateException, java.rmi.RemoteException {
BeanO beanO = null;
cl.cencosud.ventaseguros.interfaces.InformesComerciales result = null;
boolean createFailed = false;
try {
	result = (cl.cencosud.ventaseguros.interfaces.InformesComerciales) super.createWrapper(null);
}
catch (javax.ejb.CreateException ex) {
	createFailed = true;
	throw ex;
} catch (java.rmi.RemoteException ex) {
	createFailed = true;
	throw ex;
} catch (Throwable ex) {
	createFailed = true;
	throw new CreateFailureException(ex);
} finally {
	if (createFailed) {
		super.createFailure(beanO);
	}
}
return result;	}
}
