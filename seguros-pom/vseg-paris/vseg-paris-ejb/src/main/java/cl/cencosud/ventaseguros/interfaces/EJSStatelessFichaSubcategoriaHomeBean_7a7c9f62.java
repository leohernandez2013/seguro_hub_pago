package cl.cencosud.ventaseguros.interfaces;

import com.ibm.ejs.container.*;

/**
 * EJSStatelessFichaSubcategoriaHomeBean_7a7c9f62
 */
public class EJSStatelessFichaSubcategoriaHomeBean_7a7c9f62 extends EJSHome {
	static final long serialVersionUID = 61;
	/**
	 * EJSStatelessFichaSubcategoriaHomeBean_7a7c9f62
	 */
	public EJSStatelessFichaSubcategoriaHomeBean_7a7c9f62() throws java.rmi.RemoteException {
		super();	}
	/**
	 * create
	 */
	public cl.cencosud.ventaseguros.interfaces.FichaSubcategoria create() throws javax.ejb.CreateException, java.rmi.RemoteException {
BeanO beanO = null;
cl.cencosud.ventaseguros.interfaces.FichaSubcategoria result = null;
boolean createFailed = false;
try {
	result = (cl.cencosud.ventaseguros.interfaces.FichaSubcategoria) super.createWrapper(null);
}
catch (javax.ejb.CreateException ex) {
	createFailed = true;
	throw ex;
} catch (java.rmi.RemoteException ex) {
	createFailed = true;
	throw ex;
} catch (Throwable ex) {
	createFailed = true;
	throw new CreateFailureException(ex);
} finally {
	if (createFailed) {
		super.createFailure(beanO);
	}
}
return result;	}
}
