package cl.cencosud.ventaseguros.interfaces;

import com.ibm.ejs.container.*;

/**
 * EJSStatelessComparadorHomeBean_4e99e107
 */
public class EJSStatelessComparadorHomeBean_4e99e107 extends EJSHome {
	static final long serialVersionUID = 61;
	/**
	 * EJSStatelessComparadorHomeBean_4e99e107
	 */
	public EJSStatelessComparadorHomeBean_4e99e107() throws java.rmi.RemoteException {
		super();	}
	/**
	 * create
	 */
	public cl.cencosud.ventaseguros.interfaces.Comparador create() throws javax.ejb.CreateException, java.rmi.RemoteException {
BeanO beanO = null;
cl.cencosud.ventaseguros.interfaces.Comparador result = null;
boolean createFailed = false;
try {
	result = (cl.cencosud.ventaseguros.interfaces.Comparador) super.createWrapper(null);
}
catch (javax.ejb.CreateException ex) {
	createFailed = true;
	throw ex;
} catch (java.rmi.RemoteException ex) {
	createFailed = true;
	throw ex;
} catch (Throwable ex) {
	createFailed = true;
	throw new CreateFailureException(ex);
} finally {
	if (createFailed) {
		super.createFailure(beanO);
	}
}
return result;	}
}
