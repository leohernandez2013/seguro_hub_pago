package cl.cencosud.ventaseguros.interfaces;

import java.rmi.RemoteException;

import com.ibm.ejs.container.*;

/**
 * EJSRemoteStatelessMantenedorClientes_06b9b79b
 */
public class EJSRemoteStatelessMantenedorClientes_06b9b79b extends EJSWrapper implements MantenedorClientes {
	/**
	 * EJSRemoteStatelessMantenedorClientes_06b9b79b
	 */
	public EJSRemoteStatelessMantenedorClientes_06b9b79b() throws java.rmi.RemoteException {
		super();	}
	/**
	 * actualizarFormaPago
	 */
	public boolean actualizarFormaPago(int rutCliente, int numeroCotizacion, int codigoFormaPago) throws cl.tinet.common.model.exception.BusinessException, java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		boolean _EJS_result = false;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[3];
				_jacc_parms[0] = new java.lang.Integer(rutCliente);
				_jacc_parms[1] = new java.lang.Integer(numeroCotizacion);
				_jacc_parms[2] = new java.lang.Integer(codigoFormaPago);
			}
	cl.cencosud.ventaseguros.ejb.MantenedorClientesBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorClientesBean)container.preInvoke(this, 0, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.actualizarFormaPago(rutCliente, numeroCotizacion, codigoFormaPago);
		}
		catch (cl.tinet.common.model.exception.BusinessException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 0, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerPolizaHtml
	 */
	public byte[] obtenerPolizaHtml(int rutCliente, int numeroSolicitud) throws cl.tinet.common.model.exception.BusinessException, java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		byte[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = new java.lang.Integer(rutCliente);
				_jacc_parms[1] = new java.lang.Integer(numeroSolicitud);
			}
	cl.cencosud.ventaseguros.ejb.MantenedorClientesBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorClientesBean)container.preInvoke(this, 1, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerPolizaHtml(rutCliente, numeroSolicitud);
		}
		catch (cl.tinet.common.model.exception.BusinessException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 1, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerPolizaPdf
	 */
	public byte[] obtenerPolizaPdf(int rutCliente, int numeroSolicitud) throws cl.tinet.common.model.exception.BusinessException, java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		byte[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = new java.lang.Integer(rutCliente);
				_jacc_parms[1] = new java.lang.Integer(numeroSolicitud);
			}
	cl.cencosud.ventaseguros.ejb.MantenedorClientesBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorClientesBean)container.preInvoke(this, 2, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerPolizaPdf(rutCliente, numeroSolicitud);
		}
		catch (cl.tinet.common.model.exception.BusinessException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 2, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerSolicitudesWS
	 */
	public cl.cencosud.ventaseguros.asesorvirtual.model.SolicitudCotizacion[] obtenerSolicitudesWS(java.lang.Long rutCliente) throws cl.tinet.common.model.exception.BusinessException, java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.ventaseguros.asesorvirtual.model.SolicitudCotizacion[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = rutCliente;
			}
	cl.cencosud.ventaseguros.ejb.MantenedorClientesBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorClientesBean)container.preInvoke(this, 3, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerSolicitudesWS(rutCliente);
		}
		catch (cl.tinet.common.model.exception.BusinessException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 3, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerEstadosCiviles
	 */
	public cl.cencosud.ventaseguros.common.EstadoCivil[] obtenerEstadosCiviles() throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.ventaseguros.common.EstadoCivil[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[0];
			}
	cl.cencosud.ventaseguros.ejb.MantenedorClientesBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorClientesBean)container.preInvoke(this, 4, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerEstadosCiviles();
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 4, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerCiudades
	 */
	public cl.cencosud.ventaseguros.common.Parametro[] obtenerCiudades(java.lang.String idComuna) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.ventaseguros.common.Parametro[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = idComuna;
			}
	cl.cencosud.ventaseguros.ejb.MantenedorClientesBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorClientesBean)container.preInvoke(this, 5, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerCiudades(idComuna);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 5, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerComunaPorCiudad
	 */
	public cl.cencosud.ventaseguros.common.Parametro[] obtenerComunaPorCiudad(java.lang.String idCiudad) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.ventaseguros.common.Parametro[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = idCiudad;
			}
	cl.cencosud.ventaseguros.ejb.MantenedorClientesBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorClientesBean)container.preInvoke(this, 6, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerComunaPorCiudad(idCiudad);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 6, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerComunas
	 */
	public cl.cencosud.ventaseguros.common.Parametro[] obtenerComunas(java.lang.String idRegion) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.ventaseguros.common.Parametro[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = idRegion;
			}
	cl.cencosud.ventaseguros.ejb.MantenedorClientesBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorClientesBean)container.preInvoke(this, 7, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerComunas(idRegion);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 7, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerRegionPorComuna
	 */
	public cl.cencosud.ventaseguros.common.Parametro[] obtenerRegionPorComuna(java.lang.String idComuna) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.ventaseguros.common.Parametro[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = idComuna;
			}
	cl.cencosud.ventaseguros.ejb.MantenedorClientesBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorClientesBean)container.preInvoke(this, 8, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerRegionPorComuna(idComuna);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 8, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerRegiones
	 */
	public cl.cencosud.ventaseguros.common.Parametro[] obtenerRegiones() throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.ventaseguros.common.Parametro[] _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[0];
			}
	cl.cencosud.ventaseguros.ejb.MantenedorClientesBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorClientesBean)container.preInvoke(this, 9, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerRegiones();
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 9, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerNroOrdenCompra
	 */
	public int obtenerNroOrdenCompra(java.lang.String idSolicitud) throws cl.tinet.common.model.exception.BusinessException, java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		int _EJS_result = 0;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = idSolicitud;
			}
	cl.cencosud.ventaseguros.ejb.MantenedorClientesBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorClientesBean)container.preInvoke(this, 10, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerNroOrdenCompra(idSolicitud);
		}
		catch (cl.tinet.common.model.exception.BusinessException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 10, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerCoberturas
	 */
	public java.util.List obtenerCoberturas(int idPlan) throws cl.tinet.common.model.exception.BusinessException, java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = new java.lang.Integer(idPlan);
			}
	cl.cencosud.ventaseguros.ejb.MantenedorClientesBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorClientesBean)container.preInvoke(this, 11, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerCoberturas(idPlan);
		}
		catch (cl.tinet.common.model.exception.BusinessException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 11, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerParametro
	 */
	public java.util.List obtenerParametro(java.lang.String idGrupo) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = idGrupo;
			}
	cl.cencosud.ventaseguros.ejb.MantenedorClientesBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorClientesBean)container.preInvoke(this, 12, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerParametro(idGrupo);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 12, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerParametro
	 */
	public java.util.List obtenerParametro(java.lang.String idGrupo, java.lang.String valorParametro) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = idGrupo;
				_jacc_parms[1] = valorParametro;
			}
	cl.cencosud.ventaseguros.ejb.MantenedorClientesBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorClientesBean)container.preInvoke(this, 13, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerParametro(idGrupo, valorParametro);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 13, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerSolicitudes
	 */
	public java.util.List obtenerSolicitudes(java.lang.Long rutUsuario) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.util.List _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = rutUsuario;
			}
	cl.cencosud.ventaseguros.ejb.MantenedorClientesBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorClientesBean)container.preInvoke(this, 14, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerSolicitudes(rutUsuario);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 14, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * actualizarCliente
	 */
	public void actualizarCliente(java.util.Map datos) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = datos;
			}
	cl.cencosud.ventaseguros.ejb.MantenedorClientesBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorClientesBean)container.preInvoke(this, 15, _EJS_s, _jacc_parms);
			beanRef.actualizarCliente(datos);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 15, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * enviarEmailFormularioContacto
	 */
	public void enviarEmailFormularioContacto(cl.tinet.common.seguridad.model.UsuarioExterno usuario, java.lang.String tipoContacto, java.lang.String mensajeContacto) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[3];
				_jacc_parms[0] = usuario;
				_jacc_parms[1] = tipoContacto;
				_jacc_parms[2] = mensajeContacto;
			}
	cl.cencosud.ventaseguros.ejb.MantenedorClientesBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorClientesBean)container.preInvoke(this, 16, _EJS_s, _jacc_parms);
			beanRef.enviarEmailFormularioContacto(usuario, tipoContacto, mensajeContacto);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 16, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * registrarCliente
	 */
	public void registrarCliente(cl.tinet.common.seguridad.model.UsuarioExterno usuario, java.lang.String clave) throws cl.cencosud.ventaseguros.common.exception.ValidacionUsuarioException, java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = usuario;
				_jacc_parms[1] = clave;
			}
	cl.cencosud.ventaseguros.ejb.MantenedorClientesBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorClientesBean)container.preInvoke(this, 17, _EJS_s, _jacc_parms);
			beanRef.registrarCliente(usuario, clave);
		}
		catch (cl.cencosud.ventaseguros.common.exception.ValidacionUsuarioException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 17, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * obtenerSubcategoria
	 */
	public java.lang.String obtenerSubcategoria(java.lang.String idSubcategoria) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.lang.String _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = idSubcategoria;
			}
	cl.cencosud.ventaseguros.ejb.MantenedorClientesBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorClientesBean)container.preInvoke(this, 18, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerSubcategoria(idSubcategoria);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 18, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * registrarCotizacion
	 */
	public void registrarCotizacion(java.lang.String rut, java.lang.String nombre, java.lang.String telefono, java.lang.String descSubcategoria) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[4];
				_jacc_parms[0] = rut;
				_jacc_parms[1] = nombre;
				_jacc_parms[2] = telefono;
				_jacc_parms[3] = descSubcategoria;
			}
	cl.cencosud.ventaseguros.ejb.MantenedorClientesBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorClientesBean)container.preInvoke(this, 19, _EJS_s, _jacc_parms);
		beanRef.registrarCotizacion(rut, nombre, telefono, descSubcategoria);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 19, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return;
	}
	/**
	 * enviarEmailCotizador
	 */
	public void enviarEmailCotizador(java.lang.String rut, java.lang.String nombre, java.lang.String telefono, java.lang.String descSubcategoria) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[4];
				_jacc_parms[0] = rut;
				_jacc_parms[1] = nombre;
				_jacc_parms[2] = telefono;
				_jacc_parms[3] = descSubcategoria;
			}
	cl.cencosud.ventaseguros.ejb.MantenedorClientesBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorClientesBean)container.preInvoke(this, 20, _EJS_s, _jacc_parms);
		beanRef.registrarCotizacion(rut, nombre, telefono, descSubcategoria);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 20, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return;
	}
	
	public java.lang.String generarSecuenciaDenuncia() throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.lang.String _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[0];
				
			}
	cl.cencosud.ventaseguros.ejb.MantenedorClientesBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorClientesBean)container.preInvoke(this, 21, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.generarSecuenciaDenuncia();
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 21, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	
	
	public void enviarEmailDenuncias(java.lang.String ruta,java.lang.String folio,java.lang.String rut,java.lang.String nombre,java.lang.String apellidoPat,java.lang.String apellidoMat,java.lang.String direccion,java.lang.String comuna,java.lang.String ciudad,java.lang.String fonoFijo,java.lang.String fonoCelular,java.lang.String detalle) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[12];
				_jacc_parms[0] = ruta;
				_jacc_parms[1] = folio;
				_jacc_parms[2] = rut;
				_jacc_parms[3] = nombre;
				_jacc_parms[4] = apellidoPat;
				_jacc_parms[5] = apellidoMat;
				_jacc_parms[6] = direccion;
				_jacc_parms[7] = comuna;
				_jacc_parms[8] = ciudad;
				_jacc_parms[9] = fonoFijo;
				_jacc_parms[10] = fonoCelular;
				_jacc_parms[11] = detalle;

				
			}
	cl.cencosud.ventaseguros.ejb.MantenedorClientesBean beanRef = (cl.cencosud.ventaseguros.ejb.MantenedorClientesBean)container.preInvoke(this, 22, _EJS_s, _jacc_parms);
		beanRef.enviarEmailDenuncias(ruta, folio, rut, nombre, apellidoPat, apellidoMat, direccion, comuna, ciudad, fonoFijo, fonoCelular, detalle);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 22, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return;
	}
}
