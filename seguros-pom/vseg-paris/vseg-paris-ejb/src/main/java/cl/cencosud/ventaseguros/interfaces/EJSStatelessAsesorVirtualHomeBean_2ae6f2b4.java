package cl.cencosud.ventaseguros.interfaces;

import com.ibm.ejs.container.*;

/**
 * EJSStatelessAsesorVirtualHomeBean_2ae6f2b4
 */
public class EJSStatelessAsesorVirtualHomeBean_2ae6f2b4 extends EJSHome {
	static final long serialVersionUID = 61;
	/**
	 * EJSStatelessAsesorVirtualHomeBean_2ae6f2b4
	 */
	public EJSStatelessAsesorVirtualHomeBean_2ae6f2b4() throws java.rmi.RemoteException {
		super();	}
	/**
	 * create
	 */
	public cl.cencosud.ventaseguros.interfaces.AsesorVirtual create() throws javax.ejb.CreateException, java.rmi.RemoteException {
BeanO beanO = null;
cl.cencosud.ventaseguros.interfaces.AsesorVirtual result = null;
boolean createFailed = false;
try {
	result = (cl.cencosud.ventaseguros.interfaces.AsesorVirtual) super.createWrapper(null);
}
catch (javax.ejb.CreateException ex) {
	createFailed = true;
	throw ex;
} catch (java.rmi.RemoteException ex) {
	createFailed = true;
	throw ex;
} catch (Throwable ex) {
	createFailed = true;
	throw new CreateFailureException(ex);
} finally {
	if (createFailed) {
		super.createFailure(beanO);
	}
}
return result;	}
}
