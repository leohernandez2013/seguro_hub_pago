package cl.cencosud.ventaseguros.delegate;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

import cl.cencosud.ventaseguros.asesorvirtual.model.RestriccionVida;
import cl.cencosud.ventaseguros.common.Ficha;
import cl.cencosud.ventaseguros.common.LegalFicha;
import cl.cencosud.ventaseguros.common.PlanLeg;
import cl.cencosud.ventaseguros.interfaces.FichaSubcategoria;
import cl.cencosud.ventaseguros.interfaces.FichaSubcategoriaHome;

import com.tinet.comun.jndi.ServiceLocator;
import com.tinet.exceptions.system.SystemException;

/**
 * Delegate encargado de manejar lo referente a la ficha.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 23/08/2010
 */
public class FichaSubcategoriaDelegate {

    /**
     * Instancia de la clase FichaSubcategoria.
     */
    private FichaSubcategoria fichaSubcategoria;

    /**
     * Home de FichaSubcategoria.
     */
    private FichaSubcategoriaHome fichaSubcategoriaHome;

    /**
     * Constructor de FichaSubcategoriaDelegate.
     */
    public FichaSubcategoriaDelegate() {
        try {
            this.fichaSubcategoriaHome =
                (FichaSubcategoriaHome) ServiceLocator.singleton()
                    .getRemoteHome(FichaSubcategoriaHome.JNDI_NAME,
                        FichaSubcategoriaHome.class);

            this.fichaSubcategoria = this.fichaSubcategoriaHome.create();
        } catch (Exception e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene un listado de fichas asociadas a una subcategoria.
     * @param idRama Identificador de la rama.
     * @param idSubcategoria Identificador de la subcategoria.
     * @return listado de fichas.
     */
    public List < Map < String, ? > > obtenerFichasSubcategoria(int idRama,
        int idSubcategoria) {
        try {
            return this.fichaSubcategoria.obtenerFichasSubcategoria(idRama,
                idSubcategoria);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    /**
     * Obtiene un listado de fichas asociadas a una subcategoria.
     * @param idRama Identificador de la rama.
     * @param idSubcategoria Identificador de la subcategoria.
     * @return listado de fichas.
     */
    public List < Map < String, ? > > obtenerPlanesPromocion(int idRama,
        int idSubcategoria) {
        try {
            return this.fichaSubcategoria.obtenerPlanesPromocion(idRama,
                idSubcategoria);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    public long agregarFichaProducto(Ficha ficha) {
        try {
            return this.fichaSubcategoria.agregarFichaProducto(ficha);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }
    
    //CBM
    public void agregarLegalFichaProducto(LegalFicha legalFicha) {
        try {
            this.fichaSubcategoria.agregarLegalFichaProducto(legalFicha);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }
    

    public void actualizarFichaProducto(Ficha ficha) {
        try {
            this.fichaSubcategoria.actualizarFichaProducto(ficha);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }
    
    //CBM
    
    public void actualizarLegalFichaProducto(LegalFicha legalFicha){
        try {
            this.fichaSubcategoria.actualizarLegalFichaProducto(legalFicha);
        } catch (RemoteException re) {
            throw new SystemException(re);
        } 	
    }

    public void subirArchivo(long id_ficha, byte[] bios, String tipo) {
        try {
            this.fichaSubcategoria.subirArchivo(id_ficha, bios, tipo);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    public Map < String, Object > obtenerArchivo(long id_ficha, String tipo) {
        try {
            return this.fichaSubcategoria.obtenerArchivo(id_ficha, tipo);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    public List < PlanLeg > obtenerPlanes(int idSubcategoria) {
        try {
            return this.fichaSubcategoria.obtenerPlanes(idSubcategoria);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    public Ficha obtenerFicha(Long id_ficha) {
        try {
            return this.fichaSubcategoria.obtenerFicha(id_ficha);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }
    
    //CBM legal
    public LegalFicha obtenerLegalFicha(Long id_ficha) {
        try {
            return this.fichaSubcategoria.obtenerLegalFicha(id_ficha);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    public PlanLeg obtenerDatosPlan(long idPlan) {
        try {
            return this.fichaSubcategoria.obtenerDatosPlan(idPlan);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
    }

    public boolean existePrimaUnica(long idPlan) {
        try {
            return this.fichaSubcategoria.existePrimaUnica(idPlan);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    public RestriccionVida obtenerRestriccionVida(long idPlan) {
        try {
            return this.fichaSubcategoria.obtenerRestriccionVida(idPlan);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    public void administrarPlan(PlanLeg plan, RestriccionVida restriccion,
        boolean primaUnica, int rama) {
        try {
            this.fichaSubcategoria.administrarPlan(plan, restriccion,
                primaUnica, rama);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }
}
