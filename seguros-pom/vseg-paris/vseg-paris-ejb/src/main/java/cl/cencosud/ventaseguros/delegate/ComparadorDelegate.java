package cl.cencosud.ventaseguros.delegate;

import java.rmi.RemoteException;
import java.util.LinkedHashMap;
import java.util.List;

import com.tinet.comun.jndi.ServiceLocator;
import com.tinet.exceptions.system.SystemException;

import cl.cencosud.ventaseguros.common.PlanLeg;
import cl.cencosud.ventaseguros.comparador.model.Caracteristica;
import cl.cencosud.ventaseguros.interfaces.Comparador;
import cl.cencosud.ventaseguros.interfaces.ComparadorHome;
import cl.tinet.common.seguridad.model.UsuarioInterno;

/**
 * Delegate encargado de manejar lo referente al comparador de planes.
 * <br/>
 * @author Miguel Cornejo V. (TInet)
 * @version 1.0
 * @created 22/06/2011
 */
public class ComparadorDelegate {
	
	private Comparador comparador;	
	private ComparadorHome comparadorHome;

	public ComparadorDelegate() {
		try {
            this.comparadorHome = (ComparadorHome) 
            	ServiceLocator.singleton().getRemoteHome(this.comparadorHome.JNDI_NAME, ComparadorHome.class);
            this.comparador = this.comparadorHome.create();
        } catch (Exception e) {
        	e.printStackTrace();
            throw new SystemException(e);
        }
	}
	
	public List < LinkedHashMap < String, ? > > obtenerProductos(int idRama, int idSubcategoria, Long idProducto, int idTipo) {
		try {
            return this.comparador.obtenerProductos(idRama, idSubcategoria, idProducto, idTipo);
        } catch (RemoteException re) {
            throw new SystemException(re);
        }
	}
	
	public List<PlanLeg> obtenerPlanesPorProducto(int estado, Long idProducto) {
		try {
			return this.comparador.obtenerPlanesPorProducto(estado, idProducto);
		} catch (RemoteException e) {
			throw new SystemException(e);
		}
	}
	
	public List<Caracteristica> obtenerCaracteristicasPorPlan(int idPlan, int tipoCaracteristica, int estado) {
		try {
			return this.comparador.obtenerCaracteristicasPorPlan(idPlan, tipoCaracteristica, estado);
		} catch (RemoteException e) {
			throw new SystemException(e);
		}
	}
	
    public void guardarContenidoComparadorProducto(LinkedHashMap<Integer, Caracteristica> mapCaracteristicas, 
    		LinkedHashMap<Integer, List<Caracteristica>> mapPlanCaracteristica, UsuarioInterno usuario, String habilitado) {
    	try {
    		this.comparador.guardarContenidoComparadorProducto(mapCaracteristicas, mapPlanCaracteristica, usuario, habilitado);
    	} catch (RemoteException e) {
    		throw new SystemException(e);
		}
    }
    
    public void guardarContenidoCaracteristicasCompartidas(
    		LinkedHashMap<Integer, Caracteristica> mapCaractComp, UsuarioInterno usuario, String idProducto) {
		try {
			this.comparador.guardarContenidoCaracteristicasCompartidas(mapCaractComp, usuario, idProducto);
		} catch (RemoteException e) {
			throw new SystemException(e);
		}
	}
	
	public void activarProducto(Long idProducto, int estado) {
		try {
			this.comparador.activarProducto(idProducto, estado);
		} catch (RemoteException e) {
			throw new SystemException(e);
		}
	}

	public boolean isProductoHabilitado(Long idProducto) {
		try {
			return this.comparador.isProductoHabilitado(idProducto);
		} catch (RemoteException e) {
			throw new SystemException(e);
		}
	}
		
	public List<Caracteristica> obtenerCaracteristicasCompartidas(Long idProducto, Integer estado) {
		try {
			return this.comparador.obtenerCaracteristicasCompartidas(idProducto, estado);
		} catch (RemoteException e) {
			throw new SystemException(e);
		}
	}
		
	public List<Caracteristica> getCaracteristicasPorPlan(Integer idPlan, Integer idEstado) {
		try {
			return this.comparador.getCaracteristicasPorPlan(idPlan, idEstado);
		} catch (RemoteException e) {
			throw new SystemException(e);
		}
	}

}
