package cl.cencosud.ventaseguros.delegate;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

import cl.cencosud.ventaseguros.asesorvirtual.model.SolicitudCotizacion;
import cl.cencosud.ventaseguros.common.EstadoCivil;
import cl.cencosud.ventaseguros.common.Parametro;
import cl.cencosud.ventaseguros.common.exception.ValidacionUsuarioException;
import cl.cencosud.ventaseguros.common.model.Solicitud;
import cl.cencosud.ventaseguros.interfaces.MantenedorClientes;
import cl.cencosud.ventaseguros.interfaces.MantenedorClientesHome;
import cl.tinet.common.model.exception.BusinessException;
import cl.tinet.common.seguridad.model.UsuarioExterno;

import com.tinet.comun.jndi.ServiceLocator;
import com.tinet.exceptions.system.SystemException;

/**
 * Clase que sirve de delegate entre el action y el EJB.
 * <br/>
 * @author Ricardo
 * @version 1.0
 * @created Oct 4, 2010
 */
public class MantenedorClientesDelegate {

    /**
     * Atributo para EJB.
     */
    private MantenedorClientes mantenedorClientes;
    /**
     * Atributo para EJB.
     */
    private MantenedorClientesHome mantenedorClientesHome;

    /**
     * Constructor de la clase.
     */
    public MantenedorClientesDelegate() {
        try {
            this.mantenedorClientesHome =
                (MantenedorClientesHome) ServiceLocator.singleton()
                    .getRemoteHome(MantenedorClientesHome.JNDI_NAME,
                        MantenedorClientesHome.class);

            this.mantenedorClientes = this.mantenedorClientesHome.create();
        } catch (Exception e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene listado de parametros según un grupo.
     * @param idGrupo identificador del grupo.
     * @return listado de parametros.
     */
    public List < Map < String, ? > > obtenerParametro(String idGrupo) {
        try {
            return this.mantenedorClientes.obtenerParametro(idGrupo);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene un parametro de sistema en particular.
     * @param idGrupo identificador de grupo de parametros.
     * @param valorParametro valor del parametro.
     * @return parametro de sistema.
     */
    public List < Map < String, ? > > obtenerParametro(String idGrupo,
        String valorParametro) {
        try {
            return this.mantenedorClientes.obtenerParametro(idGrupo,
                valorParametro);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene listado de estados civiles.
     * @return listado de estados civiles.
     */
    public EstadoCivil[] obtenerEstadosCiviles() {
        try {
            return this.mantenedorClientes.obtenerEstadosCiviles();
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene listado de regiones.
     * @return listado de regiones.
     */
    public Parametro[] obtenerRegiones() {
        try {
            return this.mantenedorClientes.obtenerRegiones();
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene listado de comunas.
     * @param idRegion identificador de region.
     * @return listado de comunas.
     */
    public Parametro[] obtenerComunas(String idRegion) {
        try {
            return this.mantenedorClientes.obtenerComunas(idRegion);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene listado de ciudades.
     * @param idComuna identificador de comuna.
     * @return listado de ciudades.
     */
    public Parametro[] obtenerCiudades(String idComuna) {
        try {
            return this.mantenedorClientes.obtenerCiudades(idComuna);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene la comuna a la que pertenece una ciudad.
     * @param idCiudad identificador de ciudad.
     * @return comuna.
     */
    public Parametro[] obtenerComunaPorCiudad(String idCiudad) {
        try {
            return this.mantenedorClientes.obtenerComunaPorCiudad(idCiudad);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene la region a la que pertenece una comuna.
     * @param idComuna identificador de comuna.
     * @return region.
     */
    public Parametro[] obtenerRegionPorComuna(String idComuna) {
        try {
            return this.mantenedorClientes.obtenerRegionPorComuna(idComuna);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
    * Actualiza los datos de un cliente.
    * @param datos datos a actualizar.
    */
    public void actualizarCliente(Map < String, String > datos) {
        try {
            this.mantenedorClientes.actualizarCliente(datos);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtener solicitudes de un usuario.
     * @param rutUsuario rut del usuario
     * @return listado de solicitudes.
     */
    public List < Solicitud > obtenerSolicitudes(Long rutUsuario) {
        try {
            return this.mantenedorClientes.obtenerSolicitudes(rutUsuario);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Método obtenerSolicitudesWS.
     * @param rutCliente
     * @return
     * @throws BusinessException
     */
    public SolicitudCotizacion[] obtenerSolicitudesWS(Long rutCliente)
        throws BusinessException {
        try {
            return this.mantenedorClientes.obtenerSolicitudesWS(rutCliente);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Método obtenerPolizaPdf.
     * @param rutCliente
     * @param numeroSolicitud
     * @return
     */
    public byte[] obtenerPolizaPdf(int rutCliente, int numeroSolicitud)
        throws BusinessException {
        try {
            return this.mantenedorClientes.obtenerPolizaPdf(rutCliente,
                numeroSolicitud);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Método obtenerPolizaHtml.
     * @param rutCliente
     * @param numeroSolicitud
     * @return
     */
    public byte[] obtenerPolizaHtml(int rutCliente, int numeroSolicitud)
        throws BusinessException {
        try {
            return this.mantenedorClientes.obtenerPolizaHtml(rutCliente,
                numeroSolicitud);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Método registrarCliente.
     * @param usuario
     * @param clave
     */
    public void registrarCliente(UsuarioExterno usuario, String clave)
        throws ValidacionUsuarioException {
        try {
            this.mantenedorClientes.registrarCliente(usuario, clave);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir método obtenerNroOrdenCompra.
     * @param idSolicitud
     * @return
     * @throws BusinessException
     */
    public int obtenerNroOrdenCompra(String idSolicitud)
        throws BusinessException {
        try {
            return this.mantenedorClientes.obtenerNroOrdenCompra(idSolicitud);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir método actualizarFormaPago.
     * @param rutCliente
     * @param numeroCotizacion
     * @param codigoFormaPago
     * @return
     * @throws BusinessException
     */
    public boolean actualizarFormaPago(int rutCliente, int numeroCotizacion,
        int codigoFormaPago) throws BusinessException {
        try {
            return this.mantenedorClientes.actualizarFormaPago(rutCliente,
                numeroCotizacion, codigoFormaPago);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir método obtenerCoberturas.
     * @param idPlan
     * @return
     * @throws BusinessException
     */
    public List < Map < String, Object > > obtenerCoberturas(int idPlan)
        throws BusinessException {
        try {
            return this.mantenedorClientes.obtenerCoberturas(idPlan);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    
    /**
     * TODO Describir método enviarEmailFormularioContacto.
     * @param usuario
     * @param tipoContacto
     * @param mensajeContacto
     */
    public void enviarEmailFormularioContacto(UsuarioExterno usuario,
        String tipoContacto, String mensajeContacto) {
        try { 
            this.mantenedorClientes.enviarEmailFormularioContacto(usuario,
                tipoContacto, mensajeContacto);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }

    }
    
    /**
     * Obtiene el nombre de la subcategoria desde el identificador.
     * @param idSubcategoria identificador subcategoria.
     * @return nombre subcategoria.
     */
    public String obtenerSubcategoria(String idSubcategoria){
        try {
            return this.mantenedorClientes.obtenerSubcategoria(idSubcategoria);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }
    
    /**
     * Guarda datos del formulario de cotizacion.
     * @param rut 
     * @param nombre
     * @param telefono
     * @param descSubcategoria
     */
    public void registrarCotizacion(String rut, String nombre, String telefono, String descSubcategoria){
        try {
            this.mantenedorClientes.registrarCotizacion(rut, nombre, telefono, descSubcategoria);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }
    
    /**
     * TODO Describir método enviarEmailCotizador.
     * @param rut 
     * @param nombre
     * @param telefono
     * @param descSubcategoria
     */
    public void enviarEmailCotizador(String rut, String nombre, String telefono, String descSubcategoria) {
        try { 
            this.mantenedorClientes.enviarEmailCotizador(rut, nombre, telefono, descSubcategoria);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }

    }
    
 // CBM metodo que obtiene la secuencia del formulario de contacto de denuncias
    
    public String generarSecuenciaDenuncia(){
    	try{
    		return this.mantenedorClientes.generarSecuenciaDenuncia();
    		
    	}catch(RemoteException e){
    		throw new SystemException(e);
    	}  	
    }
    
    
    public void enviarEmailDenuncias(String ruta,String folio,String rut,String nombre,String apellidoPat,String apellidoMat,String direccion,String comuna,String ciudad,String fonoFijo,String fonoCelular,String detalle){
    		try{
				this.mantenedorClientes.enviarEmailDenuncias(ruta,folio,rut,nombre,apellidoPat,apellidoMat,direccion,comuna,ciudad,fonoFijo,fonoCelular,detalle);		
	}catch(RemoteException e){
		throw new SystemException(e); 
	}  	
}

    
    
    
}
