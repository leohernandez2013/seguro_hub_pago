package cl.cencosud.ventaseguros.ejb;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.cencosud.ventaseguros.common.exception.MantenedorEventosException;
import cl.cencosud.ventaseguros.dao.VSPDAOFactory;
import cl.cencosud.ventaseguros.mantenedoreventos.dao.MantenedorEventosDAO;
import cl.tinet.common.model.exception.BusinessException;
import cl.tinet.common.util.validate.ValidacionUtil;

import com.tinet.exceptions.system.SystemException;

/**
 * @author Roberto San Martín (TInet Soluciones Informáticas).
 * @version 1.0
 * @created 26-Jul-2010 15:32:03
 *
 *
 * @ejb.bean name="MantenedorEventos"
 *           display-name="MantenedorEventos"
 *           description="Servicios relacionados a Mantenedor Eventos."
 *           jndi-name="ejb/ventaseguros/MantenedorEventos"
 *           type="Stateless"
 *           transaction-type="Container"
 *           view-type="remote"
 *
 * @ejb.resource-ref res-ref-name="jdbc/VSPDS" 
 *           res-type="javax.sql.DataSource"
 *           res-sharing-scope="Shareable"
 *           res-auth="Container"
 *           jndi-name="jdbc/VSPDS"
 *
 * @ejb.transaction type="Supports"
 */
public class MantenedorEventosBean implements SessionBean {

    private static final Log LOGGER =
        LogFactory.getLog(MantenedorEventosBean.class);

    private static final long serialVersionUID = -1391101178708021505L;

    public void ejbCreate() {
        // Create.
    }

    /**
     * Buscar los eventos.
     *
     * @param filtros
     * @param filtros
     * @return Lista de evento
     * @ejb.interface-method "remote"
     */
    public List < Map < String, Object > > buscarEventos(Map filtros) {

        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorEventosDAO dao =
            factory.getVentaSegurosDAO(MantenedorEventosDAO.class);
        try {
            return dao.buscarEventos(filtros);
        } finally {
            dao.close();
        }
    }

    /**
     * Metodo relacionado con subir un archivo a la BD.
     *
     * @param id_evento
     * @param bios
     * @param tipo
     * @return
     * @ejb.interface-method "remote"
     */
    public boolean subirArchivo(Long id_evento, byte[] bios, String tipo) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorEventosDAO dao =
            factory.getVentaSegurosDAO(MantenedorEventosDAO.class);
        try {
            return dao.subirArchivo(id_evento, bios, tipo);
        } finally {
            dao.close();
        }
    }

    /**
     * Metodo relacionado con la insercion de un nuevo evento.
     *
     * @param datos
     * @return boolean
     * @ejb.interface-method "remote"
     */
    public Long nuevoEvento(Map datos) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorEventosDAO dao =
            factory.getVentaSegurosDAO(MantenedorEventosDAO.class);
        try {
            return dao.nuevoEvento(datos);
        } finally {
            dao.close();
        }
    }

    /**
     * Metodo relacion con la actualizacion de evento.
     * @param datos
     * @return
     * @ejb.interface-method "remote"
     */
    public boolean actualizarEvento(Map datos) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorEventosDAO dao =
            factory.getVentaSegurosDAO(MantenedorEventosDAO.class);
        try {
            return dao.actualizarEvento(datos);
        } finally {
            dao.close();
        }
    }

    /**
     * Metodo relacionado con la insercion de los Rut desde un archivo.
     *
     * @param datos
     * @return
     * @ejb.interface-method "remote"
     */
    public boolean insertarRutArchivos(Map < String, Object > datos) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorEventosDAO dao =
            factory.getVentaSegurosDAO(MantenedorEventosDAO.class);
        try {
            return dao.insertarRutArchivos(datos);
        } finally {
            dao.close();
        }
    }

    /**
     * Metodo relacionado con la obtencion de un archivo desde la base de datos.
     *
     * @param id_evento
     * @param tipo
     * @return
     * @ejb.interface-method "remote"
     */
    public Map < String, Object > obtenerArchivo(String id_evento, String tipo) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorEventosDAO dao =
            factory.getVentaSegurosDAO(MantenedorEventosDAO.class);
        Map < String, Object > resultado = null;
        try {
            resultado = dao.obtenerArchivo(id_evento, tipo);
        } finally {
            dao.close();
        }
        return resultado;
    }

    /**
     * Metodo relacionado a obtener un map con datos de un evento en particular.
     *
     * @param idEvento
     * @return
     * @ejb.interface-method "remote"
     */
    public Map < String, Object > obtenerEvento(Long idEvento) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorEventosDAO dao =
            factory.getVentaSegurosDAO(MantenedorEventosDAO.class);
        Map < String, Object > resultado = null;
        try {
            resultado = dao.obtenerEvento(idEvento);
        } finally {
            dao.close();
        }
        return resultado;
    }

    /**
     * Metodo relacionado con obtencion de posiciones Evento.
     * @return
     * @ejb.interface-method "remote"
     */
    public List < Map < String, ? >> obtenerPosiciones() {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorEventosDAO dao =
            factory.getVentaSegurosDAO(MantenedorEventosDAO.class);
        List < Map < String, ? >> resultado = null;
        try {
            resultado = dao.obtenerPosiciones();
        } finally {
            dao.close();
        }
        return resultado;
    }

    /**
     * Metodo relacionado con la obtencion de tipo de pagina.
     * @return
     * @ejb.interface-method "remote"
     */
    public List < Map < String, ? >> obtenerTipoPagina() {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorEventosDAO dao =
            factory.getVentaSegurosDAO(MantenedorEventosDAO.class);
        List < Map < String, ? >> resultado = null;
        try {
            resultado = dao.obtenerTipoPagina();
        } finally {
            dao.close();
        }
        return resultado;
    }

    /**
     * Metodo relacionado con la obtencion de Prioridades.
     * @return
     * @ejb.interface-method "remote"
     */
    public List < Map < String, ? >> obtenerPrioridad() {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorEventosDAO dao =
            factory.getVentaSegurosDAO(MantenedorEventosDAO.class);
        List < Map < String, ? >> resultado = null;
        try {
            resultado = dao.obtenerPrioridad();
        } finally {
            dao.close();
        }
        return resultado;
    }

    /**
     * Metodo relacionado con la obtencion de Tipo Evento.
     * @return
     * @ejb.interface-method "remote"
     */
    public List < Map < String, ? >> obtenerTipoEvento() {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorEventosDAO dao =
            factory.getVentaSegurosDAO(MantenedorEventosDAO.class);
        List < Map < String, ? >> resultado = null;
        try {
            resultado = dao.obtenerTipoEvento();
        } finally {
            dao.close();
        }
        return resultado;

    }

    /**
     * Metodo relacionado con obtener la cantida de resultado de un codigo
     * No debe ser mas de 1.
     * @param codigo
     * @return
     * @ejb.interface-method "remote"
     */
    public Integer obtenerCantidadEvento(String codigo) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorEventosDAO dao =
            factory.getVentaSegurosDAO(MantenedorEventosDAO.class);
        Integer resultado = null;
        try {
            resultado = dao.obtenerCantidadEvento(codigo);
        } finally {
            dao.close();
        }
        return resultado;
    }

    /**
     * Metodo relacionado con modificarArchivo.
     * @param id_evento
     * @param bios
     * @param tipo
     * @return
     * @ejb.interface-method "remote"
     */
    public boolean modificarArchivo(Long id_evento, byte[] bios, String tipo) {
        boolean resultado = false;
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorEventosDAO dao =
            factory.getVentaSegurosDAO(MantenedorEventosDAO.class);
        try {
            resultado = dao.modificarArchivo(id_evento, bios, tipo);
        } finally {
            dao.close();
        }
        return resultado;
    }

    /**
     * Metodo relacionado con el modificarArchivoRut.
     * @param datos
     * @return
     * @ejb.interface-method "remote"
     */
    public Map < Object, String > modificarArchivoRut(
        Map < String, Object > datos) throws BusinessException {
        Map < Object, String > mRut = new HashMap < Object, String >();
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorEventosDAO dao =
            factory.getVentaSegurosDAO(MantenedorEventosDAO.class);
        try {
            Long idEvento = (Long) datos.get("ID_evento");

            //Obtener ocurrencia evento.
            Integer ocurrencia = dao.obtenerOcurrencia(idEvento);

            //Validar ruts.
            String[] ruts = (String[]) datos.get("ArregloRut");
            mRut = validarRuts(ruts, idEvento, dao);

            //Procesar ruts VALIDOs
            Iterator iruts = mRut.entrySet().iterator();
            while (iruts.hasNext()) {
                Map.Entry < Object, String > entry =
                    (Map.Entry < Object, String >) iruts.next();

                //Procesar ruts VALIDOS
                if ("VALIDO".equals(entry.getValue())) {
                    String key = (String) entry.getKey();
                    String srut = key.substring(0, key.length() - 2);
                    dao.grabarRut(Long.valueOf(srut), idEvento, ocurrencia);
                    entry.setValue("INGRESADO");
                }
            }

        } finally {
            dao.close();
        }
        return mRut;
    }

    private Map < Object, String > validarRuts(String[] ruts, Long idEvento,
        MantenedorEventosDAO dao) {

        Map < Object, String > mRut = new HashMap < Object, String >();

        for (int i = 0; i < ruts.length; i++) {
            String rut = ruts[i];

            if (rut.contains("-")) {
                char digitoVerificador =
                    rut.substring(rut.length() - 1, rut.length()).charAt(0);
                long numRUT = Long.valueOf(rut.substring(0, rut.length() - 2));

                //Se descartan repetidos en el archivo.
                if (!mRut.containsKey(numRUT)) {
                    if (!ValidacionUtil.isValidoRUT(numRUT, digitoVerificador)) {
                        mRut.put(rut, "INVALIDO");
                        LOGGER.error("RUT: " + rut + " INVALIDO");
                    } else {
                        //buscar rut.
                        Map resRut = dao.buscarRut(idEvento, numRUT);
                        if (resRut == null) {
                            //Agregar rut.
                            mRut.put(rut, "VALIDO");
                            LOGGER.error("RUT: " + rut + " VALIDO");
                        } else {
                            //Rut ya existe para ese evento.
                            mRut.put(rut, "REPETIDO");
                            LOGGER.error("RUT: " + rut + " REPETIDO");
                        }
                    }
                }
            } else {
                Integer.valueOf(rut);
                mRut.put(rut, "FORMATOINVALIDO");
                LOGGER.error("RUT: " + rut + " FORMATO INVALIDO");
            }
        }
        return mRut;
    }

    /**
     * Metodo relacionado con obtener si existe conflicto entre Evento.
     * @param prioridad
     * @param fechaInicioDate
     * @param fechaTerminoDate
     * @return
     * @ejb.interface-method "remote"
     */
    public boolean conflictoPrioridad(Integer prioridad, Date fechaInicioDate,
        Date fechaTerminoDate) {

        boolean resultado = false;
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorEventosDAO dao =
            factory.getVentaSegurosDAO(MantenedorEventosDAO.class);
        try {
            if (dao.conflictoPrioridad(prioridad, fechaInicioDate,
                fechaTerminoDate)) {
                resultado = true;
            }

        } finally {
            dao.close();
        }
        return resultado;
    }

    /**
     * Metodo relacionado con obtener una nueva sugerencia.
     * @param fechaInicioDate
     * @param fechaTerminoDate
     * @return
     * @ejb.interface-method "remote"
     */
    public Integer obtenerSugerenciaPrioridad(Integer prioridad,
        Date fechaInicioDate, Date fechaTerminoDate) {
        Integer resultado = 0;
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorEventosDAO dao =
            factory.getVentaSegurosDAO(MantenedorEventosDAO.class);
        try {
            Integer prioridadRes = prioridad;

            Integer sugerencia =
                dao.obtenerMaxPriori(fechaInicioDate, fechaTerminoDate);

            Integer maximaPrioridad = dao.obtenerMaxPriori();

            if (maximaPrioridad <= prioridad) {
                resultado = sugerencia + 1; //le agrego uno a la sugerencia
                dao.insertarNuevaPrioridad(resultado);
            } else {

                boolean conflicto = true;
                while (conflicto) {
                    prioridadRes++;
                    conflicto =
                        this.conflictoPrioridad(prioridadRes, fechaInicioDate,
                            fechaTerminoDate);
                }

                if (prioridadRes > maximaPrioridad) {
                    dao.insertarNuevaPrioridad(prioridadRes);
                }

                resultado = prioridadRes;
            }

        } finally {
            dao.close();
        }
        return resultado;
    }

    /**
     * Metodo relacionado con la obtencion de la prioridad de un evento.
     * @param idEvento
     * @return
     * @ejb.interface-method "remote"
     */
    public Integer obtenerPrioridadEvento(Long idEvento) {
        Integer resultado = 0;
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorEventosDAO dao =
            factory.getVentaSegurosDAO(MantenedorEventosDAO.class);
        try {
            return dao.obtenerPrioridadEvento(idEvento);

        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }
    }

    /**
     * Metodo relacionado con la obtencion
     * de la fecha Inicio.
     * @param idEvento
     * @return
     * @ejb.interface-method "remote"
     */
    public Date obtenerFechaInicio(Long idEvento) {
        Date resultado;
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorEventosDAO dao =
            factory.getVentaSegurosDAO(MantenedorEventosDAO.class);
        try {
            return dao.obtenerFechaInicio(idEvento);

        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }
    }

    /**
     *  Metodo relacionado con obtener la fecha de termino.
     * @param idEvento
     * @return
     * @ejb.interface-method "remote"
     */
    public Date obtenerFechaTermino(Long idEvento) {
        Date resultado;
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorEventosDAO dao =
            factory.getVentaSegurosDAO(MantenedorEventosDAO.class);
        try {
            return dao.obtenerFechaTermino(idEvento);

        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }
    }

    /**
     * Metodo relacion con la borrar un evento.
     * @param datos
     * @return
     * @ejb.interface-method "remote"
     */
    public boolean borrarEvento(Long idEvento) throws BusinessException {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorEventosDAO dao =
            factory.getVentaSegurosDAO(MantenedorEventosDAO.class);
        Map < String, Object > resultado = null;
        try {
            resultado = this.obtenerEvento(idEvento);
            if (resultado.isEmpty() || resultado.size() == 0) {
                throw new MantenedorEventosException(
                    MantenedorEventosException.EVENTO_NO_EXISTE,
                    MantenedorEventosException.SIN_ARGUMENTOS);
            }
            return dao.borrarEvento(idEvento);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtener evento para mostrar en el BO.
     * @param filtros listado de parametros.
     * @return Evento a mostrar.
     * @ejb.interface-method "remote"
     */
    public Map < String, Object > obtenerEventoMostrar(Map filtros) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorEventosDAO dao =
            factory.getVentaSegurosDAO(MantenedorEventosDAO.class);
        Map < String, Object > resultado = null;
        try {
            //Calcular evento a mostrar deacuerdo a los parametros.

            LOGGER.info("Buscar evento especifico para (usuario: "
                + filtros.get("usuario") + ", posicion: "
                + filtros.get("posicion") + ", rama: " + filtros.get("rama"));
            long idEvento = dao.buscarEventoMostrar(filtros);
            if (idEvento == -1) {
                //Se busca eventos genericos.
                LOGGER
                    .info("No se encontraron eventos, se busca evento generico.");
                filtros.remove("usuario");
                idEvento = dao.buscarEventoMostrar(filtros);
            }

            //Se obtiene el evento calculado.
            resultado = dao.obtenerEvento(idEvento);
        } finally {
            dao.close();
        }
        return resultado;
    }

    /**
     * TODO Describir método verEvento.
     * @param id
     * @param rut
     * @ejb.interface-method "remote"
     */
    public void verEvento(Long id, Long rut) {
        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        MantenedorEventosDAO dao =
            factory.getVentaSegurosDAO(MantenedorEventosDAO.class);
        try {
            dao.verEvento(id, rut);
        } finally {
            dao.close();
        }

    }

    /**
     * Metodo Activate EJB.
     * @throws EJBException
     * @throws RemoteException
     */
    public void ejbActivate() throws EJBException, RemoteException {
    }

    /**
     * Metodo Passivate EJB.
     * @throws EJBException
     * @throws RemoteException
     */
    public void ejbPassivate() throws EJBException, RemoteException {
    }

    /**
     * Metodo Remove.
     * @throws EJBException
     * @throws RemoteException
     */
    public void ejbRemove() throws EJBException, RemoteException {
    }

    /**
     * Metodo SessionContext.
     * @param arg0
     * @throws EJBException
     * @throws RemoteException
     */
    public void setSessionContext(SessionContext arg0) throws EJBException,
        RemoteException {
    }
}
