package cl.cencosud.ventaseguros.ejb;

import java.io.UnsupportedEncodingException;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.mail.MessagingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.cencosud.ventaseguros.common.PlanLeg;
import cl.cencosud.ventaseguros.comparador.dao.ComparadorDAO;
import cl.cencosud.ventaseguros.comparador.model.Caracteristica;
import cl.cencosud.ventaseguros.dao.VSPDAOFactory;
import cl.tinet.common.mail.Mail;
import cl.tinet.common.seguridad.model.UsuarioInterno;

/**
 * @author Miguel Cornejo V. (TInet Soluciones Informáticas).
 * @version 1.0
 * @created 22-Jun-2011 15:32:03
 * 
 * 
 * @ejb.bean name="Comparador"
 *           display-name="Comparador"
 *           description="Servicios relacionados al comparador de planes."
 *           jndi-name="ejb/ventaseguros/Comparador"
 *           type="Stateless"
 *           view-type="remote"
 *
 * @ejb.resource-ref res-ref-name="jdbc/VSPDS" 
 *           res-type="javax.sql.DataSource"
 *           res-sharing-scope="Shareable"
 *           res-auth="Container"
 *           jndi-name="jdbc/VSPDS"
 *           
 * @ejb.transaction type="Supports"
 */
public class ComparadorBean implements SessionBean {

	private static final long serialVersionUID = 2412542429363129052L;
	private static final int ID_BENEFICIO = 1;
	private static final int ID_COB_PART = 2;
	private static final int ID_COB_COMP = 3;
	private static final int ID_EXCLUSION = 4;
	
	private static final Log logger = LogFactory.getLog(ComparadorBean.class);
	
    /**
     * ejbCreate.
     */
    public void ejbCreate() {
        //Create.
    }
    
	public void ejbActivate() throws EJBException, RemoteException {
		// TODO Auto-generated method stub
		
	}

	public void ejbPassivate() throws EJBException, RemoteException {
		// TODO Auto-generated method stub
		
	}

	public void ejbRemove() throws EJBException, RemoteException {
		// TODO Auto-generated method stub
		
	}

	public void setSessionContext(SessionContext arg0) throws EJBException,
			RemoteException {
		// TODO Auto-generated method stub
		
	}
	
	public ComparadorBean () {
		
	}

	/**
     * Obtiene un listado de ramas de productos.
     * @return Listado de ramas.
     * 
     * @ejb.interface-method "remote"
     * 
     */
    public List < LinkedHashMap < String, ? > > obtenerProductos(int idRama, int idSubcategoria, Long idProducto, int idTipo) {
    	ComparadorDAO dao = this.getComparadorDAO();
    	try {
            return dao.obtenerProductos(idRama, idSubcategoria, idProducto, idTipo);
        } finally {
            dao.close();
        }
    }
    
    /**
     * Obtiene un listado de los planes en base a 
     * un producto seleccionado.
     * 
     * @ejb.interface-method "remote"
     * 
     */
    public List<PlanLeg> obtenerPlanesPorProducto(int estado, Long idProducto) {
    	ComparadorDAO dao = this.getComparadorDAO();
    	try {
    		return dao.obtenerPlanesPorProducto(estado, idProducto);
    	} finally {
    		dao.close();
    	}
    }
    
    /**
     * Obtiene un listado de las caracterisitcas por plan.
     * 
     * @ejb.interface-method "remote"
     * 
     */
    public List<Caracteristica> obtenerCaracteristicasPorPlan(int idPlan, int tipoCaracteristica, int estado) {
    	ComparadorDAO dao = this.getComparadorDAO();
    	try {
    		return dao.obtenerCaracteristicasPorPlan(idPlan, tipoCaracteristica, estado);
        } finally {
            dao.close();
        }
    }
    
    /**
     * Permite guardar el contenido asociado a los planes de un producto 
     * para ser utilizado por el comparador de planes.
     * 
     * @ejb.interface-method "remote"
     * 
     */
	public void guardarContenidoComparadorProducto(LinkedHashMap<Integer, Caracteristica> mapCaracteristicas, 
			LinkedHashMap<Integer, List<Caracteristica>> mapPlanCaracteristica,
			UsuarioInterno usuario, String habilitado) {
		ComparadorDAO dao = this.getComparadorDAO();
		try {
			Collection c = mapCaracteristicas.values();
			Iterator iter = c.iterator();
			
			while (iter.hasNext()) {
				Caracteristica caracTemp = (Caracteristica)iter.next();
				
				if (caracTemp.getEs_eliminado().trim().equalsIgnoreCase("new")) {
					int codCaractTemp = dao.agregarCaracteristica(usuario, caracTemp);

					if (mapPlanCaracteristica.containsKey(caracTemp.getId_caracteristica())) {
						List<Caracteristica> list = mapPlanCaracteristica.get(caracTemp.getId_caracteristica());
						for (Caracteristica caractTemp : list) {
							caractTemp.setId_caracteristica(codCaractTemp);
							caractTemp.setEs_eliminado("0");
							caractTemp.setHabilitado(habilitado);
							if (caractTemp.getValor() != null
									&& !caractTemp.getValor().trim().equals("")) {
								dao.agregarPlanCaracteristica(caractTemp);
							}
						}
					} 
				} else if (caracTemp.getEs_eliminado().trim().equalsIgnoreCase("mod")) {
					int codCaractTemp = caracTemp.getId_caracteristica();
					caracTemp.setEs_eliminado("0");
					dao.actualizarCaracteristica(usuario, caracTemp);

					if (mapPlanCaracteristica.containsKey(caracTemp.getId_caracteristica())) {
						List<Caracteristica> list = mapPlanCaracteristica.get(caracTemp.getId_caracteristica());

						for (Caracteristica caractTemp : list) {
							caractTemp.setId_caracteristica(codCaractTemp);
							caractTemp.setEs_eliminado("0");
							caractTemp.setHabilitado(habilitado);
							boolean existe = dao.existePlanCaracteristica(caractTemp);
							
							if (existe) {
								if (caractTemp.getValor() != null
										&& !caractTemp.getValor().trim().equals("")) {
									dao.actualizarPlanCaracteristica(caractTemp);
								} else {
									dao.deshabilitarPlanCaracteristica(caractTemp);
								}
							} else {
								if (caractTemp.getValor() != null
										&& !caractTemp.getValor().trim().equals("")) {
									dao.agregarPlanCaracteristica(caractTemp);
								}
							}
						}
					} 
				} else if (caracTemp.getEs_eliminado().trim().equalsIgnoreCase("del")) {
					int codCaractTemp = caracTemp.getId_caracteristica();

					boolean existeCaract = dao.existeCaracteristica(caracTemp);

					if (existeCaract) {
						dao.deshabilitarCaracteristica(usuario, caracTemp);
					}

					if (mapPlanCaracteristica.containsKey(caracTemp.getId_caracteristica())) {
						List<Caracteristica> list = mapPlanCaracteristica.get(caracTemp.getId_caracteristica());

						for (Caracteristica caractTemp : list) {
							caractTemp.setId_caracteristica(codCaractTemp);
							caractTemp.setEs_eliminado("1");

							boolean existe = dao
									.existePlanCaracteristica(caractTemp);
							if (existe) {
								dao.deshabilitarPlanCaracteristica(caractTemp);
							}
						}
					}
				} else {
					int codCaractTemp = caracTemp.getId_caracteristica();

					if (mapPlanCaracteristica.containsKey(caracTemp.getId_caracteristica())) {
						List<Caracteristica> list = mapPlanCaracteristica
								.get(caracTemp.getId_caracteristica());

						for (Caracteristica caractTemp : list) {
							caractTemp.setId_caracteristica(codCaractTemp);
							caractTemp.setEs_eliminado("0");
							caractTemp.setHabilitado(habilitado);
							boolean existe = dao
									.existePlanCaracteristica(caractTemp);
							if (existe) {
								if (caractTemp.getValor() != null
										&& !caractTemp.getValor().trim()
												.equals("")) {
									dao.actualizarPlanCaracteristica(caractTemp);
								} else {
									caractTemp.setEs_eliminado("1");
									dao.deshabilitarPlanCaracteristica(caractTemp);
								}
							} else {
								if (caractTemp.getValor() != null
										&& !caractTemp.getValor().trim()
												.equals("")) {
									dao.agregarPlanCaracteristica(caractTemp);
								}
							}
						}
					}
				}
			}
		} finally {
			dao.close();
		}
    }
    
    /**
     * Permite guardar el contenido asociado a las coberturas 
     * compartidas de un producto.
     * 
     * @ejb.interface-method "remote"
     * 
     */
	public void guardarContenidoCaracteristicasCompartidas(
			LinkedHashMap<Integer, Caracteristica> mapCaractComp, UsuarioInterno usuario, String idProducto) {
    	ComparadorDAO dao = this.getComparadorDAO();
		try {
			Collection c = mapCaractComp.values();
			Iterator iter = c.iterator();

			while (iter.hasNext()) {
				Caracteristica caract = (Caracteristica)iter.next();
				caract.setId_producto(Long.valueOf(idProducto));
				
				if (caract.getEs_eliminado().trim().equalsIgnoreCase("new")) {
					caract.setEs_eliminado("0");
					Integer codTemp = dao.agregarCaracteristicaCompartida(usuario, caract);
					caract.setId_caracteristica(codTemp);
					dao.agregarProductoCaracteristica(caract);					
				} else if (caract.getEs_eliminado().trim().equalsIgnoreCase("mod")){
					caract.setEs_eliminado("0");
					dao.actualizarCaracteristicaCompartida(usuario, caract);
					dao.actualizarProductoCaracteristica(caract);
				} else if (caract.getEs_eliminado().trim().equalsIgnoreCase("del")) {
					caract.setEs_eliminado("1");
					dao.deshabilitarCaracteristicaCompartida(usuario, caract);
					dao.deshabilitarProductoCaracteristica(caract);
				}
			} 
		} finally {
			dao.close();
		}
    }
    
    /**
     * Permite activar o desactivar un producto para el comprador de planes.
     * @return Listado de ramas.
     * 
     * @ejb.interface-method "remote"
     * 
     */
    public void activarProducto(Long idProducto, int estado) {
    	ComparadorDAO dao = this.getComparadorDAO();
    	try {
    		dao.activarProducto(idProducto, estado);
        } finally {
            dao.close();
        }
    }
    
    /**
     * Obtiene todas las caracteristicas asociadas a un plan.
     * @return Listado de Caracteristica.
     * 
     * @ejb.interface-method "remote"
     * 
     */
    public List<Caracteristica> getCaracteristicasPorPlan(Integer idPlan, Integer idEstado) {
    	
    	/* CODIGO DE LOS TIPOS DE CARACTERISTICAS
    	 * 
    	 *  BENEFICIO: 1
    	 *  COBERTURAS: 2
    	 *  COBERTURAS COMPARTIDAS: 3
    	 *  EXCLUSIONES: 4 
    	 *  
    	 *  */
    	
    	ComparadorDAO dao = this.getComparadorDAO();
    	try {
    		List<Caracteristica> result = new ArrayList<Caracteristica>();
    	
    	List<Caracteristica> listBeneficios = dao.obtenerCaracteristicasPorPlan(idPlan, ID_BENEFICIO, idEstado);
    	List<Caracteristica> listCobPart = dao.obtenerCaracteristicasPorPlan(idPlan, ID_COB_PART, idEstado);
    	List<Caracteristica> listExclusiones = dao.obtenerCaracteristicasPorPlan(idPlan, ID_EXCLUSION, idEstado);
    	
    	if (listBeneficios != null && listBeneficios.size() > 0) {
    		if (listBeneficios.get(0).getHabilitado().trim().equals("1")) {
    			result.addAll(listBeneficios);
    		} 
    	}
    	
    	if (listCobPart != null && listCobPart.size() > 0) {
    		result.addAll(listCobPart);
    	}
    	
    	if (listExclusiones != null && listExclusiones.size() > 0) {
    		result.addAll(listExclusiones);
    	}
    	
    	return result;
    	
    	} finally {
			dao.close();
		}
    }
    
    /**
     * Permite identificar i un producto se encuentra habilitado 
     * para el comparador de planes.
     * @return estado.
     * 
     * @ejb.interface-method "remote"
     * 
     */
    public boolean isProductoHabilitado(Long idProducto) {
    	ComparadorDAO dao = this.getComparadorDAO();
    	try {
    		return dao.isProductoHabilitado(idProducto);
        } finally {
            dao.close();
        }
    }
    
    /**
     * Obtiene un listado de las caracteristicas compartidas por producto leg.
     * 
     * @ejb.interface-method "remote"
     * 
     */
    public List<Caracteristica> obtenerCaracteristicasCompartidas(Long idProducto, Integer estado) {
    	ComparadorDAO dao = this.getComparadorDAO();
    	try {
    		return dao.obtenerCaracteristicasCompartidas(idProducto, estado);
        } finally {
            dao.close();
        }
    }
    
    /**
     * Obtiene un listado de las caracteristicas compartidas por producto.
     * 
     * @ejb.interface-method "remote"
     * 
     */
    public List<Caracteristica> obtenerCaracteristicasCompartidasFront(Long idProducto, Integer idEstado) {
    	ComparadorDAO dao = this.getComparadorDAO();
    	try {
    		return dao.obtenerCaracteristicasCompartidasFront(idProducto, idEstado);
        } finally {
            dao.close();
        }
    }
    
    /**
     * Metodo que permite habilitar o deshabilitar los beneficios.
     * 
     * @ejb.interface-method "remote"
     * 
     */
    public void actualizarHabilitadoPorPlan(int idPlan, int habilitado, int tipoCaract) {
    	ComparadorDAO dao = this.getComparadorDAO();
    	try {
    		dao.actualizarHabilitadoPorPlan(idPlan, habilitado, tipoCaract);
        } finally {
            dao.close();
        }
    }
    
    /**
     * Metodo que permite guardar la información que el cliente
     * instruyo comparar.
     * 
     * @ejb.interface-method "remote"
     * 
     */
    public void guardarComparador(long rutCliente, LinkedHashMap mapResultComparador) {
    	ComparadorDAO dao = this.getComparadorDAO();
    	try {
			List<Caracteristica> listPlanes = (List<Caracteristica>) mapResultComparador .get("listPlanes");
			LinkedHashMap<Integer, LinkedHashMap<String, List<Caracteristica>>> mapCaracteristicas = (LinkedHashMap<Integer, LinkedHashMap<String, List<Caracteristica>>>) mapResultComparador.get("listCaracteristicas");

			SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmm");			
			String idComprador = format.format(new Date());
			
			Iterator iter = mapCaracteristicas.entrySet().iterator();

			while (iter.hasNext()) {
				Map.Entry<Integer, LinkedHashMap<String, List<Caracteristica>>> e = (Map.Entry<Integer, LinkedHashMap<String, List<Caracteristica>>>) iter.next();

				Integer tipo = e.getKey();
				LinkedHashMap<String, List<Caracteristica>> mapTemp = e.getValue();

				Collection c = mapTemp.values();
				Iterator ite = c.iterator();

				while (ite.hasNext()) {
					List<Caracteristica> list = (List<Caracteristica>)ite.next();

					for (Caracteristica ca : list) {
						ca.setTipo(tipo);
						if (ca.getTipo() != ID_COB_COMP) {
							for (Caracteristica caract : listPlanes) {
								if (ca.getId_plan() == caract.getId_plan()) {
									ca.setPrimaMensualPesos(caract.getPrimaMensualPesos());
									ca.setPrimaMensualUF(caract.getPrimaMensualUF());
									ca.setIdPlanCompannia(caract.getIdPlanCompannia());
									dao.guardarComparadorCaracteristica(idComprador, rutCliente, ca);
									break;
								}
							}
						} else {
							dao.guardarComparadorCaracteristicaCompartida(idComprador, rutCliente, ca);
						}
					}
				}
			}
        } finally {
            dao.close();
        }   	
    }
    
    /**
     * Metodo que permite guardar la información que el cliente
     * instruyo comparar.
     * 
     * @ejb.interface-method "remote"
     * 
     */
    public boolean enviarComparador (String emailCliente, String html) { 
		boolean result = false;

		ComparadorDAO dao = this.getComparadorDAO();
		try {
			String server = dao.obtenerDatosEmail("SERVER_CORREO");
			String from = dao.obtenerDatosEmail("MAIL_FROM");
			String asunto = dao.obtenerDatosEmail("MAIL_SUBJECT_COTIZACION");

			Mail mail = new Mail(from, emailCliente, asunto, "", server);
			mail.addContenido(html);
			// mail.addContenido(pdf, "application/pdf", "cotizacion.pdf");
			mail.sendMail();
			result = true;
		} catch (UnsupportedEncodingException e) {
			logger.error("Error al generar el email", e);
		} catch (MessagingException e) {
			logger.error("Error al generar el email", e);
		} catch (Exception e) {
			logger.error("Error al generar el email", e);
		} finally {
			dao.close();
		}
		return result;
	}
    
    
    private ComparadorDAO getComparadorDAO() {
    	VSPDAOFactory factory = VSPDAOFactory.getInstance();
        return factory.getVentaSegurosDAO(ComparadorDAO.class);
    }
}
