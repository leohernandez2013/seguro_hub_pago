package cl.cencosud.ventaseguros.mantenedorclientes.dao.ws;

import cl.cencosud.bigsa.cotizacion.webservice.client.WsBigsaPolizaPDFDelegate;
import cl.cencosud.bigsa.cotizacion.webservice.client.WsBigsaPolizaPDFService_Impl;
import cl.cencosud.bigsa.gestion.formapago.grabar.webservice.client.WsBigsaActulizaFormaPagoImplDelegate;
import cl.cencosud.bigsa.gestion.formapago.grabar.webservice.client.WsBigsaActulizaFormaPagoService_Impl;
import cl.cencosud.bigsa.gestion.solicitud.webservice.client.SolicitudVO;
import cl.cencosud.bigsa.gestion.solicitud.webservice.client.WsBigsaSolicitudesImplDelegate;
import cl.cencosud.bigsa.gestion.solicitud.webservice.client.WsBigsaSolicitudes_Impl;
import cl.cencosud.bigsa.poliza.html.webservice.client.WsBigsaPolizaHtmlImplDelegate;
import cl.cencosud.bigsa.poliza.html.webservice.client.WsBigsaPolizaHtml_Impl;
import cl.cencosud.ventaseguros.asesorvirtual.model.SolicitudCotizacion;
import cl.cencosud.ventaseguros.common.exception.ObtenerArchivosException;
import cl.cencosud.ventaseguros.common.exception.SolicitudException;
import cl.cencosud.ventaseguros.dao.VSPDAOFactory;
import cl.cencosud.ventaseguros.mantenedorclientes.dao.MantenedorClientesWSDAO;
import cl.cencosud.ventaseguros.mantenedorclientes.dao.VSPConfigDAO;
import cl.tinet.common.config.BaseConfigurable;
import cl.tinet.common.model.exception.BusinessException;
import cl.tinet.common.util.resource.ResourceLeakUtil;
import com.tinet.exceptions.system.SystemException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.xml.rpc.soap.SOAPFaultException;
import javax.xml.soap.Detail;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class MantenedorClientesWSDAOWS extends BaseConfigurable
  implements MantenedorClientesWSDAO
{
  private static Log logger = LogFactory.getLog(MantenedorClientesWSDAOWS.class);
  private static final String SERVICE_ENDPOINT_KEY = "SERVICE_ENDPOINT_KEY";
  private static final String WSDL_SOLICITUDES_BIGSA = "cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWS.wsdl.solicitudes";
  private static final String WSDL_POLIZA_PDF = "cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWS.wsdl.poliza.pdf";
  private static final String WSDL_POLIZA_HTML = "cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWS.wsdl.poliza.html";
  private static final String ACTUALIZAR_FORMA_PAGO = "cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWS.wsdl.actualizaFormaPago";
  private static final int SEGUROS_VIGENTES = 4;

  public void close()
  {
  }

  public Class getDAOInterface()
  {
    return null;
  }

  public void setDAOInterface(Class interfazDAO)
  {
  }

  private <T> T inicializarWS(T stub, Map<String, ?> param)
    throws SystemException
  {
    if (stub == null) {
      throw new NullPointerException("Debe especificar un stub no nulo.");
    }
    if (param == null) {
      logger.warn("No se especific� URL destino del servicio.");
    }
    if (param.containsKey("SERVICE_ENDPOINT_KEY")) {
      Object endpoint = param.get("SERVICE_ENDPOINT_KEY");
      if (logger.isDebugEnabled())
        logger.debug("Intentando establecer URL destino: " + endpoint);
      try
      {
        Method method = 
          stub.getClass().getMethod("_setProperty", new Class[] { String.class, 
          Object.class });
        logger.debug("M�todo encontrado. Invocando...");
        method.invoke(stub, new Object[] { "javax.xml.rpc.service.endpoint.address", param
          .get("SERVICE_ENDPOINT_KEY") });
        logger.debug("Propiedad establecida exitosamente.");
      } catch (SecurityException se) {
        throw new SystemException(se);
      } catch (NoSuchMethodException nsme) {
        throw new SystemException(nsme);
      } catch (IllegalArgumentException iae) {
        throw new SystemException(iae);
      } catch (IllegalAccessException iae) {
        throw new SystemException(iae);
      } catch (InvocationTargetException ite) {
        throw new SystemException(ite);
      }
    } else {
      logger.warn("No se especific� URL destino del servicio.");
    }
    return stub;
  }

  private void obtenerExcepcionWS(RemoteException e)
    throws SolicitudException
  {
    if ((e.detail instanceof SOAPFaultException)) {
      SOAPFaultException sfe = (SOAPFaultException)e.detail;
      Detail detail = sfe.getDetail();
      if (detail != null) {
        SolicitudException vex = 
          obtenerErrorValorizacion(detail.getChildNodes());
        if (vex != null) {
          logger.debug("Error de negocio durante invocacion a WS.", 
            vex);
          throw vex;
        }
      }
    }
  }

  private SolicitudException obtenerErrorValorizacion(NodeList nodes)
  {
    if (nodes == null) {
      return null;
    }
    SolicitudException vex = null;
    String message = null;
    String codigo = null;
    for (int i = 0; i < nodes.getLength(); i++) {
      Node item = nodes.item(i);
      vex = obtenerErrorValorizacion(item.getChildNodes());
      if (logger.isDebugEnabled()) {
        logger.debug(i + ") item : " + item);
        logger.debug(i + ") name : " + item.getNodeName());
        logger.debug(i + ") value: " + item.getNodeValue());
        logger.debug(i + ") eie  : " + vex);
      }
      if (vex == null) {
        if ("mensaje".equals(item.getNodeName()))
          message = item.getFirstChild().getNodeValue();
        else if ("codigo".equals(item.getNodeName()))
          codigo = item.getFirstChild().getNodeValue();
      }
      else {
        return vex;
      }
    }
    if ((message != null) && (codigo != null)) {
      logger.debug("Descripcion encontrada. Se crea excepcion.");
      vex = new SolicitudException(message, Integer.valueOf(codigo).intValue());
    }
    return vex;
  }

  public SolicitudCotizacion[] obtenerSolicitudes(Long rutCliente)
    throws SolicitudException
  {
    VSPDAOFactory factory = VSPDAOFactory.getInstance();
    VSPConfigDAO dao = (VSPConfigDAO)factory.getVentaSegurosDAO(VSPConfigDAO.class);

    Map params = new HashMap();
    params.put("SERVICE_ENDPOINT_KEY", dao.getString("cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWS.wsdl.solicitudes"));

    logger.info("WS: Obtener Solicitudes");
    logger.info("Antes de intanciar IMPL.");

    WsBigsaSolicitudes_Impl svc = new WsBigsaSolicitudes_Impl();

    logger.info("Antes de Ininicalizar el servicio.");

    SolicitudVO[] result = (SolicitudVO[])null;
    SolicitudCotizacion[] res = (SolicitudCotizacion[])null;
    try
    {
      WsBigsaSolicitudesImplDelegate ws = 
        (WsBigsaSolicitudesImplDelegate)inicializarWS(svc.getWsBigsaSolicitudesImplPort(), params);

      String sRut = String.valueOf(rutCliente);
      int iRut = Integer.parseInt(sRut);

      result = ws.obtenerListaSolicitudes(iRut, 5);

      if (result != null) {
        res = new SolicitudCotizacion[result.length];
        for (int i = 0; i < result.length; i++) {
          SolicitudVO solicitudVO = result[i];
          try
          {
            res[i] = new SolicitudCotizacion();
            BeanUtils.copyProperties(res[i], solicitudVO);
          } catch (IllegalAccessException e) {
            logger.error("Error al copiar objeto", e);
          } catch (InvocationTargetException e) {
            logger.error("Error al copiar objeto", e);
          }
        }
        Arrays.sort(res);
      }
    }
    catch (RemoteException e) {
      logger.error("error remoto", e);
    }
    catch (cl.cencosud.bigsa.gestion.solicitud.webservice.client.ErrorInternoException e)
    {
      logger.error("error ws", e);
      throw new SystemException(e);
    } finally {
      ResourceLeakUtil.close(dao);
    }

    return res;
  }

  public byte[] obtenerPolizaPdf(int rutCliente, int numeroSolicitud)
    throws BusinessException
  {
    VSPDAOFactory factory = VSPDAOFactory.getInstance();
    VSPConfigDAO dao = (VSPConfigDAO)factory.getVentaSegurosDAO(VSPConfigDAO.class);

    Map params = new HashMap();
    params.put("SERVICE_ENDPOINT_KEY", dao.getString("cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWS.wsdl.poliza.pdf"));

    logger.debug("WS: Obtener PDF");
    logger.debug("Antes de intanciar IMPL.");

    WsBigsaPolizaPDFService_Impl svc = new WsBigsaPolizaPDFService_Impl();

    logger.debug("Antes de Ininicalizar el servicio.");

    byte[] result = (byte[])null;
    try
    {
      WsBigsaPolizaPDFDelegate ws = 
        (WsBigsaPolizaPDFDelegate)inicializarWS(svc.getWsBigsaPolizaPDFPort(), params);

      result = ws.obtenerPolizaPDF(rutCliente, numeroSolicitud);
    }
    catch (RemoteException e) {
      logger.error("error remoto", e);
      throw new ObtenerArchivosException(
        "cl.cencosud.ventaseguros.common.exception.ARCHIVO_NO_ENCONTRADO", 
        new Object[] { "poliza" });
    } catch (cl.cencosud.bigsa.cotizacion.webservice.client.ErrorInternoException e) {
      logger.info(e);
    } finally {
      ResourceLeakUtil.close(dao);
    }

    return result;
  }

  public byte[] obtenerPolizaHtml(int rutCliente, int numeroSolicitud)
    throws BusinessException
  {
    VSPDAOFactory factory = VSPDAOFactory.getInstance();
    VSPConfigDAO dao = (VSPConfigDAO)factory.getVentaSegurosDAO(VSPConfigDAO.class);

    Map params = new HashMap();
    params.put("SERVICE_ENDPOINT_KEY", dao.getString("cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWS.wsdl.poliza.html"));

    logger.debug("WS: Obtener PDF");
    logger.debug("Antes de intanciar IMPL.");

    WsBigsaPolizaHtml_Impl svc = new WsBigsaPolizaHtml_Impl();

    logger.debug("Antes de Ininicalizar el servicio.");

    byte[] result = (byte[])null;
    try
    {
      WsBigsaPolizaHtmlImplDelegate ws = 
        (WsBigsaPolizaHtmlImplDelegate)inicializarWS(svc.getWsBigsaPolizaHtmlImplPort(), params);

      result = ws.obtenerCotizacionHTML(rutCliente, numeroSolicitud);
    } catch (RemoteException e) {
      logger.error("error remoto", e);
      throw new ObtenerArchivosException(
        "cl.cencosud.ventaseguros.common.exception.ARCHIVO_NO_ENCONTRADO", 
        new Object[] { "poliza" });
    } catch (cl.cencosud.bigsa.poliza.html.webservice.client.ErrorInternoException e) {
      logger.error("error ws", e);
    } finally {
      ResourceLeakUtil.close(dao);
    }

    return result;
  }

  public boolean validarSerieRut(String rut, String nroSerie) {
    return true;
  }

  public boolean actualizarFormaPago(int rutCliente, int numeroCotizacion, int codigoFormaPago)
    throws BusinessException
  {
    VSPDAOFactory factory = VSPDAOFactory.getInstance();
    VSPConfigDAO dao = (VSPConfigDAO)factory.getVentaSegurosDAO(VSPConfigDAO.class);

    boolean res = false;

    Map params = new HashMap();
    params.put("SERVICE_ENDPOINT_KEY", dao.getString("cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWS.wsdl.actualizaFormaPago"));

    logger.debug("WS: Actualizar forma de Pago");
    logger.debug("Antes de intanciar IMPL.");

    WsBigsaActulizaFormaPagoService_Impl svc = 
      new WsBigsaActulizaFormaPagoService_Impl();

    logger.debug("Antes de Ininicalizar el servicio.");

    logger.info("rutCliente: " + rutCliente);
    logger.info("numeroCotizacion: " + numeroCotizacion);
    logger.info("codigoFormaPago: " + codigoFormaPago);
    try
    {
      WsBigsaActulizaFormaPagoImplDelegate ws = 
        (WsBigsaActulizaFormaPagoImplDelegate)inicializarWS(svc.getWsBigsaActulizaFormaPagoImplPort(), 
        params);

      res = 
        ws.actualizarFormaPago(rutCliente, numeroCotizacion, 
        codigoFormaPago);

      logger.info("Respuesta: " + res);
    }
    catch (RemoteException e) {
      logger.error("error remoto", e);
      throw new ObtenerArchivosException(
        "cl.cencosud.ventaseguros.common.exception.ARCHIVO_NO_ENCONTRADO", 
        new Object[] { "poliza" });
    } catch (cl.cencosud.bigsa.gestion.formapago.grabar.webservice.client.ErrorInternoException e) {
      logger.error("error ws", e);
      throw new ObtenerArchivosException("ERROR INTERNO", 
        ObtenerArchivosException.SIN_ARGUMENTOS);
    } finally {
      ResourceLeakUtil.close(dao);
    }

    return res;
  }
}