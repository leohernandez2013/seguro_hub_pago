package cl.cencosud.ventaseguros.paginaintermedia.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cl.cencosud.ventaseguros.common.ProductoLeg;
import cl.cencosud.ventaseguros.common.Rama;
import cl.cencosud.ventaseguros.common.Subcategoria;
import cl.cencosud.ventaseguros.common.TipoRama;
import cl.tinet.common.dao.jdbc.BaseDAO;

/**
 * DAO de Pagina intermedia.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 24/08/2010
 */
public interface PaginaIntermediaDAO extends BaseDAO {

    /**
     * Obtiene un listado de ramas de productos.
     * @return Listado de ramas.
     */
    List < Rama > obtenerRamas();

    /**
     * Obtiene un listado de subcategorias de productos.
     * @param idRama Identificador de la rama.
     * @return Listado de subcategorias.
     */
    List < Subcategoria > obtenerSubcategorias(int idRama);
    
    /**
     * Obtiene un listado de subcategorias de productos.
     * @param idRama identificador de la rama.
     * @param idTipo identificador del tipo de subcategoria
     * @return listado de subcategorias.
     */
    List < Subcategoria > obtenerSubcategoriasTipo(int idRama, int idTipo);
    
    /**
     * Obtiene un listado de tipos de rama.
     * @param idRama identificador de la rama.
     * @return listado de tipos de rama.
     */
    List < TipoRama > obtenerTiposRama(int idRama);

    /**
     * Obtiene un lsitado de subcategorias asociadas a una rama.
     * @param idRama Identificador de la rama.
     * @param idSubcategoria Identificador de la subcategoria.
     * @return listado de subcategorias.
     */
    List < Map < String, ? >> obtenerListaPaginaIntermedia(int idRama,
        int idSubcategoria);

    /**
     * Obtiene los datos de una subcatrgoria a partir del id especificado.
     * @param idSubcategoria identificador de subcategoria.
     * @return Subcatogoria.
     */
    public Subcategoria obtenerSubcategoria(int idSubcategoria);

    /**
     * Obtiene un listado de producto que tienen relacion con la subcategoria.
     * @param idSubcategoria Identificador de la subcategoria.
     * @param asignado Si es verdadero, se obtienen los productos asociados a 
     * la subcategoria, si es falso, se obtienen los productos disponibles 
     * para asociar a la subcategoria.
     * @param nombre Nombre del producto a buscar.
     * @return Listado de productos.
     */
    public List < ProductoLeg > obtenerListaProductos(int idSubcategoria,
        boolean asignado, String nombre);

    /**
     * Agrega un producto a un a subcategoria.
     * @param idSubcategoria identificador de subcategoria.
     * @param idProductoLeg identificador de producto legacy.
     * @return identificador de insercion.
     */
    public long agregarProducto(int idSubcategoria, int idProductoLeg);

    public boolean existeProducto(int idSubcategoria, int idProductoLeg);

    public void activarProducto(int idSubcategoria, int idProductoLeg);

    public void eliminarProducto(int idSubcategoria, int idProductoLeg);

    public void modificarSubcategoria(int idSubcategoria, String titulo,
        int estado);

    /**
     * Obtiene listado de Subcategor�as disponibles para la P�gina Intermedia.
     * @param idRama identificador de la rama.
     * @return listado de subcategorias disponibles.
     */
    public List < Subcategoria > obtenerSubcategoriasPaginaIntermedia(int idRama);

    /**
     * Agrega una nueva subcategoria para una rama determinada.
     * @param idRama identificador de la rama.
     * @param subcategoria titulo de la subcategoria.
     * @return identificador de inserci�n.
     */
    public long agregarSubcategoria(int idRama, String subcategoria, int idTipo);
    
    
    /**
     * Obtiene las promociones que se encuentran en el sitio 
     * @param tipo Tipo promocion.
     * @return Listado de promociones disponibles.
     */
	List<HashMap<String, Object>> obtenerPromociones(String tipo);

	List<HashMap<String, Object>> obtenerPromocionesSecundarias(String idRama);

}
