package cl.cencosud.ventaseguros.comparador.dao;

import java.util.LinkedHashMap;
import java.util.List;

import cl.cencosud.ventaseguros.common.PlanLeg;
import cl.cencosud.ventaseguros.comparador.model.Caracteristica;
import cl.tinet.common.dao.jdbc.BaseDAO;
import cl.tinet.common.seguridad.model.UsuarioInterno;

/**
 * DAO de Comparador de Planes.
 * <br/>
 * @author Miguel Cornejo V. (TInet)
 * @version 1.0
 * @created 21/06/2011
 */
public interface ComparadorDAO extends BaseDAO {

	void activarProducto(Long idProducto, int idEstado);
	
	List < LinkedHashMap < String, ? > > obtenerProductos(int idRama, int idSubcategoria, Long idProducto, int idTipo);
	List <PlanLeg> obtenerPlanesPorProducto(int idEstado, Long idProducto);
	List <Caracteristica> obtenerCaracteristicasPorPlan(int idPlan, int tipoCaracteristica, int idEstado);
	List <Caracteristica> obtenerCaracteristicasCompartidas(Long idProducto, int idEstado);
	List <Caracteristica> obtenerCaracteristicasCompartidasFront(Long idProducto, int idEstado);
	
	Integer agregarCaracteristica(UsuarioInterno usuario, Caracteristica caract);
	Integer agregarCaracteristicaCompartida(UsuarioInterno usuario, Caracteristica caract);
	void agregarPlanCaracteristica(Caracteristica caract);
	void agregarProductoCaracteristica(Caracteristica caract);
	
	void actualizarCaracteristica(UsuarioInterno usuario, Caracteristica caract);
	void actualizarCaracteristicaCompartida(UsuarioInterno usuario, Caracteristica caract);
	void actualizarPlanCaracteristica(Caracteristica caract);
	void actualizarProductoCaracteristica(Caracteristica caract);
	void actualizarHabilitadoPorPlan(int idPlan, int habilitado, int tipoCaract);
	
	boolean existePlanCaracteristica(Caracteristica caract);
	boolean existeProductoCaracteristica(Caracteristica caract);
	boolean existeCaracteristica(Caracteristica caract);
	boolean existeCaracteristicaCompartida(Caracteristica caract);
	
	void deshabilitarCaracteristica(UsuarioInterno usuario, Caracteristica caract);
	void deshabilitarCaracteristicaCompartida(UsuarioInterno usuario, Caracteristica caract);
	void deshabilitarPlanCaracteristica(Caracteristica caract);
	void deshabilitarProductoCaracteristica(Caracteristica caract);
	
	void guardarComparadorCaracteristica(String idComprador, long rutCliente, Caracteristica caract);
	void guardarComparadorCaracteristicaCompartida(String idComprador, long rutCliente, Caracteristica caract);
	
	boolean isProductoHabilitado(Long idProducto);
	
	String obtenerDatosEmail(String nombre);
	
}
