package cl.cencosud.ventaseguros.mantenedoretl.dao.ws;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.rpc.Stub;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.cencosud.bigsa.cotizacion.seguros.webservice.client.ErrorInternoException;
import cl.cencosud.bigsa.cotizacion.seguros.webservice.client.PlanesVigentesVO;
import cl.cencosud.bigsa.cotizacion.seguros.webservice.client.ProductoVO;
import cl.cencosud.bigsa.cotizacion.seguros.webservice.client.SubTipoSeguroVO;
import cl.cencosud.bigsa.cotizacion.seguros.webservice.client.TipoSeguroVO;
import cl.cencosud.bigsa.cotizacion.seguros.webservice.client.WsBigsaPlanesImplDelegate;
import cl.cencosud.bigsa.cotizacion.seguros.webservice.client.WsBigsaPlanes_Impl;
import cl.cencosud.bigsa.cotizacion.seguros.webservice.client.WsBigsaProductosImplDelegate;
import cl.cencosud.bigsa.cotizacion.seguros.webservice.client.WsBigsaProductos_Impl;
import cl.cencosud.bigsa.cotizacion.seguros.webservice.client.WsBigsaSubTiposSegurosImplDelegate;
import cl.cencosud.bigsa.cotizacion.seguros.webservice.client.WsBigsaSubTiposSeguros_Impl;
import cl.cencosud.bigsa.cotizacion.seguros.webservice.client.WsBigsaTiposSegurosImplDelegate;
import cl.cencosud.bigsa.cotizacion.seguros.webservice.client.WsBigsaTiposSeguros_Impl;
import cl.cencosud.ventaseguros.asesorvirtual.model.PlanLeg;
import cl.cencosud.ventaseguros.asesorvirtual.model.ProductoLeg;
import cl.cencosud.ventaseguros.asesorvirtual.model.SubtipoLeg;
import cl.cencosud.ventaseguros.asesorvirtual.model.TipoLeg;
import cl.cencosud.ventaseguros.dao.VSPDAOFactory;
import cl.cencosud.ventaseguros.mantenedorclientes.dao.VSPConfigDAO;
import cl.cencosud.ventaseguros.mantenedoretl.dao.MantenedorETLWSDAO;
import cl.tinet.common.config.BaseConfigurable;
import cl.tinet.common.util.resource.ResourceLeakUtil;

import com.tinet.exceptions.system.SystemException;

public class MantenedorETLDAOWS extends BaseConfigurable implements
    MantenedorETLWSDAO {

    private static Log logger = LogFactory.getLog(MantenedorETLDAOWS.class);

    private static final String SERVICE_ENDPOINT_KEY = "SERVICE_ENDPOINT_KEY";

    private static final String WSDL_TIPO_LEG =
        "cl.cencosud.ventaseguros.mantenedoretl.dao.ws.MantenedorETLDAOWS.WSDL.etl.tipoleg";

    private static final String WSDL_SUBTIPO_LEG =
        "cl.cencosud.ventaseguros.mantenedoretl.dao.ws.MantenedorETLDAOWS.WSDL.etl.subtipoleg";

    private static final String WSDL_PRODUCTO_LEG =
        "cl.cencosud.ventaseguros.mantenedoretl.dao.ws.MantenedorETLDAOWS.WSDL.etl.productoleg";

    private static final String WSDL_PLAN_LEG =
        "cl.cencosud.ventaseguros.mantenedoretl.dao.ws.MantenedorETLDAOWS.WSDL.etl.planleg";

    public void close() {
        // TODO Auto-generated method stub

    }

    public Class getDAOInterface() {
        // TODO Auto-generated method stub
        return null;
    }

    public void setDAOInterface(Class interfazDAO) {
        // TODO Auto-generated method stub

    }

    /**
     * TODO Describir m�todo inicializarServicio.
     * @param stub
     * @param param
     * @throws
     */
    private < T > T inicializarWS(T stub, Map < String, ? > param)
        throws SystemException {
        if (stub == null) {
            throw new NullPointerException("Debe especificar un stub no nulo.");
        }
        if (param == null) {
            logger.warn("No se especific� URL destino del servicio.");
        }
        if (param.containsKey(SERVICE_ENDPOINT_KEY)) {
            Object endpoint = param.get(SERVICE_ENDPOINT_KEY);
            if (logger.isDebugEnabled()) {
                logger.debug("Intentando establecer URL destino: " + endpoint);
            }
            try {
                Method method =
                    stub.getClass().getMethod("_setProperty", String.class,
                        Object.class);
                logger.debug("M�todo encontrado. Invocando...");
                method.invoke(stub, Stub.ENDPOINT_ADDRESS_PROPERTY, param
                    .get(SERVICE_ENDPOINT_KEY));
                logger.debug("Propiedad establecida exitosamente.");
            } catch (SecurityException se) {
                throw new SystemException(se);
            } catch (NoSuchMethodException nsme) {
                throw new SystemException(nsme);
            } catch (IllegalArgumentException iae) {
                throw new SystemException(iae);
            } catch (IllegalAccessException iae) {
                throw new SystemException(iae);
            } catch (InvocationTargetException ite) {
                throw new SystemException(ite);
            }
        } else {
            logger.warn("No se especific� URL destino del servicio.");
        }
        return stub;
    }

    /**
     * TODO Describir m�todo obtenerTipoLeg.
     * @return
     */
    public TipoLeg[] obtenerTipoLeg() {
        TipoLeg[] tipos = null;

        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        VSPConfigDAO dao = factory.getVentaSegurosDAO(VSPConfigDAO.class);

        Map < String, String > params = new HashMap < String, String >();
        params.put(SERVICE_ENDPOINT_KEY, dao.getString(WSDL_TIPO_LEG));

        logger.debug("WS: Obtener Tipos LEG");
        logger.debug("Antes de intanciar IMPL.");

        WsBigsaTiposSeguros_Impl svc = new WsBigsaTiposSeguros_Impl();

        // Inicializa el servicio (URL).
        WsBigsaTiposSegurosImplDelegate ws =
            inicializarWS(svc.getWsBigsaTiposSegurosImplPort(), params);

        try {
            TipoSeguroVO[] tiposSeguro = ws.obtenerTiposSeguro("vnp");

            if (tiposSeguro != null) {

                tipos = new TipoLeg[tiposSeguro.length];
                int i = 0;

                for (TipoSeguroVO tipoSeguroVO : tiposSeguro) {
                    TipoLeg tipo = new TipoLeg();
                    tipo.setCodigo_tipo_prod_leg(tipoSeguroVO
                        .getCodigoTipoSeguro()
                        + "");
                    tipo.setNombre(tipoSeguroVO.getDescripcion());

                    tipos[i] = tipo;

                    i++;
                }
            }
        } catch (RemoteException e) {
            logger.error("Error durante invocando WS de obtener tipos leg.", e);
            throw new SystemException(e);
        } catch (ErrorInternoException e) {
            logger.error("Error durante invocando WS de obtener tipos leg.", e);
            throw new SystemException(e);
        } finally {
            ResourceLeakUtil.close(dao);
        }

        return tipos;
    }

    /**
     * TODO Describir m�todo obtenerSubTipoLeg.
     * @param codigo_tipo_prod_leg
     * @return
     */
    public SubtipoLeg[] obtenerSubTipoLeg(String codigo_tipo_prod_leg) {
        SubtipoLeg[] subtipos = null;

        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        VSPConfigDAO dao = factory.getVentaSegurosDAO(VSPConfigDAO.class);

        Map < String, String > params = new HashMap < String, String >();
        params.put(SERVICE_ENDPOINT_KEY, dao.getString(WSDL_SUBTIPO_LEG));

        logger.debug("WS: Obtener Sub Tipos LEG");
        logger.debug("Antes de intanciar IMPL.");

        WsBigsaSubTiposSeguros_Impl svc = new WsBigsaSubTiposSeguros_Impl();

        // Inicializa el servicio (URL).
        WsBigsaSubTiposSegurosImplDelegate ws =
            inicializarWS(svc.getWsBigsaSubTiposSegurosImplPort(), params);

        try {
            Integer tipo = Integer.parseInt(codigo_tipo_prod_leg);

            SubTipoSeguroVO[] subTipoSeguro =
                ws.obtenerSubtiposSeguros("vnp", tipo);

            if (subTipoSeguro != null) {
                subtipos = new SubtipoLeg[subTipoSeguro.length];
                int i = 0;
                for (SubTipoSeguroVO subTipoSeguroVO : subTipoSeguro) {
                    SubtipoLeg subtipoLeg = new SubtipoLeg();
                    subtipoLeg.setCodigo_sub_tipo_prod_leg(subTipoSeguroVO
                        .getCodigoSubTipoSeguro());
                    subtipoLeg.setCodigo_tipo_prod_leg(codigo_tipo_prod_leg);
                    subtipoLeg.setNombre(subTipoSeguroVO.getDescripcion());

                    subtipos[i] = subtipoLeg;
                    i++;
                }
            }
        } catch (RemoteException e) {
            logger.error("Error durante invocando WS de obtener subtipos leg.",
                e);
            throw new SystemException(e);
        } catch (ErrorInternoException e) {
            logger.error("Error durante invocando WS de obtener subtipos leg.",
                e);
            throw new SystemException(e);
        } finally {
            ResourceLeakUtil.close(dao);
        }
        return subtipos;
    }

    /**
     * TODO Describir m�todo obtenerProductoLeg.
     * @param codigo_tipo_prod_leg
     * @param codigo_sub_tipo_prod_leg
     * @return
     */
    public ProductoLeg[] obtenerProductoLeg(String codigo_tipo_prod_leg,
        String codigo_sub_tipo_prod_leg) {
        ProductoLeg[] productos = null;

        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        VSPConfigDAO dao = factory.getVentaSegurosDAO(VSPConfigDAO.class);

        Map < String, String > params = new HashMap < String, String >();
        params.put(SERVICE_ENDPOINT_KEY, dao.getString(WSDL_PRODUCTO_LEG));

        logger.debug("WS: Obtener Productos LEG");
        logger.debug("Antes de intanciar IMPL.");

        WsBigsaProductos_Impl svc = new WsBigsaProductos_Impl();

        // Inicializa el servicio (URL).
        WsBigsaProductosImplDelegate ws =
            inicializarWS(svc.getWsBigsaProductosImplPort(), params);

        try {
            Integer tipo = Integer.parseInt(codigo_tipo_prod_leg);

            ProductoVO[] producto =
                ws.obtenerListaProductos("vnp", tipo, codigo_sub_tipo_prod_leg);

            if (producto != null) {
                productos = new ProductoLeg[producto.length];
                int i = 0;

                for (ProductoVO productoVO : producto) {
                    ProductoLeg productoLeg = new ProductoLeg();
                    productoLeg
                        .setCodigo_sub_tipo_prod_leg(codigo_sub_tipo_prod_leg);
                    productoLeg.setCodigo_tipo_prod_leg(codigo_tipo_prod_leg);
                    productoLeg.setCodigo_producto_leg(productoVO
                        .getCodigoProducto());
                    productoLeg.setNombre(productoVO.getDescripcion());

                    productos[i] = productoLeg;
                    i++;
                }
            }
        } catch (RemoteException e) {
            logger.error(
                "Error durante invocando WS de obtener productos leg.", e);
            throw new SystemException(e);
        } catch (ErrorInternoException e) {
            logger.error(
                "Error durante invocando WS de obtener productos leg.", e);
            throw new SystemException(e);
        } finally {
            ResourceLeakUtil.close(dao);
        }
        return productos;
    }

    /**
     * TODO Describir m�todo obtenerPlanLeg.
     * @param codigo_tipo_prod_leg
     * @param codigo_sub_tipo_prod_leg
     * @param codigo_producto_leg
     * @return
     */
    public PlanLeg[] obtenerPlanLeg(String codigo_tipo_prod_leg,
        String codigo_sub_tipo_prod_leg, String codigo_producto_leg) {
        PlanLeg[] planes = null;

        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        VSPConfigDAO dao = factory.getVentaSegurosDAO(VSPConfigDAO.class);

        Map < String, String > params = new HashMap < String, String >();
        params.put(SERVICE_ENDPOINT_KEY, dao.getString(WSDL_PLAN_LEG));

        logger.debug("WS: Obtener Planes LEG");
        logger.debug("Antes de intanciar IMPL.");

        WsBigsaPlanes_Impl svc = new WsBigsaPlanes_Impl();

        // Inicializa el servicio (URL).
        WsBigsaPlanesImplDelegate ws =
            inicializarWS(svc.getWsBigsaPlanesImplPort(), params);

        try {
            Integer tipo = Integer.parseInt(codigo_tipo_prod_leg);

            PlanesVigentesVO[] planesVigentes =
                ws.obtenerPlanesVigentes("vnp", tipo, codigo_sub_tipo_prod_leg,
                    codigo_producto_leg);

            if (planesVigentes != null) {
                planes = new PlanLeg[planesVigentes.length];
                int i = 0;

                for (PlanesVigentesVO planesVigentesVO : planesVigentes) {
                    PlanLeg plan = new PlanLeg();
                    plan.setCodigo_plan_leg(planesVigentesVO.getCodigoPlan()
                        + "");
                    plan.setCodigo_producto_leg(codigo_producto_leg);
                    plan.setCodigo_sub_tipo_prod_leg(codigo_sub_tipo_prod_leg);
                    plan.setCodigo_tipo_prod_leg(codigo_tipo_prod_leg);
                    plan.setNombre(planesVigentesVO.getDescripcion());

                    planes[i] = plan;
                    i++;
                }
            }
        } catch (RemoteException e) {
            logger.error(
                "Error durante invocando WS de obtener productos leg.", e);
            throw new SystemException(e);
        } catch (ErrorInternoException e) {
            logger.error(
                "Error durante invocando WS de obtener productos leg.", e);
            throw new SystemException(e);
        } finally {
            ResourceLeakUtil.close(dao);
        }
        return planes;
    }

}
