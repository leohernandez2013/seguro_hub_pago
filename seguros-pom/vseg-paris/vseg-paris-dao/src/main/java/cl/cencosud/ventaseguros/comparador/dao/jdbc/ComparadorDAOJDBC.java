package cl.cencosud.ventaseguros.comparador.dao.jdbc;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import cl.cencosud.ventaseguros.common.PlanLeg;
import cl.cencosud.ventaseguros.common.ProductoLeg;
import cl.cencosud.ventaseguros.common.config.VSPSQLConfig;
import cl.cencosud.ventaseguros.comparador.dao.ComparadorDAO;
import cl.cencosud.ventaseguros.comparador.model.Caracteristica;
import cl.tinet.common.dao.jdbc.managed.ManagedBaseDAOJDBC;
import cl.tinet.common.seguridad.model.UsuarioInterno;

public class ComparadorDAOJDBC extends ManagedBaseDAOJDBC implements ComparadorDAO {
	
	private static final String SQL_LISTA_PRODUCTOS = "SQL_LISTA_PRODUCTOS_KEY";
	private static final String SQL_UPDATE_COMPARADOR_ACTIVO = "SQL_UPDATE_COMPARADOR_ACTIVO_KEY";
	private static final String SQL_LISTA_PLANES_POR_PRODUCTO = "SQL_LISTA_PLANES_POR_PRODUCTO_KEY";
	private static final String SQL_LISTA_CARACTERISTICAS_POR_PLAN = "SQL_LISTA_CARACTERISTICAS_POR_PLAN_KEY";
	private static final String SQL_UPDATE_CARACTERISTICA = "SQL_UPDATE_CARACTERISTICA_KEY";
	private static final String SQL_UPDATE_PLAN_CARACTERISTICA = "SQL_UPDATE_PLAN_CARACTERISTICA_KEY";
	private static final String SQL_UPDATE_CARACTERISTICAS_COMPARTIDAS = "SQL_UPDATE_CARACTERISTICAS_COMPARTIDAS_KEY";
	private static final String SQL_UPDATE_PRODUCTO_CARACT_COMP = "SQL_UPDATE_PRODUCTO_CARACT_COMP_KEY";
	private static final String SQL_INSERT_PLAN_CARACTERISTICA = "SQL_INSERT_PLAN_CARACTERISTICA_KEY";
	private static final String SQL_INSERT_CARACTERISTICA = "SQL_INSERT_CARACTERISTICA_KEY";
	private static final String SQL_INSERT_CARACTERISTICA_COMPARTIDA = "SQL_INSERT_CARACTERISTICA_COMPARTIDA_KEY";
	private static final String SQL_INSERT_PRODUCTO_CARACT_COMP = "SQL_INSERT_PRODUCTO_CARACT_COMP_KEY";
	private static final String SQL_INSERT_COMPARADOR_CARACTERISTICA = "SQL_INSERT_COMPARADOR_CARACTERISTICA_KEY";
	private static final String SQL_INSERT_COMPARADOR_CARACTERISTICA_COMPARTIDA = "SQL_INSERT_COMPARADOR_CARACTERISTICA_COMPARTIDA_KEY";
	private static final String SQL_DESHABILITAR_CARACTERISTICA = "SQL_DESHABILITAR_CARACTERISTICA_KEY";
	private static final String SQL_DESHABILITAR_PLAN_CARACTERISTICA = "SQL_DESHABILITAR_PLAN_CARACTERISTICA_KEY";
	private static final String SQL_UPDATE_PLAN_CARACTERISTICA_HABILITADO = "SQL_UPDATE_PLAN_CARACTERISTICA_HABILITADO_KEY";
	private static final String SQL_DESHABILITAR_CARACT_COMP = "SQL_DESHABILITAR_CARACT_COMP_KEY";	
	private static final String SQL_DESHABILITAR_PRODUCTO_CARACT_COMP = "SQL_DESHABILITAR_PRODUCTO_CARACT_COMP_KEY";
	private static final String SQL_LISTA_EXISTE_PLAN_CARACTERISTICA = "SQL_LISTA_EXISTE_PLAN_CARACTERISTICA_KEY";
	private static final String SQL_LISTA_EXISTE_CARACTERISTICA = "SQL_LISTA_EXISTE_CARACTERISTICA_KEY";
	private static final String SQL_LISTA_EXISTE_CARACTERISTICA_COMPARTIDA = "SQL_LISTA_EXISTE_CARACTERISTICA_COMPARTIDA_KEY";
	private static final String SQL_LISTA_EXISTE_PRODUCTO_CARACT_COMP = "SQL_LISTA_EXISTE_PRODUCTO_CARACT_COMP_KEY";
	private static final String SQL_IS_PRODUCTO_HABILITADO = "SQL_IS_PRODUCTO_HABILITADO_KEY";
	private static final String SQL_LISTA_CARACTERISTICAS_COMPARTIDAS = "SQL_LISTA_CARACTERISTICAS_COMPARTIDAS_KEY";
	private static final String SQL_LISTA_CARACTERISTICAS_COMPARTIDAS_FRONT = "SQL_LISTA_CARACTERISTICAS_COMPARTIDAS_FRONT_KEY";
	private static final String SQL_LISTA_CARACTERISTICAS_COMPARTIDAS_COMPARA_DIF = "SQL_LISTA_CARACTERISTICAS_COMPARTIDAS_COMPARA_KEY";
	
	private static final String SQL_OBTENER_DATOS_EMAIL = "SQL_OBTENER_DATOS_EMAIL_KEY";       
	
	@SuppressWarnings("unchecked")
	public List < LinkedHashMap < String, ? > > obtenerProductos(int idRama, int idSubcategoria, Long idProducto, int idTipo) {
		String sql = this.obtenerSQL(SQL_LISTA_PRODUCTOS);
		Object[] params = null;
		
		if (idProducto != -1) {
			sql = sql + " AND PL.ID_PRODUCTO = ? ";
			params = new Object[] { idTipo, idSubcategoria, idProducto};
		} else {
			params = new Object[] { idTipo, idSubcategoria};
		}
		
		sql = sql + " order by PL.NOMBRE";
		
		List < LinkedHashMap < String, ? > > result = (ArrayList < LinkedHashMap < String, ? > >)  this.query(Map.class, sql, params);
        return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<PlanLeg> obtenerPlanesPorProducto(int idEstado, Long idProducto) {
		Object[] params = new Object[] { idEstado, idProducto};
		List <PlanLeg> result = (ArrayList <PlanLeg>)  this.query(PlanLeg.class, this.obtenerSQL(SQL_LISTA_PLANES_POR_PRODUCTO), params);
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Caracteristica> obtenerCaracteristicasPorPlan(int idPlan, int tipoCaracteristica, int estado) {
		Object[] params = new Object[] { estado, tipoCaracteristica, idPlan };
		List <Caracteristica> result = (ArrayList <Caracteristica>)  this.query(Caracteristica.class, this.obtenerSQL(SQL_LISTA_CARACTERISTICAS_POR_PLAN), params);
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Caracteristica> obtenerCaracteristicasCompartidas(
			Long idProducto, int idEstado) {
		
		Object[] params = new Object[] { idProducto, idEstado };
		List <Caracteristica> result = (ArrayList <Caracteristica>)  this.query(Caracteristica.class, this.obtenerSQL(SQL_LISTA_CARACTERISTICAS_COMPARTIDAS), params);
		return result;
		}
	
	
	public List<Caracteristica> obtenerCaracteristicasCompartidasFront(
			Long idProducto, int idEstado) {
		Object[] params = new Object[] { idProducto, idEstado };
		List <Caracteristica> result = (ArrayList <Caracteristica>)  this.query(Caracteristica.class, this.obtenerSQL(SQL_LISTA_CARACTERISTICAS_COMPARTIDAS_FRONT), params);
		return result;
	}
	
	public void activarProducto(Long idProducto, int idEstado) {
		Object[] params = new Object[] {idEstado, idProducto};		
		this.update(this.obtenerSQL(SQL_UPDATE_COMPARADOR_ACTIVO), params);
	}
	
	public void actualizarCaracteristica(UsuarioInterno usuario,
			Caracteristica caract) {
		Object[] params = new Object[] {caract.getDescripcion(), 
										usuario.getUsername(),
										Integer.valueOf(caract.getEs_eliminado()),
										caract.getTooltip(),
										caract.getId_caracteristica(), 
										caract.getTipo() };
		
		this.update(this.obtenerSQL(SQL_UPDATE_CARACTERISTICA), params);
	}

	public void actualizarPlanCaracteristica(Caracteristica caract) {
		Object[] params = new Object[] {caract.getValor(), 
										caract.getEs_eliminado(),
										caract.getHabilitado(),
										caract.getId_plan(), 
										caract.getId_caracteristica()};
		this.update(this.obtenerSQL(SQL_UPDATE_PLAN_CARACTERISTICA), params);
	}

	public Integer agregarCaracteristica(UsuarioInterno usuario,
			Caracteristica caract) {
		Object[] params = new Object[] {caract.getDescripcion(),
										usuario.getUsername(),
										caract.getTipo(),
										caract.getTooltip()};
		Long result = this.insert(this.obtenerSQL(SQL_INSERT_CARACTERISTICA), params);
		return Integer.valueOf(result.toString());
	}

	public void agregarPlanCaracteristica(Caracteristica caract) {
		Object[] params = new Object[] {caract.getId_plan(),
										caract.getId_caracteristica(),
										caract.getValor(),
										caract.getHabilitado()};
		this.insert(this.obtenerSQL(SQL_INSERT_PLAN_CARACTERISTICA), params);
	}

	public void deshabilitarCaracteristica(UsuarioInterno usuario,
			Caracteristica caract) {
		Object[] params = new Object[] {usuario.getUsername(),
										caract.getId_caracteristica(),
										caract.getTipo()};
		this.update(this.obtenerSQL(SQL_DESHABILITAR_CARACTERISTICA), params);
	}

	public void deshabilitarPlanCaracteristica(Caracteristica caract) {
		Object[] params = new Object[] {caract.getId_plan(),						
										caract.getId_caracteristica()};		
		this.update(this.obtenerSQL(SQL_DESHABILITAR_PLAN_CARACTERISTICA), params);
	}
	
	@SuppressWarnings("unchecked")
	public boolean existeCaracteristica(Caracteristica caract) {
		Object[] params = new Object[] {caract.getId_caracteristica()};
		List < Map < String, ? > > result = (ArrayList < Map < String, ? > >)
			this.query(Map.class, this.obtenerSQL(SQL_LISTA_EXISTE_CARACTERISTICA), params);
		
		if (result != null && result.size() > 0) {
			
			for (Map < String, ? > map : result) {
				if (map != null && map.size() > 0) {
					return true;
				}
			}
		}		
		return false;
	}

	@SuppressWarnings("unchecked")
	public boolean existePlanCaracteristica(Caracteristica caract) {
		Object[] params = new Object[] {caract.getId_plan(),						
										caract.getId_caracteristica()};
		List < Map < String, ? > > result = (ArrayList < Map < String, ? > >)
			this.query(Map.class, this.obtenerSQL(SQL_LISTA_EXISTE_PLAN_CARACTERISTICA), params);
		
		if (result != null && result.size() > 0) {
			
			for (Map < String, ? > map : result) {
				if (map != null && map.size() > 0) {
					return true;
				}
			}
		}		
		return false;
	}
	
	public boolean isProductoHabilitado(Long idProducto) {
		Object[] params = new Object[] {idProducto};
		ProductoLeg producto = (ProductoLeg) this.find(ProductoLeg.class, this.obtenerSQL(SQL_IS_PRODUCTO_HABILITADO), params);
		
		if (producto != null && producto.getComparador_activo() == 1) {
			return true;			
		}		
		return false;
	}
	
	public void actualizarCaracteristicaCompartida(UsuarioInterno usuario,
			Caracteristica caract) {
		Object[] params = new Object[] {caract.getDescripcion(), 
										usuario.getUsername(), 
										Integer.valueOf(caract.getEs_eliminado()),
										caract.getTooltip(),
										caract.getId_caracteristica()};
		this.update(this.obtenerSQL(SQL_UPDATE_CARACTERISTICAS_COMPARTIDAS), params);
	}

	public void actualizarProductoCaracteristica(Caracteristica caract) {
		Object[] params = new Object[] {caract.getValor(), 
				caract.getEs_eliminado(), 
				caract.getId_producto(), 
				caract.getId_caracteristica()};
		
		this.update(this.obtenerSQL(SQL_UPDATE_PRODUCTO_CARACT_COMP), params);
	}

	public Integer agregarCaracteristicaCompartida(UsuarioInterno usuario,
			Caracteristica caract) {
		Object[] params = new Object[] {caract.getDescripcion(), usuario.getUsername(), caract.getTooltip()};
		Long result = this.insert(this.obtenerSQL(SQL_INSERT_CARACTERISTICA_COMPARTIDA), params);
		return Integer.valueOf(result.toString());
	}

	public void agregarProductoCaracteristica(Caracteristica caract) {
		Object[] params = new Object[] {caract.getId_producto(),
				caract.getId_caracteristica(),
				caract.getValor()};
		this.insert(this.obtenerSQL(SQL_INSERT_PRODUCTO_CARACT_COMP), params);
	}

	public void deshabilitarCaracteristicaCompartida(UsuarioInterno usuario,
			Caracteristica caract) {
		Object[] params = new Object[] {usuario.getUsername(), caract.getId_caracteristica()};
		this.update(this.obtenerSQL(SQL_DESHABILITAR_CARACT_COMP), params);
	}

	public void deshabilitarProductoCaracteristica(Caracteristica caract) {
		Object[] params = new Object[] {caract.getId_producto(), caract.getId_caracteristica()};
		this.update(this.obtenerSQL(SQL_DESHABILITAR_PRODUCTO_CARACT_COMP), params);
	}

	@SuppressWarnings("unchecked")
	public boolean existeCaracteristicaCompartida(Caracteristica caract) {
		Object[] params = new Object[] {caract.getId_caracteristica()};
		List < Map < String, ? > > result = (ArrayList < Map < String, ? > >)
			this.query(Map.class, this.obtenerSQL(SQL_LISTA_EXISTE_CARACTERISTICA_COMPARTIDA), params);
		
		if (result != null && result.size() > 0) {
			
			for (Map < String, ? > map : result) {
				if (map != null && map.size() > 0) {
					return true;
				}
			}
		}		
		return false;
	}

	@SuppressWarnings("unchecked")
	public boolean existeProductoCaracteristica(Caracteristica caract) {
		Object[] params = new Object[] { caract.getId_producto(), caract.getId_caracteristica() };
		List<Map<String, ?>> result = (ArrayList<Map<String, ?>>) this.query(
				Map.class, this.obtenerSQL(SQL_LISTA_EXISTE_PRODUCTO_CARACT_COMP), params);

		if (result != null && result.size() > 0) {

			for (Map<String, ?> map : result) {
				if (map != null && map.size() > 0) {
					return true;
				}
			}
		}
		return false;
	}
	
	public void actualizarHabilitadoPorPlan(int idPlan, int habilitado, int tipoCaract) {
		Object[] params = new Object[] {habilitado, idPlan, tipoCaract};
		this.update(this.obtenerSQL(SQL_UPDATE_PLAN_CARACTERISTICA_HABILITADO), params);
	}
	
	public void guardarComparadorCaracteristica(String idComprador, long rutCliente, Caracteristica caract) {
		Object[] params = new Object[] {idComprador,
				rutCliente, 
				caract.getId_plan(), 
				caract.getId_caracteristica(), 
				caract.getDescripcion(), 
				caract.getValor(),
				caract.getPrimaMensualPesos(),
				caract.getPrimaMensualUF(),
				caract.getIdPlanCompannia(),
				caract.getTipo()};
		Long idResult = this.insert(this.obtenerSQL(SQL_INSERT_COMPARADOR_CARACTERISTICA), params);
	}
	
	public void guardarComparadorCaracteristicaCompartida(String idComprador, long rutCliente, Caracteristica caract) {
		Object[] params = new Object[] {idComprador,
				rutCliente, 
				caract.getId_producto(), 
				caract.getId_caracteristica(), 
				caract.getDescripcion(), 
				caract.getValor()};
		Long idResult = this.insert(this.obtenerSQL(SQL_INSERT_COMPARADOR_CARACTERISTICA_COMPARTIDA), params);
	}
	
	public String obtenerDatosEmail(String nombre) {
        String res = "";
        Object[] params = new Object[] { "CONFIG_EMAIL", nombre };
        List < Map < String, String >> lres =
            this.query(Map.class, this.obtenerSQL(SQL_OBTENER_DATOS_EMAIL), params);
        if (lres.size() > 0) {
            res = lres.get(0).get("VALOR");
        }
        return res;
    }

	/**
     * M�todo obtenerSQL, permite recuerar un SQL en base a una key 
     * definido en un archivo propiedades.
     * @param sqlKey
     * @return String
     */
    private String obtenerSQL(String sqlKey) {
        return VSPSQLConfig.getInstance().getString(sqlKey,
            this.getLocale());
    }

}
