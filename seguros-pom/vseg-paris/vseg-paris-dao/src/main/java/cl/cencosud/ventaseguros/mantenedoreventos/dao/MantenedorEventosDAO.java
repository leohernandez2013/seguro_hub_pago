package cl.cencosud.ventaseguros.mantenedoreventos.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import cl.tinet.common.dao.jdbc.BaseDAO;

public interface MantenedorEventosDAO extends BaseDAO {

    /**
     * Metodo relacionado con los filtros de buscar.
     *
     * @param filtros
     * @return
     */
    List < Map < String, Object >> buscarEventos(Map < String, Object > filtros);

    /**
     * Metodo relacionado con subir archivo.
     *
     * @param id_evento
     * @param bios
     * @param tipo
     * @return
     */
    boolean subirArchivo(Long id_evento, byte[] bios, String tipo);

    /**
     * Metodo relacionado con crear un nuevo Evento.
     *
     * @param datos
     * @return
     */
    Long nuevoEvento(Map datos);

    /**
     * Metodo relacionado con agregar rut�s del archivo gestionado.
     *
     * @param datos
     * @return
     */
    boolean insertarRutArchivos(Map < String, Object > datos);

    /**
     * Metodo relacionado con Obtener un archivo desde BD Se debe especificar el
     * id del evento, y el tipo.
     * <li> imagen </li>
     * <li> archivo </li>
     *
     * @param id_evento
     * @param tipo
     * @return
     */
    Map < String, Object > obtenerArchivo(String id_evento, String tipo);

    /**
     * Metodo relacionado con la obtencion de un evento.
     *
     * @param idEvento
     * @return
     */
    Map < String, Object > obtenerEvento(Long idEvento);

    /**
     * Metodo relacionado con obtener las posiciones.
     *
     * @return
     */
    List < Map < String, ? >> obtenerPosiciones();

    /**
     * Metodo relacionado con obtener los tipos de paginas.
     *
     * @return
     */
    List < Map < String, ? >> obtenerTipoPagina();

    /**
     * Metodo relacionado con obtener prioridades.
     *
     * @return
     */
    List < Map < String, ? >> obtenerPrioridad();

    /**
     * Metodo relacionado con obtener tipos de eventos.
     *
     * @return
     */
    List < Map < String, ? >> obtenerTipoEvento();

    /**
     * Metodo relacionado con Obtener la cantidad de eventos, por codigo Debe
     * ser <strong>1 </strong>.
     *
     * @param codigo
     * @return
     */
    Integer obtenerCantidadEvento(String codigo);

    /**
     * Metodo relacionado con la actualizacion de un eventos<br>
     * como parametros un map con los datos actualizados.
     *
     * @param datos
     * @return
     */
    boolean actualizarEvento(Map datos);

    /**
     * metodo relacionado con la modificacion de un archivo <br>
     * como parametros: Id evento, y un arreglo de byte del archivo a cargar, y
     * el tipo de archivo.
     * <li> archivo </li>
     * <li> cargaRut </li>
     *
     * @param id_evento
     * @param bios
     * @param tipo
     * @return
     */
    boolean modificarArchivo(Long id_evento, byte[] bios, String tipo);

    /**
     * Metodo relacionado con la modificacion de archivos Rut como parametros:
     * un map con los rut y el id del evento asignado.
     *
     * @param datos
     * @return
     */
    boolean modificarArchivoRut(Map < String, Object > datos);

    /**
     * Metodo relacionado con obtener el maxima prioridad en un tramo de fecha.
     * @param fechaInicioDate
     * @param fechaTerminoDate
     * @return
     */
    Integer obtenerMaxPriori(Date fechaInicioDate, Date fechaTerminoDate);

    /**
     * Metodo relacionado con insertar nueva prioridad.
     * @param sugerencia
     */
    void insertarNuevaPrioridad(Integer sugerencia);

    /**
     * Metodo para obtener conflictos de prioridad.
     * @param prioridad
     * @param fechaInicioDate
     * @param fechaTerminoDate
     * @return
     */
    boolean conflictoPrioridad(Integer prioridad, Date fechaInicioDate,
        Date fechaTerminoDate);

    /**
     * Metodo  para obtener la maxima prioridad de toda la aplicacion.
     * @return
     */
    Integer obtenerMaxPriori();

    /**
     * Metodo relacionado con la obtencion de la prioridad de un evento.
     * @param idEvento
     * @return
     */
    Integer obtenerPrioridadEvento(Long idEvento);

    /**
     * Metodo relacionado con la obtencion de la fecha inicio.
     * @param idEvento
     * @return
     */
    Date obtenerFechaInicio(Long idEvento);

    /**
     * Metodo relacionado con la obtencion de la fecha de termino.
     * @param idEvento
     * @return
     */
    Date obtenerFechaTermino(Long idEvento);

    /**
     * Metodo relacionado con borrar un evento.
     *
     * @param datos
     * @return
     */
    boolean borrarEvento(Long idEvento);

    Integer obtenerOcurrencia(Long idEvento);

    Map buscarRut(Long idEvento, Long rut);

    boolean grabarRut(Long rut, Long idEvento, Integer ocurrencia);

    Long buscarEventoMostrar(Map < String, Object > filtros);

    void verEvento(Long id, Long rut);
}
