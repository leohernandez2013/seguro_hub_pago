package cl.cencosud.ventaseguros.mantenedoretl.dao.ws;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.xml.rpc.ServiceException;
import javax.xml.rpc.Stub;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.cencosud.bigsa.cotizacion.ramos.plan.was.ErrorInternoException;
import cl.cencosud.bigsa.cotizacion.ramos.plan.was.PlanesVigentesVO;
import cl.cencosud.bigsa.cotizacion.ramos.plan.was.WsBigsaPlanes;
import cl.cencosud.bigsa.cotizacion.ramos.plan.was.WsBigsaPlanesImplDelegate;
import cl.cencosud.bigsa.cotizacion.ramos.producto.was.ProductoVO;
import cl.cencosud.bigsa.cotizacion.ramos.producto.was.WsBigsaProductos;
import cl.cencosud.bigsa.cotizacion.ramos.producto.was.WsBigsaProductosImplDelegate;
import cl.cencosud.bigsa.cotizacion.ramos.sutipo.was.SubTipoSeguroVO;
import cl.cencosud.bigsa.cotizacion.ramos.sutipo.was.WsBigsaSubTiposSeguros;
import cl.cencosud.bigsa.cotizacion.ramos.sutipo.was.WsBigsaSubTiposSegurosImplDelegate;
import cl.cencosud.bigsa.cotizacion.ramos.was.TipoSeguroVO;
import cl.cencosud.bigsa.cotizacion.ramos.was.WsBigsaTiposSeguros;
import cl.cencosud.bigsa.cotizacion.ramos.was.WsBigsaTiposSegurosImplDelegate;
import cl.cencosud.ventaseguros.asesorvirtual.model.PlanLeg;
import cl.cencosud.ventaseguros.asesorvirtual.model.ProductoLeg;
import cl.cencosud.ventaseguros.asesorvirtual.model.SubtipoLeg;
import cl.cencosud.ventaseguros.asesorvirtual.model.TipoLeg;
import cl.cencosud.ventaseguros.dao.VSPDAOFactory;
import cl.cencosud.ventaseguros.mantenedorclientes.dao.VSPConfigDAO;
import cl.cencosud.ventaseguros.mantenedoretl.dao.MantenedorETLWSDAO;
import cl.tinet.common.config.BaseConfigurable;
import cl.tinet.common.util.resource.ResourceLeakUtil;

import com.tinet.exceptions.system.SystemException;

public class MantenedorETLDAOWSWAS extends BaseConfigurable implements
    MantenedorETLWSDAO {

    private static Log logger = LogFactory.getLog(MantenedorETLDAOWSWAS.class);

    private static final String SERVICE_ENDPOINT_KEY = "SERVICE_ENDPOINT_KEY";

    private static final String WSDL_TIPO_LEG =
        "cl.cencosud.ventaseguros.mantenedoretl.dao.ws.MantenedorETLDAOWS.WSDL.etl.tipoleg";

    private static final String WSDL_SUBTIPO_LEG =
        "cl.cencosud.ventaseguros.mantenedoretl.dao.ws.MantenedorETLDAOWS.WSDL.etl.subtipoleg";

    private static final String WSDL_PRODUCTO_LEG =
        "cl.cencosud.ventaseguros.mantenedoretl.dao.ws.MantenedorETLDAOWS.WSDL.etl.productoleg";

    private static final String WSDL_PLAN_LEG =
        "cl.cencosud.ventaseguros.mantenedoretl.dao.ws.MantenedorETLDAOWS.WSDL.etl.planleg";

    public void close() {
        // TODO Auto-generated method stub

    }

    public Class getDAOInterface() {
        // TODO Auto-generated method stub
        return null;
    }

    public void setDAOInterface(Class interfazDAO) {
        // TODO Auto-generated method stub

    }

    /**
     * TODO Describir m�todo inicializarServicio.
     * @param stub
     * @param param
     * @throws ExtraccionDatosException 
     */
    private < T > T inicializarWS(T stub, Map < String, ? > param)
        throws SystemException {
        if (stub == null) {
            throw new NullPointerException("Debe especificar un stub no nulo.");
        }
        if (param == null) {
            logger.warn("No se especific� URL destino del servicio.");
        }
        if (param.containsKey(SERVICE_ENDPOINT_KEY)) {
            Object endpoint = param.get(SERVICE_ENDPOINT_KEY);
            if (logger.isDebugEnabled()) {
                logger.debug("Intentando establecer URL destino: " + endpoint);
            }
            try {
                Method method =
                    stub.getClass().getMethod("_setProperty", String.class,
                        Object.class);
                logger.debug("M�todo encontrado. Invocando...");
                method.invoke(stub, Stub.ENDPOINT_ADDRESS_PROPERTY, param
                    .get(SERVICE_ENDPOINT_KEY));
                logger.debug("Propiedad establecida exitosamente.");
            } catch (SecurityException se) {
                throw new SystemException(se);
            } catch (NoSuchMethodException nsme) {
                throw new SystemException(nsme);
            } catch (IllegalArgumentException iae) {
                throw new SystemException(iae);
            } catch (IllegalAccessException iae) {
                throw new SystemException(iae);
            } catch (InvocationTargetException ite) {
                throw new SystemException(ite);
            }
        } else {
            logger.warn("No se especific� URL destino del servicio.");
        }
        return stub;
    }

    /**
     * TODO Describir m�todo obtenerTipoLeg.
     * @return
     */
    public TipoLeg[] obtenerTipoLeg() {
        TipoLeg[] tipos = null;

        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        VSPConfigDAO dao = factory.getVentaSegurosDAO(VSPConfigDAO.class);

        logger.info("WS: Obtener Tipos LEG");
        logger.info("Antes de intanciar IMPL.");

        Context context = null;
        try {
            context = new InitialContext();
            WsBigsaTiposSeguros svc =
                (WsBigsaTiposSeguros) context
                    .lookup("java:comp/env/service/WsBigsaTiposSeguros");

            WsBigsaTiposSegurosImplDelegate ws =
                svc.getWsBigsaTiposSegurosImplPort(new URL(dao
                    .getString(WSDL_TIPO_LEG)));

            TipoSeguroVO[] tiposSeguro = ws.obtenerTiposSeguro("vnp");

            if (tiposSeguro != null) {

                tipos = new TipoLeg[tiposSeguro.length];
                int i = 0;

                for (TipoSeguroVO tipoSeguroVO : tiposSeguro) {
                    TipoLeg tipo = new TipoLeg();
                    tipo.setCodigo_tipo_prod_leg(tipoSeguroVO
                        .getCodigoTipoSeguro()
                        + "");
                    tipo.setNombre(tipoSeguroVO.getDescripcion());

                    tipos[i] = tipo;

                    i++;
                }
            }
        } catch (NamingException e) {
            logger.error("Error durante invocando WS obtenerTipoLeg.", e);
            throw new SystemException(e);
        } catch (cl.cencosud.bigsa.cotizacion.ramos.was.ErrorInternoException e) {
            logger.error("Error durante invocando WS obtenerTipoLeg.", e);
            throw new SystemException(e);
        } catch (RemoteException e) {
            logger.error("error remoto obtenerTipoLeg", e);
            throw new SystemException(e);
        } catch (ServiceException e) {
            logger.error("Error durante invocando WS obtenerTipoLeg.", e);
            throw new SystemException(e);
        } catch (MalformedURLException e) {
            logger.error("Error durante invocando WS obtenerTipoLeg.", e);
            throw new SystemException(e);
        } finally {
            ResourceLeakUtil.close(context);
            ResourceLeakUtil.close(dao);
        }
        return tipos;
    }

    /**
     * TODO Describir m�todo obtenerSubTipoLeg.
     * @param codigo_tipo_prod_leg
     * @return
     */
    public SubtipoLeg[] obtenerSubTipoLeg(String codigo_tipo_prod_leg) {
        SubtipoLeg[] subtipos = null;

        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        VSPConfigDAO dao = factory.getVentaSegurosDAO(VSPConfigDAO.class);

        logger.info("WS: Obtener Sub Tipos LEG");
        logger.info("Antes de intanciar IMPL.");

        Context context = null;
        try {
            context = new InitialContext();
            WsBigsaSubTiposSeguros svc =
                (WsBigsaSubTiposSeguros) context
                    .lookup("java:comp/env/service/WsBigsaSubTiposSeguros");

            WsBigsaSubTiposSegurosImplDelegate ws =
                svc.getWsBigsaSubTiposSegurosImplPort(new URL(dao
                    .getString(WSDL_SUBTIPO_LEG)));

            Integer tipo = Integer.parseInt(codigo_tipo_prod_leg);

            SubTipoSeguroVO[] subTipoSeguro =
                ws.obtenerSubtiposSeguros("vnp", tipo);

            if (subTipoSeguro != null) {
                subtipos = new SubtipoLeg[subTipoSeguro.length];
                int i = 0;
                for (SubTipoSeguroVO subTipoSeguroVO : subTipoSeguro) {
                    SubtipoLeg subtipoLeg = new SubtipoLeg();
                    subtipoLeg.setCodigo_sub_tipo_prod_leg(subTipoSeguroVO
                        .getCodigoSubTipoSeguro());
                    subtipoLeg.setCodigo_tipo_prod_leg(codigo_tipo_prod_leg);
                    subtipoLeg.setNombre(subTipoSeguroVO.getDescripcion());

                    subtipos[i] = subtipoLeg;
                    i++;
                }
            }
        } catch (NamingException e) {
            logger.error("Error durante invocando WS obtenerSubTipoLeg.", e);
            throw new SystemException(e);
        } catch (cl.cencosud.bigsa.cotizacion.ramos.sutipo.was.ErrorInternoException e) {
            logger.error("Error durante invocando WS obtenerSubTipoLeg.", e);
            throw new SystemException(e);
        } catch (RemoteException e) {
            logger.error("error remoto obtenerSubTipoLeg", e);
            throw new SystemException(e);
        } catch (ServiceException e) {
            logger.error("Error durante invocando WS obtenerSubTipoLeg.", e);
            throw new SystemException(e);
        } catch (MalformedURLException e) {
            logger.error("Error durante invocando WS obtenerSubTipoLeg.", e);
            throw new SystemException(e);
        } finally {
            ResourceLeakUtil.close(context);
            ResourceLeakUtil.close(dao);
        }
        return subtipos;
    }

    /**
     * TODO Describir m�todo obtenerProductoLeg.
     * @param codigo_tipo_prod_leg
     * @param codigo_sub_tipo_prod_leg
     * @return
     */
    public ProductoLeg[] obtenerProductoLeg(String codigo_tipo_prod_leg,
        String codigo_sub_tipo_prod_leg) {
        ProductoLeg[] productos = null;

        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        VSPConfigDAO dao = factory.getVentaSegurosDAO(VSPConfigDAO.class);

        logger.info("WS: Obtener Productos LEG");
        logger.info("Antes de intanciar IMPL.");

        Context context = null;
        try {
            context = new InitialContext();
            WsBigsaProductos svc =
                (WsBigsaProductos) context
                    .lookup("java:comp/env/service/WsBigsaProductos");

            WsBigsaProductosImplDelegate ws =
                svc.getWsBigsaProductosImplPort(new URL(dao
                    .getString(WSDL_PRODUCTO_LEG)));

            Integer tipo = Integer.parseInt(codigo_tipo_prod_leg);

            ProductoVO[] producto =
                ws.obtenerListaProductos("vnp", tipo, codigo_sub_tipo_prod_leg);

            if (producto != null) {
                productos = new ProductoLeg[producto.length];
                int i = 0;

                for (ProductoVO productoVO : producto) {
                    ProductoLeg productoLeg = new ProductoLeg();
                    productoLeg
                        .setCodigo_sub_tipo_prod_leg(codigo_sub_tipo_prod_leg);
                    productoLeg.setCodigo_tipo_prod_leg(codigo_tipo_prod_leg);
                    productoLeg.setCodigo_producto_leg(productoVO
                        .getCodigoProducto());
                    productoLeg.setNombre(productoVO.getDescripcion());

                    productos[i] = productoLeg;
                    i++;
                }
            }
        } catch (NamingException e) {
            logger.error("Error durante invocando WS obtenerProductoLeg.", e);
            throw new SystemException(e);
        } catch (cl.cencosud.bigsa.cotizacion.ramos.producto.was.ErrorInternoException e) {
            logger.error("Error durante invocando WS obtenerProductoLeg.", e);
            throw new SystemException(e);
        } catch (RemoteException e) {
            logger.error("error remoto obtenerProductoLeg", e);
            throw new SystemException(e);
        } catch (ServiceException e) {
            logger.error("Error durante invocando WS obtenerProductoLeg.", e);
            throw new SystemException(e);
        } catch (MalformedURLException e) {
            logger.error("Error durante invocando WS obtenerProductoLeg.", e);
            throw new SystemException(e);
        } finally {
            ResourceLeakUtil.close(context);
            ResourceLeakUtil.close(dao);
        }
        return productos;
    }

    /**
     * TODO Describir m�todo obtenerPlanLeg.
     * @param codigo_tipo_prod_leg
     * @param codigo_sub_tipo_prod_leg
     * @param codigo_producto_leg
     * @return
     */
    public PlanLeg[] obtenerPlanLeg(String codigo_tipo_prod_leg,
        String codigo_sub_tipo_prod_leg, String codigo_producto_leg) {
        PlanLeg[] planes = null;

        VSPDAOFactory factory = VSPDAOFactory.getInstance();
        VSPConfigDAO dao = factory.getVentaSegurosDAO(VSPConfigDAO.class);

        logger.info("WS: Obtener Planes LEG");
        logger.info("Antes de intanciar IMPL.");

        Context context = null;
        try {
            context = new InitialContext();
            WsBigsaPlanes svc =
                (WsBigsaPlanes) context
                    .lookup("java:comp/env/service/WsBigsaPlanes");

            WsBigsaPlanesImplDelegate ws =
                svc.getWsBigsaPlanesImplPort(new URL(dao
                    .getString(WSDL_PLAN_LEG)));

            Integer tipo = Integer.parseInt(codigo_tipo_prod_leg);

            PlanesVigentesVO[] planesVigentes =
                ws.obtenerPlanesVigentes("vnp", tipo, codigo_sub_tipo_prod_leg,
                    codigo_producto_leg);

            if (planesVigentes != null) {
                planes = new PlanLeg[planesVigentes.length];
                int i = 0;

                for (PlanesVigentesVO planesVigentesVO : planesVigentes) {
                    PlanLeg plan = new PlanLeg();
                    plan.setCodigo_plan_leg(planesVigentesVO.getCodigoPlan()
                        + "");
                    plan.setCodigo_producto_leg(codigo_producto_leg);
                    plan.setCodigo_sub_tipo_prod_leg(codigo_sub_tipo_prod_leg);
                    plan.setCodigo_tipo_prod_leg(codigo_tipo_prod_leg);
                    plan.setNombre(planesVigentesVO.getDescripcion());

                    planes[i] = plan;
                    i++;
                }
            }
        } catch (NamingException e) {
            logger.error("Error durante invocando WS obtenerPlanLeg.", e);
            throw new SystemException(e);
        } catch (ErrorInternoException e) {
            logger.error("Error durante invocando WS obtenerPlanLeg.", e);
            throw new SystemException(e);
        } catch (RemoteException e) {
            logger.error("error remoto obtenerPlanLeg", e);
            throw new SystemException(e);
        } catch (ServiceException e) {
            logger.error("Error durante invocando WS obtenerPlanLeg.", e);
            throw new SystemException(e);
        } catch (MalformedURLException e) {
            logger.error("Error durante invocando WS obtenerPlanLeg.", e);
            throw new SystemException(e);
        } finally {
            ResourceLeakUtil.close(context);
            ResourceLeakUtil.close(dao);
        }
        return planes;
    }

}
