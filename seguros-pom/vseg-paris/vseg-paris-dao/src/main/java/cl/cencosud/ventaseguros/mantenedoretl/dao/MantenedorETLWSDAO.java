package cl.cencosud.ventaseguros.mantenedoretl.dao;

import cl.cencosud.ventaseguros.asesorvirtual.model.PlanLeg;
import cl.cencosud.ventaseguros.asesorvirtual.model.ProductoLeg;
import cl.cencosud.ventaseguros.asesorvirtual.model.SubtipoLeg;
import cl.cencosud.ventaseguros.asesorvirtual.model.TipoLeg;
import cl.tinet.common.dao.jdbc.BaseDAO;

public interface MantenedorETLWSDAO extends BaseDAO {

    TipoLeg[] obtenerTipoLeg();

    SubtipoLeg[] obtenerSubTipoLeg(String codigo_tipo_prod_leg);

    ProductoLeg[] obtenerProductoLeg(String codigo_tipo_prod_leg,
        String codigo_sub_tipo_prod_leg);

    PlanLeg[] obtenerPlanLeg(String codigo_tipo_prod_leg,
        String codigo_sub_tipo_prod_leg, String codigo_plan_leg);

}
