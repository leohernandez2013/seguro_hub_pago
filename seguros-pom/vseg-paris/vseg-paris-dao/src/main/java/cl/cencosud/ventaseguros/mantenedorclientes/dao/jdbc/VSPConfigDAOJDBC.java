package cl.cencosud.ventaseguros.mantenedorclientes.dao.jdbc;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.cencosud.ventaseguros.mantenedorclientes.dao.VSPConfigDAO;
import cl.tinet.common.dao.jdbc.managed.ManagedBaseDAOJDBC;

/**
 * Dao encargado de implementar los servicios para ACVConfigDAO.
 * <br/>
 * @author fmendoza
 * @version 1.0
 * @created 30/09/2010
 */
public class VSPConfigDAOJDBC extends ManagedBaseDAOJDBC implements
    VSPConfigDAO {

    /**
     * TODO Describir atributo ID_GRUPO.
     */
    private static final String ID_GRUPO = "WSDL";

    /**
     * TODO Describir atributo VALOR.
     */
    private static final String VALOR = "valor";

    /**
     * Logger de la clase
     */
    private static final Log logger = LogFactory.getLog(VSPConfigDAOJDBC.class);

    /**
     * TODO Describir atributo SQL_OBTENER_PARAMETRO.
     */
    private static final String SQL_OBTENER_PARAMETRO =
        "select ID_PARAMETRO, GRUPO, NOMBRE, DESCRIPCION, VALOR "
            + "from PARAMETRO_SISTEMA where GRUPO = ?";

    /**
     * TODO Describir m�todo getString.
     * @param descripcion
     * @return
     */
    public String getString(String descripcion) {
        logger.debug("Obtener parametro para: " + descripcion);
        Object[] params = new Object[] { ID_GRUPO, descripcion };
        String sql = SQL_OBTENER_PARAMETRO;
        sql += " and DESCRIPCION = ? order by ORDEN_LISTADO";

        Map < String, ? > result =
            (Map < String, ? >) this.find(Map.class, sql, params);

        String res = "";

        if (result != null && result.size() > 0) {
            res = (String) result.get(VALOR);
        }

        logger.debug("Parametro obtenido: " + res);

        return res;
    }

    /**
     * TODO Describir m�todo getString.
     * @param grupo
     * @param descripcion
     * @return
     */
    public String getString(String grupo, String descripcion) {
        logger.debug("Obtener parametro para: " + grupo + ", descripcion: "
            + descripcion);
        Object[] params = new Object[] { grupo, descripcion };
        String sql = SQL_OBTENER_PARAMETRO;
        sql += " and DESCRIPCION = ? order by ORDEN_LISTADO";

        Map < String, ? > result =
            (Map < String, ? >) this.find(Map.class, sql, params);

        String res = "";

        if (result != null && result.size() > 0) {
            res = (String) result.get(VALOR);
        }

        logger.debug("Parametro obtenido: " + res);

        return res;
    }

}
