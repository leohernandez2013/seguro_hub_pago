package cl.cencosud.ventaseguros.mantenedorclientes.dao.jdbc;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import cl.cencosud.ventaseguros.asesorvirtual.model.SolicitudCotizacion;
import cl.cencosud.ventaseguros.common.EstadoCivil;
import cl.cencosud.ventaseguros.common.Parametro;
import cl.cencosud.ventaseguros.common.model.Solicitud;
import cl.cencosud.ventaseguros.mantenedorclientes.dao.MantenedorClientesDAO;
import cl.tinet.common.dao.jdbc.managed.ManagedBaseDAOJDBC;
import cl.tinet.common.seguridad.model.UsuarioExterno;

import com.tinet.exceptions.system.SystemException;

/**
 * Encargado de manejar las consultas del mantenedor Usuarios, agregar usuario.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 24/08/2010
 */
public class MantenedorClientesDAOJDBC extends ManagedBaseDAOJDBC implements
    MantenedorClientesDAO {

    private static final String SQL_OBTENER_PARAMETRO =
        "select ID_PARAMETRO, GRUPO, NOMBRE, DESCRIPCION, VALOR "
            + "from PARAMETRO_SISTEMA where GRUPO = ?";

    private static final String SQL_OBTENER_ESTADO_CIVIL =
        "SELECT ID_ESTADO_CIVIL as id, NOMBRE_ESTADO_CIVIL as descripcion "
            + "FROM ESTADO_CIVIL_LEG WHERE ES_ACTIVO = 1";

    private static final String SQL_OBTENER_REGIONES =
        "SELECT ID_REGION AS id, DESCRIPCION_REGION as descripcion "
            + "FROM REGION_LEG WHERE ES_ACTIVO = 1";

    private static final String SQL_OBTENER_COMUNAS =
        "SELECT C.ID_COMUNA as id, C.DESCRIPCION_COMUNA as descripcion "
            + "FROM COMUNA_LEG C, REGION_LEG R " + "WHERE R.ID_REGION = ? "
            + "AND C.CODIGO_REGION_LEG = R.CODIGO_REGION_LEG "
            + "AND R.ES_ACTIVO = 1 AND C.ES_ACTIVO = 1";

    private static final String SQL_OBTENER_CIUDADES =
        "SELECT CI.ID_CIUDAD as id, CI.DESCRIPCION_CIUDAD as descripcion "
            + "FROM COMUNA_LEG C, CIUDAD_LEG CI  WHERE C.ID_COMUNA = ? "
            + "AND C.CODIGO_COMUNA_LEG = CI.CODIGO_COMUNA_LEG AND C.ES_ACTIVO = 1 "
            + "AND CI.ES_ACTIVO = 1";

    private static final String SQL_OBTENER_COMUNA_POR_CIUDAD =
        "SELECT C.ID_COMUNA as id, C.DESCRIPCION_COMUNA as descripcion "
            + "FROM COMUNA_LEG C, CIUDAD_LEG CI  WHERE CI.ID_CIUDAD = ? "
            + "AND C.CODIGO_COMUNA_LEG = CI.CODIGO_COMUNA_LEG AND C.ES_ACTIVO = 1 "
            + "AND CI.ES_ACTIVO = 1";

    private static final String SQL_OBTENER_REGION_POR_COMUNA =
        "SELECT R.ID_REGION AS id, R.DESCRIPCION_REGION as descripcion "
            + "FROM COMUNA_LEG C, REGION_LEG R " + "WHERE C.ID_COMUNA = ? "
            + "AND C.CODIGO_REGION_LEG = R.CODIGO_REGION_LEG "
            + "AND R.ES_ACTIVO = 1 AND C.ES_ACTIVO = 1";

    private static final String ACTUALIZAR_CLIENTE_JDBC_KEY =
        "cl.cencosud.ventaseguros.mantenedorclientes.dao.jdbc."
            + "MantenedorClientesDAOJDBC.actualizarCliente";

    private static final String SQL_ACTUALIZAR_CLIENTE =
        "update usuario_externo set NOMBRE=?, APELLIDO_PATERNO=?, "
            + "APELLIDO_MATERNO=?, TELEFONO_1=?, EMAIL=?, "
            + "NUMERO_DEPARTAMENTO=?, NUMERO=?, CALLE=?, ID_CIUDAD=?, "
            + "FECHA_MODIFICACION=sysdate, FECHA_NACIMIENTO=?, "
            + "ID_TIPO_TELEFONO_1=?, ID_ESTADO_CIVIL=?, ID_SEXO=? "
            + "where RUT_CLIENTE=?";

    private static final String SQL_OBTENER_SOLICITUDES =
        "SELECT s.ID_SOLICITUD, s.ID_PLAN, s.CLIENTE_ES_ASEGURADO, s.ID_RAMA, "
            + "s.ID_PRODUCTO, s.ESTADO_SOLICITUD, s.NRO_TARJETA_REFERENCIA, "
            + "s.FECHA_CREACION, s.FECHA_MODIFICACION, s.ID_PAIS, "
            + "s.RUT_USUARIO, r.DESCRIPCION_RAMA, p.NOMBRE as NOMBRE_PLAN "
            + "FROM SOLICITUD s, RAMA r, PLAN_LEG p "
            + "where s.RUT_USUARIO = ? and s.ESTADO_SOLICITUD IN (1, 2, 3) "
            + "and p.id_plan=s.id_plan and s.id_rama=r.id_rama "
            + "and s.FECHA_CREACION > (select sysdate - 31 from dual)"
            + " order by s.fecha_creacion DESC";

    private static final String SQL_OBTENER_SOLICITUDES_WS =
        "select S.id_solicitud as numeroSolicitud, PC.CODIGO_CIA_LEG as codigoCompania, PC.NOMBRE_CIA_LEG "
            + "as nombreCompania, (select descripcion from PARAMETRO_SISTEMA where valor = S.estado_solicitud and grupo='ESTADO_SOLICITUD') as estado, "
            + "R.TITULO_RAMA as nombreRamo, P.NOMBRE as nombrePlan, S.FECHA_CREACION as fechaSolicitud, null as fechaInicioVigencia, "
            + "null as fechaTerminoVigencia, 'UF' as moneda, 0 as primaMensual, 0 as primaAnual, PR.CODIGO_TIPO_PROD_LEG as codigoTipoProducto, "
            + "PR.CODIGO_SUB_TIPO_PROD_LEG as codigoSubTipoProducto,  "
            + "(select nombre from sub_tipo_producto_leg stpl where  "
            + "                stpl.CODIGO_TIPO_PROD_LEG=PR.CODIGO_TIPO_PROD_LEG  "
            + "                and stpl.CODIGO_SUB_TIPO_PROD_LEG=PR.CODIGO_SUB_TIPO_PROD_LEG) as descripcionSubTipoProducto, "
            + "P.CODIGO_PRODUCTO_LEG as codigoProducto, "
            + "(select nombre from producto_leg PL where  "
            + "                P.CODIGO_PRODUCTO_LEG=PL.CODIGO_PRODUCTO_LEG "
            + "                and P.CODIGO_TIPO_PROD_LEG=PL.CODIGO_TIPO_PROD_LEG  "
            + "                and PL.CODIGO_SUB_TIPO_PROD_LEG=P.CODIGO_SUB_TIPO_PROD_LEG ) as descripcionProducto, "
            + "P.CODIGO_PLAN_LEG as codigoPlan, "
            + "0 as codigoCanal, "
            + "null as nombreCanal "
            + "FROM SOLICITUD S, PLAN_LEG P, PRODUCTO_LEG PR, TIPO_PRODUCTO_LEG TPL, RAMA R, PLAN_COMPANNIA PC "
            + "WHERE S.RUT_USUARIO = ? and S.ID_PLAN=P.ID_PLAN "
            + "and PR.CODIGO_PRODUCTO_LEG = P.CODIGO_PRODUCTO_LEG AND PR.CODIGO_TIPO_PROD_LEG = P.CODIGO_TIPO_PROD_LEG AND PR.CODIGO_SUB_TIPO_PROD_LEG = P.CODIGO_SUB_TIPO_PROD_LEG "
            + "and TPL.CODIGO_TIPO_PROD_LEG = PR.CODIGO_TIPO_PROD_LEG "
            + "AND R.ID_TIPO_PRODUCTO = TPL.ID_TIPO_PRODUCTO "
            + "AND P.CODIGO_TIPO_PROD_LEG = PC.CODIGO_TIPO_PROD_LEG "
            + "AND P.CODIGO_SUB_TIPO_PROD_LEG = PC.CODIGO_SUB_TIPO_PROD_LEG "
            + "AND P.CODIGO_PRODUCTO_LEG = PC.CODIGO_PRODUCTO_LEG "
            + "AND P.CODIGO_PLAN_LEG = PC.CODIGO_PLAN_LEG "
            + "AND S.ESTADO_SOLICITUD = 4 ORDER BY R.ID_RAMA, S.FECHA_CREACION";

    private static final String SQL_OBTENER_CLIENTE =
        "SELECT ID_USUARIO AS idUsuario FROM USUARIO_EXTERNO "
            + "WHERE RUT_CLIENTE = ? AND DV_CLIENTE = ?";

    private static final String SQL_INSERT_USUARIO =
        "INSERT INTO USUARIO(ID_USUARIO, ID_PAIS, "
            + "TIPO_USUARIO, FECHA_CREACION, "
            + "FECHA_MODIFICACION, USERNAME, ES_ACTIVO, "
            + "CANTINTENTOS) VALUES(incremento_usuario.nextval,?,?,"
            + "{fn CURTIME()},{fn CURTIME()},?,?,?)";

    private static final String SQL_INSERT_USUARIO_EXTERNO =
        "INSERT INTO USUARIO_EXTERNO(ID_USUARIO_EXT, ID_USUARIO, "
            + "RUT_CLIENTE, DV_CLIENTE, NOMBRE, APELLIDO_PATERNO, "
            + "APELLIDO_MATERNO, TELEFONO_1, EMAIL, NUMERO_DEPARTAMENTO, "
            + "NUMERO, CALLE, ID_CIUDAD, ID_PAIS, FECHA_CREACION, FECHA_MODIFICACION,"
            + "FECHA_NACIMIENTO, ID_TIPO_TELEFONO_1, "
            + "ID_ESTADO_CIVIL, ID_SEXO) VALUES(incremento_usuario_externo.nextval, "
            + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 1, {fn CURDATE()},{fn CURDATE()}, ?, ?, ?, ?)";

    private static final String SQL_INSERT_PASSWORD_USER_EXT =
        "INSERT INTO PSW(ID_PSW, ID_ESTADO_PSW, ID_USUARIO, PSW, FECHA_CREACION,FECHA_MODIFICACION, ID_PAIS, NUM_INTENTOS) "
            + "VALUES(incremento_psw.nextval, 1, ?, ?, {fn CURDATE()}, {fn CURDATE()}, 1, 0)";

    private static final String SQL_INSERT_USUARIO_ROL =
        "INSERT INTO USUARIO_ROL (ID_ROL, ID_USUARIO, FECHA_CREACION, FECHA_MODIFICACION)"
            + "VALUES(11, ?, {fn CURDATE()}, {fn CURDATE()})";

    private static final String OBTENER_NRO_ORDEN_COMPRA =
        "select numero_orden_compra from transaccion where id_solicitud= ?";

    private static final String SQL_OBTENER_DATOS_EMAIL =
        "SELECT ID_PARAMETRO, GRUPO, GLOBAL, NOMBRE, DESCRIPCION, VALOR, "
            + "ES_EXTERNO, ID_PAIS, FECHA_CREACION, FECHA_MODIFICACION, "
            + "ORDEN_LISTADO FROM PARAMETRO_SISTEMA WHERE GRUPO = ? "
            + "AND NOMBRE= ?";

    private static final String SQL_OBTENER_CUERPO_EMAIL =
        "SELECT ARCHIVO FROM ARCHIVO_EMAIL WHERE TIPO_EMAIL = ?";

    private static final String SQL_OBTENER_COBERTURAS =
        "SELECT ID_COBERTURA, CODIGO_PLAN_LEG, DESCRIPCION, MONTO, FECHA_CREACION, "
            + "FECHA_ACTUALIZACION FROM COBERTURA_LEG WHERE CODIGO_PLAN_LEG = ?"
            + " ORDER BY DESCRIPCION";
    
    private static final String SQL_OBTENER_SUBCATEGORIA =
            "SELECT ID_SUBCATEGORIA, ID_RAMA, TITULO_SUBCATEGORIA "
                + "FROM SUBCATEGORIA WHERE ID_SUBCATEGORIA=?";
    
    private static final String SQL_INSERT_FORMULARIO_COTIZACION =
    		"INSERT INTO FORMULARIO_COTIZADOR(ID_FORMULARIO, RUT, NOMBRE_COMPLETO, TELEFONO, SEGURO, FECHA_CREACION) "
                + "VALUES(SQ_ID_FORMULARIO.nextval, ?, ?, ?, ?, {fn CURDATE()})";

    //CBM
    private static final String SQL_OBTENER_SECUENCIA_DENUNCIAS = "SELECT USR_VSP_ADM.SECUENCIA_DENUNCIAS.nextval from dual";
    
    
    
    /**
     * @see MantenedorClientesDAO#obtenerParametro(String, String)
     */
    public List < Map < String, ? >> obtenerParametro(String idGrupo) {
        Object[] params = new Object[] { idGrupo };
        String sql = SQL_OBTENER_PARAMETRO;
        sql += " order by ORDEN_LISTADO";
        List < Map < String, ? >> result =
            (ArrayList < Map < String, ? >>) this.query(Map.class, sql, params);
        return result;
    }

    /**
     * @see MantenedorClientesDAO#obtenerParametro(String,String)
     */
    public List < Map < String, ? >> obtenerParametro(String idGrupo,
        String valorParametro) {
        Object[] params = new Object[] { idGrupo, valorParametro };
        String sql = SQL_OBTENER_PARAMETRO;
        sql += " and valor = ? order by ORDEN_LISTADO";
        List < Map < String, ? >> result =
            (ArrayList < Map < String, ? >>) this.query(Map.class, sql, params);
        return result;
    }

    /**
     * @see MantenedorClientesDAO#obtenerEstadosCiviles()
     */
    public EstadoCivil[] obtenerEstadosCiviles() {
        Object[] params = new Object[] {};
        List < EstadoCivil > result =
            (ArrayList < EstadoCivil >) this.query(EstadoCivil.class,
                SQL_OBTENER_ESTADO_CIVIL, params);
        return result.toArray(new EstadoCivil[result.size()]);
    }

    /**
     * @see MantenedorClientesDAO#obtenerComunas(String)
     */
    public Parametro[] obtenerComunas(String idRegion) {
        Object[] params = new Object[] { idRegion };
        List < Parametro > result =
            (ArrayList < Parametro >) this.query(Parametro.class,
                SQL_OBTENER_COMUNAS, params);
        return result.toArray(new Parametro[result.size()]);
    }

    /**
     * @see MantenedorClientesDAO#obtenerRegiones()
     */
    public Parametro[] obtenerRegiones() {
        Object[] params = new Object[] {};
        List < Parametro > result =
            (ArrayList < Parametro >) this.query(Parametro.class,
                SQL_OBTENER_REGIONES, params);
        return result.toArray(new Parametro[result.size()]);
    }

    /**
     * @see MantenedorClientesDAO#obtenerCiudades(String)
     */
    public Parametro[] obtenerCiudades(String idComuna) {
        Object[] params = new Object[] { idComuna };
        List < Parametro > result =
            (ArrayList < Parametro >) this.query(Parametro.class,
                SQL_OBTENER_CIUDADES, params);
        return result.toArray(new Parametro[result.size()]);
    }

    /**
     * @see MantenedorClientesDAO#obtenerComunaPorCiudad(String)
     */
    public Parametro[] obtenerComunaPorCiudad(String idCiudad) {
        Object[] params = new Object[] { idCiudad };
        List < Parametro > result =
            (ArrayList < Parametro >) this.query(Parametro.class,
                SQL_OBTENER_COMUNA_POR_CIUDAD, params);
        return result.toArray(new Parametro[result.size()]);
    }

    /**
     * @see MantenedorClientesDAO#obtenerRegionPorComuna(String)
     */
    public Parametro[] obtenerRegionPorComuna(String idComuna) {
        Object[] params = new Object[] { idComuna };
        List < Parametro > result =
            (ArrayList < Parametro >) this.query(Parametro.class,
                SQL_OBTENER_REGION_POR_COMUNA, params);
        return result.toArray(new Parametro[result.size()]);
    }

    /* (non-Javadoc)
     * @see cl.cencosud.ventaseguros.mantenedorclientes.dao.MantenedorClientesDAO#actualizarCliente(java.util.Map)
     */
    public void actualizarCliente(Map < String, String > datos) {
        Connection connection = this.getConnection(ACTUALIZAR_CLIENTE_JDBC_KEY);

        Calendar cal = new GregorianCalendar();
        int year = Integer.valueOf(datos.get("anyoFechaNacimiento")).intValue();
        int month = Integer.valueOf(datos.get("mesFechaNacimiento")).intValue();
        int date = Integer.valueOf(datos.get("diaFechaNacimiento")).intValue();
        cal.set(year, month - 1, date);
        Date fechaNacimiento = cal.getTime();

        Object[] params =
            new Object[] { datos.get("nombre"), datos.get("apellido_paterno"),
                datos.get("apellido_materno"), datos.get("telefono_1"),
                datos.get("email"), datos.get("numero_departamento"),
                datos.get("numero"), datos.get("calle"),
                datos.get("id_ciudad"), fechaNacimiento,
                datos.get("tipo_telefono_1"), datos.get("estado_civil"),
                datos.get("sexo"), datos.get("rut_cliente") };
        this.update(connection, SQL_ACTUALIZAR_CLIENTE, params);
    }

    /**
     * Obtener solicitudes de un usuario.
     * @param rutUsuario rut del usuario
     * @return listado de solicitudes.
     */
    public List < Solicitud > obtenerSolicitudes(Long rutUsuario) {
        Object[] params = new Object[] { rutUsuario };

        return this.query(Solicitud.class, SQL_OBTENER_SOLICITUDES, params);
    }

    /**
     * obtenerSolicitudesWS.
     * @param rutCliente rut del cliente
     * @return listado de solicitudes.
     */
    public SolicitudCotizacion[] obtenerSolicitudesWS(Long rutCliente) {
        Object[] params = new Object[] { rutCliente };

        List < SolicitudCotizacion > solicitudes =
            this.query(SolicitudCotizacion.class, SQL_OBTENER_SOLICITUDES_WS,
                params);

        return (SolicitudCotizacion[]) solicitudes
            .toArray(new SolicitudCotizacion[0]);
    }

    public long registrarUsuario(UsuarioExterno usuario) {
        Connection connection = this.getConnection(ACTUALIZAR_CLIENTE_JDBC_KEY);
        //long idCliente = this.incrementarId(connection, "incremento_usuario");
        String userName =
            usuario.getRut_cliente() + "-" + usuario.getDv_cliente();
        Object[] params = new Object[] { 1, 1, userName, 1, 0 };

        return this.insert(connection, SQL_INSERT_USUARIO, params);
    }

    public long registrarUsuarioExterno(UsuarioExterno usuario) {

        long id = -1;

        Connection connection = this.getConnection(ACTUALIZAR_CLIENTE_JDBC_KEY);
        Object[] params =
            new Object[] { usuario.getIdUsuario(), usuario.getRut_cliente(),
                usuario.getDv_cliente(), usuario.getNombre(),
                usuario.getApellido_paterno(), usuario.getApellido_materno(),
                usuario.getTelefono_1(), usuario.getEmail(),
                usuario.getNumero_departamento(), usuario.getNumero(),
                usuario.getCalle(), usuario.getId_ciudad(),
                usuario.getFecha_nacimiento(), usuario.getTipo_telefono_1(),
                usuario.getEstado_civil(), usuario.getSexo() };
        return this.insert(connection, SQL_INSERT_USUARIO_EXTERNO, params);
    }

    public UsuarioExterno getCliente(String rutCliente, String dv) {
        UsuarioExterno usuario = null;
        Connection connection = this.getConnection(ACTUALIZAR_CLIENTE_JDBC_KEY);
        Object[] params = new Object[] { rutCliente, dv };
        return (UsuarioExterno) this.find(connection, UsuarioExterno.class,
            SQL_OBTENER_CLIENTE, params);

    }

    public long registrarClaveUsuarioExterno(long idUsuario, String password) {
        Connection connection = this.getConnection(ACTUALIZAR_CLIENTE_JDBC_KEY);
        Object[] params = new Object[] { idUsuario, password };
        return this.insert(connection, SQL_INSERT_PASSWORD_USER_EXT, params);
    }

    public void actualizarRolUsuario(long idUsuario) {
        Connection connection = this.getConnection(ACTUALIZAR_CLIENTE_JDBC_KEY);
        Object[] params = new Object[] { idUsuario };
        this.insertWithoutGeneratedKey(connection, SQL_INSERT_USUARIO_ROL,
            params);
    }

    /**
     * TODO Describir m�todo obtenerNroOrdenCompra.
     * @param idSolicitud
     * @return
     */
    public int obtenerNroOrdenCompra(String idSolicitud) {
        int res = -1;
        Object params[] = new Object[] { Integer.valueOf(idSolicitud) };
        Map mnum = (Map) this.find(Map.class, OBTENER_NRO_ORDEN_COMPRA, params);

        if (mnum.size() > 0) {
            res = Integer.valueOf((String) mnum.get("numero_orden_compra"));
        }

        return res;
    }

    /**
     * TODO Describir m�todo obtenerDatosEmail.
     * @param nombre
     * @return
     */
    public String obtenerDatosEmail(String nombre) {
        String res = "";
        Object[] params = new Object[] { "CONFIG_EMAIL", nombre };
        List < Map < String, String >> lres =
            this.query(Map.class, SQL_OBTENER_DATOS_EMAIL, params);
        if (lres.size() > 0) {
            res = lres.get(0).get("VALOR");
        }
        return res;
    }

    /**
     * TODO Describir m�todo obtenerCuerpoEmail.
     * @param nombre
     * @return
     */
    public InputStream obtenerCuerpoEmail(String nombre) {

        InputStream is = null;

        Object[] params = new Object[] { nombre };
        Map < String, Object > mRes =
            (Map < String, Object >) this.find(Map.class,
                SQL_OBTENER_CUERPO_EMAIL, params);

        if (mRes != null && mRes.get("ARCHIVO") != null) {
            try {
                Blob archivo = (Blob) mRes.get("ARCHIVO");

                is = archivo.getBinaryStream();

            } catch (SQLException e) {
                throw new SystemException(e);
            }

        }
        return is;
    }

    /**
     * TODO Describir m�todo obtenerCoberturas.
     * @param idPlan
     * @return
     */
    public List < Map < String, Object > > obtenerCoberturas(int idPlan) {
        Object[] params = new Object[] { idPlan };

        List < Map < String, Object > > lista =
            this.query(Map.class, SQL_OBTENER_COBERTURAS, params);

        return lista;
    }
    
    /**
     * TODO Describir m�todo obtenerSubcategoria.
     * @param idSubcategoria
     * @return
     */
    public String obtenerSubcategoria(String idSubcategoria) {
        String res = "";
        Object[] params = new Object[] { idSubcategoria };
        List < Map < String, String >> lres =
            this.query(Map.class, SQL_OBTENER_SUBCATEGORIA, params);
        if (lres.size() > 0) {
            res = lres.get(0).get("TITULO_SUBCATEGORIA");
        }
        return res;
    }
    
    /**
     * Guarda datos del formulario de cotizacion.
     * @param rut 
     * @param nombre
     * @param telefono
     * @param descSubcategoria
     */
    public void registrarCotizacion(String rut, String nombre, String telefono, String descSubcategoria) {
        Object[] params = new Object[] { rut, nombre, telefono, descSubcategoria };
        this.insert( SQL_INSERT_FORMULARIO_COTIZACION, params);
    }
    
    //CBM 
    
    public String generarSecuenciaDenuncias(){
    	   String Folio = this.obtieneSecuencia(SQL_OBTENER_SECUENCIA_DENUNCIAS);
           return Folio;
    
    }
    
}
