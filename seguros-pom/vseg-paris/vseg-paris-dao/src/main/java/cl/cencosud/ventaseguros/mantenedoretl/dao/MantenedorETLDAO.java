package cl.cencosud.ventaseguros.mantenedoretl.dao;

import cl.cencosud.ventaseguros.asesorvirtual.model.PlanLeg;
import cl.cencosud.ventaseguros.asesorvirtual.model.ProductoLeg;
import cl.tinet.common.dao.jdbc.BaseDAO;

public interface MantenedorETLDAO extends BaseDAO {

    long enviarPlan(PlanLeg plan) throws Exception;

    PlanLeg obtenerDatosPlan(long idPlanLeg);

    void actualizarDatosPlan(PlanLeg plan) throws Exception;

    long enviarProducto(ProductoLeg producto) throws Exception;

    ProductoLeg obtenerDatosProducto(long idProductoLeg);

    void actualizarDatosProducto(ProductoLeg producto) throws Exception;
}
