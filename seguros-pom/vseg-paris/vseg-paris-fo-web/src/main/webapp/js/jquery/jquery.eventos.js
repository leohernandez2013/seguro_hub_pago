/**** MANEJO DE EVENTOS.
 * 
 * 
*/

var opciones_corredera = {interval: true, intervaltime: 5000, display: 3};

//Definir posicion
;(function( $ ){
	
	$.fn.evento = function(posicion, usuario, rama) {
		var url = "javascript:alert('NO HAY URL');";
		var imagen = "images/banner/banner-que-seguro-m-c.jpg";
		var destino = this;
		
		//Limpiar destino
		this.html('');

		//Obtener evento con ajax y json. Se envia posicion, usuario, rama y se obtiene datos del evento.
		$.getJSON("/vseg-paris/obtener-evento.do",{posicion: posicion, usuario:usuario, rama:rama, ajax: 'true'}, function(j){
			if(j!=null) {
				url = "/vseg-paris/ver-evento.do?id="+j.id_evento;   //j.url;
				imagen = '/vseg-paris/obtener-evento.do?img=' + j.id_evento + '&x=img.jpg';
				
				//Generar html para incrustar en posicion
				try {
					var myA = $('<a></a>');
					myA.attr("onfocus", "javascript:blur()");
					
					if(j.tipo_pagina==1) {
						//Levantar un popup
						myA.attr("href", url);
						myA.colorbox({width:"80%", height:"80%", iframe:true});
					} else {
						//Abrir en la misma ventana
						url = 'javascript:top.location.href="'+url+'";return false;';
						myA.attr("href", "#");
						myA.attr("onclick", url);
					}
	
					var myImg = $('<img></img>')
					myImg.attr("alt","prueba");
	
					myImg.attr("width", $(destino).width());
					if($(destino).height() > 0 ) {
						myImg.attr("height", $(destino).height());
					}
					myImg.attr("border", 0);
					myImg.attr("src", imagen);
					
					myImg.hover(function() {
						$(destino).stop().fadeTo(10,0.85);
					}, function() {
						$(destino).stop().fadeTo(10,1);
					});
					
					myA.append(myImg);
					
					destino.html(myA);
	
				} catch(e) {
					alert(e.message);
				}
			} else {
				if( posicion.indexOf("CORREDERA")>=0 ){
					//Eliminar el nodo sin banner.
					destino.parent().remove();
					//Reorganizar la corredera.
					$('#slider-code').tinycarousel(opciones_corredera);
				} else {
					destino.remove();					
				}
			}
		})
	}
	
	
	$.fn.evento_corredera = function(usuario) {
		
		var carousel	= 	'<div id="slider-code">';
			carousel	+=	'	<a class="buttons prev" href="#">left</a>';
			carousel	+=	'	<div class="viewport">';
			carousel	+=	'		<ul class="overview">';
			carousel	+=	'			<li><div id="CORREDERA_VEHICULO"></div></li>';
			carousel	+=	'			<li><div id="CORREDERA_HOGAR"></div></li>';
			carousel	+=	'			<li><div id="CORREDERA_VIDA"></div></li>';
			carousel	+=	'			<li><div id="CORREDERA_SALUD"></div></li>';
			carousel	+=	'			<li><div id="CORREDERA_CESANTIA"></div></li>';
			carousel	+=	'			<li><div id="CORREDERA_FRAUDE"></div></li>';
			carousel	+=	'		</ul>';
			carousel	+=	'	</div>';
			carousel	+=	'	<a class="buttons next" href="#">right</a>';
			carousel	+=	'</div>';
		
		this.html(carousel);
		
		//Setear evento para corredera.
		$("#CORREDERA_VEHICULO").evento("CORREDERA_VEHICULO", null);
		$("#CORREDERA_HOGAR").evento("CORREDERA_HOGAR", null);
		$("#CORREDERA_VIDA").evento("CORREDERA_VIDA", null);
		$("#CORREDERA_SALUD").evento("CORREDERA_SALUD", null);
		$("#CORREDERA_CESANTIA").evento("CORREDERA_CESANTIA", null);
		$("#CORREDERA_FRAUDE").evento("CORREDERA_FRAUDE", null);

		//Definir corredera.
		$('#slider-code').tinycarousel(opciones_corredera);
		
		
	}
})( jQuery );