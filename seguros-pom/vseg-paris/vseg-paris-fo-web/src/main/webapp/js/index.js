function cargarSubcategorias(){
				
		var rama1 = $('select#rama').val();
		if (rama1 == 1){
			$('select#subcategoria').val('22','Full Servicio');
		}	else if (rama1 == 6){
			$('select#subcategoria').empty();
			$('select#subcategoria').append(new Option("Seleccione","0"));
			$('select#subcategoria').append(new Option("Seguro de Viajes","221"));
			$('select#subcategoria').append(new Option("Seguro Obligatorio Mercosur","36"));
		}else if (rama1 == 3){
			$('select#subcategoria').empty();
			$('select#subcategoria').append(new Option("Seleccione","0"));
			$('select#subcategoria').append(new Option("Seguro de Mascota","140"));
			
		}
		
        	if($('select#rama').val() == 9){
				//location.href= '/soap';	
				//$('select#subcategoria').hide();
				$('select#subcategoria').attr('disabled','disabled');
				$('select#subcategoria').html('<option value="0" selected="selected">Seleccione</option>');
        	}else{
				$('select#subcategoria').removeAttr('disabled');
				//$('select#subcategoria').show();
				if (rama1 != 6 && rama1 !=3){
				var url = '/vseg-paris/obtener-subcategorias-cotizador.do';
				var params = '?idRama=' + $('select#rama').val();
				$('select#subcategoria option').remove();
				$('select#subcategoria').html('<option value="0" selected="selected">Seleccione</option>');
				$.getJSON(url + params, function(data){
					//alert(data);
					var options = "";
					if ($('select#rama').val()!= 0){
					options += '<option value="0">Seleccione</option>';
					for (var i=0;i<data.length;i++){

					//alert(data[i].id_subcategoria)

					options += '<option value="'+data[i].id_subcategoria+'">'+data[i].titulo_subcategoria+'</option>';
					}
					$('select#subcategoria').html(options);
					}else {
						$('select#subcategoria').html('<option value="0" selected="selected">Seleccione</option>');
					}
				});
				$('select#subcategoria').trigger('click');
        	}
		}
}

$(document).ready(function(){
	$('#cotizar').click(function(){
		//_gaq.push(['_trackEvent', 'Boton', 'Click', 'Cotiza',0,true]);
		var rama = $('select#rama').val();
		var subcategoria = $('select#subcategoria').val();
		//$('#AtajoCotizadorForm').submit();
		if(rama==9){
			url = 'https://www.seguroscencosud.cl/soap/home.do';
			location.href = url ;
			$(this).attr('href', url);
		}else if(rama==0 || subcategoria ==0){
			alert('Favor seleccionar una rama y una subcategoria');
		}else{
			
			if(subcategoria == 221){
				url = '/cotizador/desplegar-cotizacion.do?idRama=6&idSubcategoria=221';
			}else if(subcategoria == 36){
				url = '/cotizador/desplegar-cotizacion.do?idRama=1&idSubcategoria=36';
			}else if(subcategoria == 140){
				url = '/cotizador/desplegar-cotizacion.do?idRama=6&idSubcategoria=140';
			}else{
				url = '/cotizador/desplegar-cotizacion.do?idRama='+rama+'&idSubcategoria='+subcategoria;
			}
			location.href = url ;
			$(this).attr('href', url);
			
		}
	});
}) ;
