<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<!DOCTYPE html>
<html lang="en">
<head>
<%@ include file="../home/includes/head.jsp"%>
<script src="/vseg-paris/js/formularios.js"></script>
<script src="/vseg-paris/js/cotizacion.js"></script>
<script src='https://www.google.com/recaptcha/api.js?hl=es'></script>
<!-- inicio lightbox -->

<!--<link href="/vseg-paris/css/lightbox/lity.css" rel="stylesheet"/>
<link href="/vseg-paris/css/lightbox/lightbox_Denuncias.css" rel="stylesheet"/>
<script src="/vseg-paris/js/lightbox/lity.min.js"></script>-->

<script type="text/javascript">
<logic:notPresent name="errorCaptcha">
	$(function () {
	  $('.btn_lightbox').trigger("click");
	})
</logic:notPresent>
</script>

<script type="text/javascript">
<logic:notPresent name="errorCaptcha">
	$(document).ready(function(e) {
		$("#inline").click(function(e) {
			$('.lity').hide()
		});
	});
</logic:notPresent>
</script>

</head>
<body>


<!-- Fin lightbox -->
	
	<%@ include file="/google-analytics/google-analytics.jsp"%>
	<%@ include file="/home/includes/header.jsp"%>
	<!--<div class="container contenedor-sitio">
		<div class="row">
			<ol class="breadcrumb hidden-xs">
				<li><a href="javascript: void(0);">Home</a>
				</li>
				<li class="active">Denuncias Ley N� 20.393</li>
			</ol>
			<h4 class="titulo-cotizacion">Denuncias Ley N� 20.393</h4>
			<p>Ingresa informaci�n relevante a cualquier hecho, circunstancia
				o situaci�n irregular que digan relaci�n con Lavado de Activos,
				financiamiento del terrorismo;cohecho y receptaci�n. Recuerda que la informaci�n
				ingresada ser� tratada en forma confidencial.</p>
		</div>
	</div>-->
<main role="main">
      <div class="remodal-bg">
        <section class="o-breadcrumb">
          <div class="container">
            <div class="row">
              <div class="col-12"><a class="o-breadcrumb__link" href="#">Inicio</a><a class="o-breadcrumb__link o-breadcrumb__link--current" href="#">Denuncias Ley N� 20.393</a></div>
            </div>
          </div>
        </section>
        <section class="container">
          <div class="row">
            <div class="col-12">
              <h1 class="o-title o-title--primary">Denuncias Ley N� 20.393</h1>
            </div>
            <div class="col-12">
              <section class="o-box o-box--quotation u-mt20 u-mb40">
				<div class="o-alert o-alert--info u-mtb20">
                              <div class="o-alert__content">
                                <h2 class="o-title o-title--subtitle"> A CONTINUACI�N TE SOLICITAMOS INGRESAR EXCLUSIVAMENTE DENUNCIAS RELACIONADAS A LOS SIGUIENTES DELITOS</h2>
 <p class="o text">
	 Cohecho a funcionario p�blico, nacional o extranjero; lavado de activo o financiamiento del terrorismo; receptaci�n, cometidos por alg�n accionista, director, funcionario o proveedor de CAT Administradora de Tarjetas y sus filiales, en el ejercicio de sus funciones, y en beneficio de CAT Administradora de Tarjetas y/o sus filiales. </p></div></div>
                 
                   <p class="o text">Ingresa informaci�n relevante a cualquier hecho, circunstancia o situaci�n irregular que digan relaci�n con Lavado de Activos, financiamiento del terrorismo; cohecho y receptaci�n. Recuerda que la informaci�n ingresada ser� tratada en forma confidencial.</p>
               
                
                <form class="o-form o-form--standard" id="denunciasForm" name="denunciasForm" id="denunciasForm" enctype="multipart/form-data" method="post" action="/vseg-paris/contactoDenuncias.do?donde=enviar">
                
                <div id="Ley20393">
                 <h2 class="o-title o-title--subtitle">1.- Detalle de la Denuncia</h2>
                <div class="row u-mb50">
                    <div class="col-lg-12">
                      <div class="o-form__field">
                        <label class="o-form__label">Denuncias</label>
                        <textarea class="form-control" style="width: 100%" id="detalleDenuncia" name="detalleDenuncia" maxlength="1000" rows="10" cols="0"></textarea><span class="o-form__message"></span>
                      </div>
                    </div> </div>
                
                <h2 class="o-title o-title--subtitle">2.- DATOS PERSONALES</h2>
                   
                    
                      <div class="row u-mb50">
                  
                    <div class="col-lg-4">
                      <div class="o-form__field">
                        <label class="o-form__label">RUT</label>
                        <input class="o-form__input" placeholder="Rut" type="text" placeholder="Ej: 1234567-9" required="" name="rutDenuncia" id="rutDenuncia" maxlength="12"><span class="o-form__message"></span>
                      </div>
                    </div> 
                    
                           </div>
                         <div class="row u-mb50">
                    <div class="col-lg-4">
                      <div class="o-form__field">
                        <label class="o-form__label">Nombres</label>
                        <input class="o-form__input" type="text" placeholder="Ingrese su nombre" required="" name="nombreDenuncia" id="nombreDenuncia"><span class="o-form__line"><span class="o-form__message"> </span>
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="o-form__field">
                        <label class="o-form__label">Apellido paterno</label>
                        <input class="o-form__input" type="text" placeholder="Ingrese su apellido paterno" required="" name="apePatDenuncia" id="apePatDenuncia"><span class="o-form__line"><span class="o-form__message"></span>
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="o-form__field">
                        <label class="o-form__label">Apellido materno</label>
                        <input class="o-form__input" type="text" placeholder="Ingrese su apellido materno" required="" name="apeMatDenuncia" id="apeMatDenuncia"><span class="o-form__line"><span class="o-form__message"></span>
                      </div>
                    </div>
                  </div>
                     <h2 class="o-title o-title--subtitle">3.- DATOS DEL CONTACTO</h2>
                      <div class="row u-mb50">
                    <div class="col-lg-4">
                      <div class="o-form__field">
                        <label class="o-form__label">Direcci�n</label>
                        <input class="o-form__input" type="text" placeholder="Ingrese su direcci�n" required="" name="direccionDenuncia" id="direccionDenuncia"><span class="o-form__line"><span class="o-form__message"> </span>
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="o-form__field">
                        <label class="o-form__label">Comuna</label>
                        <input class="o-form__input" type="text" placeholder="Ingrese su comuna" required="" name="comunaDenuncia" id="comunaDenuncia"><span class="o-form__line"><span class="o-form__message"></span>
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="o-form__field">
                        <label class="o-form__label">Ciudad</label>
                        <input class="o-form__input" type="text" placeholder="Ingrese su ciudad" required="" name="ciudadDenuncia" id="ciudadDenuncia"><span class="o-form__line"><span class="o-form__message"></span>
                      </div>
                    </div>
                  </div>
                   <div class="row u-mb50">
                     <div class="col-lg-4">
                      <div class="o-form__field">
                        <label class="o-form__label">Tel�fono fijo</label>
                        <input class="o-form__input" type="text" placeholder="00 0000 00 00" required="" name="telefonoFijoDenuncia" id="telefonoFijoDenuncia" onkeypress="return isNumberKey(event)"><span class="o-form__line"><span class="o-form__message"></span>
                      </div>
                    </div>
                          <div class="col-lg-4">
                      <div class="o-form__field">
                        <label class="o-form__label">Tel�fono celular</label>
                        <input class="o-form__input" type="text" placeholder="00 0000 00 00" required="" name="telefonoCelularDenuncia" id="telefonoCelularDenuncia" onkeypress="return isNumberKey(event)"><span class="o-form__line"><span class="o-form__message"></span>
                        
                      </div>
                    </div>
                   
                    
                  </div>
                  <div class="row u-mb50">
                    
                    <div class="col-lg-12">
                      <div class="o-form__field">
                        <div class="g-recaptcha" data-sitekey="6Ld9ARUTAAAAAAkrHBRvrB_eTcMhJVDJBmDY5goB"></div>
                      </div>
                    </div>
                  </div>
                  <div class="row o-actions o-actions--flex">
                    <div class="col-lg-3">
                      <div class="o-form__field is-last"><a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="/vseg-paris/index.jsp"><i class="mx-arrow-left"></i> Volver</a></div>
                    </div>
                    <div class="col-lg-3 offset-lg-6">
                      <div class="o-form__field is-last">
                        <button class="o-btn o-btn--primary o-btn o-btn--icon" id="send_form_quotation_step2" name="Enviar" type="button" onclick="enviar()">Enviar<i class="o-icon">
                            <svg width="13px" height="14px" viewbox="0 0 13 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                              <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <g id="botones/orangeButton+rightArrow" transform="translate(-113.000000, -19.000000)" fill-rule="nonzero" fill="#FFFFFF">
                                  <g id="orangeButton+rightArrow">
                                    <g id="arrows" transform="translate(113.000000, 19.000000)">
                                      <path id="Shape" d="M12.6595914,6.18869213 L6.92029216,0.341285393 C6.69713064,0.113730337 6.42956532,0 6.11805938,0 C5.81235867,0 5.54791211,0.113730337 5.32456532,0.341285393 L4.66341805,1.01492135 C4.44004038,1.23052135 4.32838242,1.49998202 4.32838242,1.82336629 C4.32838242,2.14668764 4.44004038,2.41614831 4.66341805,2.63181124 L7.24652732,5.27252584 L1.03990736,5.27252584 C0.734361045,5.27252584 0.486033254,5.38477753 0.295016627,5.60937528 C0.104,5.83397303 0.00849168646,6.10494382 0.00849168646,6.42222472 L0.00849168646,7.57198652 C0.00849168646,7.8893618 0.104030879,8.1602382 0.295016627,8.38483596 C0.486002375,8.60943371 0.734361045,8.72159101 1.03990736,8.72159101 L7.2464038,8.72159101 L4.66332542,11.3535281 C4.43994774,11.5809258 4.32828979,11.8534382 4.32828979,12.1709079 C4.32828979,12.4883146 4.43994774,12.7607955 4.66332542,12.9881933 L5.32447268,13.6618292 C5.5537791,13.8834067 5.81822565,13.9941798 6.11796675,13.9941798 C6.42363658,13.9941798 6.69114014,13.8834067 6.9202304,13.6618292 L12.6594988,7.81442247 C12.8769786,7.59284494 12.9857957,7.32045843 12.9857957,6.9970427 C12.9857957,6.66774382 12.8769786,6.3981573 12.6595914,6.18869213 Z"></path>
                                    </g>
                                  </g>
                                </g>
                              </g>
                            </svg></i></button>
                      </div>
                    </div>
                  </div>
                  </div>
                  <div class="row">
						<div class="col-xs-12 col-sm-4">
							<span class="pull-left small">Todos los campos con * son obligatorios</span>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12 col-sm-4">
							<div class="alert alert-warning" id="incomplete-Ley20393" style="display: none">
								<p align="center" id="incomplete-text-Ley20393"></p>
							</div>
						</div>
					</div>
                </form>
              </section>
            </div>
          </div>
        </section>
      </div>
    </main>



	<div class="container">

		<!-- INICIO FORM OLD -->

		<!-- <form name="denunciasForm" id="denunciasForm" novalidate="novalidate"
			autocomplete="off" enctype="multipart/form-data" method="post"
			action="/vseg-paris/contactoDenuncias.do?donde=enviar">
		-->

			<!-- 
			<div id="Ley20393">


				<div class="col-lg-12">
					<h4>1.- Detalle de la Denuncia</h4>
				</div>

				<div class="col-lg-12">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="nombre" class="hidden-xs">Denuncias*</label>
								<textarea class="form-control" style="width: 100%"
									id="detalleDenuncia" name="detalleDenuncia" maxlength="1000"
									rows="10" cols="0"></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-12">
					<h4>2.- DATOS PERSONALES</h4>
				</div>
				<div class="col-lg-12">
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label for="nombre" class="hidden-xs">Rut</label> <input
									type="text" name="rutDenuncia" maxlength="10" size="16"
									onkeyup="autocompletarrut(this)"
									onkeypress="return isRutKey(event)" id="rutDenuncia"
									class="form-control datos-rut-input optional" placeholder="Rut">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label for="nombre" class="hidden-xs">Nombre</label> <input
									type="text" name="nombreDenuncia" class="form-control optional"
									id="nombreDenuncia" maxlength="50" placeholder="Nombre">
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label for="nombre" class="hidden-xs">Apellido Paterno</label> <input
									type="text" name="apePatDenuncia" class="form-control optional"
									id="apePatDenuncia" maxlength="50"
									placeholder="Apellido Paterno">
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label for="nombre" class="hidden-xs">Apellido Materno</label> <input
									type="text" name="apeMatDenuncia" class="form-control optional"
									id="apeMatDenuncia" maxlength="50"
									placeholder="Apellido Materno">
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-12">
					<h4>3.- DATOS DEL CONTACTO</h4>
				</div>

				<div class="col-lg-12">
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label for="nombre" class="hidden-xs">Direcci�n</label> <input
									type="text" name="direccionDenuncia" maxlength="30" size="16"
									id="direccionDenuncia" class="form-control optional"
									placeholder="Direcci�n">
							</div>
						</div>

						<div class="col-sm-4">
							<div class="form-group">
								<label for="nombre" class="hidden-xs">Comuna</label> <input
									type="text" name="comunaDenuncia" maxlength="30" size="16"
									id="comunaDenuncia" class="form-control optional"
									placeholder="Comuna">
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label for="nombre" class="hidden-xs">Ciudad</label> <input
									type="text" name="ciudadDenuncia" maxlength="30" size="16"
									id="ciudadDenuncia" class="form-control optional"
									placeholder="Ciudad">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label for="nombre" class="hidden-xs">Tel�fono Fijo</label> <input
									type="text" name="telefonoFijoDenuncia"
									onkeypress="return isNumberKey(event)"
									class="form-control optional" id="telefonoFijoDenuncia"
									maxlength="10" placeholder="Tel�fono Fijo">
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label for="nombre" class="hidden-xs">Tel�fono Celular</label> <input
									type="text" name="telefonoCelularDenuncia"
									onkeypress="return isNumberKey(event)"
									class="form-control optional" id="telefonoCelularDenuncia"
									maxlength="10" placeholder="Tel�fono Celular">
							</div>
						</div>
						<div class="col-sm-4" >
								<div class="form-group">
								    <!-- test <div class="g-recaptcha" data-sitekey="6LdOYScTAAAAAEG_L0pnI3r3JSJB6C3ASBifqyes"></div>-->
									<!-- Produccion-->  <!--  <div class="g-recaptcha" data-sitekey="6Ld9ARUTAAAAAAkrHBRvrB_eTcMhJVDJBmDY5goB"></div> 
								</div>-->
								<!-- Error Captcha -->
						<!-- </div>
					</div> -->
					<!-- 
					<div class="row">
						<div class="col-xs-12 col-sm-4">
							<span class="pull-left small">Todos los campos con * son obligatorios</span>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12 col-sm-4">
							<div class="alert alert-warning" id="incomplete-Ley20393" style="display: none">
								<p align="center" id="incomplete-text-Ley20393"></p>
							</div>
						</div>
					</div>

					<div class="row top15">
						<div class="hidden-xs">
							<div class="col-md-3">
								<a id="volver_denuncias" href="/vseg-paris/index.jsp"
									type="button" class="btn btn-primary">Volver</a>
							</div>
						</div>

						<div class="col-md-3 col-md-offset-6">
							<button type="button"
								class="btn btn-primary btn-block pull-right" onclick="enviar();"
								name="Enviar">ENVIAR</button>
						</div>
 						
					</div>
				</div>

			</div>

		</form>
		-->
		<!-- FIN FORM OLD -->
		<script>
			

			function enviar() {			
				if (validateInput("Ley20393")) {
					document.getElementById('denunciasForm').action = "/vseg-paris/contactoDenuncias.do?donde=enviar";
					document.getElementById('denunciasForm').method = "post";
					document.getElementById('denunciasForm').submit();
				}
			}

			function validateInput(id) {
				validacionInput = 0;
				console.log('in validate input: ' + id);
				$('#' + id + '')
						.find(":input:not(:hidden,:button,:submit)")
						.each(
								function() {
									console.log('testing ' + this.id);
									var optional = $(this).hasClass("optional");
									if (optional == false) {
										$(this).css("border-color", "#cecece");
										if (!this.value) {
											
											
											$(this).css("border-color", "red");
											$(this).focus();
											console.log(this.id+ ' est� vac�o');
											validacionInput = 1;
											return false;
										}
									}

									if ($(this).attr("rut")) {
										if (this.value) {
											console.log("testing rut");
											var result = validaRut(this.value);
											if (result == false) {
												$("#incomplete-text-" + id)
														.html(
																"El rut tiene el formato incorrecto");
												$("#incomplete-" + id).show(
														"fast");
												$(this).css("border-color",
														"red");
												$(this).focus();
												console
														.log(this.id
																+ ' formato incorrecto');
												validacionInput = 1;
												return false;
											}
										}
									}
								});
				if (validacionInput == 0) {
					$("#incomplete-text-" + id).html("");
					$("#incomplete-" + id).hide("fast");
					return true;
				}else{
					return false;
				}
			}
		</script>

	</div>
	<%@ include file="/home/includes/footer.jsp"%>
</body>
</html>