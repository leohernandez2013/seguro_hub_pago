<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<!DOCTYPE html>
<html lang="en">
  <head>
  	<%@ include file="/home/includes/head.jsp"%>
  </head>
  <body>
    <%@ include file="/google-analytics/google-analytics.jsp"%>
	<%@ include file="/home/includes/header.jsp"%> 
 <!-- NEW CONTENIDO -->
 <main role="main">
      <div class="remodal-bg">
        <!-- Migas de pan-->
        <section class="o-breadcrumb">
          <div class="container">
            <div class="row">
              <div class="col-12"><a class="o-breadcrumb__link" href="/vseg-paris/index.jsp">Inicio</a><a class="o-breadcrumb__link o-breadcrumb__link--current" href="#">Denuncias Ley N� 20.393</a></div>
            </div>
          </div>
        </section>
        <!-- Contrataci�n-->
        <section class="container">
          <div class="row">
            <div class="col-12">
              <h1 class="o-title o-title--primary">Denuncias Ley N� 20.393</h1>
            </div>
            <div class="col-12">
              <section class="o-box o-box--message u-text-center u-mtb50"><img class="u-image" src="img/check_success_big.png" alt="">
                
				  <h1 class="o-title o-title--secundary">�Felicitaciones!</h1>
                <p class="o-text u-mb20">Tu denuncia de Lavado de Activos, financiamiento del terrorismo o coecho ha sido recibida por el banco.</p>
				  <small class="o-text ">Registro de denuncia n�mero</small>
                <h2 class="o-title o-title--subtitle o-title--primary"><bean:write name="denunciasForm" property="folioDenuncia" /></h2>
                
				  
				  
                <div class="o-alert o-alert--info u-mtb20">
                  <div class="o-alert__content">
                    <p class="o-alert__text u-text-center">Con este dato puedes consultar el estado de tu denuncia o cualquier duda llamando al <strong>6006 700 500.</strong></p>
                  </div>
                </div><a class="o-btn o-btn--primary" href="/vseg-paris/index.jsp">Volver al Inicio</a>
              </section>
            </div>
          </div>
        </section>
 <!-- FIN NEW CONTENIDO -->
 <%@ include file="/home/includes/footer.jsp"%> 
  </body>
 </html>