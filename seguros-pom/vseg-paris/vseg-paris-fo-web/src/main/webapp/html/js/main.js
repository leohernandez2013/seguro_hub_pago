//!     jQuery.rut.js
//		Permission is hereby granted, free of charge, to any person obtaining a copy
//		of this software and associated documentation files (the "Software"), to deal
//		in the Software without restriction, including without limitation the rights
//		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//		copies of the Software, and to permit persons to whom the Software is
//		furnished to do so, subject to the following conditions:

//		The above copyright notice and this permission notice shall be included in
//		all copies or substantial portions of the Software.

//		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//		THE SOFTWARE.

//		Para obtener este programa bajo otra licencia, póngase en
//		contacto con @pablomarambio en Twitter.
;(function($){
	var defaults = {
		validateOn: 'blur',
		formatOn: 'blur',
		ignoreControlKeys: true,
		useThousandsSeparator: true,
		minimumLength: 2
	};

	//private methods
	function clearFormat(value) {
		return value.replace(/[\.\-]/g, "");
	}

	function format(value, useThousandsSeparator) {
		var rutAndDv = splitRutAndDv(value);
		var cRut = rutAndDv[0]; var cDv = rutAndDv[1];
		if(!(cRut && cDv)) { return cRut || value; }
		var rutF = "";
		var thousandsSeparator = useThousandsSeparator ? "." : "";
		while(cRut.length > 3) {
			rutF = thousandsSeparator + cRut.substr(cRut.length - 3) + rutF;
			cRut = cRut.substring(0, cRut.length - 3);
		}
		return cRut + rutF + "-" + cDv;
	}

	function isControlKey(e) {
		return e.type && e.type.match(/^key(up|down|press)/) &&
			(
				e.keyCode ===  8 || // del
				e.keyCode === 16 || // shift
				e.keyCode === 17 || // ctrl
				e.keyCode === 18 || // alt
				e.keyCode === 20 || // caps lock
				e.keyCode === 27 || // esc
				e.keyCode === 37 || // arrow
				e.keyCode === 38 || // arrow
				e.keyCode === 39 || // arrow
				e.keyCode === 40 || // arrow
				e.keyCode === 91    // command
			);
	}

	function isValidRut(rut, options) {
		if(typeof(rut) !== 'string') { return false; }
		var cRut = clearFormat(rut);
		// validar por largo mínimo, sin guiones ni puntos:
		// x.xxx.xxx-x
		if ( typeof options.minimumLength === 'boolean' ) {
			if ( options.minimumLength && cRut.length < defaults.minimumLength ) {
				return false;
			}
		} else {
			var minLength = parseInt( options.minimumLength, 10 );
			if ( cRut.length < minLength ) {
				return false;
			}
		}
		var cDv = cRut.charAt(cRut.length - 1).toUpperCase();
		var nRut = parseInt(cRut.substr(0, cRut.length - 1));
		if(isNaN(nRut)){ return false; }
		return computeDv(nRut).toString().toUpperCase() === cDv;
	}

	function computeDv(rut) {
		var suma	= 0;
		var mul		= 2;
		if(typeof(rut) !== 'number') { return; }
		rut = rut.toString();
		for(var i=rut.length -1;i >= 0;i--) {
			suma = suma + rut.charAt(i) * mul;
			mul = ( mul + 1 ) % 8 || 2;
		}
		switch(suma % 11) {
			case 1	: return 'k';
			case 0	: return 0;
			default	: return 11 - (suma % 11);
		}
	}

	function formatInput($input, useThousandsSeparator) {
		$input.val(format($input.val(), useThousandsSeparator));
	}

	function validateInput($input) {
		if(isValidRut($input.val(), $input.opts)) {
			$input.trigger('rutValido', splitRutAndDv($input.val()));
		} else {
			$input.trigger('rutInvalido');
		}
	}

	function splitRutAndDv(rut) {
		var cValue = clearFormat(rut);
		if(cValue.length === 0) { return [null, null]; }
		if(cValue.length === 1) { return [cValue, null]; }
		var cDv = cValue.charAt(cValue.length - 1);
		var cRut = cValue.substring(0, cValue.length - 1);
		return [cRut, cDv];
	}

	// public methods
	var methods = {
		init: function(options) {
			if (this.length > 1) {
				/* Valida multiples objetos a la vez */
				for (var i = 0; i < this.length; i++) {
					console.log(this[i]);
					$(this[i]).rut(options);
				}
			} else {
				var that = this;
				that.opts = $.extend({}, defaults, options);
				that.opts.formatOn && that.on(that.opts.formatOn, function(e) {
					if(that.opts.ignoreControlKeys && isControlKey(e)) { return; }
					formatInput(that, that.opts.useThousandsSeparator);
				});
				that.opts.validateOn && that.on(that.opts.validateOn, function() {
					validateInput(that);
				});
			}
			return this;
		}
	};

	$.fn.rut = function(methodOrOptions) {
		if(methods[methodOrOptions]) {
			return methods[methodOrOptions].apply(this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error("El método " + methodOrOptions + " no existe en jQuery.rut");
		}
	};

	$.formatRut = function (rut, useThousandsSeparator) {
		if(useThousandsSeparator===undefined) { useThousandsSeparator = true; }
		return format(rut, useThousandsSeparator);
	};

	$.computeDv = function(rut){
		var cleanRut = clearFormat(rut);
		return computeDv( parseInt(cleanRut, 10) );
	};

	$.validateRut = function(rut, fn, options) {
		options = options || {};
		if(isValidRut(rut, options)) {
			var rd = splitRutAndDv(rut);
			$.isFunction(fn) && fn(rd[0], rd[1]);
			return true;
		} else {
			return false;
		}
	};
})(jQuery);

$(document).ready(function() {

	function goTo(target){
		$('html, body').animate({scrollTop: $(target).offset().top}, 600);
	}

	// Menu mobile
	$('body').on('click', '#menu-mobile', function(event) {
		event.preventDefault();
		if($(this).attr('state') == 1) {
			$(this).attr('state', 0);
			$('.c-nav').removeClass('is-active');
		} else {
			$(this).attr('state', 1);
			$('.c-nav').addClass('is-active');
		}
	});

	// Cierra el menu
	$('#close_menu').on('click',function(event) {
		event.preventDefault();
		$('#menu-mobile').trigger('click');
	}); 
		 
	$('body').on('click','.is_mobile .js-nav-mobile',function(event) {
		event.preventDefault();
		$(this).toggleClass('is-active');
		$(this).parent().find('.c-nav__sub').toggle();
		 
	}); 
		 
	// Detecta si es mobile
	if( window.matchMedia('(max-width:992px)').matches) {
  		 $('.c-nav').addClass('is_mobile');
  	}


	// Detecta si es mobile
	var consulta = window.matchMedia('(max-width:992px)');
	consulta.addListener(mediaquery);
	function mediaquery(){
		if(consulta.matches){
			$('.c-nav').addClass('is_mobile'); 
		}else{
			 $('.c-nav').removeClass('is_mobile'); 
			 $('.c-nav__sub').show(); 
		}
	}

	// slider ofertas offer
	$('#offer-slick').slick({
		centerMode: true,
		centerPadding: '0px',
		arrows: false,
		slidesToShow: 3,
		dots: false,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '0px',
					slidesToShow: 2
				}
			},
			{
				breakpoint: 768,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '40px',
					slidesToShow: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '40px',
					slidesToShow: 1
				}
			}
		]
	});
	// slider reason
	$('#reason-slick').slick({
		centerMode: true,
		centerPadding: '0px',
		arrows: false,
		slidesToShow: 3,
		dots: false,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					arrows: false,
					dots: true,
					centerMode: true,
					centerPadding: '0px',
					slidesToShow: 2
				}
			},
			{
				breakpoint: 768,
				settings: {
					arrows: false,
					dots: true,
					centerMode: true,
					centerPadding: '0px',
					slidesToShow: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					arrows: false,
					dots: true,
					centerMode: true,
					centerPadding: '00px',
					slidesToShow: 1
				}
			}
		]
	});

	// function toogle
	$('body').on('click', '.js-toogle', function(event) {
		event.preventDefault();
		if($(this).attr('state') == 1) {
			$(this).parents('.o-list').removeClass('is-active');
			$(this).attr('state', 0);
		} else {
			$('.js-toogle').attr('state', 0);
			$('.o-list').removeClass('is-active');
			$(this).parents('.o-list').addClass('is-active');
			$(this).attr('state', 1);
		}
	});

	// Slider
	$('.slider').slick({
		dots: false,
		infinite: true,
		speed: 500,
		slidesToShow: 1,
		slidesToScroll: 1,
				responsive: [
				{
					breakpoint: 992,
					settings: {
						arrows: true,
						dots: false,
						centerMode: true,
						centerPadding: '0px',
						slidesToShow: 1
					}
				},
				{
					breakpoint: 768,
					settings: {
						arrows: true,
						dots: false,
						centerMode: true,
						centerPadding: '0px',
						slidesToShow: 1
					}
				},
				{
					breakpoint: 480,
					settings: {
						arrows: true,
						dots: false,
						centerMode: false,
						centerPadding: '0px',
						slidesToShow: 1
					}
				}
			]
	});

	// Format rut
	if ($('#form_quotation_step2').length) {
		$('input[name="rut"]').rut({formatOn: 'keyup'});
	}

	// Collapse
	// menu mobile
	$('.o-collapse').on('click', '.o-collapse__button', function(event) {
		event.preventDefault();
		if($(this).attr('state') == 1) {
			$(this).removeClass('is-active');
			$(this).next('.o-collapse__content').removeClass('is-active');
			$(this).attr('state', 0);
		} else {
			$(this).addClass('is-active');
			$(this).next('.o-collapse__content').addClass('is-active');
			$(this).attr('state', 1);
		}
	});

	$('body').on('click', '.js-collapse', function(event) {
		 $(this).toggleClass('is_active');
		 $(this).next().slideToggle();
	});
});
// Muestra y esconde tabla comparativa
$('.o-table.o-table--comparative').on('click', '#show_cover', function(event) {
		event.preventDefault();
		if($(this).attr('state') == 1) {
			$(this).parents('.o-table').find('.o-table__row.is-visible').addClass('is-hidden');
			$(this).parents('.o-table').find('.o-table__row.is-hidden').removeClass('is-visible');
			$(this).html('Ver más coberturas');
			$(this).attr('state', 0);
		} else {
			$(this).parents('.o-table').find('.o-table__row.is-hidden').addClass('is-visible');
			$(this).parents('.o-table').find('.o-table__row.is-hidden').removeClass('is-hidden');
			$(this).html('Ver menos coberturas');
			$(this).attr('state', 1);
		}
	});
$('.o-box.o-box--plan').on('click', '.js-show_cover', function(event) {
		event.preventDefault();
		if($(this).attr('state') == 1) {
			$(this).parents('.o-box.o-box--plan').find('.o-box__body').removeClass('is-active');
			$(this).removeClass('is-active');
			$(this).attr('state', 0);
		} else {
			$(this).parents('.o-box.o-box--plan').find('.o-box__body').addClass('is-active');
			$(this).addClass('is-active');
			$(this).attr('state', 1);
		}
	});

// tabs

// select tabs

$('.tabgroup > div').hide();
$('.tabgroup > div:first-of-type').show();
$('.tabs a').click(function(e){
  e.preventDefault();
    var $this = $(this),
        tabgroup = '#'+$this.parents('.tabs').data('tabgroup'),
        others = $this.closest('li').siblings().children('a'),
        target = $this.attr('href');
    others.removeClass('active');
    $this.addClass('active');
    $(tabgroup).children('div').hide();
    $(target).show();
});
// Muestra y esconde los tabs
$('#tabselect').on('change', function() {
	if ( this.value == '#tab1')
	{
		$("#tab1").show();
	}
	else
	{
		$("#tab1").hide();
	}
	if ( this.value == '#tab2')
	{
		$("#tab2").show();
	}
	else
	{
		$("#tab2").hide();
	}
	if ( this.value == '#tab3')
	{
		$("#tab3").show();
	}
	else
	{
		$("#tab3").hide();
	}
	if ( this.value == '#tab4')
	{
		$("#tab4").show();
	}
	else
	{
		$("#tab4").hide();
	}
	if ( this.value == '#tab5')
	{
		$("#tab5").show();
	}
	else
	{
		$("#tab5").hide();
	}
});

// Compare
$('.c-result .o-checkbox__input').each(function(index,element){
	$(element).change(function() {
		var checked = $('.c-result .o-checkbox__input:checked').length;
		if(this.checked) {
			if (checked === 1) {
				$(this).parents('.o-checkbox').addClass('is-checked');
				$(this).parents('.o-checkbox').append(tooltip01);
			}
			if (checked >= 2 && checked <= 4) {
				$('#tooltip01').remove();
				$('#tooltip02').remove();
				$(this).parents('.o-checkbox').addClass('is-checked');
				$(this).parents('.o-checkbox').append(tooltip02);
			}
		}else{
			$(this).parents('.o-checkbox').removeClass('is-checked');
			$('#tooltip02').remove();
			// $('#tooltip01').remove();
		}
		if ($("#tooltip01").length){
			setTimeout(function(){
				$('#tooltip01').fadeOut('400', function() {
					$('#tooltip01').remove();
				});
			},3000);
		};
		if (checked >= 4) {
			$('.c-result .o-checkbox__input:checkbox:not(:checked)').prop("disabled", true);
		}else{
			$('.c-result .o-checkbox__input:checkbox:not(:checked)').prop("disabled", false);
		}
	});

});


// format money
var formatNumber = {
	separador: ".",
	sepDecimal: ',',
	formatear:function (num){
		num +='';
		var splitStr = num.split('.');
		var splitLeft = splitStr[0];
		var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
		var regx = /(\d+)(\d{3})/;
		while (regx.test(splitLeft)) {
			splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
		}
		return this.simbol + splitLeft  +splitRight;
	},
	new:function(num, simbol){
		this.simbol = simbol ||'';
		return this.formatear(num);
	}
}

// Range
$( function() {
  $( "#slider-range-min" ).slider({
    range: "min",
    value: 150000,
    min: 1,
    max: 500000,
    slide: function( event, ui ) {
			$('.ui-slider-handle .o-slider__tooltip').remove();
			$('.ui-slider-handle').append( '<span class="o-slider__tooltip">$ '+formatNumber.new(ui.value)+'</span>' );
    }
  });
  $('.ui-slider-handle').append( '<span class="o-slider__tooltip">$ '+formatNumber.new($( "#slider-range-min" ).slider( "value" ))+'</span>' );
} );



// Validate
 
	// Validación cotización paso 1
	$( '#form_quotation_step1' ).validate({
		rules: {
			vehicle_type: {
				required:true
			},
			vehicle_brand: {
				required:true
			},
			vehicle_model: {
				required:true
			},
			vehicle_year: {
				required:true
			}
		},
		messages: {
			vehicle_type: {
				required: 'Este campo es requerido'
			},
			vehicle_brand: {
				required: 'Este campo es requerido'
			},
			vehicle_model: {
				required: 'Este campo es requerido'
			},
			vehicle_year: {
				required: 'Este campo es requerido'
			}
		},
		highlight: function(element) {
			$(element).parent().addClass("is-error");
			$(element).parent().removeClass("is-ok");
		},
		unhighlight: function(element) {
			$(element).parent().removeClass("is-error");
			$(element).parent().addClass("is-ok");
		},
		errorPlacement: function ($error, $element) {
			$element.siblings('.o-form__message').append($error);
		},
		submitHandler: function(form) {
			return false;
		}
	});
 
 
	// Validación cotización paso 2
	$( '#form_quotation_step2' ).validate({
		rules: {
			name: {
				required:true
			},
			lastname: {
				required:true
			},
			lastname2: {
				required:true
			},
			rut: {
				required:true,
				rut:true
			},
			day_birth: {
				required:true
			},
			month_birth: {
				required:true
			},
			year_birth: {
				required:true
			},
			civil_state: {
				required:true
			},
			gender: {
				required:true
			},
			email: {
				required:true,
				email:true
			},
			phone_type: {
				required:true
			},
			phone_number: {
				required:true
			},
			who: {
				required:true
			}
		},
		groups: {
			date_of_birth: 'day_birth month_birth year_birth',
			phone: 'phone_type phone_number'
		},
		messages: {
			name: {
				required: 'Este campo es requerido'
			},
			lastname: {
				required: 'Este campo es requerido'
			},
			lastname2: {
				required: 'Este campo es requerido'
			},
			rut: {
				required: 'Este campo es requerido',
				rut: 'El rut ingresado no es válido'
			},
			day_birth: {
				required: 'Este campo es requerido'
			},
			month_birth: {
				required: 'Este campo es requerido'
			},
			year_birth: {
				required: 'Este campo es requerido'
			},
			civil_state: {
				required: 'Este campo es requerido'
			},
			gender: {
				required: 'Este campo es requerido'
			},
			email: {
				required: 'Este campo es requerido',
				email: 'El email ingresado no es válido'
			},
			phone_type: {
				required: 'Este campo es requerido'
			},
			phone_number: {
				required: 'Este campo es requerido'
			},
			who: {
				required: 'Este campo es requerido'
			}
		},
		highlight: function(element) {
			$(element).parent().addClass("is-error");
			$(element).parent().removeClass("is-ok");
		},
		unhighlight: function(element) {
			$(element).parent().removeClass("is-error");
			$(element).parent().addClass("is-ok");
		},
		errorPlacement: function ($error, $element) {
			$element.siblings('.o-form__message').append($error);
			if ($element.attr("name") == "day_birth" || $element.attr("name") == "month_birth" || $element.attr("name") == "year_birth"){
				$("#year_of_birth.o-form__message").append($error);
			}
			if ($element.attr("name") == "phone_type" || $element.attr("name") == "phone_number"){
				$("#phone.o-form__message").append($error);
			}
		},
		submitHandler: function(form) {
			return false;
		}
	});
 
 
	// Validación Contrato paso 1
	$( '#form_recruiting_step1' ).validate({
		rules: {
			name: {
				required:true
			},
			lastname: {
				required:true
			},
			lastname2: {
				required:true
			},
			rut: {
				required:true,
				rut:true
			},
			day_birth: {
				required:true
			},
			month_birth: {
				required:true
			},
			year_birth: {
				required:true
			},
			civil_state: {
				required:true
			},
			gender: {
				required:true
			},
			email: {
				required:true,
				email:true
			},
			phone_type: {
				required:true
			},
			phone_number: {
				required:true,
				number: true,
				minlength:8,
			},
			district: {
				required:true
			},
			state: {
				required:true
			},
			direction: {
				required:true
			},
			number: {
				required:true,
				number: true
			},
			depto: {
				required:true
			}
		},
		groups: {
			date_of_birth: 'day_birth month_birth year_birth',
			phone: 'phone_type phone_number',
			directions: 'direction number depto'
		},
		messages: {
			name: {
				required: 'Este campo es requerido'
			},
			lastname: {
				required: 'Este campo es requerido'
			},
			lastname2: {
				required: 'Este campo es requerido'
			},
			rut: {
				required: 'Este campo es requerido',
				rut: 'El rut ingresado no es válido'
			},
			day_birth: {
				required: 'Este campo es requerido'
			},
			month_birth: {
				required: 'Este campo es requerido'
			},
			year_birth: {
				required: 'Este campo es requerido'
			},
			civil_state: {
				required: 'Este campo es requerido'
			},
			gender: {
				required: 'Este campo es requerido'
			},
			email: {
				required: 'Este campo es requerido',
				email: 'El email ingresado no es válido'
			},
			phone_type: {
				required: 'Este campo es requerido'
			},
			phone_number: {
				required: 'Este campo es requerido',
				number: 'Sólo debes ingresar números',
				minlength: 'Debes ingresar 9 dígitos'
			},
			district: {
				required: 'Este campo es requerido'
			},
			state: {
				required: 'Este campo es requerido'
			},
			direction: {
				required: 'Este campo es requerido'
			},
			number: {
				required: 'Este campo es requerido',
				number: 'Sólo debes ingresar números'
			},
			depto: {
				required: 'Este campo es requerido'
			}
		},
		highlight: function(element) {
			$(element).parent().addClass("is-error");
			$(element).parent().removeClass("is-ok");
		},
		unhighlight: function(element) {
			$(element).parent().removeClass("is-error");
			$(element).parent().addClass("is-ok");
		},
		errorPlacement: function ($error, $element) {
			$element.siblings('.o-form__message').append($error);
			if ($element.attr("name") == "direction" || $element.attr("name") == "number" || $element.attr("name") == "depto"){
				$("#direction--recruiting.o-form__message").append($error);
			}
			if ($element.attr("name") == "day_birth" || $element.attr("name") == "month_birth" || $element.attr("name") == "year_birth"){
				$("#year_of_birth--recruiting.o-form__message").append($error);
			}
			if ($element.attr("name") == "phone_type" || $element.attr("name") == "phone_number"){
				$("#phone--recruiting.o-form__message").append($error);
			}
		},
		submitHandler: function(form) {
			return false;
		}
	});
 
 
	// Validación contrato paso 2
	$( '#form_recruiting_step2' ).validate({
		rules: {
			car_type: {
				required:true
			},
			car_brand: {
				required:true
			},
			car_model: {
				required:true
			},
			car_year: {
				required:true
			},
			car_antiquity: {
				required:true
			},
			color: {
				required:true
			},
			patent: {
				required:true
			},
			engine: {
				required:true
			},
			chassis: {
				required:true
			},
			vehicle_private: {
				required:true
			},
			vehicle_have_damages: {
				required:true
			},
			vehicle_have_damages2: {
				required:true
			},
			vehicle_damaged_affects_safety: {
				required:true
			}
		},
		groups: {
			phone: 'phone_type phone_number'
		},
		messages: {
			car_type: {
				required: 'Este campo es requerido'
			},
			car_brand: {
				required: 'Este campo es requerido'
			},
			car_model: {
				required: 'Este campo es requerido'
			},
			car_year: {
				required: 'Este campo es requerido'
			},
			car_antiquity: {
				required: 'Este campo es requerido'
			},
			color: {
				required: 'Este campo es requerido'
			},
			patent: {
				required: 'Este campo es requerido'
			},
			engine: {
				required: 'Este campo es requerido'
			},
			chassis: {
				required: 'Este campo es requerido'
			},
			vehicle_private: {
				required: 'Este campo es requerido'
			},
			vehicle_have_damages: {
				required: 'Este campo es requerido'
			},
			vehicle_have_damages2: {
				required: 'Este campo es requerido'
			},
			vehicle_damaged_affects_safety: {
				required: 'Este campo es requerido'
			}
		},
		highlight: function(element) {
			$(element).parent().addClass("is-error");
			$(element).parent().addClass("o-form__field--error");
			$(element).parent().removeClass("is-ok");
			if($(element).attr('type')== 'radio'){
				$(element).parents('.o-form__field').addClass("is-error");
			} 
		},
		unhighlight: function(element) {
			$(element).parent().removeClass("is-error");
			$(element).parent().removeClass("o-form__field--error"); 
			$(element).parent().addClass("is-ok");
		},
		errorPlacement: function ($error, $element) {
			$element.siblings('.o-form__message').append($error);

			if($element.attr('type')== 'radio'){
				$element.parents('.o-form__field').children('.o-form__message').append($error);
			}
		},
		submitHandler: function(form) {
			var inst = $('[data-remodal-id=continueModal]').remodal();
			inst.open();
			return false; 
		}
	});
 
 
	// Validación contrato paso 3
	$( '#form_recruiting_step3' ).validate({
		rules: {
			comercial: {
				required:true
			},
			card: {
				required:true
			},
			card_number: {
				required:true,
				maxlength:20
			}
		},
		messages: {
			comercial: {
				required: 'Este campo es requerido'
			},
			card: {
				required: 'Este campo es requerido'
			},
			card_number: {
				required: 'Este campo es requerido',
				maxlength: 'Debes ingresar 16 dígitos'
			}
		},
		highlight: function(element) {
			$(element).parent().addClass("is-error");
			if($(element).attr('type')== 'checkbox'){
				$(element).parents('.o-form__field').addClass("is-error");
			}
		},
		unhighlight: function(element) {
			$(element).parent().removeClass("is-error");
		},
		errorPlacement: function ($error, $element) {
			$element.siblings('.o-form__message').append($error);
			if($element.attr('type')== 'checkbox'){
				$element.parents('.o-form__field').children('.o-form__message').append($error);
			}
			if ($element.attr("name") == "card"){
				$("#card_field.o-form__message").append($error);
			}
		},
		submitHandler: function(form) {
			return false;
		}
	});
 

// Validar rut
function validaRut(campo){
	if ( campo.length == 0 ){ return false; }
	if ( campo.length < 8 ){ return false; }

	campo = campo.replace('-','')
	campo = campo.replace(/\./g,'')

	if ( campo.length > 9 ){ return false; }

	var suma = 0;
	var caracteres = "1234567890kK";
	var contador = 0;
	for (var i=0; i < campo.length; i++){
		u = campo.substring(i, i + 1);
		if (caracteres.indexOf(u) != -1)
		contador ++;
	}
	if ( contador==0 ) { return false }

	var rut = campo.substring(0,campo.length-1)
	var drut = campo.substring( campo.length-1 )
	var dvr = '0';
	var mul = 2;

	for (i= rut.length -1 ; i >= 0; i--) {
		suma = suma + rut.charAt(i) * mul
								if (mul == 7) 	mul = 2
						else	mul++
	}
	res = suma % 11
	if (res==1)		dvr = 'k'
								else if (res==0) dvr = '0'
	else {
		dvi = 11-res
		dvr = dvi + ""
	}
	if ( dvr != drut.toLowerCase() ) { return false; }
	else { return true; }
}

// Rut no valido
$.validator.addMethod("rut", function(value, element) {
	return this.optional(element) || validaRut(value);
}, "El rut no es válido");


// Center Image
$('.js-getImage').each(function(i, e) {
	var image;
	image = $(e).find('.js-image img').attr('src');
	$(e).find('.js-image').css('background-image', 'url(' + image + ')');
	return $(e).find('.js-image');
});


// Scroll control
$(window).on("scroll", function () {
	// console.log($(this).scrollTop()); 
	var altodiv = $('.o-box--recruiting').height() - 260; 
	var anchoTotal = $(document).width();
	console.log(anchoTotal);
	if(anchoTotal > 991 ){
		console.log('1');
		if ($('.o-box--recruiting').height()>800) {

			if ($(this).scrollTop() > 450 && $(this).scrollTop() < altodiv) {
				$('#summary_sure').addClass('is-fixed');
			}else{
				$('#summary_sure').removeClass('is-fixed');
			}
			if ($(this).scrollTop() > altodiv) {
				$('#summary_sure').addClass('is-absolute');
			}else{
				$('#summary_sure').removeClass('is-absolute');
			}
		}
	}else{
		console.log('2');
		$('#summary_sure').removeClass('is-fixed');
	}
	
});

  //  Modal
  $('[data-remodal-id=loginModal]').remodal({
    modifier: 'with-red-theme'
  });

// only numbres
function just_numbers(e){
	var keynum = window.event ? window.event.keyCode : e.which;
	if ((keynum == 8) || (keynum == 46))
	return true;
	return /\d/.test(String.fromCharCode(keynum));
}

// Mandatory inspection
	$('#mandatory_inspection_check').change(function() {
		if(this.checked) {
			$('#mandatory_inspection_input').prop("disabled", false);
		}else{
			$('#mandatory_inspection_input').prop("disabled", true);
		}
	});

// separador
$('#card_number').on('keypress change', function () {
	$(this).val(function (index, value) {
		return value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
	});
});
