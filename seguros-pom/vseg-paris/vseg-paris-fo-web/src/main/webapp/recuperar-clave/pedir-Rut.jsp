<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<bean:define id="contextpath" value="<%=request.getContextPath()%>" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>
	
	<title>Cambio de Clave</title>

	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	<link href="<bean:write name="contextpath"/>/css/formulario-r.css"
		rel="stylesheet" type="text/css" />
	<link href="/cotizador/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/cotizador/css/estilos.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/cotizador/css/base.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/cotizador/css/jquery-ui.css" rel="stylesheet" type="text/css" media="all" />	
<script src="js/jquery-1.11.3.min.js"></script>	
	<script src="<bean:write name="contextpath"/>/js/jquery/jquery-1.4.2.js"></script>
	

	
<!--
.dsR1 /*agl rulekind: base;*/ {
	width: 16px;
	height: 16px;
}

.cont-text-captcha {
	float: right;
	height: auto;
	width: 135px;
	text-align: left;
}
-->
</style>
	<!--[if lt IE 7]>
		<script type="text/javascript" src="/vseg-paris/js/unitpngfix.js"></script>
	<![endif]-->

	<script>
		function isRutKey(e){
			var unicode=e.charCode? e.charCode : e.keyCode;
			if (unicode!==8&&unicode!==107&&unicode!==75){ 
				if (unicode<48||unicode>57) 
					return false;
			}
		}
		function autocompletarrut(input) {
			if(input && input.value && input.value.length && input.value.length > 1) {
				input.value = input.value.replace("-","");
				var value = input.value.substring(0, input.value.length -1);
				var digit = input.value.substring(input.value.length -1, input.value.length);
				input.value = value + "-" + digit;
			}
		}
		function enviarRut() {
			personalInfoRut();
			if (!validateInputRut("validaForm")) {
					console.log("FALSE");
					return false;
				}else{
					console.log("SUBMIT");
					$("#formRut").submit();
				}
		}
		function validateInputRut(id) {
			validacionInput = 0;
			console.log('in validate input: ' + id);
			$('#'+id+'').find(":input:not(:hidden,:button,:submit)").each(function(){
				console.log('testing ' + this.id);
				var optional = $(this).hasClass("optional");
				if(optional==false) {
					$(this).css("border-color","#cecece");
					if(!this.value) {
						$("#incomplete-text-"+id).html("Por favor complete el formulario correctamente");
						$("#incomplete-"+id).show("fast");
						$(this).css("border-color","red");
						$(this).focus();
						console.log(this.id + ' est� vac�o');
						validacionInput = 1;
						return false;
					} 
					if($(this).attr("mail")) {
						console.log("testing mail");
						var result = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(this.value);
						if(result == false) {
							$("#incomplete-text-"+id).html("El correo tiene el formato incorrecto");
							$("#incomplete-"+id).show("fast");
							$(this).css("border-color","red");
							$(this).focus();
							console.log(this.id + ' formato incorrecto');
							validacionInput = 1;
							return false;
						} 
					}

				}
				
			});
			
			if(validacionInput == 0) {
				$("#incomplete-text-"+id).html("");
				$("#incomplete-"+id).hide("fast");
				return true;
			}else{
				return false;
			}
		}
		function personalInfoRut() {
			if(document.getElementById('datos.rutcompleto')) {
				var rutCompleto = document.getElementById('datos.rutcompleto').value.split('-');
				if(rutCompleto.length > 0) {
					document.getElementById('datos.rut').value = rutCompleto[0];
					if(rutCompleto.length > 1) {
						document.getElementById('datos.dv').value = rutCompleto[1];
					}
				}
			}
		}
		$(document).ready(function(){
			$('.datos-rut-input').attr('rut','12345678-9');
		})
		
		
		function registrate(){
		jQuery.noConflict();
		$("#modalOlvidasteClave").hide();		
		top.location.href = "/vseg-paris/registrocliente/registroCliente.do?rut_cliente=&dv_cliente=";	
		}

	  </script>
</head>
<body>
	<div class="container">
		<html:form action="/generar-clave-temporal" method="post" styleId="formRut">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="validaForm">
				<html:hidden property="datos(rut)" styleId="datos.rut"/>
				<html:hidden property="datos(dv)" styleId="datos.dv"/>
				<div class="row top20">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<label>Ingrese su Rut*</label>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<input class="o-form__input" type="text" placeholder="12345678-9" 
							maxlength="10" size="12" id="datos.rutcompleto" 
							onkeypress="return isRutKey(event)" onkeyup="autocompletarrut(this);"/>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
						<h5 style="color:#fb7f03;"><html:errors property="datos.rutdv" /></h5>
						<h5 style="color:#fb7f03;"><logic:present name="MensajeError">* <bean:write name="MensajeError"/></logic:present></h5>
					</div>
				</div>
				<div class="row top15">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<button type="button" class="o-btn o-btn--primary" id="enter" onclick="javascript:enviarRut();">Recuperar</button>
					</div>
				</div>
				<div class="row top15">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
						<div class="txt_rc_inv" align="left"><logic:present name="LinkRegistroCliente"><a id="registroUsuario2" href="#" onclick="registrate();" target="_self">Registrate aqu&iacute;</a></logic:present></div>
					</div>
				</div>
			</div>
		</html:form>
	</<div>
</body>
</html:html>
