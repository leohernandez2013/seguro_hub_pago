<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>
<bean:define id="contextpath" value="<%=request.getContextPath()%>" />
<!DOCTYPE html>
<html lang="es">
<head>
<%@ include file="home/includes/head.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Seguros Cencosud - Seguros de Auto, Argentina Mercosur, Viaje, Hogar y Vida.</title>

<%
	String ip = request.getHeader("X-Forwarded-For");
	if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
		ip = request.getHeader("Proxy-Client-IP");
	}
	if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
		ip = request.getHeader("WL-Proxy-Client-IP");
	}
	if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
		ip = request.getHeader("HTTP_CLIENT_IP");
	}
	if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
		ip = request.getHeader("HTTP_X_FORWARDED_FOR");
	}
	if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
		ip = request.getRemoteAddr();
	}
	//System.out.println("ipAddress: "+ip);
%>

<a class="btn_lightbox" href="#inline" data-lity>
</a>
<script language="JavaScript"></script>
<div id="inline" style="overflow:hidden; max-width:100%;" class="lity-hide">
</div> 

<script src="js/index.js"></script>
<script type="text/javascript">
	
	 $(document).on('load', function(event) {
		event.preventDefault();
		if ($("select#rama").val() == 9){
				$('select#subcategoria').css( "visibility", "hidden" );
		}else if ($('select#rama').val() != 9){
			$('select#subcategoria').css( "visibility", "visible" );
			$('select#subcategoria').attr('disabled',false);
		}
		});
</script>

<script type="text/javascript">
	$(document).change(function(){
		$('select#subcategoria').css( "visibility", "visible" );
		 if ($('select#rama').val() == 9){
				$('#subCat').hide();
			}else{
			    $('#subCat').show();
			}
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#rama').val($('#rama > option:first').val());
		$("#subcategoria").val("22");
	}); 
</script>
</head>
<body>
	<%@ include file="google-analytics/google-analytics.jsp"%>
	<%@ include file="home/includes/header.jsp"%>
	
	<main role="main">
      <div class="remodal-bg">
        <div class="c-hero">
          <div class="c-quotient">
            <div class="c-quotient__inner">
              <h1 class="c-quotient__title"><strong>Cotiza</strong><br>y contrata ahora</h1>
              <p class="c-quotient__subtitle">Obt�n tu seguro en l�nea</p>
              <form class="o-form o-form--quotient" action="">
                <div class="o-form__field o-form__field--select">
                  <select class="o-form__select" name="CountrySelect" id="rama"
											onchange="cargarSubcategorias()">
                    <option value="1" selected="selected">Veh&iacute;culo</option>
					<option value="9" >SOAP 2017</option>
					<option value="8">Moto</option>
					<option value="6">Viajes</option>
					<option value="2">Hogar</option>
					<option value="5">Fraude</option>
                  </select>
                </div>
                <div id="subCat" class="o-form__field o-form__field--select">
                  <select class="o-form__select" name="RegionSelect"
											id="subcategoria">
                    <option value="null">Selecciona un Servico</option>
                    <option value="22" selected="selected">Full Cobertura</option>
					<option value="22">Full Cobertura Auto Play</option>
					<option value="21">P&eacute;rdida Total</option>
					<option value="36">Seguro Obligatorio Mercosur</option>
                  </select>
                </div>
                <div class="o-form__field o-form__field--actions">
                  <input class="o-btn o-btn--primary" id="cotizar" type="button" value="Cotizar">
                </div>
              </form>
            </div>
          </div>
		  
<div class="slider">
  <div class="slider_bck" style="background-image: url(img/banner_home/2017/12/Banner_EligetuRegalo_12.jpg);">
    <div class="o-advertising">
      <div class="u-center">
        <div class="caja_banner">
          <h3 class="o-advertising__volante">En esta Navidad elige tu regalo</h3>
          <h1 class="o-advertising__title">Contrata tu seguro de Auto y elige lo que quieras</h1>
          <p class="o-advertising__text">UNA GIFTCARD DE $40.000<br>50% DCTO EN 2 CUOTAS<br>ABONO DE $40.000</p>
          <hr class="u-line">
          <small class="o-advertising__legal">V�lido hasta el 02/01/18</small>
          <div class="o-advertising__actions"><a class="o-advertising__btn" href="/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=22&idTipo=1">Cotiza Aqu�</a></div>
        </div>
      </div>
    </div>
  </div>
  <!--div class="slider_bck" style="background-image: url(img/banner_home/2017/11/Banner_gopro_11.jpg);">
    <div class="o-advertising">
      <div class="u-center">
        <div class="caja_banner">
          <h3 class="o-advertising__volante">�VIAJA EN HD! Y PARTICIPA POR DOS</h3>
          <h1 class="o-advertising__title">C�mara GoPro<br>Hero 5 Black</h1>
          <p class="o-advertising__text">Contratando tu Seguro de Viaje o RCI</p>
          <hr class="u-line">
          <small class="o-advertising__legal">V�lido hasta el 30/11/17</small>
          <div class="o-advertising__actions"><a class="o-advertising__btn" href="https://www.seguroscencosud.cl/vseg-paris/html/GoPro.html">Cotiza Aqu�</a></div>
        </div>
      </div>
    </div>
  </div-->
  <div class="slider_bck" style="background-image: url(img/banner_home/2017/11/Banner_viaje_11.jpg);">
    <div class="o-advertising">
      <div class="u-center">
        <div class="caja_banner">
          <h3 class="o-advertising__volante">DESPREOC�PATE Y VIAJA TRANQUILO</h3>
          <h1 class="o-advertising__title">Contrata tu Seguro<br>de Viaje</h1>
          <p class="o-advertising__text">Y elige tu plan de acuerdo a tu destino</p>
          <hr class="u-line">
          <div class="o-advertising__actions"><a class="o-advertising__btn" href="/vseg-paris/desplegar-ficha.do?idRama=6&idSubcategoria=221">Cotiza Aqu�</a></div>
        </div>
      </div>
    </div>
  </div>
  <div class="slider_bck" style="background-image: url(img/banner_home/2017/11/Banner_rci_11.jpg);">
    <div class="o-advertising">
      <div class="u-center">
        <div class="caja_banner">
          <h3 class="o-advertising__volante">SI VIAJAS A CUALQUIER CIUDAD DE ARGENTINA, BRASIL Y URUGUAY</h3>
          <h1 class="o-advertising__title">Debes contratar<br>tu Seguro RCI </h1>
          <p class="o-advertising__text">Contamos con planes 3, 4, 5, 7, 10, 15, 30 y 180 d�as</p>
          <hr class="u-line">
          <div class="o-advertising__actions"><a class="o-advertising__btn" href="/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=36">Cotiza Aqu�</a></div>
        </div>
      </div>
    </div>
  </div>
</div>

</div>
 
<div class="c-products u-mt-5">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="o-box u-mb40 u-text-center">
					<h2 class="o-title o-title--secundary">�Qu� quieres asegurar?</h2>
					<h5 class="o-title o-title--epigraph">Selecciona un producto para m�s informaci�n</h5>
					<ul class="o-list o-list--products row">
						<li class="o-list__item col-md-2 col-sm-6">
						<a class="o-list__link" href="#" onclick="window.location.href='/vseg-paris/desplegar-pagina-intermedia-vehiculos.do?idRama=1'">
						<span class="icon_home auto"><svg data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 350.59 268.71"><defs><style>.cls-1{fill:#019EE3;fill-rule:evenodd;}</style></defs><title>auto</title><path class="cls-1" d="M313.77,156.81a20.46,20.46,0,1,1-20.43-20.43A20.45,20.45,0,0,1,313.77,156.81ZM71.26,136.38A20.46,20.46,0,1,0,91.7,156.81,20.47,20.47,0,0,0,71.26,136.38Zm76,46.7a14.64,14.64,0,0,0,0,29.28h70.12a14.64,14.64,0,0,0,0-29.28ZM264.1,241.57H100.48v11.68a29.21,29.21,0,0,1-58.42,0V241.57A20.5,20.5,0,0,1,21.59,221.1V156.81a49.7,49.7,0,0,1,30-45.56l-2.37-2.35a5.59,5.59,0,0,0-4.1-1.72H21.59a14.62,14.62,0,0,1,0-29.23H36.78a28.25,28.25,0,0,1,20.65,8.58l3,3,8.89-32.69a58.05,58.05,0,0,1,56.37-43.07H238.84a58.1,58.1,0,0,1,56.42,43.07l8.9,32.69,3-2.93A28.22,28.22,0,0,1,327.79,78H343a14.62,14.62,0,1,1,0,29.23H319.49a5.55,5.55,0,0,0-4.09,1.72L313,111.25a49.63,49.63,0,0,1,29.9,45.56V221.1a20.45,20.45,0,0,1-20.4,20.47v11.68a29.22,29.22,0,0,1-58.44,0ZM125.71,42.88A28.89,28.89,0,0,0,97.54,64.44L85.85,107.18H278.67L267,64.44a28.93,28.93,0,0,0-28.2-21.56Z" transform="translate(-7.01 -13.72)"></path></svg>
						</span>              
						<span class="o-list__title">VEH�CULO</span></a>
						</li>
						
						<li class="o-list__item col-md-2 col-sm-6">
						<a class="o-list__link" href="#" onclick="window.location.href='/vseg-paris/desplegar-pagina-intermedia-fraude.do?idRama=8'">
						<span class="icon_home moto"><svg data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 577.54 318.1"><defs><style>.cls-1{fill:#019EE3;}</style></defs><title>Seguro moto</title><path class="cls-1" d="M443,389.37q20.31,0,35,6.77l47.38-81.22a11.08,11.08,0,0,1,10.15-5.64c8.27,0,12.41,3.78,12.41,11.28a7.62,7.62,0,0,1-2.26,5.64l-47.37,81.22q37.21,26,37.22,72.19,0,37.23-27.63,64.3t-66,27.07q-38.35,0-64.86-27.07t-26.51-64.3q0-37.22,27.07-63.73T443,389.37ZM443,536q24.81,0,41.18-16.35t16.35-40q0-27.08-19.17-42.86l-28.2,48.5c-2.26,3.78-6,5.64-11.28,5.64q-11.28,0-11.28-11.28a8.77,8.77,0,0,1,1.12-3.38V474L461,425.47a60.63,60.63,0,0,0-19.17-3.39q-23.7,0-40.61,16.92T384.3,479.61a57.51,57.51,0,0,0,18.05,40A56.35,56.35,0,0,0,443,536Zm16.92-171.45c-4.51,0-6.76-1.87-6.76-5.64v-44l6.76-5.64q14.65,1.14,24.26,10.16t9.59,16.92q0,9-9.59,18T459.87,364.56Zm153.41-85.73h-1.12l-42.87-10.16-18,31.59c-1.51,3-3.77,4.51-6.76,4.51q-7.9,0-7.9-7.9l1.13-2.25v-2.26l22.56-35c.74-3,3-4.51,6.77-4.51a1,1,0,0,1,1.12,1.13l47.38,11.28q5.64,0,5.64,6.77T613.28,278.83ZM553.5,406.29H745.26a12.37,12.37,0,0,1,9.59,4.51c2.63,3,3.56,6.77,2.82,11.28l-7.9,53h12.41A84.74,84.74,0,0,1,790.38,426q22.56-19.71,54.15-19.74,35,0,59.22,23.69T928,488.64q0,33.84-24.25,57.53t-59.22,23.68q-31.59,0-54.15-19.74a84.62,84.62,0,0,1-28.2-49.06h-18c-1.51,3.77-3.38,6.39-5.64,7.89l-28.2,20.31a10.85,10.85,0,0,1-7.89,3.38H597.49c-6,0-10.15-3-12.41-9l-44-100.4c-2.25-3.76-1.88-7.5,1.13-11.28A13.87,13.87,0,0,1,553.5,406.29Zm333.89-25.94q-6.76,5.65-52.45,7.89t-87.42,2.26H590.72q-13.53,0-25.94-16.36t-12.41-27.63a24.79,24.79,0,0,1,3.39-11.85q3.38-6.18,18.61-13t41.17-6.77q10.15,0,34.4,11.28t37.79,13.54q21.42,3.39,56.4,5.64t49.64,4.51q3.38,1.13,44.55-5.64t51.33-4.51q12.4,2.28,11.28,16.92T887.39,380.35ZM844.53,536a48.64,48.64,0,0,0,35-14.1q14.64-14.1,14.66-33.27a46,46,0,0,0-14.66-34.41,48.72,48.72,0,0,0-35-14.1q-36.12,0-46.25,35h48.5q5.64,0,5.64,9v10.16q0,7.89-5.64,7.89h-48.5Q808.44,536,844.53,536Z" transform="translate(-350.46 -252.88)"/></svg></span>
						<span class="o-list__title">MOTO</span>
						</a>
						</li>
						
						<li class="o-list__item col-md-2 col-sm-6">
						<a class="o-list__link" href="#" onclick="window.location.href='/vseg-paris/desplegar-pagina-intermedia-fraude.do?idRama=6'">
						<span class="icon_home viajes">
						<svg data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 350.78 193.08"><defs><style>.cls-1{fill:#019EE3;}</style></defs><title>Sin t�tulo-1</title><path class="cls-1" d="M356.75,196.87l-.36-3.33a7.74,7.74,0,0,0-7.25-6.89c-.12,0-13.79-.8-38.08-5.1a329.62,329.62,0,0,1-35-8.22,20.93,20.93,0,0,0-2.16-10.73,14.77,14.77,0,0,0-9-7.18,20.05,20.05,0,0,0-5.39-.77c-7.77,0-13.07,4.93-16.23,10-4.31-1-10.77-2.59-18.51-4.91-7.44-2.23-13.1-4.18-16.81-5.56,1.49-15.32,4.77-56.1.22-75.28C201.59,51.15,187.41,47,179.56,47c-15.12,0-25.3,10.37-31.14,31.67-5.46,20-2.19,61.13-.71,76.55-3.53,1.29-8.8,3.15-15.7,5.33-7.92,2.48-15.28,4.73-20.22,6.23-3.55-5.84-10.32-13.16-20.8-11.74a14.93,14.93,0,0,0-10.62,6.54c-2.73,4.11-3.3,9.2-3.14,13.41-4.92,1.12-13,2.91-25.39,5.47-24.29,5-37.57,6.19-37.71,6.21a7.7,7.7,0,0,0-6.66,5.18l-1,3.06a7.74,7.74,0,0,0,.91,6.82,7.64,7.64,0,0,0,5.95,3.4s2.65.18,8.68.18,16.19-.18,31.43-.9L60.5,204c12.46-.56,15.38-.72,29.21-2.06,2.78,2,7.1,4.34,11.88,4.34A14.06,14.06,0,0,0,111.3,202l41.17,1.64c.47,1.87,1.1,4,1.85,6.22.13.39.28.79.42,1.16l-35.8,6a7.83,7.83,0,0,0-4.43,2.38l-2.23,2.44a7.76,7.76,0,0,0-1.35,8.39,8,8,0,0,0,7.21,4.54l47.52-.93c2.68,2.87,7,6.26,12.43,6.26,5.94,0,10.38-3.82,13.06-7l52.46.69a7.43,7.43,0,0,0,7.19-4.66,7.69,7.69,0,0,0-1.55-8.41l-1.82-1.89a7.82,7.82,0,0,0-4.38-2.27L200.13,210l.31-1.14c.53-2,.95-3.91,1.33-5.62l40.65-1.37a13.81,13.81,0,0,0,10.06,4.33,15.94,15.94,0,0,0,9.92-3.55c7.49.4,26.36,1.36,47.77,2,22.51.73,38.76.75,38.91.75a7.69,7.69,0,0,0,5.75-2.58,7.77,7.77,0,0,0,1.92-6" transform="translate(-6.01 -47)"></path></svg>
						</span>
						<span class="o-list__title">VIAJES</span>
						</a>
						</li>
						
						<li class="o-list__item col-md-2 col-sm-6">
						<a class="o-list__link" href="#" onclick="window.location.href='/vseg-paris/desplegar-pagina-intermedia-vida.do?idRama=3'">
						<span class="icon_home vida"><svg  data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 165.29 151.95"><defs><style>.cls-1{fill:#009bdb;}</style></defs><title>Sin t�tulo-1</title><path class="cls-1" d="M331.6,344.26a32,32,0,0,1,30.87,24.93c.55,2.55.76,5.18,1.1,7.78a48.18,48.18,0,0,1-4.09,17.67c-3.76,8.78-9.32,16.32-15.83,23.22-8.69,9.26-18.63,17-29.15,24-2.81,1.88-5.71,3.62-8.56,5.43a1.44,1.44,0,0,1-.74.26,1.63,1.63,0,0,1-.79-.26,193,193,0,0,1-31.67-23.44c-7.47-6.94-14.12-14.59-19.1-23.54a49.46,49.46,0,0,1-6.54-20.66c-.66-10.44,2.45-19.57,10.21-26.82a30.86,30.86,0,0,1,18.22-8.31c1.25-.12,2.49-.16,3.74-.16A30.41,30.41,0,0,1,295.7,349a23.57,23.57,0,0,1,8.18,8.9c.26.45.48.92.76,1.37s.36.49.55.49.37-.16.56-.51c.47-.81.9-1.68,1.4-2.48,3.86-6.29,9.59-9.92,16.67-11.55a34.36,34.36,0,0,1,7.78-.93m0-24.33h0a59.74,59.74,0,0,0-13.25,1.52,54.5,54.5,0,0,0-13.2,4.87A54.64,54.64,0,0,0,279.27,320c-2,0-4.14.1-6.19.3a56.57,56.57,0,0,0-46.85,36.28,59.38,59.38,0,0,0-3.43,24.65c.67,10.39,3.79,20.51,9.55,30.88s13.47,20,23.78,29.57c9.87,9.22,21.19,17.61,35.59,26.38h0a25.51,25.51,0,0,0,27.39-.26c1-.64,2-1.27,3.11-2,1.83-1.17,3.76-2.39,5.75-3.69,13.45-8.95,24.38-17.95,33.42-27.56,9.26-9.85,16-19.78,20.48-30.33a73.29,73.29,0,0,0,6-26.51l0-2-.28-2c-.06-.54-.15-1.21-.22-1.91a66.65,66.65,0,0,0-1.25-7.91,56.32,56.32,0,0,0-54.6-43.9Z" transform="translate(-222.67 -319.93)"/></svg></span>
						<span class="o-list__title">VIDA Y SALUD</span>
						</a>
						</li>
						
						<li class="o-list__item col-md-2 col-sm-6">
						<a class="o-list__link" href="#" onclick="window.location.href='/vseg-paris/desplegar-pagina-intermedia-hogar.do?idRama=2'">
						<span class="icon_home hogar"><svg data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 290.11 281.48"><defs><style>.cls-1{fill:#019EE3;}</style></defs><title>Sin t�tulo-1</title><path class="cls-1" d="M328.34,108.49,294.92,84.65V33.85H259.21V59.23L183.28,5.12,38.22,108.49v31.25h27V286.6H305.73V139.74h22.61ZM268.9,244.76H208.84V197.5H155.28v47.26H102.12V135.38H86.29l97-69.13,97,69.13H268.9Z" transform="translate(-38.22 -5.12)"></path></svg>
						</span>
						<span class="o-list__title">HOGAR</span>
						</a>
						</li>
						
						<li class="o-list__item col-md-2 col-sm-6">
						<a class="o-list__link" href="#" onclick="window.location.href='/vseg-paris/desplegar-pagina-intermedia-fraude.do?idRama=5'">
						<span class="icon_home fraude"><svg data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 150.5 155.36"><defs><style>.cls-1{fill:#019EE3;}</style></defs><title>Sin t�tulo-1</title><path class="cls-1" d="M206.73,60.45a10,10,0,0,0-20,0V122h20Z" transform="translate(-116.73 -50.64)"></path><path class="cls-1" d="M180.73,74.67a10,10,0,0,0-20,0V122h20Z" transform="translate(-116.73 -50.64)"></path><path class="cls-1" d="M235.73,89.39a10,10,0,0,0-20,0V122h20Z" transform="translate(-116.73 -50.64)"></path><path class="cls-1" d="M264.8,131.78a11,11,0,0,0-14.86,0L239.73,141v27.75l24.09-23.11C267.65,141.82,268.63,135.62,264.8,131.78Z" transform="translate(-116.73 -50.64)"></path><path class="cls-1" d="M235.73,196.8c0,5.47-6.72,9.2-12.21,9.2H125.9c-5.48,0-9.17-3.73-9.17-9.2V142.59c0-5.47,3.69-13.59,9.17-13.59h97.62c5.49,0,12.21,8.12,12.21,13.59Z" transform="translate(-116.73 -50.64)"></path><path class="cls-1" d="M154.73,107.54c0-5.42-5.09-9.82-10.5-9.82s-10.5,4.4-10.5,9.82V122h21Z" transform="translate(-116.73 -50.64)"></path></svg>
						</span><span class="o-list__title">FRAUDE</span>
						</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
       	<div class="container">
          <div class="row">
            <div class="col-sm-12">
              <h2 class="o-title o-title--secundary u-text-center">Aprovecha nuestras ofertas</h2>
            </div>
            <div class="col-sm-12">
              <section class="u-mb20">
				<div id="offer-slick">
					  <div class="slick_slide">
						<div class="o-offer js-getImage">
						  <div class="o-offer__tag">
							<!-- Auto -->
							<svg data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 350.59 268.71" style="padding-top: 3px;">
							  <defs><style>.cls-2 {fill:#ffffff; fill-rule:evenodd;}</style></defs><title>auto</title><path class="cls-2" d="M313.77,156.81a20.46,20.46,0,1,1-20.43-20.43A20.45,20.45,0,0,1,313.77,156.81ZM71.26,136.38A20.46,20.46,0,1,0,91.7,156.81,20.47,20.47,0,0,0,71.26,136.38Zm76,46.7a14.64,14.64,0,0,0,0,29.28h70.12a14.64,14.64,0,0,0,0-29.28ZM264.1,241.57H100.48v11.68a29.21,29.21,0,0,1-58.42,0V241.57A20.5,20.5,0,0,1,21.59,221.1V156.81a49.7,49.7,0,0,1,30-45.56l-2.37-2.35a5.59,5.59,0,0,0-4.1-1.72H21.59a14.62,14.62,0,0,1,0-29.23H36.78a28.25,28.25,0,0,1,20.65,8.58l3,3,8.89-32.69a58.05,58.05,0,0,1,56.37-43.07H238.84a58.1,58.1,0,0,1,56.42,43.07l8.9,32.69,3-2.93A28.22,28.22,0,0,1,327.79,78H343a14.62,14.62,0,1,1,0,29.23H319.49a5.55,5.55,0,0,0-4.09,1.72L313,111.25a49.63,49.63,0,0,1,29.9,45.56V221.1a20.45,20.45,0,0,1-20.4,20.47v11.68a29.22,29.22,0,0,1-58.44,0ZM125.71,42.88A28.89,28.89,0,0,0,97.54,64.44L85.85,107.18H278.67L267,64.44a28.93,28.93,0,0,0-28.2-21.56Z" transform="translate(-7.01 -13.72)"></path></svg>
						  </div>
						  <div class="o-offer__content">
							<h2 class="o-offer__title">Contrata tu seguro Auto Full Cobertura</h2>
							<p class="o-offer__text">Nos encargamos de proteger lo que tanto te ha costado.</p>
							<a class="o-offer__link" href="https://www.seguroscencosud.cl/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=22&idTipo=1">Contr�talo aqu�</a> </div>
						  <div class="js-image"><img src="img/offer-01.jpg" alt=""></div>
						  <div class="cama-calugas-home"></div>
						</div>
					  </div>
					  <div class="slick_slide">
						<div class="o-offer js-getImage">
						  <div class="o-offer__tag">
							<!-- Mascota -->
							<svg  data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 165.29 151.95" style="padding-top: 2px;">
							  <defs><style>.cls-2{fill:#ffffff;}</style></defs><title>Sin t�tulo-1</title><path class="cls-2" d="M331.6,344.26a32,32,0,0,1,30.87,24.93c.55,2.55.76,5.18,1.1,7.78a48.18,48.18,0,0,1-4.09,17.67c-3.76,8.78-9.32,16.32-15.83,23.22-8.69,9.26-18.63,17-29.15,24-2.81,1.88-5.71,3.62-8.56,5.43a1.44,1.44,0,0,1-.74.26,1.63,1.63,0,0,1-.79-.26,193,193,0,0,1-31.67-23.44c-7.47-6.94-14.12-14.59-19.1-23.54a49.46,49.46,0,0,1-6.54-20.66c-.66-10.44,2.45-19.57,10.21-26.82a30.86,30.86,0,0,1,18.22-8.31c1.25-.12,2.49-.16,3.74-.16A30.41,30.41,0,0,1,295.7,349a23.57,23.57,0,0,1,8.18,8.9c.26.45.48.92.76,1.37s.36.49.55.49.37-.16.56-.51c.47-.81.9-1.68,1.4-2.48,3.86-6.29,9.59-9.92,16.67-11.55a34.36,34.36,0,0,1,7.78-.93m0-24.33h0a59.74,59.74,0,0,0-13.25,1.52,54.5,54.5,0,0,0-13.2,4.87A54.64,54.64,0,0,0,279.27,320c-2,0-4.14.1-6.19.3a56.57,56.57,0,0,0-46.85,36.28,59.38,59.38,0,0,0-3.43,24.65c.67,10.39,3.79,20.51,9.55,30.88s13.47,20,23.78,29.57c9.87,9.22,21.19,17.61,35.59,26.38h0a25.51,25.51,0,0,0,27.39-.26c1-.64,2-1.27,3.11-2,1.83-1.17,3.76-2.39,5.75-3.69,13.45-8.95,24.38-17.95,33.42-27.56,9.26-9.85,16-19.78,20.48-30.33a73.29,73.29,0,0,0,6-26.51l0-2-.28-2c-.06-.54-.15-1.21-.22-1.91a66.65,66.65,0,0,0-1.25-7.91,56.32,56.32,0,0,0-54.6-43.9Z" transform="translate(-222.67 -319.93)"/></svg>
						  </div>
						  <div class="o-offer__content">
							<h2 class="o-offer__title">Seguro de Mascota</h2>
							<p class="o-offer__text">Por que tu mascota es importante para tu familia.</p>
							<a class="o-offer__link" href="https://www.seguroscencosud.cl/vseg-paris/desplegar-ficha.do?idRama=6&idSubcategoria=140">Contr�talo aqu�</a> </div>
						  <div class="js-image"><img src="img/offer-02.jpg" alt=""></div>
						  <div class="cama-calugas-home"></div>
						</div>
					  </div>
					  <div class="slick_slide">
						<div class="o-offer js-getImage">
						  <div class="o-offer__tag">
							<!-- Viajes -->
							<svg data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 350.78 193.08" style="padding-top: 3px; margin-left: -3px; margin-right: -3px;">
							  <defs><style>.cls-2{fill:#ffffff;}</style></defs><title>Sin t�tulo-1</title><path class="cls-2" d="M356.75,196.87l-.36-3.33a7.74,7.74,0,0,0-7.25-6.89c-.12,0-13.79-.8-38.08-5.1a329.62,329.62,0,0,1-35-8.22,20.93,20.93,0,0,0-2.16-10.73,14.77,14.77,0,0,0-9-7.18,20.05,20.05,0,0,0-5.39-.77c-7.77,0-13.07,4.93-16.23,10-4.31-1-10.77-2.59-18.51-4.91-7.44-2.23-13.1-4.18-16.81-5.56,1.49-15.32,4.77-56.1.22-75.28C201.59,51.15,187.41,47,179.56,47c-15.12,0-25.3,10.37-31.14,31.67-5.46,20-2.19,61.13-.71,76.55-3.53,1.29-8.8,3.15-15.7,5.33-7.92,2.48-15.28,4.73-20.22,6.23-3.55-5.84-10.32-13.16-20.8-11.74a14.93,14.93,0,0,0-10.62,6.54c-2.73,4.11-3.3,9.2-3.14,13.41-4.92,1.12-13,2.91-25.39,5.47-24.29,5-37.57,6.19-37.71,6.21a7.7,7.7,0,0,0-6.66,5.18l-1,3.06a7.74,7.74,0,0,0,.91,6.82,7.64,7.64,0,0,0,5.95,3.4s2.65.18,8.68.18,16.19-.18,31.43-.9L60.5,204c12.46-.56,15.38-.72,29.21-2.06,2.78,2,7.1,4.34,11.88,4.34A14.06,14.06,0,0,0,111.3,202l41.17,1.64c.47,1.87,1.1,4,1.85,6.22.13.39.28.79.42,1.16l-35.8,6a7.83,7.83,0,0,0-4.43,2.38l-2.23,2.44a7.76,7.76,0,0,0-1.35,8.39,8,8,0,0,0,7.21,4.54l47.52-.93c2.68,2.87,7,6.26,12.43,6.26,5.94,0,10.38-3.82,13.06-7l52.46.69a7.43,7.43,0,0,0,7.19-4.66,7.69,7.69,0,0,0-1.55-8.41l-1.82-1.89a7.82,7.82,0,0,0-4.38-2.27L200.13,210l.31-1.14c.53-2,.95-3.91,1.33-5.62l40.65-1.37a13.81,13.81,0,0,0,10.06,4.33,15.94,15.94,0,0,0,9.92-3.55c7.49.4,26.36,1.36,47.77,2,22.51.73,38.76.75,38.91.75a7.69,7.69,0,0,0,5.75-2.58,7.77,7.77,0,0,0,1.92-6" transform="translate(-6.01 -47)"></path></svg>
						  </div>
						  <div class="o-offer__content">
							<h2 class="o-offer__title">Contrata tu Seguro De viaje</h2>
							<p class="o-offer__text">Elige tu plan de acuerdo al destino, coberturas y cantidad de pasajeros.</p>
							<a class="o-offer__link" href="https://www.seguroscencosud.cl/vseg-paris/desplegar-pagina-intermedia-fraude.do?idRama=6">Contr�talo aqu�</a> </div>
						  <div class="js-image"><img src="img/offer-03.jpg" alt=""></div>
						  <div class="cama-calugas-home"></div>
						</div>
					  </div>
					</div>
              </section>
            </div>
          </div>
        </div>
        <div class="c-why">
          <div class="container">
            <div class="row">
              <div class="col-sm-12">
                <h2 class="o-title o-title--light u-text-center">�Por qu� elegir Seguros Cencosud?</h2>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <div id="reason-slick">
                  <div class="slick_slide">
                    <div class="c-reason"><span class="c-reason__circle"><i class="c-reason__icon mx-user"></i></span>
                      <h3 class="c-reason__title">C�mo obtener un seguro en Seguros Cencosud.</h3>
                      <p class="c-reason__text"> Puedes contratar directamente  en seguroscencosud.cl, tambi�n llamando al 600 500 5000  opci�n 1 o visitando nuestros m�dulos de Seguros en tiendas Paris</p>
                    </div>
                  </div>
                  <div class="slick_slide">
                    <div class="c-reason"><span class="c-reason__circle"><i class="c-reason__icon mx-key"></i></span>
                      <h3 class="c-reason__title">C�mo usar mi seguro de Seguros Cencosud.</h3>
                      <p class="c-reason__text"> Dependiendo de tu seguro y el siniestro podr�s hacer uso con el respectivo procedimiento, revisa tu p�liza para tener m�s informaci�n o llama al 600 500 50000 donde un especialista te guiar�.</p>
                    </div>
                  </div>
                  <div class="slick_slide">
                    <div class="c-reason"><span class="c-reason__circle"><i class="c-reason__icon mx-user"></i></span>
                      <h3 class="c-reason__title">Reembolso de mi seguro en Seguros Cencosud.</h3>
                      <p class="c-reason__text">Si tu seguro es con rembolso podr�s hacer uso de este beneficio con sus respectivo formularios, para m�s informaci�n puedes llamar al 600 500 5000 o dirigirte a nuestros m�dulos de seguros en las tiendas Paris.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>

	<div class="container full">
		<div class="carousel-inner">
		</div>
	</div>
	</div>
	<%@ include file="home/includes/footer.jsp"%>
</body>

</html>