<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page import="cl.tinet.common.struts.form.ValidableActionForm"%>
<bean:define id="contextpath" value="<%=request.getContextPath()%>" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta name="generator" content="cencosud" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="css/lity.css" rel="stylesheet"/>
<script src="js/cotizacion.js"></script>
<script src="js/formularios.js"></script>

<!-- LIGHTBOX -->

<script src="js/jquery.js"></script>
<script src="js/formularios.js"></script>
<script src="js/lity.min.js"></script>
<link rel="stylesheet" href="css/est_form_cot.css"  type="text/css" media="screen" />

<!-- FIN LIGTHBOX -->

<script type="text/javascript">	
		function submitFormulario() {
			window.parent.$("html, body").animate({scrollTop:0}, 'fast');
			
			$('.grupo_nombre :input').each(function() {
				if ($(this).val() == "") {
					$(this).val("");
				}
			});
				var rut = $("#rut").val();
				var dv = $("#dv").val();
				var rutCompleto = rut+""+dv;
				
			if ($("#rut").val() != "" || $("#dv").val() != "" || $("#telefono_1").val().length < 8 || validaRut(rutCompleto) == false){
				
				if (validaRut(rutCompleto) == false){
					$("#ErrorRut").show();
					$("#ErrorRut").text("Rut invalido");
				}
				if ($("#nombre").val() == ""){
					$("#ErrorNombre").show();
					$("#ErrorNombre").text("Ingrese nombre");
				}
				if($("#telefono_1").val().length < 8) {  
					$("#ErrorTelefono").show();
        			$("#ErrorTelefono").text("Numero Telefonico Invalido");
        		}
        	}
        	if (validaRut(rutCompleto) && $("#rut").val() != "" && $("#dv").val() != "" && $("#telefono_1").val().length > 7){
				$(".form-basic").submit();
				}
			
	}

	
		</script>

<script type="text/javascript">
$(document).ready(function(){
	$("#rut").attr("placeholder", "12345678");
	$("#dv").attr("placeholder", "9");
	$("#nombre").attr("placeholder", "Ejemplo: Alejandro");
	$("#telefono_1").attr("placeholder", "22345678");
});
</script>
<script type="text/javascript">
$(document).focusout(function(event){
	$('#dv').focusout(function(event){
		if($("#rut").val() != '' && $("#dv").val() != ''){
			$("#ErrorRut").hide();
		}
	});
	$('#nombre').focusout(function(event){
		if($("#nombre").val() != ''){
			$("#ErrorNombre").hide();
		}
	});
	$('#telefono_1').focusout(function(event){
		if($("#telefono_1").val().length > 7) {
			$("#ErrorTelefono").hide();
		} 
	}); 
});
</script>
</head>

<body>
	<%@ include file="../google-analytics/google-analytics.jsp"%>
	<div class="container">
		<html:form action="/enviar-formulario-cotizador" styleClass="form-basic">
			<div class="form-title-row">
				<h1>
					<strong>Cotiza tu Seguro</strong><br />Pronto nos contáctaremos
					contigo
				</h1>
			</div>
			<div class="form-row">
				<label> 
					<span>RUT*</span> 
				<html:text property="datos(rut)" styleClass="form-ex1" maxlength="8"
					size="15" styleId="rut" onkeypress="return isRutKey(event)" />
				
				<input type="text" name="datos(dv)" maxlength="1" size="1" id="dv" class="form-ex2"/>
				</label>
					<div id="ErrorRut" style="color: red; font-size: 12px;font-weight: 100;">
					</div>
			</div>

			<div class="form-row">
				<label> <span>Nombre*</span> 
					<html:text property="datos(nombre)" size="15" maxlength="15"
						styleId="nombre" onkeypress="return validar(event)" />
				</label>
				<div id="ErrorNombre" style="color: red; font-size: 12px;font-weight: 100;">
				</div>
			</div>
			<div class="form-row">
				<label> <span>Telefono*</span> 
					<html:select property="datos(area_telefono)"
						styleId="area_telefono">
						<html:option value="2">2</html:option>
						<html:option value="35">35</html:option>
						<html:option value="34">34</html:option>
						<html:option value="33">33</html:option>
						<html:option value="09">09</html:option>
						<html:option value="08">08</html:option>
						<html:option value="07">07</html:option>
						<html:option value="06">06</html:option>
					</html:select>
					<html:text property="datos(telefono_1)" size="8" maxlength="8"
						styleId="telefono_1" onkeypress="return isNumberKey(event)"
						styleClass="form-ex1" />
				</label>
				<div id="ErrorTelefono" style="color: red; font-size: 12px;font-weight: 100;">
				</div>
						</div>
			<div class="form-row chico">
				<button type="button" id="btnEnviar" class="btn btn-primary center-block"
					onclick="javascript:submitFormulario()">Enviar</button>
			</div>

			<p>*Campo obligatorio</p>
		</html:form>
	</div>
</body>
</html>