<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<bean:define id="contextpath" value="<%=request.getContextPath()%>" />
<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Maqueta de promociones</title>



<link rel="stylesheet" href="/vseg-paris/css/p7pmm/css/aw_promociones.css" />
</head>

<body>

        <div id="contenedorPromociones_rama">
                <div id="PromocionCentralAuto">
                	<logic:present name="listBannerRamaPpal" >
                	<logic:iterate id="listBanner" name="listBannerRamaPpal" >
                	<logic:equal name="listBanner" property="tipo" value="BPXR">
                	
                    <img src="<bean:write name="listBanner" property="imagen"/>" width="798" height="276" border="0" alt="" usemap="#botones" />
					<map name="botones">
						<area target="_blank" shape"rect" coords="85,180,190,200" href="<bean:write name="contextpath"/><bean:write name="listBanner" property="link_Conocer"/>"  />
						<a onclick="pageTracker._trackEvent(<bean:write name="listBanner" property="tracker"/>);" target='_parent' href="<bean:write name="listBanner" property="link_Cotizar"/>"> <div id="contratar"> </div> </a>
						<area target="_blank" shape"rect" coords="90,230,185,250" href="<bean:write name="contextpath"/><bean:write name="listBanner" property="link_Ver_Bases"/>"  />

					</map>
                	</logic:equal>
                	</logic:iterate>
                	</logic:present>
                </div>
                <!-- AW -->
				
				<div id="tablaPromociones_rama"> 
					<div id="columnaPromociones_auto">
					<!-- auto -->	
					<logic:present name="listBannerRamaPpal">		
					<logic:iterate id="listBanner" name="listBannerRamaPpal">			
					<logic:equal name="listBanner" property="tipo" value="BSXR">
							<div id="calugaPromociones_rama">
								<div id="verBases-promociones"><a target='_blank' href="<bean:write name="contextpath"/><bean:write name="listBanner" property="link_Ver_Bases"/>" target='_blank'>ver bases</a> </div>
								<div id="imgCaluga">	<img src="<bean:write name="contextpath"/><bean:write name="listBanner" property="imagen"/>" width="172" height="127" border="0" alt="">	</div>
								<div id="botonesCaluga"> 
									<a  target='_parent' href="<bean:write name="listBanner" property="link_Conocer"/>"> <div id="conocerMas"> </div> </a>
									<a  onclick="pageTracker._trackEvent(<bean:write name="listBanner" property="tracker"/>);" target='_parent' href="<bean:write name="listBanner" property="link_Cotizar"/>"> <div id="contratar"> </div> </a>
								</div>
							</div>
					</logic:equal>
					</logic:iterate>
					</logic:present>
					</div>
			  </div>
        </div>
		
</body>
</html>
