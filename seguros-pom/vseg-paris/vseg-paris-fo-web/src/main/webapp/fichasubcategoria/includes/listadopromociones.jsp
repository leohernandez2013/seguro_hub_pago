<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<bean:define id="contextpath" value="<%=request.getContextPath()%>" />
<%
	String sIdSubcategoria = (String)request.getAttribute("idSubcategoria");
	sIdSubcategoria = (sIdSubcategoria!=null ? sIdSubcategoria : "-1");
	
	String sIdPlan = (String)request.getAttribute("idPlan");
	sIdPlan = (sIdPlan!=null ? sIdPlan : "-1" );

%>
	<logic:present name="promocionesVehiculo">
	<bean:define id="idRamaInt" value="1"/>
	<div class="estilo-titulo-menu" style="cursor: pointer;" onclick="javascript: top.location.href='desplegar-promociones.do?idRama=<bean:write name="idRamaInt"/>'">
	Promoci�n auto
	</div>
	<div id="menu-seguro-vida-iz">
	<logic:iterate id="subcategoria" name="promocionesVehiculo">
		<!-- -->
		<!-- -->
		<!-- -->
		<div id="btn-desplegado-vida-iz">
			<div class="btn-desplegado-vida-iz-seguro">
				<div class="btn-desplegado-vida-iz_icono-seguro">
					<img src="images/btn-img/icono_flecha_indica_2.jpg" alt=""
						width="11" height="23" border="0" />
				</div>
				<p>
					<logic:equal value="<%=sIdSubcategoria %>" name="subcategoria" property="id_subcategoria" >
						
						<logic:equal value="<%=sIdPlan %>" name="subcategoria" property="id_plan">
							<span><bean:write name="subcategoria" property="nombre_comercial" /></span>
						</logic:equal>
						
						<logic:notEqual value="<%=sIdPlan %>" name="subcategoria" property="id_plan">
							<a href="<bean:write name="contextpath"/>/desplegar-ficha.do?idRama=<bean:write name="idRamaInt" />&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria" />&idPlanPromocion=<bean:write name="subcategoria" property="id_ficha" />"><bean:write name="subcategoria" property="nombre_comercial" /></a>
						</logic:notEqual>
						
					</logic:equal>
					
					<logic:notEqual value="<%=sIdSubcategoria %>" name="subcategoria" property="id_subcategoria" >
						<a href="<bean:write name="contextpath"/>/desplegar-ficha.do?idRama=<bean:write name="idRamaInt" />&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria" />&idPlanPromocion=<bean:write name="subcategoria" property="id_ficha" />"><bean:write name="subcategoria" property="nombre_comercial" /></a>
					</logic:notEqual>
				</p>
			</div>
		</div>
		<!-- -->
		<!-- -->

	</logic:iterate>
	
	<!-- -->
	</div>
	</logic:present>
	
	
	
	
	
	
	
	<logic:present name="promocionesHogar">
	<bean:define id="idRamaInt" value="2"/>
	<div class="estilo-titulo-menu" style="cursor: pointer;" onclick="javascript: top.location.href='desplegar-promociones.do?idRama=<bean:write name="idRamaInt"/>'">
	Promoci�n hogar</div>
	<div id="menu-seguro-vida-iz">
	<logic:iterate id="subcategoria" name="promocionesHogar">
		<!-- -->
		<!-- -->
		<!-- -->
		<div id="btn-desplegado-vida-iz">
			<div class="btn-desplegado-vida-iz-seguro">
				<div class="btn-desplegado-vida-iz_icono-seguro">
					<img src="images/btn-img/icono_flecha_indica_2.jpg" alt=""
						width="11" height="23" border="0" />
				</div>
				<p>
					<logic:equal value="<%=sIdSubcategoria %>" name="subcategoria" property="id_subcategoria" >
						
						<logic:equal value="<%=sIdPlan %>" name="subcategoria" property="id_plan">
							<span><bean:write name="subcategoria" property="nombre_comercial" /></span>
						</logic:equal>
						
						<logic:notEqual value="<%=sIdPlan %>" name="subcategoria" property="id_plan">
							<a href="<bean:write name="contextpath"/>/desplegar-ficha.do?idRama=<bean:write name="idRamaInt" />&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria" />&idPlanPromocion=<bean:write name="subcategoria" property="id_ficha" />"><bean:write name="subcategoria" property="nombre_comercial" /></a>
						</logic:notEqual>
						
					</logic:equal>
					
					<logic:notEqual value="<%=sIdSubcategoria %>" name="subcategoria" property="id_subcategoria" >
						<a href="<bean:write name="contextpath"/>/desplegar-ficha.do?idRama=<bean:write name="idRamaInt" />&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria" />&idPlanPromocion=<bean:write name="subcategoria" property="id_ficha" />"><bean:write name="subcategoria" property="nombre_comercial" /></a>
					</logic:notEqual>
				</p>
			</div>
		</div>
		<!-- -->
		<!-- -->

	</logic:iterate>
	
	<!-- -->
	</div>
	</logic:present>

	
	
	
	
	
	
	
	<logic:present name="promocionesVida">
	<bean:define id="idRamaInt" value="3"/>
	<div class="estilo-titulo-menu" style="cursor: pointer;" onclick="javascript: top.location.href='desplegar-promociones.do?idRama=<bean:write name="idRamaInt"/>'">
	Promoci�n vida</div>
	<div id="menu-seguro-vida-iz">
	<logic:iterate id="subcategoria" name="promocionesVida">
		<!-- -->
		<!-- -->
		<!-- -->
		<div id="btn-desplegado-vida-iz">
			<div class="btn-desplegado-vida-iz-seguro">
				<div class="btn-desplegado-vida-iz_icono-seguro">
					<img src="images/btn-img/icono_flecha_indica_2.jpg" alt=""
						width="11" height="23" border="0" />
				</div>
				<p>
					<logic:equal value="<%=sIdSubcategoria %>" name="subcategoria" property="id_subcategoria" >
						
						<logic:equal value="<%=sIdPlan %>" name="subcategoria" property="id_plan">
							<span><bean:write name="subcategoria" property="nombre_comercial" /></span>
						</logic:equal>
						
						<logic:notEqual value="<%=sIdPlan %>" name="subcategoria" property="id_plan">
							<a href="<bean:write name="contextpath"/>/desplegar-ficha.do?idRama=<bean:write name="idRamaInt" />&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria" />&idPlanPromocion=<bean:write name="subcategoria" property="id_ficha" />"><bean:write name="subcategoria" property="nombre_comercial" /></a>
						</logic:notEqual>
						
					</logic:equal>
					
					<logic:notEqual value="<%=sIdSubcategoria %>" name="subcategoria" property="id_subcategoria" >
						<a href="<bean:write name="contextpath"/>/desplegar-ficha.do?idRama=<bean:write name="idRamaInt" />&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria" />&idPlanPromocion=<bean:write name="subcategoria" property="id_ficha" />"><bean:write name="subcategoria" property="nombre_comercial" /></a>
					</logic:notEqual>
				</p>
			</div>
		</div>
		<!-- -->
		<!-- -->

	</logic:iterate>
	
	<!-- -->
	</div>
	</logic:present>
	
	
	
	
	
	<logic:present name="promocionesSalud">
	<bean:define id="idRamaInt" value="4"/>
	<div class="estilo-titulo-menu" style="cursor: pointer;" onclick="javascript: top.location.href='desplegar-promociones.do?idRama=<bean:write name="idRamaInt"/>'">
	Promoci�n salud</div>
	<div id="menu-seguro-vida-iz">
	<logic:iterate id="subcategoria" name="promocionesSalud">
		<!-- -->
		<!-- -->
		<!-- -->
		<div id="btn-desplegado-vida-iz">
			<div class="btn-desplegado-vida-iz-seguro">
				<div class="btn-desplegado-vida-iz_icono-seguro">
					<img src="images/btn-img/icono_flecha_indica_2.jpg" alt=""
						width="11" height="23" border="0" />
				</div>
				<p>
					<logic:equal value="<%=sIdSubcategoria %>" name="subcategoria" property="id_subcategoria" >
						
						<logic:equal value="<%=sIdPlan %>" name="subcategoria" property="id_plan">
							<span><bean:write name="subcategoria" property="nombre_comercial" /></span>
						</logic:equal>
						
						<logic:notEqual value="<%=sIdPlan %>" name="subcategoria" property="id_plan">
							<a href="<bean:write name="contextpath"/>/desplegar-ficha.do?idRama=<bean:write name="idRamaInt" />&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria" />&idPlanPromocion=<bean:write name="subcategoria" property="id_ficha" />"><bean:write name="subcategoria" property="nombre_comercial" /></a>
						</logic:notEqual>
						
					</logic:equal>
					
					<logic:notEqual value="<%=sIdSubcategoria %>" name="subcategoria" property="id_subcategoria" >
						<a href="<bean:write name="contextpath"/>/desplegar-ficha.do?idRama=<bean:write name="idRamaInt" />&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria" />&idPlanPromocion=<bean:write name="subcategoria" property="id_ficha" />"><bean:write name="subcategoria" property="nombre_comercial" /></a>
					</logic:notEqual>
				</p>
			</div>
		</div>
		<!-- -->
		<!-- -->

	</logic:iterate>
	
	<!-- -->
	</div>
	</logic:present>
	
	
	
	
	
	<logic:present name="promocionesCesantia">
	<bean:define id="idRamaInt" value="5"/>
	<div class="estilo-titulo-menu" style="cursor: pointer;" onclick="javascript: top.location.href='desplegar-promociones.do?idRama=<bean:write name="idRamaInt"/>'">
	Promoci�n cesant�a</div>
	<div id="menu-seguro-vida-iz">
	<logic:iterate id="subcategoria" name="promocionesCesantia">
		<!-- -->
		<!-- -->
		<!-- -->
		<div id="btn-desplegado-vida-iz">
			<div class="btn-desplegado-vida-iz-seguro">
				<div class="btn-desplegado-vida-iz_icono-seguro">
					<img src="images/btn-img/icono_flecha_indica_2.jpg" alt=""
						width="11" height="23" border="0" />
				</div>
				<p>
					<logic:equal value="<%=sIdSubcategoria %>" name="subcategoria" property="id_subcategoria" >
						
						<logic:equal value="<%=sIdPlan %>" name="subcategoria" property="id_plan">
							<span><bean:write name="subcategoria" property="nombre_comercial" /></span>
						</logic:equal>
						
						<logic:notEqual value="<%=sIdPlan %>" name="subcategoria" property="id_plan">
							<a href="<bean:write name="contextpath"/>/desplegar-ficha.do?idRama=<bean:write name="idRamaInt" />&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria" />&idPlanPromocion=<bean:write name="subcategoria" property="id_ficha" />"><bean:write name="subcategoria" property="nombre_comercial" /></a>
						</logic:notEqual>
						
					</logic:equal>
					
					<logic:notEqual value="<%=sIdSubcategoria %>" name="subcategoria" property="id_subcategoria" >
						<a href="<bean:write name="contextpath"/>/desplegar-ficha.do?idRama=<bean:write name="idRamaInt" />&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria" />&idPlanPromocion=<bean:write name="subcategoria" property="id_ficha" />"><bean:write name="subcategoria" property="nombre_comercial" /></a>
					</logic:notEqual>
				</p>
			</div>
		</div>
		<!-- -->
		<!-- -->

	</logic:iterate>
	
	<!-- -->
	</div>
	</logic:present>
	
	
	
	
	
	<logic:present name="promocionesFraude">
	<bean:define id="idRamaInt" value="6"/>
	<div class="estilo-titulo-menu" style="cursor: pointer;" onclick="javascript: top.location.href='desplegar-promociones.do?idRama=<bean:write name="idRamaInt"/>'">
	Promoci�n fraude</div>
	<div id="menu-seguro-vida-iz">
	<logic:iterate id="subcategoria" name="promocionesFraude">
		<!-- -->
		<!-- -->
		<!-- -->
		<div id="btn-desplegado-vida-iz">
			<div class="btn-desplegado-vida-iz-seguro">
				<div class="btn-desplegado-vida-iz_icono-seguro">
					<img src="images/btn-img/icono_flecha_indica_2.jpg" alt=""
						width="11" height="23" border="0" />
				</div>
				<p>
					<logic:equal value="<%=sIdSubcategoria %>" name="subcategoria" property="id_subcategoria" >
						
						<logic:equal value="<%=sIdPlan %>" name="subcategoria" property="id_plan">
							<span><bean:write name="subcategoria" property="nombre_comercial" /></span>
						</logic:equal>
						
						<logic:notEqual value="<%=sIdPlan %>" name="subcategoria" property="id_plan">
							<a href="<bean:write name="contextpath"/>/desplegar-ficha.do?idRama=<bean:write name="idRamaInt" />&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria" />&idPlanPromocion=<bean:write name="subcategoria" property="id_ficha" />"><bean:write name="subcategoria" property="nombre_comercial" /></a>
						</logic:notEqual>
						
					</logic:equal>
					
					<logic:notEqual value="<%=sIdSubcategoria %>" name="subcategoria" property="id_subcategoria" >
						<a href="<bean:write name="contextpath"/>/desplegar-ficha.do?idRama=<bean:write name="idRamaInt" />&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria" />&idPlanPromocion=<bean:write name="subcategoria" property="id_ficha" />"><bean:write name="subcategoria" property="nombre_comercial" /></a>
					</logic:notEqual>
				</p>
			</div>
		</div>
		<!-- -->
		<!-- -->

	</logic:iterate>
	
	<!-- -->
	</div>
	</logic:present>