<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<bean:define id="contextpath" value="<%=request.getContextPath()%>" />
<%
	String sIdSubcategoria = (String)request.getAttribute("idSubcategoria");
	sIdSubcategoria = (sIdSubcategoria!=null ? sIdSubcategoria : "-1");
	
	String sIdPlan = (String)request.getAttribute("idPlan");
	sIdPlan = (sIdPlan!=null ? sIdPlan : "-1" );

%>

<logic:equal value="1" name="idRama">
	<bean:define id="url" value="/vseg-paris/html/comparador-general-ficha_vehiculos.html"/>
<script>
   $(document).ready(function (){
	   			$("#compara").fancybox ({

				  'onStart'   : function() {
			   $("#compara").attr("href","<bean:write name="url"/>");	 
			  },
			    'width'    : 900,
			    'height'   : 1850,
			    'autoScale'   : false,
			    'transitionIn'  : 'none',
			    'transitionOut'  : 'none',
			    'type'    : 'iframe',
		    	'scrolling'  : 'no'
			   });
			  }); 
</script>
</logic:equal>
<logic:equal value="2" name="idRama">
	<bean:define id="url" value="/vseg-paris/html/comparador-general-ficha_hogar.html"/>
<script>
   $(document).ready(function (){
	   			$("#compara").fancybox ({

				  'onStart'   : function() {
			   $("#compara").attr("href","<bean:write name="url"/>");	 
			  },
			    'width'    : 1000,
			    'height'   : 850,
			    'autoScale'   : false,
			    'transitionIn'  : 'none',
			    'transitionOut'  : 'none',
			    'type'    : 'iframe',
		    	'scrolling'  : 'no'
			   });
			  }); 
</script>
</logic:equal>
<logic:equal value="3" name="idRama">
	<bean:define id="url" value="/vseg-paris/html/comparador-general-ficha_vida.html"/>
<script>
   $(document).ready(function (){
	   			$("#compara").fancybox ({

				  'onStart'   : function() {
			   $("#compara").attr("href","<bean:write name="url"/>");	 
			  },
			    'width'    : 700,
			    'height'   : 700,
			    'autoScale'   : false,
			    'transitionIn'  : 'none',
			    'transitionOut'  : 'none',
			    'type'    : 'iframe',
		    	'scrolling'  : 'no'
			   });
			  }); 
</script>
</logic:equal>
<logic:equal value="4" name="idRama">
	<bean:define id="url" value="/vseg-paris/html/comparador-general-ficha_Salud.html"/>
<script>
   $(document).ready(function (){
	   			$("#compara").fancybox ({

				  'onStart'   : function() {
			   $("#compara").attr("href","<bean:write name="url"/>");	 
			  },
			    'width'    : 900,
			    'height'   : 900,
			    'autoScale'   : false,
			    'transitionIn'  : 'none',
			    'transitionOut'  : 'none',
			    'type'    : 'iframe',
		    	'scrolling'  : 'no'
			   });
			  }); 
</script>
</logic:equal>
<logic:equal value="5" name="idRama">
	<bean:define id="url" value="/vseg-paris/html/comparador-general-cesantia.html"/>
<script>	
   $(document).ready(function (){
	   			$("#compara").fancybox ({

				  'onStart'   : function() {
			   $("#compara").attr("href","<bean:write name="url"/>");	 
			  },
			    'width'    : 900,
			    'height'   : 1850,
			    'autoScale'   : false,
			    'transitionIn'  : 'none',
			    'transitionOut'  : 'none',
			    'type'    : 'iframe',
		    	'scrolling'  : 'no'
			   });
			  }); 
</script>
</logic:equal>
<logic:equal value="6" name="idRama">
	<bean:define id="url" value="/vseg-paris/html/comparador-general-fraude.html"/>
<script>
   $(document).ready(function (){
	   			$("#compara").fancybox ({

				  'onStart'   : function() {
			   $("#compara").attr("href","<bean:write name="url"/>");	 
			  },
			    'width'    : 900,
			    'height'   : 1850,
			    'autoScale'   : false,
			    'transitionIn'  : 'none',
			    'transitionOut'  : 'none',
			    'type'    : 'iframe',
		    	'scrolling'  : 'no'
			   });
			  }); 
</script>
</logic:equal>

<script>
//	$(document).ready(function(){
//		$("#compara").colorbox({innerWidth:"60%", innerHeight:"60%", iframe:true, href:"<bean:write name="url"/>"});
//	});
</script>
<logic:present name="promocion">
	<div class="estilo-titulo-menu" style="cursor: pointer;" onclick="javascript: top.location.href='desplegar-promociones.do?idRama=<bean:write name="idRama"/>'">
	<logic:equal value="1" name="idRama">Promoci&oacute;n auto</logic:equal>
	<logic:equal value="2" name="idRama">Promoci&oacute;n hogar</logic:equal>
	<logic:equal value="3" name="idRama">Promoci&oacute;n vida</logic:equal>
	<logic:equal value="4" name="idRama">Promoci&oacute;n salud</logic:equal>
	<logic:equal value="5" name="idRama">Promoci&oacute;n cesant&iacute;a</logic:equal>
	<logic:equal value="6" name="idRama">Promoci&oacute;n fraude</logic:equal>
	</a>
	</div>
</logic:present>
<logic:notPresent name="promocion">
	<div class="estilo-titulo-menu">
		<bean:write name="ramaDesc"/>
	</div>
</logic:notPresent>
<div id="menu-seguro-vida-iz">
	<logic:present name="subcategorias">
	<logic:iterate id="subcategoria" name="subcategorias">
		<!-- -->
		<!-- -->
		<!-- -->
		<div id="btn-desplegado-vida-iz">
			<div class="btn-desplegado-vida-iz-seguro">
				<div class="btn-desplegado-vida-iz_icono-seguro">
					<img src="images/btn-img/icono_flecha_indica_2.jpg" alt=""
						width="11" height="23" border="0" />
				</div>
				<p>
					<logic:equal value="<%=sIdSubcategoria %>" name="subcategoria" property="id_subcategoria" >
						<span><bean:write name="subcategoria" property="titulo_subcategoria" /></span>
					</logic:equal>
					<logic:notEqual value="<%=sIdSubcategoria %>" name="subcategoria" property="id_subcategoria" >
						<a href="<bean:write name="contextpath"/>/desplegar-ficha.do?idRama=<bean:write name="idRama" />&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria" />"><bean:write name="subcategoria" property="titulo_subcategoria" /></a>
					</logic:notEqual>
				</p>
			</div>
		</div>
		<!-- -->
		<!-- -->

	</logic:iterate>
	</logic:present>
	<logic:present name="planesPromocion">
	<logic:iterate id="subcategoria" name="planesPromocion">
		<!-- -->
		<!-- -->
		<!-- -->
		<div id="btn-desplegado-vida-iz">
			<div class="btn-desplegado-vida-iz-seguro">
				<div class="btn-desplegado-vida-iz_icono-seguro">
					<img src="images/btn-img/icono_flecha_indica_2.jpg" alt=""
						width="11" height="23" border="0" />
				</div>
				<p>
					<logic:equal value="<%=sIdSubcategoria %>" name="subcategoria" property="id_subcategoria" >
						
						<logic:equal value="<%=sIdPlan %>" name="subcategoria" property="id_plan">
							<span><bean:write name="subcategoria" property="nombre_comercial" /></span>
						</logic:equal>
						
						<logic:notEqual value="<%=sIdPlan %>" name="subcategoria" property="id_plan">
							<a href="<bean:write name="contextpath"/>/desplegar-ficha.do?idRama=<bean:write name="idRama" />&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria" />&idPlanPromocion=<bean:write name="subcategoria" property="id_ficha" />"><bean:write name="subcategoria" property="nombre_comercial" /></a>
						</logic:notEqual>
						
					</logic:equal>
					
					<logic:notEqual value="<%=sIdSubcategoria %>" name="subcategoria" property="id_subcategoria" >
						<a href="<bean:write name="contextpath"/>/desplegar-ficha.do?idRama=<bean:write name="idRama" />&idSubcategoria=<bean:write name="subcategoria" property="id_subcategoria" />&idPlanPromocion=<bean:write name="subcategoria" property="id_ficha" />"><bean:write name="subcategoria" property="nombre_comercial" /></a>
					</logic:notEqual>
				</p>
			</div>
		</div>
		<!-- -->
		<!-- -->

	</logic:iterate>
	</logic:present>
	

	<logic:notPresent name="promocion">
		<logic:notEqual value="6" name="idRama">
		<logic:notEqual value="5" name="idRama">
	<div id="btn-desplegable-vida-iz_fichas-mas">
		<div class="btn-desplegado-vida-iz_icono">
			<img src="images/btn-img/icono_signo_mas_bla.jpg" alt="" width="11"
				height="20" border="0" />
		</div>
		<p id="compara">
			<a href="javascript:void(0);">Compara los Seguros</a>
		</p>
	</div>
	    </logic:notEqual>
		</logic:notEqual>
	</logic:notPresent>
	<!-- -->
</div>
