
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>

<%@page import="cl.tinet.common.seguridad.servlet.AutenticacionServlet"%>


<logic:present name="cl.tinet.common.seguridad.USUARIO_CONECTADO">
<script type="text/javascript">
<bean:define id="usuario"
		name="<%=SeguridadUtil.USUARIO_CONECTADO_KEY %>" scope="session"
		type="UsuarioExterno" />
</script>
</logic:present>

<script type="text/javascript">
	
	 function SubmitFormulario() {
      userName = document.getElementById("rut_login").value + "-" +  document.getElementById("dv_login").value;
      clave =    document.getElementById("clave").value;
      document.formularioLogin.username.value = userName;
      document.formularioLogin.password.value = clave;
      document.forms.formularioLogin.submit();  
   }
   
   $(document).ready(function (){
   	
			$("#registroUsuario").fancybox ({
				  'onStart'   : function() {
			    var rut_cliente = "";
				var dv_cliente = "";
				  
				var url = '<html:rewrite action="/registroCliente" module="/registrocliente"/>';
				var params = "?rut_cliente=" + rut_cliente;
				params += "&dv_cliente=" + dv_cliente;
				
			   $("#registroUsuario").attr("href",url+params);
				   
			  },
			    'width'    : 650,
			    'height'   : 900,
			    'autoScale'   : false,
			    'transitionIn'  : 'none',
			    'transitionOut'  : 'none',
			    'type'    : 'iframe',
		    'scrolling'  : 'no' 
			   });
			    
			//Se envia formulario al presionar enter.
			$('#clave').keypress(function(e){
				if(e.which == 13){
					e.preventDefault();
				    SubmitFormulario()
					return false;
				}
			});
   });
   
   function obtenerclave() {
			    document.formularioRecuperarClave.rut.value = document.formularioLogin.rut.value;
			    document.formularioRecuperarClave.dv.value =  document.formularioLogin.dv.value; 
			    document.formularioRecuperarClave.submit();
			}
   
</script>

<!-- Formularios Ocultos -->
	<form name="formularioRecuperarClave" method="post" action="cambio-clave/recuperar-clave.do">
		<input type="hidden" name="rut"  />
		<input type="hidden" name="dv" />
	</form>
	
<logic:notPresent name="usuario">			

<form action="/vseg-paris/Login" method="post" name="formularioLogin">
			<input type="hidden" name="username" value="">
			<input type="hidden" name="password" value="">
			
		<input type="hidden" name="cl.tinet.common.seguridad.servlet.URI_EXITO" id="cl.tinet.common.seguridad.servlet.URI_EXITO" />
		<input type="hidden" name="cl.tinet.common.seguridad.servlet.URI_FRACASO" id="cl.tinet.common.seguridad.servlet.URI_FRACASO"/>
		
		<input type="hidden" name="idRama_ficha" />
		<input type="hidden" name="idSubcategoria_ficha" />
		
		<script type="text/javascript">
			$(document).ready(function(){
				var id_rama = "<%= request.getParameter("idRama") %>";
				var id_subcat = "<%= request.getParameter("idSubcategoria") %>";
				
				var url_pagina = "/desplegar-ficha.do?idRama=" + id_rama + "&idSubcategoria=" + id_subcat;
				
				document.getElementById("cl.tinet.common.seguridad.servlet.URI_EXITO").value = url_pagina;
				document.getElementById("cl.tinet.common.seguridad.servlet.URI_FRACASO").value = url_pagina;
				
				<% request.getSession().setAttribute("idRama_ficha", request.getParameter("idRama"));%>
				<% request.getSession().setAttribute("idSubcategoria_ficha", request.getParameter("idSubcategoria"));%>
				
			});
		</script>
		
<!-- 
	<div id="login_in_ti"><img src="images/imagenes/titulo-mis-seguro-inte.png" alt="" width="172" height="25" border="0" /></div>
	
	<div id="login_dat_in">
	<div id="login_dat_in_per"><p><bean:message bundle="labels-seguridad" key="labels.usuario.rut"/></p></div>
	<div id="login_dat_in_tex"><input type="text" class="textbox" name="rut_login"  id="rut_login" size="8"  maxlength="8"/>
	- </div>
	<div class="personal_login_rut_b">
	  <input name="dv_login" id="dv_login" type="text" class="box-rut-verificador" value="" size="1" maxlength="1" />
	  </div>
    </div>
	
	<div id="login_dat_in">
	<div id="login_dat_in_per"><p><bean:message bundle="labels-seguridad" key="labels.usuario.clave"/></p></div>
	<div id="login_dat_in_tex"><input type="password" class="textbox" name="clave"  id="clave" size="8" maxlength="6"/></div>
	<div id="login_dat_in_ir"><a onfocus="blur();" href="javascript:javascript:SubmitFormulario();"><img src="images/btn-img/ir-in-btn-log.gif"onmouseover="this.src = 'images/btn-img/ir-in-btn-log-hover.gif'"  onmouseout="this.src = 'images/btn-img/ir-in-btn-log.gif'"  alt="" width="28" height="19" border="0" /></a></div>
	</div>
	
	<div id="login_dat_in_obten"><p><img src="images/btn-img/flecha-login-home.gif" alt="" width="7" height="9" border="0" /><a href="javascript:void(0);" style="cursor: pointer;"><span id="registroUsuario">Obt�n tu Clave</span></a></p> <p><img src="images/btn-img/flecha-login-home.gif" alt="" width="7" height="9" border="0" /><a id="olvidasteClave" style="cursor: pointer;" href="javascript:void(0);">�Olvidaste tu Clave?</a></p></div>
	 -->
	 <!-- Caja de logueo -->
						
	<div id="p7PMM03contenedorMenu2">
		<div id="p7PMM_1" class="p7PMMh03 p7PMMnoscript">
	       <ul class="p7PMM">					      	
	         <li><a href="#"> <span style="font-family: Arial, verdana; text-indent: 60px; *text-indent: 0px; padding-top: 12px; font-size: 15px; color: #56a9d5; font-weight: bold; padding-top: 4px;"> Mis seguros </span> <b style="color: #666;">Online</b> </a>							
	          <div>
	           <ul>						    
	            <li><a href="#"  >Rut: <input name="rut_login" id="rut_login" type="text" value="" size="8" maxlength="8"/> 
	             - <input name="dv_login" id="dv_login" type="text" value="" style="width:10px" size="1" maxlength="1"/> 					              
		         &nbsp clave: <input name="clave" id="clave" type="password" value="" size="5" /> 
			    <input id="ir03" value="ir" style="border:0; background-repeat: no-repeat; text-align:center;" /><a onfocus="blur();" href="#" onclick="javascrtip:SubmitFormulario();"/></a>
				 </li>					              
				<li> <span class="fancy iframe"
					style="cursor: pointer; float:left; text-indent: 23px; *margin-top:0px"
					 id="registroUsuario"><img width="9" height="10" src="css/p7pmm/img/linkk.png" alt="" />Obt&eacute;n tu Clave</span>						        
					<span class="fancy iframe"
					style="cursor: pointer; float:left; text-indent: 23px; *margin-top:0px"
					href="javascript:void(0);" id="olvidasteClave"><img width="9" height="10" src="css/p7pmm/img/linkk.png" alt="" />�olvidaste tu clave?</span>
				</li>					
				</ul>
				</div>
				</li>			        
				</ul>
				<div class="p7pmmclearfloat">&nbsp;</div>
			</div>
		</div>						
						
		<!-- <div id="fondo"> </div> -->
						
<!-- Fin caja de logueo -->	
	</form>
	
	<!--login-->
	
	<script type="text/javascript">

			/*$("#olvidasteClave").click(function(){
			
			    var url = "/vseg-paris/recuperar-clave.do";
				var params = "?rut=" + $("#rut_login").val();
				params += "&dv=" + $("#dv_login").val();
				
				$(this).colorbox({
				     iframe:true, 
				     width:"450px", 
				     height:"260px", 
				     href: url + params,
				     close: ""
			    });
				
			});*/
			
			$(document).ready(function() {	
			$("#olvidasteClave").fancybox({ 
		    'onStart'   : function() {
					var url = "/vseg-paris/recuperar-clave.do";
					//var params = "?rut=" + $("#rut_login").val();
					//	params += "&dv=" + $("#dv_login").val();
					var params = "?rut=";
					params += "&dv=";

		     $("#olvidasteClave").attr("href",url+params);
		     
		    },
		    'width'    : 430,
		    'height'   : 130,
		    'autoScale'   : false,
		    'transitionIn'  : 'none',
		    'transitionOut'  : 'none',
		    'type'    : 'iframe',
		    'scrolling'  : 'no' 
		  });
			});

		</script>
		
</logic:notPresent>

<logic:present name="usuario">

	<!--login-->
<!--	<div id="login">-->
<!--	<div id="login_in_ti"><img src="images/imagenes/titulo-mis-seguro-inte.png" alt="" width="172" height="25" border="0" /></div>-->
        	<!-- -->
<!--	<div id="login_dat_in">-->
<!--	  <div class="saludo-login2">Bienvenido</div>--%>
<!--	  <div class="nombre-login"><a onfocus="blur();" href="/vseg-paris/secure/inicio/inicio.do"><img src="images/btn-img/btn_ingresar_home.gif" onmouseover="this.src = 'images/btn-img/btn_ingresar_home_hover.gif'" border="0" onmouseout="this.src = 'images/btn-img/btn_ingresar_home.gif'" alt="" width="70" height="22" /></a></div>-->
<!--    </div>-->
	<!-- -->
<!--	<div class="datos-login2">-->
<!--	  <div class="textos2-login"><a href="/vseg-paris/secure/inicio/inicio.do">&gt; Mis Seguros</a></div>-->
<!--	  </div>-->
<!--	<div class="datos-login2">-->
<!--	  <div class="textos2-login"><a href="/vseg-paris/secure/inicio/cotizaciones.do">&gt; Mis Cotizaciones</a></div>-->
<!--	  </div>-->
	<div id="p7PMM03contenedorMenu2">
	    <div id="p7PMM_1" class="p7PMMh03 p7PMMnoscript">
	      <ul class="p7PMM">
	      	<!-- Vehiculo -->
	         <li><a> <span style="font-family: Arial, verdana; text-indent: 60px; *text-indent: 0px; padding-top: 12px; font-size: 15px; color: #56a9d5; font-weight: bold; padding-top: 4px;"> Mis seguros </span> <b style="color: #666;">Online</b> </a>
			
	          <div>
		  
				
					<ul>
						<li style="height:50px;*margin-top: 0px;"> 
						
							
							<a href="/vseg-paris/secure/inicio/inicio.do" style="padding: 2px 2px 2px 57px;">
							<img width="9" height="10" border="0" onmouseout="this.src = 'css/p7pmm/img/linkk.png'" onmouseover="this.src = 'css/p7pmm/img/linkk.png'" src="css/p7pmm/img/linkk.png"> Mis Seguros
							</a>
						
						
						
							<a  href="/vseg-paris/secure/inicio/cotizaciones.do" style="padding: 2px 2px 2px 57px;">
							<img width="9" height="10" border="0" onmouseout="this.src = 'css/p7pmm/img/linkk.png'" onmouseover="this.src = 'css/p7pmm/img/linkk.png'" src="css/p7pmm/img/linkk.png"> Mis Cotizaciones
							</a>
							
							
							<a href="/vseg-paris/secure/inicio/inicio.do"  style="margin-top: -37px; margin-left: 143px;" >
							
								<img width="62" height="18" border="0" onmouseout="this.src = 'images/ingresar.png'" onmouseover="this.src = 'images/ingresarOver.png'" src="images/ingresar.png">
							</a>
						</li>
					</ul>
	          </div>
	        </li>
		</ul>
	      <div class="p7pmmclearfloat">&nbsp;</div>
	    </div>
	</div>
</logic:present>


<logic:present name="<%=AutenticacionServlet.ERROR_AUTENTICACION_KEY%>">
<!--BOX PROCESO -->

<div id="errorlogin">
<div align="center" style="padding-top:25px;height:168px;" >
	<div id="estamos_procesando">
		
			<a onclick="javascript:parent.$.fn.colorbox.close();$('#errorlogin').hide();"></a>
		
		<div class="pantalla-error_cuerpo">
			<div id="conte_estamos_procesando">
				<div class="icono_geeral">
					<img src="/vseg-paris/images/img/icono_error.png" alt="" width="46" height="46" border="0" />
				</div>
				<div id="texto_procesando">
					<h3>
						<bean:write name="<%=AutenticacionServlet.ERROR_AUTENTICACION_KEY%>" ignore="true" />
						<br />
					</h3>
				</div>
			</div>
		</div>
	</div>
</div>
	<!--BOX PROCESO FIN-->
</div>

<script type="text/javascript">
		//$(document).ready(function(){
		//	$("#errorlogin").show();
		//	$.fn.colorbox({
		//			 inline: true, 
		//		     width:"750", 
		//		     height:"550", 
		//	     href: "#errorlogin"
		//	});
		//});
		
		$(document).ready(function(){
			$('<a href="#errorlogin" class="login_error"></a>').appendTo('body');
			
			$("a.login_error").fancybox({ 
			    'hideOnContentClick': false,
				'frameWidth' : 470,
				'frameHeight' : 185,
		    	'type':'inline',
			'scrolling'  : 'no' 
			  });
			$("#fancybox-close").click(function(){
				$("#errorlogin").css("display","none");
			});
			$("a.login_error").click();
		});
</script>
</logic:present>

<!--div id="banner-columna-der-01" style="padding-bottom:18px;">
		<logic:present name="tipoSeguro">
			<logic:notEqual name="tipoSeguro" value="1"> 
				<a onfocus="blur();" href="https://www.seguroscencosud.cl/asesor/index.html#/robo-documentos"><img
						src="images/banner/banner-que-seguro-m-c.jpg" alt="" width="172"
						onmouseover="this.src = 'images/banner/banner-que-seguro-m-c-hover.jpg'"
						onmouseout="this.src = 'images/banner/banner-que-seguro-m-c.jpg'"
						height="127" border="0" /> </a>
			</logic:notEqual>
		</logic:present>
		<logic:present name="idRama">
			<logic:notEqual name="idRama" value="1"> 
				<a onfocus="blur();" href="https://www.seguroscencosud.cl/asesor/index.html#/robo-documentos"><img
						src="images/banner/banner-que-seguro-m-c.jpg" alt="" width="172"
						onmouseover="this.src = 'images/banner/banner-que-seguro-m-c-hover.jpg'"
						onmouseout="this.src = 'images/banner/banner-que-seguro-m-c.jpg'"
						height="127" border="0" /> </a>
			</logic:notEqual>
		</logic:present>
</div-->								
									