<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<bean:define id="contextpath" value="<%=request.getContextPath()%>" />


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="generator" content="Cencosud" />

		<logic:equal name="idRama" value="1">
			<meta name="description" lang="es" content=" Cotiza y Contrata Online tu seguro en Seguros Cencosud. Asegura tu Auto, Casa, Vida y Salud con las promociones m�s convenientes y asesor�a " />
			<title> Promoci�n Seguro de Auto | Seguros Cencosud </title>
		</logic:equal>
		<logic:notEqual name="idRama" value="1">
			<title>Cotizar Seguro | Seguros Cencosud</title>
		</logic:notEqual>
		
		<link type="image/x-icon" rel="shortcut icon" href="images/btn-img/favicon.ico" />
		<script type="text/javascript"
			src="js/jquery/jquery.fancybox/jquery-1.3.2.min.js"></script>
<!-- Rodrigo 09-11-2011-->
        <link rel="stylesheet" type="text/css" href="css/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
		<script type="text/javascript" src="css/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<script type="text/javascript" src="js/jquery/jquery-1.4.2.min.js"></script> 
		<!--		<script>-->
<!--  		!window.jQuery && document.write('<script src="css/fancybox/jquery-1.4.3.min.js"><\/script>');-->
<!--		</script>-->
<!-- Fin Rodrigo 09-11-2011-->
		<link rel="stylesheet" href="css/estilo-index.css" type="text/css"
			media="all" />
		<link rel="stylesheet" href="css/nubes.css" type="text/css"
			media="screen" />
		<script type="text/javascript"
			src="js/jquery/jquery.fancybox/jquery-1.3.2.min.js"></script>
		<link rel="stylesheet" href="css/jquery/jqModal.css" type="text/css" />
		<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet"
			href="js/jquery/jquery.fancybox/jquery.fancybox.css" type="text/css" />

		<link rel="stylesheet" href="css/estilos-light-box.css"
			type="text/css" media="all" />
		<link href="css/formulario-r.css" rel="stylesheet" type="text/css" />

		<script type="text/javascript" src="js/jquery/jquery.min.js"></script>
		<script src="js/jquery/jquery-1.4.2.js"></script>

		<link rel="stylesheet" href="css/header_multisitio.css"  type="text/css" media="screen" />
		<script type="text/javascript" src="js/jquery/jquery.colorbox.js"></script>
		
		<script type="text/javascript" src="js/acordeon.js"></script>	
		<link href="css/SpryAccordion.css" rel="stylesheet" type="text/css" />
	
		<link href="css/acordionTeAyudamos.css" rel="stylesheet" type="text/css" />
		
		<script type="text/javascript" src="css/p7pmm/p7PMMscripts.js"></script>

        <link href="css/p7pmm/p7PMMh02.css" rel="stylesheet" type="text/css" media="all" />
        
        <!-- ------------------------------------------------------------ -->
        
        <!-- cabecera -->

<!--<link rel="stylesheet" href="css/cabecera.css" />-->

<!-- mis seguros online -->
<link rel="stylesheet" href="css/p7pmm/css/miseguros.css" />

<!-- preparacion pagina -->
<link rel="stylesheet" href="css/p7pmm/css/aw_promociones_settings.css" />

<!-- cotizar-->
<link rel="stylesheet" href="css/p7pmm/cotizar/cotizar.css" />






<!-- pestana -->


<link href="css/p7pmm/css/document.css" rel="stylesheet" type="text/css" />
<link href="css/p7pmm/css/jquery.bubblepopup.v2.3.1.css" rel="stylesheet" type="text/css" />
<script src="css/p7pmm/js/jquery.min.js" type="text/javascript"></script>
<script src="css/p7pmm/js/jquery.bubblepopup.v2.3.1.min.js" type="text/javascript"></script>


<!-- SLIDER *** -->

<link rel="stylesheet" href="css/p7pmm/css/aw_promociones.css" />

<script type="text/javascript" src="css/p7pmm/js/aw.js"></script>

    <!-- AW (SLIDER) -->
    
    <script type="text/javascript">

	$(document).ready(function()
	{
		$("#showcase").awShowcase(
		{
			content_width:			798,
			content_height:			276,
			fit_to_parent:			false,
			auto:					true,
			interval:				5000,
			continuous:				true,
			loading:				true,
			//tooltip_width:		200,
			//tooltip_icon_width:	32,
			//tooltip_icon_height:	32,
			//tooltip_offsetx:		18,
			//tooltip_offsety:		0,
			arrows:				    false,
			buttons:				true,
			btn_numbers:			true,
			keybord_keys:			false,
			mousetrace:				false, /* Trace x and y coordinates for the mouse */
			pauseonover:			true,
			stoponclick:			false,
			transition:				'fade', /* hslide/vslide/fade */
			transition_delay:		0,
			transition_speed:		750,
			show_caption:			'onload', /* onload/onhover/show */
			thumbnails:				false,
			thumbnails_position:	'outside-last', /* outside-last/outside-first/inside-last/inside-first */
			thumbnails_direction:	'vertical', /* vertical/horizontal */
			thumbnails_slidex:		1, /* 0 = auto / 1 = slide one thumbnail / 2 = slide two thumbnails / etc. */
			dynamic_height:			false, /* For dynamic height to work in webkit you need to set the width and height of images in the source. Usually works to only set the dimension of the first slide in the showcase. */
			speed_change:			true, /* Set to true to prevent users from swithing more then one slide at once. */
			viewline:				false, /* If set to true content_width, thumbnails, transition and dynamic_height will be disabled. As for dynamic height you need to set the width and height of images in the source. */
			custom_function:		null /* Define a custom function that runs on content change */
		});
	});
	
	</script>
	   
	
	<!-- acordion -->
	
	<link rel="stylesheet" href="css/p7pmm/css/acordeon.css" />
	
	
	<script type="text/javascript" src="css/p7pmm/js/lib/chili-1.7.pack.js"></script>
	
	<script type="text/javascript" src="css/p7pmm/js/lib/jquery.easing.js"></script>
	<script type="text/javascript" src="css/p7pmm/js/lib/jquery.dimensions.js"></script>
	<script type="text/javascript" src="css/p7pmm/js/jquery.accordion.js"></script>

	<script type="text/javascript">
	jQuery().ready(function(){
		
		
		// highly customized accordion
		jQuery('#list2').accordion({
			event: 'mouseover',
			active: '.selected',
			selectedClass: 'active',
			animated: 'easeslide',
			header: "dt"
		}).bind("change.ui-accordion", function(event, ui) {
			jQuery('<div>' + ui.oldHeader.text() + ' hidden, ' + ui.newHeader.text() + ' shown</div>').appendTo('#log');
		});
		
		
		
		var wizard = $("#wizard").accordion({
			header: '.title',
			event: false
		});
		
		var wizardButtons = $([]);
		$("div.title", wizard).each(function(index) {
			wizardButtons = wizardButtons.add($(this)
			.next()
			.children(":button")
			.filter(".next, .previous")
			.click(function() {
				wizard.accordion("activate", index + ($(this).is(".next") ? 1 : -1))
			}));
		});
		
		// bind to change event of select to control first and seconds accordion
		// similar to tab's plugin triggerTab(), without an extra method
		var accordions = jQuery('#list2');
		
		jQuery('#switch select').change(function() {
			accordions.accordion("activate", this.selectedIndex-1 );
		});
		jQuery('#close').click(function() {
			accordions.accordion("activate", -1);
		});
		jQuery('#switch2').change(function() {
			accordions.accordion("activate", this.value);
		});
		jQuery('#enable').click(function() {
			accordions.accordion("enable");
		});
		jQuery('#disable').click(function() {
			accordions.accordion("disable");
		});
		jQuery('#remove').click(function() {
			accordions.accordion("destroy");
			wizardButtons.unbind("click");
		});
		
		
		
		
	});
	</script>
		
		

	<script type="text/javascript">
	var clear="../images/ayudamos/clear.gif"; //path to clear.gif
	function MM_swapImgRestore() { //v3.0
	var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
	}
	function MM_preloadImages() { //v3.0
	var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
	var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
	if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
	}

	function MM_findObj(n, d) { //v4.01
	var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
	if(!x && d.getElementById) x=d.getElementById(n); return x;
	}

	function MM_swapImage() { //v3.0
	var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
	}
	</script>

	<!--<script type="text/javascript" src="/vseg-paris/js/unitpngfix.js"></script> -->

	<link href="css/te-ayudamos.css" rel="stylesheet" type="text/css" />
    


		<script type="text/javascript"> 
 
    $(document).ready(function() {
 
	//Default Action
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content
	
	//On Click Event
	$("ul.tabs li").click(function() {
		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content
		var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
		$(activeTab).show(); //Fade in the active content
		return false;
	});
 
    });
    </script>

		

		<style type="text/css" media="all">
<!--
.dsR1 /*agl rulekind: base;*/ {
	width: 11px;
	height: 23px;
}
-->
</style>


</head>

	<body>
		<%@ include file="../google-analytics/google-analytics.jsp"%>
		<!-- INICIO CONTENIDO -->
		<div id="content">
			<!-- Incio HEADER -->

			<%@ include file="includes/ficha-header-promocion.jsp"%>

			<%@ include file="includes/menuprincipalpromociones.jsp"%>

		</div>
		</div>
		<div>
		<!-- FIN HEADER -->
		<!-- INCIO CONTENIDO CENTRAL -->
		<div id="cuerpo-central">

		<!--	<div id="curva-topcont-secciones2">

				<img src="images/ayudamos/background-curva-contenido-secciones.png"
					alt="" width="982" height="12" border="0" />

			</div>-->

			<div id="contenido-empresas" style="margin-top:12px;">

				<div id="menu-iz-teayudamos">



					<!-- menu desplegable vida izquerdo -->

					<%@ include file="includes/listadopromociones2.jsp"%>

					<!-- FINmenu desplegable vida izquerdo -->

					<div class="estilo-titulo-menu">
						<!-- Te Ayudamos -->
					</div>
					<div id="menu-seguro-vida-iz">
						<!-- -->
						<!-- -->
						<!-- -->

						<div id="btn-desplegado-vida-iz">
							<div class="btn-desplegado-vida-iz-seguro">
								<!-- <div class="btn-desplegado-vida-iz_icono-seguro">
									<img src="images/btn-img/flecha_indica_gris_teayudamos.gif"
										alt="" width="11" height="23" border="0" />
								</div>
								<p>
									<a href="/vseg-paris/html/preguntas-frecuentes.html"
										id="enlace_preg_frec">Preguntas Frecuentes</a>
								</p> -->
							</div>
						</div>
						<div id="btn-desplegado-vida-iz3">
							<div class="btn-desplegado-vida-iz-seguro2">
								<!-- <div class="btn-desplegado-vida-iz_icono-seguro">
									<img src="images/btn-img/flecha_indica_gris_teayudamos.gif"
										alt="" width="11" height="23" border="0" />
								</div>
								<p>
									<a href="/vseg-paris/html/caso-siniestro.html"
										id="enlace_siniestro">Qu� Hacer en Caso de Siniestro</a>
								</p> -->
							</div>
						</div>
						<!-- -->
						<!-- -->
						<!-- -->
						<!-- -->
						<div id="btn-desplegado-vida-iz">
							<div class="btn-desplegado-vida-iz-seguro">
								<!-- <div class="btn-desplegado-vida-iz_icono-seguro">
									<img src="images/btn-img/flecha_indica_gris_teayudamos.gif"
										alt="" width="11" height="23" border="0" />
								</div>
								<p>
									<a href="/vseg-paris/html/glosario.html">Glosario</a>
								</p> -->
							</div>
						</div>
						<div id="btn-desplegado-vida-iz4">
							<div class="btn-desplegado-vida-iz-seguro">
								<!-- <div class="btn-desplegado-vida-iz_icono-seguro">
									<img src="images/btn-img/flecha_indica_gris_teayudamos.gif"
										alt="" width="11" height="23" border="0" />
								</div>
								<p>
									<a href="/vseg-paris/html/polizas.html" id="enlace_poliza">P�liza</a>s
								</p> -->
							</div>
						</div>
						<div id="btn-desplegado-vida-iz5">
							<div class="btn-desplegado-vida-iz-seguro">
								<!-- <div class="btn-desplegado-vida-iz_icono-seguro">
									<img src="images/btn-img/flecha_indica_gris_teayudamos.gif"
										alt="" width="11" height="23" border="0" />
								</div>
								<p>
									<a href="/vseg-paris/Sucursales/">Nuestras
										Sucursales</a>
								</p> -->
							</div>
						</div>
						<div id="btn-desplegado-vida-iz6">
							<div class="btn-desplegado-vida-iz-seguro2">
								<!-- <div class="btn-desplegado-vida-iz_icono-seguro">
									<img src="images/btn-img/flecha_indica_gris_teayudamos.gif"
										alt="" width="11" height="23" border="0" />
								</div>
								<p>
									<a href="http://www.svs.cl" target="_blank">Superintendencia
										de Valores y Seguros</a>
								</p> -->
							</div>
						</div>
						<div id="btn-desplegado-vida-iz">
    <div class="btn-desplegado-vida-iz-seguro">
	        <!-- <div class="btn-desplegado-vida-iz_icono-seguro"><img src="/vseg-paris/images/btn-img/flecha_indica_gris_teayudamos.gif" alt="" width="11" height="23" border="0" /></div>
	        <p><a href="/vseg-paris/formulario-contacto.do">Cont�ctanos</a></p> -->
	</div>
	</div>
						<!-- -->
						<!-- -->
						<!-- -->
					</div>
				</div>
				
				
				<!-- contenido ficha vida -->

				<div id="contenido-chica-vida" style="margin-top:20px;margin-left:14px;">

				<logic:equal name="idRama" value="50">



				<!-- SLIDER CENTRAL -->
				<!-- AW -->
				
				<div>
                <div id="carruselPromociones">
                    <div id="showcase" class="showcase">
                    	<logic:present name="listBannerPpal">
                    		<logic:iterate id="listBanner" name="listBannerPpal">
                    			<div class="showcase-slide">
		                              <div class="productos-carrusel-a">
		                                    <div style="background-image: url('<bean:write name="contextpath"/><bean:write name="listBanner" property="imagen"/>'); width: 798px; height: 276px;"> 
		                                        <div id="aLink1"> 
		                                        	<a href="<bean:write name="listBanner" property="link_Ver_Bases"/>" onclick="pageTracker._trackEvent(<bean:write name="listBanner" property="tracker"/>);" > 
		                                        		<img src="css/p7pmm/img/link.png" alt="" width="768" height="276" border="0" /> 
		                                        	</a> 
		                                        </div>
		                              		</div>
		                              </div>
		                        </div>
                    		</logic:iterate>
                    	</logic:present>
                    </div>
				</div>	

            <div id="tablaPromociones"> 
				
					
				
					<div id="tituloPromociones" style="float:left;"> <a href="/vseg-paris/desplegar-promociones.do?idRama=1"> Auto </a> </div>
					<div id="tituloPromociones" style="float:left;"> <a href="/vseg-paris/desplegar-promociones.do?idRama=2"> Hogar </a> </div>
					<div id="tituloPromociones" style="float:left;"> <a href="/vseg-paris/desplegar-promociones.do?idRama=3"> Vida y Salud </a> </div>

					
					
					<!-- auto -->						
					<logic:present name="listBannerSec">
					
					<div id="columnaPromociones">
						<logic:iterate id="listBanner" name="listBannerSec">
						<logic:equal value="1" name="listBanner" property="rama">
							<div id="calugaPromociones">
								<div id="verBases-promociones"> <a target="_blank" href="<bean:write name="listBanner" property="link_Ver_Bases"/>">ver bases</a> </div>
								<div id="imgCaluga">	<img src="<bean:write name="contextpath"/><bean:write name="listBanner" property="imagen"/>" width="172" height="127" border="0" alt="">	</div>
								<div id="botonesCaluga"> 
									<a  href="<bean:write name="listBanner" property="link_Conocer"/>"> <div id="conocerMas"> </div> </a>
									<a  onclick="pageTracker._trackEvent(<bean:write name="listBanner" property="tracker"/>);" href="<bean:write name="listBanner" property="link_Cotizar"/>"> <div id="contratar"> </div> </a>
								</div>
							</div>
						</logic:equal>	
						</logic:iterate>						
						<div id="verMasPromociones"> <a  href="/desplegar-promociones.do?idRama=1"> Ver m�s promociones >> </a>	</div>
					</div>
					
					
					<div id="columnaPromociones">
						<logic:iterate id="listBanner" name="listBannerSec">
						<logic:equal value="2" name="listBanner" property="rama">
							<div id="calugaPromociones">
							<logic:notEqual value="-" name="listBanner" property="link_Ver_Bases">
								<div id="verBases-promociones"> <a target="_blank" href="<bean:write name="listBanner" property="link_Ver_Bases"/>">ver bases</a> </div>
							</logic:notEqual>
								<div id="imgCaluga">	<img src="<bean:write name="contextpath"/><bean:write name="listBanner" property="imagen"/>" width="172" height="127" border="0" alt="">	</div>
								<div id="botonesCaluga"> 
									<a  href="<bean:write name="listBanner" property="link_Conocer"/>"> <div id="conocerMas"> </div> </a>
									<a  onclick="pageTracker._trackEvent(<bean:write name="listBanner" property="tracker"/>);" href="<bean:write name="listBanner" property="link_Cotizar"/>"> <div id="contratar"> </div> </a>
								</div>
							</div>
						</logic:equal>	
						</logic:iterate>						
							<!--<div id="verMasPromociones"> <a  href="/vseg-paris/desplegar-promociones.do?idRama=2"> Ver m�s promociones >> </a>	</div>							-->
					</div>
					
					
					<div id="columnaPromocionesDer">
						<logic:iterate id="listBanner" name="listBannerSec">
						<logic:equal value="3" name="listBanner" property="rama">
							<div id="calugaPromociones">
								<div id="verBases-promociones"> <a target="_blank" href="<bean:write name="listBanner" property="link_Ver_Bases"/>">ver bases</a> </div>
								<div id="imgCaluga">	<img src="<bean:write name="contextpath"/><bean:write name="listBanner" property="imagen"/>" width="172" height="127" border="0" alt="">	</div>
								<div id="botonesCaluga"> 
									<a  href="<bean:write name="listBanner" property="link_Conocer"/>"> <div id="conocerMas"> </div> </a>
									<a  onclick="pageTracker._trackEvent(<bean:write name="listBanner" property="tracker"/>);" href="<bean:write name="listBanner" property="link_Cotizar"/>"> <div id="contratar"> </div> </a>
								</div>
							</div>
						</logic:equal>
						</logic:iterate>						
							<!--<div id="verMasPromociones"> <a  href="/vseg-paris/desplegar-promociones.do?idRama=3"> Ver m�s promociones >> </a>	</div>							-->
					</div>
					
					</logic:present>
					
				
			</div>
					</logic:equal>
					
					<logic:equal name="idRama" value="1">
						 <div id="contenedorPromociones_rama">
				                <div id="PromocionCentralAuto">
				                	<logic:present name="listBannerRamaPpal" >
				                	<logic:iterate id="listBanner" name="listBannerRamaPpal" >
				                	<logic:equal name="listBanner" property="tipo" value="BPXR">
										<a onclick="pageTracker._trackEvent(<bean:write name="listBanner" property="tracker"/>);" target='_parent' href="<bean:write name="listBanner" property="link_Ver_Bases"/>">
										<img src="<bean:write name="contextpath"/><bean:write name="listBanner" property="imagen"/>" width="798" height="276" border="0" alt="" usemap="#botones" />
										</a>
				                	</logic:equal>
				                	</logic:iterate>
				                	</logic:present>
				                </div>
				                <!-- AW -->
								
								<div id="tablaPromociones_rama"> 
									<div id="columnaPromociones_auto">
									<!-- auto -->	
									<logic:present name="listBannerRamaPpal">		
									<logic:iterate id="listBanner" name="listBannerRamaPpal">			
									<logic:equal name="listBanner" property="tipo" value="BSXR">
											<div id="calugaPromociones_rama">
												<div id="verBases-promociones"><a target='_blank' href="<bean:write name="listBanner" property="link_Ver_Bases"/>" target='_blank'>ver bases</a> </div>
												<div id="imgCaluga">	<img src="<bean:write name="contextpath"/><bean:write name="listBanner" property="imagen"/>" width="172" height="127" border="0" alt="">	</div>
												<div id="botonesCaluga"> 
													<a  target='_parent' href="<bean:write name="listBanner" property="link_Conocer"/>"> <div id="conocerMas"> </div> </a>
													<a  onclick="pageTracker._trackEvent(<bean:write name="listBanner" property="tracker"/>);" target='_parent' href="<bean:write name="listBanner" property="link_Cotizar"/>"> <div id="contratar"> </div> </a>
												</div>
											</div>
									</logic:equal>
									</logic:iterate>
									</logic:present>
									</div>
							  </div>
				        </div>
					</logic:equal>
					<logic:equal name="idRama" value="2">
						<div id="contenedorPromociones_rama">
				                <div id="PromocionCentralAuto">
				                	<logic:present name="listBannerRamaPpal" >
				                	<logic:iterate id="listBanner" name="listBannerRamaPpal" >
				                	<logic:equal name="listBanner" property="tipo" value="BPXR">
				                	<a onclick="pageTracker._trackEvent(<bean:write name="listBanner" property="tracker"/>);" target='_parent' href="<bean:write name="listBanner" property="link_Ver_Bases"/>">
										<img src="<bean:write name="contextpath"/><bean:write name="listBanner" property="imagen"/>" width="798" height="276" border="0" alt="" usemap="#botones" />
										</a>
				                	</logic:equal>
				                	</logic:iterate>
				                	</logic:present>
				                </div>
				                <!-- AW -->
								
								<div id="tablaPromociones_rama"> 
									<div id="columnaPromociones_auto">
									<!-- auto -->	
									<logic:present name="listBannerRamaPpal">		
									<logic:iterate id="listBanner" name="listBannerRamaPpal">			
									<logic:equal name="listBanner" property="tipo" value="BSXR">
											<div id="calugaPromociones_rama">
												<div id="verBases-promociones"><a target='_blank' href="<bean:write name="listBanner" property="link_Ver_Bases"/>" target='_blank'>ver bases</a> </div>
												<div id="imgCaluga">	<img src="<bean:write name="contextpath"/><bean:write name="listBanner" property="imagen"/>" width="172" height="127" border="0" alt="">	</div>
												<div id="botonesCaluga"> 
													<a  target='_parent' href="<bean:write name="listBanner" property="link_Conocer"/>"> <div id="conocerMas"> </div> </a>
													<a  onclick="pageTracker._trackEvent(<bean:write name="listBanner" property="tracker"/>);" target='_parent' href="<bean:write name="listBanner" property="link_Cotizar"/>"> <div id="contratar"> </div> </a>
												</div>
											</div>
									</logic:equal>
									</logic:iterate>
									</logic:present>
									</div>
							  </div>
				        </div>
					</logic:equal>
					<logic:equal name="idRama" value="3">
						<div id="contenedorPromociones_rama">
				                <div id="PromocionCentralAuto">
				                	<logic:present name="listBannerRamaPpal" >
				                	<logic:iterate id="listBanner" name="listBannerRamaPpal" >
				                	<logic:equal name="listBanner" property="tipo" value="BPXR">
				                	<a onclick="pageTracker._trackEvent(<bean:write name="listBanner" property="tracker"/>);" target='_parent' href="<bean:write name="listBanner" property="link_Ver_Bases"/>">
										<img src="<bean:write name="contextpath"/><bean:write name="listBanner" property="imagen"/>" width="798" height="276" border="0" alt="" usemap="#botones" />
										</a>
				                	</logic:equal>
				                	</logic:iterate>
				                	</logic:present>
				                </div>
				                <!-- AW -->
								
								<div id="tablaPromociones_rama"> 
									<div id="columnaPromociones_auto">
									<!-- auto -->	
									<logic:present name="listBannerRamaPpal">		
									<logic:iterate id="listBanner" name="listBannerRamaPpal">			
									<logic:equal name="listBanner" property="tipo" value="BSXR">
											<div id="calugaPromociones_rama">
												<div id="verBases-promociones"><a target='_blank' href="<bean:write name="listBanner" property="link_Ver_Bases"/>" target='_blank'>ver bases</a> </div>
												<div id="imgCaluga">	<img src="<bean:write name="contextpath"/><bean:write name="listBanner" property="imagen"/>" width="172" height="127" border="0" alt="">	</div>
												<div id="botonesCaluga"> 
													<a  target='_parent' href="<bean:write name="listBanner" property="link_Conocer"/>"> <div id="conocerMas"> </div> </a>
													<a  onclick="pageTracker._trackEvent(<bean:write name="listBanner" property="tracker"/>);" target='_parent' href="<bean:write name="listBanner" property="link_Cotizar"/>"> <div id="contratar"> </div> </a>
												</div>
											</div>
									</logic:equal>
									</logic:iterate>
									</logic:present>
									</div>
							  </div>
				        </div>
					</logic:equal>
					<logic:equal name="idRama" value="4">
						<div id="contenedorPromociones_rama">
				                <div id="PromocionCentralAuto">
				                	<logic:present name="listBannerRamaPpal" >
				                	<logic:iterate id="listBanner" name="listBannerRamaPpal" >
				                	<logic:equal name="listBanner" property="tipo" value="BPXR">
				                    <a onclick="pageTracker._trackEvent(<bean:write name="listBanner" property="tracker"/>);" target='_parent' href="<bean:write name="listBanner" property="link_Ver_Bases"/>">
										<img src="<bean:write name="contextpath"/><bean:write name="listBanner" property="imagen"/>" width="798" height="276" border="0" alt="" usemap="#botones" />
										</a>
				                	</logic:equal>
				                	</logic:iterate>
				                	</logic:present>
				                </div>
				                <!-- AW -->
								
								<div id="tablaPromociones_rama"> 
									<div id="columnaPromociones_auto">
									<!-- auto -->	
									<logic:present name="listBannerRamaPpal">		
									<logic:iterate id="listBanner" name="listBannerRamaPpal">			
									<logic:equal name="listBanner" property="tipo" value="BSXR">
											<div id="calugaPromociones_rama">
												<div id="verBases-promociones"><a target='_blank' href="<bean:write name="listBanner" property="link_Ver_Bases"/>" target='_blank'>ver bases</a> </div>
												<div id="imgCaluga">	<img src="<bean:write name="contextpath"/><bean:write name="listBanner" property="imagen"/>" width="172" height="127" border="0" alt="">	</div>
												<div id="botonesCaluga"> 
													<a  target='_parent' href="<bean:write name="listBanner" property="link_Conocer"/>"> <div id="conocerMas"> </div> </a>
													<a  onclick="pageTracker._trackEvent(<bean:write name="listBanner" property="tracker"/>);" target='_parent' href="<bean:write name="listBanner" property="link_Cotizar"/>"> <div id="contratar"> </div> </a>
												</div>
											</div>
									</logic:equal>
									</logic:iterate>
									</logic:present>
									</div>
							  </div>
				        </div>
					</logic:equal>
					<logic:equal name="idRama" value="5">
						<iframe frameborder="0" scrolling="auto" height="810" width="830" src="/vseg-paris/html/promociones_cesantia.html"></iframe>
					</logic:equal>
					<logic:equal name="idRama" value="6">
						<iframe frameborder="0" scrolling="auto" height="810" width="830" src="/vseg-paris/html/promociones_fraude.html"></iframe>
					</logic:equal> 
				
				<!-- FIN contenido ficha vida -->
				<!-- contenido ficha VIDA-->
				<!-- FIN contenido ficha VIDA-->

			</div>
		</div>
		<!-- FIN CONTENIDO CENTRAL -->
		<!-- curva-booton -->
		<div id="curva-bottoncont-secciones"></div>
		<div class="companias">
			<img src="images/imagenes/companias.jpg" width="932" height="40" />
		</div>
		<div id="footer">
			<%@ include file="../home/includes/footer.jsp"%>
			<div id="footer-copiright"  style="*margin-top: 10px;">
				<h5>
					Copyright Seguros &amp; Servicios 2010. Todos los derechos
					reservados.
				</h5>
			</div>
			<div id="footer-copiright2">
				<h5>
					<!-- footer FIN -->
				</h5>
			</div>
		</div>
		<!-- FINcurva-booton -->
		<!-- footer -->
		</div>
		<!-- FIN INICIO CONTENIDO -->
		</div>
	</body>

</html>