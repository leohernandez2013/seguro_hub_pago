<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<!--BOX PROCESO -->
<html>
	<head>
		<title>No autorizado</title>
		<script src="<%= request.getContextPath() %>/js/jquery/jquery-1.4.2.js"></script>
		<link href="<%= request.getContextPath() %>/css/formulario-ver.css" rel="stylesheet" type="text/css" />
        <!--[if lt IE 7]>
        		<script type="text/javascript" src="/vseg-paris/js/unitpngfix.js"></script>
	    <![endif]-->
	</head>
	<body>
		<div  style="display: none;">
		<div id="errorlogin">
			<div align="center" style="padding-top: 50px;">
				<div id="estamos_procesando">
					<div id="curba_top_proceso">
						<a
							onclick="javascript:history.back();"><img
								src="<%= request.getContextPath() %>/images/img_light_box/btn-cerrar.gif"
								onmouseover="this.src = '<%= request.getContextPath() %>/images/img_light_box/btn-cerrar-hover.gif'"
								onmouseout="this.src = '<%= request.getContextPath() %>/images/img_light_box/btn-cerrar.gif'"
								alt="" width="39" height="39" border="0" /> </a>
					</div>
					<div class="pantalla-error_cuerpo">
						<div id="conte_estamos_procesando">
							<div class="icono_geeral">
								<img src="<%= request.getContextPath() %>/images/img/icono_error.png" alt=""
									width="46" height="46" border="0" />
							</div>
							<div id="texto_procesando">
								<h3>
									No cuenta con los permisos suficientes.
									<br />
								</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--BOX PROCESO FIN-->
		</div>
		</div>
		
		<script type="text/javascript">
		/*$(document).ready(function(){
			$("#errorlogin").show();
			$.fn.colorbox({
					 inline: true, 
				     width:"100%", 
				     height:"100%", 
				     href: "#errorlogin",
			});
		});*/
		
		$(document).ready(function(){
			$('<a href="#errorlogin" class="login_error"></a>').appendTo('body');
			
			$("a.login_error").fancybox({ 
			    'hideOnContentClick': false,
				'frameWidth' : 470,
				'frameHeight' : 185,
		    	'type'  : 'inline',
			'scrolling'  : 'no'
			  });
			$("#fancybox-close").click(function(){
				$("#errorlogin").css("display","none");
			});
			$("a.login_error").click();
		});
	</script>
	</body>
</html>
