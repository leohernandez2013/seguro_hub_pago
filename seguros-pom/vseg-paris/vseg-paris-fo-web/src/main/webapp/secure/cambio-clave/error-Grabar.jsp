<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>

<bean:define id="contextpath" value="<%=request.getContextPath()%>" />

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html:html lang="true">
  <head>
    <html:base />
	<!--[if lt IE 7]>
		<script type="text/javascript" src="/vseg-paris/js/unitpngfix.js"></script>
	<![endif]-->
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link href="/cotizador/css/estilos.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/cotizador/css/base.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/cotizador/css/jquery-ui.css" rel="stylesheet" type="text/css" media="all" />		
	<script src="/vseg-paris/js/jquery/jquery-1.4.2.js"></script>
  </head>
  
  <body>
	 <div align="center">
	 <!--BOX PROCESO -->
		<div id="container" style="margin-top:90px">
			<div id="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<!--h3><bean:message key="labels.cambio.clave.titulo" bundle="labels-seguridad"/></h3-->
					<!--h4><logic:present name="MensajeError"> <bean:write name="MensajeError" /></logic:present></h4-->
					<h4>La clave nueva debe coincidir con la de repetici&oacute;n</h4>
				</div>
				<div class="row">
					<button class="btn btn-primary center-block" onclick="javascript:history.back()">Volver</button>
				</div>
		</div>
	 <!--BOX PROCESO FIN-->	
	 </div>
  </body>
</html:html>
