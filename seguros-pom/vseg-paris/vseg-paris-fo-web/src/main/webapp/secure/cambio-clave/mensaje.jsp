<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html:html lang="true">
  <head>
    <html:base />
    
    <%
    String path = request.getContextPath();
    String basePath =
        request.getScheme() + "://" + request.getServerName() + ":"
            + request.getServerPort() + path + "/";
  %>
    
    <title>Recuperar Clave</title>

	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link href="<%=request.getContextPath()%>/css/formulario-r.css" rel="stylesheet" type="text/css" />

  </head>
  
  <body>
  
  <body>
	<div align="center">
	<!--BOX PROCESO -->
	<div id="estamos_procesando">
	<div id="curba_top_proceso"><a href="javascript:parent.$.fn.colorbox.close();"><img src="<%=request.getContextPath()%>/images/img_light_box/btn-cerrar.gif"onmouseover="this.src = '<%=request.getContextPath()%>/images/img_light_box/btn-cerrar-hover.gif'" border="0" onmouseout="this.src = '<%=request.getContextPath()%>/images/img_light_box/btn-cerrar.gif'" border="0" alt="" width="39" height="39" border="0" /></a></div>
	<div class="pantalla-exito_cuerpo">
	<div id="conte_estamos_procesando">
	<div class="icono_geeral"><img src="<%=request.getContextPath()%>/imagenes/img_light_box/icono_ok.gif" alt="" width="46" height="46" border="0" /></div>

	<div id="texto_procesando">
	   <h3> <span> <bean:message key="labels.correo.exito" bundle="labels-seguridad"/> </span> </h3> <strong><p> <bean:write  name="correo" /> </strong> 
	  </div>					
	</div>
	</div>
	</div>
	<!--BOX PROCESO FIN-->
	<!--BOX PROCESO FIN-->
	</div>	
	

  
 
    
  </body>
</html:html>
