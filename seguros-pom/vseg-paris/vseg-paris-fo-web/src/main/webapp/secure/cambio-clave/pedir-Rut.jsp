<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html:html lang="true">
  <head>
    <html:base />
    
    <title>Cambio de Clave</title>

	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link href="<%=request.getContextPath()%>/css/formulario-r.css" rel="stylesheet" type="text/css" />

  </head>  
  
  <body>
  
  <!--BOX PROCESO -->
  <div align="center">
	<div id="estamos_procesando">
	<div id="curba_top_proceso"><a href="javascript:parent.$.fn.colorbox.close();"></a></div>
	<div class="pantalla-exito_cuerpo">
	<div id="conte_estamos_procesando">
	
	<div class="icono_geeral"> <div class="bloue_datos">  </div>
	</div>
	     <div class="bloue_datos">
	        <div id="formulario-registro">
		    	<div id="formulario-registro_titulo"><h1><bean:message key="labels.usuario.rut.ingresar" bundle="labels-seguridad"/></h1></div>
			</div>

		<div class="bloue_datos">
		<div class="bloue_datos_personal">
		<p>Rut:</p>
		</div>
		<div class="bloue_datos_llenar" style="width:150px;">
		 <html:form action="/generar-clave-temporal" method="post" >
          <table border="0">
            <tr>
              <td> <html:text property="datos(rut)" styleClass="textbox-con-rut"/> - <html:text maxlength="1"  size="1" property="datos(dv)" styleClass="textbox-con-rut-n" /> </td>
           </tr>
          <tr>
		        <td>
		         <html:submit value="enviar"></html:submit>
		        </td>
		        <td>
		         <h4>  <html:errors/> </h4>
		        </td>
		        
          </tr>
         </table>
       </html:form>
		</div>
		</div>
	     
	
	
	
	</div>
	</div>
	</div>
	</div>
  
   </div>
  </body>
</html:html>
