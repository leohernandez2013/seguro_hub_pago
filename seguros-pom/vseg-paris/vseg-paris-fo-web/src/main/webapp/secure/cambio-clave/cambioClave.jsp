<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html:html lang="true">
  <head>
  
    <html:base />
    
    <title>Cambio de Clave</title>

	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  <html:errors/>
    <html:form action="/cambio-clave-grabar.do" method="post" focus="">
      <table border="0">
        <tr>
          <td><bean:message key="labels.usuario.clave.temporal" bundle="labels-seguridad"/>  </td>
          <td><html:text property="datos(claveTemporal)"/></td>
        </tr>
        <tr>
          <td><bean:message key="labels.usuario.clave.nueva" bundle="labels-seguridad"/> :</td>
          <td><html:password property="datos(claveNueva)" />
          </td>
        </tr>
        <tr>
          <td colspan="2" align="center"><html:submit/></td>
        </tr>
      </table>
    </html:form>
  </body>
</html:html>
