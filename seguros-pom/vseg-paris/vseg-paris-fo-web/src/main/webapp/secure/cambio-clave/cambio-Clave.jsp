<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<%@page import="cl.tinet.common.struts.form.ValidableActionForm"%>

<bean:define id="contextpath" value="<%=request.getContextPath()%>"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>

	<title>Cambio de Clave</title>

	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	
	<link href="/cotizador/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/cotizador/css/estilos.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/cotizador/css/base.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/cotizador/css/jquery-ui.css" rel="stylesheet" type="text/css" media="all" />		
	<script src="<bean:write name="contextpath"/>/js/jquery/jquery-1.4.2.js"></script>
	<script>
		
		function cambiarClave(){
			if (!validateInputRut("validaForm")) {
					return false;
				}else{
					$("#cambiarClaveForm").submit();
				}
		}
		
		function validateInputRut(id) {
			validacionInput = 0;
			console.log('in validate input: ' + id);
			$('#'+id+'').find(":input:not(:hidden,:button,:submit)").each(function(){
				console.log('testing ' + this.id);
				var optional = $(this).hasClass("optional");
				if(optional==false) {
					$(this).css("border-color","#cecece");
					if(!this.value) {
						$("#incomplete-text-"+id).html("Por favor complete el formulario correctamente");
						$("#incomplete-"+id).show("fast");
						$(this).css("border-color","red");
						$(this).focus();
						console.log(this.id + ' est� vac�o');
						validacionInput = 1;
						return false;
					} 
					if($(this).attr("mail")) {
						console.log("testing mail");
						var result = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(this.value);
						if(result == false) {
							$("#incomplete-text-"+id).html("El correo tiene el formato incorrecto");
							$("#incomplete-"+id).show("fast");
							$(this).css("border-color","red");
							$(this).focus();
							console.log(this.id + ' formato incorrecto');
							validacionInput = 1;
							return false;
						} 
					}

				}
				
			});
			
			if(validacionInput == 0) {
				$("#incomplete-text-"+id).html("");
				$("#incomplete-"+id).hide("fast");
				return true;
			}else{
				return false;
			}
		}
	</script>
	
</head>
<body>
	<div class="container">
		<html:form action="/cambio-clave-grabar.do" method="post"
			styleId="cambiarClaveForm">
			<logic:present name="esCambioClaveTemporal">
				<html:hidden property="esCambioClaveTemporal" value="true" />
			</logic:present>
			
			<!--div id="formulario-registro_titulo2" style="width: 200px;">
				<h1>
					<bean:message key="labels.cambio.clave.tiulo.cambio.clave"
						bundle="labels-seguridad" />
				</h1>
			</div-->
			<div id="validaForm">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<label>
							<logic:present name="esCambioClaveTemporal">
								<bean:message key="labels.usuario.clave.temporal"
									bundle="labels-seguridad"/>*
							</logic:present>
						</label>
						<label>
						<logic:notPresent name="esCambioClaveTemporal">
							<bean:message key="labels.usuario.clave.actual"
								bundle="labels-seguridad"/>*
						</logic:notPresent>
						</label>
							<html:password property="datos(claveTemporal)"
								styleClass="textbox-con-rut form-control" maxlength="6"
								styleId="datos.claveTemporal" size="10" />
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
						<h5 style="color:#fb7f03;">
							<html:errors property="datos.claveTemporal" />
						</h5>
					</div>
				</div>		
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<label>
							<bean:message key="labels.usuario.clave.nueva"
								bundle="labels-seguridad"/>*
						</label>
							<html:password property="datos(claveNueva)"
								styleClass="textbox-con-rut form-control" maxlength="6"
								styleId="datos.claveNueva" size="10" />
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
						<h5 style="color:#fb7f03;">
							<html:errors property="datos.claveNueva" />
						</h5>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<label>
							<bean:message key="labels.usuario.clave.nueva2"
								bundle="labels-seguridad"/>*
						</label>
						<html:password property="datos(claveNueva2)"
									styleClass="textbox-con-rut form-control" maxlength="6"
									styleId="datos.claveNueva2" size="10" />
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
						<h5 style="color:#fb7f03;">
							<html:errors property="datos.claveNueva2" />
						<h5>
					</div>
				</div>
				<div class="row top10">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">
						<p>Tu clave debe contener 6 caracteres
							<span style="font-style: italic;">(ej. abc123)</span>.
						</p>
					</div>
				</div>
				<div class="row top15">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<button type="button" class="btn btn-primary center-block" onclick="javascript:cambiarClave();">CAMBIAR CLAVE</button>
					</div>
				</div>			
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
						<html:errors property="datos.rutdv" />
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-right">
						<logic:present name="MensajeError">* <bean:write
								name="MensajeError" />
						</logic:present>
					</div>
				</div>
			</div>
		</html:form>
	</div>
</body>
</html:html>
