<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>

<logic:present name="cl.tinet.common.seguridad.USUARIO_CONECTADO">
	<script type="text/javascript">
		<bean:define id="usuario" name="<%=SeguridadUtil.USUARIO_CONECTADO_KEY %>" 
		scope="session" type="UsuarioExterno" />
	</script>
</logic:present>

<!-- INICIO  RESUMEN -->
<logic:notPresent name="usuario">
	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3-fluid">
		<div class="row bg-resumen hidden-xs">
			<h4>Resumen</h4>
			
        <div class="p-resumen">
            <p class="text-center">�Estas registrado?<br>
              Ahorra tiempo cotizando con tu clave</p>
          </div>
        <div class="col-lg-1 col-xs-1"></div>
        <div class="col-lg-10 col-xs-10">     
          <form action="/cotizador/Login" method="post" name="loginForm">
			<input type="hidden" name="cl.tinet.common.seguridad.servlet.URI_EXITO" value="/desplegar-cotizacion.do?idRama=<bean:write name="idRama"/>&idSubcategoria=<bean:write name="idSubcategoria"/>&idTipo=<bean:write name="idTipo"/>" />
			<input type="hidden" name="cl.tinet.common.seguridad.servlet.URI_FRACASO" value="/desplegar-cotizacion.do?idRama=<bean:write name="idRama"/>&idSubcategoria=<bean:write name="idSubcategoria"/>&idTipo=<bean:write name="idTipo"/>"/>
            <div class="form-group">
              <label for="username" class="control-label hidden-lg hidden-xs">Username</label>
              <input type="text" class="form-control" name="username" id="username" placeholder="Usuario" size="10" maxlength="10">
              <logic:present name="<%=AutenticacionServlet.ERROR_AUTENTICACION_KEY%>">
	              <span class="input-group-addon">
	    			<i data-toggle="tooltip" title="Mensaje de error" data-container="body">Agregue su Usuario</i>
	  			  </span>
  			  </logic:present>
              <span class="help-block"></span> </div>
            <div class="form-group">
              <label for="password" class="control-label hidden-lg hidden-xs">Password</label>
              <input type="password" class="form-control" placeholder="Ingresa tu Contrase�a" name="password" id="password" maxlength="6">
              <logic:present name="<%=AutenticacionServlet.ERROR_AUTENTICACION_KEY%>">
	              <span class="input-group-addon">
	    			<i data-toggle="tooltip" title="Mensaje de error" data-container="body">Agregue su Contase�a</i>
	  			  </span>
  			  </logic:present>
              <span class="help-block"></span> </div>
            <div class="col-lg-5 col-xs-5 help-block-login p0 text-center"><a href="/vseg-paris/registrocliente/registroCliente.do?rut_cliente=&dv_cliente=">Obten tu Clave</a></div>
            <div class="col-lg-7 col-xs-7 help-block-login p0 text-center"><a href="/vseg-paris/recuperar-clave.do?rut=&dv=">Olvidaste tu contrase�a?</a></div>
            <button type="submit" class="btn btn-primary btn-block">Login</button>
          </form>
        </div>
        <div class="col-lg-1 col-xs-1"></div>
		
			<div class="col-lg-12 col-xs-12 top15 text-center p0 hidden-xs"
				id="resumen-banner">
				<a
					href="/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=22&idTipo=1"><img
					src="img/banner-promocional.jpg">
				</a>
			</div>
		</div>
		<div id="resumen-content"></div>
		<!-- RESUMEN OCULTO MOBILE -->


		<div class="row bg-resumen hidden-lg hidden-sm hidden-md">
			<div class="panel-group" id="resumen" role="tablist"
				aria-multiselectable="true">
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#resumen" href="#collapseResumen" aria-expanded="true" aria-controls="collapseResumen">
          �Cotizar con mi cuenta!
        </a>
      </h4>
    </div>
					<div id="collapseResumen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <p class="text-center">�Estas registrado?<br>
              Ahorra tiempo cotizando con tu clave</p>
          </div>
        <div class="col-lg-1 col-xs-1"></div>
        <div class="col-lg-10 col-xs-10">     
          <form action="/cotizador/Login" method="post" name="loginForm">
          	<input type="hidden" name="cl.tinet.common.seguridad.servlet.URI_EXITO" value="/desplegar-cotizacion.do?idRama=<bean:write name="idRama"/>&idSubcategoria=<bean:write name="idSubcategoria"/>&idTipo=<bean:write name="idTipo"/>" />
			<input type="hidden" name="cl.tinet.common.seguridad.servlet.URI_FRACASO" value="/desplegar-cotizacion.do?idRama=<bean:write name="idRama"/>&idSubcategoria=<bean:write name="idSubcategoria"/>&idTipo=<bean:write name="idTipo"/>"/>
            <div class="form-group">
              <label for="username" class="control-label hidden-lg hidden-xs">Username</label>
              <input type="text" class="form-control" name="username" id="username" placeholder="Usuario" size="10" maxlength="10">
              <logic:present name="<%=AutenticacionServlet.ERROR_AUTENTICACION_KEY%>">
	              <span class="input-group-addon">
	    			<i data-toggle="tooltip" title="Mensaje de error" data-container="body">Agregue su Usuario</i>
	  			  </span>
  			  </logic:present>
              <span class="help-block"></span> </div>
            <div class="form-group">
              <label for="password" name="password" id="password" maxlength="6" class="control-label hidden-lg hidden-xs">Password</label>
              <input type="password" class="form-control" placeholder="Ingresa tu Contrase�a">
              <logic:present name="<%=AutenticacionServlet.ERROR_AUTENTICACION_KEY%>">
	              <span class="input-group-addon">
	    			<i data-toggle="tooltip" title="Mensaje de error" data-container="body">Agregue su Contrase�a</i>
	  			  </span>
  			  </logic:present>
              <span class="help-block"></span> </div>
            <div class="col-lg-5 col-xs-5 help-block-login p0 text-center"><a href="/vseg-paris/registrocliente/registroCliente.do?rut_cliente=&dv_cliente=">Obten tu Clave</a></div>
            <div class="col-lg-7 col-xs-7 help-block-login p0 text-center"><a href="/vseg-paris/recuperar-clave.do?rut=&dv=">Olvidaste tu contrase�a?</a></div>
            <button type="submit" class="btn btn-primary btn-block">Login</button>
          </form>
        </div>
        <div class="col-lg-1 col-xs-1 bot15"></div>
      </div>
				</div>
			</div>
		</div>
	</div>
</logic:notPresent>
<logic:present name="usuario">
	<!--div class="col-lg-3 col-md-3 col-sm-3 col-xs-3-fluid">
		<div class="row bg-resumen">
			<h4 class="hidden-xs">Resumen</h4>
			<div class="row">
				<div class="p-resumen">
					<div class="col-md-12">
						<div class="col-md-3 col-sm-3 col-xs-3">
							<img src="img/cliente.jpg" class="img-cliente">
						</div>
						<div class="col-md-9 col-sm-9 col-xs-9 pleft">
							<div class="col-md-4 col-xs-4 p0">
								<p class="m0">Bienvenido</p>
							</div>
							<div class="col-md-8 col-xs-8 pright">
								<p class="text-right m0">
									<a href="#"><span class="glyphicon glyphicon-remove"
										aria-hidden="true"></span> Cerrar</a>
								</p>
							</div>
							<p>
								<strong>Juan Perez Perez</strong>
							</p>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12 p0">
							<div class="separacion-hor-cont-lateral hidden-xs"></div>
						</div>
						<div class="contenedor-pasos-lateral hidden-xs">
							<div class="col-md-12 col-sm-12 col-xs-12 bg-lateral-on">
								<div class="col-md-4 col-sm-4 col-xs-4">
									<img src="img/icono-dato-vehiculo-activo.png" width="49"
										height="49" class="img-iconos">
								</div>
								<div class="col-md-7 col-sm-7 col-xs-7">
									<p class="titulo-lateral-listado">DATOS DEL VEH�CULO</p>
									<p>
										Autom�vil Mazda 6<br> 2014
									</p>
								</div>
							</div>
							<div class="col-md-12 col-xs-12 bg-lateral-on">
								<div class="col-md-4 col-sm-4 col-xs-4">
									<img src="img/icono-cotizar-seguro-activo.png" width="49"
										height="49" class="img-iconos">
								</div>
								<div class="col-md-7 col-sm-7 col-xs-7">
									<p class="titulo-lateral-listado">COTIZAR SEGURO</p>
									<p>
										S�per Seguros<br> <strong>$20.000</strong> (Monto
										Mensual)<br> <strong>UF 0,85</strong> (Monto Mensual)
									</p>
								</div>
							</div>
							<div class="col-md-12 col-xs-12 bg-lateral-off">
								<div class="col-md-4 col-sm-4 col-xs-4">
									<img src="img/icono-pagar-seguro-off.png" width="49"
										height="49" class="img-iconos">
								</div>
								<div class="col-md-8 col-sm-8 col-xs-8">
									<p class="titulo-lateral-listado">PAGAR SEGURO</p>
								</div>
							</div>
							<div class="col-md-12">
								<div class="separacion-hor2"></div>
								<div class="col-md-4 col-sm-4 col-xs-4">
									<img src="img/icono-cotizar-listo-off.png" width="48"
										height="48" class="img-iconos">
								</div>
								<div class="col-md-8 col-sm-8 col-xs-8">
									<p class="titulo-lateral-listado">�LISTO!</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div-->
</logic:present>

<logic:present name="usuario">
<!-- INICIO  resumen -->
      <div class="row bg-resumen">
        <h4 class="hidden-xs">Resumen</h4>
        <div class="row">
          <div class="p-resumen">
            <div class="col-md-12">
              <div class="col-md-4 col-sm-4 col-xs-4">
			  <!--img src="/vseg-paris/img/cliente.jpg" width="102" height="102" class="img-cliente"-->
			  </div>
              <div class="col-md-8 col-sm-8 col-xs-8">
                <p>Bienvenido<br>
                  <strong><bean:write name="usuario" property="usuarioExterno.nombre" /> <bean:write name="usuario" property="usuarioExterno.apellido_paterno" /> <bean:write name="usuario" property="usuarioExterno.apellido_materno" /></strong></p>
              </div>
              <div class="separacion-hor-cont-lateral"></div>
              <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="col-md-4 col-sm-4 col-xs-4">
                    <p><strong>RUT</strong></p>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-8">
                    <p>
						<bean:write name="usuario" property="usuarioExterno.rut_cliente" />
						- 
						<bean:write name="usuario" property="usuarioExterno.dv_cliente" />
					</p>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-4 col-sm-4 col-xs-4">
                    <p><strong>Direcci�n</strong></p>
                  </div>
                  <div class="col-md-8t col-sm-8 col-xs-8">
					<bean:write name="usuario" property="usuarioExterno.calle" /> 
					<bean:write name="usuario" property="usuarioExterno.numero" />
					<bean:write name="usuario" property="usuarioExterno.numero_departamento" />
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-4 col-sm-4 col-xs-4">
                    <p><strong>Tel�fono</strong></p>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-8">
                    <p><bean:write name="usuario" property="usuarioExterno.telefono_1" /></p>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-4 col-sm-4 col-xs-4">
                    <p><strong>Email</strong></p>
                  </div>
                  <div class="col-md-8 col-sm-8 col-xs-8">
                    <p><bean:write name="usuario" property="usuarioExterno.email" /></p>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 cuadro-textos-resumen centrar-texto">
                  <button type="button" class="btn btn-primary" onclick="javascript: parent.document.location.href = '/vseg-paris/secure/inicio/actualizar-datos-cliente.do';">MODIFICAR PERFIL</button>
                </div>
              </div>
              <div class="separacion-hor"></div>
              <!-- fin row --> 
              
            </div>
          </div>
        </div>
      </div>
      <!-- FIN  resumen --> 
</logic:present>
<!-- FIN  RESUMEN -->
