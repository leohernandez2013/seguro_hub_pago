<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<%@ page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@ page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>
<%@ page import="cl.cencosud.ventaseguros.common.model.Solicitud"%>
<%@ page import="cl.cencosud.ventaseguros.asesorvirtual.model.SolicitudCotizacion"%>

<bean:define id="contextpath" value="<%=request.getContextPath()%>" />

<logic:notEmpty name="<%= SeguridadUtil.USUARIO_CONECTADO_KEY %>">
	<bean:define id="usuario" name="<%=SeguridadUtil.USUARIO_CONECTADO_KEY %>" scope="session" type="UsuarioExterno" />
</logic:notEmpty>

<script>
	function mostrar(id, idPlan) {
		//Buscar coberturas.
		$
				.getJSON(
						"/vseg-paris/secure/inicio/obtener-coberturas.do",
						{
							idPlan : idPlan,
							ajax : 'true'
						},
						function(j) {
							cob = "<div class=\"contenedorseg\">";
							cob += "			<div class=\"restoseguros\">";
							cob += "				<div class=\"conseg\">";
							cob += "					<div class=\"cobpoliza\">";
							cob += "						<div class=\"titcoberturas\">";
							cob += "							coberturas";
							cob += "						</div>";
							cob += "					</div>";

							for ( var i = 0; i < j.length; i++) {

								cob += "									<div class=\"mis-productos-despliegue-dos\">";
								cob += "										<div class=\"mis-produ-seguro-texto-dos\" style=\"width: 500px;\">";
								cob += "											<p>";
								cob += j[i].descripcion;
								cob += "											</p>";

								cob += "										</div>";
								cob += "										<div class=\"mis-produ-seguro-compania-texto-dos\"></div>";
								cob += "									</div>";

							}

							cob += "								</div>";
							cob += "							</div>";
							cob += "						</div>";

							$("#" + id).html(cob);

						});

		var obj = $("#" + id);
		var cls = obj.parent().children(':nth-child(2)').attr('class');
		if (cls == 'contmombreseguro') {
			obj
					.parent()
					.children(':first-child')
					.children(':first-child')
					.attr("src",
							"<bean:write name="contextpath" />/images/btn-img/flecha.celeste.jpg");
			obj.parent().children(':nth-child(2)').attr('class',
					'contnombresegact');
		} else {
			obj
					.parent()
					.children(':first-child')
					.children(':first-child')
					.attr("src",
							"<bean:write name="contextpath" />/images/btn-img/flecha.ploma.jpg");
			obj.parent().children(':nth-child(2)').attr('class',
					'contmombreseguro');
		}
		obj.toggle();
	}
</script>
<!-- INICIO RESPONSIVE MIS SEGUROS -->

<div class="row center-block top15 plan">
	<!-- No hay seguros contratados -->
	<logic:equal value="false" name="tieneSeguros">
		<div class="col-md-6 col-xs-12 panel-body text-center p0">
			<h5 class="btn-block">No cuentas con seguros contratados.</h5>
		</div>
	</logic:equal>
	<div class="col-md-6 col-xs-12 panel-body text-center p0">
		<h5 class="btn-block">No cuentas con seguros contratados.</h5>
	</div>
	<!-- No hay seguros contratados -->
	
	<logic:equal value="true" name="tieneSeguros">
		<%
		//	String rama = "";
		//	String lastRama = "";
			int lastIdRama = -1;
			int idRama = -1;
			boolean espacio = false;
		%>
		<logic:iterate id="seguro" name="solicitudesWS" scope="request" indexId="pos" type="SolicitudCotizacion">
			<logic:equal value="Solicitud por Aprobar" property="estado" name="seguro">
			<!-- 1- INICIO Solicitud por aprobar -->
				<div class="col-md-3 col-xs-12 caja-mi-perfil-2">
					<h3><bean:write name="seguro" property="nombreCompania" /></h3>
				</div>
				<div class="col-md-3 col-xs-6 caja-tit-seg text-center p0">
					<h5 class="btn-block hidden-xs">PLAN</h5>
					<p> <bean:write name="seguro" property="nombrePlan" />
						<br>
						<bean:write name="seguro" property="primaMensual" />
					</p>
				</div>
				<div class="col-md-2 col-xs-6 caja-tit-seg text-center p0">
					<h5 class="btn-block hidden-xs">VIGENCIA</h5>
					<p>DEL <bean:write name="seguro" property="fechaInicioVigencia" /><br>
					AL 29/04/2016</p>
				</div>
				<div class="col-md-2 col-xs-12 caja-tit-seg2 text-center p0">
					<h5 class="btn-block hidden-xs">PRIMA MENSUAL</h5>
					<div class="hidden-lg col-xs-6 p0">
						<h5 class="btn-block">PRIMA MENSUAL</h5>
					</div>
					<div class="hidden-lg col-xs-6">
						<p>$ 0.</p>
					</div>
					<p class="hidden-xs">$ 0.</p>
				</div>
				<div class="col-md-2 col-xs-12 p0">
					<button type="button" class="btn btn-primary caja-mi-perfil-4b btn-block" 
					id="poliza_<bean:write name="seguro" property="numeroSolicitud"/>>VER P&oacute;LIZA</button>
					<script>
						$("#poliza_"+ <bean:write name="seguro" property="numeroSolicitud"/>)
							.click(function() {
								verPoliza(this.id);
							});
					</script>
				</div>
			<!-- 1- FIN Solicitud por aprobar -->
			</logic:equal>
			<logic:equal value="Vigente" property="estado" name="seguro">
			<!-- 2- INICIO Solicitud por aprobar -->
				<div class="col-md-3 col-xs-12 caja-mi-perfil-2">
					<h3><bean:write name="seguro" property="nombreCompania" /></h3>
				</div>
				<div class="col-md-3 col-xs-6 caja-tit-seg text-center p0">
					<h5 class="btn-block hidden-xs">PLAN</h5>
					<p> <bean:write name="seguro" property="nombrePlan" />
						<br>
						<bean:write name="seguro" property="primaMensual" />
					</p>
				</div>
				<div class="col-md-2 col-xs-6 caja-tit-seg text-center p0">
					<h5 class="btn-block hidden-xs">VIGENCIA</h5>
					<p>DEL <bean:write name="seguro" property="fechaInicioVigencia" /><br>
					AL 29/04/2016</p>
				</div>
				<div class="col-md-2 col-xs-12 caja-tit-seg2 text-center p0">
					<h5 class="btn-block hidden-xs">PRIMA MENSUAL</h5>
					<div class="hidden-lg col-xs-6 p0">
						<h5 class="btn-block">PRIMA MENSUAL</h5>
					</div>
					<div class="hidden-lg col-xs-6">
						<p>$ 0.</p>
					</div>
					<p class="hidden-xs">$ 0.</p>
				</div>
				<div class="col-md-2 col-xs-12 p0">
					<button type="button" class="btn btn-primary caja-mi-perfil-4b btn-block"
						id="poliza_<bean:write name="seguro" property="numeroSolicitud"/>">VER P&oacute;LIZA</button>
					<script>
						$("#poliza_"+ <bean:write name="seguro" property="numeroSolicitud"/>)
							.click(function() {
								verPoliza(this.id);
							});
					</script>
				</div>
			<!-- 2- FIN Solicitud por aprobar -->
			</logic:equal>
		</logic:iterate>
	</logic:equal>
</div>

<!-- FIN RESPONSIVE MIS SEGUROS -->

<!--  columna secciones -->

<div id="seccion-columna-der" style="height: 0px; padding-bottom: 0px;">


	<!--login-->
	<script type="text/javascript">
		function asesorVirtual(rut) {
			$("#rutVsegParis").val(rut);
			$("#asesorForm").submit();
		}
	</script>
	<form action="/vseg-paris/asesor-virtual/asesor-virtual.jsp" method="POST" name="asesorForm" id="asesorForm" target="_blank">
		<!--  <div id="banner-columna-der-01">
					<a onfocus="blur();" href="javascript:asesorVirtual('<logic:present name="usuario"><bean:write name="usuario" property="rut_cliente"/>-<bean:write name="usuario" property="dv_cliente"/></logic:present>');"><img
							src="/vseg-paris/images/banner/banner-que-seguro-m-c.jpg" alt="" width="172"
							onmouseover="this.src = '/vseg-paris/images/banner/banner-que-seguro-m-c-hover.jpg'"
							onmouseout="this.src = '/vseg-paris/images/banner/banner-que-seguro-m-c.jpg'"
							height="127" border="0" /> </a>
	</div> -->
		<input type="hidden" name="rutVsegParis" id="rutVsegParis" />
	</form>

	<!--login-->

	
</div>
<!-- FINcolumna secciones -->


<form action="obtener-poliza.do" method="post" id="poliza" name="poliza">
	<input type="hidden" name="id_poliza" value="" /> <input type="hidden" name="nro_solicitud" value="" />
</form>
