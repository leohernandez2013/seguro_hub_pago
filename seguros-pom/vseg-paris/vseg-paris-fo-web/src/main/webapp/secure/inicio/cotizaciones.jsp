<!DOCTYPE html>

<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>
<%@page import="cl.cencosud.ventaseguros.common.model.Solicitud"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<bean:define id="contextpath" value="<%=request.getContextPath()%>" />

<logic:notEmpty name="<%= SeguridadUtil.USUARIO_CONECTADO_KEY %>">
	<bean:define id="usuario"
		name="<%=SeguridadUtil.USUARIO_CONECTADO_KEY %>" scope="session"
		type="UsuarioExterno" />
</logic:notEmpty>

<html lang="en">
  <head>
	<%@ include file="../../home/includes/head.jsp"%>
	<logic:present name="PolizaError">
		<script>
			$( document ).ready(function() {
			$('#pestanaSeguros').trigger("click");
			$("#polizaErrorModal").modal({
			backdrop: 'static',
			show: true
			});
			});
			
		</script>
	</logic:present>
	<script>
		function cerrarPoliza() {
			<%request.getSession().removeAttribute("PolizaError");%>

			}
	</script>
	
	

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="#">
	

  </head>
	<!-- onload="cargarFichas();" -->
	<body>
		<%@ include file="../../google-analytics/google-analytics.jsp"%>
		<%@ include file="../../home/includes/header.jsp"%>
		<link href="../../css/estilos.css" rel="stylesheet">
		<!--<link href="../../css/base.css" rel="stylesheet">-->
		<div class="container contenedor-sitio">
			<ol class="breadcrumb hidden-xs">
				<li>
					<a href="/vseg-paris/index.jsp">Home</a>
				</li>
				<li class="active">Mi Perfil</li>
			</ol>
			<div class="row pull-left hidden-xs">
				<h4 class="titulo-cotizacion">MI PERFIL</h4>
			</div>
		</div>
		<div class="modal fade" id="polizaErrorModal" role="dialog" >
		<div class="modal-dialog modal-md" style="padding-left: 110px;">
			<div class="modal-content" style="height: 219px; width:450px" >
				<div class="modal-body">
					<div class="modal-header"> 	
						<h4>Ver p&oacute;liza</h4> 		
					</div>
					<div class="modal-body"> 			
						<p> El archivo no se encuentra disponible, intentelo m&aacute;s tarde</p>	 		
					</div>
					<div class="modal-footer"> 	   		
						<a class=" btn btn-large btn-primary center-block" data-dismiss="modal" onclick="javascript:cerrarPoliza();">
							Aceptar
						</a> 		
					</div>
				</div>
			</div>
		</div>
	</div> 
		<div class="container">
			<div class="row"> 
			<!-- INICIO  caja lateral 1 -->
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3-fluid"> 
					<%@ include file="perfil-resumen.jsp"%>
					
				  <!-- INICIO  PF -->
				  <div class="row bg-resumen hidden-xs faq">
					<h4>Preguntas Frecuentes</h4>
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					  <div class="panel panel-default padre">
						<div class="panel-heading color-acordeon" role="tab" id="headingOne"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
						  <h5>&iquest;Qu&eacute; es un seguro de veh&iacute;culo?</h5>
						  </a><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="cruz-acordion6"></a>  </div>
						<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
						  <div class="panel-body">Un seguro de veh&iacute;culo es un contrato con una compa&ntilde;&iacute;a de seguros en el que &eacute;sta se compromete a pagar un monto establecido en caso de choque o robo seg&utilde;n las coberturas incluidas. </div>
						</div>
					  </div>
					  <div class="panel panel-default padre">
						<div class="panel-heading color-acordeon" role="tab" id="headingTwo"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
						  <h5>&iquest;Cuales son los requisitos para<br>
							tomar un seguro de veh&iacute;culos?</h5>
						  </a><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" class="cruz-acordion6"></a> </div>
						<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
						  <div class="panel-body">Para contratar tu seguro de Veh&iacute;culo s&oacute;lo necesitas tener Tarjeta M&aacute;s Paris, M&aacute;s Jumbo o M&aacute;s Easy con tus cuotas al d&iacute;a o cualquier tarjeta de cr&eacute;dito bancaria. La contrataci&oacute;n la puedes realizar de forma r&aacute;pida y segura por www.seguroscencosud.cl, acerc&aacute;ndote a cualquier m&oacute;dulo de Seguros y Servicios de las Tiendas Paris, locales Jumbo y Santa Isabel habilitados o llamando al 600 500 5000. Adem&aacute;s tu veh&iacute;culo necesita cumplir con el requisito de antig&uuml;edad m&iacute;nima, la cual puede alcanzar hasta los 20 a&ntilde;os y depender&aacute; del plan y cobertura que desee contratar. </div>
						</div>
					  </div>
					  <!--div class="panel panel-default padre">
						<div class="panel-heading color-acordeon" role="tab" id="headingThree"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
						  <h5>Deseo saber m&aacute;s sobre el seguro<br>
							de veh&iacute;culos Full Servicio</h5>
						  </a><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree" class="cruz-acordion6"></a> </div>
						<div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
						  <div class="panel-body">
							<div class="row">
							  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<p>
								  <input class="form-control" id="rut" name="rut" required="" size="30" type="text" placeholder="RUT">
								  <br>
								  <input class="form-control" id="nombre" name="nombre" required="" size="30" type="text" placeholder="Nombre">
								  <br>
								  <input class="form-control" id="telefono" name="telefono" required="" size="30" type="text" placeholder="Tel&eacute;fono">
								  <br>
								</p>
								<div class="centrar-texto">
								  <button type="button" class="btn btn-primary btn-block">LL&aacute;MENME</button>
								</div>
								<p></p>
							  </div>
							</div>
						  </div>
						</div>
					  </div-->
					</div>
				  </div>
				  <!-- FIN  PF --> 
				</div>
				<!-- FIN  caja lateral 1 --> 
		
		
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9-fluid"> 
				  
				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">COTIZACIONES</a></li>
					<li role="presentation"><a href="#profile" aria-controls="profile" id="pestanaSeguros" role="tab" data-toggle="tab" >MIS SEGUROS</a></li>
				  </ul>
				  
				  <!-- Tab panes -->
				  <div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="home">
					  <div class="col-md-12">
						<p>En este espacio podr&aacute;s recuperar las cotizaciones realizadas en el &uacute;ltimo mes. Ten en cuenta que el valor de la UF se va ajustando diariamente, lo que podr&iacute;a modificar el valor de tu cotizaci&oacute;n.</p>
					  </div>
					  <!-- INICIO MIS COTIZACION -->
					  <%@ include file="includes/mis-cotizaciones.jsp"%>
					  <!-- FIN MIS COTIZACION -->
					</div>
					<div role="tabpanel" class="tab-pane" id="profile">
					  <div class="col-md-12">
						<p>A continuaci&oacute;n podr&aacute;s revisar las p&oacute;lizas de tus seguros contratados</p>
					  </div>
					  <!-- INICIO MIS SEGUROS -->
						<div id="misseguros">		
						</div>
					<script>
						 $.ajax({
						type : "POST",
						url : "/vseg-paris/secure/inicio/inicio.do",
						async: false,
						success : function(data) {
							$("#misseguros").html(data);
						},
						error : function(e) {
							
						}
						});
					</script>
					  <!-- FIN MIS SEGUROS -->
					  <div class="col-md-12 top10">
						<p><strong>&iquest;Tienes preguntas?</strong> ll&aacute;manos <span class="texto-destacado">600 500 5000</span></p>
					  </div>
					</div>
				  </div>
				</div>
			</div>
		</div>
		
		<%@ include file="../../home/includes/footer.jsp"%> 
	</body>
</html>
