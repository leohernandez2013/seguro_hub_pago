<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>
<%@page import="cl.cencosud.ventaseguros.common.model.Solicitud"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<bean:define id="contextpath" value="<%=request.getContextPath()%>" />
<logic:notEmpty name="<%= SeguridadUtil.USUARIO_CONECTADO_KEY %>">
	<bean:define id="usuario"
		name="<%=SeguridadUtil.USUARIO_CONECTADO_KEY %>" scope="session"
		type="UsuarioExterno" />
</logic:notEmpty>

<script type="text/javascript">
	function retomarCotizacion(id) {
				var id = id.replace("retomar_", "");
				$('#cotizador input[name="id_solicitud"]').attr("value", id);
				$('#cotizador').submit();
				return false;
			}
</script>
<logic:iterate id="solicitud" name="solicitudes" type="Solicitud"
	scope="request">
	  <div class="row center-block top15 plan">
		<div class="col-md-3 col-xs-12 caja-mi-perfil-2">
		  <h3><bean:write name="solicitud" property="descripcion_rama" /></h3>
		</div>
		<div class="col-md-3 col-xs-6 caja-tit-cot text-center p0">
		  <h5 class="btn-block hidden-xs">PLAN</h5>
		  <p><bean:write name="solicitud" property="nombre_plan" /></p>
		</div>
		<div class="col-md-4 col-xs-6 caja-tit-cot text-center p0">
		  <h5 class="btn-block hidden-xs">FECHA COTIZACI&Oacute;N</h5>
		  <p>DEL <bean:write name="solicitud" property="fecha_creacion" format="dd/MM/yyyy" /></p>
		</div>
		<div class="col-md-2 col-xs-12 p0">
			<button type="button" class="btn btn-primary caja-mi-perfil-4b btn-block"
				id="retomar_<bean:write name="solicitud" property="id_solicitud" />">RETOMAR COTIZACI&Oacute;N</button>
			<script>
				$("#retomar_" + <bean:write name="solicitud" property="id_solicitud" />).click(function() {
					//pageTracker._trackEvent('Boton', 'Mis Seguros-Ver Cotizaciones');
					retomarCotizacion(this.id);
				});
			</script>
		</div>
	  </div>
</logic:iterate>
<div class="col-md-12 top15">
	<p><strong>&iquest;Tienes preguntas?</strong> ll&aacute;manos <span class="texto-destacado">600 500 5000</span></p>
</div>

<form action="/cotizador/desplegar-cotizacion.do" method="post"
	id="cotizador" name="cotizador">
	<input type="hidden" name="id_solicitud" value="" />
</form>