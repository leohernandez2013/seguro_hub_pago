<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<bean:define id="contextpath" value="<%=request.getContextPath()%>" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Actualizacion Datos</title>
	<script src="/vseg-paris/js/jquery-1.11.3.min.js"></script>
	<%@ include file="../../home/includes/head.jsp"%>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="#">
  </head>  

<body>
<%@ include file="../../home/includes/header.jsp"%>
<div class="container contenedor-sitio">
    <ol class="breadcrumb hidden-xs">
      <li><a href="/vseg-paris/index.html">Home</a></li>
      <li><a href="/vseg-paris/secure/inicio/cotizaciones.do">Mi perfil</a></li>
      <li class="active">Actualiza tus datos</li>
    </ol>
    <div class="row hidden-xs">
      <h1 class="titulo-cotizacion pull-left">ACTUALIZA TUS DATOS</h1>
    </div>
</div>
 <div class="container caja-actualiza-exito">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h4 class="text-center center-block">Sus datos han sido actualizados correctamente</h4>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 top30">
			<button type="button" class="btn btn-primary center-block top30" onclick="javascript:window.parent.location.href='/vseg-paris/secure/inicio/cotizaciones.do';">Volver</button>
			</div>
		</div>
	</div>
 </div>
 
 <%@ include file="../../home/includes/footer.jsp"%>
</body>
</html>