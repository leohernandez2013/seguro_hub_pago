
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>

<%@page import="cl.tinet.common.seguridad.servlet.AutenticacionServlet"%>


<logic:present name="cl.tinet.common.seguridad.USUARIO_CONECTADO">
<script type="text/javascript">
<bean:define id="usuario"
		name="<%=SeguridadUtil.USUARIO_CONECTADO_KEY %>" scope="session"
		type="UsuarioExterno" />
</script>
</logic:present>



<script type="text/javascript">
	
	function SubmitFormulario() {
      userName = document.getElementById("rut_login").value + "-" +  document.getElementById("dv_login").value;
      clave =    document.getElementById("clave").value;
      document.formularioLogin.username.value = userName;
      document.formularioLogin.password.value = clave;
      document.forms.formularioLogin.submit();  
      
   }
   
   $(document).ready(function (){
	   			$("#registroUsuario").fancybox ({
				  'onStart'   : function() {
			    var rut_cliente = "";
				var dv_cliente = "";
				  
				var url = '<html:rewrite action="/registroCliente" module="/registrocliente"/>';
				var params = "?rut_cliente=" + rut_cliente;
				params += "&dv_cliente=" + dv_cliente;
				
			   $("#registroUsuario").attr("href",url+params);
				   
			  },

			    'width'    : 630,

			    'height'   : 850,

			    'autoScale'   : false,
			    'transitionIn'  : 'none',
			    'transitionOut'  : 'none',
			    'type'    : 'iframe',
		    	'scrolling'  : 'no'
			   });
			    
			//Se envia formulario al presionar enter.
			$('#clave').keypress(function(e){
				if(e.which == 13){
					e.preventDefault();
				    SubmitFormulario();
					return false;
				}
			});
   });
   
   
   function obtenerclave() {
			    document.formularioRecuperarClave.rut.value = document.formularioLogin.rut.value;
			    document.formularioRecuperarClave.dv.value =  document.formularioLogin.dv.value; 
			    document.formularioRecuperarClave.submit();
			}
   
</script>

<!-- Formularios Ocultos -->
	<form name="formularioRecuperarClave" method="post" action="cambio-clave/recuperar-clave.do">
		<input type="hidden" name="rut"  />
		<input type="hidden" name="dv" />
	</form>
	
<logic:notPresent name="usuario">			


<form action="/vseg-paris/Login" method="post" name="formularioLogin">
			<input type="hidden" name="username" id="username" value="">
			<input type="hidden" name="password" id="password" value="">
			
			
		<input type="hidden" name="cl.tinet.common.seguridad.servlet.URI_EXITO" id="cl.tinet.common.seguridad.servlet.URI_EXITO"/>
		<input type="hidden" name="cl.tinet.common.seguridad.servlet.URI_FRACASO" id="cl.tinet.common.seguridad.servlet.URI_FRACASO" />
		
		
		<script type="text/javascript">
			$(document).ready(function(){
				var id_rama = "<%= request.getParameter("idRama") %>";
				var url_pagina = "/desplegar-pagina-intermedia-vehiculos.do?idRama=";
				
				if(id_rama == 1){
					url_pagina = "/desplegar-pagina-intermedia-vehiculos.do?idRama=";
				}
				 else if(id_rama == 2){
				 	url_pagina = "/desplegar-pagina-intermedia-hogar.do?idRama=";
				}
				else if(id_rama == 3) {
					url_pagina = "/desplegar-pagina-intermedia-vida.do?idRama=";
				}
				else if(id_rama == 4){
					url_pagina = "/desplegar-pagina-intermedia-salud.do?idRama=";
				}
				else if(id_rama == 5) {
					url_pagina = "/desplegar-pagina-intermedia-cesantia.do?idRama=";
				}
				else if(id_rama == 6) {
					url_pagina = "/desplegar-pagina-intermedia-fraude.do?idRama=";
				}
				
				document.getElementById("cl.tinet.common.seguridad.servlet.URI_EXITO").value = url_pagina + id_rama;
				document.getElementById("cl.tinet.common.seguridad.servlet.URI_FRACASO").value = url_pagina + id_rama;
				
			});
		</script>
<!-- 
	<div id="login_in_ti"><img src="images/imagenes/titulo-mis-seguro-inte.png" alt="" width="172" height="25" border="0" /></div>
	
	<div id="login_dat_in">
	<div id="login_dat_in_per"><p><bean:message bundle="labels-seguridad" key="labels.usuario.rut"/></p></div>
	<div id="login_dat_in_tex"><input type="text" class="textbox" name="rut_login"  id="rut_login" size="8"  maxlength="8"/>
	- </div>
	<div class="personal_login_rut_b">
	  <input name="dv_login" id="dv_login" type="text" class="box-rut-verificador" value="" size="1" maxlength="1" />
	  </div>
    </div>
	
	<div id="login_dat_in">
	<div id="login_dat_in_per"><p><bean:message bundle="labels-seguridad" key="labels.usuario.clave"/></p></div>
	<div id="login_dat_in_tex"><input type="password" class="textbox" name="clave"  id="clave" size="8" maxlength="6"/></div>
	<div id="login_dat_in_ir"><a onfocus="blur();" href="javascript:SubmitFormulario();"><img src="images/btn-img/ir-in-btn-log.gif"onmouseover="this.src = 'images/btn-img/ir-in-btn-log-hover.gif'"  onmouseout="this.src = 'images/btn-img/ir-in-btn-log.gif'"  alt="" width="28" height="19" border="0" /></a></div>
	</div>
	
	<div id="login_dat_in_obten"><p><img src="images/btn-img/flecha-login-home.gif" alt="" width="7" height="9" border="0" /><a href="javascript:void(0);" style="cursor: pointer;"><span id="registroUsuario">Obt�n tu Clave</span></a></p> <p><img src="images/btn-img/flecha-login-home.gif" alt="" width="7" height="9" border="0" /><a  id="olvidasteClave" style="cursor: pointer;" href="javascript:void(0);">�Olvidaste tu Clave?</a></p></div>
	 -->	
	
	<!-- Caja de logueo -->
	<div id="login_dat_in">
	<div id="login_dat_in_per"></div>
	<div id="login_dat_in_tex"></div>
	</div>					
	<div id="p7PMM03contenedorMenu">
		<div id="p7PMM_1" class="p7PMMh03 p7PMMnoscript">
	       <ul class="p7PMM">					      	
	         <li><a href="#"> <span style="font-family: Arial, verdana; text-indent: 60px; *text-indent: 0px; padding-top: 12px; font-size: 15px; color: #56a9d5; font-weight: bold; padding-top: 4px;"> Mis seguros </span> <b style="color: #666;">Online</b> </a>							
	          <div>
	           <ul>						    
	            <li><a href="#"  >Rut: <input name="rut_login" id="rut_login" type="text" size="8" maxlength="8"/> 
	             - <input name="dv_login" id="dv_login" type="text" style="width:10px" size="1" maxlength="1"/> 					              
		         &nbsp clave: <input name="clave" id="clave" type="password" size="6" /> 
			    <input id="ir03" value="ir" alt="" style="border:0; background-repeat: no-repeat; text-align:center;" /><a onfocus="blur();" href="#" onclick="javascrtip:SubmitFormulario();"/></a>
				 </li>					              
				<li> <span class="fancy_iframe" 
					style="cursor: pointer; float:left; text-indent: 23px; *margin-top:0px; -margin-top:-10px;"
					 id="registroUsuario"><img width="9" height="10" src="css/p7pmm/img/linkk.png" alt="" />Obt&eacute;n tu Clave</span>						        
					<span class="fancy_iframe"
					style="cursor: pointer; float:left; text-indent: 23px; *margin-top:0px"
					href="javascript:void(0);" id="olvidasteClave"><img width="9" height="10" src="css/p7pmm/img/linkk.png" alt="" />�olvidaste tu clave?</span>
				
				</li>					
				</ul>
				
				</div>
				</li>			        
				</ul>
				<div class="p7pmmclearfloat">&nbsp;</div>
			</div>
		</div>						
						
		<!-- <div id="fondo"> </div> -->
						
<!-- Fin caja de logueo -->	
	</form>
	
	
	<script type="text/javascript">

			$("#olvidasteClave").fancybox({ 
		    'onStart'   : function() {
					var url = "/vseg-paris/recuperar-clave.do";
					var params = "?rut=";
					params += "&dv=";
					//var params = "?rut=" + $("#rutlogin").val();
					//	params += "&dv=" + $("#dvlogin").val();
		     $("#olvidasteClave").attr("href",url+params);
		     
		    },
		    'width'    : 430,
		    'height'   : 130,
		    'autoScale'   : false,
		    'transitionIn'  : 'none',
		    'transitionOut'  : 'none',
		    'type'    : 'iframe',
		    	'scrolling'  : 'no'
		  });

	</script>
		
	<!--login-->
</logic:notPresent>



<logic:present name="usuario">



<!--	-->

<!--	<div id="login">-->

<!--	<div id="login_in_ti"><img src="images/imagenes/titulo-mis-seguro-inte.png" alt="" width="172" height="25" border="0" /></div>-->

<!--	-->

<!--	<div id="login_dat_in">-->

<!--	  <div class="saludo-login2">Bienvenido</div>-->

<!--	  <div class="nombre-login"><a onfocus="blur();" href="/vseg-paris/secure/inicio/inicio.do"><img src="images/btn-img/btn_ingresar_home.gif" onmouseover="this.src = 'images/btn-img/btn_ingresar_home_hover.gif'" border="0" onmouseout="this.src = 'images/btn-img/btn_ingresar_home.gif'" alt="" width="70" height="22" /></a></div>-->

<!--    </div>-->

<!--	-->

<!--	<div class="datos-login2">-->

<!--	  <div class="textos2-login"><a href="/vseg-paris/secure/inicio/inicio.do">&gt; Mis Seguros</a></div>-->

<!--	  </div>-->

<!--	<div class="datos-login2">-->

<!--	  <div class="textos2-login"><a href="/vseg-paris/secure/inicio/cotizaciones.do">&gt; Mis Cotizaciones</a></div>-->

<!--	  </div>-->

<!---->

<!--</div>-->

<div id="p7PMM03contenedorMenu">
	    <div id="p7PMM_1" class="p7PMMh03 p7PMMnoscript">
	      <ul class="p7PMM">
	      	<!-- Vehiculo -->
	         <li><a> <span style="font-family: Arial, verdana; text-indent: 60px; *text-indent: 0px; padding-top: 12px; font-size: 15px; color: #56a9d5; font-weight: bold; padding-top: 4px;"> Mis seguros </span> <b style="color: #666;">Online</b> </a>
			
	          <div>
					<ul>

						<li style="*margin-top: 0px;height:50px;"> 

							<a href="/vseg-paris/secure/inicio/inicio.do" style="padding: 2px 2px 2px 57px;">
							<img width="9" height="10" border="0" onmouseout="this.src = 'css/p7pmm/img/linkk.png'" onmouseover="this.src = 'css/p7pmm/img/linkk.png'" src="css/p7pmm/img/linkk.png"> Mis Seguros
							</a>
							<a  href="/vseg-paris/secure/inicio/cotizaciones.do" style="padding: 2px 2px 2px 57px;">
							<img width="9" height="10" border="0" onmouseout="this.src = 'css/p7pmm/img/linkk.png'" onmouseover="this.src = 'css/p7pmm/img/linkk.png'" src="css/p7pmm/img/linkk.png"> Mis Cotizaciones
							</a>
							<a href="/vseg-paris/secure/inicio/inicio.do"  style="margin-top: -37px; margin-left: 143px;" >
							
								<img width="62" height="18" border="0" onmouseout="this.src = 'images/ingresar.png'" onmouseover="this.src = 'images/ingresarOver.png'" src="images/ingresar.png">
							</a>
						</li>
					</ul>
	          </div>
	        </li>
		</ul>
	      <div class="p7pmmclearfloat">&nbsp;</div>
	    </div>
	</div>
</logic:present>

<logic:present name="<%=AutenticacionServlet.ERROR_AUTENTICACION_KEY%>">
<!--BOX PROCESO -->

<div id="errorlogin">

<div align="center" style="padding-top:25px;height:168px;" >

	<div id="estamos_procesando">
		
			<a onclick="javascript:parent.$.fn.colorbox.close();$('#errorlogin').hide();"></a>
		
		<div class="pantalla-error_cuerpo">
			<div id="conte_estamos_procesando">
				<div class="icono_geeral">
					<img src="/vseg-paris/images/img/icono_error.png" alt="" width="46" height="46" border="0" />
				</div>
				<div id="texto_procesando">
					<h3>
						<bean:write name="<%=AutenticacionServlet.ERROR_AUTENTICACION_KEY%>" ignore="true" />
						<br />
					</h3>
				</div>
			</div>
		</div>
	</div>
</div>
	<!--BOX PROCESO FIN-->
</div>

<script type="text/javascript">
		//$(document).ready(function(){
		//	$("#errorlogin").show();
		//	$.fn.colorbox({
		//			 inline: true, 
		//		     width:"750", 
		//		     height:"550", 
		//		     href: "#errorlogin"
		//	});
		
		
		$(document).ready(function(){
			$('<a href="#errorlogin" class="login_error"></a>').appendTo('body');
			
			$("a.login_error").fancybox({ 
			    'hideOnContentClick': false,
				'frameWidth' : 750,
				'frameHeight' : 550,
		    	'type'  : 'inline'
			  });
			$("#fancybox-close").click(function(){
				$("#errorlogin").css("display","none");
			});
			$("a.login_error").click();
			
		});

</script>
</logic:present>

<script type="text/javascript">
			function asesorVirtualBanner(rut){
				$("#rutVsegParis").val(rut);
				$("#tipoSeguroAsesor").val("<%= request.getAttribute("tipoSeguro") %>");
				$("#asesorForm").submit();
			}
</script>
<form action="/vseg-paris/asesor-virtual/asesor-virtual.jsp" method="post" name="asesorForm" id="asesorForm" target="_blank">
	<input type="hidden" name="rutVsegParis" id="rutVsegParis"/>
	<input type="hidden" name="tipoSeguroAsesor" id="tipoSeguroAsesor"/>
</form>