<!DOCTYPE html>
<html lang="en">
<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
  <head>
  	<%@ include file="includes/paginaintermedia-head.jsp"%>
	<%@ include file="/home/includes/head.jsp"%>
  </head>

<body>
<%@ include file="/google-analytics/google-analytics.jsp"%>
<%@ include file="includes/paginaintermedia-header.jsp"%>
<%@ include file="/home/includes/header.jsp"%>
<!-- INICIO  BREADCRUMB -->

<div class="container contenedor-sitio">
  <ol class="breadcrumb hidden-xs">
    <li><a href="/vseg-paris/index.jsp">Home</a></li>
    <li class="active">Salud</li>
  </ol>
  <div class="row">
    <h4 class="titulo-cotizacion"><strong>Salud</strong></h4>
  </div>
  
  <!-- FIN  BREADCRUMB --> 
  <!-- INICIO  CONTENIDOS -->
  <div class="container">
    <div class="row">
      <div><img src="img/img-contacto.jpg" alt="contacto" class="img-responsive"></div>
    </div>
    <div class="row">
        <!-- INICIO COLUMNA 1 -->
        
        <div class="col-md-9"> 
          <%@ include file="includes/listadopaginaintermedia.jsp"%>
        </div>
        
        <!-- FIN COLUMNA 1 --> 
        
        <!-- INICIO COLUMNA BANNERS -->
        <div class="col-md-3 top15">
          <div class="col-md-12"> <a onfocus="blur();" href="/vseg-paris/desplegar-ficha.do?idRama=4&idSubcategoria=30&idPlanPromocion=249">
          <img class="img-responsive" src="images/promociones/nav50.jpg"/> </a>	 </div>
        </div>
        <!-- FIN COLUMNA BANNERS --> 
      </div>
    </div>
  </div>
  <!-- FIN CONTENIDOS -->
<%@ include file="/home/includes/footer.jsp"%> 
</body>
</html>
