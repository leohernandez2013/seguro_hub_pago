<!DOCTYPE html>
<html lang="en">
	<%@ page language="java" pageEncoding="ISO-8859-1"%>

	<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
	<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
	<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
	<head>
		<%@ include file="includes/paginaintermedia-head.jsp"%>
		<%@ include file="/home/includes/head.jsp"%>
	</head>
	<body>
		<%@ include file="/google-analytics/google-analytics.jsp"%>
		<%@ include file="includes/paginaintermedia-header.jsp"%>
		<%@ include file="/home/includes/header.jsp"%>
		<!-- INICIO  BREADCRUMB -->
		<!-- FIN  BREADCRUMB --> 
		<!-- INICIO  CONTENIDOS -->
			<main role="main">
		      <div class="remodal-bg">
		        <div class="c-herosimple" style="background-image: url(img/bn_intermedia/vidasalud_Intermedia.jpg);">
		          <div class="o-advertising">
		            <div class="u-center">
		              <div class="container">
		                <h1 class="o-advertising__title">Seguros de Vida</h1>
		                <p class="o-advertising__text--big">Asegura la tranquilidad de tu familia, prot�gelos si t� no est�s.</p>
		              </div>
		            </div>
		          </div>
		        </div>
		        <div class="c-articles">
		          <div class="container">
		            <div class="row">
		            	<div class="col-sm-12"> 
						<%@ include file="includes/listadopaginaintermedia.jsp"%>
					   	</div>
		            </div>
		          </div>
		        </div>
		        <div class="container">
			          <div class="row">
			            <div class="col-sm-12">
			              <h2 class="o-title o-title--secundary u-text-center">Aprovecha nuestras ofertas</h2>
			            </div>
			            <div class="col-sm-12">
			              <section class="u-mb20">
			              <div id="offer-slick">
					  <div class="slick_slide">
						<div class="o-offer js-getImage">
						  <div class="o-offer__tag">
							<!-- Auto -->
							<svg data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 350.59 268.71" style="padding-top: 3px;">
							  <defs><style>.cls-2 {fill:#ffffff; fill-rule:evenodd;}</style></defs><title>auto</title><path class="cls-2" d="M313.77,156.81a20.46,20.46,0,1,1-20.43-20.43A20.45,20.45,0,0,1,313.77,156.81ZM71.26,136.38A20.46,20.46,0,1,0,91.7,156.81,20.47,20.47,0,0,0,71.26,136.38Zm76,46.7a14.64,14.64,0,0,0,0,29.28h70.12a14.64,14.64,0,0,0,0-29.28ZM264.1,241.57H100.48v11.68a29.21,29.21,0,0,1-58.42,0V241.57A20.5,20.5,0,0,1,21.59,221.1V156.81a49.7,49.7,0,0,1,30-45.56l-2.37-2.35a5.59,5.59,0,0,0-4.1-1.72H21.59a14.62,14.62,0,0,1,0-29.23H36.78a28.25,28.25,0,0,1,20.65,8.58l3,3,8.89-32.69a58.05,58.05,0,0,1,56.37-43.07H238.84a58.1,58.1,0,0,1,56.42,43.07l8.9,32.69,3-2.93A28.22,28.22,0,0,1,327.79,78H343a14.62,14.62,0,1,1,0,29.23H319.49a5.55,5.55,0,0,0-4.09,1.72L313,111.25a49.63,49.63,0,0,1,29.9,45.56V221.1a20.45,20.45,0,0,1-20.4,20.47v11.68a29.22,29.22,0,0,1-58.44,0ZM125.71,42.88A28.89,28.89,0,0,0,97.54,64.44L85.85,107.18H278.67L267,64.44a28.93,28.93,0,0,0-28.2-21.56Z" transform="translate(-7.01 -13.72)"></path></svg>
						  </div>
						  <div class="o-offer__content">
							<h2 class="o-offer__title">Contrata tu seguro Auto Full Cobertura</h2>
							<p class="o-offer__text">Nos encargamos de proteger lo que tanto te ha costado.</p>
							<a class="o-offer__link" href="https://www.seguroscencosud.cl/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=22&idTipo=1">Contr�talo aqu�</a> </div>
						  <div class="js-image"><img src="img/offer-01.jpg" alt=""></div>
						  <div class="cama-calugas-home"></div>
						</div>
					  </div>
					  <div class="slick_slide">
						<div class="o-offer js-getImage">
						  <div class="o-offer__tag">
							<!-- Mascota -->
							<svg  data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 165.29 151.95" style="padding-top: 2px;">
							  <defs><style>.cls-2{fill:#ffffff;}</style></defs><title>Sin t�tulo-1</title><path class="cls-2" d="M331.6,344.26a32,32,0,0,1,30.87,24.93c.55,2.55.76,5.18,1.1,7.78a48.18,48.18,0,0,1-4.09,17.67c-3.76,8.78-9.32,16.32-15.83,23.22-8.69,9.26-18.63,17-29.15,24-2.81,1.88-5.71,3.62-8.56,5.43a1.44,1.44,0,0,1-.74.26,1.63,1.63,0,0,1-.79-.26,193,193,0,0,1-31.67-23.44c-7.47-6.94-14.12-14.59-19.1-23.54a49.46,49.46,0,0,1-6.54-20.66c-.66-10.44,2.45-19.57,10.21-26.82a30.86,30.86,0,0,1,18.22-8.31c1.25-.12,2.49-.16,3.74-.16A30.41,30.41,0,0,1,295.7,349a23.57,23.57,0,0,1,8.18,8.9c.26.45.48.92.76,1.37s.36.49.55.49.37-.16.56-.51c.47-.81.9-1.68,1.4-2.48,3.86-6.29,9.59-9.92,16.67-11.55a34.36,34.36,0,0,1,7.78-.93m0-24.33h0a59.74,59.74,0,0,0-13.25,1.52,54.5,54.5,0,0,0-13.2,4.87A54.64,54.64,0,0,0,279.27,320c-2,0-4.14.1-6.19.3a56.57,56.57,0,0,0-46.85,36.28,59.38,59.38,0,0,0-3.43,24.65c.67,10.39,3.79,20.51,9.55,30.88s13.47,20,23.78,29.57c9.87,9.22,21.19,17.61,35.59,26.38h0a25.51,25.51,0,0,0,27.39-.26c1-.64,2-1.27,3.11-2,1.83-1.17,3.76-2.39,5.75-3.69,13.45-8.95,24.38-17.95,33.42-27.56,9.26-9.85,16-19.78,20.48-30.33a73.29,73.29,0,0,0,6-26.51l0-2-.28-2c-.06-.54-.15-1.21-.22-1.91a66.65,66.65,0,0,0-1.25-7.91,56.32,56.32,0,0,0-54.6-43.9Z" transform="translate(-222.67 -319.93)"/></svg>
						  </div>
						  <div class="o-offer__content">
							<h2 class="o-offer__title">Seguro de Mascota</h2>
							<p class="o-offer__text">Por que tu mascota es importante para tu familia.</p>
							<a class="o-offer__link" href="https://www.seguroscencosud.cl/vseg-paris/desplegar-ficha.do?idRama=6&idSubcategoria=140">Contr�talo aqu�</a> </div>
						  <div class="js-image"><img src="img/offer-02.jpg" alt=""></div>
						  <div class="cama-calugas-home"></div>
						</div>
					  </div>
					  <div class="slick_slide">
						<div class="o-offer js-getImage">
						  <div class="o-offer__tag">
							<!-- Viajes -->
							<svg data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 350.78 193.08" style="padding-top: 3px; margin-left: -3px; margin-right: -3px;">
							  <defs><style>.cls-2{fill:#ffffff;}</style></defs><title>Sin t�tulo-1</title><path class="cls-2" d="M356.75,196.87l-.36-3.33a7.74,7.74,0,0,0-7.25-6.89c-.12,0-13.79-.8-38.08-5.1a329.62,329.62,0,0,1-35-8.22,20.93,20.93,0,0,0-2.16-10.73,14.77,14.77,0,0,0-9-7.18,20.05,20.05,0,0,0-5.39-.77c-7.77,0-13.07,4.93-16.23,10-4.31-1-10.77-2.59-18.51-4.91-7.44-2.23-13.1-4.18-16.81-5.56,1.49-15.32,4.77-56.1.22-75.28C201.59,51.15,187.41,47,179.56,47c-15.12,0-25.3,10.37-31.14,31.67-5.46,20-2.19,61.13-.71,76.55-3.53,1.29-8.8,3.15-15.7,5.33-7.92,2.48-15.28,4.73-20.22,6.23-3.55-5.84-10.32-13.16-20.8-11.74a14.93,14.93,0,0,0-10.62,6.54c-2.73,4.11-3.3,9.2-3.14,13.41-4.92,1.12-13,2.91-25.39,5.47-24.29,5-37.57,6.19-37.71,6.21a7.7,7.7,0,0,0-6.66,5.18l-1,3.06a7.74,7.74,0,0,0,.91,6.82,7.64,7.64,0,0,0,5.95,3.4s2.65.18,8.68.18,16.19-.18,31.43-.9L60.5,204c12.46-.56,15.38-.72,29.21-2.06,2.78,2,7.1,4.34,11.88,4.34A14.06,14.06,0,0,0,111.3,202l41.17,1.64c.47,1.87,1.1,4,1.85,6.22.13.39.28.79.42,1.16l-35.8,6a7.83,7.83,0,0,0-4.43,2.38l-2.23,2.44a7.76,7.76,0,0,0-1.35,8.39,8,8,0,0,0,7.21,4.54l47.52-.93c2.68,2.87,7,6.26,12.43,6.26,5.94,0,10.38-3.82,13.06-7l52.46.69a7.43,7.43,0,0,0,7.19-4.66,7.69,7.69,0,0,0-1.55-8.41l-1.82-1.89a7.82,7.82,0,0,0-4.38-2.27L200.13,210l.31-1.14c.53-2,.95-3.91,1.33-5.62l40.65-1.37a13.81,13.81,0,0,0,10.06,4.33,15.94,15.94,0,0,0,9.92-3.55c7.49.4,26.36,1.36,47.77,2,22.51.73,38.76.75,38.91.75a7.69,7.69,0,0,0,5.75-2.58,7.77,7.77,0,0,0,1.92-6" transform="translate(-6.01 -47)"></path></svg>
						  </div>
						  <div class="o-offer__content">
							<h2 class="o-offer__title">Contrata tu Seguro De viaje</h2>
							<p class="o-offer__text">Elige tu plan de acuerdo al destino, coberturas y cantidad de pasajeros.</p>
							<a class="o-offer__link" href="https://www.seguroscencosud.cl/vseg-paris/desplegar-pagina-intermedia-fraude.do?idRama=6">Contr�talo aqu�</a> </div>
						  <div class="js-image"><img src="img/offer-03.jpg" alt=""></div>
						  <div class="cama-calugas-home"></div>
						</div>
					  </div>
					</div>
			              </section>
			            </div>
			          </div>
			        </div>
		      </div>
		    </main>   
		<!-- FIN CONTENIDOS -->
		<%@ include file="/home/includes/footer.jsp"%> 
	</body>
</html>
