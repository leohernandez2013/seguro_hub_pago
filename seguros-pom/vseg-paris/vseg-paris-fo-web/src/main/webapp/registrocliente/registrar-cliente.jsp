<!DOCTYPE html>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ page import="cl.tinet.common.struts.form.ValidableActionForm"%>
<%@ page import="org.apache.struts.Globals"%> 


<html lang="en">
	
	<head>
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />	
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="#">
		
		<script src="/vseg-paris/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
		<script src="/vseg-paris/js/ie-emulation-modes-warning.js"></script>
		<script src="/vseg-paris/js/jquery-1.11.3.min.js"></script>
		<script src="/vseg-paris/js/vendor/bootstrap.min.js"></script>
		<script src="/vseg-paris/js/jPushMenu.js"></script>
		<script src="/vseg-paris/js/v2p.js"></script>
		<script src="/vseg-paris/js/owl.carousel.js"></script>
		<script src="/cotizador/js/jquery-ui.js"></script>
		
		<link href="/vseg-paris/css/bootstrap.min.css" rel="stylesheet">
		<link href="/vseg-paris/css/bootstrap-theme.min.css" rel="stylesheet">
		<link href="/vseg-paris/css/estilos.css" rel="stylesheet">
		<link href="/vseg-paris/css/carousel.css" rel="stylesheet">
		<link href="/vseg-paris/css/base.css" rel="stylesheet">
		<link rel="stylesheet" href="/vseg-paris/css/owl.carousel.css">
		<link rel="stylesheet" href="/vseg-paris/css/owl.theme.css">
		<link href="/cotizador/css/jquery-ui.css" rel="stylesheet">
		
		<title>Formulario de registro</title>
<script type="text/javascript">
	$(document).ready(function() {

		var owl = $("#owl-demo");

		owl.owlCarousel({

			items : 6, //10 items above 1000px browser width
			itemsDesktop : [ 1200, 8 ], //5 items between 1000px and 901px
			itemsDesktopSmall : [ 992, 3 ], // 3 items betweem 900px and 601px
			itemsTablet : [ 768, 2 ], //2 items between 600 and 0;
			itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option

		});
	});
</script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				$('.datos-rut-input').attr('placeholder','12345678-9');
				$('.datos-serie-input').attr('placeholder','N�mero De Serie');
				$('.datos-nombre-input').attr('placeholder','Nombre');
				$('.datos-paterno-input').attr('placeholder','Apellido Paterno');
				$('.datos-materno-input').attr('placeholder','Apellido Materno');
				$('.datos-email-input').attr('placeholder','ej: email@email.com');
				$('.datos-fecha-input').attr('placeholder','01-01-1980');
				$('.datos-telefono-input').attr('placeholder','Tel�fono');
				$('.datos-calle-input').attr('placeholder','Calle');
				$('.datos-direccion-input').attr('placeholder','N�');
				$('.datos-depto-input').attr('placeholder','N� Depto/Casa');
				$('.datos-pass1-input').attr('placeholder','Clave');
				$('.datos-pass2-input').attr('placeholder','Repita Su Clave');
			});
			function validar(e) {
				tecla = (document.all) ? e.keyCode : e.which;
				if (tecla==8) return true;
				patron =/[A-Za-z\s]/;
				te = String.fromCharCode(tecla);
				return patron.test(te);
			} 
			function isNumberKey(evt){
				var charCode = (evt.which) ? evt.which : event.keyCode
				if (charCode > 31 && (charCode < 48 || charCode > 57))
				return false;
				 
				return true;
			}
			function isCaracterEmail(evt) {
                var tecla = (evt.which) ? evt.which : evt.keyCode;
                var isCtrl = false;
                var isShift = false;
                var teclasPermitidas = new Array("a","n","c","x","v","j", "~", "�");
                var codigosNoPermitidos = new Array('!', '"', '#', '$', '%', '&', '/', '(', ')', '=', '\'', '�', '�', '?', ':', ';', ',', '�', '|', '�', '�', '+', '{', '}', '[', ']', '<', '>', '/', '*', '�', '�', '~');
                var noPermitidosSinShift = new Array("|", "/", "*",  "+", "<", "�", "{", "}", ",", "'", "�");
                if (window.event) {
				   key = window.event.keyCode;     //IE      
				   isCtrl = false;
				   if (window.event.ctrlKey) { isCtrl = true; }
				   if (window.event.shiftKey) { isShift = true; }
                } else {                                 
				   key = evt.which;     //firefox
				   isCtrl = false;
				   if (evt.ctrlKey) { isCtrl = true; }
				   if (evt.shiftKey) { isShift = true; }
                }
                if (isCtrl) {
				   for (i = 0; i < teclasPermitidas.length; i++) {
					   if (teclasPermitidas[i].toLowerCase() == String.fromCharCode(key).toLowerCase()) {
						   return false;
						   break;
					   }
				   }
                }
                if (isShift) {
				   for (j = 0; j < codigosNoPermitidos.length; j++) {
					   if (codigosNoPermitidos[j] == String.fromCharCode(key).toLowerCase()) {
						   return false;
						   break;
					   }
				   }                              
                } else {
				   for (k = 0; k < noPermitidosSinShift.length; k++) {
					   if (noPermitidosSinShift[k] == String.fromCharCode(key).toLowerCase()) {
						   return false;
						   break;
					   }
				   } 
                }                              
}
			function addDias(dias, select) {
				for(var i = 1; i <= dias ; i++){
					var optn = document.createElement("OPTION");
					optn.text = i;
					optn.value = i;
					document.getElementById(select).options.add(optn);
				}
			}
			
			function registarCliente() {
				//window.parent.$("html, body").animate({scrollTop:0}, 'fast');
				personalInfoRegistro();
				
				if (!validateInputRegistro("formularioRegistro")) {
					return false;
				}else{
					$("#formulario").submit();
				}
			}
		
			//Generar objeto para celulares
			var codigo_celular = "<option value=''>Seleccione</option>";
				codigo_celular += "<option value=\"09\">09</option>";
				codigo_celular += "<option value=\"08\">08</option>";
				codigo_celular += "<option value=\"07\">07</option>";
				codigo_celular += "<option value=\"06\">06</option>";
				codigo_celular += "<option value=\"05\">05</option>";
			
			function recargar() {
				$("#captchaimg").attr("src", "../captcha.png"+'?'+Math.random());
 				return false;
 			}
			
			$(document).ready(function(){
				$('.toggle-menu').jPushMenu({closeOnClickLink: false});
				$('.dropdown-toggle').dropdown();
				
				var image = '<img src="/vseg-paris/images/carnet_2.gif" width="189">';
				$('#popover').popover({placement: 'bottom', content: image, html: true});
				
			if(navigator.appName != "Microsoft Internet Explorer") {
				$("#tooltip_rut").tooltip({position: "center right"}); // tooltip
			}
						
			var codigo_fijo = $("#formulario select[name='datos(codigo_area)']").html();
			
			if($("#formulario select[name='datos(tipo_telefono_1)'] :selected").text() == 'Celular') {
				$("#formulario select[name='datos(codigo_area)']").html(codigo_celular);
			}
			
			$("#formulario select[name='datos(tipo_telefono_1)']").change(function(){
				var sel = $("#formulario select[name='datos(tipo_telefono_1)'] :selected").text();

				if(sel == 'Celular'){
					$("#formulario select[name='datos(codigo_area)']").html(codigo_celular);
				} else {
					$("#formulario select[name='datos(codigo_area)']").html(codigo_fijo);
				}
			});

			var maxdias = 31;
			var options = '';
			options += '<option value="">dia</option>';
			for(var i=1; i<=maxdias; i++) {
				options += '<option value="' + i + '">' + i + '</option>';				
			}
			$('select#diasFecha').html(options);
		
			$("select#mesFecha").change(function(){
				var tdia = $('select#diasFecha').val();
				
				if(this.value == 2) {
					$('select#diasFecha option').remove();
					$('select#diasFecha').html('<option value="">dia</option>');
					addDias(29, "diasFecha");
				}
				else if(this.value == 4 || this.value == 6 || this.value == 9 || this.value == 11) {
					$('select#diasFecha option').remove();
					$('select#diasFecha').html('<option value="">dia</option>');
					addDias(30, "diasFecha");
				}
				else{
					$('select#diasFecha option').remove();
					$('select#diasFecha').html('<option value="">dia</option>');
					addDias(31, "diasFecha");
				}
				
				$('select#diasFecha').val(tdia);
			});

			$("select#id_region").change(function() {
				
				$.getJSON('/cotizador/buscar-comunas.do?idRegion=' + this.value, function(data){
					$('select#id_comuna option').remove();
					var options = '';
					options += '<option value="0">Seleccione Comuna</option>';
					
					for(var i = 0; i < data.length; i++) {
						options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
					}

					$('select#id_comuna').html(options);
					$("select#id_ciudad").html('<option value="">Seleccione Ciudad</option>');
				});
			});

			
			$("select#id_comuna").change(function() {
				
				$.getJSON('/cotizador/buscar-ciudades.do?idComuna=' + this.value, function(data){
					$('select#id_ciudad option').remove();
					var options = '';
					options += '<option value="0">Seleccione Ciudad</option>';
					
					for(var i = 0; i < data.length; i++) {
						options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
					}

					$('select#id_ciudad').html(options);
				});
			});
			
			$("#cerrar").click(function() {
			    parent.$.fn.colorbox.close();
			    return false;
			});
			
		});
		
		</script>

<script type="text/javascript">	
		$(document).ready(function(){
			$("select#regionResidencia").change(function() {
				$.getJSON('/cotizador/buscar-comunas.do?idRegion=' + this.value, function(data){
					$('select#comunaResidencia option').remove();
					var options = '';
					options += '<option value="">Seleccione Comuna</option>';				
					for(var i = 0; i < data.length; i++) {
						options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
					}
					$('select#comunaResidencia').html(options);

				});
			});
		});
		
		$(document).ready(function(){
			$("#fechaCompleta").datepicker({
				changeMonth: true,
				changeYear: true,
				yearRange: '1941:1998',
				dateFormat: 'dd-mm-yy',
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
				monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
				'Junio', 'Julio', 'Agosto', 'Septiembre',
				'Octubre', 'Noviembre', 'Diciembre'],
				monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
				'May', 'Jun', 'Jul', 'Ago',
				'Sep', 'Oct', 'Nov', 'Dic']
			}); 
		});
</script>

<script type="text/javascript">
		 
		    $(document).ready(function() {		      
		      $('.session-expirada').hide();
		      $('.datos-email-input').attr('mail','mail');
		      $('.datos-rut-input').attr('rut','rut');
			  $('.datos-fecha-input').attr('fecha','fecha');
		    });
		    
    
    		function isRutKey(e){
			    var unicode=e.charCode? e.charCode : e.keyCode;
			    if (unicode!==8&&unicode!==107&&unicode!==75){ 
			        if (unicode<48||unicode>57) 
			            return false;
			    }
			}
    		
			
			function replaceAll( text, busca, reemplaza ){

				while (text.toString().indexOf(busca) != -1)
				text = text.toString().replace(busca,reemplaza);
				return text;

			}

				
    		function autocompletarfecha(input) {
			
				if(input && input.value && input.value.length && input.value.length >= 8) {
								
										var value = replaceAll(input.value, "-", "" );
										var dia = value.substring(0,2);
										var mes = value.substring(2,4);
										var anio = value.substring(4,8);
    				    				input.value = dia + "-" + mes + "-" + anio;
				}
			  
    		}
			
			
    		function autocompletarrut(input) {
    			if(input && input.value && input.value.length && input.value.length > 1) {
    				input.value = input.value.replace("-","");
    				    				var value = input.value.substring(0, input.value.length -1);
    				    				var digit = input.value.substring(input.value.length -1, input.value.length);
    				    				input.value = value + "-" + digit;
    				    			}
    		}
    		
    		var rutResult = 0;
    		function validaRut(campo){
    			rutResult = 0;
    			if ( campo.length == 0 ){ rutResult = 1;return false; }
    			if ( campo.length < 8 ){ rutResult = 1;return false; }

    			campo = campo.replace('-','')
    			campo = campo.replace(/\./g,'')

    			var suma = 0;
    			var caracteres = "1234567890kK";
    			var contador = 0;    
    			for (var i=0; i < campo.length; i++){
    				u = campo.substring(i, i + 1);
    				if (caracteres.indexOf(u) != -1)
    				contador ++;
    			}
    			if ( contador==0 ) { rutResult = 1;return false }
    			
    			var rut = campo.substring(0,campo.length-1)
    			var drut = campo.substring( campo.length-1 )
    			var dvr = '0';
    			var mul = 2;
				var rut2 = parseInt(rut) ;
			
    			
    			for (i= rut.length -1 ; i >= 0; i--) {
    				suma = suma + rut.charAt(i) * mul
    		                if (mul == 7) 	mul = 2
    				        else	mul++
    			}
				
				if(suma == 0){

				return false;
				
				}
				
    			res = suma % 11
    			if (res==1)		dvr = 'k'
    		                else if (res==0) dvr = '0'
    			else {
    				dvi = 11-res
    				dvr = dvi + ""
    			}
    			if ( dvr != drut.toLowerCase() ) { rutResult = 1;return false; }
    			else { return true; }
    		}
    
    		var validacionInput = 0;
    		function validateInputRegistro(id) {
    			validacionInput = 0;
    			console.log('in validate input: ' + id);
    			$('#'+id+'').find(":input:not(:hidden,:button,:submit)").each(function(){
    				console.log('testing ' + this.id);
    				var optional = $(this).hasClass("optional");
    				if(optional==false) {
    					$(this).css("border-color","#cecece");
	    				if(!this.value) {
	    					$("#incomplete-text-"+id).html("Por favor complete el formulario correctamente");
	    					$("#incomplete-"+id).show("fast");
	    					$(this).css("border-color","red");
	    					$(this).focus();
	    					console.log(this.id + ' est� vac�o');
	    					validacionInput = 1;
		    				return false;
		    			} 
	    				if($(this).attr("mail")) {
	    					console.log("testing mail");
	    					var result = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(this.value);
	    					if(result == false) {
	    						$("#incomplete-text-"+id).html("El correo tiene el formato incorrecto");
	    						$("#incomplete-"+id).show("fast");
	    						$(this).css("border-color","red");
	    						$(this).focus();
	    						console.log(this.id + ' formato incorrecto');
	    						validacionInput = 1;
	    						return false;
	    					} 
	    				}
						if(this.id == "datos.rutcompleto") {
							console.log("testing rut");
							var result = validaRut(this.value);
							if(result==false){
								$("#incomplete-text-"+id).html("El rut tiene el formato incorrecto");
								$("#incomplete-"+id).show("fast");
								$(this).css("border-color","red");
								$(this).focus();
								console.log(this.id + ' formato incorrecto');
								validacionInput = 1;
								return false;
							} 
						}
					}
					
	    		});
				
    			if(validacionInput == 0) {
					$("#incomplete-text-"+id).html("");
					$("#incomplete-"+id).hide("fast");
					return true;
				}else{
					return false;
				}
    		}
    			    	
	    	function personalInfoRegistro() {
	    		if(document.getElementById('datos.rutcompleto')) {
	    	  		var rutCompleto = document.getElementById('datos.rutcompleto').value.split('-');
	    	  		if(rutCompleto.length > 0) {
		    			document.getElementById('datos.rut_cliente').value = rutCompleto[0];
		    			if(rutCompleto.length > 1) {
		    				document.getElementById('datos.dv_cliente').value = rutCompleto[1];
		    			}
	    			}
	    	  	}
	    		if(document.getElementById('fechaCompleta')) {
	    			var fechaCompleta = document.getElementById('fechaCompleta').value.split('-');
	    			if(fechaCompleta.length === 3) {
	    				document.getElementById('datos.diaFechaNacimiento').value = fechaCompleta[0];
	    				document.getElementById('datos.mesFechaNacimiento').value = fechaCompleta[1];
	    				document.getElementById('datos.anyoFechaNacimiento').value = fechaCompleta[2];
	    			}
	    		}
	    	}
			
			//calcular EDAD
			function calcularEdad(asegurado){
				if(asegurado){
				
					var dia = $("#diasFechaDuenyo").val();
					var mes = $("#mesFechaDuenyo").val();
					var ano = $("#anyosFechaDuenyo").val();
					
				}else{
				
					var dia = $("#diasFecha").val();
					var mes = $("#mesFecha").val();
					var ano = $("#anyosFecha").val();

				}
				
					// rescatamos los valores actuales
					var fecha_hoy = new Date();
					var ahora_ano = fecha_hoy.getYear();
					var ahora_mes = fecha_hoy.getMonth()+1;
					var ahora_dia = fecha_hoy.getDate();

					// realizamos el calculo
					var edad = (ahora_ano + 1900) - ano;
					if ( ahora_mes < mes ){
						edad--;
					}
					if ((mes == ahora_mes) && (ahora_dia < dia)){
						edad--;
					}
					if (edad > 1900){
						edad -= 1900;
					}

					// calculamos los meses
					var meses=0;
					if(ahora_mes>mes)
						meses=ahora_mes-mes;
					if(ahora_mes<mes)
						meses=12-(mes-ahora_mes);
					if(ahora_mes==mes && dia>ahora_dia)
						meses=11;

					// calculamos los dias
					var dias=0;
					if(ahora_dia>dia)
						dias=ahora_dia-dia;
					if(ahora_dia<dia){
						var ultimoDiaMes=new Date(ahora_ano, ahora_mes, 0);
						dias=ultimoDiaMes.getDate()-(dia-ahora_dia);
					}
					
					if(edad>18 && edad<75){
						return true;
					}else{
						return false;
					}	
			}
			
			
			
			$(document).ready(function(){
			
			// Tooltip.
			if(navigator.appName == "Microsoft Internet Explorer") {
				$(".tooltip_show").tooltip({position: "top right", offset: [30, 3]});
			}
			else{
				$(".tooltip_show").tooltip({position: "top right", offset: [20,3]});
			}
		  	
			});
			</script>
			
		<style type="text/css">
			.ui-datepicker {
				color: #000000 width :       216px;
				height: auto;
				margin: 5px auto 0;
				font: 9pt Arial, sans-serif;
				-webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
				-moz-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
				box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
			}

			.ui-datepicker-month {
				color: #000000
			}

			.ui-datepicker-year {
				color: #000000
			}
		</style>
	<%@ include file="../home/includes/head.jsp"%>
	</head>
	<script src='https://www.google.com/recaptcha/api.js?hl=es'></script>
	<body>
		<%@ include file="../google-analytics/google-analytics.jsp" %>
		<%@ include file="../home/includes/header.jsp" %>
<!-- Inicio Responsive -->
<!-- INICIO  BREADCRUMB -->			
		<div class="container contenedor-sitio">
			<ol class="breadcrumb hidden-xs">
			  <li><a href="/vseg-paris/index.html">Home</a></li>
			  <li class="active">Reg�strate</li>
			</ol>
			<div class="row hidden-xs">
			  <h4 class="titulo-cotizacion">REG�STRATE EN SEGUROS CENCOSUD Y PODR�S:</h4>
			</div>
		</div>
<!-- FIN  BREADCRUMB --> 		
<!-- INICIO  CONTENIDOS -->
<html:form action="/registrarCliente" styleId="formulario" >
	<html:hidden property="datos(rut_cliente)" styleId="datos.rut_cliente"/>
	<html:hidden property="datos(dv_cliente)" styleId="datos.dv_cliente"/>
	<html:hidden property="datos(diaFechaNacimiento)" styleId="datos.diaFechaNacimiento" />
	<html:hidden property="datos(mesFechaNacimiento)" styleId="datos.mesFechaNacimiento" />
	<html:hidden property="datos(anyoFechaNacimiento)" styleId="datos.anyoFechaNacimiento" />
	<div class="container">
		<div class="row hidden-xs">
			<div class="col-md-4 col-sm-4 col-xs-4 contenedor-caja-registro">
			    <h5><strong>COTIZAR Y CONTRATAR DE FORMA M�S R�PIDA</strong></h5>
			    <p>Tu informaci�n ya estar� ingresada previamente en los formularios al momento de cotizar.</p>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-4 contenedor-caja-registro">
			    <h5><strong>TU INFORMACI�N A UN SOLO CLICK</strong> </h5>
			    <p>Todas tus cotizaciones y p�lizas estar�n guardadas en Mis Seguros Online.</p>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-4 contenedor-caja-registro">
			    <h5><strong>OFERTAS EXCLUSIVAS</strong></h5>
			    <p>Te actualizaremos con nuevas ofertas y promociones peri�dicamente.</p>
			</div>
		</div>
		<div class="alert alert-warning top20" id="incomplete-formularioRegistro" style="display:none">
			<p align="center" id="incomplete-text-formularioRegistro"></p>
		</div>
		<logic:present name="MensajeError">			
			<div class="alert alert-warning" id="incomplete-error">
				<p align="center" id="incomplete-text-error">
					<bean:write name="MensajeError"/>
				</p>
			</div>
		</logic:present>
		<div class="row top15 registro" id="formularioRegistro">
			<div class="col-md-6 col-sm-6 col-xs-12 caja-form-fondo1">
				<h2 class="titulo-cotizacion"><img src="/vseg-paris/img/1.jpg" width="33" height="33" alt="1" class="caja-imagen-numero">Ingresa tus datos</h2>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-horizontal">
							<div class="form-group">
								<label for="inputRut" class="col-sm-4 control-label hidden-xs">Rut</label>
								<div class="col-sm-6">
									<html:text styleClass="form-control datos-rut-input"
										property="datos(rutCompleto)" maxlength="10"
										styleId="datos.rutcompleto"
										onkeypress="return isRutKey(event)"
										onkeyup="autocompletarrut(this);" />
								</div>
							</div>
							<div class="form-group">
								<label for="inputNumeroSerie" class="col-sm-4 control-label hidden-xs">N� de Serie</label>
								<div class="col-sm-5 col-xs-9">
									<html:text property="datos(numero_serie)" styleId="datos.numero_serie" styleClass="form-control datos-serie-input" size="24"/>
									<span class="texto-chico-informativo-osc">Sin puntos, ej: 200300400 o A012345678</span>	
								</div>
								<a id="popover" class="btn" rel="popover"><img src="/vseg-paris/img/tooltip.png"></a>
							</div>
							<div class="clearfix"></div>
							<div class="form-group">
								<label for="inputNombre" class="col-sm-4 control-label hidden-xs">Nombre</label>
								<div class="col-sm-6">
									<html:text property="datos(nombre)"
										styleClass="form-control datos-nombre-input" size="16"
										styleId="datos.nombre" maxlength="25"
										onkeypress="return validar(event)"/>
								</div>
							</div>
							<div class="form-group">
								<label for="inputApellidoPaterno" class="col-sm-4 control-label hidden-xs">Apellido Paterno</label>
								<div class="col-sm-6">
									<html:text property="datos(apellido_paterno)"
										styleClass="form-control datos-paterno-input"
										styleId="datos.apellido_paterno" size="16" maxlength="25"
										onkeypress="return validar(event)" />
								</div>
							</div>
							<div class="form-group">
								<label for="inputApellidoMaterno" class="col-sm-4 control-label hidden-xs">Apellido Materno</label>
								<div class="col-sm-6">
									<html:text property="datos(apellido_materno)"
										styleClass="form-control datos-materno-input"
										styleId="datos.apellido_materno" size="16" maxlength="25"
										onkeypress="return validar(event)" />
								</div>
							</div>
							<div class="form-group">
								<label for="inputTelefono" class="col-sm-4 control-label hidden-xs">Tel�fono</label>
								<div class="col-sm-8 telefono">
									<div class="col-md-3 col-xs-4 pleft">
										<html:select property="datos(tipo_telefono_1)"
											styleClass="form-control" styleId="datos.tipo_telefono_1">
											<html:option value="">Seleccione</html:option>
											<html:optionsCollection name="tiposTelefono"
												label="descripcion" value="valor" />
										</html:select>
									</div>
									<div class="col-md-3 col-xs-3 pleft">
										<html:select property="datos(codigo_area)" styleClass="form-control"
											styleId="datos.codigo_area">
											<option value="">
												Seleccione
											</option>
											<html:optionsCollection name="codigosArea" label="descripcion"
												value="valor" />
										</html:select>
										<script type="text/javascript">	
											$(document).ready(function(){
												<logic:present property="datos(codigo_area)" name="registroClienteForm">
													valor = "<bean:write name='registroClienteForm' property='datos(codigo_area)'/>";
													$("select[name='datos(codigo_area)']").val(valor);
												</logic:present>
											});
										</script>
									</div>
									<div class="col-md-3 col-xs-5 p0">
										<html:text property="datos(telefono_1)" styleClass="form-control  datos-telefono-input"
											size="8" maxlength="8" styleId="datos.telefono_1" onkeypress="return isNumberKey(event)"/>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail" class="col-sm-4 control-label hidden-xs">Email</label>
								<div class="col-sm-6">
									<html:text property="datos(email)" styleId="datos.email" size="60" styleClass="form-control datos-email-input" 
										onkeypress="return isCaracterEmail(event);"/>
								</div>
							</div>
							<div class="form-group">
								<label for="inputFechaNacimiento" class="col-sm-4 control-label hidden-xs">Fecha de Nacimiento</label>
								<div class="col-sm-6">
									<html:text property="datos(fechaNacimiento)" size="60" styleClass="form-control datos-fecha-input"  
										maxlength="10" styleId="fechaCompleta" onblur="autocompletarfecha(this);" onfocus="autocompletarfecha(this);"/>
								</div>
							</div>
							<div class="form-group">
								<label for="inputEstadoCivil" class="col-sm-4 control-label hidden-xs">Estado Civil</label>
								<div class="col-sm-6">
									<html:select property="datos(estado_civil)" styleClass="form-control">
										<html:option value="">Seleccione Estado Civil</html:option>
										<html:options collection="estadosCiviles" property="id"
											labelProperty="descripcion" />
									</html:select>
								</div>
							</div>
							<div class="form-group">
								<label for="inputSexo" class="col-sm-4 control-label hidden-xs">Sexo</label>
								<div class="col-sm-6">
									<html:select styleClass="form-control"
										property="datos(sexo)" styleId="datos.sexo">
										<html:option value="">Seleccione Sexo</html:option>
										<html:option value="F">Femenino</html:option>
										<html:option value="M">Masculino</html:option>
									</html:select>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 caja-form-fondo2">
			    <h2 class="titulo-cotizacion"><img src="/vseg-paris/img/2.jpg" width="33" height="33" alt="2" class="caja-imagen-numero">Tu direcci�n</h2>
			    <div class="row">
					<div class='col-md-12 col-sm-12 col-xs-12'>
					    <div class="form-horizontal" role="form">
							<div class="form-group">
							    <label for="region" class="col-sm-4 control-label hidden-xs">Regi�n</label>
							    <div class="col-sm-6">
									<html:select property="datos(id_region)" styleClass="form-control"
										styleId="id_region">
									<html:option value="">Seleccione Regi�n</html:option>
									<html:options collection="regiones" property="id"
										labelProperty="descripcion" />
									</html:select>
							    </div>
							</div>
							<div class="form-group">
								<label for="comuna" class="col-sm-4 control-label hidden-xs">Comuna</label>
								<div class="col-sm-6">
									<html:select property="datos(id_comuna)" styleClass="form-control"
										styleId="id_comuna">
									<html:option value="">Seleccione Comuna</html:option>
									<html:options collection="comunas" property="id"
										labelProperty="descripcion" />
									</html:select>
								</div>
							</div>
							<div class="form-group">
								<label for="ciudad" class="col-sm-4 control-label hidden-xs">Ciudad</label>
								<div class="col-sm-6">
									<html:select property="datos(id_ciudad)" styleClass="form-control"
										styleId="id_ciudad">
									<html:option value="">Seleccione Ciudad</html:option>
									<html:options collection="ciudades" property="id"
										labelProperty="descripcion" />
									</html:select>
								</div>
							</div>   
							<div class="form-group">
								<label class="col-md-4 control-label hidden-xs">Direcci�n*</label>
								<div class="col-md-8 telefono">
									<div class="col-md-4 pleft">
										<div class='form-group m0direccion'>
											<html:text property="datos(calle)"
												styleClass="form-control datos-calle-input"
												maxlength="30" styleId="datos.calle" />
										</div>
									</div>
									<div class="col-md-3 col-xs-6 pleft">
										<div class='form-group m0'>
											<html:text property="datos(numero)"
												styleClass="form-control datos-direccion-input"
												maxlength="5" size="5" styleId="datos.numero"
												onkeypress="return isNumberKey(event)" />
										</div>
									</div>
									<div class="col-md-2 col-xs-6 p0">
										<div class='form-group m0'>
											<html:text property="datos(numero_departamento)"
												styleClass="form-control datos-depto-input optional" maxlength="5"
												size="10" styleId="datos.numero_departamento"
												onkeypress="return isNumberKey(event)" />
										</div>
									</div>
								</div>
							</div>
							<script>
								$(document).ready(function(){
									var valor = "";
							
									if($("#id_region").val() != null && $("#id_region").val() != "" ) {
									
										$.getJSON('/cotizador/buscar-comunas.do?idRegion=' + $("#id_region").val(), function(data){
											<logic:present property="datos(id_comuna)" name="registroClienteForm">
												valor = "<bean:write name='registroClienteForm' property='datos(id_comuna)'/>";
											</logic:present>
											$('select#id_comuna option').remove();
											var options = '';
											options += '<option value="">Seleccione Comuna</option>';
											for(var i = 0; i < data.length; i++) {
												if(valor!='' && valor==data[i].id) {
													options += '<option value="' + data[i].id + '" selected="selected">' + data[i].descripcion + '</option>';
												} else {
													options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
												}
											}
											$('select#id_comuna').html(options);
											recargarCiudades3(valor);
										});
									}
								});
						
								function recargarCiudades3(valor) {
									$.getJSON('/cotizador/buscar-ciudades.do?idComuna=' + valor, function(data){
											var valorCiudad = '';
											<logic:present property="datos(id_ciudad)" name="registroClienteForm">
												valorCiudad = "<bean:write name='registroClienteForm' property='datos(id_ciudad)'/>";
											</logic:present>

											$('select#id_ciudad option').remove();
											var options = '';
											options += '<option value="">Seleccione Ciudad</option>';
											for(var i = 0; i < data.length; i++) {
												if (valorCiudad!='' && valorCiudad==data[i].id) {
													options += '<option value="' + data[i].id + '" selected="selected">' + data[i].descripcion + '</option>';
												} else {
													options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
												}
											}
											$('select#id_ciudad').html(options);
									});
								}
							</script>
					    </div>
					</div>
			    </div>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 caja-form-fondo3">
			    <h2 class="titulo-cotizacion"><img src="/vseg-paris/img/3.jpg" width="33" height="33" alt="3" class="caja-imagen-numero">Tu clave</h2>
					<div class="row top15">
						<div class='col-md-12 col-sm-12 col-xs-12'>
							<div class="form-horizontal" role="form">
								<div class="form-group">
								    <label for="inputClave" class="col-sm-4 control-label hidden-xs">Clave</label>
								    <div class="col-sm-6">
										<html:password property="datos(clave)" styleId="datos.clave" styleClass="form-control datos-pass1-input" size="10" maxlength="6"/>
								    </div>
								</div>
								<div class="form-group">
								    <label for="inputRepetirClave" class="col-sm-4 control-label hidden-xs">Repetir Clave</label>
								    <div class="col-sm-6">
										<html:password property="datos(confirma_clave)" styleId="datos.confirma_clave" styleClass="form-control datos-pass2-input" size="10" maxlength="6"/>
								    </div>
								</div>
								<div class="form-group">
								    <div class='col-md-12 col-sm-12 col-xs-12'>
										<label class="checkbox-inline">
											<input type="checkbox" id="inlineCheckbox2" value="option2" checked>
											<p class="texto-chico-informativo-osc">Acepto recibir informaci�n de mis compras, promociones, ofertas de Seguros Cencosud y sus negocios asociados, v�a mail.</p>
										</label>
								    </div>
								</div>
								<div class="form-group">
								    <div class='col-md-12 col-sm-12 col-xs-12'>
										<div class="center-captcha">
											<!-- Test <div class="g-recaptcha" data-sitekey="6LdOYScTAAAAAEG_L0pnI3r3JSJB6C3ASBifqyes"></div>-->
											<!-- Produccion--><div class="g-recaptcha" data-sitekey="6Ld9ARUTAAAAAAkrHBRvrB_eTcMhJVDJBmDY5goB"></div>
										</div>
									</div>
								</div>
								<div class="form-group">
								<div class="col-md-3 hidden-xs"></div>
								    <div class='col-md-6 col-xs-12'>
										<button type="button" class="btn btn-primary btn-block" onclick="javascript:registarCliente();">ENVIAR</button>
								    </div>
								</div>
							<div class="col-md-3 hidden-xs"></div>
						</div>
					</div>
			    </div>
			</div>
		</div>
	</div>
<!-- FIN  CONTENIDOS -->
<!-- Fin Responsive -->
</html:form>
		<%@ include file="../home/includes/footer.jsp" %>
	</body>

</html>