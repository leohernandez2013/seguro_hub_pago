package cl.cencosud.ventaseguros.common.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.common.struts.form.CotizadorForm;
import cl.cencosud.ventaseguros.delegate.MantenedorClientesDelegate;
import cl.tinet.common.seguridad.model.UsuarioExterno;

public class EnviarFormularioCotizadorAction extends Action{
	
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        CotizadorForm oForm = (CotizadorForm) form;
        MantenedorClientesDelegate oDelegate = new MantenedorClientesDelegate();
        String rut, nombre, telefono, descSubcategoria;
        rut = oForm.getDatos().get("rut") +"-"+ oForm.getDatos().get("dv");
        nombre = oForm.getDatos().get("nombre");
        telefono = oForm.getDatos().get("area_telefono") + oForm.getDatos().get("telefono_1");
        descSubcategoria = request.getSession().getAttribute("subcategoriaDesc").toString();
        
        oDelegate.registrarCotizacion(rut, nombre, telefono, descSubcategoria);

        return mapping.findForward("continuar");
    }
}
