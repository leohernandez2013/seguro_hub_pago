package cl.cencosud.ventaseguros.common.struts.action;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.common.Ficha;
import cl.cencosud.ventaseguros.common.LegalFicha;
import cl.cencosud.ventaseguros.common.Rama;
import cl.cencosud.ventaseguros.common.Subcategoria;
import cl.cencosud.ventaseguros.delegate.FichaSubcategoriaDelegate;
import cl.cencosud.ventaseguros.delegate.PaginaIntermediaDelegate;

/**
 * Action encargado de desplegar una Ficha de Subcategoria.
 * <br/>
 * @author miguelgarcia
 * @version 1.0
 * @created 01/10/2010
 */
public class DesplegarFichaAction extends Action {
	static Log logger = LogFactory.getLog(DesplegarFichaAction.class);    
	@Override    
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        String idRama = request.getParameter("idRama");
        String idSubcategoria = request.getParameter("idSubcategoria");
        String productoVP = request.getParameter("VP");

        if (idRama == null) {
            idRama = (String) request.getSession().getAttribute("idRama_ficha");
            request.getSession().removeAttribute("idRama_ficha");
        }

        if (idSubcategoria == null) {
            idSubcategoria =
                (String) request.getSession().getAttribute(
                    "idSubcategoria_ficha");
            request.getSession().removeAttribute("idSubcategoria_ficha");
        }

        String idPlanPromocion = request.getParameter("idPlanPromocion");
        
        logger.info("IDrama: "  + idRama);
        logger.info("IDSubcategoria: "  + idSubcategoria);
        logger.info("IDPlanPromocion: "  + idPlanPromocion);
        
        FichaSubcategoriaDelegate delegate = new FichaSubcategoriaDelegate();

        List < Map < String, ? > > resultado = null;

        Map < String, ? >[] fichas = null;

        if (idPlanPromocion != null && idPlanPromocion.length() > 0) {
            request.setAttribute("idPlanPromocion", idPlanPromocion);
            logger.info("IDPlanPromocion distinto de null; buscando ficha con promoción");
            if (idSubcategoria == null)
                idSubcategoria = "0";

            if (idRama == null)
                idRama = "0";

            resultado =
                delegate.obtenerPlanesPromocion(Integer.parseInt(idRama),
                    Integer.parseInt(idSubcategoria));

            Ficha ficha_plan_prmocion =
                delegate.obtenerFicha(Long.parseLong(idPlanPromocion));
            
            LegalFicha legalFicha = delegate.obtenerLegalFicha(Long.parseLong(idPlanPromocion));
            
            logger.info("Ficha encontrada: " + ficha_plan_prmocion.getId_ficha());
            request.setAttribute("idFicha", ficha_plan_prmocion.getId_ficha());
            request.setAttribute("descripcionFicha", ficha_plan_prmocion
                .getDescripcion_ficha());
            
            if(legalFicha != null ){
	            request.setAttribute("legalFicha", legalFicha.getTexto_legal());
	            request.setAttribute("habilitarLegalFicha",legalFicha.getActivo());
	            
	            logger.info("Legal Ficha encontrada ");
	        
            }
            
            
	            request.setAttribute("idPlan", ficha_plan_prmocion.getId_plan()
	                .toString());

            if (resultado != null && !resultado.isEmpty()) {
                fichas =
                    (Map < String, ? >[]) resultado.toArray(new Map[resultado
                        .size()]);
            }

            request.setAttribute("fichas", fichas);
            request.setAttribute("promocion", 1);

        } else {
        	logger.info("idPlanPromocion = null, buscando ficha sin promoción");
        	resultado =
                delegate.obtenerFichasSubcategoria(Integer.parseInt(idRama),
                    Integer.parseInt(idSubcategoria));            

            if (resultado != null && !resultado.isEmpty()) {
                fichas =
                    (Map < String, ? >[]) resultado.toArray(new Map[resultado
                        .size()]);

                if (fichas != null && fichas.length > 0) {
                	logger.info("Ficha encontrada: " + fichas[0].get("id_Ficha"));
                    request.setAttribute("idFicha", fichas[0].get("id_Ficha"));
                    request.setAttribute("descripcionFicha", fichas[0]
                        .get("descripcion_ficha"));
                    System.out.println("IDFICHA :" + request.getAttribute("idFicha"));
                    
                    Long idFicha = ((BigDecimal) request.getAttribute("idFicha")).longValue();
                    
                    LegalFicha legalFicha = delegate.obtenerLegalFicha(idFicha);

                    
                    if(legalFicha != null ){
                    request.setAttribute("legalFicha", legalFicha.getTexto_legal());
                    request.setAttribute("habilitarLegalFicha",legalFicha.getActivo());
                    
    	            logger.info("Legal Ficha encontrada ");
                    
                    }
                }
            }
            
            if(request.getSession().getAttribute("idPlan") != null) {
                request.getSession().removeAttribute("idPlan");
            }

            request.setAttribute("fichas", fichas);

        } 

        // Obteniendo listado de Subcategorias por la rama.
        PaginaIntermediaDelegate paginaDelegate =
            new PaginaIntermediaDelegate();

        if (idPlanPromocion != null && idPlanPromocion.length() > 0) {

            List < Map < String, ? > > promocionRama =
                delegate.obtenerPlanesPromocion(Integer.parseInt(idRama), 0);
            request.setAttribute("planesPromocion", promocionRama);

        } else {

            List < Subcategoria > subcategorias =
                paginaDelegate.obtenerSubcategoriasPaginaIntermedia(Integer
                    .parseInt(idRama));
            request.setAttribute("subcategorias", subcategorias);

        }

        int iRama = Integer.parseInt(idRama);

        List < Rama > ramas = paginaDelegate.obtenerRamas();
        if (ramas != null && ramas.size() > 0) {
            for (Iterator iterator = ramas.iterator(); iterator.hasNext();) {
                Rama rama = (Rama) iterator.next();
                if (rama.getId_rama() == iRama) {
                    request
                        .setAttribute("ramaDesc", rama.getDescripcion_rama());
                    break;
                }
            }
        }

        // Se depliega la Rama a la cual corresponden las subcategorias.
        request.setAttribute("idRama", idRama);
        request.setAttribute("idSubcategoria", idSubcategoria);
        request.setAttribute("VP", productoVP);
        return mapping.findForward("desplegaFicha");

    }
}
