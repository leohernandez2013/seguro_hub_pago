package cl.cencosud.ventaseguros.eventos.struts.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import cl.cencosud.ventaseguros.delegate.MantenedorEventosDelegate;
import cl.tinet.common.model.exception.BusinessException;
import cl.tinet.common.seguridad.model.UsuarioExterno;
import cl.tinet.common.seguridad.util.SeguridadUtil;

import com.tinet.exceptions.system.SystemException;

public class ObtenerEventoAction extends Action {

    private static final Log logger =
        LogFactory.getLog(ObtenerEventoAction.class);

    private static final String UNDEFINED = "undefined";

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws BusinessException, SystemException {

        String posicion = request.getParameter("posicion");
        //String usuario = request.getParameter("usuario");
        String rama = request.getParameter("rama");
        String img = request.getParameter("img");

        UsuarioExterno usuarioext =
            (UsuarioExterno) SeguridadUtil.getUsuario(request);

        String usuario = null;
        if (usuarioext != null) {
            usuario = usuarioext.getRut_cliente();
        }

        MantenedorEventosDelegate delegate = new MantenedorEventosDelegate();
        try {
            response.setHeader("pragma", "no-cache");
            response.setHeader("cache-control", "no-cache");
            response.setDateHeader("expires", -1);

            if (img != null) {
                logger.info("Obtener imagen del evento " + img);

                ServletOutputStream out = null;
                Map < String, Object > archivo =
                    delegate.obtenerArchivo(img, "imagen");

                out = response.getOutputStream();
                response.setContentType("image/jpeg");
                response.setHeader("Content-Disposition",
                    "inline;filename=imagen.jpg");

                byte[] imagen = (byte[]) archivo.get("archivo");
                out.write(imagen, 0, imagen.length);

            } else {
                logger.info("Obtener evento para: " + posicion + ", " + usuario
                    + ", " + rama);

                //Obtener el evento.
                Map < String, Object > filtros =
                    new HashMap < String, Object >();
                if (posicion != null && !UNDEFINED.equals(posicion)) {
                    filtros.put("posicion", posicion);
                }
                if (usuario != null && !UNDEFINED.equals(usuario)) {
                    filtros.put("usuario", usuario);
                }
                if (rama != null && !UNDEFINED.equals(rama)) {
                    filtros.put("rama", rama);
                }

                Map < String, Object > evento =
                    delegate.obtenerEventoMostrar(filtros);

                response.setContentType("text/html");

                PrintWriter pwriter = response.getWriter();
                ObjectMapper mapper = new ObjectMapper();
                StringWriter json = new StringWriter();
                mapper.writeValue(json, evento);
                pwriter.write(json.toString());
            }
        } catch (IOException e) {
            logger.error("Error al obtener evento", e);
            throw new SystemException(e);
        }

        return null;
    }
}
