package cl.cencosud.ventaseguros.common.struts.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;



import cl.cencosud.ventaseguros.common.Rama;
import cl.cencosud.ventaseguros.common.Subcategoria;
import cl.cencosud.ventaseguros.delegate.PaginaIntermediaDelegate;

import com.tinet.exceptions.system.SystemException;

/**
 * Action encargado de desplegar Comunas.
 * <br/>
 * @author miguelgarcia
 * @version 1.0
 * @created 30/09/2010
 */
public class ObtenerSubcategoriasAction extends Action{		
	public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {
//        try {
//
//            PrintWriter pwritter = response.getWriter();
//            ObjectMapper mapper = new ObjectMapper();
//            StringWriter json = new StringWriter();
//            mapper.writeValue(json, "TEST");
//            pwritter.write(json.toString());
//
//        } catch (IOException e) {
//            throw new SystemException(e);
//        }   
        String idRama =
            request.getParameter("idRama") == null ? "1" : request
                .getParameter("idRama");
        idRama = "1";
        PaginaIntermediaDelegate delegate = new PaginaIntermediaDelegate();
        List< Rama > ramas = delegate.obtenerRamas();
        List < Subcategoria > listado = new ArrayList < Subcategoria>();
        
        for (Rama oRama : ramas){
        	listado.addAll(delegate.obtenerSubcategoriasPaginaIntermedia(oRama.getId_rama()));
        }
        Subcategoria[] subcategorias = null;
        if (listado != null && !listado.isEmpty()) {     
            subcategorias =
                (Subcategoria[]) listado.toArray(new Subcategoria[listado
                    .size()]);
            PrintWriter pwritter = response.getWriter();
            ObjectMapper mapper = new ObjectMapper();
            StringWriter json = new StringWriter();
            mapper.writeValue(json,subcategorias);
            pwritter.write(json.toString());
        }       
        if(request.getSession().getAttribute("idPlan") != null) {
            request.getSession().removeAttribute("idPlan");
        }
        request.setAttribute("subcategorias", subcategorias);
        request.setAttribute("tipoSeguro", idRama);
        return null;
        }	
}