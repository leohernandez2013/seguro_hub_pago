package cl.cencosud.ventaseguros.common.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.delegate.MantenedorClientesDelegate;

public class DesplegarFormularioCotizadorAction extends Action{
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {     
    	
    	MantenedorClientesDelegate oDelegate = new MantenedorClientesDelegate();
    	
    	String idSubcategoria, subcategoriaDesc;
    	idSubcategoria = request.getParameter("idSubcategoria");
    	request.getSession().setAttribute("idSubcategoria", idSubcategoria);
    	
    	subcategoriaDesc = oDelegate.obtenerSubcategoria(idSubcategoria);
    	request.getSession().setAttribute("subcategoriaDesc", subcategoriaDesc);
    	 	
        return mapping.findForward("continuar");

    }

}
