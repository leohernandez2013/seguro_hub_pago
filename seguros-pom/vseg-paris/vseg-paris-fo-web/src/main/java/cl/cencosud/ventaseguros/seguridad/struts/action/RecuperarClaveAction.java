/*
 * Generated by MyEclipse Struts
 * Template path: templates/java/JavaClass.vtl
 */
package cl.cencosud.ventaseguros.seguridad.struts.action;

import java.text.MessageFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.tinet.common.seguridad.config.SeguridadConfig;
import cl.tinet.common.seguridad.delegate.GestorDeSeguridadDelegate;
import cl.tinet.common.seguridad.model.UsuarioExterno;
import cl.tinet.common.util.validate.ValidacionUtil;

/** 
 * MyEclipse Struts
 * Creation date: 08-26-2010
 * 
 * XDoclet definition:
 * @struts.action validate="true"
 */
public class RecuperarClaveAction extends Action {

    /**
     *Cadena del flujo error.
     */
    public static final String FLUJO_ERROR = "error";
    /**
     * Cadena de flujo de exito.
     */
    public static final String FLUJO_EXITO = "continuar";
    /**
     * Cadena de flujo volver.
     */
    public static final String FLUJO_VOLVER = "inicio";
    /**
     * Cadena, de mail. 
     */
    public static final String MESSAGE_MAIL_USER =
        "cl.tinet.common.seguridad.MESSAGE_MAIL_USER";

    /** 
     * Metodo ActionForward struts.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        String flujo = null;
        String rut = null;
        String dv = null;

        SeguridadConfig config = SeguridadConfig.getInstance();

        try {
            rut = request.getParameter("rut").trim();
            dv = request.getParameter("dv").trim();

            if (rut.length() == 0 || dv.length() == 0) {
                flujo = "pedirRut";
            } else {
                try {
                    if (!ValidacionUtil.isValidoRUT(Long.parseLong(rut.trim()), dv
                        .charAt(0))) {
                        flujo = "pedirRut";
                        //request.setAttribute("MensajeError", "Rut invalido");
                    } else {
                        GestorDeSeguridadDelegate delegateSeguridad =
                            new GestorDeSeguridadDelegate();
                        if (delegateSeguridad.generarClaveTemporal(rut, dv, 1, 1)) {
    
                            String userName = rut + "-" + dv;
                            UsuarioExterno usuarioExterno =
                                delegateSeguridad
                                    .obtenerUsuarioExterno(userName, 1);
    
                            String correoUsuarioExterno =
                                MessageFormat.format(config
                                    .getString(MESSAGE_MAIL_USER), usuarioExterno
                                    .getEmail());
    
                            request.setAttribute("correo", correoUsuarioExterno);
    
                            flujo = FLUJO_EXITO;
                        } else {
                            flujo = FLUJO_ERROR;
                            request.setAttribute("MensajeError",
                                "Error al modificar clave - Usuario bloqueado");
                        }
    
                    }
                } catch(NumberFormatException e) {
                    //En caso de que ingresen mal el rut.
                    flujo = "pedirRut";                    
                }
            }

        } catch (NullPointerException e) {
            flujo = FLUJO_VOLVER;
        } catch (NumberFormatException e) {
            flujo = FLUJO_ERROR;
            request.setAttribute("MensajeError", "rut solo puede ser numerico");
        }

        return mapping.findForward(flujo);
    }
}