package cl.cencosud.ventaseguros.eventos.struts.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.delegate.MantenedorEventosDelegate;
import cl.tinet.common.model.exception.BusinessException;
import cl.tinet.common.seguridad.model.UsuarioExterno;
import cl.tinet.common.seguridad.util.SeguridadUtil;

import com.tinet.exceptions.system.SystemException;

public class VerEventoAction extends Action {

    private static final Log logger = LogFactory.getLog(VerEventoAction.class);

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws BusinessException, SystemException {

        MantenedorEventosDelegate delegate = new MantenedorEventosDelegate();

        String id = request.getParameter("id");
        logger.info("Ver evento: " + id);

        //Grabar el click en la base de datos. Si el usuario esta logeado se debe descontar del contador.
        UsuarioExterno usuarioext =
            (UsuarioExterno) SeguridadUtil.getUsuario(request);

        String usuario = null;
        if (usuarioext != null) {
            usuario = usuarioext.getRut_cliente();
            logger.info("Usuario logeado " + usuario);

            delegate.verEvento(Long.parseLong(id), Long.parseLong(usuario));
        }

        //Buscar url del evento para la salida.
        Map < String, Object > evento =
            delegate.obtenerEvento(Long.parseLong(id));
        String url = (String) evento.get("url");
        request.setAttribute("url", url);

        return mapping.findForward("continuar");
    }
}
