package cl.cencosud.ventaseguros.common.struts.action;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.common.struts.form.DatosClienteForm;
import cl.cencosud.ventaseguros.delegate.MantenedorClientesDelegate;
import cl.tinet.common.seguridad.model.UsuarioExterno;
import cl.tinet.common.seguridad.util.SeguridadUtil;

/**
 * Despliega la vista de Pagina Intermedia. <br/>
 * 
 * @author miguelgarcia
 * @version 1.0
 * @created 08/10/2010
 */
public class ActualizarDatosClienteGrabarAction extends Action {

    private static final Log logger =
        LogFactory.getLog(ActualizarDatosClienteGrabarAction.class);

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        MantenedorClientesDelegate delegate = new MantenedorClientesDelegate();
        DatosClienteForm oForm = (DatosClienteForm) form;

        Map < String, String > datos =
            (HashMap < String, String >) oForm.getDatos();

        String telefono1 = datos.get("codigo_area") + datos.get("telefono_1");

        datos.put("telefono_1", telefono1);

        delegate.actualizarCliente(datos);

        //actualizar variable de sesion
        UsuarioExterno usuario =
            (UsuarioExterno) SeguridadUtil.getUsuario(request);
        BeanUtils.populate(usuario, datos);

        Calendar cal = new GregorianCalendar();
        int year = Integer.valueOf(datos.get("anyoFechaNacimiento")).intValue();
        int month = Integer.valueOf(datos.get("mesFechaNacimiento")).intValue();
        int date = Integer.valueOf(datos.get("diaFechaNacimiento")).intValue();
        cal.set(year, month - 1, date);
        Date fechaNacimiento = cal.getTime();

        usuario.setFecha_nacimiento(fechaNacimiento);

        SeguridadUtil.setAutenticado(usuario, request);

        request.getSession().setAttribute("usuario_modificado", true);

        return mapping.findForward("continuar");
    }

}
