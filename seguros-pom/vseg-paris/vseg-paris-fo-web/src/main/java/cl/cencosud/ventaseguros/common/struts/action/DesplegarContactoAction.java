package cl.cencosud.ventaseguros.common.struts.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.common.Parametro;
import cl.cencosud.ventaseguros.delegate.MantenedorClientesDelegate;

public class DesplegarContactoAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        MantenedorClientesDelegate oDelegate = new MantenedorClientesDelegate();

        List < Map < String, ? >> tipoTelefono =
            oDelegate.obtenerParametro("TIPO_TELEFONO");
        request.setAttribute("tipoTelefono", tipoTelefono);
        
        List < Map < String, ? >> codigoArea =
            oDelegate.obtenerParametro("TEL_CODIGO_AREA");
        request.setAttribute("codigoArea", codigoArea);

        List < Map < String, ? >> tipoContacto =
            oDelegate.obtenerParametro("TIPO_CONTACTO");
        request.setAttribute("tipoComentario", tipoContacto);
        
        if(request.getSession().getAttribute("send")!=null) {
            request.setAttribute("send", "1");
            request.getSession().removeAttribute("send");
        }

        return mapping.findForward("continuar");
    }

}
