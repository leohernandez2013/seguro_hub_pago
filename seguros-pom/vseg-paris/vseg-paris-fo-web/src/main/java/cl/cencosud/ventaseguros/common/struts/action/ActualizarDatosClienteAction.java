package cl.cencosud.ventaseguros.common.struts.action;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.common.EstadoCivil;
import cl.cencosud.ventaseguros.common.Parametro;
import cl.cencosud.ventaseguros.common.struts.form.DatosClienteForm;
import cl.cencosud.ventaseguros.delegate.MantenedorClientesDelegate;
import cl.tinet.common.seguridad.model.UsuarioExterno;
import cl.tinet.common.seguridad.util.SeguridadUtil;

/**
 * Despliega la vista de Pagina Intermedia. <br/>
 * 
 * @author miguelgarcia
 * @version 1.0
 * @created 08/10/2010
 */
public class ActualizarDatosClienteAction extends Action {

    private static final Log logger =
        LogFactory.getLog(ActualizarDatosClienteAction.class);

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        MantenedorClientesDelegate delegate = new MantenedorClientesDelegate();
        DatosClienteForm oForm = (DatosClienteForm) form;
        
        if (request.getSession().getAttribute("usuario_modificado") != null) {
            boolean modificado =
                (Boolean) request.getSession().getAttribute(
                    "usuario_modificado");

            if (modificado) {
                request.setAttribute("usuario_modificado", true);
            }
            
            oForm.setDatos(new HashMap < String, String >());

            request.getSession().removeAttribute("usuario_modificado");
        }


        String idCiudad = "";
        String idComuna = "";
        String idRegion = "";
        if (oForm.getDatos().get("rut_cliente") == null) {
            // Obtener datos de sesion.
            UsuarioExterno usuario =
                (UsuarioExterno) SeguridadUtil.getUsuario(request);

            idCiudad = usuario.getId_ciudad();

            Map < String, String > datos = BeanUtils.describe(usuario);

            Parametro[] comuna = delegate.obtenerComunaPorCiudad(idCiudad);
            idComuna = comuna[0].getId();

            Parametro[] region = delegate.obtenerRegionPorComuna(idComuna);
            idRegion = region[0].getId();

            // Descomponer fecha de nacimiento
            if (datos.get("fecha_nacimiento") != null) {
                Calendar cal = Calendar.getInstance();
                Date fechaNacimiento = (Date) usuario.getFecha_nacimiento();
                cal.setTime(fechaNacimiento);
                datos.put("diaFechaNacimiento", String.valueOf(cal
                    .get(Calendar.DATE)));
                datos.put("mesFechaNacimiento", String.valueOf(cal
                    .get(Calendar.MONTH) + 1));
                datos.put("anyoFechaNacimiento", String.valueOf(cal
                    .get(Calendar.YEAR)));
                datos.put("maxDias", String.valueOf(cal
                    .getActualMaximum(Calendar.DATE)));
            }
            
            if (datos.get("telefono_1") != null) {
                String telefono1 = datos.get("telefono_1").trim();
                String cod = telefono1.substring(0, 1);

                if(cod.equals("2") || cod.equals("9") || cod.equals("8") || cod.equals("7")) {
                    datos.put("codigo_area", "0" + cod);
                    datos.put("telefono_1", telefono1.substring(1, telefono1
                        .length()));
                } else {
                    datos.put("codigo_area", telefono1.substring(0, 2));
                    datos.put("telefono_1", telefono1.substring(2, telefono1
                        .length()));
                }
            }

            datos.put("id_comuna", idComuna);
            datos.put("id_region", idRegion);

            oForm.setDatos(datos);
            logger.debug(datos);
        } else {
            idCiudad = oForm.getDatos().get("id_ciudad");
            idComuna = oForm.getDatos().get("id_comuna");
            idRegion = oForm.getDatos().get("id_region");
        }

        // Tipos de telefono.
        List < Map < String, ? >> tiposTelefono =
            delegate.obtenerParametro("TIPO_TELEFONO");
        request.setAttribute("tiposTelefono", tiposTelefono);
        logger.debug(tiposTelefono);

        // Codigos de area.
        List < Map < String, ? >> codigosArea =
            delegate.obtenerParametro("TEL_CODIGO_AREA");
        request.setAttribute("codigosArea", codigosArea);
        logger.debug(codigosArea);

        // Meses
        List < Map < String, ? >> meses = delegate.obtenerParametro("MESES");
        request.setAttribute("meses", meses.iterator());
        logger.debug(meses);

        // Agnos
        List < Map < String, ? >> anyos = delegate.obtenerParametro("ANYOS");
        request.setAttribute("anyos", anyos.iterator());
        logger.debug(anyos);

        // Estado civil.
        EstadoCivil[] estadosCiviles = delegate.obtenerEstadosCiviles();
        request.setAttribute("estadosCiviles", estadosCiviles);
        logger.debug(estadosCiviles);

        // Sexo
        List < Map < String, ? >> sexos = delegate.obtenerParametro("SEXO");
        request.setAttribute("sexos", sexos.iterator());
        logger.debug(sexos);

        // Region
        Parametro[] regiones = delegate.obtenerRegiones();
        request.setAttribute("regiones", regiones);
        logger.debug(regiones);

        // Comuna
        Parametro[] comunas = delegate.obtenerComunas(idRegion);
        request.setAttribute("comunas", comunas);
        logger.debug(comunas);

        // Ciudad
        Parametro[] ciudades = delegate.obtenerCiudades(idComuna);
        request.setAttribute("ciudades", ciudades);
        logger.debug(ciudades);


        return mapping.findForward("continuar");
    }
}
