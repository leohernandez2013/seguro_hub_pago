package cl.cencosud.ventaseguros.common;

import java.io.Serializable;
import java.util.Date;

/**
 * Subcateogria de producto.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 24/08/2010
 */
public class Subcategoria implements Serializable {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -7634940670536811107L;

    /**
     * Identificador de la subcategoria.
     */
    private int id_subcategoria;
    /**
     * Identificador de la rama.
     */
    private int id_rama;
    /**
     * Titulo de la subcategoria.
     */
    private String titulo_subcategoria;
    /**
     * Indicador de item eliminado.
     */
    private int es_eliminado;
    /**
     * Fecha de creacion de la subcategoria.
     */
    private Date fecha_creacion;
    /**
     * Fecha de la ultima modificacion.
     */
    private Date fecha_modificacion;
    /**
     * Identificador del pais.
     */
    private int id_pais;
    /**
     * Nombre de usuario que realizo la ultima modificacion.
     */
    private String username;

    /**
     * Descripcion de la Subcategoria.
     */
    private String descripcion_pagina;

    /**
     * Identificador del tipo de rama.
     */
    private int id_tipo;

    /**
     * @return retorna el valor del atributo id_subcategoria
     */
    public int getId_subcategoria() {
        return id_subcategoria;
    }

    /**
     * @param id_subcategoria a establecer en el atributo id_subcategoria.
     */
    public void setId_subcategoria(int id_subcategoria) {
        this.id_subcategoria = id_subcategoria;
    }

    /**
     * @return retorna el valor del atributo id_rama
     */
    public int getId_rama() {
        return id_rama;
    }

    /**
     * @param id_rama a establecer en el atributo id_rama.
     */
    public void setId_rama(int id_rama) {
        this.id_rama = id_rama;
    }

    /**
     * @return retorna el valor del atributo titulo_subcategoria
     */
    public String getTitulo_subcategoria() {
        return titulo_subcategoria;
    }

    /**
     * @param titulo_subcategoria a establecer en el atributo titulo_subcategoria.
     */
    public void setTitulo_subcategoria(String titulo_subcategoria) {
        this.titulo_subcategoria = titulo_subcategoria;
    }

    /**
     * @return retorna el valor del atributo es_eliminado
     */
    public int getEs_eliminado() {
        return es_eliminado;
    }

    /**
     * @param es_eliminado a establecer en el atributo es_eliminado.
     */
    public void setEs_eliminado(int es_eliminado) {
        this.es_eliminado = es_eliminado;
    }

    /**
     * @return retorna el valor del atributo fecha_creacion
     */
    public Date getFecha_creacion() {
        return fecha_creacion;
    }

    /**
     * @param fecha_creacion a establecer en el atributo fecha_creacion.
     */
    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    /**
     * @return retorna el valor del atributo fecha_modificacion
     */
    public Date getFecha_modificacion() {
        return fecha_modificacion;
    }

    /**
     * @param fecha_modificacion a establecer en el atributo fecha_modificacion.
     */
    public void setFecha_modificacion(Date fecha_modificacion) {
        this.fecha_modificacion = fecha_modificacion;
    }

    /**
     * @return retorna el valor del atributo id_pais
     */
    public int getId_pais() {
        return id_pais;
    }

    /**
     * @param id_pais a establecer en el atributo id_pais.
     */
    public void setId_pais(int id_pais) {
        this.id_pais = id_pais;
    }

    /**
     * @return retorna el valor del atributo username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username a establecer en el atributo username.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return retorna el valor del atributo descripcion_pagina
     */
    public String getDescripcion_pagina() {
        return descripcion_pagina;
    }

    /**
     * @param descripcionPagina a establecer en el atributo descripcion_pagina.
     */
    public void setDescripcion_pagina(String descripcionPagina) {
        descripcion_pagina = descripcionPagina;
    }
    
    /**
     * @return id_Tipo el valor del atributo id_tipo.
     */

    public int getId_tipo() {
        return id_tipo;
    }

    /**
     * @param idTipo a establecer en el atributo id_tipo.
     */
    
    public void setId_tipo(int idTipo) {
        id_tipo = idTipo;
    }    

}