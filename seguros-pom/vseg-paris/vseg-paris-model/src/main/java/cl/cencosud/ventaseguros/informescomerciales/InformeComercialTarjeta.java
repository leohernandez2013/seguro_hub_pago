package cl.cencosud.ventaseguros.informescomerciales;

public class InformeComercialTarjeta extends InformeComercial {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -7845074526336795296L;

    private String codigoautorizacion;

    private String digitostarjeta;

    public String getCodigoautorizacion() {
        return codigoautorizacion;
    }

    public void setCodigoautorizacion(String codigoautorizacion) {
        this.codigoautorizacion = codigoautorizacion;
    }

    public String getDigitostarjeta() {
        return digitostarjeta;
    }

    public void setDigitostarjeta(String digitostarjeta) {
        this.digitostarjeta = digitostarjeta;
    }



}
