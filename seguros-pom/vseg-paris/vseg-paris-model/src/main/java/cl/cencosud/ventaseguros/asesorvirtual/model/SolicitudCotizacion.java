package cl.cencosud.ventaseguros.asesorvirtual.model;

import java.io.Serializable;

public class SolicitudCotizacion implements Serializable,
    Comparable < SolicitudCotizacion > {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = -3585885885854681111L;
    private int numeroSolicitud;
    private int codigoCompania;
    private String nombreCompania;
    private String estado;
    private String nombreRamo;
    private String nombrePlan;
    private String fechaSolicitud;
    private String fechaInicioVigencia;
    private String fechaTerminoVigencia;
    private String moneda;
    private float primaMensual;
    private float primaAnual;
    private int codigoTipoProducto;
    private String codigoSubTipoProducto;
    private String descripcionSubTipoProducto;
    private String codigoProducto;
    private String descripcionProducto;
    private int codigoPlan;
    private int codigoCanal;
    private String nombreCanal;

    /**
     * @return retorna el valor del atributo numeroSolicitud
     */
    public int getNumeroSolicitud() {
        return numeroSolicitud;
    }

    /**
     * @param numeroSolicitud a establecer en el atributo numeroSolicitud.
     */
    public void setNumeroSolicitud(int numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    /**
     * @return retorna el valor del atributo codigoCompania
     */
    public int getCodigoCompania() {
        return codigoCompania;
    }

    /**
     * @param codigoCompania a establecer en el atributo codigoCompania.
     */
    public void setCodigoCompania(int codigoCompania) {
        this.codigoCompania = codigoCompania;
    }

    /**
     * @return retorna el valor del atributo nombreCompania
     */
    public String getNombreCompania() {
        return nombreCompania;
    }

    /**
     * @param nombreCompania a establecer en el atributo nombreCompania.
     */
    public void setNombreCompania(String nombreCompania) {
        this.nombreCompania = nombreCompania;
    }

    /**
     * @return retorna el valor del atributo estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado a establecer en el atributo estado.
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return retorna el valor del atributo nombreRamo
     */
    public String getNombreRamo() {
        return nombreRamo;
    }

    /**
     * @param nombreRamo a establecer en el atributo nombreRamo.
     */
    public void setNombreRamo(String nombreRamo) {
        this.nombreRamo = nombreRamo;
    }

    /**
     * @return retorna el valor del atributo nombrePlan
     */
    public String getNombrePlan() {
        return nombrePlan;
    }

    /**
     * @param nombrePlan a establecer en el atributo nombrePlan.
     */
    public void setNombrePlan(String nombrePlan) {
        this.nombrePlan = nombrePlan;
    }

    /**
     * @return retorna el valor del atributo fechaSolicitud
     */
    public String getFechaSolicitud() {
        return fechaSolicitud;
    }

    /**
     * @param fechaSolicitud a establecer en el atributo fechaSolicitud.
     */
    public void setFechaSolicitud(String fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    /**
     * @return retorna el valor del atributo fechaInicioVigencia
     */
    public String getFechaInicioVigencia() {
        return fechaInicioVigencia;
    }

    /**
     * @param fechaInicioVigencia a establecer en el atributo fechaInicioVigencia.
     */
    public void setFechaInicioVigencia(String fechaInicioVigencia) {
        this.fechaInicioVigencia = fechaInicioVigencia;
    }

    /**
     * @return retorna el valor del atributo fechaTerminoVigencia
     */
    public String getFechaTerminoVigencia() {
        return fechaTerminoVigencia;
    }

    /**
     * @param fechaTerminoVigencia a establecer en el atributo fechaTerminoVigencia.
     */
    public void setFechaTerminoVigencia(String fechaTerminoVigencia) {
        this.fechaTerminoVigencia = fechaTerminoVigencia;
    }

    /**
     * @return retorna el valor del atributo moneda
     */
    public String getMoneda() {
        return moneda;
    }

    /**
     * @param moneda a establecer en el atributo moneda.
     */
    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    /**
     * @return retorna el valor del atributo primaMensual
     */
    public float getPrimaMensual() {
        return primaMensual;
    }

    /**
     * @param primaMensual a establecer en el atributo primaMensual.
     */
    public void setPrimaMensual(float primaMensual) {
        this.primaMensual = primaMensual;
    }

    /**
     * @return retorna el valor del atributo primaAnual
     */
    public float getPrimaAnual() {
        return primaAnual;
    }

    /**
     * @param primaAnual a establecer en el atributo primaAnual.
     */
    public void setPrimaAnual(float primaAnual) {
        this.primaAnual = primaAnual;
    }

    /**
     * @return retorna el valor del atributo codigoTipoProducto
     */
    public int getCodigoTipoProducto() {
        return codigoTipoProducto;
    }

    /**
     * @param codigoTipoProducto a establecer en el atributo codigoTipoProducto.
     */
    public void setCodigoTipoProducto(int codigoTipoProducto) {
        this.codigoTipoProducto = codigoTipoProducto;
    }

    /**
     * @return retorna el valor del atributo codigoSubTipoProducto
     */
    public String getCodigoSubTipoProducto() {
        return codigoSubTipoProducto;
    }

    /**
     * @param codigoSubTipoProducto a establecer en el atributo codigoSubTipoProducto.
     */
    public void setCodigoSubTipoProducto(String codigoSubTipoProducto) {
        this.codigoSubTipoProducto = codigoSubTipoProducto;
    }

    /**
     * @return retorna el valor del atributo descripcionSubTipoProducto
     */
    public String getDescripcionSubTipoProducto() {
        return descripcionSubTipoProducto;
    }

    /**
     * @param descripcionSubTipoProducto a establecer en el atributo descripcionSubTipoProducto.
     */
    public void setDescripcionSubTipoProducto(String descripcionSubTipoProducto) {
        this.descripcionSubTipoProducto = descripcionSubTipoProducto;
    }

    /**
     * @return retorna el valor del atributo codigoProducto
     */
    public String getCodigoProducto() {
        return codigoProducto;
    }

    /**
     * @param codigoProducto a establecer en el atributo codigoProducto.
     */
    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    /**
     * @return retorna el valor del atributo descripcionProducto
     */
    public String getDescripcionProducto() {
        return descripcionProducto;
    }

    /**
     * @param descripcionProducto a establecer en el atributo descripcionProducto.
     */
    public void setDescripcionProducto(String descripcionProducto) {
        this.descripcionProducto = descripcionProducto;
    }

    /**
     * @return retorna el valor del atributo codigoPlan
     */
    public int getCodigoPlan() {
        return codigoPlan;
    }

    /**
     * @param codigoPlan a establecer en el atributo codigoPlan.
     */
    public void setCodigoPlan(int codigoPlan) {
        this.codigoPlan = codigoPlan;
    }

    /**
     * @return retorna el valor del atributo codigoCanal
     */
    public int getCodigoCanal() {
        return codigoCanal;
    }

    /**
     * @param codigoCanal a establecer en el atributo codigoCanal.
     */
    public void setCodigoCanal(int codigoCanal) {
        this.codigoCanal = codigoCanal;
    }

    /**
     * @return retorna el valor del atributo nombreCanal
     */
    public String getNombreCanal() {
        return nombreCanal;
    }

    /**
     * @param nombreCanal a establecer en el atributo nombreCanal.
     */
    public void setNombreCanal(String nombreCanal) {
        this.nombreCanal = nombreCanal;
    }

    /**
     * TODO Describir m�todo compareTo.
     * @param o
     * @return
     */
    public int compareTo(SolicitudCotizacion o) {
        Integer c1p1 = this.getCodigoTipoProducto();
        Integer c2p1 = o.getCodigoTipoProducto();
        
        return (c1p1.compareTo(c2p1));

        //        if (this.getNombreRamo() != null && o.getNombreRamo() != null) {
        //            return this.getNombreRamo().compareTo(o.getNombreRamo());
        //        } else {
        //            return 0;
        //        }
    }

}
