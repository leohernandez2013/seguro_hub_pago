package cl.cencosud.ventaseguros.asesorvirtual.model;

import java.io.Serializable;
import java.util.Date;

public class PlanLeg implements Serializable {

    private static final long serialVersionUID = -2280421593110837253L;

    private int id_plan;
    private String codigo_tipo_prod_leg;
    private String codigo_sub_tipo_prod_leg;
    private String codigo_producto_leg;
    private String codigo_plan_leg;
    private String nombre;
    private int id_pais;
    private int es_activo;
    private String compannia;
    private Date fecha_creacion;
    private Date fecha_modificacion;
    private int max_dias_ini_vig;
    private int inpeccinobsp;
    private int solicita_nuevo_usado;
    private int solicita_factura;
    private String forma_pago;

    /**
     * @return retorna el valor del atributo id_plan
     */
    public int getId_plan() {
        return id_plan;
    }

    /**
     * @param id_plan a establecer en el atributo id_plan.
     */
    public void setId_plan(int id_plan) {
        this.id_plan = id_plan;
    }

    /**
     * @return retorna el valor del atributo codigo_tipo_prod_leg
     */
    public String getCodigo_tipo_prod_leg() {
        return codigo_tipo_prod_leg;
    }

    /**
     * @param codigo_tipo_prod_leg a establecer en el atributo codigo_tipo_prod_leg.
     */
    public void setCodigo_tipo_prod_leg(String codigo_tipo_prod_leg) {
        this.codigo_tipo_prod_leg = codigo_tipo_prod_leg;
    }

    /**
     * @return retorna el valor del atributo codigo_sub_tipo_prod_leg
     */
    public String getCodigo_sub_tipo_prod_leg() {
        return codigo_sub_tipo_prod_leg;
    }

    /**
     * @param codigo_sub_tipo_prod_leg a establecer en el atributo codigo_sub_tipo_prod_leg.
     */
    public void setCodigo_sub_tipo_prod_leg(String codigo_sub_tipo_prod_leg) {
        this.codigo_sub_tipo_prod_leg = codigo_sub_tipo_prod_leg;
    }

    /**
     * @return retorna el valor del atributo codigo_producto_leg
     */
    public String getCodigo_producto_leg() {
        return codigo_producto_leg;
    }

    /**
     * @param codigo_producto_leg a establecer en el atributo codigo_producto_leg.
     */
    public void setCodigo_producto_leg(String codigo_producto_leg) {
        this.codigo_producto_leg = codigo_producto_leg;
    }

    /**
     * @return retorna el valor del atributo codigo_plan_leg
     */
    public String getCodigo_plan_leg() {
        return codigo_plan_leg;
    }

    /**
     * @param codigo_plan_leg a establecer en el atributo codigo_plan_leg.
     */
    public void setCodigo_plan_leg(String codigo_plan_leg) {
        this.codigo_plan_leg = codigo_plan_leg;
    }

    /**
     * @return retorna el valor del atributo nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre a establecer en el atributo nombre.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return retorna el valor del atributo id_pais
     */
    public int getId_pais() {
        return id_pais;
    }

    /**
     * @param id_pais a establecer en el atributo id_pais.
     */
    public void setId_pais(int id_pais) {
        this.id_pais = id_pais;
    }

    /**
     * @return retorna el valor del atributo es_activo
     */
    public int getEs_activo() {
        return es_activo;
    }

    /**
     * @param es_activo a establecer en el atributo es_activo.
     */
    public void setEs_activo(int es_activo) {
        this.es_activo = es_activo;
    }

    /**
     * @return retorna el valor del atributo compannia
     */
    public String getCompannia() {
        return compannia;
    }

    /**
     * @param compannia a establecer en el atributo compannia.
     */
    public void setCompannia(String compannia) {
        this.compannia = compannia;
    }

    /**
     * @return retorna el valor del atributo fecha_creacion
     */
    public Date getFecha_creacion() {
        return fecha_creacion;
    }

    /**
     * @param fecha_creacion a establecer en el atributo fecha_creacion.
     */
    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    /**
     * @return retorna el valor del atributo fecha_modificacion
     */
    public Date getFecha_modificacion() {
        return fecha_modificacion;
    }

    /**
     * @param fecha_modificacion a establecer en el atributo fecha_modificacion.
     */
    public void setFecha_modificacion(Date fecha_modificacion) {
        this.fecha_modificacion = fecha_modificacion;
    }

    /**
     * @return retorna el valor del atributo max_dias_ini_vig
     */
    public int getMax_dias_ini_vig() {
        return max_dias_ini_vig;
    }

    /**
     * @param max_dias_ini_vig a establecer en el atributo max_dias_ini_vig.
     */
    public void setMax_dias_ini_vig(int max_dias_ini_vig) {
        this.max_dias_ini_vig = max_dias_ini_vig;
    }

    /**
     * @return retorna el valor del atributo inpeccinobsp
     */
    public int getInpeccinobsp() {
        return inpeccinobsp;
    }

    /**
     * @param inpeccinobsp a establecer en el atributo inpeccinobsp.
     */
    public void setInpeccinobsp(int inpeccinobsp) {
        this.inpeccinobsp = inpeccinobsp;
    }

    /**
     * @return retorna el valor del atributo solicita_nuevo_usado
     */
    public int getSolicita_nuevo_usado() {
        return solicita_nuevo_usado;
    }

    /**
     * @param solicita_nuevo_usado a establecer en el atributo solicita_nuevo_usado.
     */
    public void setSolicita_nuevo_usado(int solicita_nuevo_usado) {
        this.solicita_nuevo_usado = solicita_nuevo_usado;
    }

    /**
     * @return retorna el valor del atributo solicita_factura
     */
    public int getSolicita_factura() {
        return solicita_factura;
    }

    /**
     * @param solicita_factura a establecer en el atributo solicita_factura.
     */
    public void setSolicita_factura(int solicita_factura) {
        this.solicita_factura = solicita_factura;
    }

    /**
     * @return retorna el valor del atributo forma_pago
     */
    public String getForma_pago() {
        return forma_pago;
    }

    /**
     * @param forma_pago a establecer en el atributo forma_pago.
     */
    public void setForma_pago(String forma_pago) {
        this.forma_pago = forma_pago;
    }

}
