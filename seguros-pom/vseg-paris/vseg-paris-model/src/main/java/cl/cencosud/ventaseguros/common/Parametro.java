package cl.cencosud.ventaseguros.common;

import java.io.Serializable;

/**
 * Clase encargada de transportar valores de listados en el proceso de cotizacion.
 * @author Miguel Garc�a (TInet).
 *
 */
public class Parametro implements Serializable {

	/**
	 * Identificador de la Clase para Serializaci�n.
	 */
	private static final long serialVersionUID = -8636502787055467934L;
	
	
	private String id;
	
	private String descripcion;

	public String getId() {
		return id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String toString() {
		String result = "";
		result += "(id=" + this.id + ")";
		result += "(descripcion=" + this.descripcion + ")";
		return result;
	}
	

}
