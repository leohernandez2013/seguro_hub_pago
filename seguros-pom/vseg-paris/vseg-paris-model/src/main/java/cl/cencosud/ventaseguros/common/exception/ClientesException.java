package cl.cencosud.ventaseguros.common.exception;

import cl.cencosud.ventaseguros.common.config.VSPErrorsConfig;
import cl.tinet.common.config.AbstractConfigurator;
import cl.tinet.common.model.exception.BusinessException;

/**
 * En caso de que el tipo de archivo no corresponda con el esperado.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 23/09/2010
 */
public class ClientesException extends BusinessException {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 1168274411458863623L;

    public static final String RUT_NO_VALIDO =
        "cl.cencosud.ventaseguros.common.exception.RUT_NO_VALIDO";

    public static final String USUARIO_NO_ENCONTRADO =
        "cl.cencosud.ventaseguros.common.exception.USUARIO_NO_ENCONTRADO";

    /**
     * constructor.
     * @param messageKey llave del mensaje.
     * @param arguments argumentos del mensaje.
     */
    public ClientesException(String messageKey, Object[] arguments) {
        super(messageKey, arguments);
    }

    /**
     * loadConfigurator.
     * @return configuracion.
     */
    @Override
    public AbstractConfigurator loadConfigurator() {
        return VSPErrorsConfig.getInstance();
    }

}
