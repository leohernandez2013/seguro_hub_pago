package cl.cencosud.ventaseguros.common;

import java.io.Serializable;
import java.util.Date;

/**
 * Subcateogria de producto.
 * <br/>
 * @author jisler
 * @version 1.0
 * @created 24/08/2010
 */
public class TipoRama implements Serializable {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -7634940670536811107L;

    /**
     * Identificador del tipo.
     */
    private int id_tipo;
    /**
     * Identificador de la rama.
     */
    private int id_rama;
    /**
     * Descripcion del tipo.
     */
    private String descripcion_tipo;
    
    /**
     * @return retorna el valor del atributo id_tipo
     */
    public int getId_tipo() {
        return id_tipo;
    }
    
    /**
     * @param idTipo a establecer en el atributo id_tipo
     */
    public void setId_tipo(int idTipo) {
        id_tipo = idTipo;
    }
    
    /**
     * @return retorna el valor del atributo id_rama
     */
    public int getId_rama() {
        return id_rama;
    }
    
    /**
     * @return idRama a establecer en el atributo id_rama
     */
    public void setId_rama(int idRama) {
        id_rama = idRama;
    }
    
    /**
     * @return retorna el valor del atributo descripcion_tipo
     */
    public String getDescripcion_tipo() {
        return descripcion_tipo;
    }
    
    /**
     * @param descripcionTipo a establecer en el atributo descripcion_tipo
     */
    public void setDescripcion_tipo(String descripcionTipo) {
        descripcion_tipo = descripcionTipo;
    }
}
