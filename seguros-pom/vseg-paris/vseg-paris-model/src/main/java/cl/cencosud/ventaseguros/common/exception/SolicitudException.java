package cl.cencosud.ventaseguros.common.exception;

/**
 * TODO Falta descripcion de clase ValorizacionException.
 * <br/>
 * @author Roberto San Mart�n
 * @version 1.0
 * @created Oct 6, 2010
 */
public class SolicitudException extends VSPException {

    /**
     * TODO Describir atributo serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * TODO Describir atributo ERROR_VALORIZACION_KEY.
     */
    public static final String ERROR_VALORIZACION_KEY =
        "cl.cencosud.ventaseguros.cotizacion.exception.ERROR_VALORIZACION";

    /**
     * TODO Describir atributo codigo.
     */
    private int codigo;

    /**
     * TODO Describir constructor de ValorizacionException.
     * @param mensaje
     * @param codigo
     */
    public SolicitudException(String mensaje, int codigo) {
        super(ERROR_VALORIZACION_KEY, new Object[] { mensaje, codigo });
        this.codigo = codigo;
    }

    /**
     * TODO Describir m�todo getCodigo.
     * @return
     */
    public int getCodigo() {
        return codigo;
    }
}
