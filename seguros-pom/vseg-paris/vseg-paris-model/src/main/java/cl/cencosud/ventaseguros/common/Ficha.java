package cl.cencosud.ventaseguros.common;

import java.io.Serializable;
import java.util.Date;

public class Ficha implements Serializable {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = -6031649600360017018L;
	/**
	 * Indentificador de la ficha.
	 */
	private Long id_ficha;
	/**
	 * Identificador de la subcategoria.
	 */
	private Integer id_subcategoria;
	/**
	 * Identificador del plan.
	 */
	private Integer id_plan;
	/**
	 * Nombre comercial.
	 */
	private String nombre_comercial;
	/**
	 * Descripci�n de la ficha.
	 */
	private String descripcion_ficha;
	/**
	 * Descripci�n de la pagina.
	 */
	private String descripcion_pagina;
	/**
	 * Es subcategor�a.
	 */
	private Integer es_subcategoria;
	/**
	 * Estado de la ficha.
	 */
	private Integer estado_ficha;
	/**
	 * Monto en UF.
	 */
	private Double monto_uf;
	/**
	 * Idenficador del pa�s.
	 */
	private Integer id_pais;
	/**
	 * fecha de creaci�n.
	 */
	private Date fecha_creacion;
	/**
	 * Fecha de modificaci�n.
	 */
	private Date fecha_modificacion;

	/**
	 * @return retorna el valor del atributo id_ficha
	 */
	public Long getId_ficha() {
		return id_ficha;
	}

	/**
	 * @param id_ficha
	 *            a establecer en el atributo id_ficha.
	 */
	public void setId_ficha(Long id_ficha) {
		this.id_ficha = id_ficha;
	}

	/**
	 * @return retorna el valor del atributo id_subcategoria
	 */
	public Integer getId_subcategoria() {
		return id_subcategoria;
	}

	/**
	 * @param id_subcategoria
	 *            a establecer en el atributo id_subcategoria.
	 */
	public void setId_subcategoria(Integer id_subcategoria) {
		this.id_subcategoria = id_subcategoria;
	}

	/**
	 * @return retorna el valor del atributo id_plan
	 */
	public Integer getId_plan() {
		return id_plan;
	}

	/**
	 * @param id_plan
	 *            a establecer en el atributo id_plan.
	 */
	public void setId_plan(Integer id_plan) {
		this.id_plan = id_plan;
	}

	/**
	 * @return retorna el valor del atributo nombre_comercial
	 */
	public String getNombre_comercial() {
		return nombre_comercial;
	}

	/**
	 * @param nombre_comercial
	 *            a establecer en el atributo nombre_comercial.
	 */
	public void setNombre_comercial(String nombre_comercial) {
		this.nombre_comercial = nombre_comercial;
	}

	/**
	 * @return retorna el valor del atributo descripcion_ficha
	 */
	public String getDescripcion_ficha() {
		return descripcion_ficha;
	}

	/**
	 * @param descripcion_ficha
	 *            a establecer en el atributo descripcion_ficha.
	 */
	public void setDescripcion_ficha(String descripcion_ficha) {
		this.descripcion_ficha = descripcion_ficha;
	}

	/**
	 * @return retorna el valor del atributo descripcion_pagina
	 */
	public String getDescripcion_pagina() {
		return descripcion_pagina;
	}

	/**
	 * @param descripcion_pagina
	 *            a establecer en el atributo descripcion_pagina.
	 */
	public void setDescripcion_pagina(String descripcion_pagina) {
		this.descripcion_pagina = descripcion_pagina;
	}

	/**
	 * @return retorna el valor del atributo es_subcategoria
	 */
	public Integer getEs_subcategoria() {
		return es_subcategoria;
	}

	/**
	 * @param es_subcategoria
	 *            a establecer en el atributo es_subcategoria.
	 */
	public void setEs_subcategoria(Integer es_subcategoria) {
		this.es_subcategoria = es_subcategoria;
	}

	/**
	 * @return retorna el valor del atributo estado_ficha
	 */
	public Integer getEstado_ficha() {
		return estado_ficha;
	}

	/**
	 * @param estado_ficha
	 *            a establecer en el atributo estado_ficha.
	 */
	public void setEstado_ficha(Integer estado_ficha) {
		this.estado_ficha = estado_ficha;
	}

	/**
	 * @return retorna el valor del atributo monto_uf
	 */
	public Double getMonto_uf() {
		return monto_uf;
	}

	/**
	 * @param monto_uf
	 *            a establecer en el atributo monto_uf.
	 */
	public void setMonto_uf(Double monto_uf) {
		this.monto_uf = monto_uf;
	}

	/**
	 * @return retorna el valor del atributo id_pais
	 */
	public Integer getId_pais() {
		return id_pais;
	}

	/**
	 * @param id_pais
	 *            a establecer en el atributo id_pais.
	 */
	public void setId_pais(Integer id_pais) {
		this.id_pais = id_pais;
	}

	/**
	 * @return retorna el valor del atributo fecha_creacion
	 */
	public Date getFecha_creacion() {
		return fecha_creacion;
	}

	/**
	 * @param fecha_creacion
	 *            a establecer en el atributo fecha_creacion.
	 */
	public void setFecha_creacion(Date fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}

	/**
	 * @return retorna el valor del atributo fecha_modificacion
	 */
	public Date getFecha_modificacion() {
		return fecha_modificacion;
	}

	/**
	 * @param fecha_modificacion
	 *            a establecer en el atributo fecha_modificacion.
	 */
	public void setFecha_modificacion(Date fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}

}
