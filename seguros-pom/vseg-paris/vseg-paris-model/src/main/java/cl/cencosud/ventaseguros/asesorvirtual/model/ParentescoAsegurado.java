package cl.cencosud.ventaseguros.asesorvirtual.model;

import java.io.Serializable;

public class ParentescoAsegurado implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 8577735914088242849L;

    private long idParentesco;

    private String codigoLegParentesco;

    private String nombre;

    public long getIdParentesco() {
        return idParentesco;
    }

    public void setIdParentesco(long idParentesco) {
        this.idParentesco = idParentesco;
    }

    public String getCodigoLegParentesco() {
        return codigoLegParentesco;
    }

    public void setCodigoLegParentesco(String codigoLegParentesco) {
        this.codigoLegParentesco = codigoLegParentesco;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
