package cl.cencosud.ventaseguros.asesorvirtual.model;

import java.io.Serializable;

/**
 * Alternativa como respuesta de una pregunta.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 09/09/2010
 */
//@XStreamAlias("alternativa")
public class Alternativa implements Serializable {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 7378438799546482250L;

    /**
     * Identificador de la alternativa.
     */
    //@XStreamAlias("id")
    //@XStreamAsAttribute
    private int id;

    /**
     * Primer texto de la alternativa.
     */
    //@XStreamAlias("texto0")
    //@XStreamAsAttribute
    private String texto0;

    /**
     * Segundo texto de la alternativa.
     */
    //@XStreamAlias("texto1")
    //@XStreamAsAttribute
    private String texto1;

    /**
     * Primer valor de la alternativa.
     */
    //@XStreamAlias("valor0")
    //@XStreamAsAttribute
    private String valor0;

    /**
     * Segundo valor de la alternativa.
     */
    //@XStreamAlias("valor1")
    //@XStreamAsAttribute
    private String valor1;

    /**
     * Respuesta asociada a la alternativa.
     */
    //@XStreamAlias("respuesta")
    //@XStreamAsAttribute
    private int respuesta;

    //@XStreamAlias("proximaPregunta")
    //@XStreamAsAttribute
    private int proximaPregunta;

    /**
     * @return retorna el valor del atributo id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id a establecer en el atributo id.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return retorna el valor del atributo texto0
     */
    public String getTexto0() {
        return texto0;
    }

    /**
     * @param texto0 a establecer en el atributo texto0.
     */
    public void setTexto0(String texto0) {
        this.texto0 = texto0;
    }

    /**
     * @return retorna el valor del atributo texto1
     */
    public String getTexto1() {
        return texto1;
    }

    /**
     * @param texto1 a establecer en el atributo texto1.
     */
    public void setTexto1(String texto1) {
        this.texto1 = texto1;
    }

    /**
     * @return retorna el valor del atributo valor0
     */
    public String getValor0() {
        return valor0;
    }

    /**
     * @param valor0 a establecer en el atributo valor0.
     */
    public void setValor0(String valor0) {
        this.valor0 = valor0;
    }

    /**
     * @return retorna el valor del atributo valor1
     */
    public String getValor1() {
        return valor1;
    }

    /**
     * @param valor1 a establecer en el atributo valor1.
     */
    public void setValor1(String valor1) {
        this.valor1 = valor1;
    }

    /**
     * @return retorna el valor del atributo respuesta
     */
    public int getRespuesta() {
        return respuesta;
    }

    /**
     * @param respuesta a establecer en el atributo respuesta.
     */
    public void setRespuesta(int respuesta) {
        this.respuesta = respuesta;
    }

    /**
     * @return retorna el valor del atributo proximaPregunta
     */
    public int getProximaPregunta() {
        return proximaPregunta;
    }

    /**
     * @param proximaPregunta a establecer en el atributo proximaPregunta.
     */
    public void setProximaPregunta(int proximaPregunta) {
        this.proximaPregunta = proximaPregunta;
    }

}
