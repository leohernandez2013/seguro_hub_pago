package cl.cencosud.ventaseguros.asesorvirtual.model;

import java.io.Serializable;
import java.util.List;

/**
 * Pregunta.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 09/09/2010
 */
//@XStreamAlias("pregunta")
public class Pregunta implements Serializable {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 4810947667726318201L;

    /**
     * Identificador de la pregunta.
     */
    //@XStreamAlias("id")
    //@XStreamAsAttribute
    private int id;

    /**
     * Titulo de la pergunta.
     */
    //@XStreamAlias("titulo")
    //@XStreamAsAttribute
    private String titulo;

    /**
     * Tipo de Pregunta.
     */
    //@XStreamAlias("tipoPregunta")
    //@XStreamAsAttribute
    private String tipoPregunta;

    /**
     * Alternativas con las posibles respuestas.
     */
    //@XStreamImplicit(itemFieldName = "alternativa")
    private List < Alternativa > alternativas;

    /**
     * @return retorna el valor del atributo id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id a establecer en el atributo id.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return retorna el valor del atributo titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo a establecer en el atributo titulo.
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return retorna el valor del atributo tipoPregunta
     */
    public String getTipoPregunta() {
        return tipoPregunta;
    }

    /**
     * @param tipoPregunta a establecer en el atributo tipoPregunta.
     */
    public void setTipoPregunta(String tipoPregunta) {
        this.tipoPregunta = tipoPregunta;
    }

    /**
     * @return retorna el valor del atributo alternativas
     */
    public List < Alternativa > getAlternativas() {
        return alternativas;
    }

    /**
     * @param alternativas a establecer en el atributo alternativas.
     */
    public void setAlternativas(List < Alternativa > alternativas) {
        this.alternativas = alternativas;
    }

}
