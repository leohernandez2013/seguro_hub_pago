package cl.cencosud.ventaseguros.common;

import java.io.Serializable;
import java.util.Date;

/**
 * Definicion de producto legacy.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 01/09/2010
 */
public class ProductoLeg implements Serializable {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = -3137204394046087494L;

    /**
     * Indetificador del producto.
     */
    private int id_producto;
    /**
     * Codigo del tipo de producto en tablas legacy.
     */
    private int codigo_tipo_prod_leg;
    /**
     * codigo de subtipo de producto en tablas legacy.
     */
    private int codigo_sub_tipo_prod_leg;
    /**
     * Codigo de producto en tablas legacy.
     */
    private int id_producto_leg;
    /**
     * Nombre del producto.
     */
    private String nombre;
    /**
     * Identificador del pais.
     */
    private int id_pais;
    /**
     * Flag de producto activo.
     */
    private int es_activo;
    /**
     * Fecha de creacion del producto.
     */
    private Date fecha_creacion;
    /**
     * Fecha de modificacion del producto.
     */
    private Date fecha_modificacion;
    
    /**
     * Identificador si el producto tiene 
     * habilitado el comparador de planes.
     */
    private int comparador_activo;

    /**
     * @return retorna el valor del atributo id_producto
     */
    public int getId_producto() {
        return id_producto;
    }

    /**
     * @param id_producto a establecer en el atributo id_producto.
     */
    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    /**
     * @return retorna el valor del atributo codigo_tipo_prod_leg
     */
    public int getCodigo_tipo_prod_leg() {
        return codigo_tipo_prod_leg;
    }

    /**
     * @param codigo_tipo_prod_leg a establecer en el atributo codigo_tipo_prod_leg.
     */
    public void setCodigo_tipo_prod_leg(int codigo_tipo_prod_leg) {
        this.codigo_tipo_prod_leg = codigo_tipo_prod_leg;
    }

    /**
     * @return retorna el valor del atributo codigo_sub_tipo_prod_leg
     */
    public int getCodigo_sub_tipo_prod_leg() {
        return codigo_sub_tipo_prod_leg;
    }

    /**
     * @param codigo_sub_tipo_prod_leg a establecer en el atributo codigo_sub_tipo_prod_leg.
     */
    public void setCodigo_sub_tipo_prod_leg(int codigo_sub_tipo_prod_leg) {
        this.codigo_sub_tipo_prod_leg = codigo_sub_tipo_prod_leg;
    }

    /**
     * @return retorna el valor del atributo codigo_producto_leg
     */
    public int getId_producto_leg() {
        return id_producto_leg;
    }

    /**
     * @param codigo_producto_leg a establecer en el atributo codigo_producto_leg.
     */
    public void setId_producto_leg(int id_producto_leg) {
        this.id_producto_leg = id_producto_leg;
    }

    /**
     * @return retorna el valor del atributo nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre a establecer en el atributo nombre.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return retorna el valor del atributo id_pais
     */
    public int getId_pais() {
        return id_pais;
    }

    /**
     * @param id_pais a establecer en el atributo id_pais.
     */
    public void setId_pais(int id_pais) {
        this.id_pais = id_pais;
    }

    /**
     * @return retorna el valor del atributo es_activo
     */
    public int getEs_activo() {
        return es_activo;
    }

    /**
     * @param es_activo a establecer en el atributo es_activo.
     */
    public void setEs_activo(int es_activo) {
        this.es_activo = es_activo;
    }

    /**
     * @return retorna el valor del atributo fecha_creacion
     */
    public Date getFecha_creacion() {
        return fecha_creacion;
    }

    /**
     * @param fecha_creacion a establecer en el atributo fecha_creacion.
     */
    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    /**
     * @return retorna el valor del atributo fecha_modificacion
     */
    public Date getFecha_modificacion() {
        return fecha_modificacion;
    }

    /**
     * @param fecha_modificacion a establecer en el atributo fecha_modificacion.
     */
    public void setFecha_modificacion(Date fecha_modificacion) {
        this.fecha_modificacion = fecha_modificacion;
    }

	public int getComparador_activo() {
		return comparador_activo;
	}

	public void setComparador_activo(int comparadorActivo) {
		comparador_activo = comparadorActivo;
	}    
    

}
