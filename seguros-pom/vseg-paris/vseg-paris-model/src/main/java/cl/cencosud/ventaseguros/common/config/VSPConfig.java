package cl.cencosud.ventaseguros.common.config;

import java.util.Locale;
import java.util.ResourceBundle;

import cl.tinet.common.config.AbstractConfigurator;

/**
 * VSPConfig.
 * <br/>
 * @author tinet
 * @version 1.0
 * @created Jan 27, 2010
 */
public class VSPConfig extends AbstractConfigurator {

    /**
     * instance.
     */
    private static VSPConfig instance = new VSPConfig();

    /**
     * getInstance.
     * @return config.
     */
    public static VSPConfig getInstance() {
        return instance;
    }

    /**
     *  loadBundle.
     * @param locale local.
     * @return recurso.
     */
    public ResourceBundle loadBundle(Locale locale) {
        return ResourceBundle.getBundle("VSPConfig", locale);
    }
}
