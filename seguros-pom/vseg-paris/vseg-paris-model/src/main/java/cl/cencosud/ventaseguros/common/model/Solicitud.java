package cl.cencosud.ventaseguros.common.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Define una solicitud de compra de seguro.
 * @author ghost23
 * @version 1.0
 * @created 26/10/2010
 */
public class Solicitud implements Serializable {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = -3299434357567044154L;

    private long id_solicitud;
    private long id_plan;
    private long cliente_es_asegurado;
    private String id_rama;
    private String id_producto;
    private String estado_solicitud;
    private String nro_tarjeta_referencia;
    private Date fecha_creacion;
    private Date fecha_modificacion;
    private int id_pais;
    private long rut_usuario;
    private String descripcion_rama;
    private String nombre_plan;
    private int tipo_vehiculo;

    /**
     * @return retorna el valor del atributo id_solicitud
     */
    public long getId_solicitud() {
        return id_solicitud;
    }

    /**
     * @param id_solicitud a establecer en el atributo id_solicitud.
     */
    public void setId_solicitud(long id_solicitud) {
        this.id_solicitud = id_solicitud;
    }

    /**
     * @return retorna el valor del atributo id_plan
     */
    public long getId_plan() {
        return id_plan;
    }

    /**
     * @param id_plan a establecer en el atributo id_plan.
     */
    public void setId_plan(long id_plan) {
        this.id_plan = id_plan;
    }

    /**
     * @return retorna el valor del atributo cliente_es_asegurado
     */
    public long getCliente_es_asegurado() {
        return cliente_es_asegurado;
    }

    /**
     * @param cliente_es_asegurado a establecer en el atributo cliente_es_asegurado.
     */
    public void setCliente_es_asegurado(long cliente_es_asegurado) {
        this.cliente_es_asegurado = cliente_es_asegurado;
    }

    /**
     * @return retorna el valor del atributo id_rama
     */
    public String getId_rama() {
        return id_rama;
    }

    /**
     * @param id_rama a establecer en el atributo id_rama.
     */
    public void setId_rama(String id_rama) {
        this.id_rama = id_rama;
    }

    /**
     * @return retorna el valor del atributo id_producto
     */
    public String getId_producto() {
        return id_producto;
    }

    /**
     * @param id_producto a establecer en el atributo id_producto.
     */
    public void setId_producto(String id_producto) {
        this.id_producto = id_producto;
    }

    /**
     * @return retorna el valor del atributo estado_solicitud
     */
    public String getEstado_solicitud() {
        return estado_solicitud;
    }

    /**
     * @param estado_solicitud a establecer en el atributo estado_solicitud.
     */
    public void setEstado_solicitud(String estado_solicitud) {
        this.estado_solicitud = estado_solicitud;
    }

    /**
     * @return retorna el valor del atributo nro_tarjeta_referencia
     */
    public String getNro_tarjeta_referencia() {
        return nro_tarjeta_referencia;
    }

    /**
     * @param nro_tarjeta_referencia a establecer en el atributo nro_tarjeta_referencia.
     */
    public void setNro_tarjeta_referencia(String nro_tarjeta_referencia) {
        this.nro_tarjeta_referencia = nro_tarjeta_referencia;
    }

    /**
     * @return retorna el valor del atributo fecha_creacion
     */
    public Date getFecha_creacion() {
        return fecha_creacion;
    }

    /**
     * @param fecha_creacion a establecer en el atributo fecha_creacion.
     */
    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    /**
     * @return retorna el valor del atributo fecha_modificacion
     */
    public Date getFecha_modificacion() {
        return fecha_modificacion;
    }

    /**
     * @param fecha_modificacion a establecer en el atributo fecha_modificacion.
     */
    public void setFecha_modificacion(Date fecha_modificacion) {
        this.fecha_modificacion = fecha_modificacion;
    }

    /**
     * @return retorna el valor del atributo id_pais
     */
    public int getId_pais() {
        return id_pais;
    }

    /**
     * @param id_pais a establecer en el atributo id_pais.
     */
    public void setId_pais(int id_pais) {
        this.id_pais = id_pais;
    }

    /**
     * @return retorna el valor del atributo rut_usuario
     */
    public long getRut_usuario() {
        return rut_usuario;
    }

    /**
     * @param rut_usuario a establecer en el atributo rut_usuario.
     */
    public void setRut_usuario(long rut_usuario) {
        this.rut_usuario = rut_usuario;
    }

    /**
     * @return retorna el valor del atributo descripcion_rama
     */
    public String getDescripcion_rama() {
        return descripcion_rama;
    }

    /**
     * @param descripcion_rama a establecer en el atributo descripcion_rama.
     */
    public void setDescripcion_rama(String descripcion_rama) {
        this.descripcion_rama = descripcion_rama;
    }

    /**
     * @return retorna el valor del atributo nombre_plan
     */
    public String getNombre_plan() {
        return nombre_plan;
    }

    /**
     * @param nombre_plan a establecer en el atributo nombre_plan.
     */
    public void setNombre_plan(String nombre_plan) {
        this.nombre_plan = nombre_plan;
    }

    /**
     * 
     * @param tipo_vehiculo 
     */
	public void setTipo_vehiculo(int tipo_vehiculo) {
		this.tipo_vehiculo = tipo_vehiculo;
	}
	
	/**
	 * 
	 * @return el tipo de vehiculo
	 */
	public int getTipo_vehiculo() {
		return tipo_vehiculo;
	}

}
