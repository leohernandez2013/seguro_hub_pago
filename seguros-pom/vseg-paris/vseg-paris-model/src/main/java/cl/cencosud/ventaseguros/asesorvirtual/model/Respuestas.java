package cl.cencosud.ventaseguros.asesorvirtual.model;

import java.io.Serializable;
import java.util.List;

/**
 * Respuestas.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 09/09/2010
 */
//@XStreamAlias("respuestas")
public class Respuestas implements Serializable {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 6394587581730593376L;

    /**
     * Listado de resultados.
     */
    //@XStreamImplicit(itemFieldName = "resultado")
    private List < Resultado > resultados;

    /**
     * @return retorna el valor del atributo resultados
     */
    public List < Resultado > getResultados() {
        return resultados;
    }

    /**
     * @param resultados a establecer en el atributo resultados.
     */
    public void setResultados(List < Resultado > resultados) {
        this.resultados = resultados;
    }

}
