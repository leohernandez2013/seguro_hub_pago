<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%
    String path = request.getContextPath();
    String basePath =
        request.getScheme() + "://" + request.getServerName() + ":"
            + request.getServerPort() + path + "/";
    response.setStatus(301);
    response.setHeader("Location", basePath + "inicio.do");
    response.setHeader("Connection", "close");
%>
