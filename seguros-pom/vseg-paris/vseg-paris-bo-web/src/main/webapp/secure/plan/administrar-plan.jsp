<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>
	<html:base />

	<title>Administrar planes</title>

	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="../../js/jquery-tinet.js"></script>
	<script type="text/javascript" src="../../js/jquery.alerts.js"></script>
	<script type="text/javascript" src="../../js/jquery-ui-1.8.4.custom.min.js"></script>

	<link rel="stylesheet" type="text/css" href="../../css/style_seguros.css" />
	<link rel="stylesheet" type="text/css" href="../../css/jquery.alerts.css" />
	<link rel="stylesheet" type="text/css" href="../../css/smoothness/jquery-ui-1.8.4.custom.css" />

	<style type="text/css">
		.destacar {
			font-weight: bold;
		}
	</style>
	<script type="text/javascript" charset="utf-8">
	
			$(document).ready(function() {
				$('.c_vehiculo').hide();
				$('.c_vida').hide();
				$('.c_plan').hide();
			
				$('select#rama').change( function () {
					$('.c_vehiculo').hide();
					$('.c_vida').hide();
					$('.c_plan').hide();
					$.getJSON("/vseg-paris-adm/buscar-subcategorias.do",{idRama: $(this).val(), ajax: 'true'}, function(j){
						$('select#subcategoria option').remove();
						var options = '';
						options += '<option value=""><bean:message bundle="labels" key="general.seleccione" /></option>';
						for (var i = 0; i < j.length; i++) {
							options += '<option value="' + j[i].id_subcategoria + '">' + j[i].titulo_subcategoria + '</option>';
		                   }
		      			$('select#subcategoria').html(options);
						$('select#id_plan option').remove();
						options = '<option value=""><bean:message bundle="labels" key="general.seleccione" /></option>';
		      			$('select#id_plan').html(options);
		    		})
				});
				
				$("select#subcategoria").change(function() {
					$('.c_vehiculo').hide();
					$('.c_vida').hide();
					$('.c_plan').hide();
					$.getJSON("/vseg-paris-adm/buscar-planes.do",{idSubcategoria: $(this).val(), ajax: 'true'}, function(j){
						$('select#id_plan option').remove();
						var options = '';
						options += '<option value=""><bean:message bundle="labels" key="general.seleccione" /></option>';
						for (var i = 0; i < j.length; i++) {
							options += '<option value="' + j[i].id_plan + '">' + j[i].nombre + '</option>';
		                }
		      			$('select#id_plan').html(options);
		    		})		
				});

				$("select#id_plan").change(function() {
					$("#desc_plan").html($("select#id_plan option:selected").text());
					$(".c_plan").show();

					//Validar la rama.
					var rama = $("select#rama option:selected").val();
					if(rama == 1) {
						//Vehiculo
						$(".c_vehiculo").show();
					} else if(rama == 3) {
						//Vida
						$(".c_vida").show();
					}
					cargarDatosPlan();
				});

				$("#grabar").click(function(){
					$("#grabar").attr('disabled', true);

					$("#formulario").attr("ACTION","<html:rewrite action="/administrar-plan-grabar" module="/secure" />");
					$("#formulario").attr("action","<html:rewrite action="/administrar-plan-grabar" module="/secure" />");
					
					$("#formulario").attr("target", "controlador");
					$("#formulario").submit();
					
				});

			});

			function reset() {
				$("select#tipo_pago").val('');
				$("#fecha_vigencia").val('');

				$("input[name=asegurados_adicionales]:checkbox").attr("checked", false);
				$("input[name=beneficiarios]:checkbox").attr("checked", false);

				$("input[name=auto_nuevo]:checkbox").attr("checked", false);
				$("input[name=factura]:checkbox").attr("checked", false);
			}
			
			function cargarDatosPlan() {
				$("#grabar").attr('disabled', false);
				reset();
				var idPlan = $("select#id_plan option:selected").val();

				if(idPlan == ''){
					$('.c_vehiculo').hide();
					$('.c_vida').hide();
					$('.c_plan').hide();
					return false;
				}
				
				$.ajaxSetup({
					  "error":function() {
						jAlert('Error al obtener datos del plan', "Error");
						$("#grabar").attr('disabled', true);
						return false;
				}});
				
				$.getJSON("/vseg-paris-adm/administrar-plan-ver.do",{id_plan: idPlan, ajax: 'true'}, function(j){

					if(j.error != null){
						jAlert(j.error, "Error");
						$("#grabar").attr('disabled', true);
					} else {
						//*** GENERAL ***//
						if (j.plan['compannia'] != null) {
							$("#desc_plan").append(" (" + j.plan['compannia'] + ")");
						}
	
						//forma de pago.
						$('input:checkbox').each( function() {
							if($(this).attr("name") == "forma_pago") {
					    		$(this).attr("checked", false);
					    		if(j.hFormaPago != null && j.hFormaPago[$(this).val()] != null ){
									if(j.hFormaPago[$(this).val()] == '1') {
							    		$(this).attr("checked", true);
									}
					    		}
							}
						});
	
						//Tipo de pago
						if(j.tipo_pago != null){
							$("select#tipo_pago").val(j.tipo_pago);
						}
	
						//Fecha vigencia.
						if(j.plan['maxDiaIniVig'] != null ) {
							$("#fecha_vigencia").val(j.plan['maxDiaIniVig']);
						}

						//Compannia.
						$("#compannia").val('');
						if(j.plan['codigo_cia_leg'] != null ){
							$("#compannia").val(j.plan['codigo_cia_leg']);
						}
	
						//*** VIDA ***//
						//Asegurados adicionales
						if(j.restricciones['requiereGrupoFamiliar']!= null && j.restricciones['requiereGrupoFamiliar'] == '1') {
							$("input[name=asegurados_adicionales]:checkbox").attr("checked", true);
						}
	
						//Beneficiarios.
						if(j.restricciones['requiereBeneficiarios']!= null && j.restricciones['requiereBeneficiarios'] == 'true'){
							$("input[name=beneficiarios]:checkbox").attr("checked", true);
						}
	
	
						//*** VEHICULO ***//
						//Seleccion Inspeccion
						$("input:radio").each( function () {
							if($(this).attr("name") == "inspeccion") {
								$(this).attr("checked", false);
								if(j.plan['inspeccionVehi'] != null ) {
									if($(this).val() == j.plan['inspeccionVehi']) {
										$(this).attr("checked", true);
									}
								}
							}
						});
	
						//Seleccion Auto nuevo/usado.
						if(j.plan['solicitaAutoNuevo'] != null && j.plan['solicitaAutoNuevo'] == '1') {
							$("input[name=auto_nuevo]:checkbox").attr("checked", true);
						}
	
						//Requiere factura
						if(j.plan['solicitaFactura'] != null && j.plan['solicitaFactura'] == '1') {
							$("input[name=factura]:checkbox").attr("checked", true);
						}
					}
				});
			}
	</script>
</head>

<html:form action="/administrar-plan-inicio" styleId="formulario">
	<body>
		<table cellpadding="0" cellspacing="0" border="0" width="800" class="datos">
			<thead>
				<tr>
					<td colspan="3"><h2>Administrador de planes</h2></td>
				</tr>
				<tr>
					<td colspan="3">
						<table width="100%">
							<tr>
								<td class="destacar">Rama:&nbsp;
									<html:select property="rama" styleId="rama">
										<option value="">
											<bean:message bundle="labels" key="general.seleccione" />
										</option>
										<html:options collection="ramas" property="id_rama" labelProperty="titulo_rama" />
									</html:select>
								</td>
								<td class="destacar">Subcategor&iacute;a:&nbsp;
									<html:select property="subcategoria" styleId="subcategoria">
										<option value="">
											<bean:message bundle="labels" key="general.seleccione" />
										</option>	
										<logic:present name="subcategorias">
											<html:options collection="subcategorias" property="id_subcategoria" labelProperty="titulo_subcategoria" />
										</logic:present>
									</html:select>
								</td>
								<td class="destacar">Plan:&nbsp;
									<html:select property="id_plan" styleId="id_plan">
										<option value="">
											<bean:message bundle="labels" key="general.seleccione" />
										</option>	
										<logic:present name="planes">
											<html:options collection="planes" property="id_plan" labelProperty="nombre" />
										</logic:present>
									</html:select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</thead>
			<tbody>
				<tr class="c_plan">
					<td colspan="3" style="padding-top: 10px; padding-bottom: 10px;">
						<div style="display: inline;" class="destacar">Descripci&oacute;n: </div> &nbsp; <div id="desc_plan" style="display: inline;"></div>
					</td>
				</tr>
				<tr class="c_plan">
					<td colspan="3" class="destacar">General</td>
				</tr>
				<tr class="c_plan">
					<td style="width: 10%;">&nbsp;</td>
					<td style="width: 20%;">Forma de pago:</td>
					<td style="width: 70%;">
						<table width="100%">
							<tr>
								<logic:iterate name="formaPago" id="forma">
									<td><bean:write name="forma" property="descripcion"/>&nbsp;
										<html:multibox property="forma_pago" styleId="forma_pago">
											<bean:write name="forma" property="valor"/>
										</html:multibox>
									</td>
								</logic:iterate>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="c_plan">
					<td>&nbsp;</td>
					<td>Tipo de pago:</td>
					<td>
						<html:select property="tipo_pago" styleId="tipo_pago">
							<option value="" selected="selected"><bean:message bundle="labels" key="general.seleccione" /></option>	
							<option value="1">Recurrente</option>
							<option value="2">Unico</option>
						</html:select>
					</td>
				</tr>
				<tr class="c_plan">
					<td>&nbsp;</td>
					<td>Fecha vigencia:</td>
					<td><html:text property="fecha_vigencia" size="4" styleId="fecha_vigencia"/> d&iacute;as</td>
				</tr>
				<tr class="c_plan">
					<td>&nbsp;</td>
					<td>Compa&ntilde;&iacute;a:</td>
					<td>
						<html:select property="compannia" styleId="compannia">
							<option value="" selected="selected"><bean:message bundle="labels" key="general.seleccione" /></option>
							<html:options collection="compannias" property="valor" labelProperty="nombre" />
						</html:select>
					</td>
				</tr>



				<!-- Solo mostrar en caso de tipo vida -->
				<tr class="c_vida">
					<td colspan="3" class="destacar">Vida</td>
				</tr>
				<tr class="c_vida">
					<td>&nbsp;</td>
					<td>Asegurados adicionales:</td>
					<td><html:checkbox property="asegurados_adicionales" styleId="asegurados_adicionales" value="1"/></td>
				</tr>
				<tr class="c_vida">
					<td>&nbsp;</td>
					<td>Beneficiarios:</td>
					<td><html:checkbox property="beneficiarios" styleId="beneficiarios" value="true"/></td>
				</tr>



				<!-- Solo mostrar en caso de tipo vehiculo -->
				<tr class="c_vehiculo">
					<td colspan="3" class="destacar">Veh&iacute;culos</td>
				</tr>
				<tr class="c_vehiculo">
					<td>&nbsp;</td>
					<td>Selecci&oacute;n inspecci&oacute;n:</td>
					<td>
						<html:radio property="inspeccion" value="1" styleId="inspeccion">Si</html:radio>&nbsp;&nbsp;
						<html:radio property="inspeccion" value="0" styleId="inspeccion" >No</html:radio>
					</td>
				</tr>
				<tr class="c_vehiculo">
					<td>&nbsp;</td>
					<td>Seleccion auto nuevo/usado:</td>
					<td><html:checkbox property="auto_nuevo" styleId="auto_nuevo" value="1"/></td>
				</tr>
				<tr class="c_vehiculo">
					<td>&nbsp;</td>
					<td>Ingreso de factura:</td>
					<td><html:checkbox property="factura" styleId="factura" value="1"/></td>
				</tr>

				<tr class="c_plan">
					<td colspan="3" style="text-align: right;"><input type="button" name="grabar" value="Grabar" id="grabar" /></td>
				</tr>
			</tbody>
		</table>
		<iframe src="/cotizador/cotizacion/blank.jsp" id="controlador" name="controlador" height="0" frameborder="0" width="500"></iframe>
	</body>
</html:form>
</html:html>
