<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>
	<html:base />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	<title>Modificar Evento</title>

	<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="../../js/ajaxfileupload.js"></script>
	<script type="text/javascript" src="../../js/jquery.alerts.js"></script>
	<script type="text/javascript"
		src="../../js/jquery-ui-1.8.4.custom.min.js"></script>
	<script type="text/javascript" src="../../js/jquery.colorbox.js"></script>
	<script type="text/javascript" src="../../js/jquery.form.js"></script>
	<script type="text/javascript" src="../../js/jquery.maxlength.js"></script>

	<link rel="stylesheet" type="text/css" href="../../css/colorbox.css" />


	<link rel="stylesheet" type="text/css"
		href="../../css/jquery.alerts.css" />
	<link rel="stylesheet" type="text/css"
		href="../../css/smoothness/jquery-ui-1.8.4.custom.css" />
	<link rel="stylesheet" type="text/css"
		href="../../css/style_seguros.css" />


	<link type="text/css" rel="stylesheet"
		href="http://jquery-ui.googlecode.com/svn/tags/1.7/themes/redmond/jquery-ui.css" />
	<link href="../../js/jquery.alerts.css" rel="stylesheet"
		type="text/css" media="screen" />
	<link rel="stylesheet" type="text/css"
		href="../../css/style_seguros.css" />

	<script type="text/javascript">
	
	var id_evento = -1;
	var subir_archivo = -1;
	var subir_cargaRut = -1;
	var dataruts = '';
	
	function uploadfile(archivo){
		try {
			if( $("#"+archivo).attr('value') != "") { 
				valor_subir(archivo, 1);
	
				$.ajaxFileUpload( {
					url:'subir-archivo.do?tipo='+archivo+'&action=modificar', 
					secureuri:false, 
					fileElementId: archivo, 
					dataType: 'json', 
					success: function (data, status) {
						updloadFinished(archivo, data);
					},
					error: function (data, status, e) {
						jAlert(data.mensaje);
						updloadFinished(archivo, data); 
					}
				});
			}
		} catch (e) {
			alert(e.message);
		}
	} 
	
	function updloadFinished(archivo, data){
		if(data.estado == 'success') {
			$("#"+archivo+"_div").html('<bean:message key="nueva-ficha-producto.archivoListo" />');
			$("#"+archivo+"_div").show();
		} else if (data.estado  == 'nofile') {
			$("#"+archivo+"_div").html('<bean:message key="nueva-ficha-producto.noSubioArchivo" />');
			$("#"+archivo+"_div").show();		
		} else {
			$("#"+archivo+"_div").html(data.estado + ': ' + data.mensaje);
			$("#"+archivo+"_div").show();
		}

		if(data.ruts != null) {
			dataruts = data.ruts;
		}
		
		valor_subir(archivo, -1);
		check_termino();
	}
	
	function valor_subir(archivo, valor) {
		eval("subir_" + archivo + "=" + valor + ";");
	}
	
	function check_termino() {
		if(subir_archivo < 0 && subir_cargaRut < 0) {
			var msj_imagen = $("#archivo_div").html();
			var msj_cargarut = $("#cargaRut_div").html();
			
			var msj = "";
			if(msj_imagen != '') {
				msj += "<li>imagen: "+msj_imagen+"</li>";
			}
			if(msj_cargarut != "") {
				msj += "<li>carga rut: "+msj_cargarut+"</li>";
			}

			if(dataruts != '') {
				parent.jAlert(dataruts, 'RESULTADO CARGA', function() { 
					cerrarVentana(msj);
				});
			} else {
				cerrarVentana(msj);
			}
		}
		
	}

	function cerrarVentana(msj)�{
		parent.jConfirm("<li>Evento modificado con exito</li> <br>" + msj + " <br>�Cerrar ventana?", "Evento", function(r) {
			if(r) {
				self.close();
			}
		});
	}	
	
	function enviar_documentos() {
		if(id_evento > 0) {
			 uploadfile('archivo');
			 uploadfile('cargaRut');
			 check_termino();
		}
	}
	 
 
   function cargarComboPriorid(sugerencia){
    
    var  htmlCode = "";
     $.getJSON("/vseg-paris-adm/secure/mantenedor-eventos/CargarComboPrioridad.do", {
            ajax : 'true'}, function(j){
                  
             if(j.length>0){
                 
                 htmlCode += '<select name="datos(prioridad)">';
                 htmlCode +=' <option value="">Seleccionar una opcion </option>';
                 
                 for (var i = 0; i < j.length; i++) {
                 
                  htmlCode+='<option ';
                  if(j[i].id_prioridad_evento==sugerencia){
                  htmlCode+='value="'+j[i].id_prioridad_evento+'" selected ="true" >';
                  }else{
                  htmlCode+='value="'+j[i].id_prioridad_evento+'">';
                  }
                  htmlCode+=j[i].prioridad;
                  htmlCode+='</option>';  
                 }
                 htmlCode += '</select>';
                 
                 $("#elementoPrioridad").empty();
                 $("#elementoPrioridad").append(htmlCode);
              }
           });
      
      
    }
  
  $(document).ready(function() {
  
	$('textarea.limited').maxlength({
		'useInput' : true
	});
    
     var id_evento  = $("#id_evento").attr("value");
    
     $("#radioNosegmentado").click(function(){
          $("#cargaRut").attr('disabled','disabled'); 
     });
     
     $("#radioSegmentado").click(function(){
          $("#cargaRut").removeAttr('disabled');
     });
    
     $("#verImagen").colorbox({
    				width:'60%',
    				title:'Imagen',
    				href:"/vseg-paris-adm/secure/mantenedor-eventos/obtener-archivo.do?tipo=imagen&id_evento=" + id_evento + "&x=imagen.jpg"
    			});

  
    $("#guardar").click(function() {
    	//Validar segmentacion.
    	if($("input[id=radioSegmentado]:checked").val() == 1) {
    		if($("#cargaRut").attr("value").length == 0) {
    			parent.jAlert('Debe indicar archivo carga rut','Error');
    			return false;
    		}
    	} else {
    		//NO aplica archivo de rut.
    		$("#cargaRut").attr("value", "");
    	}
		$("#formulario").attr("ACTION","modificarEventoEjecutar.do");
		$("#formulario").attr("action","modificarEventoEjecutar.do");
		$("#formulario").attr("target", "frameOculto");
		$("#formulario").submit();
		return false;
	});
     
    $("#borrar").click(function() {
    	parent.jConfirm('Se eliminar� el evento, �Desea continuar?', 'Borrar Evento', function(r) {
    		if(r) {
				$("#formulario").attr("ACTION","borrarEvento.do");
				$("#formulario").attr("action","borrarEvento.do");
				$("#formulario").attr("target", "frameOculto");
				$("#formulario").submit();
				return false;
    		}
    	});
	});

    $("#cerrar").click(function() {
		parent.jConfirm("Se perder�n todos los cambios no guardados", "Cuidado", function(r) {
			if(r) {
				self.close();
			}
		});
     });

     
     var dates = $("#fecha_inicio, #fecha_termino").datepicker({
			changeMonth: true,
			changeYear: true,
			showOn: "button",
			buttonImage: "../../images/calendar.gif",
			buttonImageOnly: true,
			dateFormat: 'dd/mm/yy',
			dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
			monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
				'Junio', 'Julio', 'Agosto', 'Septiembre',
				'Octubre', 'Noviembre', 'Diciembre'],
			monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
				'May', 'Jun', 'Jul', 'Ago',
				'Sep', 'Oct', 'Nov', 'Dic'],
			onSelect: function( selectedDate ) {
				var option = this.id == "fecha_inicio" ? "minDate" : "maxDate",
				instance = $( this ).data( "datepicker" );
				date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}
		});
		          
  });
  </script>
</head>
<body>
	<table class="datos">
		<tr>
			<td>
				&nbsp;
			</td>
		</tr>
		<tr>
			<td>
				<html:form action="/modificarEvento" styleId="formulario"
					method="post" enctype="multipart/form-data">
					<table cellpadding="0" cellspacing="0" border="0" width="100%"
						id="cont_crear">
						<tr>
							<th width="100">
								<html:hidden property="datos(id_evento)" styleId="id_evento" />
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.codigo" />
								:
							</th>
							<td width="250">
								<html:hidden property="datos(codigo_evento)"
									style="width: 100px" write="true"
									styleId="datos(codigo_evento)" />
							</td>
							<th width="120">
								&nbsp;

							</th>
							<td>
								&nbsp;
							</td>
							<td width="136">
								&nbsp;
							</td>
						</tr>
						<tr>
							<th>
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.nombre_evento" />
								:
							</th>
							<td width="110">
								<html:text property="datos(nombre)" style="width: 100px"
									styleId="nombre_evento" />

							</td>
							<th valign="top">
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.descripcion" />
								:
							</th>
							<td rowspan="3" valign="top">
								<html:textarea property="datos(descripcion)" cols="26" rows="4"
									styleId="datos(descripcion)" styleClass="limited" />
								<input type="hidden" name="maxlength" value="254" />
							</td>
							<td>
								&nbsp;
							</td>
						</tr>
						<tr>

							<th>
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.fecha_inicio" />
								:
							</th>
							<td width="110">
								<html:text property="datos(fecha_inicio)" style="width: 70px"
									styleId="fecha_inicio" />
							</td>
							<th>
								&nbsp;
							</th>
							<td>
								&nbsp;
							</td>
						</tr>
						<tr>
							<th>
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.fecha_termino" />
								:
							</th>
							<td width="110">
								<html:text property="datos(fecha_termino)" style="width: 70px"
									styleId="fecha_termino" />
							</td>

							<th>
								&nbsp;
							</th>
							<td>
								&nbsp;
							</td>
						</tr>
						<tr>
							<th>
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.ocurrencia" />
								:
							</th>
							<td width="110">
								<html:text property="datos(ocurrencia)"
									styleId="datos(ocurrencia)" style="width: 30px" />
							</td>
							<th>
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.Imagen" />
								:
							</th>
							<td colspan="2">

								<span id="rut_arch"> <input type="file" name="archivo"
										id="archivo" /> <input type="button" id="verImagen"
										value="<bean:message bundle="labels" key="mantenedor.eventos.boton.ver_imagen" />" />
								</span>

								<div style="display: none;" id="archivo_div"></div>

							</td>

						</tr>
						<tr>
							<th>
								<bean:message bundle="labels" key="mantenedor.evento.crear.url" />
								:
							</th>
							<td>
								<html:text property="datos(url)" style="width: 220px"
									styleId="datos(url)" />
							</td>
							<th>
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.tipo_pagina" />
								:
							</th>
							<td>
								<input type="radio" name="datos(tipo_pagina)" value="1" />
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.tipo_pagina_tipo_1" />
								&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="radio" checked="checked" name="datos(tipo_pagina)"
									value="0" />
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.tipo_pagina_tipo_2" />
							</td>
							<td>
								&nbsp;
							</td>

						</tr>
						<tr>
							<th>
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.estado" />
								:
							</th>
							<td width="110">

								<logic:equal value="1" name="evento" property="estado">

									<input type="checkbox" checked="checked"
										name="datos(estado_habilitado)" id="habilitado" />
									<bean:message bundle="labels"
										key="mantenedor.evento.crear.estado_habilitado" />

								</logic:equal>

								<logic:notEqual value="1" name="evento" property="estado">

									<input type="checkbox" name="datos(estado_habilitado)"
										id="deshabilitado" />
									<bean:message bundle="labels"
										key="mantenedor.evento.crear.estado_habilitado" />

								</logic:notEqual>

							</td>
							<th>
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.posicion" />
								:
							</th>

							<td>
								<html:select property="datos(posicion)" styleId="posicion">
									<html:option value="">
										<bean:message bundle="labels"
											key="mantenedor.evento.crear.seleccionar_opcion" />
									</html:option>
									<html:optionsCollection name="posiciones"
										value="ID_POSICION_EVENTO" label="posicion_evento" />
								</html:select>
							</td>

							<td>
								&nbsp;
							</td>
						</tr>
						<tr>
							<th>
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.orden" />
								/
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.Prioridad" />
								:
							</th>
							<td width="110">
								<html:select property="datos(prioridad)" styleId="prioridad">
									<option value="">
										<bean:message bundle="labels"
											key="mantenedor.evento.crear.seleccionar_opcion" />
									</option>
									<html:optionsCollection name="prioridad"
										value="ID_PRIORIDAD_EVENTO" label="prioridad" />
								</html:select>

							</td>
							<th>
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.tipo_evento" />
								:
							</th>
							<td>

								<html:select property="datos(tipo_evento)" styleId="tipoEvento">
									<html:option value="">
										<bean:message bundle="labels"
											key="mantenedor.evento.crear.seleccionar_opcion" />
									</html:option>
									<html:optionsCollection name="tipoEvento"
										value="ID_TIPO_EVENTO" label="tipo_evento" />
								</html:select>
							</td>

							<td>
								&nbsp;
							</td>
						</tr>
						<tr>
							<th>
								<bean:message bundle="labels"
									key="mantenedor.evento.crear.Segmentado" />
								:
							</th>
							<td>

								<input type="radio" name="datos(radio_segmentado)" value="1"
									checked="checked" id="radioSegmentado" />
								Si &nbsp;&nbsp;&nbsp;&nbsp;
								<input type="radio" name="datos(radio_segmentado)" value="0"
									id="radioNosegmentado" />
								No

							</td>


							<th>
								<span id="rut"><bean:message bundle="labels"
										key="mantenedor.evento.crear.carga_rut" /> :</span>
							</th>
							<td>
								<span id="rut_arch"> <input type="file" name="cargaRut"
										id="cargaRut" /> </span>

								<div style="display: none;" id="cargaRut_div"></div>
							</td>
							<td>
								&nbsp;
							</td>

						</tr>
						<tr>
							<td colspan="5">

								<input type="button"
									value="<bean:message bundle="labels" key="general.guardar"/>"
									id="guardar" name="guardar" />
								&nbsp;&nbsp;
								<logic:notEqual value="1" name="evento" property="estado">
									<input id="borrar" type="button"
										value="<bean:message bundle="labels" key="general.borrar"/>" />
								&nbsp;&nbsp;
								</logic:notEqual>
								<input id="cerrar" type="button"
									value="<bean:message bundle="labels" key="general.cerrar"/>" />
							</td>
						</tr>
					</table>

				</html:form>

			</td>
		</tr>
	</table>

	<iframe id="frameOculto" frameborder="0" width="500" height="0"
		src="" name="frameOculto">
	</iframe>
</body>

</html:html>