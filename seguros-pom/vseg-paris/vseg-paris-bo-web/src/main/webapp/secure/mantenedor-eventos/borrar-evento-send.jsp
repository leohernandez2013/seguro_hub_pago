<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="org.apache.struts.Globals"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>
	<html:base />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	<title>proxy</title>
	<link rel="stylesheet" type="text/css"
		href="../../css/style_seguros.css" />
	<script type="text/javascript" charset="utf-8">
	<logic:notEmpty name="resultado">
		try {
			parent.opener.jAlert("<bean:message key="gestion-eventos.eventoEliminado"/>","<bean:message key="general.errores"/>");
			parent.close();
		} catch(e) {
			alert(e.message);
		}
	</logic:notEmpty>

	
	var errorValidacion = '<bean:write name="<%=Globals.EXCEPTION_KEY%>" property="message" ignore="true" />';
	if(errorValidacion != "") {
		try {
			parent.jAlert(errorValidacion, "<bean:message key="general.errores"/>");
		} catch(e) {
			alert(e.message);
		}
	}

	</script>
</head>
<body>
</body>
</html:html>
