<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="org.apache.struts.Globals"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>
	<html:base />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	<title>proxy</title>
	<link rel="stylesheet" type="text/css"
		href="../../css/style_seguros.css" />
	<script type="text/javascript" charset="utf-8">

		var errorValidacion = '<bean:write name="<%=Globals.EXCEPTION_KEY%>" property="message" ignore="true" />';
		if(errorValidacion != "") {
			try {
				parent.jAlert(errorValidacion, "<bean:message key="general.errores"/>");
			} catch(e) {
				alert(e.message);
			}
		} else {
				
			   <logic:notEmpty name="resultado">
			    
				    <logic:notEmpty name="id_evento">
				    
						parent.id_evento = '<bean:write name="id_evento" />';
						
						parent.enviar_documentos();
						
						//parent.jConfirm('� Cerrar ventana ?', 'se ha actualizado correctamente el evento', function(r) {
						    //parent.jAlert('Confirmed: ' + r, 'Confirmation Results');
						    //if(r){
						    	//parent.close();
						    //}
						//});
										
					</logic:notEmpty>
			    </logic:notEmpty>
			 
			 <logic:empty name="id_evento">
			
			     var errores = "<html:errors />";
					  if(errores!=null && errores.length > 0){
					     parent.jAlert(errores, "Errores");
					    }				
					
					  <logic:notEmpty name="error_evento">
						  var error_varios = '<bean:write name="error_evento"/>';
						    if(error_varios!=null && error_varios.length > 0){
						     parent.jAlert(error_varios, "Errores");
						    }
			          </logic:notEmpty> 
			 
			   <logic:notEmpty name="error_prioridad">
				        
				        var mensaje = '<bean:write name="error_prioridad"/>';
						    if(mensaje!=null && mensaje.length > 0){
						     var sugerencia = '<bean:write name="sugerencia"/>';
						     parent.cargarComboPriorid(sugerencia);
						     
						     parent.jAlert(mensaje, "Errores");
						     
						    }
				      
				      </logic:notEmpty> 
				
			 </logic:empty>
	 	}
	</script>
</head>
<body>
<html:errors />
	
</body>
</html:html>
