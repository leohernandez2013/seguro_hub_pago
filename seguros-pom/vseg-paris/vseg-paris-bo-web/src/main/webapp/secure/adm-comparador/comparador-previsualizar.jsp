<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Comparador de Planes</title>

	<link href="../../css/comparador.css" rel="stylesheet" type="text/css"/>
	<link href="../../css/nubes.css" rel="stylesheet" type="text/css"/>
	
	<script type="text/javascript">
		var clear="clear.gif"; //path to clear.gif
	</script>
	
	<script type="text/javascript" src="../../js/unitpngfix.js"></script>		
    <script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="../../js/slider.js"></script>
    <script type="text/javascript" src="../../js/jquery.formatCurrency-1.4.0.js"></script>
    <script type="text/javascript" src="../../js/jquery.tools.min.js" ></script>
    
	<script type="text/javascript" charset="utf-8">
		
		var logos = new Array();
		logos['LIBERTY GENERALES'] = 'logo-liberty.gif';
		logos['MAGALLANES S.A'] = 'logo-magallanes.gif';
		logos['CHARTIS'] = 'logo-chartis.gif';
		logos['RSA SEGUROS (CHILE)'] = 'logo-rsa.gif';
		logos['CARDIF'] = 'logo-cardif.gif';
		logos['ZENIT'] = 'logo-zenit.gif';
		logos['INTERAMERICANA'] = 'logo-metlife.gif';
		logos['CONSORCIO GENERALES'] = 'logo-consorcio.gif';
		
		function desplegarCaracteristicas (listPlanes, listCaracteristicas) {
		
			if (listPlanes != null && listPlanes != undefined) {
					
					var html = '';
					var footBtn = '';
					var ancho = '112px';
					var idAutoCliente = 'auto_cliente';
					var styleTitulo = "width:204px !important;";
					var styleSeguro = "width:112px !important;padding:5px;";
					
					$("#barra-seguros").attr("style", "width:660px;height:85px;margin-bottom:5px;background-image:url(../../images/comparador/barra-seguros.gif);background-position:0px;");
					
					if (listPlanes.length == 2) {
						styleTitulo = "width:255px !important;";
						styleSeguro = "width:200px !important;padding:5px;";
						ancho = '195px';
						idAutoCliente = 'auto_cliente2';
						$("#auto_cliente").attr("style", "width:270px;height:85px;float:left;");
						$("#barra-seguros").attr("style", "width:660px;height:85px;margin-bottom:5px;background-image:url(../../images/comparador/barra-seguros-2opciones.gif);background-position:0px;");			
					}
					
					if (listPlanes.length == 3) {
						styleTitulo = "width:204px !important;";
						styleSeguro = "width:150px !important;padding:5px;";
						ancho = '147px';
						$("#auto_cliente").attr("style", "width:208px;height:85px;float:left;");
						$("#barra-seguros").attr("style", "width:660px;height:85px;margin-bottom:5px;background-image:url(../../images/comparador/barra-seguros-3opciones.gif);background-position:0px;");				
					}
					
					footBtn += '<table cellpadding="0" cellspacing="0" border="0" class="grillaGris" width="653">';
					footBtn += '<tr>';
					footBtn += '<th valign="top" style="' + styleTitulo + '">&nbsp;</th>';
					
					for (i=0; i < listPlanes.length; i++) {
					
						var itm = listPlanes[i];
						
						//var pesos = $(itm.primaMensualPesos).formatCurrency({digitGroupSymbol: '.', roundToDecimalPlace: -2, positiveFormat: '%s %n'});
						//var uf = $(itm.primaMensualUF).formatCurrency({digitGroupSymbol: '.', decimalSymbol: ',', roundToDecimalPlace: 4, positiveFormat: '%s %n', symbol: 'UF'})
											
						html += '<div class="compania" style="width:' + ancho + ';height:85px;float:left;">';						
						html += '<a href="javascript:void(0);">';
						html += '<img src="../../images/comparador/btn-cerrar-seguro.gif" width="14" height="14" align="right" border="0" style="margin-top:10px; margin-right:13px;" onmouseover="this.src = \'../../images/comparador/btn-cerrar-seguro-hover.gif\'" onmouseout="this.src = \'../../images/comparador/btn-cerrar-seguro.gif\'" />';
						html += '</a>';
						html += '<img src="/vseg-paris/images/' + logos[itm.nombre_cia_leg] + ' " width="85" height="30" style="margin-top:10px;" />';
						html += '<div class="valores" style="margin:0px; padding:0px;">';
						html += '<h2 style="font-size:13px;"><strong>$ XX.XXX / mes</strong></h2>';
						html += '<p style="margin:0px; padding:0px;"> UF XX.XXX / mes</p>';
						html += '</div>';
						html += '</div>';
						
						footBtn += '<td>';
						footBtn += '<a href="javascript:void(0);">';
						footBtn += '<img src="../../images/comparador/contratar-comparador.gif"  alt="" width="90" height="27" border="0"onmouseover="this.src = \'../../images/comparador/contratar-comparador-hover.gif\'" onmouseout="this.src = \'../../images/comparador/contratar-comparador.gif\'"  />';
						footBtn += '</a><br />';
						footBtn += '<a href="javascript:void(0);">';
						footBtn += '<img src="../../images/comparador/btn-enviar-cotiz.gif" width="90" height="40" border="0" onmouseover="this.src = \'../../images/comparador/btn-enviar-cotiz-hover.gif\'" onmouseout="this.src = \'../../images/comparador/btn-enviar-cotiz.gif\'" style="margin-top:-5px;"  />';
						footBtn += '</a>';
						footBtn += '</td>';
					}
					
					footBtn += '</tr>';
					footBtn += '</table>';
					
					$("#div_compannia").empty();
					$("#contratar").empty();
					
					$("#div_compannia").append(html);
					$("#contratar").append(footBtn);
					
					
					if (listCaracteristicas != null && typeof(listCaracteristicas) != "undefined") {
						
						var listBeneficio = listCaracteristicas[1];
						var listCobPart = listCaracteristicas[2];
						var listCobComp = listCaracteristicas[3];
						var listExclusion = listCaracteristicas[4];
						
						if (listBeneficio != null && typeof(listBeneficio) != "undefined") {
							$("#beneficios").empty();
							$("#beneficios").append(generarHTML(listBeneficio, listPlanes));
						} else {
							$("#titulo_beneficios").remove();
							$("#beneficios").remove();
						}
						
						if (listCobPart != null && typeof(listCobPart) != "undefined") {
							$("#coberturas").empty();
							$("#coberturas").append(generarHTML(listCobPart, listPlanes));
						} else {
							$("#titulo_coberturas").remove();
							$("#coberturas").remove();
						}
						
						if (listExclusion != null && typeof(listExclusion) != "undefined") {
							$("#exclusiones").empty();
							$("#exclusiones").append(generarHTML(listExclusion, listPlanes));
						} else {
							$("#titulo_exclusiones").remove();
							$("#exclusiones").remove();
						}
						
						if (listCobComp != null && typeof(listCobComp) != 'undefined') {
							$("#coberturas_comunes").empty();
							$("#coberturas_comunes").append(generarHTMLComun(listCobComp));						
						} else {
							$("#titulo_coberturas_comunes").remove();
							$("#coberturas_comunes").remove();
						}
					}
			}
		}
		
		function generarHTMLComun(listCaract) {
			var html = '';
			var key = 'caractComp';
			
			if (listCaract.hasOwnProperty(key)) {									
				var listValor = listCaract[key];				
				var cont = 1;
				
				html += '<ul class="the_menu">';
				html += '<li>';
				html += '<table cellpadding="0" cellspacing="0" border="0" width="657" class="acc_container">';
				
				for (i=0; i < listValor.length; i++) {
					if (cont == 1) {
						html += '<tr>';
					}			
					html += '<td width="25%" class="block1">';
					html += '<table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding:none; margin:0px;">';
					html += '<tr>';
					html += '<td width="90%"style="font-size:11px;">' + listValor[i].descripcion + '</td>';
					html += '<td width="10%" valign="top">';
					
					if (listValor[i].tooltip != null && typeof(listValor[i].tooltip) != 'undefined' && listValor[i].tooltip != '') {
						html += '<img src="../../images/comparador/icono-pregunta.gif" width="16" height="16" class="tooltip_show" border="0" align="right" valign="top"/>';
						html += '<div id="nube_pregunta" class="tooltip_contenedor" style="display: none;">';
						html += '<div id="curva_nube_pregunta_top"></div>';
						html += '<div id="nube_pregunta_texto"><p style="margin-left: 20px;">';
						html += listValor[i].tooltip;
						html += '</p></div>';
						html += '<div id="curva_nube_pregunta_botton"></div>';
						html += '</div>';
					} else {
						html += '&nbsp;';
					}
					
					html += '</td>';
					html += '</tr>';
					html += '</table>';
					html += '</td>';
					html += '<td width="25%" class="block">';
					html += listValor[i].valor;
					html += '</td>';
					
					if (cont == 2) {
						html += '</tr>';
						cont = 1;
						continue;
					}
					cont++;
				}
				
				if (cont < 3) {
					for (i=cont; i <=2; i++) {
						if (i == 1) {
							html += '<tr>';
						}
						html += '<td width="25%">&nbsp;</td>';
						html += '<td width="25%">&nbsp;</td>';
						if (i == 2) {
							html += '<tr>';
						}
					}
				}
				html += '</table>';	
				html += '</li>';
				html += '</ul>';
			}
			
			return html;
		}
		
		function generarHTML(listCaract, listPlanes) {
			var styleTitulo = "width:204px !important;";
			var	styleSeguro = "width:112px !important;padding:5px;";
			var	styleDescripcion= "font-variant:small-caps;margin:0px;color:#8E8E8E;font-size:11px;font-weight:bold;";
			var styleMonto= "color:#8E8E8E;";
			var styleToolTip = '';
			var benef = '';
			var titulo = '';
			
			if (listPlanes.length == 2) {
			   styleTitulo = "width:255px !important;";
			   styleSeguro = "width:200px !important;padding:5px;";
			}
			
			if (listPlanes.length == 3) {
				styleTitulo = "width:204px !important;";
				styleSeguro = "width:150px !important;padding:5px;";
			}
			
			titulo += '<table cellpadding="0" cellspacing="2" border="0" class="grillaGris" width="656">';
			
			for (var key in listCaract) {
				if (listCaract.hasOwnProperty(key)) {									
					var listValor = listCaract[key];
					
					if (listValor != null && listValor.length > 0) {
					
						var descripcion = listValor[0].descripcion;
						var tooltipText = listValor[0].tooltip;
						
						titulo += '<tr>';
						titulo += '<th valign="top" style="' + styleTitulo + '">';
						titulo += '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
						titulo += '<tr>';
						titulo += '<td width="90%" style="' + styleDescripcion + '">' + descripcion + '</td>';
						titulo += '<td width="10%" valign="top">';
							if (typeof(tooltipText) != 'undefined' && tooltipText != null && tooltipText != '') {
								titulo += '<img src="../../images/comparador/icono-pregunta.gif" width="16" height="16" class="tooltip_show" border="0" align="right" />';
								titulo += '<div id="nube_pregunta" class="tooltip_contenedor" style="display: none;">';
								titulo += '<div id="curva_nube_pregunta_top"></div>';
								titulo += '<div id="nube_pregunta_texto"><p style="margin-left:15px;font-weight:normal;">';
								titulo += tooltipText;
								titulo += '</p></div>';
								titulo += '<div id="curva_nube_pregunta_botton"></div>';
								titulo += '</div>';
							}
						titulo += '</td>';
						titulo += '</tr>';
						titulo += '</table>';						
						titulo += '</th>';
						
						for (i=0; i < listPlanes.length; i++) {
							var itm = listPlanes[i];
							var benef = '';
							var existe = false;
							
							for (j=0; j < listValor.length; j++) {
								if (itm.id_plan == listValor[j].id_plan) {
									if (listValor[j].valor != null && typeof(listValor[j].valor) != 'undefined') {
										benef += '<td valign="top" style="' + styleSeguro + '"><span style="' + styleMonto + '">' + listValor[j].valor + '</td>';
									} else {
										benef += '<td style="' + styleSeguro + '">&nbsp;</td>';
									}
									existe = true;								
									break;
								}
							}
							
							if (!existe) {
								benef += '<td style="' + styleSeguro + '">&nbsp;</td>';
							}						
							titulo += benef;
						}					
					}
				}
			}
			titulo += '</tr>';
			titulo += '</table>';			
			return titulo;		
		}
	
	</script>

    
<!--** PRUEBA ESTILOS**-->
<style>

body{
	margin:0;
	padding:0;
	background-color:#FFF;
	border:none;
}
 
ul#seguro li{
	 height:35px;
	 border-top:solid 1px #FFF;
	 border-bottom:solid 1px #FFF;
	 border-left:solid 1px #FFF;
	 border-right:solid 1px #FFF;
 }
 ul#titulos li{
	 height:35px;
	 border-top:solid 1px #FFF;
	 border-bottom:solid 1px #FFF;
 }
 ul#titulos li p{
	 font-variant:normal;
	 margin:0px;
	 padding:5px;
	 color:#8E8E8E;
	 padding-left:5px;
	 font-size:11px;
	 font-weight:bold;
 }
  ul#seguro li p{
	 margin:0px;
	 margin-top:2px;
	 margin-bottom:2px;
 	 color:#8E8E8E;
	 padding:5px;
	 padding-left:10px;

 }
 /**--esto es una prueba--**/
 h2.acc_trigger {
 	background: url(../../images/comparador/flecha.gif) no-repeat scroll 0 0;
	padding: 0;
	height: 20px;
	line-height: 25px;
	width: 650px;
	font-size: 11px;
	font-weight: normal;
	float: left;
	margin-bottom:6px;
	padding-left:5px;
}
h2.acc_trigger a {
	text-decoration: none;
	width:653px;
	font-size:11px;
	color:#3BBEEB;
	padding-left:5px;
	margin-bottom:10px;
}
h2.acc_trigger a:hover {
 	background: url(../../images/comparador/flecha.gif) no-repeat scroll 0 0;
	text-decoration:underline
}
h2.active {
	background-position: left bottom;
}
h2.active:hover{
	 	background: url(../../images/comparador/flecha.gif) no-repeat scroll 0 0;
		background-position: left bottom;
}

.acc_container {
	margin: 0 0 5px;
	margin-top:5px;
	padding: 0;
	overflow: hidden;
	font-size: 1.2em;
	width: 656px;
	clear: both;
	background: #FFF;
	border: 1px solid #CCC;
}
.acc_container .block {
	width:100px;
	float:left;
	font-size:11px;
	color:#8F8F8F;
	line-height:180%;
}
.acc_container .block1 {
	width:195px;
	float:left;
	margin-left:12px;
	margin-right:5px;
	font-size:11px;
	color:#8F8F8F;
	line-height:180%;
}

#coberturas-comunes{
	width:657px;
	height:auto;
}

#auto_cliente{
	width:208px;
	height:85px;
	float:left;
}
#auto_cliente2{
	width:222px;
	height:85px;
	float:left;
}
#beneficios{
	width:657px; 
	height:auto;
	margin-top:5px; 
	margin-bottom:0px;
	border-top:solid 2px #FFF;
	border-bottom:solid 2px #FFF;
}

</style>
<!--** /PRUEBA ESTILOS**-->



<!--** ESTILO TINET **-->
<style type="text/css">
.menu_class {
	border:0;
}

.the_menu {
 display:none;
 padding-left: 0px;
 list-style:none;
}



.the_menu li {
	background-color:fff;
	text-decoration:none; 
	display:block;
}

.linkCobertura a { color:#3BBEEB; font-size:11px; text-decoration:none;}
.linkCobertura a:hover { color:#3BBEEB; font-size:11px; text-decoration:underline;}

.grillaGris { font-size:11px; color:#8E8E8E;}
.grillaGris th { background-color:#F5F5F5; text-align:left;  font-size:11px; padding:5px; }
.grillaGris td { background-color:#F5F5F5; font-size:12px; padding: 1px;}

</style>

</head>

<body bgcolor="#FFFFFF">
<table width="600" height="auto" align="center">
<tr>
<td valign="top">

<div id="despliegue_ficha_salud" style="background-color:#FFF; background-image:none;">
<div id="ficha_salud">
			<div id="ficha_titulo_salud" style="width:650px; height:5px;">
				<h1 style="margin-bottom:-20px; margin-left:0px;">Comparador de <span>seguros</span></h1>
                <p style="margin-top:0px; float:right">Las mejores ofertas para ti: 
                	<span style="color:#3BBEEB">
                		<strong>
							<logic:present name="datosCotizacion" scope="session">
								<logic:notEmpty name="datosCotizacion" property="nombre"> <bean:write name="datosCotizacion" property="nombre"/> </logic:notEmpty>
								<logic:notEmpty name="datosCotizacion" property="apellidoPaterno"> <bean:write name="datosCotizacion" property="apellidoPaterno"/> <logic:notEmpty name="datosCotizacion" property="apellidoMaterno"> <bean:write name="datosCotizacion" property="apellidoMaterno"/> </logic:notEmpty> </logic:notEmpty>
							</logic:present>
							<logic:notPresent name="datosCotizacion" scope="session">
							Nombre Apellido Apellido
							</logic:notPresent>
                		</strong>
                	</span>
                </p>
			</div>
            <!-- superior -->
            <div style="width:650px; height:40px; margin-bottom:15px; ">
           	  <div class="izq" style="width:430px; height:20px; float:left; margin-top:12px; padding-left:10px; padding-top:8px; background-color: #F5F5F5">
                	<h2 style="color: #3BBEEB; margin-top:-5px;">
                		<strong>
                			<logic:present name="descSubcategoria"> <bean:write name="descSubcategoria"/> &nbsp; <bean:write name="descProducto"/> </logic:present>
                			<logic:present name="txtSubcategoria"> <bean:write name="txtSubcategoria"/> &nbsp; <bean:write name="txtProducto"/> </logic:present>
                		 </strong></h2>
              </div>                      
                <div class="der" style="width:204px; float:right; margin-top:10px;"> <a href="javascript:void(0);"><img src="../../images/comparador/btn-enviar-comp.gif" width="203" height="33" align="right" border="0" onmouseover="this.src = '../../images/comparador/btn-enviar-comp-hover.gif'" onmouseout="this.src = '../../images/comparador/btn-enviar-comp.gif'" /></a>
                </div>
            </div>
			<!-- /superior -->
            <!-- compa�ias -->
          <div id="barra-seguros">
            <div id="auto_cliente">
            	<logic:present name="idRama" scope="session">
            		<logic:equal value="1" name="idRama">
            			<p style="padding:5px;">
	            			<bean:write name="tipoDesc" scope="request" /> :
	            			<br />
	            			<strong><bean:write name="marcaDesc" scope="request"/> <bean:write name="modeloDes" scope="request"/></strong>
							<br />
							A&ntilde;o: <bean:write name="datosCotizacion" property="anyoVehiculo"/>
						</p>
            		</logic:equal>
            		<logic:equal value="2" name="idRama">
            			<p style="padding:5px;">
						<logic:notEmpty name="datosCotizacion" property="direccion"><span><bean:message bundle="labels-cotizador" key="labels.usuario.direccion"/>:</span> <bean:write name="datosCotizacion" property="direccion"/> <bean:write name="datosCotizacion" property="numeroDireccion"/> <logic:notEmpty name="datosCotizacion" property="numeroDepto"><bean:write name="datosCotizacion" property="numeroDepto"/> <br /></logic:notEmpty></logic:notEmpty>	
						<logic:notEmpty name="comunaDescripcion"> <bean:write name="comunaDescripcion"/> <logic:notEmpty name="ciudadDescripcion"> ,&nbsp; <bean:write name="ciudadDescripcion"/><br /></logic:notEmpty></logic:notEmpty> 
						<logic:notEmpty name="datosCotizacion" property="antiguedadVivienda"> <span><bean:message bundle="labels-cotizador" key="labels.usuario.antiguedadVivienda"/>:</span> <bean:write name="datosCotizacion" property="antiguedadVivienda"/></logic:notEmpty>
						</p>
            		</logic:equal>
            		<logic:notEqual value="1" name="idRama">
            			<logic:notEqual value="2" name="idRama">
            				<logic:present name="datosCotizacion" scope="session">
            					<p style="padding:5px;">
            					<logic:notEmpty name="datosCotizacion" property="nombre"> <bean:write name="datosCotizacion" property="nombre"/> 
            					<logic:notEmpty name="datosCotizacion" property="apellidoPaterno"> <bean:write name="datosCotizacion" property="apellidoPaterno"/> <logic:notEmpty name="datosCotizacion" property="apellidoMaterno"> <bean:write name="datosCotizacion" property="apellidoMaterno"/> <br /> </logic:notEmpty> </logic:notEmpty> </logic:notEmpty>
            					<logic:notEqual name="datosCotizacion" property="rutCliente" value="0"> <bean:write name="datosCotizacion" property="rutCliente"/>-<bean:write name="datosCotizacion" property="dv"/><br /></logic:notEqual>
            					<logic:notEmpty name="datosCotizacion" property="fechaNacimiento"> <bean:write name="datosCotizacion" property="fechaNacimiento" format="dd/MM/yyyy"/> <br /> </logic:notEmpty>
            					<logic:notEmpty name="datosCotizacion" property="sexo"> <logic:equal value="M" name="datosCotizacion" property="sexo">Masculino</logic:equal> <logic:equal value="F" name="datosCotizacion" property="sexo">Femenino</logic:equal><br /></logic:notEmpty>
								<logic:notEmpty name="datosCotizacion" property="estadoCivil"> <bean:write name="datosCotizacion" property="estadoCivilDesc"/> </logic:notEmpty>  
								</p>          					
            				</logic:present>
            			</logic:notEqual>
            		</logic:notEqual>            	
            	</logic:present>
            </div>
            <div id="div_compannia" >
            
            </div>
          </div>
            
                        
            <!-- beneficios titulo -->
			<div id="titulo_beneficios" style="width:657px; margin-top:0px; margin-bottom:0px;">
 				<p style="padding:5px; margin:0px; padding-bottom:0px; padding-top:0px; color:#3BBEEB"><strong>Beneficios y condiciones :</strong></p>
            </div>
            <!-- /beneficios titulo -->
            
            <!-- beneficios -->
          <div id="beneficios" style="width:657px; height:auto; margin-top:5px; margin-bottom:0px; border-top:solid 2px #FFF; border-bottom:solid 2px #FFF;">
                   
          </div>
          
          
          
            <!-- /beneficios -->
            <!-- coberturas titulo -->
			<div id="titulo_coberturas" style="width:657px; margin-top:5px; margin-bottom:5px;">
 				<p style="padding:5px; margin:0px; padding-bottom:0px; padding-top:0px; color:#3BBEEB"><strong>Coberturas por plan :</strong></p>
            </div>
            <!-- /coberturas titulo -->
            
            <!-- Coberturas -->
           <div id="coberturas" style="width:657px; height:auto; margin-top:5px; margin-bottom:0px; border-top:solid 2px #FFF; border-bottom:solid 2px #FFF;">
           
			</div>
            <!--  /Coberturas -->
            
            <!-- Exclusiones titulo -->
			<div id="titulo_exclusiones" style="width:657px; margin-top:5px; margin-bottom:5px;">
 				<p style="padding:5px; margin:0px; padding-bottom:0px; padding-top:0px; color:#3BBEEB"><strong>Exclusiones :</strong></p>
            </div>
            <!-- /Exclusiones titulo -->
            
            <!-- Exclusiones -->
            <div id="exclusiones" style="width:657px; height:auto; margin-top:5px; margin-bottom:0px; border-top:solid 2px #FFF; border-bottom:solid 2px #FFF;">
           
			</div>
            <!--  /Exclusiones -->
            
            <!-- botones contratar -->
			<div id="contratar" style="width:657px; height:70px; margin-top:-2px; margin-bottom:5px; background-color:#F5F5F5; float:left;border-left:solid 2px #FFF;">
            	
                
			</div>
            <!-- /contratar -->
     
         
            
            <!-- coberturas comunes -->
			<div id="titulo_coberturas_comunes" style="width:657px; margin-top:5px; margin-bottom:5px;">
 				<p style="padding:5px; margin:0px; padding-bottom:0px; padding-top:0px; color:#3BBEEB"><strong>Coberturas comunes para todos :</strong><span class="linkCobertura" style="margin-left:50px;"><a href="javascript:void(0);" class="menu_class">Ver coberturas</a></span></p>
            </div>
            <!-- /coberturas comunes -->
            <div id="coberturas_comunes">
            
            </div>
         </div>       
  </div> 
    <div style="width:726px; height:40px; display:block; float:left;">&nbsp;</div>
</td>
</tr>
</table>

</body>
<script type="text/javascript" charset="utf-8">

	$(document).ready(function() {
			
			$.getJSON('<html:rewrite action="/comparador-previsualizar-planes"  module="/secure/adm-comparador" />',{ajax: 'true'}, function(j){
						
				var listPlanes = j.result.listPlanes;
				var listCaracteristicas = j.result.listCaracteristicas;
				
				desplegarCaracteristicas(listPlanes, listCaracteristicas);
				
				// Tooltip.
				if(navigator.appName == "Microsoft Internet Explorer") {
					$(".tooltip_show").tooltip({position: "top right", offset: [30, 3]});
				}
				else{
					$(".tooltip_show").tooltip({position: "top right", offset: [20,3]});
				}
			});			
		});
</script>
</html:html>
