<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
  <head>
    <html:base />
    <meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	
    <title>Administrador de Contenido Comparador de Planes</title>
	<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="../../js/jquery-tinet.js"></script>
	<script type="text/javascript" src="../../js/jquery.alerts.js"></script>
	
	<link rel="stylesheet" type="text/css" href="../../css/style_seguros.css" />
	<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
	<link href="../../js/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />
	
	<script type="text/javascript" src="js/jquery/jquery.colorbox.js"></script>
	
	<script> 
		function modificar(id_producto, id_subcategoria, id_rama){
			var url = "<html:rewrite action="/modificar-producto" module="/secure/adm-comparador" />";
			$("#idProducto").attr("value", id_producto);
			$("#idSubcategoria").attr("value", id_subcategoria);
			$("#idRama").attr("value", id_rama);
			$("#formulario").attr("action", url);
			$("#formulario").submit();
			return false;
		}
	</script>	
	
	<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {

			$("select#rama").change(function() {
				$('#listProductos').hide();
				$('#table_btn').hide();
				$.getJSON("/vseg-paris-adm/buscar-subcategorias.do",{idRama: $(this).val(), ajax: 'true'}, function(j){
					$('select#subcategoria option').remove();
					$('select#producto option').remove();
					var options = '';
					options += '<option value="-1"><bean:message bundle="labels" key="general.seleccione" /></option>';
					
					$('select#producto').html(options);
					
					for (var i = 0; i < j.length; i++) {
						options += '<option value="' + j[i].id_subcategoria + '">' + j[i].titulo_subcategoria + '</option>';
                    }
	      			$('select#subcategoria').html(options);
	    		})
  			})
  			
  			$("select#subcategoria").change(function() {
  				$('#listProductos').hide();
  				$('#table_btn').hide();
				$.getJSON("/vseg-paris-adm/buscar-productos.do",{idSubcategoria: $(this).val(), ajax: 'true'}, function(j){
					$('select#producto option').remove();
					var options = '';
					options += '<option value="-1"><bean:message bundle="labels" key="general.seleccione" /></option>';
					for (var i = 0; i < j.length; i++) {
						options += '<option value="' + j[i].id_producto + '">' + j[i].nombre + '</option>';
                    }
	      			$('select#producto').html(options);
	    		})
  			})
  			
  			$("#guardar").click(function() {
  				var box = $("input[type='checkbox']");
  				var arrProducto = '';

  				for (i=0; i < box.length;i++) {
  					arrProducto += box[i].value + '_' + box[i].checked + ';';
  				}
  				
  				$.getJSON("<html:rewrite action="/activar-producto" module="/secure/adm-comparador" />", {arrProductos: arrProducto, ajax : 'true'}, function(j){
  					jAlert(j.mensaje, "");
  				});  				
  				
  			});
  			
  			$("#buscar").click(function() {
  				var idRama = $("select#rama").attr("value");
  				var idSubcategoria = $("select#subcategoria").attr("value");
  				var idProducto = $("select#producto").attr("value");
  				var idTipo = $("input[name='tipo']:checked").val();
  				
  				if (idRama == -1) {
  					jAlert("Debe seleccionar una opci�n para el filtro Rama.", "Errores");
  					return false;
  				}
  				
  				if (idSubcategoria == -1) {
  					jAlert("Debe seleccionar una opci�n para el filtro Subcategoria.", "Errores");
  					return false;
  				}
  				
  				$.getJSON("<html:rewrite action="/buscar_producto" module="/secure/adm-comparador" />", {idTipo: idTipo, idRama: idRama, idSubcategoria: idSubcategoria, idProducto: idProducto, ajax : 'true'}, function(j){
  					$('#listProductos tbody').remove();
  					var html = '<tbody>';
					for (var i = 0; i < j.length; i++) {
						html += '<tr>'
						html += '<td align="center">' + j[i].titulo_rama + '</td>';
						html += '<td align="center">' + j[i].titulo_subcategoria + '</td>';
						html += '<td align="center">' + j[i].nombre + '</td>';
						html += '<td align="center"><input type="checkbox" name="option1" ';
						
						if (j[i].comparador_activo == 1) {
							html += 'checked="true" ';
						} 
						
						html += 'value="' + j[i].id_producto + '"></td>';						
						html += '<td align="center"><img src="../../images/modif.gif" onClick="modificar(' + j[i].id_producto + ', ' + j[i].id_subcategoria + ', ' + j[i].id_rama + ');" style="cursor:pointer"/></td>';
						html += '</tr>';
					}
					if (j.length > 0) {
						$('#table_btn').show();
					} else {
						html += '<tr>'
						html += '<td align="left" colspan="5">No existe informaci�n para los filtros ingresados.</td>'
						html += '</tr>'
					}
					html += '</tbody>';
					$('#listProductos').append(html);
					colores('listProductos');
					$('#listProductos').show();

  				})
  			})
  			
  			
  			
  		});
	</script>

 </head>
  
  <body>
    <html:form action="/buscar-producto-comparador.do" styleId="formulario">
    	<html:hidden property="idProducto" value="" styleId="idProducto" />
    	<html:hidden property="idSubcategoria" value="" styleId="idSubcategoria" />
    	<html:hidden property="idRama" value="" styleId="idRama" />
		<table cellpadding="0" cellspacing="0" border="0" width="800" align="center" class="datos">
			<tr>
				<td>
					<table cellpadding="0" cellspacing="0" border="0" width="98%" align="center">
						<tr>
							<td colspan="6">
								<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
									<tr>
										<td width="150">
											<b>
												<bean:message bundle="labels" key="comparador.adm-producto.titulo" />
											</b>
										</td>					
										<td bgcolor="#dbe1dd">
												
													<bean:message bundle="labels" key="comparador.adm-producto.tipoProducto.titulo" />
												
													<html:radio property="tipo" value="1" >
														<bean:message bundle="labels" key="comparador.adm-producto.tipoProducto.activo" />
													</html:radio>&nbsp;&nbsp;&nbsp;
													<html:radio property="tipo" value="0" >
														<bean:message bundle="labels" key="comparador.adm-producto.tipoProducto.inactivo" />
													</html:radio>
										</td>
										<td>&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td width="15">&nbsp;</td>
						</tr>
						<tr>
							<th width="80">
								<bean:message bundle="labels" key="comparador.adm-producto.filtro.rama" />
								:
							</th>
							<td width="100">
								<html:select property="rama" styleId="rama">
									<option value="-1">
										<bean:message bundle="labels" key="general.seleccione" />
									</option>
									<html:options collection="listRamas" property="id_rama" labelProperty="titulo_rama" />
								</html:select>
							</td>
							<th width="80">
								<bean:message bundle="labels" key="comparador.adm-producto.filtro.subcategoria" />
								:
							</th>
							<td width="220">
								<html:select property="subcategoria" styleId="subcategoria">
									<option value="-1">
										<bean:message bundle="labels" key="general.seleccione" />
									</option>
									<html:options collection="listSubcategorias" property="id_subcategoria" labelProperty="titulo_subcategoria"/>
								</html:select>
							</td>
							<th width="80">
								<bean:message bundle="labels" key="comparador.adm-producto.filtro.producto" />
								:
							</th>
							<td width="150">
								<html:select property="producto" styleId="producto">
									<option value="-1">
										<bean:message bundle="labels" key="general.seleccione" />
									</option>
								</html:select>
							</td>
						</tr>
						<tr>
							<td colspan="6">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td colspan="6" align="center">
								<a href="javascript:void(0);" id="buscar" style="vertical-align: text-bottom;"><img src="../../images/btn_buscar.png" border="0" width="120px" height="24px"></img></a> 
							</td>
						</tr>
						<tr>
							<td colspan="6">
								&nbsp;
							</td>
						</tr>
					</table>
					<table cellpadding="0" cellspacing="0" border="0" width="95%"
						align="center" id="listProductos" style="display: none">
						<thead>
							<tr bgcolor="#CCCCCC">
								<th width="21%">
									<div align="center">
										<bean:message bundle="labels" key="comparador.adm-producto.tabla.head.rama" />
									</div>
								</th>
								<th width="21%">
									<div align="center">
										<bean:message bundle="labels" key="comparador.adm-producto.tabla.head.subcategoria" />
									</div>
								</th>
								<th width="28%">
									<div align="center">
										<bean:message bundle="labels" key="comparador.adm-producto.tabla.head.producto" />
									</div>
								</th>
								<th width="20%">
									<div align="center">
										<bean:message bundle="labels" key="comparador.adm-producto.tabla.head.comparador" />
									</div>
								</th>
								<th width="15%">
									<div align="center">
										<bean:message bundle="labels" key="comparador.adm-producto.tabla.head.modificar" />
									</div>
								</th>
							</tr>
						</thead>
					</table>
					<table cellpadding="0" cellspacing="0" border="0" width="95%"
						align="center" id="table_btn" style="display: none">
						<tr>
							<td>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td align="center"><a href="javascript:void(0);" id="guardar" style="vertical-align: text-bottom;"><img src="../../images/btn_guardar.png" border="0" width="120px" height="24px"></img></a></td>
						</tr>
					</table>									
				</td>
			</tr>
		</table>
	</html:form>
  </body>
</html:html>
