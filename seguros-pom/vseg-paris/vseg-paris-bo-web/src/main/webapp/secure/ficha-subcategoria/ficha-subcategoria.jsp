<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>
	<html:base />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	<title></title>
	<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="../../js/jquery-tinet.js"></script>
	<link rel="stylesheet" type="text/css"
		href="../../css/style_seguros.css" />
		
		
		
		<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
		
		<script type="text/javascript" src="js/jquery/jquery.colorbox.js"></script>
	<script> 
		function pop_modificar(id_ficha){
			var url = "<html:rewrite action="/modificar-ficha-producto" module="/secure" />";
			$("#id_ficha").attr("value", id_ficha);
			$("#formulario").attr("action", url);
			parametros="width=820,height=650,top=150,left=350,scrollbars=yes,location=no,directories=no,status=yes,menubar=no,toolbar=no,resizable=no";
			window.open("","_detalle",parametros);
			$("#formulario").attr("target", "_detalle");
			$("#formulario").submit();
			return false;
		}
	</script>
	<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {

			$("select#rama").change(function() {
				$.getJSON("/vseg-paris-adm/buscar-subcategorias.do",{idRama: $(this).val(), ajax: 'true'}, function(j){
					$('select#subcategoria option').remove();
					var options = '';
					options += '<option value="0"><bean:message bundle="labels" key="general.seleccione" /></option>';
					for (var i = 0; i < j.length; i++) {
						options += '<option value="' + j[i].id_subcategoria + '">' + j[i].titulo_subcategoria + '</option>';
                    }
	      			$('select#subcategoria').html(options);
	    		})
  			})
  			
  			$("#buscar").click(function() {
  				var idRama = $("select#rama").attr("value");
  				var idSubcategoria = $("select#subcategoria").attr("value");
  				$.getJSON("<html:rewrite action="/buscar-fichas-subcategoria" module="/secure" />", {idRama: idRama, idSubcategoria: idSubcategoria, ajax : 'true'}, function(j){
  					$('#listadoFichaProducto tbody').remove();
  					var html = '<tbody>';
					for (var i = 0; i < j.length; i++) {
						html += '<tr>'
						<!--Tipo Ficha -->
						if(j[i].es_subcategoria == 1) {
							html += '<td align="center"><bean:message bundle="labels" key="ficha-subcategoria.subcategoria" /></td>';
						} else {
							html += '<td align="center"><bean:message bundle="labels" key="ficha-subcategoria.fichaPromocion" /></td>';
						}
						html += '<td align="center">' + j[i].titulo_rama + '</td>';
						html += '<td align="center">' + j[i].titulo_subcategoria + '</td>';
						if(j[i].es_subcategoria == 1) {
							html += '<td align="center">&nbsp;</td>';
						} else {
							html += '<td align="center">' + j[i].nombre + '</td>';
						}
						html += '<td align="center"><img src="../../images/modif.gif" onClick="pop_modificar(' + j[i].id_ficha + ');" style="cursor:pointer"/></td>';
						html += '</tr>';
					}
					html += '</tbody>';
					$('#listadoFichaProducto').append(html);
					colores('listadoFichaProducto');
					$('#listadoFichaProducto').show();
  				})
  			})
  			
  			$("#nueva").click(function() {
  				var url = "<html:rewrite action="/nueva-ficha-producto" module="/secure" />";
  				$("#formulario").attr("action", url);
				parametros="width=820,height=650,top=150,left=350,scrollbars=yes,location=no,directories=no,status=yes,menubar=no,toolbar=no,resizable=no";
				window.open("","_detalle",parametros);
				$("#formulario").attr("target", "_detalle");
				$("#formulario").submit();
				return false;
  			})
  			
  		});
	</script>
	
	<script>
		$(document).ready(function(){
			//Examples of how to assign the ColorBox event to elements
			
			$(".iframe").colorbox({iframe:true, width:"50%", height: 450, rel:'group1', current:"P�gina {current} de {total}"});
			
						
			
		});
	</script>
</head>

<body>
	<html:form action="/ficha-subcategoria.do" styleId="formulario">
		<html:hidden property="id_ficha" value="" styleId="id_ficha" />
		<table cellpadding="0" cellspacing="0" border="0" width="800"
			align="center" class="datos">
			<tr>
				<td>
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr>
							<td colspan="4">
								<h2>
									<bean:message bundle="labels"
										key="ficha-subcategoria.fichaSubcategoria" />
								</h2>
							</td>
						</tr>
						<tr>
							<th width="200">
								<bean:message bundle="labels" key="ficha-subcategoria.rama" />
								:
							</th>
							<td width="200">
								<html:select property="rama" styleId="rama">
									<option value="">
										<bean:message bundle="labels" key="general.seleccione" />
									</option>
									<html:options collection="ramas" property="id_rama"
										labelProperty="titulo_rama" />
								</html:select>
							</td>
							<th width="80">
								<bean:message bundle="labels"
									key="ficha-subcategoria.subcategoria" />
								:
							</th>
							<td>
								<html:select property="subcategoria" styleId="subcategoria">
									<option value="0">
										<bean:message bundle="labels" key="general.seleccione" />
									</option>
								</html:select>
							</td>
						</tr>
						<tr>
							<td colspan="4">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td colspan="4" align="center">
								<input type="button"
									value="<bean:message bundle="labels" key="general.buscar"/>"
									id="buscar" name="buscar" />
							</td>
						</tr>
					</table>
					<table cellpadding="0" cellspacing="0" border="0" width="95%"
						align="center" id="listadoFichaProducto" style="display: none">
						<thead>
							<tr bgcolor="#CCCCCC">
								<th width="21%">
									<div align="center">
										<bean:message bundle="labels"
											key="ficha-subcategoria.tipoFicha" />
									</div>
								</th>
								<th width="21%">
									<div align="center">
										<bean:message bundle="labels" key="ficha-subcategoria.rama" />
									</div>
								</th>
								<th width="28%">
									<div align="center">
										<bean:message bundle="labels"
											key="ficha-subcategoria.subcategoria" />
									</div>
								</th>
								<th width="20%">
									<div align="center">
										<bean:message bundle="labels"
											key="ficha-subcategoria.producto" />
									</div>
								</th>
								<th width="15%">
									<div align="center">
										<bean:message bundle="labels" key="general.modificar" />
									</div>
								</th>
							</tr>
						</thead>

					</table>

					<table cellpadding="0" cellspacing="0" border="0" width="100%"
						align="center">
						<tr>

							<td colspan="6" align="right">
								<a id="nueva" name="nueva" style="cursor: pointer" href="#"><bean:message
										bundle="labels" key="ficha-subcategoria.nuevaFichaProducto" />
								</a>
							</td>

						</tr>

					</table>
				</td>
			</tr>
		</table>
	</html:form>
</body>
</html:html>
