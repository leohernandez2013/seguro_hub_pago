<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.servlet.AutenticacionServlet"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html:html lang="true">
<head>
	<html:base />

	<title>intento-fallido.jsp</title>

	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link href="<%=request.getContextPath()%>/css/formulario-r.css"
		rel="stylesheet" type="text/css" />

</head>

<body>
	<!--BOX PROCESO -->
	<div id="estamos_procesando">
		<div id="curba_top_proceso">
			<a href="<%=request.getContextPath()%>/inicio.do" target="_parent"><img
					src="<%=request.getContextPath()%>/images/img_light_box/btn-cerrar.gif"
					onmouseover="this.src = '<%=request.getContextPath()%>/images/img_light_box/btn-cerrar-hover.gif'"
					border="0"
					onmouseout="this.src = '<%=request.getContextPath()%>/images/img_light_box/btn-cerrar.gif'"
					border="0" alt="" width="39" height="39" border="0" /> </a>
		</div>
		<div class="pantalla-error_cuerpo">
			<div id="conte_estamos_procesando">

				<div id="texto_procesando">
					<h3>
						<bean:write
							name="<%=AutenticacionServlet.ERROR_AUTENTICACION_KEY%>"
							ignore="true" />
						<br />
					</h3>
				</div>
			</div>
		</div>
	</div>
	<!--BOX PROCESO FIN-->

</body>
</html:html>
