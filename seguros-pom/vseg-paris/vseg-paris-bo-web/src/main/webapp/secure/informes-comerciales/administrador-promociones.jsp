<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>

<%@page import="cl.tinet.common.seguridad.servlet.AutenticacionServlet"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>
	<html:base />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	<title></title>
	
	<link rel="stylesheet" type="text/css" href="../../css/style_seguros.css" />
	   <link type="text/css" rel="stylesheet" href="../../css/jquery-ui.css" />	
	   <link href="../../css/colorbox.css" rel="stylesheet" type="text/css" />   
		<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
    <script type="text/javascript"
		src="../../js/jquery-ui-1.8.4.custom.min.js"></script>
	
	<script type="text/javascript" src="../../js/jquery.colorbox.js"></script>
	
	
<script type="text/javascript">
 $(document).ready(function(){
$("#inputFechaDesde, #inputFechaHasta").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesde, #inputFechaHasta").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
$("#inputFechaDesde2, #inputFechaHasta2").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesde2, #inputFechaHasta2").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
$("#inputFechaDesde3, #inputFechaHasta3").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesde3, #inputFechaHasta3").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
$("#inputFechaDesde4, #inputFechaHasta4").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesde4, #inputFechaHasta4").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
	 
 $("#inputFechaDesdeVB1, #inputFechaHastaVB1").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeVB1, #inputFechaHastaVB1").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});

 $("#inputFechaDesdeVB2, #inputFechaHastaVB2").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeVB2, #inputFechaHastaVB2").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
 $("#inputFechaDesdeHB1, #inputFechaHastaHB1").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeHB1, #inputFechaHastaHB1").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
		
			
 $("#inputFechaDesdeHB2, #inputFechaHastaHB2").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeHB2, #inputFechaHastaHB2").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
 $("#inputFechaDesdeVSB1, #inputFechaHastaVSB1").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeVSB1, #inputFechaHastaVSB1").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			
 $("#inputFechaDesdeVSB2, #inputFechaHastaVSB2").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesdeVSB2, #inputFechaHastaVSB2").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
 })
 
</script>	
	
<script type="text/javascript">
		$(document).ready(function(){
			$('div#secundario').hide();
			$('div#principal').hide();
			$('div#principalPosicion1').hide();
			$('div#principalPosicion2').hide();
			$('div#principalPosicion3').hide();
			$('div#principalPosicion4').hide();
			
		});	
		function cargarPagina(){
			var subcat = $('select#subcategoriaPrincipal').val();
			
			if (subcat == 1){
				$('div#secundario').hide();
				$('div#principal').show();
			}else if (subcat == 2){
				$('div#secundario').show();
				$('div#principal').hide();
			}else if (subcat == 0){
				$('div#secundario').hide();
				$('div#principal').hide();
			};
		};
		
		function cargarPosicion(){
			var posic = $('select#subcategoriaPosicion').val();
			if (posic == 0){
				$('div#principalPosicion1').hide();
				$('div#principalPosicion2').hide();
				$('div#principalPosicion3').hide();
				$('div#principalPosicion4').hide();
			} else if (posic == 1){
				$('div#principalPosicion2').hide();
				$('div#principalPosicion3').hide();
				$('div#principalPosicion4').hide();
				$('div#principalPosicion1').show();
				
			} else if (posic == 2){
				$('div#principalPosicion1').hide();
				$('div#principalPosicion3').hide();
				$('div#principalPosicion4').hide();
				$('div#principalPosicion2').show();
				
			} else if (posic == 3){
				$('div#principalPosicion1').hide();
				$('div#principalPosicion2').hide();
				$('div#principalPosicion4').hide();
				$('div#principalPosicion3').show();
				
			} else if (posic == 4){
				$('div#principalPosicion1').hide();
				$('div#principalPosicion2').hide();
				$('div#principalPosicion3').hide();
				$('div#principalPosicion4').show();
			}
		}
		
		
		function guardarPromocion(){
			var principal = $('select#subcategoriaPrincipal').val();
			//alert(principal);
			if (principal == 1){
				var posicion = $('select#subcategoriaPosicion').val();
				if (posicion == 0){
					alert ("Debe seleccionar una posición para el banner");
					return false;
				} else if (posicion == 1) {
					var idplan = document.getElementById('idPlan').value;
					var nombre = document.getElementById('nombre').value;
					var tracker = document.getElementById('tracker').value;
					var link = document.getElementById('link').value;
					var imagen = document.getElementById('imagen').value;
						document.getElementById('hiddIdPlan').value = idplan;
						document.getElementById('hiddNombre').value = nombre;
						document.getElementById('hiddTracker').value = tracker;
						document.getElementById('hiddLink').value = link;
						document.getElementById('hiddImagen').value = imagen;
						document.getElementById('hiddPosicion').value = posicion;
						document.getElementById('hiddPrincipal').value = principal;
						alert('Promocion Ingresada Correctamente');
						document.getElementById('hiddSubmit').value = "0";
						$("#informeComercialTarjetas").submit();
						return false;
					
				} else if (posicion == 2) {
					var idplan = document.getElementById('idPlan2').value;
					var nombre = document.getElementById('nombre2').value;
					var tracker = document.getElementById('tracker2').value;
					var link = document.getElementById('link2').value;
					var imagen = document.getElementById('imagen2').value;
						document.getElementById('hiddIdPlan2').value = idplan;
						document.getElementById('hiddNombre2').value = nombre;
						document.getElementById('hiddTracker2').value = tracker;
						document.getElementById('hiddLink2').value = link;
						document.getElementById('hiddImagen2').value = imagen;
						document.getElementById('hiddPosicion').value = posicion;
						document.getElementById('hiddPrincipal2').value = principal;
						alert('Promocion Ingresada Correctamente');
						document.getElementById('hiddSubmit').value = "0";
						$("#informeComercialTarjetas").submit();
						return false;
				} else if (posicion == 3) {
					var idplan = document.getElementById('idPlan3').value;
					var nombre = document.getElementById('nombre3').value;
					var tracker = document.getElementById('tracker3').value;
					var link = document.getElementById('link3').value;
					var imagen = document.getElementById('imagen3').value;
						document.getElementById('hiddIdPlan3').value = idplan;
						document.getElementById('hiddNombre3').value = nombre;
						document.getElementById('hiddTracker3').value = tracker;
						document.getElementById('hiddLink3').value = link;
						document.getElementById('hiddImagen3').value = imagen;
						document.getElementById('hiddPosicion').value = posicion;
						document.getElementById('hiddPrincipal3').value = principal;
						alert('Promocion Ingresada Correctamente');
						document.getElementById('hiddSubmit').value = "0";
						$("#informeComercialTarjetas").submit();
						return false;		
				} else if (posicion == 4) {
					var idplan = document.getElementById('idPlan4').value;
					var nombre = document.getElementById('nombre4').value;
					var tracker = document.getElementById('tracker4').value;
					var link = document.getElementById('link4').value;
					var imagen = document.getElementById('imagen4').value;
						document.getElementById('hiddIdPlan4').value = idplan;
						document.getElementById('hiddNombre4').value = nombre;
						document.getElementById('hiddTracker4').value = tracker;
						document.getElementById('hiddLink4').value = link;
						document.getElementById('hiddImagen4').value = imagen;
						document.getElementById('hiddPosicion').value = posicion;
						document.getElementById('hiddPrincipal4').value = principal;
						alert('Promocion Ingresada Correctamente');
						document.getElementById('hiddSubmit').value = "0";
						$("#informeComercialTarjetas").submit();
						return false;
				}
			} else if (principal == 2){
				var posicion = $('select#subcategoriaPosicionSecundaria').val();
				//if (posicion == "A0" || posicion == "H0" || posicion == "V0"){
					var nombreVB1 = document.getElementById('nombreVB1').value;
					var nombreVB2 = document.getElementById('nombreVB2').value;
					var nombreHB1 = document.getElementById('nombreHB1').value;
					var nombreHB2 = document.getElementById('nombreHB2').value;
					var nombreVIB1 = document.getElementById('nombreVIB1').value;
					var nombreVIB2 = document.getElementById('nombreVIB2').value;
					var trackerVB1 = document.getElementById('trackerVB1').value;
					var trackerVB2 = document.getElementById('trackerVB2').value;
					var trackerHB1 = document.getElementById('trackerHB1').value;
					var trackerHB2 = document.getElementById('trackerHB2').value;
					var trackerVIB1 = document.getElementById('trackerVIB1').value;
					var trackerVIB2 = document.getElementById('trackerVIB2').value;
					var imagenVB1 = document.getElementById('imagenVB1').value;
					var imagenVB2 = document.getElementById('imagenVB2').value;
					var imagenHB1 = document.getElementById('imagenHB1').value;
					var imagenHB2 = document.getElementById('imagenHB2').value;
					var imagenVIB1 = document.getElementById('imagenVIB1').value;
					var imagenVIB2 = document.getElementById('imagenVIB2').value;
					var linkVerVB1 = document.getElementById('linkVerBasesVB1').value;
					var linkVerVB2 = document.getElementById('linkVerBasesVB2').value;
					var linkVerHB1 = document.getElementById('linkVerBasesHB1').value;
					var linkVerHB2 = document.getElementById('linkVerBasesHB2').value;
					var linkVerVIB1 = document.getElementById('linkVerBasesVIB1').value;
					var linkVerVIB2 = document.getElementById('linkVerBasesVIB2').value;
					var linkConocerVB1 = document.getElementById('linkConocerVB1').value;
					var linkConocerVB2 = document.getElementById('linkConocerVB2').value;
					var linkConocerHB1 = document.getElementById('linkConocerHB1').value;
					var linkConocerHB2 = document.getElementById('linkConocerHB2').value;
					var linkConocerVIB1 = document.getElementById('linkConocerVIB1').value;
					var linkConocerVIB2 = document.getElementById('linkConocerVIB2').value;
					var linkCotizarVB1 = document.getElementById('linkCotizarVB1').value;
					var linkCotizarVB2 = document.getElementById('linkCotizarVB2').value;
					var linkCotizarHB1 = document.getElementById('linkCotizarHB1').value;
					var linkCotizarHB2 = document.getElementById('linkCotizarHB2').value;
					var linkCotizarVIB1 = document.getElementById('linkCotizarVIB1').value;
					var linkCotizarVIB2 = document.getElementById('linkCotizarVIB2').value;
					var idPlanVB1 = document.getElementById('idPlanVB1').value;
					var idPlanVB2 = document.getElementById('idPlanVB2').value;
					var idPlanHB1 = document.getElementById('idPlanHB1').value;
					var idPlanHB2 = document.getElementById('idPlanHB2').value;
					var idPlanVIB1 = document.getElementById('idPlanVIB1').value;
					var idPlanVIB2 = document.getElementById('idPlanVIB2').value;
				
					alert('Insertado Correctamente');	
					document.getElementById('hiddPrincipal').value = principal;
					document.getElementById('hiddNombreVB1').value = nombreVB1;
						document.getElementById('hiddTrackerVB1').value = trackerVB1;
						document.getElementById('hiddLinkVerBasesVB1').value = linkVerVB1;
						document.getElementById('hiddLinkConocerVB1').value = linkConocerVB1;
						document.getElementById('hiddLinkCotizarVB1').value = linkCotizarVB1;
						document.getElementById('hiddImagenVB1').value = imagenVB1;
						document.getElementById('hiddOrdinalVB1').value = 1
						document.getElementById('hiddPrincipalVB1').value = principal;
					document.getElementById('hiddNombreVB2').value = nombreVB2;
						document.getElementById('hiddTrackerVB2').value = trackerVB2;
						document.getElementById('hiddLinkVerBasesVB2').value = linkVerVB2;
						document.getElementById('hiddLinkConocerVB2').value = linkConocerVB2;
						document.getElementById('hiddLinkCotizarVB2').value = linkCotizarVB2;
						document.getElementById('hiddImagenVB2').value = imagenVB2;
						document.getElementById('hiddOrdinalVB2').value = 2
						document.getElementById('hiddPrincipalVB2').value = principal;
					document.getElementById('hiddNombreHB1').value = nombreHB1;
						document.getElementById('hiddTrackerHB1').value = trackerHB1;
						document.getElementById('hiddLinkVerBasesHB1').value = linkVerHB1;
						document.getElementById('hiddLinkConocerHB1').value = linkConocerHB1;
						document.getElementById('hiddLinkCotizarHB1').value = linkCotizarHB1;
						document.getElementById('hiddImagenHB1').value = imagenHB1;
						document.getElementById('hiddOrdinalHB1').value = 1
						document.getElementById('hiddPrincipalHB1').value = principal;
					document.getElementById('hiddNombreHB2').value = nombreHB2;
						document.getElementById('hiddTrackerHB2').value = trackerHB2;
						document.getElementById('hiddLinkVerBasesHB2').value = linkVerHB2;
						document.getElementById('hiddLinkConocerHB2').value = linkConocerHB2;
						document.getElementById('hiddLinkCotizarHB2').value = linkCotizarHB2;
						document.getElementById('hiddImagenHB2').value = imagenHB2;
						document.getElementById('hiddOrdinalHB2').value = 2
						document.getElementById('hiddPrincipalHB2').value = principal;
					document.getElementById('hiddNombreVIB1').value = nombreVIB1;
						document.getElementById('hiddTrackerVIB1').value = trackerVIB1;
						document.getElementById('hiddLinkVerBasesVIB1').value = linkVerVIB1;
						document.getElementById('hiddLinkConocerVIB1').value = linkConocerVIB1;
						document.getElementById('hiddLinkCotizarVIB1').value = linkCotizarVIB1;
						document.getElementById('hiddImagenVIB1').value = imagenVIB1;
						document.getElementById('hiddOrdinalVIB1').value = 1
						document.getElementById('hiddPrincipalVIB1').value = principal;
					document.getElementById('hiddNombreVIB2').value = nombreVIB2;
						document.getElementById('hiddTrackerVIB2').value = trackerVIB2;
						document.getElementById('hiddLinkVerBasesVIB2').value = linkVerVIB2;
						document.getElementById('hiddLinkConocerVIB2').value = linkConocerVIB2;
						document.getElementById('hiddLinkCotizarVIB2').value = linkCotizarVIB2;
						document.getElementById('hiddImagenVIB2').value = imagenVIB2;
						document.getElementById('hiddOrdinalVIB2').value = 2
						document.getElementById('hiddPrincipalVIB2').value = principal;
					document.getElementById('hiddIdPlanVB1').value = idPlanVB1;
					document.getElementById('hiddIdPlanVB2').value = idPlanVB2;
					document.getElementById('hiddIdPlanHB1').value = idPlanHB1;
					document.getElementById('hiddIdPlanHB2').value = idPlanHB2;
					document.getElementById('hiddIdPlanVIB1').value = idPlanVIB1;
					document.getElementById('hiddIdPlanVIB2').value = idPlanVIB2;
					document.getElementById('hiddSubmit').value = "0";
					$("#informeComercialTarjetas").submit();
					
			}
		}
		
		
	
</script>

<script type="text/javascript">

	function abrirImagen(url){
        $.colorbox({width:"600", height:"300",  iframe:true, href:url});
    }


</script>



</head>

<body>
	<html:form action="/administrador-promociones.do" method="post" styleId="informeComercialTarjetas">
	<table class="datos" width="800" cellspacing="0" cellpadding="0" border="0" align="center">
				<tbody>
					<tr>
						<td>
		<table cellpadding="0" cellspacing="0" border="0" width="100%" style="float:left; border: 1px solid #ffffff; padding: 15px 15px 40px;" >
								
								<tr>
									<td colspan="4">
									<h2> Página Principal </h2>
									</td>
								</tr>
								<tr>
									<th width="220">Ubicación : </th>
									<td width="200">
										<select id="subcategoriaPrincipal" name="subcategoria" onChange="cargarPagina()">
											<option value="0"> Seleccionar</option>
											<option value="1">Banner Principal</option>
											<option value="2">Banner Secundario</option>
											
										</select>
										
									</td>
									<th width="80"> </th>
									<td>
										
									</td>
								</tr>
		</table>
		<div id="principal">
		<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; padding: 15px; border-collapse: separate; " >
				<tr>
					<th width="100%" style="color: rgb(95, 194, 241); font-size: 16px; "> Página Principal de Promociones - Banner Principal  </th>
				</tr>
		</table>	
		<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								
								<tr>
									<th width="30%"> Posicion : </th>
									<td width="200">
										<select id="subcategoriaPosicion" name="subcategoria" onChange="javascript:cargarPosicion();">
											<option value=""> Seleccionar</option>
											<option value="1">Banner 1</option>
											<option value="2">Banner 2</option>
											<option value="3">Banner 3</option>
											<option value="4">Banner 4</option>
											
										</select>
									</td>
								</tr>
		</table>	
		</div>				
		<div id="principalPosicion1">
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlan" type="text" value="<bean:write name="idPlan"></bean:write>" size="12" name="idPlan">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombre" type="text" value="<bean:write name="nombre"></bean:write>" size="12" name="nombre" />
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="tracker" type="text" value="<bean:write name="tracker"></bean:write>" size="12" name="tracker">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagen" type="text" value="<bean:write name="imagen"></bean:write>" size="12" name="imagen">
									</td>
									<th width="220"> Link : </th>
									<td>
										<input id="link" type="text" value="<bean:write name="link"></bean:write>" size="12" name="link">
									</td>
								</tr>
								
								<tr>
										<th width="30%">Fecha Inicio Publicación: </th>
                                    	<td><html:text property="datos(fechaDesde)" name="informeComercialTarjetas" styleId="inputFechaDesde" size="12"></html:text></td>
                                   		<th width="23%">Fecha Término Publicación: </th>
                                    	<td><html:text property="datos(fechaHasta)" name="informeComercialTarjetas" styleId="inputFechaHasta" size="12"></html:text></td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="30%">
									</th>
									<td>
										<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagen').value);">
										<input id="buscar" type="button" name="buscar" value="Guardar" onclick="guardarPromocion();">
									</td>
								</tr>
								
							</table>
							<html:hidden property="datos(hiddIdPlan)" value="1" styleId="hiddIdPlan"/>
							<html:hidden property="datos(hiddNombre)" value="1" styleId="hiddNombre"/>
							<html:hidden property="datos(hiddLink)" value="1" styleId="hiddLink"/>
							<html:hidden property="datos(hiddTracker)" value="1" styleId="hiddTracker"/>
							<html:hidden property="datos(hiddImagen)" value="1" styleId="hiddImagen"/>
							<html:hidden property="datos(hiddPosicion)" value="1" styleId="hiddPosicion"/>
							<html:hidden property="datos(hiddPrincipal)" value="1" styleId="hiddPrincipal"/>
							
							<html:hidden property="datos(hiddIdPlanX)" value="<bean:write name='idPlan'></bean:write>" styleId="hiddIdPlanX"/>
							<html:hidden property="datos(hiddNombreX)" value="<bean:write name='nombre'></bean:write>" styleId="hiddNombreX"/>
							<html:hidden property="datos(hiddLinkX)" value="<bean:write name='link'></bean:write>" styleId="hiddLinkX"/>
							<html:hidden property="datos(hiddTrackerX)" value="<bean:write name='tracker'></bean:write>" styleId="hiddTrackerX"/>
							<html:hidden property="datos(hiddImagenX)" value="<bean:write name='imagen'></bean:write>" styleId="hiddImagenX"/>
		</div>
		
		
		<!-- posicion 2 -->
		
		<div id="principalPosicion2">
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlan2" type="text" value="<bean:write name="idPlan2"></bean:write>" size="12" name="idPlan2">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombre2" type="text" value="<bean:write name="nombre2"></bean:write>" size="12" name="nombre2" />
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="tracker2" type="text" value="<bean:write name="tracker2"></bean:write>" size="12" name="tracker2">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagen2" type="text" value="<bean:write name="imagen2"></bean:write>" size="12" name="imagen2">
									</td>
									<th width="220"> Link : </th>
									<td>
										<input id="link2" type="text" value="<bean:write name="link2"></bean:write>" size="12" name="link2">
									</td>
								</tr>
								
								<tr>
										<th width="30%">Fecha Inicio Publicación: </th>
                                    	<td><html:text property="datos(fechaDesde2)" name="informeComercialTarjetas" styleId="inputFechaDesde2" size="12"></html:text></td>
                                   		<th width="23%">Fecha Término Publicación: </th>
                                    	<td><html:text property="datos(fechaHasta2)" name="informeComercialTarjetas" styleId="inputFechaHasta2" size="12"></html:text></td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="30%">
									</th>
									<td>
										<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagen2').value);">
										<input id="buscar" type="button" name="buscar" value="Guardar" onclick="guardarPromocion();">
									</td>
								</tr>
								
		</table>
		<html:hidden property="datos(hiddIdPlan2)" value="1" styleId="hiddIdPlan2"/>
		<html:hidden property="datos(hiddNombre2)" value="1" styleId="hiddNombre2"/>
		<html:hidden property="datos(hiddLink2)" value="1" styleId="hiddLink2"/>
		<html:hidden property="datos(hiddTracker2)" value="1" styleId="hiddTracker2"/>
		<html:hidden property="datos(hiddImagen2)" value="1" styleId="hiddImagen2"/>
		<html:hidden property="datos(hiddPosicion)" value="1" styleId="hiddPosicion"/>
		<html:hidden property="datos(hiddPrincipal2)" value="1" styleId="hiddPrincipal2"/>
		
		<html:hidden property="datos(hiddIdPlanX2)" value="<bean:write name='idPlan2'></bean:write>" styleId="hiddIdPlanX2"/>
		<html:hidden property="datos(hiddNombreX2)" value="<bean:write name='nombre2'></bean:write>" styleId="hiddNombreX2"/>
		<html:hidden property="datos(hiddLinkX2)" value="<bean:write name='link2'></bean:write>" styleId="hiddLinkX2"/>
		<html:hidden property="datos(hiddTrackerX2)" value="<bean:write name='tracker2'></bean:write>" styleId="hiddTrackerX2"/>
		<html:hidden property="datos(hiddImagenX2)" value="<bean:write name='imagen2'></bean:write>" styleId="hiddImagenX2"/>
		</div>
		
		<!-- posicion 3 -->
		
		<div id="principalPosicion3">
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlan3" type="text" value="<bean:write name="idPlan3"></bean:write>" size="12" name="idPlan3">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombre3" type="text" value="<bean:write name="nombre3"></bean:write>" size="12" name="nombre3" />
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="tracker3" type="text" value="<bean:write name="tracker3"></bean:write>" size="12" name="tracker3">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagen3" type="text" value="<bean:write name="imagen3"></bean:write>" size="12" name="imagen3">
									</td>
									<th width="220"> Link : </th>
									<td>
										<input id="link3" type="text" value="<bean:write name="link3"></bean:write>" size="12" name="link3">
									</td>
								</tr>
								
								<tr>
										<th width="30%">Fecha Inicio Publicación: </th>
                                    	<td><html:text property="datos(fechaDesde3)" name="informeComercialTarjetas" styleId="inputFechaDesde3" size="12"></html:text></td>
                                   		<th width="23%">Fecha Término Publicación: </th>
                                    	<td><html:text property="datos(fechaHasta3)" name="informeComercialTarjetas" styleId="inputFechaHasta3" size="12"></html:text></td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="30%">
									</th>
									<td>
										<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagen3').value);">
										<input id="buscar" type="button" name="buscar" value="Guardar" onclick="guardarPromocion();">
									</td>
								</tr>
								
		</table>
		<html:hidden property="datos(hiddIdPlan3)" value="1" styleId="hiddIdPlan3"/>
		<html:hidden property="datos(hiddNombre3)" value="1" styleId="hiddNombre3"/>
		<html:hidden property="datos(hiddLink3)" value="1" styleId="hiddLink3"/>
		<html:hidden property="datos(hiddTracker3)" value="1" styleId="hiddTracker3"/>
		<html:hidden property="datos(hiddImagen3)" value="1" styleId="hiddImagen3"/>
		<html:hidden property="datos(hiddPosicion)" value="1" styleId="hiddPosicion"/>
		<html:hidden property="datos(hiddPrincipal3)" value="1" styleId="hiddPrincipal3"/>
		
		<html:hidden property="datos(hiddIdPlanX3)" value="<bean:write name='idPlan3'></bean:write>" styleId="hiddIdPlanX3"/>
		<html:hidden property="datos(hiddNombreX3)" value="<bean:write name='nombre3'></bean:write>" styleId="hiddNombreX3"/>
		<html:hidden property="datos(hiddLinkX3)" value="<bean:write name='link3'></bean:write>" styleId="hiddLinkX3"/>
		<html:hidden property="datos(hiddTrackerX3)" value="<bean:write name='tracker3'></bean:write>" styleId="hiddTrackerX3"/>
		<html:hidden property="datos(hiddImagenX3)" value="<bean:write name='imagen3'></bean:write>" styleId="hiddImagenX3"/>
		</div>
		
		<!-- posicion 4 -->
		
		<div id="principalPosicion4">
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlan4" type="text" value="<bean:write name="idPlan4"></bean:write>" size="12" name="idPlan4">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombre4" type="text" value="<bean:write name="nombre4"></bean:write>" size="12" name="nombre4" />
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="tracker4" type="text" value="<bean:write name="tracker4"></bean:write>" size="12" name="tracker4">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagen4" type="text" value="<bean:write name="imagen4"></bean:write>" size="12" name="imagen4">
									</td>
									<th width="220"> Link : </th>
									<td>
										<input id="link4" type="text" value="<bean:write name="link4"></bean:write>" size="12" name="link4">
									</td>
								</tr>
								
								<tr>
										<th width="30%">Fecha Inicio Publicación: </th>
                                    	<td><html:text property="datos(fechaDesde4)" name="informeComercialTarjetas" styleId="inputFechaDesde4" size="12"></html:text></td>
                                   		<th width="23%">Fecha Término Publicación: </th>
                                    	<td><html:text property="datos(fechaHasta4)" name="informeComercialTarjetas" styleId="inputFechaHasta4" size="12"></html:text></td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="30%">
									</th>
									<td>
										<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagen4').value);">
										<input id="buscar" type="button" name="buscar" value="Guardar" onclick="guardarPromocion();">
									</td>
								</tr>
								
		</table>
		<html:hidden property="datos(hiddIdPlan4)" value="1" styleId="hiddIdPlan4"/>
		<html:hidden property="datos(hiddNombre4)" value="1" styleId="hiddNombre4"/>
		<html:hidden property="datos(hiddLink4)" value="1" styleId="hiddLink4"/>
		<html:hidden property="datos(hiddTracker4)" value="1" styleId="hiddTracker4"/>
		<html:hidden property="datos(hiddImagen4)" value="1" styleId="hiddImagen4"/>
		<html:hidden property="datos(hiddPosicion)" value="1" styleId="hiddPosicion"/>
		<html:hidden property="datos(hiddPrincipal4)" value="1" styleId="hiddPrincipal4"/>
		
		<html:hidden property="datos(hiddIdPlanX4)" value="<bean:write name='idPlan4'></bean:write>" styleId="hiddIdPlanX4"/>
		<html:hidden property="datos(hiddNombreX4)" value="<bean:write name='nombre4'></bean:write>" styleId="hiddNombreX4"/>
		<html:hidden property="datos(hiddLinkX4)" value="<bean:write name='link4'></bean:write>" styleId="hiddLinkX4"/>
		<html:hidden property="datos(hiddTrackerX4)" value="<bean:write name='tracker4'></bean:write>" styleId="hiddTrackerX4"/>
		<html:hidden property="datos(hiddImagenX4)" value="<bean:write name='imagen4'></bean:write>" styleId="hiddImagenX4"/>
		</div>
		<!-- secundario -->
		<div id="secundario">
		<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; padding: 15px; border-collapse: separate; " >
									<tr>
										<th width="100%" style="color: rgb(95, 194, 241); font-size: 16px; "> Página Principal de Promociones - Banner Secundarios  </th>
										
									</tr>
							</table>
							
							<!-- Banner Auto 1 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;" > Banner Auto 1 </th>
									<td width="55">
										
									</td>
									<th width="30%" > Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaPosicionSecundaria" name="subcategoria">
											<option value="A0"> Seleccionar</option>
											<option value="A2">Banner Auto 2</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanVB1" type="text" value="<bean:write name="idPlanVB1"></bean:write>" size="12" name="idPlanVB1">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreVB1" class="hasDatepicker" type="text" value="<bean:write name="nombreVB1"></bean:write>" size="12" name="nombreVB1">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerVB1" class="hasDatepicker" type="text" value="<bean:write name="trackerVB1"></bean:write>" size="12" name="trackerVB1">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenVB1" class="hasDatepicker" type="text" value="<bean:write name="imagenVB1"></bean:write>" size="12" name="imagenVB1">
									</td>
									<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesVB1" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesVB1"></bean:write>" size="12" name="linkVerBasesVB1">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer Más : </th>
									<td>
										<input id="linkConocerVB1" class="hasDatepicker" type="text" value="<bean:write name="linkConocerVB1"></bean:write>" size="12" name="linkConocerVB1">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarVB1" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarVB1"></bean:write>" size="12" name="linkCotizarVB1">
									</td>
								</tr>
								<tr>
									<th width="30%">Fecha Inicio Publicación: </th>
                                    	<td><html:text property="datos(fechaDesdeVB1)" name="informeComercialTarjetas" styleId="inputFechaDesdeVB1" size="12"></html:text></td>
                                   		<th width="23%">Fecha Término Publicación: </th>
                                    	<td><html:text property="datos(fechaHastaVB1)" name="informeComercialTarjetas" styleId="inputFechaHastaVB1" size="12"></html:text></td>
								</tr>
								<td>
										<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenVB1').value);">
								</td>
								<html:hidden property="datos(hiddIdPlanVB1)" value="1" styleId="hiddIdPlanVB1"/>
								<html:hidden property="datos(hiddNombreVB1)" value="1" styleId="hiddNombreVB1"/>
								<html:hidden property="datos(hiddLinkVerBasesVB1)" value="1" styleId="hiddLinkVerBasesVB1"/>
								<html:hidden property="datos(hiddLinkConocerVB1)" value="1" styleId="hiddLinkConocerVB1"/>
								<html:hidden property="datos(hiddLinkCotizarVB1)" value="1" styleId="hiddLinkCotizarVB1"/>
								<html:hidden property="datos(hiddTrackerVB1)" value="1" styleId="hiddTrackerVB1"/>
								<html:hidden property="datos(hiddImagenVB1)" value="1" styleId="hiddImagenVB1"/>
								<html:hidden property="datos(hiddPrincipalVB1)" value="1" styleId="hiddPrincipalVB1"/>
								<html:hidden property="datos(hiddOrdinalVB1)" value="1" styleId="hiddOrdinalVB1"/>
								
								<html:hidden property="datos(hiddIdPlanVB1X)" value="<bean:write name='idPlanVB1'></bean:write>" styleId="hiddIdPlanVB1X"/>
								<html:hidden property="datos(hiddLinkVerVB1X)" value="<bean:write name='linkVerBasesVB1'></bean:write>" styleId="hiddLinkVerVB1X"/>
								<html:hidden property="datos(hiddLinkConocerVB1X)" value="<bean:write name='linkConocerVB1'></bean:write>" styleId="hiddLinkConocerVB1X"/>
								<html:hidden property="datos(hiddLinkCotizarVB1X)" value="<bean:write name='linkCotizarVB1'></bean:write>" styleId="hiddLinkCotizarVB1X"/>
								<html:hidden property="datos(hiddTrackerVB1X)" value="<bean:write name='trackerVB1'></bean:write>" styleId="hiddTrackerVB1X"/>
								<html:hidden property="datos(hiddImagenVB1X)" value="<bean:write name='imagenVB1'></bean:write>" styleId="hiddImagenVB1X"/>
								<html:hidden property="datos(hiddNombreVB1X)" value="<bean:write name='nombreVB1'></bean:write>" styleId="hiddNombreVB1X"/>
							</table>
								
							
							<!-- Banner Auto 2 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;" > Banner Auto 2 </th>
									<td width="55">
										
									</td>
									<th width="30%" > Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaPosicionSecundaria" name="subcategoria">
											<option value="A0"> Seleccionar</option>
											<option value="A1">Banner Auto 1</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanVB2" type="text" value="<bean:write name="idPlanVB2"></bean:write>" size="12" name="idPlanVB2">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreVB2" class="hasDatepicker" type="text" value="<bean:write name="nombreVB2"></bean:write>" size="12" name="nombreVB2">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerVB2" class="hasDatepicker" type="text" value="<bean:write name="trackerVB2"></bean:write>" size="12" name="trackerVB2">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenVB2" class="hasDatepicker" type="text" value="<bean:write name="imagenVB2"></bean:write>" size="12" name="imagenVB2">
									</td>
									<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesVB2" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesVB2"></bean:write>" size="12" name="linkVerBasesVB2">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer Más : </th>
									<td>
										<input id="linkConocerVB2" class="hasDatepicker" type="text" value="<bean:write name="linkConocerVB2"></bean:write>" size="12" name="linkConocerVB2">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarVB2" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarVB2"></bean:write>" size="12" name="linkCotizarVB2">
									</td>
								</tr>
								
								<tr>
									<th width="30%">Fecha Inicio Publicación: </th>
                                    	<td><html:text property="datos(fechaDesdeVB2)" name="informeComercialTarjetas" styleId="inputFechaDesdeVB2" size="12"></html:text></td>
                                   		<th width="23%">Fecha Término Publicación: </th>
                                    	<td><html:text property="datos(fechaHastaVB2)" name="informeComercialTarjetas" styleId="inputFechaHastaVB2" size="12"></html:text></td>
								</tr>
								<td>
										<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenVB2').value);">
																			</td>
								<html:hidden property="datos(hiddIdPlanVB2)" value="1" styleId="hiddIdPlanVB2"/>
								<html:hidden property="datos(hiddNombreVB2)" value="1" styleId="hiddNombreVB2"/>
								<html:hidden property="datos(hiddLinkVerBasesVB2)" value="1" styleId="hiddLinkVerBasesVB2"/>
								<html:hidden property="datos(hiddLinkConocerVB2)" value="1" styleId="hiddLinkConocerVB2"/>
								<html:hidden property="datos(hiddLinkCotizarVB2)" value="1" styleId="hiddLinkCotizarVB2"/>
								<html:hidden property="datos(hiddTrackerVB2)" value="1" styleId="hiddTrackerVB2"/>
								<html:hidden property="datos(hiddImagenVB2)" value="1" styleId="hiddImagenVB2"/>
								<html:hidden property="datos(hiddPrincipalVB2)" value="1" styleId="hiddPrincipalVB2"/>
								<html:hidden property="datos(hiddOrdinalVB2)" value="1" styleId="hiddOrdinalVB2"/>
								
								<html:hidden property="datos(hiddIdPlanVB2X)" value="<bean:write name='idPlanVB2'></bean:write>" styleId="hiddIdPlanVB2X"/>
								<html:hidden property="datos(hiddLinkVerVB2X)" value="<bean:write name='linkVerBasesVB2'></bean:write>" styleId="hiddLinkVerVB2X"/>
								<html:hidden property="datos(hiddLinkConocerVB2X)" value="<bean:write name='linkConocerVB2'></bean:write>" styleId="hiddLinkConocerVB2X"/>
								<html:hidden property="datos(hiddLinkCotizarVB2X)" value="<bean:write name='linkCotizarVB2'></bean:write>" styleId="hiddLinkCotizarVB2X"/>
								<html:hidden property="datos(hiddTrackerVB2X)" value="<bean:write name='trackerVB2'></bean:write>" styleId="hiddTrackerVB2X"/>
								<html:hidden property="datos(hiddImagenVB2X)" value="<bean:write name='imagenVB2'></bean:write>" styleId="hiddImagenVB2X"/>
								<html:hidden property="datos(hiddNombreVB2X)" value="<bean:write name='nombreVB2'></bean:write>" styleId="hiddNombreVB2X"/>
							</table>
							
							
							
							<!-- Banner Hogar 1 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner Hogar 1 </th>
									<td width="55">
									
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaPosicionSecundaria" name="subcategoria">
											<option value="H0"> Seleccionar</option>
											<option value="H2">Banner Hogar 2</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanHB1" type="text" value="<bean:write name="idPlanHB1"></bean:write>" size="12" name="idPlanHB1">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreHB1" class="hasDatepicker" type="text" value="<bean:write name="nombreHB1"></bean:write>" size="12" name="nombreHB1">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerHB1" class="hasDatepicker" type="text" value="<bean:write name="trackerHB1"></bean:write>" size="12" name="trackerHB1">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenHB1" class="hasDatepicker" type="text" value="<bean:write name="imagenHB1"></bean:write>" size="12" name="imagenHB1">
									</td>
									<<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesHB1" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesHB1"></bean:write>" size="12" name="linkVerBasesHB1">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer Más : </th>
									<td>
										<input id="linkConocerHB1" class="hasDatepicker" type="text" value="<bean:write name="linkConocerHB1"></bean:write>" size="12" name="linkConocerHB1">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarHB1" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarHB1"></bean:write>" size="12" name="linkCotizarHB1">
									</td>
								</tr>
								
								<tr>
									<th width="30%">Fecha Inicio Publicación: </th>
                                    	<td><html:text property="datos(fechaDesdeHB1)" name="informeComercialTarjetas" styleId="inputFechaDesdeHB1" size="12"></html:text></td>
                                   		<th width="23%">Fecha Término Publicación: </th>
                                    	<td><html:text property="datos(fechaHastaHB1)" name="informeComercialTarjetas" styleId="inputFechaHastaHB1" size="12"></html:text></td>
								</tr>
								<td>
										<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenHB1').value);">
																			</td>
								<html:hidden property="datos(hiddIdPlanHB1)" value="1" styleId="hiddIdPlanHB1"/>
								<html:hidden property="datos(hiddNombreHB1)" value="1" styleId="hiddNombreHB1"/>
								<html:hidden property="datos(hiddLinkVerBasesHB1)" value="1" styleId="hiddLinkVerBasesHB1"/>
								<html:hidden property="datos(hiddLinkConocerHB1)" value="1" styleId="hiddLinkConocerHB1"/>
								<html:hidden property="datos(hiddLinkCotizarHB1)" value="1" styleId="hiddLinkCotizarHB1"/>
								<html:hidden property="datos(hiddTrackerHB1)" value="1" styleId="hiddTrackerHB1"/>
								<html:hidden property="datos(hiddImagenHB1)" value="1" styleId="hiddImagenHB1"/>
								<html:hidden property="datos(hiddPrincipalHB1)" value="1" styleId="hiddPrincipalHB1"/>
								<html:hidden property="datos(hiddOrdinalHB1)" value="1" styleId="hiddOrdinalHB1"/>
								
								<html:hidden property="datos(hiddIdPlanHB1X)" value="<bean:write name='idPlanHB1'></bean:write>" styleId="hiddIdPlanHB1X"/>
								<html:hidden property="datos(hiddLinkVerHB1X)" value="<bean:write name='linkVerBasesHB1'></bean:write>" styleId="hiddLinkVerHB1X"/>
								<html:hidden property="datos(hiddLinkConocerHB1X)" value="<bean:write name='linkConocerHB1'></bean:write>" styleId="hiddLinkConocerHB1X"/>
								<html:hidden property="datos(hiddLinkCotizarHB1X)" value="<bean:write name='linkCotizarHB1'></bean:write>" styleId="hiddLinkCotizarHB1X"/>
								<html:hidden property="datos(hiddTrackerHB1X)" value="<bean:write name='trackerHB1'></bean:write>" styleId="hiddTrackerHB1X"/>
								<html:hidden property="datos(hiddImagenHB1X)" value="<bean:write name='imagenHB1'></bean:write>" styleId="hiddImagenHB1X"/>
								<html:hidden property="datos(hiddNombreHB1X)" value="<bean:write name='nombreHB1'></bean:write>" styleId="hiddNombreHB1X"/>
							</table>
							
							
							<!-- Banner Hogar 2 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner Hogar 2 </th>
									<td width="55">
									
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaPosicionSecundaria" name="subcategoria">
											<option value="H0"> Seleccionar</option>
											<option value="H1">Banner Hogar 1</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanHB2" type="text" value="<bean:write name="idPlanHB2"></bean:write>" size="12" name="idPlanHB2">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreHB2" class="hasDatepicker" type="text" value="<bean:write name="nombreHB2"></bean:write>" size="12" name="nombreHB2">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerHB2" class="hasDatepicker" type="text" value="<bean:write name="trackerHB2"></bean:write>" size="12" name="trackerHB2">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenHB2" class="hasDatepicker" type="text" value="<bean:write name="imagenHB2"></bean:write>" size="12" name="imagenHB2">
									</td>
									<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesHB2" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesHB2"></bean:write>" size="12" name="linkVerBasesHB2">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer Más : </th>
									<td>
										<input id="linkConocerHB2" class="hasDatepicker" type="text" value="<bean:write name="linkConocerHB2"></bean:write>" size="12" name="linkConocerHB2">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarHB2" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarHB2"></bean:write>" size="12" name="linkCotizarHB2">
									</td>
								</tr>
								
								<tr>
									<th width="30%">Fecha Inicio Publicación: </th>
                                    	<td><html:text property="datos(fechaDesdeHB2)" name="informeComercialTarjetas" styleId="inputFechaDesdeHB2" size="12"></html:text></td>
                                   		<th width="23%">Fecha Término Publicación: </th>
                                    	<td><html:text property="datos(fechaHastaHB2)" name="informeComercialTarjetas" styleId="inputFechaHastaHB2" size="12"></html:text></td>
								</tr>
								<td>
										<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenHB2').value);">
																			</td>
								<html:hidden property="datos(hiddIdPlanHB2)" value="1" styleId="hiddIdPlanHB2"/>
								<html:hidden property="datos(hiddNombreHB2)" value="1" styleId="hiddNombreHB2"/>
								<html:hidden property="datos(hiddLinkVerBasesHB2)" value="1" styleId="hiddLinkVerBasesHB2"/>
								<html:hidden property="datos(hiddLinkConocerHB2)" value="1" styleId="hiddLinkConocerHB2"/>
								<html:hidden property="datos(hiddLinkCotizarHB2)" value="1" styleId="hiddLinkCotizarHB2"/>
								<html:hidden property="datos(hiddTrackerHB2)" value="1" styleId="hiddTrackerHB2"/>
								<html:hidden property="datos(hiddImagenHB2)" value="1" styleId="hiddImagenHB2"/>
								<html:hidden property="datos(hiddPrincipalHB2)" value="1" styleId="hiddPrincipalHB2"/>
								<html:hidden property="datos(hiddOrdinalHB2)" value="1" styleId="hiddOrdinalHB2"/>
								
								<html:hidden property="datos(hiddIdPlanHB2X)" value="<bean:write name='idPlanHB2'></bean:write>" styleId="hiddIdPlanHB2X"/>
								<html:hidden property="datos(hiddLinkVerHB2X)" value="<bean:write name='linkVerBasesHB2'></bean:write>" styleId="hiddLinkVerHB2X"/>
								<html:hidden property="datos(hiddLinkConocerHB2X)" value="<bean:write name='linkConocerHB2'></bean:write>" styleId="hiddLinkConocerHB2X"/>
								<html:hidden property="datos(hiddLinkCotizarHB2X)" value="<bean:write name='linkCotizarHB2'></bean:write>" styleId="hiddLinkCotizarHB2X"/>
								<html:hidden property="datos(hiddTrackerHB2X)" value="<bean:write name='trackerHB2'></bean:write>" styleId="hiddTrackerHB2X"/>
								<html:hidden property="datos(hiddImagenHB2X)" value="<bean:write name='imagenHB2'></bean:write>" styleId="hiddImagenHB2X"/>
								<html:hidden property="datos(hiddNombreHB2X)" value="<bean:write name='nombreHB2'></bean:write>" styleId="hiddNombreHB2X"/>
							</table>
							
							
							<!-- Banner Vida y Salud 1 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner Vida y Salud 1 </th>
									<td width="55">
										
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaPosicionSecundaria" name="subcategoria">
											<option value="V0"> Seleccionar</option>
											<option value="V2">Banner Vida y Salud 2</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanVIB1" type="text" value="<bean:write name="idPlanVIB1"></bean:write>" size="12" name="idPlanVIB1">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreVIB1" class="hasDatepicker" type="text" value="<bean:write name="nombreVIB1"></bean:write>" size="12" name="nombreVIB1">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerVIB1" class="hasDatepicker" type="text" value="<bean:write name="trackerVIB1"></bean:write>" size="12" name="trackerVIB1">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenVIB1" class="hasDatepicker" type="text" value="<bean:write name="imagenVIB1"></bean:write>" size="12" name="imagenVIB1">
									</td>
									<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesVIB1" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesVIB1"></bean:write>" size="12" name="linkVerBasesVIB1">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer Más : </th>
									<td>
										<input id="linkConocerVIB1" class="hasDatepicker" type="text" value="<bean:write name="linkConocerVIB1"></bean:write>" size="12" name="linkConocerVIB1">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarVIB1" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarVIB1"></bean:write>" size="12" name="linkCotizarVIB1">
									</td>
								</tr>
								<tr>
									<th width="30%">Fecha Inicio Publicación: </th>
                                    	<td><html:text property="datos(fechaDesdeVIB1)" name="informeComercialTarjetas" styleId="inputFechaDesdeVIB1" size="12"></html:text></td>
                                   		<th width="23%">Fecha Término Publicación: </th>
                                    	<td><html:text property="datos(fechaHastaVIB1)" name="informeComercialTarjetas" styleId="inputFechaHastaVIB1" size="12"></html:text></td>
								</tr>
								<td>
										<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenVIB1').value);">
																			</td>
								<html:hidden property="datos(hiddIdPlanVIB1)" value="1" styleId="hiddIdPlanVIB1"/>
								<html:hidden property="datos(hiddNombreVIB1)" value="1" styleId="hiddNombreVIB1"/>
								<html:hidden property="datos(hiddLinkVerBasesVIB1)" value="1" styleId="hiddLinkVerBasesVIB1"/>
								<html:hidden property="datos(hiddLinkConocerVIB1)" value="1" styleId="hiddLinkConocerVIB1"/>
								<html:hidden property="datos(hiddLinkCotizarVIB1)" value="1" styleId="hiddLinkCotizarVIB1"/>
								<html:hidden property="datos(hiddTrackerVIB1)" value="1" styleId="hiddTrackerVIB1"/>
								<html:hidden property="datos(hiddImagenVIB1)" value="1" styleId="hiddImagenVIB1"/>
								<html:hidden property="datos(hiddPrincipalVIB1)" value="1" styleId="hiddPrincipalVIB1"/>
								<html:hidden property="datos(hiddOrdinalVIB1)" value="1" styleId="hiddOrdinalVIB1"/>
								
								<html:hidden property="datos(hiddIdPlanVIB1X)" value="<bean:write name='idPlanVIB1'></bean:write>" styleId="hiddIdPlanVIB1X"/>
								<html:hidden property="datos(hiddLinkVerVIB1X)" value="<bean:write name='linkVerBasesVIB1'></bean:write>" styleId="hiddLinkVerVIB1X"/>
								<html:hidden property="datos(hiddLinkConocerVIB1X)" value="<bean:write name='linkConocerVIB1'></bean:write>" styleId="hiddLinkConocerVIB1X"/>
								<html:hidden property="datos(hiddLinkCotizarVIB1X)" value="<bean:write name='linkCotizarVIB1'></bean:write>" styleId="hiddLinkCotizarVIB1X"/>
								<html:hidden property="datos(hiddTrackerVIB1X)" value="<bean:write name='trackerVIB1'></bean:write>" styleId="hiddTrackerVIB1X"/>
								<html:hidden property="datos(hiddImagenVIB1X)" value="<bean:write name='imagenVIB1'></bean:write>" styleId="hiddImagenVIB1X"/>
								<html:hidden property="datos(hiddNombreVIB1X)" value="<bean:write name='nombreVIB1'></bean:write>" styleId="hiddNombreVIB1X"/>
							</table>
							
							
							<!-- Banner Vida y Salud 2 -->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;" >
								<tr>
									<th width="30%" style="color: rgb(95, 194, 241); font-size: 16px;"> Banner Vida y Salud 2 </th>
									<td width="55">
										
									</td>
									<th width="30%"> Intercambiar por </th>
									<td width="200">
										
										<select id="subcategoriaPosicionSecundaria" name="subcategoria">
											<option value="V0"> Seleccionar</option>
											<option value="V1">Banner Vida y Salud 1</option>
											
											
										</select>
										
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Id Plan : </th>
									<td>
										<input id="idPlanVIB2" type="text" value="<bean:write name="idPlanVIB2"></bean:write>" size="12" name="idPlanVIB2">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left;  padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="220"> Nombre : </th>
									<td>
										<input id="nombreVIB2" class="hasDatepicker" type="text" value="<bean:write name="nombreVIB2"></bean:write>" size="12" name="nombreVIB2">
									</td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; border: 1px solid rgb(255, 255, 255); padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									
									
									<th width="220"> Tracker : </th>
									<td>
										<input id="trackerVIB2" class="hasDatepicker" type="text" value="<bean:write name="trackerVIB2"></bean:write>" size="12" name="trackerVIB2">
									</td>
								</tr>
								<tr>
									<th width="220"> Imagen : </th>
									<td>
										<input id="imagenVIB2" class="hasDatepicker" type="text" value="<bean:write name="imagenVIB2"></bean:write>" size="12" name="imagenVIB2">
									</td>
									<th width="220"> Link Ver Bases: </th>
									<td>
										<input id="linkVerBasesVIB2" class="hasDatepicker" type="text" value="<bean:write name="linkVerBasesVIB2"></bean:write>" size="12" name="linkVerBasesVIB2">
									</td>
								</tr>
								<tr>
									<th width="220"> Link Conocer Más : </th>
									<td>
										<input id="linkConocerVIB2" class="hasDatepicker" type="text" value="<bean:write name="linkConocerVIB2"></bean:write>" size="12" name="linkConocerVIB2">
									</td>
									<th width="220"> Link Cotizar: </th>
									<td>
										<input id="linkCotizarVIB2" class="hasDatepicker" type="text" value="<bean:write name="linkCotizarVIB2"></bean:write>" size="12" name="linkCotizarVIB2">
									</td>
								</tr>
								<tr>
									<th width="30%">Fecha Inicio Publicación: </th>
                                    	<td><html:text property="datos(fechaDesdeVIB2)" name="informeComercialTarjetas" styleId="inputFechaDesdeVIB2" size="12"></html:text></td>
                                   		<th width="23%">Fecha Término Publicación: </th>
                                    	<td><html:text property="datos(fechaHastaVIB2)" name="informeComercialTarjetas" styleId="inputFechaHastaVIB2" size="12"></html:text></td>
								</tr>
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" border="0" style="float: left; padding: 15px; border-collapse: separate;  border-left: 1px solid #fff; border-right: 1px solid #fff;" class="listadoSegCab">
								<tr>
									<th width="30%">
									</th>
									<td>
										<input id="ver_imagen" type="button" name="buscar" value="Previsualizar" onClick="javascript:abrirImagen('/vseg-paris' + document.getElementById('imagenVIB2').value);">
										<input id="buscar" type="button" name="buscar" value="Guardar" onclick="guardarPromocion();">
									</td>
								</tr>
								<html:hidden property="datos(hiddIdPlanVIB2)" value="1" styleId="hiddIdPlanVIB2"/>
								<html:hidden property="datos(hiddNombreVIB2)" value="1" styleId="hiddNombreVIB2"/>
								<html:hidden property="datos(hiddLinkVerBasesVIB2)" value="1" styleId="hiddLinkVerBasesVIB2"/>
								<html:hidden property="datos(hiddLinkConocerVIB2)" value="1" styleId="hiddLinkConocerVIB2"/>
								<html:hidden property="datos(hiddLinkCotizarVIB2)" value="1" styleId="hiddLinkCotizarVIB2"/>
								<html:hidden property="datos(hiddTrackerVIB2)" value="1" styleId="hiddTrackerVIB2"/>
								<html:hidden property="datos(hiddImagenVIB2)" value="1" styleId="hiddImagenVIB2"/>
								<html:hidden property="datos(hiddPrincipalVIB2)" value="1" styleId="hiddPrincipalVIB2"/>
								<html:hidden property="datos(hiddOrdinalVIB2)" value="1" styleId="hiddOrdinalVIB2"/>
								
								<html:hidden property="datos(hiddIdPlanVIB2X)" value="<bean:write name='idPlanVIB2'></bean:write>" styleId="hiddIdPlanVIB2X"/>
								<html:hidden property="datos(hiddLinkVerVIB2X)" value="<bean:write name='linkVerBasesVIB2'></bean:write>" styleId="hiddLinkVerVIB2X"/>
								<html:hidden property="datos(hiddLinkConocerVIB2X)" value="<bean:write name='linkConocerVIB2'></bean:write>" styleId="hiddLinkConocerVIB2X"/>
								<html:hidden property="datos(hiddLinkCotizarVIB2X)" value="<bean:write name='linkCotizarVIB2'></bean:write>" styleId="hiddLinkCotizarVIB2X"/>
								<html:hidden property="datos(hiddTrackerVIB2X)" value="<bean:write name='trackerVIB2'></bean:write>" styleId="hiddTrackerVIB2X"/>
								<html:hidden property="datos(hiddImagenVIB2X)" value="<bean:write name='imagenVIB2'></bean:write>" styleId="hiddImagenVIB2X"/>
								<html:hidden property="datos(hiddNombreVIB2X)" value="<bean:write name='nombreVIB2'></bean:write>" styleId="hiddNombreVIB2X"/>
							</table>
							
							<html:hidden property="datos(hiddSubmit)" value="1" styleId="hiddSubmit"/>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</html:form>
</body>
</html:html>
