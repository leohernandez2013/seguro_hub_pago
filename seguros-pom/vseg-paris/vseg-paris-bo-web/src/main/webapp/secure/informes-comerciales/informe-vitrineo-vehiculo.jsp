<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>

<bean:define id="contextpath" value="<%=request.getContextPath()%>" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html>
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<title>:::::: BACK OFFICE ::::::</title>
		

	   <link rel="stylesheet" type="text/css" href="../css/style_seguros.css" />
	   <link type="text/css" rel="stylesheet" href="../css/jquery-ui.css" />	   
		<script type="text/javascript" src="../js/jquery-1.4.2.js"></script>
		<script type="text/javascript" src="../js/jquery-ui-1.8.4.custom.min.js"></script>
		<script src="../js/jquery-tinet.js" type="text/javascript"></script>
		
		<script type="text/javascript">		
		function exportar() {
			try {
				$("#excel").attr("value", 1);
				$("#informeComercialVehiculo").attr("target", "iframe-excel");
				$("#informeComercialVehiculo").submit();	
			} catch (e) {
				alert(e.message);
			}
		}
		
		$(document).ready(function(){
		
			$("#inputFechaDesde, #inputFechaHasta").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesde, #inputFechaHasta").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			$("#numeroPagina").change(function(){
				$("#excel").attr("value", 0);
				$("#informeComercialVehiculo").attr("target", "");
				$("#informeComercialVehiculo").submit();
			});
			
			$("#buscar").click(function () {
				try {
					$("#excel").attr("value", 0);
					$("#informeComercialVehiculo").attr("target", "");
					$("#informeComercialVehiculo").submit();	
				} catch (e) {
					alert(e.message);
				}
				return false;
			});
			$("a[name='factura_informe']").click(function(){
				 
				 $(this).colorbox({
			     href: $(this).attr("href"), 
			     photo: true
			     });
			});
			colores('lista');
		});
		</script>
		
		<script type="text/javascript">
		$(document).ready(function(){
		$("select#rama").change(function() {
				$.getJSON("/vseg-paris-adm/buscar-subcategorias.do",{idRama: $(this).val(), ajax: 'true'}, function(j){
					$('select#subcategoria option').remove();
					var options = '';
					options += '<option value="0"><bean:message bundle="labels" key="general.todas" /></option>';
					for (var i = 0; i < j.length; i++) {
						options += '<option value="' + j[i].id_subcategoria + '">' + j[i].titulo_subcategoria + '</option>';
                    }
	      			$('select#subcategoria').html(options);
	    		})
  			});
			
  			$("select#subcategoria").change(function() {
					$('.c_vehiculo').hide();
					$('.c_vida').hide();
					$('.c_plan').hide();
					$.getJSON("/vseg-paris-adm/buscar-planes.do",{idSubcategoria: $(this).val(), ajax: 'true'}, function(j){
						$('select#id_plan option').remove();
						var options = '';
						options += '<option value=""><bean:message bundle="labels" key="general.todas" /></option>';
						for (var i = 0; i < j.length; i++) {
							options += '<option value="' + j[i].id_plan + '">' + j[i].nombre + '</option>';
		                }
		      			$('select#id_plan').html(options);
		    		})		
				});

				$("select#id_plan").change(function() {
					$("#desc_plan").html($("select#id_plan option:selected").text());
					$(".c_plan").show();

					//Validar la rama.
					var rama = $("select#rama option:selected").val();
					if(rama == 1) {
						//Vehiculo
						$(".c_vehiculo").show();
					} else if(rama == 3) {
						//Vida
						$(".c_vida").show();
					}
					cargarDatosPlan();
				});
		});
		</script>
	</head>
	
<body>

<html:form action="/obtener-vitrineo-vehiculo.do" method="post" styleId="informeComercialVehiculo">
<table cellpadding="0" cellspacing="0" border="0" width="800" align="center" class="datos">
	<tr>
		<td>
            <table cellpadding="0" cellspacing="0" border="0"  width="100%"  align="center" >
				<tr valign="top">
					<td height="33" >Informe Vitrineo<br/></td>
			  </tr>
    			<tr>
    				<td>
        				<div>							
							<html:hidden property="datos(excel)" name="informeComercialVehiculo" styleId="excel" value="0"/>
                        	<table cellpadding="0" cellspacing="0" border="0" width="95%" align="center">
                              <tr>
                                	<th width="26%" >Fecha Desde: </th>
                                    <td><html:text property="datos(fechaDesde)" name="informeComercialVehiculo" styleId="inputFechaDesde" size="12"></html:text></td>
                                    <th width="26%" >Fecha Hasta: </th>
                                    <td><html:text property="datos(fechaHasta)" name="informeComercialVehiculo" styleId="inputFechaHasta" size="12"></html:text></td>
                              </tr>   
                              <tr><td><br/></td></tr>                          
                              <tr>                             
                              	<th width="26%">Rama:</th>
                              	 <td>
									<html:select property="rama" styleId="rama">
									<option value="">
										<bean:message bundle="labels" key="general.todas" />
									</option>
									<html:options collection="ramas" property="id_rama"
										labelProperty="titulo_rama" />
									</html:select>
							     </td>
							  	<th width="26%">Subcategoria:</th>
							  	 <td>
									<html:select property="subcategoria" styleId="subcategoria">
										<option value="">
											<bean:message bundle="labels" key="general.todas" />
										</option>	
										<logic:present name="subcategorias">
											<html:options collection="subcategorias" property="id_subcategoria" labelProperty="titulo_subcategoria" />
										</logic:present>
									</html:select>
							  	 </td>
							  </tr>
							  
                              <tr>
                              	<td colspan="4" align="center">
                              		<html:errors property="datos.fechaDesde"/>
                                    <html:errors property="datos.fechaHasta"/>
                                    <html:errors property="fechas"/>
                              	</td>
                              </tr>
                              <tr>
                              	<td colspan="3" align="center"><br />
                              		<input value="Buscar" type="button" name="buscar" id="buscar"/>
                              	</td>
                              </tr>
                              <tr><td colspan="3">&nbsp;</td></tr>
                            </table>                            
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" id="lista" style="display:noneX">
                           	    <tr  class="listadoSegCab">
                           	    	<td width="9%" align="center">Fecha Creacion</td>
									<td width="9%" align="center">Nombre</td>
									<td width="9%" align="center">Fecha de Nacimiento</td>
									<td width="9%" align="center">RUT</td>									
									<td width="9%" align="center">Direccion</td>
                                    <td width="9%" align="center">Telefono</td>
                                    <td width="9%" align="center">E-mail</td>
                                    <td width="9%" align="center">Rama</td>
                                    <td width="9%" align="center">Producto</td>
                                    <td width="9%" align="center">Tipo Vehiculo</td>
                                    <td width="9%" align="center">Marca</td>
                                    <td width="9%" align="center">Modelo</td>
                                    <td width="9%" align="center">A�o Vehiculo</td>
                              </tr>
                              
                              <logic:present name="informe">
                              <logic:iterate id="informeVehiculo" name="informe" scope="request" indexId="index">
                              
                              <bean:define id="fontcolor" value="000000" scope="page"/>
                              <logic:equal value="6" name="informeVehiculo" property="estadosolicitud">
                              	<bean:define id="fontcolor" value="red" scope="page"/>
                              </logic:equal>
                              <tr>
								 <td valign="top" align="left" nowrap="nowrap"><bean:write name="informeVehiculo" property="fechacreacion" format="dd-MM-yyyy"/></td>                                 
                                 <td valign="top" align="left"><bean:write name="informeVehiculo" property="nombrecliente"/></td>
                                 <td valign="top" align="left"><bean:write name="informeVehiculo" property="fechanacimiento" format="dd-MM-yyyy"/></td>
                                 <td valign="top" align="left"><bean:write name="informeVehiculo" property="rutcliente"/></td>
                                 <td valign="top" align="left"><bean:write name="informeVehiculo" property="direccion"/></td> 
                                 <td valign="top" align="left"><bean:write name="informeVehiculo" property="telefono"/></td>  
                                 <td valign="top" align="left"><bean:write name="informeVehiculo" property="email"/></td> 
                                 <td valign="top" align="left"><bean:write name="informeVehiculo" property="rama"/></td>                                 
                                 <td valign="top" align="left"><bean:write name="informeVehiculo" property="nombreproducto"/></td>
                                 <td valign="top" align="right"><bean:write name="informeVehiculo" property="tipovehiculo"/></td>
                                 <td valign="top" align="left"><bean:write name="informeVehiculo" property="marca"/></td>
                                 <td valign="top" align="left"><bean:write name="informeVehiculo" property="modelo"/></td>
                                 <td valign="top" align="left"><bean:write name="informeVehiculo" property="anniovehiculo"/></td>
                              </tr>
                              </logic:iterate>                                                          
                              <tr>
                              	<th colspan="13" bgcolor="#CCCCCC" align="center">
                                Pagina: <html:select property="datos(numeroPagina)"  name="informeComercialVehiculo" styleId="numeroPagina">
									<html:options collection="listadoPaginas" property="id" labelProperty="descripcion"/>
                                </html:select>
                                </th>
                              </tr>
                              <tr>
                              	<th colspan="13" bgcolor="#CCCCCC">&nbsp;&nbsp;&nbsp;&nbsp;
                                <span class="link_List3"><a href="javascript:exportar();" >Exportar a Excel</a></span></th>
                              </tr>
                              </logic:present>
                              <tr><td colspan="13">&nbsp;</td></tr>
                            </table>
                        </div>
       			 	</td>
    			</tr>				                 
			</table>
         </td>
	</tr>
</table>
<iframe frameborder="0" height="0" width="100" id="iframe-excel" name="iframe-excel" src=""></iframe>
</html:form>
</body>

</html:html>