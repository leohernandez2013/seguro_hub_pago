<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>
	<html:base />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	<title></title>
	<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="../../js/jquery-tinet.js"></script>
	<link rel="stylesheet" type="text/css" href="../../css/style_seguros.css" />
	<link href="css/colorbox.css" rel="stylesheet" type="text/css" />	
	<script type="text/javascript" src="js/jquery/jquery.colorbox.js"></script>
	
	
<script type="text/javascript">
	var subir_archivo = -1;
	var subir_cargaRut = -1;
	var dataruts = '';

	function uploadfile(archivo){
		try {
			if( $("#"+archivo).attr('value') != "") { 
				valor_subir(archivo, 1);
				$.ajaxFileUpload( {
					url:'subir-archivo.do?tipo='+archivo+'&action=nuevo', 
					secureuri:false, 
					fileElementId: archivo, 
					dataType: 'json', 
					success: function (data, status) {
						updloadFinished(archivo, data);
					},
					error: function (data, status, e) {
						jAlert(data.mensaje);
						updloadFinished(archivo, data); 
					}
				});
			}
		} catch (e) {
			alert(e.message);
		}
	} 
	
	function updloadFinished(archivo, data){
		if(data.estado == 'success') {
			//$("#"+archivo).attr('disabled', true);
			$("#"+archivo+"_div").html('<bean:message key="nueva-ficha-producto.archivoListo" />');
			$("#"+archivo+"_div").show();
		} else if (data.estado  == 'nofile') {
			$("#"+archivo+"_div").html('<bean:message key="nueva-ficha-producto.noSubioArchivo" />');
			$("#"+archivo+"_div").show();		
		} else {
			$("#"+archivo+"_div").html(data.estado + ': ' + data.mensaje);
			$("#"+archivo+"_div").show();
		}

		if(data.ruts != null) {
			dataruts = data.ruts;
		}

		valor_subir(archivo, -1);
		check_termino();
	}
	
</script>
</head>

<body>
	<html:form action="/carga-rut.do" styleId="formulario" method="post" enctype="multipart/form-data">
	
		<table cellpadding="0" cellspacing="0" border="0" width="800" align="center" class="datos">
			<tbody>
				<tr>
				<td>Archivo:</td>
				<span id="rut_arch"> <input type="file" name="cargaRut"
										id="archivo" /> </span>
				</tr>
				<tr>
				<td><html:button property="boton" value="Aceptar" onclick="javascript:submit();"></html:button></td>
				</tr>				
			</tbody>
		</table>
	</html:form>
</body>
</html:html>
