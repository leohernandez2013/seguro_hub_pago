<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>

<bean:define id="contextpath" value="<%=request.getContextPath()%>" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html>
	<head>
		

		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<title>:::::: BACK OFFICE ::::::</title>
		

	   <link rel="stylesheet" type="text/css" href="../css/style_seguros.css" />
	   <link type="text/css" rel="stylesheet" href="../css/jquery-ui.css" />
	   
		<script type="text/javascript" src="../js/jquery-1.4.2.js"></script>
		<script type="text/javascript" src="../js/jquery-ui-1.8.4.custom.min.js"></script>
		<script src="../js/jquery-tinet.js" type="text/javascript"></script>
		<script type="text/javascript">
		
		function exportar() {
			try {
				$("#excel").attr("value", 1);
				$("#informeComercialVehiculo").attr("target", "iframe-excel");
				$("#informeComercialVehiculo").submit();	
			} catch (e) {
				alert(e.message);
			}
		}
		
		$(document).ready(function(){
		
			$("#inputFechaDesde, #inputFechaHasta").keypress(function(){
				return false;
			});
			
			var dates = $("#inputFechaDesde, #inputFechaHasta").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "../images/calendar.gif",
					buttonImageOnly: true,
					dateFormat: 'dd/mm/yy',
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
						'Junio', 'Julio', 'Agosto', 'Septiembre',
						'Octubre', 'Noviembre', 'Diciembre'],
					monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
						'May', 'Jun', 'Jul', 'Ago',
						'Sep', 'Oct', 'Nov', 'Dic'],
					onSelect: function( selectedDate ) {
						var option = this.id == "inputFechaDesde" ? "minDate" : "maxDate",
						instance = $( this ).data( "datepicker" );
						date = $.datepicker.parseDate(
							instance.settings.dateFormat ||
							$.datepicker._defaults.dateFormat,
							selectedDate, instance.settings );
						dates.not( this ).datepicker( "option", option, date );
					}
			});
			$("#numeroPagina").change(function(){
				$("#excel").attr("value", 0);
				$("#informeComercialVehiculo").attr("target", "");
				$("#informeComercialVehiculo").submit();
			});
			
			$("#buscar").click(function () {
				try {
					$("#excel").attr("value", 0);
					$("#informeComercialVehiculo").attr("target", "");
					$("#informeComercialVehiculo").submit();	
				} catch (e) {
					alert(e.message);
				}
				return false;
			});
			$("a[name='factura_informe']").click(function(){
				 
				 $(this).colorbox({
			     href: $(this).attr("href"), 
			     photo: true
			     });
			});
			colores('lista');
		});
		</script>

	</head>

<body>

<table cellpadding="0" cellspacing="0" border="0" width="800" align="center" class="datos" >
	<tr>

		<td>
            <table cellpadding="0" cellspacing="0" border="0"  width="100%"  align="center" >
				<tr valign="top">
					<td height="33" >Informe Validacion de Compra con Tarjeta M�s y Transbank<br/></td>
			  </tr>
    			<tr >
    				<td>
        				<div >
							<form action="<bean:write name="contextpath"/>/secure/obtener-informe-tarjetas.do" method="post" name="informeComercialVehiculo" id="informeComercialVehiculo">
							<html:hidden property="datos(excel)" name="informeComercialTarjetas" styleId="excel" value="0"/>
                        	<table cellpadding="0" cellspacing="0" border="0" width="90%" align="center">
                            	<tr>
                                	<th width="26%" >Fecha Desde: </th>
                                    <td><html:text property="datos(fechaDesde)" name="informeComercialVehiculo" styleId="inputFechaDesde" size="12"></html:text></td>
                                    <th width="26%" >Fecha Hasta: </th>
                                    <td><html:text property="datos(fechaHasta)" name="informeComercialVehiculo" styleId="inputFechaHasta" size="12"></html:text></td>
                              </tr>
                              
                              <tr>
                              	<td colspan="4" align="center">
                              		<html:errors property="datos.fechaDesde"/>
                                    <html:errors property="datos.fechaHasta"/>
                                    <html:errors property="fechas"/>
                              	</td>
                              </tr>

                              <tr>
                              	<td colspan="3" align="center"><br />
                              		<input value="Buscar" type="button" name="buscar" id="buscar"/>
                              	</td>
                              </tr>
                              <tr><td colspan="3">&nbsp;</td></tr>
                            </table>
                            
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" id="lista" style="display:noneX">
                           	    <tr  class="listadoSegCab">
									<td width="9%" align="center">Id</td>

								  	<td width="6%" align="center">N�P�liza</td>
									<td width="6%" align="center">Nombre</td>
									<td width="7%" align="center">Tel�fono</td>
									<td width="10%" align="center">E-mail</td>
                                    <td width="10%" align="center">Fecha</td>
                                    <td width="10%" align="center">Rut</td>

                                    <td width="10%" align="center">Producto</td>
                                    <td width="10%" align="center">Prima UF</td>
                                    <td width="12%" align="center">Tarjeta</td>
                                    <td width="12%" align="center">C�digo de Autorizacion</td>
                                    <td width="12%" align="center">D�gitos Tarjeta</td>
                                    <td width="10%" align="center">Proporcional</td>
                              </tr>
                              
                              <logic:present name="informe">
                              <logic:iterate id="informeTarjetas" name="informe" scope="request" indexId="index">
                              
                              <bean:define id="fontcolor" value="000000" scope="page"/>
                              <logic:equal value="6" name="informeTarjetas" property="estadosolicitud">
                              	<bean:define id="fontcolor" value="red" scope="page"/>
                              </logic:equal>
                              <tr>
								 <td valign="top" align="left"><font style="color: <%=fontcolor %>"><bean:write name="informeTarjetas" property="idseguro"/></font></td>
                                 <td valign="top" align="left"><font style="color: <%=fontcolor %>"><bean:write name="informeTarjetas" property="numeropoliza"/></font></td>
                                 <td valign="top" align="left"><bean:write name="informeTarjetas" property="nombrecliente"/></td>
                                 <td valign="top" align="left"><bean:write name="informeTarjetas" property="fonocliente"/></td>
                                 <td valign="top" align="left"><bean:write name="informeTarjetas" property="emailcliente"/></td>
                                 <td valign="top" align="left"><bean:write name="informeTarjetas" property="fechaseguro" format="dd-MM-yyyy"/></td>
                                 <td valign="top" align="left" nowrap="nowrap"><bean:write name="informeTarjetas" property="rutcliente"/></td>
                                 <td valign="top" align="left"><bean:write name="informeTarjetas" property="nombreproducto"/></td>
                                 <td valign="top" align="right"><bean:write name="informeTarjetas" property="primauf"/></td>
                                 <td valign="top" align="left"><bean:write name="informeTarjetas" property="tipotarjeta"/></td>
                                 <td valign="top" align="right"><bean:write name="informeTarjetas" property="codigoautorizacion"/></td>
                                 <td valign="top" align="right"><bean:write name="informeTarjetas" property="digitostarjeta"/></td>
                                 <td valign="top" align="right"><bean:write name="informeTarjetas" property="proporcional"/></td>
                              </tr>

                              </logic:iterate>
                              
                              
                              <tr>
                              	<th colspan="13" bgcolor="#CCCCCC" align="center">
                                Pagina: <html:select property="datos(numeroPagina)"  name="informeComercialVehiculo" styleId="numeroPagina">
									<html:options collection="listadoPaginas" property="id" labelProperty="descripcion"/>
                                </html:select>
                                </th>
                              </tr>
                              <tr>
                              	<th colspan="13" bgcolor="#CCCCCC">&nbsp;&nbsp;&nbsp;&nbsp;

                                <span class="link_List3"><a href="javascript:exportar();" >Exportar a Excel</a></span></th>
                              </tr>
                              </logic:present>
                              <tr><td colspan="13">&nbsp;</td></tr>
                            </table>
                            </form>
                        </div>
       			 	</td>
    			</tr>
				
                 
			</table>

         </td>
	</tr>
</table>
<iframe frameborder="0" height="0" width="100" id="iframe-excel" name="iframe-excel" src=""></iframe>
</body>

</html:html>