<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="org.apache.struts.Globals"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>
	<html:base />

	<title>Administrar Subcategoria</title>

	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	
	<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="../../js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../../js/ajaxfileupload.js"></script>
	<script type="text/javascript" src="../../js/jquery.alerts.js"></script>
	<script type="text/javascript"
		src="../../js/jquery-ui-1.8.4.custom.min.js"></script>
	<script type="text/javascript" src="../../js/jquery.colorbox.js"></script>
	<script type="text/javascript" src="../../js/jquery.form.js"></script>

	<link rel="stylesheet" type="text/css" href="../../css/colorbox.css" />

	<link rel="stylesheet" type="text/css"
		href="../../css/jquery.alerts.css" />
	<link rel="stylesheet" type="text/css"
		href="../../css/smoothness/jquery-ui-1.8.4.custom.css" />
	<link rel="stylesheet" type="text/css"
		href="../../css/style_seguros.css" />

	<link type="text/css" rel="stylesheet" href="../../css/jquery-ui.css" />
	<link href="../../js/jquery.alerts.css" rel="stylesheet"
		type="text/css" media="screen" />
	<link rel="stylesheet" type="text/css"
		href="../../css/style_seguros.css" />
		

	<script type="text/javascript" charset="utf-8">
	
	function errores() {
		var errores = "<html:errors />";
		despl_err(errores);
		
		errores = "<bean:write name='<%=Globals.EXCEPTION_KEY%>' property='message' ignore='true' />";
		despl_err(errores);
	}
	
	function despl_err(errores){
		if(errores != "") {
			jAlert(errores, "Error");
		}
	}
	
	function loadComboTipos(obj){
		$.getJSON("/vseg-paris-adm/buscar-rama-tipos.do",{idRama: obj.val(), ajax: 'true'}, function(j){
			$('select#tipo option').remove();
			var options = '';
			options += '<option value=""><bean:message bundle="labels" key="general.seleccione" /></option>';
			if(j.length > 0){
				$('#labelTipo').attr('style','display:inline;');
				$('#selectTipo').attr('style','display:inline;');
				for (var i = 0; i < j.length; i++) {
					options += '<option value="' + j[i].id_tipo + '">' + j[i].descripcion_tipo + '</option>';
			    }
			    $('select#tipo').html(options);
			    $('select#subcategoria option').remove();
			    $('select#subcategoria').html('<option value="0"><bean:message bundle="labels" key="general.seleccione" /></option>');
			}
			else{
				$('#selectTipo').attr('style','display:none;');
				$('#labelTipo').attr('style','display:none;');
				$('select#tipo option').remove();
			}
		})
	}
	
	$(document).ready( function(){
		if($('#rama').val() != 0){
			loadComboTipos($('#rama'));
		}
		$("select#rama").change(function(){
			loadComboTipos($(this));
		});
	});
	
	</script>

  </head>
  
  <body onload="javascript:errores();">
  <html:form action="/agregar-subcategoria" method="post" target="_self">
  
  
    <table cellpadding="0" cellspacing="0" border="0" align="center" class="datos">
		<tr>
			<td width="110">
				<bean:message bundle="labels" key="administrar-subcategoria.rama"/>
			</td>
			<td width="150">
				<html:select property="datos(rama)" styleId="rama">
						<option value=""><bean:message bundle="labels" key="general.seleccione" /></option>
						<html:options collection="ramas" property="id_rama" labelProperty="titulo_rama" />
				</html:select>
			</td>
			<td width="170">
				<html:errors property="datos.rama" />
			</td>
		</tr>
		<tr>
			<td >
				<logic:present name="datos(tipo)" scope="request">
					<div id="labelTipo" style="display:inline;">
				</logic:present>
				<logic:notPresent name="datos(tipo)" scope="request">
					<div id="labelTipo" style="display:none;">
				</logic:notPresent>
				<bean:message bundle="labels" key="administrar-subcategoria.tipo"/>
				</div>
			</td>
			<td >
			
				<logic:messagesNotPresent property="datos.tipo">
					<div id="selectTipo" style="display:none;">
				</logic:messagesNotPresent>
				<logic:messagesPresent property="datos.tipo">
					<div id="selectTipo" style="display:inline;">
				</logic:messagesPresent>
				<html:select property="datos(tipo)" styleId="tipo">
				</html:select>
				</div>
			</td>
			<td >
				<html:errors property="datos.tipo" />
			</td>
		</tr>
		<tr>
			<td>
				<bean:message bundle="labels" key="administrar-subcategoria.titulo"/>
			</td>
			<td>
				<html:text property="datos(subcategoria)" maxlength="50" size="30"></html:text>
			</td>
			<td>
				<html:errors property="datos.subcategoria" />
			</td>
		</tr>
		<tr>
			<td align="center" colspan="3">
				<input type="submit" id="agregar" name="agregar" value="<bean:message bundle="labels" key="general.agregar"/>" />
				<input type="button" id="cerrar" name="cerrar" value="<bean:message bundle="labels" key="general.cerrar"/>" onclick="javascript:window.close();"/>
			</td>
		</tr>
	</table>
	</html:form>
	<logic:present name="idSubcategoria">
	<div>
		<table cellpadding="0" cellspacing="0" border="0" width="450"
			align="center" class="datos">
			<tr>
			<td align="center">
				<bean:message bundle="labels" key="administrar-subcategoria.ingresada"/>
			</td>
			</tr>
		</table>
	</div>
	</logic:present>
  </body>
</html:html>
