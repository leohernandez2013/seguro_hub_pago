<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>
	<html:base />

	<title>modificar-pagina-intermedia.jsp</title>

	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="-1" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="../../js/jquery-tinet.js"></script>
	<script type='text/javascript'
		src="../../js/bsn.AutoSuggest_2.1.3_comp.js"></script>
	<script type="text/javascript" src="../../js/jquery.alerts.js"></script>
	<script type="text/javascript"
		src="../../js/jquery-ui-1.8.4.custom.min.js"></script>
	<script type="text/javascript" src="../../js/jquery.colorbox.js"></script>

	<link rel="stylesheet" type="text/css"
		href="../../css/style_seguros.css" />
	<link rel="stylesheet" type="text/css"
		href="../../css/autosuggest_inquisitor.css" />
	<link rel="stylesheet" type="text/css"
		href="../../css/jquery.alerts.css" />
	<link rel="stylesheet" type="text/css"
		href="../../css/smoothness/jquery-ui-1.8.4.custom.css" />

	<script type="text/javascript" charset="utf-8">
		function eliminar(idProductoLeg) {
			$("#formulario").attr("target", "");
			$("#formulario").attr("ACTION","/vseg-paris-adm/eliminar-producto.do");
			$("#formulario").attr("action","/vseg-paris-adm/eliminar-producto.do");
			$("#id_producto_leg").attr("value",idProductoLeg);
			$("#formulario").submit();
		}
	</script>
	<script type="text/javascript" charset="utf-8">
	
		$(document).ready(function() {
		
 			$("#lista").ready(function() {
		  		var idSubcategoria = $("#id_subcategoria").attr("value");
	 			$.getJSON("/vseg-paris-adm/buscar-productos.do", {idSubcategoria: idSubcategoria, ajax : 'true'}, function(j){
	 			$('#lista tbody').remove();
	 			var html = '<tbody>';
					for (var i = 0; i < j.length; i++) {
						html += '<tr>'
						html += '<td align="center">' + j[i].nombre + '</td>';
						html += '<td align="center"><img src="../../images/not.gif" style="cursor:pointer" onClick="eliminar(' + j[i].id_producto + ')" /></td>';							
						html += '</tr>';
					}
					html += "<tr><th  colspan='2' bgcolor='#CCCCCC'>&nbsp;</th></tr>";
					html += '</tbody>';
					$('#lista').append(html);
					colores('lista');
					$("#id_producto_leg").attr("value","");
				})	
			})
			
  			$("#agregar").click(function() {
  				if($("#id_producto_leg").attr("value") > 0) {
					$("#formulario").attr("target", "");
	  				$("#formulario").attr("ACTION","/vseg-paris-adm/agregar-producto.do");
	  				$("#formulario").attr("action","/vseg-paris-adm/agregar-producto.do");
	  				$("#formulario").submit();
	  				return false;
  				} else {
  					alert('<bean:message key="modificar-pagina-intermedia.debeSeleccionarProducto" />');
  				}
  			})

  			$("#cerrar").click(function() {
  				self.close();
  			})

  			$("#aceptar").click(function() {
				$("#formulario").attr("target", "frameOculto");
  				$("#formulario").attr("ACTION","/vseg-paris-adm/actualizar-subcategoria.do");
  				$("#formulario").attr("action","/vseg-paris-adm/actualizar-subcategoria.do");
  				$("#formulario").submit();
  				return false;
  			})
	  	});
	
	</script>
</head>

<html:form action="/modificar-pagina-intermedia" styleId="formulario">
	<body>
		<div style="width: 490px; height: 290px;">
			<table cellpadding="0" cellspacing="0" border="0" width="100%"
				height="290" align="center" style="margin-top: 5px;" class="datos">
				<tr>
					<th colspan="4">
						<div align="left">
							<img src="../../images/modif.gif" />
							<bean:message bundle="labels" key="general.modificar" />
						</div>
					</th>
				</tr>
				<tr>
					<th width="26%">
						<bean:message bundle="labels"
							key="modificar-pagina-intermedia.subcategoria" />
						:
					</th>
					<td width="23%">
						<html:text property="titulo_subcategoria" styleId="titulo_subcategoria"/>
						<html:hidden property="id_subcategoria" styleId="id_subcategoria" />
					</td>
					<th width="20%">
						<bean:message bundle="labels" key="general.estado" />
						:
					</th>
					<td width="31%">
						<html:select property="es_eliminado">
							<html:options collection="estados" property="label" labelProperty="value"/>
						</html:select>
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<table cellpadding="0" cellspacing="0" border="0" width="100%"
							align="center" id="lista" style="margin-top: 10px;">
							<thead>
								<tr class="listadoSegCab">
									<td width="80%" align="center">
										<bean:message bundle="labels"
											key="modificar-pagina-intermedia.nombreProducto" />
									</td>
									<td width="20%" align="center">
										<bean:message bundle="labels"
											key="modificar-pagina-intermedia.eliminar" />
									</td>
								</tr>
							</thead>
						</table>
					</td>
				</tr>
				<tr>
					<th>
						<bean:message bundle="labels"
							key="modificar-pagina-intermedia.agregarProducto" />
						:
					</th>
					<td colspan="3">
						<html:hidden property="id_producto_leg" styleId="id_producto_leg" />
						<input type="text" name="txt_producto" id="txt_producto" />
						<input type="button"
							value="<bean:message bundle="labels" key="general.agregar" />"
							id="agregar" />
					</td>

				</tr>

				<tr>
					<th colspan="4">
						<input type="button"
							value="<bean:message bundle="labels" key="general.cerrar" />"
							id="cerrar" />
						<input type="button"
							value="<bean:message bundle="labels" key="general.aceptar" />"
							id="aceptar" />
					</th>
				</tr>

			</table>
		</div>

	</body>
</html:form>
<script type="text/javascript" charset="utf-8">
	var options = {
		script:"/vseg-paris-adm/buscar-productos-disponibles.do?idSubcategoria=" + document.getElementById('id_subcategoria').value + "&",
		varname:"nombre",
		json:true,
		shownoresults:true,
		maxentries:6,
		callback: function (obj) { document.getElementById('id_producto_leg').value = obj.id; }
	};
	var as_json = new bsn.AutoSuggest('txt_producto', options);

</script>
	<iframe id="frameOculto" frameborder="0" width="500" height="0"
		src="" name="frameOculto">
	</iframe>
</html:html>
