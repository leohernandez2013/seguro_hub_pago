<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="org.apache.struts.Globals"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Exito</title>
	<script type="text/javascript" charset="utf-8">
	
	var error = '<bean:write name="<%=Globals.EXCEPTION_KEY%>" property="message" ignore="true" />';
	if(error != '') {
		parent.jAlert(error, "<bean:message key="general.errores"/>");		
	} else {
		var errores = "<html:errors />";
		if(errores != "") {
			parent.jAlert(errores, "<bean:message key="general.errores"/>");
		} else {
			parent.jAlert('<bean:message bundle="labels" key="mantenedor.usuario.titulo.crear_usuario_interno.exito"/>', "<bean:message key="general.exito"/>");
		}
	}
	</script>

</head>
<body>

<h1> <bean:message bundle="labels" key="mantenedor.usuario.titulo.crear_usuario_interno.exito"/> </h1>

</body>
</html>