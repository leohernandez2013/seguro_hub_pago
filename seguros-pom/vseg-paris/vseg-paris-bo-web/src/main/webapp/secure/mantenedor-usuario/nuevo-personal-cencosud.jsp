<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>
	<html:base />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	<title>Insert title here</title>
	<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
	<script type="text/javascript" src="../../js/ajaxfileupload.js"></script>
	<script type="text/javascript" src="../../js/jquery.alerts.js"></script>
	<script type="text/javascript"
		src="../../js/jquery-ui-1.8.4.custom.min.js"></script>
	<script type="text/javascript" src="../../js/jquery.colorbox.js"></script>
	<link rel="stylesheet" type="text/css" href="../../css/colorbox.css" />
	<link rel="stylesheet" type="text/css"
		href="../../css/jquery.alerts.css" />
	<link rel="stylesheet" type="text/css"
		href="../../css/smoothness/jquery-ui-1.8.4.custom.css" />
	<link rel="stylesheet" type="text/css"
		href="../../css/style_seguros.css" />

	<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		var errores = "<html:errors />";
		if(errores != "") {
			jAlert(errores, "<bean:message key="general.errores"/>");
		}
		
		$("#crear").click(function() {
			$("#formulario").attr("target","frameOculto");
			$("#formulario").attr("ACTION","/vseg-paris-adm/secure/mantenedor-usuario/nuevoPersonalCencoGuardar.do");
			$("#formulario").attr("action","/vseg-paris-adm/secure/mantenedor-usuario/nuevoPersonalCencoGuardar.do");
			$("#formulario").submit();
		});
		
		$("#cancelar").click(function() {
			$("#formulario").attr("target","");
			$("#formulario").attr("ACTION","/vseg-paris-adm/secure/mantenedor-usuario/inicio.do");
			$("#formulario").attr("action","/vseg-paris-adm/secure/mantenedor-usuario/inicio.do");
			$("#formulario").submit();
		});
	});
	</script>
</head>
<body>
	<html:form action="/nuevoPersonalCencoGuardar" styleId="formulario">
		<table>
			<tr>
				<td>
					<bean:message bundle="labels" key="mantenedor.usuario.nombre" />
				</td>
				<td>
					<input type="text" name="datos(userName)" />
				</td>
			</tr>
			<tr>
				<td>
					<bean:message bundle="labels" key="mantenedor.usuario.email" />
				</td>
				<td>
					<input type="text" name="datos(email)" />
				</td>
			</tr>
			<tr>
				<td>
					<bean:message bundle="labels" key="mantenedor.usuario.perfiles" />
				</td>
				<td>
					<html:select property="datos(combo)">
						<option value="">
							Seleccione una opcion
						</option>
						<html:options collection="roles" property="id"
							labelProperty="nombre" />
					</html:select>

				</td>
			</tr>
			<tr>
				<td>
					<input type="button" name="crear" id="crear"
						value="<bean:message bundle="labels" key="mantenedor.usuario.boton_crear" />" />
				</td>
				<td>
					<input type="button" name="cancelar" id="cancelar"
						value="<bean:message bundle="labels" key="mantenedor.usuario.boton_cancelar" />" />
				</td>
			</tr>
		</table>
	</html:form>
	<iframe name="frameOculto" id="frameOculto" frameborder="0" src=""
		height="0" width="500" />
</body>
</html:html>