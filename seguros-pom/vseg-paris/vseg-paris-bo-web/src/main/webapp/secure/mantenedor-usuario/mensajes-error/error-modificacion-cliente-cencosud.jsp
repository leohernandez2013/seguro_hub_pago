<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="org.apache.struts.Globals"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>
	<html:base />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	<title>Insert title here</title>

	<script type="text/javascript" charset="utf-8">
	
	var error = '<bean:write name="<%=Globals.EXCEPTION_KEY%>" property="message" ignore="true" />';
	if(error != '') {
		parent.jAlert(error, "<bean:message key="general.errores"/>");		
	} else {
		var errores = "<html:errors />";
		if(errores != "") {
			parent.jAlert(errores, "<bean:message key="general.errores"/>");
		}
	}
	</script>
</head>
<body>
	<html:errors />
	<h1>
		<bean:message bundle="labels"
			key="mantenedor.usuario.titulo.actualizar.error" />
	</h1>
</body>
</html:html>