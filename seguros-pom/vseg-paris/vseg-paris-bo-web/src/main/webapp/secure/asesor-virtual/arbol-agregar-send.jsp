<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html:html lang="true">
<head>
	<html:base />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
	<meta http-equiv="description" content="This is my page" />
	<title>proxy</title>
	<script type="text/javascript" src="../../js/jquery-1.4.2.js"></script>
	<link rel="stylesheet" type="text/css"
		href="../../css/style_seguros.css" />
	<script type="text/javascript" charset="utf-8">
	
		var errores = "<html:errors />";
		if(errores != "") {
			try {
				parent.ventana.jAlert(errores, "<bean:message key="general.errores"/>");
				parent.ventana.$("#guardar").attr('disabled', false);
			} catch(e) {
				alert(e.message);
			}
		}

		<logic:notEmpty name="pregunta">
			try{
				parent.agregado("La pregunta ha sido agregada.");
				parent.ventana.close();
			} catch (e) {
				alert(e.message);
			}
		</logic:notEmpty>

		<logic:notEmpty name="resultado">
			try{
				parent.agregado("El resultado ha sido agregado.");
				parent.ventana.close();
			} catch (e) {
				alert(e.message);
			}
		</logic:notEmpty>
	</script>
</head>
<body>
</body>
</html:html>
