<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="css/style_seguros.css" />
		
		<script type="text/javascript" src="js/jquery-1.4.2.js"></script>
		<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
		<script src="js/jquery.colorbox.js" type="text/javascript"></script>
		
		<title>:::::: BACK OFFICE ::::::</title>
		<style type="text/css">
.box {
	width: 144px;
	background: #f1f3f2;
	margin-bottom: 20px;
	padding: 10px 10px 1px 10px;
}

.box h2 {
	font-size: 11px;
	font-weight: normal;
	color: #fff;
	background: #5fc2f1;
	margin: -10px -10px 0 -10px;
	padding: 6px 12px;
}

.linkMenu a {
	font-size: 12px;
	color: #3366CC;
	text-decoration: none;
}

.linkMenu a:hover {
	font-size: 12px;
	color: #003366;
	text-decoration: underline;
}

.linkMenu a:visited {
	font-size: 12px;
	color: #003366;
}

.linkMenu a:active {
	font-size: 12px;
	color: #003366;
	text-decoration: underline;
}
</style>
	</head>

	<body>
		<table cellpadding="0" cellspacing="0" border="0" width="1000">
			<tr>
				<td class="listadoSegCab" colspan="2" height="20">
					back office
				</td>
			</tr>
			<tr>
				<td width="144" valign="top">
					<table cellpadding="4" cellspacing="4" border="0" width="160"
						height="336">
						<tr>
							<td width="100%" valign="top">
								<div class="box">
									<h2>
										Administrador
									</h2>
									<br />
									<span class="linkMenu"><html:link
											action="/ficha-subcategoria" target="contenidoback"
											module="/secure"> + Ficha Sub Categoria</html:link> </span>
									<br />
									<span class="linkMenu"><a href="#"> + Promociones</a></span>
									<br/>
									<span class="linkMenu">&nbsp;&nbsp;&nbsp;<html:link
											action="/administrador-promociones" module="/secure"
											target="contenidoback"> -
											Promociones Principal </html:link> </span>
									<br />
									<span class="linkMenu">&nbsp;&nbsp;&nbsp;<html:link
											action="/administrador-promociones-rama" module="/secure"
											target="contenidoback"> -
											Promociones Rama </html:link> </span>
									<br />
									<span class="linkMenu"><a href="#">  + Comparativas</a>
									</span>
									<br />
									<span class="linkMenu"> <html:link action="/inicio"
											module="/secure/mantenedor-usuario" target="contenidoback"> + Seguridad</html:link>
									</span>
									<br />
									<span class="linkMenu"><html:link
											action="/filtrarEvento" module="/secure/mantenedor-eventos"
											target="contenidoback"> + Eventos</html:link> </span>
									<br />
									<span class="linkMenu"><a href="#"> + Comerciales</a></span>
									<br/>
									<span class="linkMenu">&nbsp;&nbsp;&nbsp;<html:link
											action="/informe-vehiculo" module="/secure"
											target="contenidoback"> -
											Veh�culos </html:link> </span>
									<br />
									<span class="linkMenu">&nbsp;&nbsp;&nbsp;<html:link
											action="/informe-tarjetas" module="/secure"
											target="contenidoback"> -
											Tarjetas </html:link> </span>
									<br />
									<span class="linkMenu"><a href="#"> + Informes </a> </span>
									<br />
									<span class="linkMenu">&nbsp;&nbsp;&nbsp;<html:link
											action="/informe-cotizaciones" module="/secure"
											target="contenidoback"> -
											Cotizaciones </html:link> </span>
									<br />
									<span class="linkMenu">&nbsp;&nbsp;&nbsp;<html:link
											action="/informe-ventas" module="/secure"
											target="contenidoback"> -
											Ventas </html:link> </span>
									<br />
																		
									<span class="linkMenu"><html:link
											action="/informe-vitrineo-vehiculo" module="/secure"
											target="contenidoback"> + Vitrineo</html:link> </span>
									<br />
									<span class="linkMenu"><a href="#"> + Google
											Analytics</a> </span>
									<br />
									<span class="linkMenu"><html:link
											action="/asesor-virtual" module="/secure"
											target="contenidoback"> + Asesor Virtual</html:link> </span>
									<br />
									<span class="linkMenu"><html:link
											action="/administrar-pagina-intermedia" module="/secure"
											target="contenidoback"> + P�gina Intermedia</html:link> </span>
									<br />
									<span class="linkMenu"><html:link
											action="/administrar-plan-inicio" module="/secure"
											target="contenidoback"> + Planes</html:link> </span>
									<br />
									<span class="linkMenu"><html:link
											action="/etl-inicio" module="/secure"
											target="contenidoback"> + ETL</html:link> </span>
									<br />
									<span class="linkMenu"><html:link
											action="/carga-rut" module="/secure"
											target="contenidoback"> + Carga de Rut</html:link> </span>
									<br />
									
									<span class="linkMenu"><a href="#"> + Comparador de Planes </a> </span>
									<br />
									<span class="linkMenu">&nbsp;&nbsp;&nbsp;<html:link
											action="/desplegarAdmComprador.do?ramaSelected=1" module="/secure/adm-comparador"
											target="contenidoback"> -
											Veh�culo </html:link> </span>
									<br />
									<span class="linkMenu">&nbsp;&nbsp;&nbsp;<html:link
											action="/desplegarAdmComprador.do?ramaSelected=2" module="/secure/adm-comparador"
											target="contenidoback"> -
											Hogar </html:link> </span>
									<br />
									<span class="linkMenu">&nbsp;&nbsp;&nbsp;<html:link
											action="/desplegarAdmComprador.do?ramaSelected=3" module="/secure/adm-comparador"
											target="contenidoback"> -
											Vida </html:link> </span>
									<br />
									<span class="linkMenu">&nbsp;&nbsp;&nbsp;<html:link
											action="/desplegarAdmComprador.do?ramaSelected=4" module="/secure/adm-comparador"
											target="contenidoback"> -
											Salud </html:link> </span>
									<br />
									<span class="linkMenu">&nbsp;&nbsp;&nbsp;<html:link
											action="/desplegarAdmComprador.do?ramaSelected=5" module="/secure/adm-comparador"
											target="contenidoback"> -
											Cesant�a </html:link> </span>
									<br />
									<span class="linkMenu">&nbsp;&nbsp;&nbsp;<html:link
											action="/desplegarAdmComprador.do?ramaSelected=6" module="/secure/adm-comparador"
											target="contenidoback"> -
											Fraude </html:link> </span>
									<br />
									
									<br />
									<span class="linkMenu"><a href="/vseg-paris-adm/inicio.do"> [ Inicio ]</a> </span>
									<br />
									<br />
									&nbsp;
								</div>
							</td>
						</tr>
					</table>
				</td>
				<td width="880" valign="top">
					<!--INICIO iframe de contenido-->
					<iframe src="<html:rewrite action="/inicio-login" />" width="880"
						height="600" frameborder="0" scrolling="auto"
						name="contenidoback" id="contenido" />
					<!--TERMINO iframe de contenido-->
				</td>
			</tr>
		</table>
	</body>
</html>
