
	$(function() {
		colores = function( tr_name ){
			$("#" + tr_name + " tr:odd").addClass('listadoSegImpar');
			$("#" + tr_name + " tr:not([th])odd").addClass('listadoSegPar');
		}
	});
	

    function getUrlCorta(url) {
		try {
	    	var res = "";
			if(url != ""){
				res = url.substring(0, url.indexOf(".do")) + ".do";
			}
		}catch(e) {
			e.message;
		}
		return res;
    }

    
	$(document).ready(function() {
		  
		jQuery(document).ajaxError(function(event, request, settings){
			try {
				if(request.status == 403) {
					jAlert(getUrlCorta(settings.url), 'Usuario sin permisos');
				} else {
					jAlert(request.responseText, 'Error');
				}
			}catch(e) {
				e.message;
			}
		});
	})