alter table FICHA
   drop constraint FK_FICHA_REFERENCE_PLAN_LEG;

alter table FICHA
   drop constraint FK_FICHA_REFERENCE_SUBCATEG;

alter table BENEFICIO_FICHA
   drop constraint FK_BENEFICI_REFERENCE_FICHA;

alter table IMAGEN_FICHA
   drop constraint FK_IMAGEN_F_REFERENCE_FICHA;

alter table COBERTURA_FICHA
   drop constraint FK_COBERTUR_REFERENCE_FICHA;

alter table EXCLUSION_FICHA
   drop constraint FK_EXCLUSIO_REFERENCE_FICHA;

alter table ASPECTO_LEGAL_FICHA
   drop constraint FK_ASPECTO__REFERENCE_FICHA;

drop index FICHA_PORPRODUCTO;

drop index FICHA_PORPLAN;

drop index FICHA_PORTIPOYPAIS;

drop table FICHA cascade constraints;

/*==============================================================*/
/* Table: FICHA                                                 */
/*==============================================================*/
create table FICHA  (
   ID_FICHA             NUMBER(8)                       not null,
   ID_SUBCATEGORIA      NUMERIC(12),
   CODIGO_PLAN_LEG      VARCHAR2(50),
   NOMBRE_COMERCIAL     VARCHAR2(150)                   not null,
   DESCRIPCION_FICHA    VARCHAR2(255),
   DESCRIPCION_PAGINA   varchar2(150),
   ES_SUBCATEGORIA      NUMBER(19)                      not null,
   ESTADO_FICHA         NUMBER(1)                       not null,
   MONTO_UF             FLOAT                           not null,
   ID_PAIS              NUMBER(8)                       not null,
   FECHA_CREACION       DATE                            not null,
   FECHA_MODIFICACION   DATE                            not null,
   constraint PK_FICHA primary key (ID_FICHA)
);

comment on table FICHA is
'Corresponde a la tabla que almacenar� los datos de la ficha de plan en promoci�n o subcategor�a.';

comment on column FICHA.ID_FICHA is
'Identificador de una ficha de un plan que se encuantra en promoci�n.';

comment on column FICHA.ID_SUBCATEGORIA is
'Identificador de subcategor�a.';

comment on column FICHA.CODIGO_PLAN_LEG is
'C�digo BIGSA del plan.';

comment on column FICHA.NOMBRE_COMERCIAL is
'Nombre comercial del plan o subcategor�a';

comment on column FICHA.DESCRIPCION_FICHA is
'Descripci�n de la ficha';

comment on column FICHA.ES_SUBCATEGORIA is
'Es subcategor�a o Plan en Promoci�n';

comment on column FICHA.ESTADO_FICHA is
'Estado de disponibilidad de ficha de una promoci�n para ser desplegada.';

comment on column FICHA.MONTO_UF is
'Monto asociado a la ficha subcategor�a.';

comment on column FICHA.ID_PAIS is
'Identificador de Pa�s.';

comment on column FICHA.FECHA_CREACION is
'Fecha de creaci�n del registro.';

comment on column FICHA.FECHA_MODIFICACION is
'Fecha de modificaci�n del registro.';

/*==============================================================*/
/* Index: FICHA_PORPRODUCTO                                     */
/*==============================================================*/
create index FICHA_PORPRODUCTO on FICHA (
   ID_PAIS ASC,
   ID_SUBCATEGORIA ASC
);

/*==============================================================*/
/* Index: FICHA_PORPLAN                                         */
/*==============================================================*/
create index FICHA_PORPLAN on FICHA (
   ID_PAIS ASC,
   CODIGO_PLAN_LEG ASC
);

/*==============================================================*/
/* Index: FICHA_PORTIPOYPAIS                                    */
/*==============================================================*/
create index FICHA_PORTIPOYPAIS on FICHA (
   ES_SUBCATEGORIA ASC,
   ID_PAIS ASC
);

alter table FICHA
   add constraint FK_FICHA_REFERENCE_PLAN_LEG foreign key (CODIGO_PLAN_LEG)
      references PLAN_LEG (CODIGO_PLAN_LEG);

alter table FICHA
   add constraint FK_FICHA_REFERENCE_SUBCATEG foreign key (ID_SUBCATEGORIA)
      references SUBCATEGORIA (ID_SUBCATEGORIA);

      
      

      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
alter table ASPECTO_LEGAL_FICHA
   drop constraint FK_ASPECTO__REFERENCE_FICHA;

drop index ASPECTO_FICHA;

drop table ASPECTO_LEGAL_FICHA cascade constraints;

/*==============================================================*/
/* Table: ASPECTO_LEGAL_FICHA                                   */
/*==============================================================*/
create table ASPECTO_LEGAL_FICHA  (
   ID_ASPECTO           NUMBER(8)                       not null,
   ID_FICHA             NUMBER(8),
   HTML                 BLOB                            not null,
   FECHA_CREACION       DATE                            not null,
   FECHA_MODIFICACION   DATE                            not null,
   ID_PAIS              NUMBER(8)                       not null,
   constraint PK_ASPECTO_LEGAL_FICHA primary key (ID_ASPECTO)
);

comment on table ASPECTO_LEGAL_FICHA is
'Corresponde a la tabla que almacenar� los datos del aspecto legal de la ficha.';

comment on column ASPECTO_LEGAL_FICHA.ID_ASPECTO is
'Identificador del archivo HTML de aspecto legal para un plan en promoci�n.';

comment on column ASPECTO_LEGAL_FICHA.ID_FICHA is
'Identificador de una ficha de un plan que se encuantra en promoci�n.';

comment on column ASPECTO_LEGAL_FICHA.HTML is
'Archivo HTML que contendr� el aspecto legal de un plan en promoci�n.';

comment on column ASPECTO_LEGAL_FICHA.FECHA_CREACION is
'Fecha de creaci�n del registro.';

comment on column ASPECTO_LEGAL_FICHA.FECHA_MODIFICACION is
'Fecha de modificaci�n del registro.';

comment on column ASPECTO_LEGAL_FICHA.ID_PAIS is
'Identificador de Pa�s.';

/*==============================================================*/
/* Index: ASPECTO_FICHA                                         */
/*==============================================================*/
create unique index ASPECTO_FICHA on ASPECTO_LEGAL_FICHA (
   ID_ASPECTO ASC
);

alter table ASPECTO_LEGAL_FICHA
   add constraint FK_ASPECTO__REFERENCE_FICHA foreign key (ID_FICHA)
      references FICHA (ID_FICHA);
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
alter table EXCLUSION_FICHA
   drop constraint FK_EXCLUSIO_REFERENCE_FICHA;

drop table EXCLUSION_FICHA cascade constraints;

/*==============================================================*/
/* Table: EXCLUSION_FICHA                                       */
/*==============================================================*/
create table EXCLUSION_FICHA  (
   ID_EXCLUSION         NUMBER(8)                       not null,
   ID_FICHA             NUMBER(8),
   HTML                 BLOB                            not null,
   FECHA_CREACION       DATE                            not null,
   FECHA_MODIFICACION   DATE                            not null,
   ID_PAIS              NUMBER(8),
   constraint PK_EXCLUSION_FICHA primary key (ID_EXCLUSION)
);

comment on table EXCLUSION_FICHA is
'Corresponde a la tabla que almacenar� los datos de la exclusi�n de la ficha.';

comment on column EXCLUSION_FICHA.ID_EXCLUSION is
'Identificador del archivo HTML de exclusion de un plan en promoci�n.';

comment on column EXCLUSION_FICHA.ID_FICHA is
'Identificador de una ficha de un plan que se encuantra en promoci�n.';

comment on column EXCLUSION_FICHA.HTML is
'Archivo HTML que contendr� las exclusiones de una plan en promoci�n.';

comment on column EXCLUSION_FICHA.FECHA_CREACION is
'Fecha de creaci�n del registro.';

comment on column EXCLUSION_FICHA.FECHA_MODIFICACION is
'Fecha de modificaci�n del registro.';

comment on column EXCLUSION_FICHA.ID_PAIS is
'Identificador de Pa�s.';

alter table EXCLUSION_FICHA
   add constraint FK_EXCLUSIO_REFERENCE_FICHA foreign key (ID_FICHA)
      references FICHA (ID_FICHA);























alter table COBERTURA_FICHA
   drop constraint FK_COBERTUR_REFERENCE_FICHA;

drop table COBERTURA_FICHA cascade constraints;

/*==============================================================*/
/* Table: COBERTURA_FICHA                                       */
/*==============================================================*/
create table COBERTURA_FICHA  (
   ID_COBERTURA         NUMBER(8)                       not null,
   ID_FICHA             NUMBER(8),
   HTML                 BLOB                            not null,
   FECHA_CREACION       DATE                            not null,
   FECHA_MODIFICACION   DATE                            not null,
   ID_PAIS              NUMBER(8)                       not null,
   constraint PK_COBERTURA_FICHA primary key (ID_COBERTURA)
);

comment on table COBERTURA_FICHA is
'Corresponde a la tabla que almacenar� los datos de la cobertura de la ficha.';

comment on column COBERTURA_FICHA.ID_COBERTURA is
'Identificador del archivo HTML de coberturas de un plan en promoci�n.';

comment on column COBERTURA_FICHA.ID_FICHA is
'Identificador de una ficha de un plan que se encuantra en promoci�n.';

comment on column COBERTURA_FICHA.HTML is
'Archivo HTML que contendr� las coberturas de un plan en propmoci�n.';

comment on column COBERTURA_FICHA.FECHA_CREACION is
'Fecha de creaci�n del registro.';

comment on column COBERTURA_FICHA.FECHA_MODIFICACION is
'Fecha de modificaci�n del registro.';

comment on column COBERTURA_FICHA.ID_PAIS is
'Identificador de Pa�s.';

alter table COBERTURA_FICHA
   add constraint FK_COBERTUR_REFERENCE_FICHA foreign key (ID_FICHA)
      references FICHA (ID_FICHA);























alter table IMAGEN_FICHA
   drop constraint FK_IMAGEN_F_REFERENCE_FICHA;

drop table IMAGEN_FICHA cascade constraints;

/*==============================================================*/
/* Table: IMAGEN_FICHA                                          */
/*==============================================================*/
create table IMAGEN_FICHA  (
   ID_IMAGEN            NUMBER(8)                       not null,
   ID_FICHA             NUMBER(8),
   IMAGEN               BLOB                            not null,
   FECHA_CREACION       DATE                            not null,
   FECHA_MODIFICACION   DATE                            not null,
   ID_PAIS              NUMBER(8)                       not null,
   constraint PK_IMAGEN_FICHA primary key (ID_IMAGEN)
);

comment on table IMAGEN_FICHA is
'Corresponde a la tabla que almacenar� los datos de la imagen de la ficha.';

comment on column IMAGEN_FICHA.ID_IMAGEN is
'Identificador de la imagen de un plan que se encuentra en promoci�n.';

comment on column IMAGEN_FICHA.ID_FICHA is
'Identificador de una ficha de un plan que se encuantra en promoci�n.';

comment on column IMAGEN_FICHA.IMAGEN is
'Imagen asociada al despliegue de un plan en promoci�n.';

comment on column IMAGEN_FICHA.FECHA_CREACION is
'Fecha de creaci�n del registro.';

comment on column IMAGEN_FICHA.FECHA_MODIFICACION is
'Fecha de modificaci�n del registro.';

comment on column IMAGEN_FICHA.ID_PAIS is
'Identificador de Pa�s.';

alter table IMAGEN_FICHA
   add constraint FK_IMAGEN_F_REFERENCE_FICHA foreign key (ID_FICHA)
      references FICHA (ID_FICHA); 

      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
alter table BENEFICIO_FICHA
   drop constraint FK_BENEFICI_REFERENCE_FICHA;

drop table BENEFICIO_FICHA cascade constraints;

/*==============================================================*/
/* Table: BENEFICIO_FICHA                                       */
/*==============================================================*/
create table BENEFICIO_FICHA  (
   ID_VENTAJA           NUMBER(8)                       not null,
   ID_FICHA             NUMBER(8),
   HTML                 BLOB                            not null,
   ID_PAIS              NUMBER(8)                       not null,
   FECHA_CREACION       DATE                            not null,
   FECHA_MODIFICACION   DATE                            not null,
   constraint PK_BENEFICIO_FICHA primary key (ID_VENTAJA)
);

comment on table BENEFICIO_FICHA is
'Corresponde a la tabla que almacenar� los datos de la ventaja de la ficha.';

comment on column BENEFICIO_FICHA.ID_VENTAJA is
'Identificador del HTML que contendr� las ventajas de un plan en promoci�n.';

comment on column BENEFICIO_FICHA.ID_FICHA is
'Identificador de una ficha de un plan que se encuantra en promoci�n.';

comment on column BENEFICIO_FICHA.HTML is
'Archivo HTML que contendr� las ventajas de un plan en promoci�n.';

comment on column BENEFICIO_FICHA.ID_PAIS is
'Identificador de Pa�s.';

comment on column BENEFICIO_FICHA.FECHA_CREACION is
'Fecha de creaci�n del registro.';

comment on column BENEFICIO_FICHA.FECHA_MODIFICACION is
'Fecha de modificaci�n del registro.';

alter table BENEFICIO_FICHA
   add constraint FK_BENEFICI_REFERENCE_FICHA foreign key (ID_FICHA)
      references FICHA (ID_FICHA);
      