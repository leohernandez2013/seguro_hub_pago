alter table plan_leg 
add 
   constraint AK_CODIGO_PLAN_LEG_PLAN_LEG unique (CODIGO_PLAN_LEG)



select * from plan_leg group by codigo_plan_leg;
select * from tipo_producto_leg;
select * from RAMA;

insert into rama values (rama_seq.nextval, 21, 'Rama1', sysdate, sysdate, 1);
insert into rama values (rama_seq.nextval, 21, 'Rama2', sysdate, sysdate, 1);
insert into rama values (rama_seq.nextval, 21, 'Rama3', sysdate, sysdate, 1);
insert into rama values (rama_seq.nextval, 22, 'Rama4', sysdate, sysdate, 1);


select * from subcategoria;

insert into subcategoria values (subcategoria_seq.nextval, 1, 'Subcat 03', 0, sysdate, sysdate, 1, 'ghost');
insert into subcategoria values (subcategoria_seq.nextval, 1, 'Subcat 04', 0, sysdate, sysdate, 1, 'ghost');
insert into subcategoria values (subcategoria_seq.nextval, 1, 'Subcat 05', 0, sysdate, sysdate, 1, 'ghost');
insert into subcategoria values (subcategoria_seq.nextval, 1, 'Subcat 06', 0, sysdate, sysdate, 1, 'ghost');
insert into subcategoria values (subcategoria_seq.nextval, 1, 'Subcat 07', 0, sysdate, sysdate, 1, 'ghost');
insert into subcategoria values (subcategoria_seq.nextval, 1, 'Subcat 08', 0, sysdate, sysdate, 1, 'ghost');

commit;


select * from producto_leg;
select * from producto;

select pl.* from producto p, producto_leg pl where p.id_subcategoria = 1
and pl.id_producto = p.id_producto_leg
and p.es_eliminado = 0
and pl.es_activo = 1
order by pl.nombre

select pl.* from producto_leg pl 
where not exists (select 1 from producto p where p.id_subcategoria=1 and p.id_producto_leg = pl.id_producto)
and pl.es_activo = 1
and upper(pl.nombre) like (upper('%'))
order by pl.nombre

insert into producto values (producto_seq.nextval, 1, 80, 0, sysdate, sysdate, 1, 'ghost');
commit;

create sequence ficha_seq;
create sequence aspecto_legal_ficha_seq;
create sequence exclusion_ficha_seq;
create sequence cobertura_ficha_seq;
create sequence imagen_ficha_seq;
create sequence beneficio_ficha_seq;

select * from plan_leg;
insert into ficha values (ficha_seq.nextval, 1, null, 'nombre comercial ficha', 'descripcion ficha', 'descripcion pagina', 1, 1, 10, 1, sysdate, sysdate);
insert into ficha values (ficha_seq.nextval, 1, 1517, 'nombre comercial otra', 'descripcion otra', 'descripcion otra pagina', 0, 1, 10, 1, sysdate, sysdate);

select * from ficha;
select * from imagen_ficha;
select * from cobertura_ficha;

update ficha set codigo_plan_leg = null;

