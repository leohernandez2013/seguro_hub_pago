package cl.tinet.common.struts.form;

import javax.servlet.http.HttpServletRequest;

import cl.cencosud.ventaseguros.common.config.VSPConfig;
import cl.tinet.common.config.AbstractConfigurator;

/**
 * TODO Falta descripcion de clase BuilderActionForm.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Aug 21, 2010
 */
public abstract class BuilderActionFormBaseVSP extends BuilderActionForm {

    /**
     * TODO Describir m�todo getConfigurator.
     * @param request
     * @return
     */
    @Override
    public AbstractConfigurator getConfigurator(HttpServletRequest request) {
        return VSPConfig.getInstance();
    }
}
