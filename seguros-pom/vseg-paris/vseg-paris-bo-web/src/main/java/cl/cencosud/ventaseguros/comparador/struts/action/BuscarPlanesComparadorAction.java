package cl.cencosud.ventaseguros.comparador.struts.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import cl.cencosud.ventaseguros.common.PlanLeg;
import cl.cencosud.ventaseguros.comparador.model.Caracteristica;
import cl.cencosud.ventaseguros.delegate.ComparadorDelegate;

import com.tinet.exceptions.system.SystemException;

public class BuscarPlanesComparadorAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String oEstado = request.getParameter("idEstado");
		String oIdProducto = request.getParameter("idProducto");
		String oTipoCaracteristica = request.getParameter("tipoCaracteristica");
		
		ComparadorDelegate delegate = new ComparadorDelegate();
		this.deleteCaracteristicasSession(request);
		
		try {
            
			List<PlanLeg> listPlanes = delegate.obtenerPlanesPorProducto(Integer.valueOf(oEstado), Long.valueOf(oIdProducto));
            
			if (listPlanes != null && listPlanes.size() > 0) {
				for (PlanLeg plan : listPlanes) {
					String oIdPlan = String.valueOf(plan.getId_plan());
					List<Caracteristica> listCaracteristica = delegate.obtenerCaracteristicasPorPlan(Integer.parseInt(oIdPlan), Integer.parseInt(oTipoCaracteristica), Integer.parseInt(oEstado));
					plan.setListCaracteristica(listCaracteristica);					
				}
				request.getSession().setAttribute("cl.cencosud.ventaseguros.comparador.listPlanesPorProducto", listPlanes);
			}
			
			LinkedHashMap<String, ?> mapCaract = this.getNormalizedRows(listPlanes);
			LinkedHashMap<String, ?> mapPlanes = this.getColumnData(listPlanes);
			
			LinkedHashMap<String, LinkedHashMap<String, ?>> result = new LinkedHashMap<String, LinkedHashMap<String,?>>();
			
			if (mapCaract != null && mapCaract.size() > 0) {
				result.put("mapCaract", mapCaract);
			}			
			result.put("mapPlanes", mapPlanes);
			
			this.setCaracteristicasSession(request, mapCaract);
						
            response.setHeader("pragma", "no-cache");
            response.setHeader("cache-control", "no-cache");
            response.setDateHeader("expires", -1);
            response.setContentType("text/html");

            PrintWriter pwritter = response.getWriter();
            ObjectMapper mapper = new ObjectMapper();
            StringWriter json = new StringWriter();
            mapper.writeValue(json, result);
            pwritter.write(json.toString());

        } catch (IOException e) {
            throw new SystemException(e);
        }
		
		return null;
	}

	@SuppressWarnings("unchecked")
	private void setCaracteristicasSession(HttpServletRequest request,
			LinkedHashMap<String, ?> mapCaract) {
		
		if (mapCaract != null && mapCaract.size() > 0) {
			LinkedHashMap<Integer, Caracteristica> mapCaracteristicas = new LinkedHashMap<Integer, Caracteristica>();
			LinkedHashMap<Integer, List<Caracteristica>> mapPlanCaracteristica = new LinkedHashMap<Integer, List<Caracteristica>>();
			
			Iterator<?> ite = mapCaract.entrySet().iterator();
			
			while (ite.hasNext()) {
				Map.Entry<String, ?> e = (Map.Entry<String, ?>) ite.next();
				Integer codCaract = Integer.valueOf(e.getKey());
				List<Caracteristica> listTemp = (List<Caracteristica>)e.getValue();
				
				if (listTemp != null && listTemp.size() > 0) {
					mapCaracteristicas.put(codCaract, listTemp.get(0));
				}
				
				mapPlanCaracteristica.put(codCaract, listTemp);			
			}
			
			request.getSession().setAttribute("cl.cencosud.ventaseguros.comparador.mapCaracteristicas", mapCaracteristicas);
			request.getSession().setAttribute("cl.cencosud.ventaseguros.comparador.mapPlanCaracteristica", mapPlanCaracteristica);
		}		
	}

	@SuppressWarnings("unchecked")
	private void deleteCaracteristicasSession(HttpServletRequest request) {
		LinkedHashMap<Integer, Caracteristica> mapCaracteristicas = (LinkedHashMap<Integer, Caracteristica>)
			request.getSession().getAttribute("cl.cencosud.ventaseguros.comparador.mapCaracteristicas");
		LinkedHashMap<Integer, List<Caracteristica>> mapPlanCaracteristica = (LinkedHashMap<Integer, List<Caracteristica>>)
			request.getSession().getAttribute("cl.cencosud.ventaseguros.comparador.mapPlanCaracteristica");	
		List<PlanLeg> listPlanes = (List<PlanLeg>)
			request.getSession().getAttribute("cl.cencosud.ventaseguros.comparador.listPlanesPorProducto");
	
		if (mapCaracteristicas != null) {
			request.getSession().removeAttribute("cl.cencosud.ventaseguros.comparador.mapCaracteristicas");
		}
		
		if (mapPlanCaracteristica != null) {
			request.getSession().removeAttribute("cl.cencosud.ventaseguros.comparador.mapPlanCaracteristica");
		}
		
		if (listPlanes != null) {
			request.getSession().removeAttribute("cl.cencosud.ventaseguros.comparador.listPlanesPorProducto");
		}
	}

	private LinkedHashMap<String, List<PlanLeg>> getColumnData(List<PlanLeg> listPlanes) {
		LinkedHashMap<String, List<PlanLeg>> map = new LinkedHashMap<String, List<PlanLeg>>();
		map.put("columnData", listPlanes);
		return map;
	}

	private LinkedHashMap<String, List<Caracteristica>> getNormalizedRows(List<PlanLeg> listPlanes) {

		if (listPlanes != null && listPlanes.size() > 0) {
			LinkedHashMap<String, List<Caracteristica>> mapCaracteristica = new LinkedHashMap<String, List<Caracteristica>>();
			for (PlanLeg plan : listPlanes) {
				List<Caracteristica> list = plan.getListCaracteristica();
				
				for (Caracteristica car : list) {										
					if (mapCaracteristica.containsKey(String.valueOf(car.getId_caracteristica()))) {
						List<Caracteristica> listTemp = mapCaracteristica.get(String.valueOf(car.getId_caracteristica()));
						listTemp.add(car);
					} else {
						List<Caracteristica> listTemp = new ArrayList<Caracteristica>();
						listTemp.add(car);
						mapCaracteristica.put(String.valueOf(car.getId_caracteristica()), listTemp);
					}
				}
			}
			return mapCaracteristica;
		}
		
		return null;		
	}

}
