package cl.cencosud.ventaseguros.paginaintermedia.struts.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.common.Rama;
import cl.cencosud.ventaseguros.delegate.PaginaIntermediaDelegate;


/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class AdministrarPaginaIntermediaAction extends Action {
    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        //        AdministrarPaginaIntermediaForm oForm =
        //            (AdministrarPaginaIntermediaForm) form;

        PaginaIntermediaDelegate delegate = new PaginaIntermediaDelegate();

        List < Rama > ramas = delegate.obtenerRamas();

        request.setAttribute("ramas", ramas);

        return mapping.findForward("continue");
    }
}
