package cl.cencosud.ventaseguros.comparador.struts.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.common.ProductoLeg;
import cl.cencosud.ventaseguros.common.Subcategoria;
import cl.cencosud.ventaseguros.comparador.struts.form.AdministrarComparadorForm;
import cl.cencosud.ventaseguros.delegate.PaginaIntermediaDelegate;

public class EditarProductoAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		AdministrarComparadorForm oForm = (AdministrarComparadorForm)form;
		int idProducto = oForm.getIdProducto();
		int idSubcategoria = oForm.getIdSubcategoria();
		int idRama = oForm.getIdRama();
		
		PaginaIntermediaDelegate delegate = new PaginaIntermediaDelegate();
		List <Subcategoria> subcategorias = delegate.obtenerSubcategorias(idRama);		
		List <ProductoLeg> productos = delegate.obtenerListaProductos(idSubcategoria, true, "");
		
		request.setAttribute("listSubcategorias", subcategorias);
		request.setAttribute("listProductos", productos);
		
		oForm.setSubcategoria(idSubcategoria);
		oForm.setProducto(idProducto);
		oForm.setIdRama(idRama);
		request.setAttribute("idRama", idRama);
		
		return mapping.findForward("continuar");
	}

}
