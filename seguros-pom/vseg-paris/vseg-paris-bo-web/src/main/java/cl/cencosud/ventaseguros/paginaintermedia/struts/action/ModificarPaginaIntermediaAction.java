package cl.cencosud.ventaseguros.paginaintermedia.struts.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import cl.cencosud.ventaseguros.common.ProductoLeg;
import cl.cencosud.ventaseguros.common.Subcategoria;
import cl.cencosud.ventaseguros.delegate.PaginaIntermediaDelegate;
import cl.cencosud.ventaseguros.paginaintermedia.struts.form.ModificarPaginaIntermediaForm;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class ModificarPaginaIntermediaAction extends Action {
    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        ModificarPaginaIntermediaForm oForm =
            (ModificarPaginaIntermediaForm) form;

        Integer sIdSubcategoria =
            (Integer) request.getSession().getAttribute("idSubcategoria");
        String pIdSubcategoria = (String) request.getParameter("id");

        List < LabelValueBean > estados = new ArrayList < LabelValueBean >();
        estados.add(new LabelValueBean("0", "Activado"));
        estados.add(new LabelValueBean("1", "Desactivado"));
        request.setAttribute("estados", estados);

        if (pIdSubcategoria != null || sIdSubcategoria != null) {

            int idSubcategoria = -1;
            if (sIdSubcategoria == null) {
                idSubcategoria = Integer.valueOf(pIdSubcategoria);
            } else {
                idSubcategoria = Integer.valueOf(sIdSubcategoria);
            }

            PaginaIntermediaDelegate delegate = new PaginaIntermediaDelegate();

            //Obtener los datos de la subcategoria.
            Subcategoria subcategoria =
                delegate.obtenerSubcategoria(idSubcategoria);
            request.setAttribute("subcategoria", subcategoria);
            oForm.setId_subcategoria(subcategoria.getId_subcategoria());

            oForm.setTitulo_subcategoria(subcategoria.getTitulo_subcategoria());

            //Obtener el listado de productos.
            List < ProductoLeg > productos =
                delegate.obtenerListaProductos(idSubcategoria, true, null);
            request.setAttribute("productos", productos);

        }
        request.getSession().removeAttribute("idSubcategoria");

        return mapping.findForward("continue");
    }
}
