/*
 * Generated by MyEclipse Struts
 * Template path: templates/java/JavaClass.vtl
 */
package cl.cencosud.ventaseguros.gestionusuario.struts.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.delegate.MantenedorUsuariosDelegate;

/**
 * MyEclipse Struts Creation date: 09-09-2010
 * 
 * XDoclet definition:
 * 
 * @struts.action validate="true"
 */
public class NuevoPersonalCencosudCargarAction extends Action {

    /**
     * cadena de flujo de exito.
     */
    private static final String EXITO = "continuar";
    /**
     * cadena de flujo, error.
     */
    private static final String ERROR = "error";

    /**
     * Metodo ActionForward struts.
     *
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {
        String flujo = ERROR;
        MantenedorUsuariosDelegate delegate;

        delegate = new MantenedorUsuariosDelegate();

        List < Map > listaRoles = delegate.obtenerRoles();

        if (listaRoles != null) {
            request.setAttribute("roles", listaRoles);
            flujo = EXITO;
        }
        return mapping.findForward(flujo);
    }

}