package cl.cencosud.ventaseguros.paginaintermedia.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.delegate.PaginaIntermediaDelegate;
import cl.cencosud.ventaseguros.paginaintermedia.struts.form.ModificarPaginaIntermediaForm;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class ModificarSubcategoriaAction extends Action {
    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        ModificarPaginaIntermediaForm oForm =
            (ModificarPaginaIntermediaForm) form;

        PaginaIntermediaDelegate delegate = new PaginaIntermediaDelegate();

        delegate.modificarSubcategoria(oForm.getId_subcategoria(), oForm
            .getTitulo_subcategoria(), oForm.getEs_eliminado());

        request.getSession().setAttribute("idSubcategoria",
            oForm.getId_subcategoria());

        return mapping.findForward("continue");
    }
}
