/*
 * Generated by MyEclipse Struts
 * Template path: templates/java/JavaClass.vtl
 */
package cl.cencosud.ventaseguros.fichasubcategoria.struts.form;

import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;

import cl.tinet.common.struts.form.BuilderActionFormBaseVSP;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 * @struts.form name="pruebaForm"
 */
public class FichaSubcategoriaForm extends BuilderActionFormBaseVSP {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 2426884372730166267L;

    private int rama;
    private int subcategoria;
    private int id_ficha;
    

    /**
     * Obtiene archivo de validación.
     * @param request HttpServletRequest
     * @return InputStream
     */
    @Override
    public InputStream getValidationRules(HttpServletRequest request) {
        return FichaSubcategoriaForm.class
            .getResourceAsStream("resource/validation.xml");
    }

    /**
     * @return retorna el valor del atributo rama
     */
    public int getRama() {
        return rama;
    }

    /**
     * @param rama a establecer en el atributo rama.
     */
    public void setRama(int rama) {
        this.rama = rama;
    }

    /**
     * @return retorna el valor del atributo subcategoria
     */
    public int getSubcategoria() {
        return subcategoria;
    }

    /**
     * @param subcategoria a establecer en el atributo subcategoria.
     */
    public void setSubcategoria(int subcategoria) {
        this.subcategoria = subcategoria;
    }

    /**
     * @return retorna el valor del atributo id_ficha
     */
    public int getId_ficha() {
        return id_ficha;
    }

    /**
     * @param id_ficha a establecer en el atributo id_ficha.
     */
    public void setId_ficha(int id_ficha) {
        this.id_ficha = id_ficha;
    }


}