package cl.cencosud.ventaseguros.admin.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.tinet.common.seguridad.model.UsuarioInterno;
import cl.tinet.common.seguridad.util.SeguridadUtil;

public class InicioAction extends Action {

    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        boolean conectado = false;

        UsuarioInterno usuario =
            (UsuarioInterno) SeguridadUtil.getUsuario(request);

        if (usuario != null) {
            conectado = true;
        }

        request.setAttribute("CONECTADO", conectado);

        return mapping.findForward("continuar");
    }
}
