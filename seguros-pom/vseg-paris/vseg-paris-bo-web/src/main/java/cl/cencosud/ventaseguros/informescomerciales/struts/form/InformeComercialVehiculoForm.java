package cl.cencosud.ventaseguros.informescomerciales.struts.form;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import cl.tinet.common.struts.form.BuilderActionFormBaseVSP;

public class InformeComercialVehiculoForm extends BuilderActionFormBaseVSP {

    /**
     * serialVersionUID 
     */
    private static final long serialVersionUID = 8653703787402335048L;
    
    private int rama;
    
    private int subcategoria;
    
    private int id_plan;
    
    private String compannia;

    @Override
    public InputStream getValidationRules(HttpServletRequest request) {
        return InformeComercialVehiculoForm.class
            .getResourceAsStream("resource/validation.xml");
    }
    
    @Override
    public ActionErrors validate(ActionMapping mapping,
        HttpServletRequest request) {

        ActionErrors errores = super.validate(mapping, request);

        if (!this.getDatos().get("fechaDesde").equals("")
            && !this.getDatos().get("fechaHasta").equals("")) {

            try {
                SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                Date fechaDesde =
                    formato.parse(this.getDatos().get("fechaDesde"));
                Date fechaHasta =
                    formato.parse(this.getDatos().get("fechaHasta"));

                if (fechaHasta.before(fechaDesde)) {
                    errores.add("datos.fechaDesde",
                        new ActionMessage("errors.validacion-fecha","Fecha Hasta"));
                }

            } catch (ParseException e) {
                errores.add("datos.fechaDesde", new ActionMessage(
                    "errors.validacion-fecha", "Fecha Hasta"));
            }

        }
        return errores;
    }

	public void setRama(int rama) {
		this.rama = rama;
	}

	public int getRama() {
		return rama;
	}

	public void setSubcategoria(int subcategoria) {
		this.subcategoria = subcategoria;
	}

	public int getSubcategoria() {
		return subcategoria;
	}

	public void setCompannia(String compannia) {
		this.compannia = compannia;
	}

	public String getCompannia() {
		return compannia;
	}

	public void setId_plan(int id_plan) {
		this.id_plan = id_plan;
	}

	public int getId_plan() {
		return id_plan;
	}

}
