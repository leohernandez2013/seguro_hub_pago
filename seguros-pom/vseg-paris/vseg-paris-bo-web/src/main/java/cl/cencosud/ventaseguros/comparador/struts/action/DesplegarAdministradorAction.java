package cl.cencosud.ventaseguros.comparador.struts.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.common.Rama;
import cl.cencosud.ventaseguros.common.Subcategoria;
import cl.cencosud.ventaseguros.comparador.struts.form.AdministrarComparadorForm;
import cl.cencosud.ventaseguros.delegate.PaginaIntermediaDelegate;

public class DesplegarAdministradorAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		PaginaIntermediaDelegate delegate = new PaginaIntermediaDelegate();
		List<Rama> listRamas = delegate.obtenerRamas();
		String rama = request.getParameter("ramaSelected");
		
		AdministrarComparadorForm formulario = (AdministrarComparadorForm)form;
		formulario.setTipo(1);
		List < Subcategoria > subcategorias = new ArrayList<Subcategoria>();
		
		if (rama!=null && !rama.trim().equals("")) {
			formulario.setRama(Integer.valueOf(rama));
			subcategorias = delegate.obtenerSubcategorias(Integer.valueOf(rama));
		}
		
		request.setAttribute("listSubcategorias", subcategorias);
		request.setAttribute("listRamas", listRamas);		
		return mapping.findForward("continuar");
	}

}
