package cl.cencosud.ventaseguros.paginaintermedia.struts.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import cl.cencosud.ventaseguros.common.TipoRama;
import cl.cencosud.ventaseguros.delegate.PaginaIntermediaDelegate;

import com.tinet.exceptions.system.SystemException;

/**
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 *
 * XDoclet definition:
 */
public class BuscarTiposRamaAction extends Action {

    /**
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        PaginaIntermediaDelegate delegate = new PaginaIntermediaDelegate();
        try {
            List < TipoRama > tiposRama =
                new ArrayList < TipoRama >();
            if (request.getParameter("idRama") != null
                && !request.getParameter("idRama").equals("")){
                int idRama =
                    Integer.parseInt((String) request.getParameter("idRama"));

                tiposRama = delegate.obtenerTiposRama(idRama);
            }
            response.setHeader("pragma", "no-cache");
            response.setHeader("cache-control", "no-cache");
            response.setDateHeader("expires", -1);
            response.setContentType("text/html");

            PrintWriter pwritter = response.getWriter();
            ObjectMapper mapper = new ObjectMapper();
            StringWriter json = new StringWriter();
            mapper.writeValue(json, tiposRama);
            pwritter.write(json.toString());

        } catch (IOException e) {
            throw new SystemException(e);
        }

        return null;

    }
}
