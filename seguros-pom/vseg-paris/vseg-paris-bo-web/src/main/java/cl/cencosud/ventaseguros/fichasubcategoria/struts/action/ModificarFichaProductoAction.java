package cl.cencosud.ventaseguros.fichasubcategoria.struts.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.common.Ficha;
import cl.cencosud.ventaseguros.common.LegalFicha;
import cl.cencosud.ventaseguros.common.PlanLeg;
import cl.cencosud.ventaseguros.common.Rama;
import cl.cencosud.ventaseguros.common.Subcategoria;
import cl.cencosud.ventaseguros.delegate.FichaSubcategoriaDelegate;
import cl.cencosud.ventaseguros.delegate.PaginaIntermediaDelegate;
import cl.cencosud.ventaseguros.fichasubcategoria.struts.form.NuevaFichaProductoForm;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class ModificarFichaProductoAction extends Action {
    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        NuevaFichaProductoForm oForm = (NuevaFichaProductoForm) form;
        request.getSession().setAttribute("id_ficha", oForm.getId_ficha());

        PaginaIntermediaDelegate paginaIntermediaDelegate =
            new PaginaIntermediaDelegate();

        FichaSubcategoriaDelegate fichaSubcategoriaDelegate =
            new FichaSubcategoriaDelegate();

        //Obtener datos de la ficha.
        Ficha ficha =
            fichaSubcategoriaDelegate.obtenerFicha(oForm.getId_ficha());
        
        //CBM
        		
        LegalFicha legalFicha = fichaSubcategoriaDelegate.obtenerLegalFicha(oForm.getId_ficha());
        if(legalFicha == null ){
        	legalFicha = new LegalFicha();
        	legalFicha.setTexto_legal("");
        	legalFicha.setActivo("N");
        }
        
        oForm.setSubcategoria(ficha.getId_subcategoria());
        oForm.setTipo(ficha.getEs_subcategoria());
        oForm.setHabilitar(ficha.getEstado_ficha());
        oForm.setDescripcion_ficha(ficha.getDescripcion_ficha());
        oForm.setDescripcion_pagina(ficha.getDescripcion_pagina());
        oForm.setId_plan(ficha.getId_plan());
        oForm.setMonto_uf(0.0);
        oForm.setNombre_comercial(ficha.getNombre_comercial());
        oForm.setId_ficha(ficha.getId_ficha());
        oForm.setTexto_legal(legalFicha.getTexto_legal());
        oForm.setHabilita_texto_legal(legalFicha.getActivo());

        Subcategoria subcategoria =
            paginaIntermediaDelegate.obtenerSubcategoria(ficha
                .getId_subcategoria());
        oForm.setRama(subcategoria.getId_rama());

        List < Rama > ramas = paginaIntermediaDelegate.obtenerRamas();
        request.setAttribute("ramas", ramas);

        if (oForm.getRama() > 0) {
            List < Subcategoria > subcategorias =
                paginaIntermediaDelegate.obtenerSubcategorias(oForm.getRama());
            request.setAttribute("subcategorias", subcategorias);
        }

        if (oForm.getSubcategoria() > 0) {
            List < PlanLeg > planes =
                fichaSubcategoriaDelegate
                    .obtenerPlanes(oForm.getSubcategoria());
            request.setAttribute("planes", planes);
        }

        return mapping.findForward("continue");
    }
}
