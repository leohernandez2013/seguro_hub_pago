package cl.cencosud.ventaseguros.gestioneventos.struts.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import cl.cencosud.ventaseguros.delegate.MantenedorEventosDelegate;

import com.tinet.exceptions.system.SystemException;

/**
 * Clase llamada por ajax, para cargar el combo de prioridades.
 * <br/>
 * @author Ricardo
 * @version 1.0
 * @created Oct 4, 2010
 */
public class CargarComboPrioridadAction extends Action {

    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        MantenedorEventosDelegate delegate = new MantenedorEventosDelegate();

        List < Map < String, ? > > lista = delegate.obtenerPrioridad();

        response.setHeader("pragma", "no-cache");
        response.setHeader("cache-control", "no-cache");
        response.setDateHeader("expires", -1);
        response.setContentType("text/html");

        try {
            PrintWriter pwriter;
            pwriter = response.getWriter();
            ObjectMapper mapper = new ObjectMapper();
            StringWriter json = new StringWriter();
            mapper.writeValue(json, lista);
            pwriter.write(json.toString());

        } catch (IOException e) {
            throw new SystemException(e);
        }

        return null;

    }

}
