package cl.cencosud.ventaseguros.asesorvirtual.util;

import cl.cencosud.ventaseguros.asesorvirtual.model.Resultado;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * Conversor especifico para el formato de resultado.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 09/09/2010
 */
public class ResultadoConverter implements Converter {

    /**
     * Se encarga de serializar un objeto de tipo resultado.
     * @param object Objeto a ser seralizado.
     * @param writer Instancia donde se escribe el objeto serializado.
     * @param context Contexto.
     */
    public void marshal(Object object, HierarchicalStreamWriter writer,
        MarshallingContext context) {
        Resultado resultado = (Resultado) object;
        writer.addAttribute("id", String.valueOf(resultado.getId()));
        writer.addAttribute("titulo", resultado.getTitulo());
        writer.addAttribute("nombreplan", resultado.getNombreplan());
        if (resultado.getLink0() == null) {
            resultado.setLink0("");
        }
        if (resultado.getLink1() == null) {
            resultado.setLink1("");
        }
        if (resultado.getIcono() == null) {
            resultado.setIcono("");
        }
        writer.addAttribute("link0", resultado.getLink0());
        writer.addAttribute("link1", resultado.getLink1());
        writer.addAttribute("icono", resultado.getIcono());
        writer.setValue(resultado.getValue());
    }

    /**
     * Se encarga de deserializar en un objeto de tipo Resultado.
     * @param reader Objeto xml que se esta leyendo.
     * @param context Contexto.
     * @return Objeto Resultado con los valores obtenidos.
     */
    public Object unmarshal(HierarchicalStreamReader reader,
        UnmarshallingContext context) {
        Resultado resultado = new Resultado();

        resultado.setIcono(reader.getAttribute("icono"));

        if (reader.getAttribute("id") != null) {
            resultado.setId(Integer.valueOf(reader.getAttribute("id")));
        }
        resultado.setLink0(reader.getAttribute("link0"));
        resultado.setLink1(reader.getAttribute("link1"));
        resultado.setNombreplan(reader.getAttribute("nombreplan"));
        resultado.setTitulo(reader.getAttribute("titulo"));
        resultado.setValue(reader.getValue());

        return resultado;
    }

    /**
     * Indica si se debe usar el conversor.
     * @param clazz Clase del objeto a ser convertido.
     * @return true si es necesario convertir.
     */
    public boolean canConvert(Class clazz) {
        return clazz.equals(Resultado.class);
    }

}
