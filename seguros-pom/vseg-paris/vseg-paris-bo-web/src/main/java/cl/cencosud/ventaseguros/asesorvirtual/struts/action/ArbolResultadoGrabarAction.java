package cl.cencosud.ventaseguros.asesorvirtual.struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.asesorvirtual.model.Resultado;
import cl.cencosud.ventaseguros.asesorvirtual.model.Seccion;
import cl.cencosud.ventaseguros.asesorvirtual.model.Sitio;
import cl.cencosud.ventaseguros.asesorvirtual.struts.form.ArbolResultadoAgregarForm;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class ArbolResultadoGrabarAction extends Action {
    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        ArbolResultadoAgregarForm oForm = (ArbolResultadoAgregarForm) form;

        //Obtener arbol de sesion.
        Sitio sitio = (Sitio) request.getSession().getAttribute("sitio");

        String[] actual = oForm.getActual().replace("A", "").split("_");

        int seccionPos = Integer.valueOf(actual[0]);
        int preguntasPos = Integer.valueOf(actual[1]);
        //int alternativasPos = Integer.valueOf(actual[2]);
        int alternativaPos = Integer.valueOf(actual[3]);

        Resultado resultado = new Resultado();
        resultado.setId(0);
        resultado.setIcono(oForm.getIcono());
        resultado.setTitulo(oForm.getTitulo());
        resultado.setLink0(oForm.getLink0());
        resultado.setLink1(oForm.getLink1());
        resultado.setNombreplan(oForm.getNombreplan());
        resultado.setValue("\n\t\t\t\t\n\t\t\t\t\t\t" + oForm.getValue()
            + "\n\t\t\t\t\n\t\t\t");

        Seccion seccion = sitio.getSecciones().get(seccionPos);

        seccion.getRespuestas().getResultados().add(resultado);

        int respuesta = seccion.getRespuestas().getResultados().size() - 1;
        resultado.setId(respuesta);

        //Actualizar pregunta apuntando al resultado.
        seccion.getPreguntas().getPreguntas().get(preguntasPos)
            .getAlternativas().get(alternativaPos).setRespuesta(respuesta);

        //Agregar resultado en la bd.
        //AsesorVirtualDelegate delegate = new AsesorVirtualDelegate();
        //delegate.resultadoGrabar(resultado, seccionPos, "ghost");

        //Actualizar en la bd.
        //delegate.actualizarAlternativa(alternativaPos, seccionPos,
        //    preguntasPos, respuesta, "ghost");

        request.setAttribute("resultado", true);

        return mapping.findForward("continue");
    }
}
