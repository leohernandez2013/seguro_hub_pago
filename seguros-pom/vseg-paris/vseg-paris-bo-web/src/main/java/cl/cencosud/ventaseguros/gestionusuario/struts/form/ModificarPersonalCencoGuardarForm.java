/*
 * Generated by MyEclipse Struts
 * Template path: templates/java/JavaClass.vtl
 */
package cl.cencosud.ventaseguros.gestionusuario.struts.form;

import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;

import cl.tinet.common.struts.form.BuilderActionFormBaseVSP;

/**
 * MyEclipse Struts Creation date: 09-07-2010.
 *
 * XDoclet definition:
 *
 * @struts.form name="nuevoPersonalCencoGuardarForm"
 */
public class ModificarPersonalCencoGuardarForm extends BuilderActionFormBaseVSP {

    /**
     * Atributo para la serializacion.
     */
    private static final long serialVersionUID = 1L;
    /**
     * Atributo del id Select.
     */
    private int idSelect;

    @Override
    public InputStream getValidationRules(HttpServletRequest request) {
        return ModificarPersonalCencoGuardarForm.class
            .getResourceAsStream("resource/validation.xml");
    }

    /**
     * Metodo que obtiene el id Select.
     * @return
     */
    public int getIdSelect() {
        return idSelect;
    }

    /**
     * Metodo que setea del idSelect.
     * @param idSelect
     */
    public void setIdSelect(int idSelect) {
        this.idSelect = idSelect;
    }

}