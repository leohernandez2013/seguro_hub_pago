package cl.cencosud.ventaseguros.comparador.struts.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import com.tinet.exceptions.system.SystemException;

import cl.cencosud.ventaseguros.common.PlanLeg;
import cl.cencosud.ventaseguros.comparador.model.Caracteristica;

public class EliminarContenidoTempAction extends Action {
	
	private static final int TIPO_BENEFICIO = 1;
	private static final int TIPO_COBERTURA_PARTICULAR = 2;
	private static final int TIPO_COBERTURA_COMPARTIDA = 3;
	private static final int TIPO_EXCLUSION = 4;

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {
			
			String oIdCaracteristica = request.getParameter("idCaracteristica");
			String oIdTipo = request.getParameter("idTipo");
			Integer codCaracteristica = Integer.valueOf(oIdCaracteristica);
			LinkedHashMap<String, LinkedHashMap<String, ?>> result = null;
			
			switch (Integer.valueOf(oIdTipo)) {
			case TIPO_BENEFICIO: result = this.deleteContenidoCaracteristicas(request, codCaracteristica);break;
			case TIPO_COBERTURA_PARTICULAR: result = this.deleteContenidoCaracteristicas(request, codCaracteristica);break;
			case TIPO_COBERTURA_COMPARTIDA: result = this.deleteContenidoCobCompartida(request, codCaracteristica);break;
			case TIPO_EXCLUSION: result = this.deleteContenidoCaracteristicas(request, codCaracteristica);break;
			}
			
			response.setHeader("pragma", "no-cache");
			response.setHeader("cache-control", "no-cache");
			response.setDateHeader("expires", -1);
			response.setContentType("text/html");

			PrintWriter pwritter = response.getWriter();
			ObjectMapper mapper = new ObjectMapper();
			StringWriter json = new StringWriter();
			mapper.writeValue(json, result);
			pwritter.write(json.toString());
						
		} catch (IOException e) {
			throw new SystemException(e);
		}

		return null;
	}
	
	@SuppressWarnings("unchecked")
	private LinkedHashMap<String, LinkedHashMap<String, ?>> deleteContenidoCobCompartida(
			HttpServletRequest request, Integer codCaracteristica) {
		
		LinkedHashMap<Integer, Caracteristica> mapCaractComp = (LinkedHashMap<Integer, Caracteristica>) 
			request.getSession().getAttribute("cl.cencosud.ventaseguros.comparador.mapCaractCompartidas");
		
		LinkedHashMap<String, List<Caracteristica>> mapTemp = new LinkedHashMap<String, List<Caracteristica>>();
		List<Caracteristica> listTemp = new ArrayList<Caracteristica>();
		
		if (mapCaractComp != null && mapCaractComp.size() > 0) {
			if (mapCaractComp.containsKey(codCaracteristica)) {
				Caracteristica caractTemp = mapCaractComp.get(codCaracteristica);
				caractTemp.setEs_eliminado("del");
			}
			
			Collection c = mapCaractComp.values();
			Iterator iter = c.iterator();
			
			while(iter.hasNext()) {
				Caracteristica carTemp = (Caracteristica)iter.next();
				
				if (!carTemp.getEs_eliminado().trim().equals("del")) {
					listTemp.add(carTemp);
				}
			}
			
			mapTemp.put("listCaractComp", listTemp);
		}
		
		LinkedHashMap<String, LinkedHashMap<String, ?>> result = new LinkedHashMap<String, LinkedHashMap<String, ?>>();
		result.put("mapCaractComp", mapTemp);		
		request.getSession().setAttribute("cl.cencosud.ventaseguros.comparador.mapCaractCompartidas", mapCaractComp);
		return result;
	}

	@SuppressWarnings("unchecked")
	private LinkedHashMap<String, LinkedHashMap<String, ?>> deleteContenidoCaracteristicas (HttpServletRequest request, Integer codCaracteristica) {
		LinkedHashMap<Integer, Caracteristica> mapCaracteristicas = (LinkedHashMap<Integer, Caracteristica>) 
			request.getSession().getAttribute("cl.cencosud.ventaseguros.comparador.mapCaracteristicas");
		LinkedHashMap<Integer, List<Caracteristica>> mapPlanCaracteristica = (LinkedHashMap<Integer, List<Caracteristica>>)
			request.getSession().getAttribute("cl.cencosud.ventaseguros.comparador.mapPlanCaracteristica");
		List<PlanLeg> listPlanes = (List<PlanLeg>)
			request.getSession().getAttribute("cl.cencosud.ventaseguros.comparador.listPlanesPorProducto");

		if (mapCaracteristicas != null && mapCaracteristicas.size() > 0) {
			if (mapCaracteristicas.containsKey(codCaracteristica)) {
				Caracteristica caractTemp = mapCaracteristicas.get(codCaracteristica);
				caractTemp.setEs_eliminado("del");
			}
		}
	
		LinkedHashMap<String, ?> mapCaract = this.getNormalizedRows(mapCaracteristicas, mapPlanCaracteristica, codCaracteristica);
		LinkedHashMap<String, ?> mapPlanes = this.getColumnData(listPlanes);
		
		LinkedHashMap<String, LinkedHashMap<String, ?>> result = new LinkedHashMap<String, LinkedHashMap<String,?>>();
		
		if (mapCaract != null && mapCaract.size() > 0) {
			result.put("mapCaract", mapCaract);
		}			
		result.put("mapPlanes", mapPlanes);
		
		request.getSession().setAttribute("cl.cencosud.ventaseguros.comparador.mapCaracteristicas", mapCaracteristicas);
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	private LinkedHashMap<String, List<Caracteristica>> getNormalizedRows(
			LinkedHashMap<Integer, Caracteristica> mapCaracteristicas, 
			LinkedHashMap<Integer, List<Caracteristica>> mapPlanCaracteristica, 
			Integer codCaract) {
		
		LinkedHashMap<String, List<Caracteristica>> result = null;
		
		if (mapCaracteristicas != null && mapCaracteristicas.size() > 0) {
			if (mapPlanCaracteristica != null && mapPlanCaracteristica.size() > 0) {
				result = new LinkedHashMap<String, List<Caracteristica>>();
				
				Collection c = mapCaracteristicas.values();
				Iterator iter = c.iterator();
				
				while(iter.hasNext()) {
					Caracteristica carTemp = (Caracteristica)iter.next();
					
					if (carTemp.getEs_eliminado().trim().equalsIgnoreCase("del")) {
						if (mapPlanCaracteristica.containsKey(carTemp.getId_caracteristica())) {
							mapPlanCaracteristica.remove(carTemp.getId_caracteristica());
						}						
					}					
				}
				
				//Collection col = mapPlanCaracteristica.values();
				//Iterator ite = mapPlanCaracteristica.entrySet().iterator();
				result = (LinkedHashMap<String, List<Caracteristica>>) mapPlanCaracteristica.clone();
				
//				while(ite.hasNext()) {
//					Map.Entry<Integer, List<Caracteristica>> e = (Map.Entry<Integer, List<Caracteristica>>) ite.next();
//					result.put(String.valueOf(e.getKey()), e.getValue());
//				}
			}				
		}
			
		return result;
	}

	private LinkedHashMap<String, List<PlanLeg>> getColumnData(List<PlanLeg> listPlanes) {
		LinkedHashMap<String, List<PlanLeg>> map = new LinkedHashMap<String, List<PlanLeg>>();
		map.put("columnData", listPlanes);
		return map;
	}

}
