package cl.cencosud.ventaseguros.comparador.struts.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import com.tinet.exceptions.system.SystemException;

import cl.cencosud.ventaseguros.comparador.model.Caracteristica;
import cl.cencosud.ventaseguros.delegate.ComparadorDelegate;

public class BuscarCoberturasCompartidasAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String oEstado = request.getParameter("idEstado");
		String oIdProducto = request.getParameter("idProducto");
		
		ComparadorDelegate oDelegate = new ComparadorDelegate();
		
		try {
			
			List<Caracteristica> listCaractComp = oDelegate.obtenerCaracteristicasCompartidas(Long.valueOf(oIdProducto), Integer.valueOf(oEstado));
			this.setCaractCompSession(request, listCaractComp);
			
			LinkedHashMap<String, List<Caracteristica>> result = new LinkedHashMap<String, List<Caracteristica>>();
			result.put("listCaractComp", listCaractComp);			
			
			response.setHeader("pragma", "no-cache");
            response.setHeader("cache-control", "no-cache");
            response.setDateHeader("expires", -1);
            response.setContentType("text/html");

            PrintWriter pwritter = response.getWriter();
            ObjectMapper mapper = new ObjectMapper();
            StringWriter json = new StringWriter();
            mapper.writeValue(json, result);
            pwritter.write(json.toString());

        } catch (IOException e) {
            throw new SystemException(e);
        }
		
		return null;
	}
	
	private void setCaractCompSession (HttpServletRequest request, List<Caracteristica> listCaractComp ) {
		
		request.getSession().removeAttribute("cl.cencosud.ventaseguros.comparador.mapCaractCompartidas");
		LinkedHashMap<Integer, Caracteristica> mapCaractComp = new LinkedHashMap<Integer, Caracteristica>();
		
		if (listCaractComp != null & listCaractComp.size() > 0) {
			for (Caracteristica ca : listCaractComp) {
				mapCaractComp.put(ca.getId_caracteristica(), ca);
			}
		}
		
		request.getSession().setAttribute("cl.cencosud.ventaseguros.comparador.mapCaractCompartidas", mapCaractComp);		
	}
}
