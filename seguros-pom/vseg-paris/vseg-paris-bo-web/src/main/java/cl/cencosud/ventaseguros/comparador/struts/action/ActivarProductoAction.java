package cl.cencosud.ventaseguros.comparador.struts.action;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import cl.cencosud.ventaseguros.delegate.ComparadorDelegate;

public class ActivarProductoAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		Map<String, String> result = new LinkedHashMap<String, String>();
		
		String oProductos = request.getParameter("arrProductos");
		String arrProducto[] = oProductos.split(";");
		ComparadorDelegate delegate = new ComparadorDelegate();
		
		for (String producto : arrProducto) {
			String idProducto = producto.split("_")[0];
			int estado = 0;
			if (Boolean.parseBoolean(producto.split("_")[1])) {
				estado = 1;
			}
			
			delegate.activarProducto(Long.valueOf(idProducto), estado);
		}
		
		result.put("mensaje", "Se actualizo el estado del producto satisfactoriamente.");
		response.setHeader("pragma", "no-cache");
        response.setHeader("cache-control", "no-cache");
        response.setDateHeader("expires", -1);
        response.setContentType("text/html");

        PrintWriter pwriter = response.getWriter();
        ObjectMapper mapper = new ObjectMapper();
        StringWriter json = new StringWriter();
        mapper.writeValue(json, result);
        pwriter.write(json.toString());
		
		return null;
	}
	
	

}
