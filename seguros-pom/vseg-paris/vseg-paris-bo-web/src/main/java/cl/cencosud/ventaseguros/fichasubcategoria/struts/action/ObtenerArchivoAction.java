package cl.cencosud.ventaseguros.fichasubcategoria.struts.action;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.delegate.FichaSubcategoriaDelegate;

import com.tinet.exceptions.system.SystemException;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class ObtenerArchivoAction extends Action {

    private static final String IMAGE_NOT_FOUND =
        "/cl/cencosud/ventaseguros/admin/struts/not.gif";

    private static final int BUFFER_SIZE = 4096;

    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        FichaSubcategoriaDelegate delegate = new FichaSubcategoriaDelegate();

        try {
            ServletOutputStream out = response.getOutputStream();

            if (request.getParameter("id_ficha") == null) {
                String mensaje = "Falta id_ficha!";
                out.write(mensaje.getBytes());

            } else {
                Long idFicha = Long.valueOf(request.getParameter("id_ficha"));
                String tipo = request.getParameter("tipo");
                Map < String, Object > archivo =
                    delegate.obtenerArchivo(idFicha, tipo);

                if (archivo != null && archivo.get("archivo") != null) {
                    response.setHeader("Pragma", "no-cache");
                    response.setHeader("cache-control", "no-store, no-cache, must-revalidate");
                    response.setDateHeader("expires", -1);

                    if ("imagen".equals(tipo)) {
                        response.setContentType("image/jpeg");
                        response.setHeader("Content-Disposition",
                            "inline;filename=imagen.jpg");
                    } else {
                        response.setContentType("text/html");
                        response.setHeader("Content-Disposition",
                            "inline;filename=pagina_" + tipo + ".htm");
                    }

                    byte[] imagen = (byte[]) archivo.get("archivo");
                    out.write(imagen, 0, imagen.length);

                } else {
                    if ("imagen".equals(tipo)) {
                        response.setHeader("Pragma", "no-cache");
                        response.setHeader("cache-control", "no-store, no-cache, must-revalidate");
                        response.setDateHeader("expires", -1);
                        //Cargar imagen por defecto.
                        response.setContentType("image/gif");
                        response.setHeader("Content-Disposition",
                            "inline;filename=not.gif");

                        InputStream in =
                            this.getClass().getClassLoader()
                                .getResourceAsStream(IMAGE_NOT_FOUND);
                        byte[] outputByte = new byte[BUFFER_SIZE];
                        while (in.read(outputByte, 0, BUFFER_SIZE) != -1) {
                            out.write(outputByte, 0, BUFFER_SIZE);
                        }
                        in.close();

                    } else {
                        String mensaje = "No existe archivo!";
                        out.write(mensaje.getBytes());
                    }
                }
            }
            out.flush();
            out.close();

        } catch (IOException e) {
            throw new SystemException(e);
        }

        return null;
    }
}
