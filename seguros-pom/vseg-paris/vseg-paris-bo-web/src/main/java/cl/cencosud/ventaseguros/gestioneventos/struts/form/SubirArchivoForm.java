/*
 * Generated by MyEclipse Struts
 * Template path: templates/java/JavaClass.vtl
 */
package cl.cencosud.ventaseguros.gestioneventos.struts.form;

import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.upload.FormFile;
import cl.tinet.common.struts.form.BuilderActionFormBaseVSP;

/** 
 * MyEclipse Struts
 * Creation date: 09-18-2010
 * 
 * XDoclet definition:
 * @struts.form name="subirArchivoForm"
 */
public class SubirArchivoForm extends BuilderActionFormBaseVSP {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     * archivo.
     */
    private FormFile archivo;
    /**
     * archivo de rut.
     */
    private FormFile cargaRut;
    /**
     * Id evento.
     */
    private int id_evento;

    /**
     * Metodo de validacio de formulario.
     * @param request
     * @return
     */
    @Override
    public InputStream getValidationRules(HttpServletRequest request) {
        return SubirArchivoForm.class
            .getResourceAsStream("resource/validation.xml");
    }

    /**
     * Metodo relacionado con obtenerArchivo.
     * @return
     */
    public FormFile getArchivo() {
        return archivo;
    }

    /**
     * Metodo relacionado con enviar Archivos.
     * @param archivo
     */
    public void setArchivo(FormFile archivo) {
        this.archivo = archivo;
    }

    /**
     * Metodo relacionado con obtener Id evento.
     * @return
     */
    public int getId_evento() {
        return id_evento;
    }

    /**
     * Metodo relacionado con enviar el ID evento.
     * @param id_evento
     */
    public void setId_evento(int id_evento) {
        this.id_evento = id_evento;
    }

    /**
     * Metodo relacionado con obtener el archivo rut.
     * @return
     */
    public FormFile getCargaRut() {
        return cargaRut;
    }

    /**
     * Metodo relacionado con enviar el archivo rut.
     * @param cargaRut
     */
    public void setCargaRut(FormFile cargaRut) {
        this.cargaRut = cargaRut;
    }

}