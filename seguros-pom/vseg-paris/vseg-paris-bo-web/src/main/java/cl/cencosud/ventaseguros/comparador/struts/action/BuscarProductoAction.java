package cl.cencosud.ventaseguros.comparador.struts.action;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import cl.cencosud.ventaseguros.delegate.ComparadorDelegate;

public class BuscarProductoAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		int idRama = -1;
		int idSubcategoria = -1;
		Long idProducto = -1l;
		int idTipo = 1; //1:Activo - 0:Inactivo
		
		String oIdRama = request.getParameter("idRama");
        String oIdSubcategoria = request.getParameter("idSubcategoria");
        String oIdProducto = request.getParameter("idProducto");
        String oIdTipo = request.getParameter("idTipo");
        
        if (oIdRama != null && !oIdRama.trim().equals("")) {
        	idRama = Integer.valueOf(oIdRama);
        }
        
        if (oIdSubcategoria != null && !oIdSubcategoria.trim().equals("")) {
        	idSubcategoria = Integer.valueOf(oIdSubcategoria);
        }
        
        if (oIdProducto != null && !oIdProducto.trim().equals("")) {
        	idProducto = Long.valueOf(oIdProducto);
        }
        
        if (oIdTipo != null && !oIdTipo.trim().equals("")) {
        	idTipo = Integer.valueOf(oIdTipo);
        }
        
        ComparadorDelegate delegate = new ComparadorDelegate();
                
        List < LinkedHashMap < String, ? > > list = delegate.obtenerProductos(idRama, idSubcategoria, idProducto, idTipo);
        
        response.setHeader("pragma", "no-cache");
        response.setHeader("cache-control", "no-cache");
        response.setDateHeader("expires", -1);
        response.setContentType("text/html");

        PrintWriter pwriter = response.getWriter();
        ObjectMapper mapper = new ObjectMapper();
        StringWriter json = new StringWriter();
        mapper.writeValue(json, list);
        pwriter.write(json.toString());
        
		return null;
	}

	
}
