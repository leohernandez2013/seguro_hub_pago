package cl.cencosud.ventaseguros.informescomerciales.struts.action;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import cl.cencosud.ventaseguros.delegate.InformesComercialesDelegate;
import cl.cencosud.ventaseguros.informescomerciales.struts.form.InformeComercialTarjetasForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AdministradorPromocionesRamaAction extends Action{

/**
* @param args
*/
public ActionForward execute(ActionMapping mapping, ActionForm form,
	        HttpServletRequest request, HttpServletResponse response)
	        throws Exception {

	InformeComercialTarjetasForm oForm =
        (InformeComercialTarjetasForm) form;
	
	InformesComercialesDelegate oDelegate =
        new InformesComercialesDelegate();
	SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
    String fechaDesde =
        oForm.getDatos().get("fechaDesde") == null ? "" : oForm.getDatos()
            .get("fechaDesde");
    String fechaHasta =
        oForm.getDatos().get("fechaHasta") == null ? "" : oForm.getDatos()
            .get("fechaHasta");
    String fechaDesdeH =
        oForm.getDatos().get("fechaDesdeH") == null ? "" : oForm.getDatos()
            .get("fechaDesdeH");
    String fechaHastaH =
        oForm.getDatos().get("fechaHastaH") == null ? "" : oForm.getDatos()
            .get("fechaHastaH");
    String fechaDesdeV =
        oForm.getDatos().get("fechaDesdeV") == null ? "" : oForm.getDatos()
            .get("fechaDesdeV");
    String fechaHastaV =
        oForm.getDatos().get("fechaHastaV") == null ? "" : oForm.getDatos()
            .get("fechaHastaV");
    String fechaDesdeS =
        oForm.getDatos().get("fechaDesdeS") == null ? "" : oForm.getDatos()
            .get("fechaDesdeS");
    String fechaHastaS =
        oForm.getDatos().get("fechaHastaS") == null ? "" : oForm.getDatos()
            .get("fechaHastaS");
    String fechaDesdeVB1 =
        oForm.getDatos().get("fechaDesdeVB1") == null ? "" : oForm.getDatos()
            .get("fechaDesdeVB1");
    String fechaHastaVB1 =
        oForm.getDatos().get("fechaHastaVB1") == null ? "" : oForm.getDatos()
            .get("fechaHastaVB1");
    String fechaDesdeVB2 =
        oForm.getDatos().get("fechaDesdeVB2") == null ? "" : oForm.getDatos()
            .get("fechaDesdeVB2");
    String fechaHastaVB2 =
        oForm.getDatos().get("fechaHastaVB2") == null ? "" : oForm.getDatos()
            .get("fechaHastaVB2");
    String fechaDesdeVB3 =
        oForm.getDatos().get("fechaDesdeVB3") == null ? "" : oForm.getDatos()
            .get("fechaDesdeVB3");
    String fechaHastaVB3 =
        oForm.getDatos().get("fechaHastaVB3") == null ? "" : oForm.getDatos()
            .get("fechaHastaVB3");
    String fechaDesdeVB4 =
        oForm.getDatos().get("fechaDesdeVB4") == null ? "" : oForm.getDatos()
            .get("fechaDesdeVB4");
    String fechaHastaVB4 =
        oForm.getDatos().get("fechaHastaVB4") == null ? "" : oForm.getDatos()
            .get("fechaHastaVB4");
    String fechaDesdeVB5 =
        oForm.getDatos().get("fechaDesdeVB5") == null ? "" : oForm.getDatos()
            .get("fechaDesdeVB5");
    String fechaHastaVB5 =
        oForm.getDatos().get("fechaHastaVB5") == null ? "" : oForm.getDatos()
            .get("fechaHastaVB5");
    String fechaDesdeVB6 =
        oForm.getDatos().get("fechaDesdeVB6") == null ? "" : oForm.getDatos()
            .get("fechaDesdeVB6");
    String fechaHastaVB6 =
        oForm.getDatos().get("fechaHastaVB6") == null ? "" : oForm.getDatos()
            .get("fechaHastaVB6");
    
    String fechaDesdeHB1 =
        oForm.getDatos().get("fechaDesdeHB1") == null ? "" : oForm.getDatos()
            .get("fechaDesdeHB1");
    String fechaHastaHB1 =
        oForm.getDatos().get("fechaHastaHB1") == null ? "" : oForm.getDatos()
            .get("fechaHastaHB1");
    String fechaDesdeHB2 =
        oForm.getDatos().get("fechaDesdeHB2") == null ? "" : oForm.getDatos()
            .get("fechaDesdeHB2");
    String fechaHastaHB2 =
        oForm.getDatos().get("fechaHastaHB2") == null ? "" : oForm.getDatos()
            .get("fechaHastaHB2");
    String fechaDesdeHB3 =
        oForm.getDatos().get("fechaDesdeHB3") == null ? "" : oForm.getDatos()
            .get("fechaDesdeHB3");
    String fechaHastaHB3 =
        oForm.getDatos().get("fechaHastaHB3") == null ? "" : oForm.getDatos()
            .get("fechaHastaHB3");
    String fechaDesdeHB4 =
        oForm.getDatos().get("fechaDesdeHB4") == null ? "" : oForm.getDatos()
            .get("fechaDesdeHB4");
    String fechaHastaHB4 =
        oForm.getDatos().get("fechaHastaHB4") == null ? "" : oForm.getDatos()
            .get("fechaHastaHB4");
    String fechaDesdeHB5 =
        oForm.getDatos().get("fechaDesdeHB5") == null ? "" : oForm.getDatos()
            .get("fechaDesdeHB5");
    String fechaHastaHB5 =
        oForm.getDatos().get("fechaHastaHB5") == null ? "" : oForm.getDatos()
            .get("fechaHastaHB5");
    String fechaDesdeHB6 =
        oForm.getDatos().get("fechaDesdeHB6") == null ? "" : oForm.getDatos()
            .get("fechaDesdeHB6");
    String fechaHastaHB6 =
        oForm.getDatos().get("fechaHastaHB6") == null ? "" : oForm.getDatos()
            .get("fechaHastaHB6");
    
    String fechaDesdeVIB1 =
        oForm.getDatos().get("fechaDesdeVIB1") == null ? "" : oForm.getDatos()
            .get("fechaDesdeVIB1");
    String fechaHastaVIB1 =
        oForm.getDatos().get("fechaHastaVIB1") == null ? "" : oForm.getDatos()
            .get("fechaHastaVIB1");
    String fechaDesdeVIB2 =
        oForm.getDatos().get("fechaDesdeVIB2") == null ? "" : oForm.getDatos()
            .get("fechaDesdeVIB2");
    String fechaHastaVIB2 =
        oForm.getDatos().get("fechaHastaVIB2") == null ? "" : oForm.getDatos()
            .get("fechaHastaVIB2");
    String fechaDesdeVIB3 =
        oForm.getDatos().get("fechaDesdeVIB3") == null ? "" : oForm.getDatos()
            .get("fechaDesdeVIB3");
    String fechaHastaVIB3 =
        oForm.getDatos().get("fechaHastaVIB3") == null ? "" : oForm.getDatos()
            .get("fechaHastaVIB3");
    String fechaDesdeVIB4 =
        oForm.getDatos().get("fechaDesdeVIB4") == null ? "" : oForm.getDatos()
            .get("fechaDesdeVIB4");
    String fechaHastaVIB4 =
        oForm.getDatos().get("fechaHastaVIB4") == null ? "" : oForm.getDatos()
            .get("fechaHastaVIB4");
    String fechaDesdeVIB5 =
        oForm.getDatos().get("fechaDesdeVIB5") == null ? "" : oForm.getDatos()
            .get("fechaDesdeVIB5");
    String fechaHastaVIB5 =
        oForm.getDatos().get("fechaHastaVIB5") == null ? "" : oForm.getDatos()
            .get("fechaHastaVIB5");
    String fechaDesdeVIB6 =
        oForm.getDatos().get("fechaDesdeVIB6") == null ? "" : oForm.getDatos()
            .get("fechaDesdeVIB6");
    String fechaHastaVIB6 =
        oForm.getDatos().get("fechaHastaVIB6") == null ? "" : oForm.getDatos()
            .get("fechaHastaVIB6");
    
    String fechaDesdeSB1 =
        oForm.getDatos().get("fechaDesdeSB1") == null ? "" : oForm.getDatos()
            .get("fechaDesdeSB1");
    String fechaHastaSB1 =
        oForm.getDatos().get("fechaHastaSB1") == null ? "" : oForm.getDatos()
            .get("fechaHastaSB1");
    String fechaDesdeSB2 =
        oForm.getDatos().get("fechaDesdeSB2") == null ? "" : oForm.getDatos()
            .get("fechaDesdeSB2");
    String fechaHastaSB2 =
        oForm.getDatos().get("fechaHastaSB2") == null ? "" : oForm.getDatos()
            .get("fechaHastaSB2");
    String fechaDesdeSB3 =
        oForm.getDatos().get("fechaDesdeSB3") == null ? "" : oForm.getDatos()
            .get("fechaDesdeSB3");
    String fechaHastaSB3 =
        oForm.getDatos().get("fechaHastaSB3") == null ? "" : oForm.getDatos()
            .get("fechaHastaSB3");
    String fechaDesdeSB4 =
        oForm.getDatos().get("fechaDesdeSB4") == null ? "" : oForm.getDatos()
            .get("fechaDesdeSB4");
    String fechaHastaSB4 =
        oForm.getDatos().get("fechaHastaSB4") == null ? "" : oForm.getDatos()
            .get("fechaHastaSB4");
    String fechaDesdeSB5 =
        oForm.getDatos().get("fechaDesdeSB5") == null ? "" : oForm.getDatos()
            .get("fechaDesdeSB5");
    String fechaHastaSB5 =
        oForm.getDatos().get("fechaHastaSB5") == null ? "" : oForm.getDatos()
            .get("fechaHastaSB5");
    String fechaDesdeSB6 =
        oForm.getDatos().get("fechaDesdeSB6") == null ? "" : oForm.getDatos()
            .get("fechaDesdeSB6");
    String fechaHastaSB6 =
        oForm.getDatos().get("fechaHastaSB6") == null ? "" : oForm.getDatos()
            .get("fechaHastaSB6");
    
	String rama  = oForm.getDatos().get("hiddRama");
	String rama2 = oForm.getDatos().get("hiddRamaH");
	String rama3 = oForm.getDatos().get("hiddRamaV");
	String rama4 = oForm.getDatos().get("hiddRamaS");
	String ramaV = oForm.getDatos().get("hiddRamaVB");
    String subcategoria = oForm.getDatos().get("hiddSubcategoria"); 
    String subcategoria2 = oForm.getDatos().get("hiddSubcategoriaH"); 
    String subcategoria3 = oForm.getDatos().get("hiddSubcategoriaV");
    
    
    //String respuesta = "ingresada";
    if (subcategoria != null){
    if (subcategoria.equalsIgnoreCase("1")){
    
        	
         if (rama.equalsIgnoreCase("1") && rama2.equalsIgnoreCase("1") && rama3.equalsIgnoreCase("1") && rama4.equalsIgnoreCase("1")){ 
        	 if (fechaHasta.equals("")){
        		 fechaHasta = "03/01/01";
        	 };
        	 if (fechaDesde.equals("")){
        		 fechaDesde = "01/01/01";
        	 }
        	 if (!fechaDesde.equals("") && !fechaHasta.equals("")) {
        	        if (!formatoFecha.parse(fechaHasta).before(
        	        formatoFecha.parse(fechaDesde))) {
        	        	String linkConocer = null;
        	             String linkCotizar = null;
        	             String posicion = null;
        	             String nombre = oForm.getDatos().get("hiddNombre");
        	             if (nombre.equals("")){
        	             	nombre = "-";
        	             }
        	             String link = oForm.getDatos().get("hiddLink");
        	             if (link.equals("")){
        	             	link = "-";
        	             }
        	             String tracker = oForm.getDatos().get("hiddTracker");
        	             if (tracker.equals("")){
        	             	tracker = "-";
        	             }
        	             String imagen = oForm.getDatos().get("hiddImagen");
        	             if (imagen.equals("")){
        	             	imagen = "-";
        	             }
        	             String idPlan = oForm.getDatos().get("hiddIdPlan");
        	             if (idPlan.equals("")){
        	             	idPlan = "0";
        	             }
        	             
        	          	if (fechaHasta.equals("")){
        	          		fechaHasta = "03/01/2001";
        	          	}
        	          	if (fechaDesde.equals("")){
        	          		fechaDesde = "01/01/2001";
        	          	}
        	String id = "R1BP";
        	String tipo = "BPXR";
        	oDelegate.ingresarPromocionSecundaria(id, tipo, posicion, nombre, link, tracker, imagen, 
        	formatoFecha.parse(fechaDesde), formatoFecha.parse(fechaHasta), subcategoria, rama, linkConocer, linkCotizar, idPlan);
        	        }}
         } else if (rama2.equalsIgnoreCase("2") && rama.equalsIgnoreCase("1") && rama3.equalsIgnoreCase("1") && rama4.equalsIgnoreCase("1")){
        	 if (fechaHastaH.equals("")){
        		 fechaHastaH = "02/01/2001";
        	 };
        	 if (fechaDesdeH.equals("")){
        		 fechaDesdeH = "01/01/2001";
        	 }
        	 if (!fechaDesdeH.equals("") && !fechaHastaH.equals("")) {
     	        if (!formatoFecha.parse(fechaHastaH).before(
     	        formatoFecha.parse(fechaDesdeH))) {
     	        	String linkConocerH = null;
     	             String linkCotizarH = null;
     	             String posicionH = null;
     	             String nombreH = oForm.getDatos().get("hiddNombreH");
     	             if (nombreH.equals("")){
     	             	nombreH = "-";
     	             }
     	             String linkH = oForm.getDatos().get("hiddLinkH");
     	             if (linkH.equals("")){
     	             	linkH = "-";
     	             }
     	             String trackerH = oForm.getDatos().get("hiddTrackerH");
     	             if (trackerH.equals("")){
     	             	trackerH = "-";
     	             }
     	             String imagenH = oForm.getDatos().get("hiddImagenH");
     	             if (imagenH.equals("")){
     	             	imagenH = "-";
     	             }
     	             String idPlan = oForm.getDatos().get("hiddIdPlanH");
     	             if (idPlan.equals("")){
     	             	idPlan = "0";
     	             }
     	             
     	          	if (fechaHastaH.equals("")){
     	          		fechaHastaH = "03/01/2001";
     	          	}
     	          	if (fechaDesdeH.equals("")){
     	          		fechaDesdeH = "01/01/2001";
     	          	}
        	String idH = "R2BP";
         	String tipoH = "BPXR";
         	oDelegate.ingresarPromocionSecundaria(idH, tipoH, posicionH, nombreH, linkH, trackerH, imagenH, 
                	formatoFecha.parse(fechaDesdeH), formatoFecha.parse(fechaHastaH), subcategoria, rama2, linkConocerH, linkCotizarH,idPlan);
     	        }}
         } else if (rama2.equalsIgnoreCase("1") && rama.equalsIgnoreCase("1") && rama3.equalsIgnoreCase("3") && rama4.equalsIgnoreCase("1")){
        	 if (fechaHastaV.equals("")){
        		 fechaHastaV = "02/01/2001";
        	 };
        	 if (fechaDesdeV.equals("")){
        		 fechaDesdeV = "01/01/2001";
        	 }
        	 if (!fechaDesdeV.equals("") && !fechaHastaV.equals("")) {
     	        if (!formatoFecha.parse(fechaHastaV).before(
     	        formatoFecha.parse(fechaDesdeV))) {
     	        	String linkConocerV = null;
     	             String linkCotizarV = null;
     	             String posicionV = null;
     	             String nombreV = oForm.getDatos().get("hiddNombreV");
     	             if (nombreV.equals("")){
     	             	nombreV = "-";
     	             }
     	             String linkV = oForm.getDatos().get("hiddLinkV");
     	             if (linkV.equals("")){
     	             	linkV = "-";
     	             }
     	             String trackerV = oForm.getDatos().get("hiddTrackerV");
     	             if (trackerV.equals("")){
     	             	trackerV = "-";
     	             }
     	             String imagenV = oForm.getDatos().get("hiddImagenV");
     	             if (imagenV.equals("")){
     	             	imagenV = "-";
     	             }
     	             String idPlan = oForm.getDatos().get("hiddIdPlanV");
     	             if (idPlan.equals("")){
     	             	idPlan = "0";
     	             }
     	             
     	          	if (fechaHastaV.equals("")){
     	          		fechaHastaV = "03/01/2001";
     	          	}
     	          	if (fechaDesdeV.equals("")){
     	          		fechaDesdeV = "01/01/2001";
     	          	}
        	String idV = "R3BP";
         	String tipoV = "BPXR";
         	oDelegate.ingresarPromocionSecundaria(idV, tipoV, posicionV, nombreV, linkV, trackerV, imagenV, 
                	formatoFecha.parse(fechaDesdeV), formatoFecha.parse(fechaHastaV), subcategoria, rama3, linkConocerV, linkCotizarV, idPlan);
     	        }}
         } else if (rama2.equalsIgnoreCase("1") && rama.equalsIgnoreCase("1") && rama3.equalsIgnoreCase("1") && rama4.equalsIgnoreCase("4")){
        	 if (fechaHastaS.equals("")){
        		 fechaHastaS = "03/01/2001";
        	 };
        	 if (fechaDesdeS.equals("")){
        		 fechaDesdeS = "01/01/2001";
        	 }
        	 if (!fechaDesdeS.equals("") && !fechaHastaS.equals("")) {
     	        if (!formatoFecha.parse(fechaHastaS).before(
     	        formatoFecha.parse(fechaDesdeS))) {
             String linkConocerS = null;
             String linkCotizarS = null;
             String posicionS = null;
             String nombreS = oForm.getDatos().get("hiddNombreS");
             if (nombreS.equals("")){
             	nombreS = "-";
             }
             String linkS = oForm.getDatos().get("hiddLinkS");
             if (linkS.equals("")){
             	linkS = "-";
             }
             String trackerS = oForm.getDatos().get("hiddTrackerS");
             if (trackerS.equals("")){
             	trackerS = "-";
             }
             String imagenS = oForm.getDatos().get("hiddImagenS");
             if (imagenS.equals("")){
             	imagenS = "-";
             }
             String idPlan = oForm.getDatos().get("hiddIdPlanS");
             if (idPlan.equals("")){
             	idPlan = "0";
             }
             
          	if (fechaHastaS.equals("")){
          		fechaHastaS = "03/01/2001";
          	}
          	if (fechaDesdeS.equals("")){
          		fechaDesdeS = "01/01/2001";
          	}
        	String idS = "R4BP";
         	String tipoS = "BPXR";
         	oDelegate.ingresarPromocionSecundaria(idS, tipoS, posicionS, nombreS, linkS, trackerS, imagenS, 
                	formatoFecha.parse(fechaDesdeS), formatoFecha.parse(fechaHastaS), subcategoria, rama4, linkConocerS, linkCotizarS,idPlan);
     	        }}
         }
       
    }
    else if (subcategoria.equalsIgnoreCase("2")){
    	if (ramaV.equalsIgnoreCase("1")){
    		String ordinalVB1 = oForm.getDatos().get("hiddOrdinalVB1");
    		String ordinalVB2 = oForm.getDatos().get("hiddOrdinalVB2");
    		String ordinalVB3 = oForm.getDatos().get("hiddOrdinalVB3");
    		String ordinalVB4 = oForm.getDatos().get("hiddOrdinalVB4");
    		String ordinalVB5 = oForm.getDatos().get("hiddOrdinalVB5");
    		String ordinalVB6 = oForm.getDatos().get("hiddOrdinalVB6");
    		if (ordinalVB1.equalsIgnoreCase("1")){
            String nombreVB1 = oForm.getDatos().get("hiddNombreVB1");
            if (nombreVB1.equals("")){
            	nombreVB1 = "-";
            }
            String linkVerVB1 = oForm.getDatos().get("hiddLinkVerVB1");
            if (linkVerVB1.equals("")){
            	linkVerVB1 = "-";
            }
            String linkConocerVB1 = oForm.getDatos().get("hiddLinkConocerVB1");
            if (linkConocerVB1.equals("")){
            	linkConocerVB1 = "-";
            }
            String linkCotizarVB1 = oForm.getDatos().get("hiddLinkCotizarVB1");
            if (linkCotizarVB1.equals("")){
            	linkCotizarVB1 = "-";
            }
            String trackerVB1 = oForm.getDatos().get("hiddTrackerVB1");
            if (trackerVB1.equals("")){
            	trackerVB1 = "-";
            }
            String imagenVB1 = oForm.getDatos().get("hiddImagenVB1");
            if (imagenVB1.equals("")){
            	imagenVB1 = "-";
            }
            String idPlan = oForm.getDatos().get("hiddIdPlanVB1");
            if (idPlan.equals("")){
            	idPlan = "0";
            }
            
         	if (fechaHastaVB1.equals("")){
         		fechaHastaVB1 = "03/01/2001";
         	}
         	if (fechaDesdeVB1.equals("")){
         		fechaDesdeVB1 = "01/01/2001";
         	}
         	String tipoBanner = subcategoria;
    		String id = "R1BS1";
         	String tipo = "BSXR";
         	oDelegate.ingresarPromocionSecundaria(id, tipo, ordinalVB1, nombreVB1, linkVerVB1, trackerVB1, imagenVB1,
        			formatoFecha.parse(fechaDesdeVB1), formatoFecha.parse(fechaHastaVB1), tipoBanner, ramaV, linkConocerVB1, linkCotizarVB1,idPlan);
    		}
    		if (ordinalVB2.equalsIgnoreCase("2")){
    			String nombreVB2 = oForm.getDatos().get("hiddNombreVB2");
                if (nombreVB2.equals("")){
                	nombreVB2 = "-";
                }
                String linkVerVB2 = oForm.getDatos().get("hiddLinkVerVB2");
                if (linkVerVB2.equals("")){
                	linkVerVB2 = "-";
                }
                String linkConocerVB2 = oForm.getDatos().get("hiddLinkConocerVB2");
                if (linkConocerVB2.equals("")){
                	linkConocerVB2 = "-";
                }
                String linkCotizarVB2 = oForm.getDatos().get("hiddLinkCotizarVB2");
                if (linkCotizarVB2.equals("")){
                	linkCotizarVB2 = "-";
                }
                String trackerVB2 = oForm.getDatos().get("hiddTrackerVB2");
                if (trackerVB2.equals("")){
                	trackerVB2 = "-";
                }
                String imagenVB2 = oForm.getDatos().get("hiddImagenVB2");
                if (imagenVB2.equals("")){
                	imagenVB2 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanVB2");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaVB2.equals("")){
             		fechaHastaVB2 = "03/01/2001";
             	}
             	if (fechaDesdeVB2.equals("")){
             		fechaDesdeVB2 = "01/01/2001";
             	}
                String tipoBanner = subcategoria;
        		String id = "R1BS2";
             	String tipo = "BSXR";
             	oDelegate.ingresarPromocionSecundaria(id, tipo, ordinalVB2, nombreVB2, linkVerVB2, trackerVB2, imagenVB2,
            			formatoFecha.parse(fechaDesdeVB2), formatoFecha.parse(fechaHastaVB2),tipoBanner, ramaV, linkConocerVB2, linkCotizarVB2,idPlan);
    		}
    		if (ordinalVB3.equalsIgnoreCase("3")){
    			String nombreVB3 = oForm.getDatos().get("hiddNombreVB3");
                if (nombreVB3.equals("")){
                	nombreVB3 = "-";
                }
                String linkVerVB3 = oForm.getDatos().get("hiddLinkVerVB3");
                if (linkVerVB3.equals("")){
                	linkVerVB3 = "-";
                }
                String linkConocerVB3 = oForm.getDatos().get("hiddLinkConocerVB3");
                if (linkConocerVB3.equals("")){
                	linkConocerVB3 = "-";
                }
                String linkCotizarVB3 = oForm.getDatos().get("hiddLinkCotizarVB3");
                if (linkCotizarVB3.equals("")){
                	linkCotizarVB3 = "-";
                }
                String trackerVB3 = oForm.getDatos().get("hiddTrackerVB3");
                if (trackerVB3.equals("")){
                	trackerVB3 = "-";
                }
                String imagenVB3 = oForm.getDatos().get("hiddImagenVB3");
                if (imagenVB3.equals("")){
                	imagenVB3 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanVB3");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaVB3.equals("")){
             		fechaHastaVB3 = "03/01/2001";
             	}
             	if (fechaDesdeVB3.equals("")){
             		fechaDesdeVB3 = "01/01/2001";
             	}
                String tipoBanner = subcategoria;
        		String id = "R1BS3";
             	String tipo = "BSXR";
             	oDelegate.ingresarPromocionSecundaria(id, tipo, ordinalVB3, nombreVB3, linkVerVB3, trackerVB3, imagenVB3,
            			formatoFecha.parse(fechaDesdeVB3), formatoFecha.parse(fechaHastaVB3),tipoBanner, ramaV, linkConocerVB3, linkCotizarVB3,idPlan);
    		}
    		if (ordinalVB4.equalsIgnoreCase("4")){
    			String nombreVB4 = oForm.getDatos().get("hiddNombreVB4");
                if (nombreVB4.equals("")){
                	nombreVB4 = "-";
                }
                String linkVerVB4 = oForm.getDatos().get("hiddLinkVerVB4");
                if (linkVerVB4.equals("")){
                	linkVerVB4 = "-";
                }
                String linkConocerVB4 = oForm.getDatos().get("hiddLinkConocerVB4");
                if (linkConocerVB4.equals("")){
                	linkConocerVB4 = "-";
                }
                String linkCotizarVB4 = oForm.getDatos().get("hiddLinkCotizarVB4");
                if (linkCotizarVB4.equals("")){
                	linkCotizarVB4 = "-";
                }
                String trackerVB4 = oForm.getDatos().get("hiddTrackerVB4");
                if (trackerVB4.equals("")){
                	trackerVB4 = "-";
                }
                String imagenVB4 = oForm.getDatos().get("hiddImagenVB4");
                if (imagenVB4.equals("")){
                	imagenVB4 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanVB4");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaVB4.equals("")){
             		fechaHastaVB4 = "03/01/2001";
             	}
             	if (fechaDesdeVB4.equals("")){
             		fechaDesdeVB4 = "01/01/2001";
             	}
                String tipoBanner = subcategoria;
        		String id = "R1BS4";
             	String tipo = "BSXR";
             	oDelegate.ingresarPromocionSecundaria(id, tipo, ordinalVB4, nombreVB4, linkVerVB4, trackerVB4, imagenVB4,
            			formatoFecha.parse(fechaDesdeVB4), formatoFecha.parse(fechaHastaVB4),tipoBanner,ramaV, linkConocerVB4, linkCotizarVB4,idPlan);
    		}
    		if (ordinalVB5.equalsIgnoreCase("5")){
    			String nombreVB5 = oForm.getDatos().get("hiddNombreVB5");
                if (nombreVB5.equals("")){
                	nombreVB5 = "-";
                }
                String linkVerVB5 = oForm.getDatos().get("hiddLinkVerVB5");
                if (linkVerVB5.equals("")){
                	linkVerVB5 = "-";
                }
                String linkConocerVB5 = oForm.getDatos().get("hiddLinkConocerVB5");
                if (linkConocerVB5.equals("")){
                	linkConocerVB5 = "-";
                }
                String linkCotizarVB5 = oForm.getDatos().get("hiddLinkCotizarVB5");
                if (linkCotizarVB5.equals("")){
                	linkCotizarVB5 = "-";
                }
                String trackerVB5 = oForm.getDatos().get("hiddTrackerVB5");
                if (trackerVB5.equals("")){
                	trackerVB5 = "-";
                }
                String imagenVB5 = oForm.getDatos().get("hiddImagenVB5");
                if (imagenVB5.equals("")){
                	imagenVB5 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanVB5");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaVB5.equals("")){
             		fechaHastaVB5 = "03/01/2001";
             	}
             	if (fechaDesdeVB5.equals("")){
             		fechaDesdeVB5 = "01/01/2001";
             	}
                String tipoBanner = subcategoria;
        		String id = "R1BS5";
             	String tipo = "BSXR";
             	oDelegate.ingresarPromocionSecundaria(id, tipo, ordinalVB5, nombreVB5, linkVerVB5, trackerVB5, imagenVB5,
            			formatoFecha.parse(fechaDesdeVB5), formatoFecha.parse(fechaHastaVB5),tipoBanner,ramaV, linkConocerVB5, linkCotizarVB5,idPlan);
    		}
    		if (ordinalVB6.equalsIgnoreCase("6")){
    			String nombreVB6 = oForm.getDatos().get("hiddNombreVB6");
                if (nombreVB6.equals("")){
                	nombreVB6 = "-";
                }
                String linkVerVB6 = oForm.getDatos().get("hiddLinkVerVB6");
                if (linkVerVB6.equals("")){
                	linkVerVB6 = "-";
                }
                String linkConocerVB6 = oForm.getDatos().get("hiddLinkConocerVB6");
                if (linkConocerVB6.equals("")){
                	linkConocerVB6 = "-";
                }
                String linkCotizarVB6 = oForm.getDatos().get("hiddLinkCotizarVB6");
                if (linkCotizarVB6.equals("")){
                	linkCotizarVB6 = "-";
                }
                String trackerVB6 = oForm.getDatos().get("hiddTrackerVB6");
                if (trackerVB6.equals("")){
                	trackerVB6 = "-";
                }
                String imagenVB6 = oForm.getDatos().get("hiddImagenVB6");
                if (imagenVB6.equals("")){
                	imagenVB6 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanVB6");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaVB6.equals("")){
             		fechaHastaVB6 = "03/01/2001";
             	}
             	if (fechaDesdeVB6.equals("")){
             		fechaDesdeVB6 = "01/01/2001";
             	}
                String tipoBanner = subcategoria;
        		String id = "R1BS6";
             	String tipo = "BSXR";
             	oDelegate.ingresarPromocionSecundaria(id, tipo, ordinalVB6, nombreVB6, linkVerVB6, trackerVB6, imagenVB6,
            			formatoFecha.parse(fechaDesdeVB6), formatoFecha.parse(fechaHastaVB6),tipoBanner,ramaV, linkConocerVB6, linkCotizarVB6,idPlan);
    		}
    	} else if (ramaV.equalsIgnoreCase("2")){
    		String ordinalHB1 = oForm.getDatos().get("hiddOrdinalHB1");
    		String ordinalHB2 = oForm.getDatos().get("hiddOrdinalHB2");
    		String ordinalHB3 = oForm.getDatos().get("hiddOrdinalHB3");
    		String ordinalHB4 = oForm.getDatos().get("hiddOrdinalHB4");
    		String ordinalHB5 = oForm.getDatos().get("hiddOrdinalHB5");
    		String ordinalHB6 = oForm.getDatos().get("hiddOrdinalHB6");
    		if (ordinalHB1.equalsIgnoreCase("1")){
    			String nombreHB1 = oForm.getDatos().get("hiddNombreHB1");
                if (nombreHB1.equals("")){
                	nombreHB1 = "-";
                }
                String linkVerHB1 = oForm.getDatos().get("hiddLinkVerHB1");
                if (linkVerHB1.equals("")){
                	linkVerHB1 = "-";
                }
                String linkConocerHB1 = oForm.getDatos().get("hiddLinkConocerHB1");
                if (linkConocerHB1.equals("")){
                	linkConocerHB1 = "-";
                }
                String linkCotizarHB1 = oForm.getDatos().get("hiddLinkCotizarHB1");
                if (linkCotizarHB1.equals("")){
                	linkCotizarHB1 = "-";
                }
                String trackerHB1 = oForm.getDatos().get("hiddTrackerHB1");
                if (trackerHB1.equals("")){
                	trackerHB1 = "-";
                }
                String imagenHB1 = oForm.getDatos().get("hiddImagenHB1");
                if (imagenHB1.equals("")){
                	imagenHB1 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanHB1");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaHB1.equals("")){
             		fechaHastaHB1 = "03/01/2001";
             	}
             	if (fechaDesdeHB1.equals("")){
             		fechaDesdeHB1 = "01/01/2001";
             	}
            String tipoBanner = subcategoria;
    		String id = "R2BS1";
         	String tipo = "BSXR";
         	oDelegate.ingresarPromocionSecundaria(id, tipo, ordinalHB1, nombreHB1, linkVerHB1, trackerHB1, imagenHB1,
        			formatoFecha.parse(fechaDesdeHB1), formatoFecha.parse(fechaHastaHB1),tipoBanner,ramaV, linkConocerHB1, linkCotizarHB1,idPlan);
    		}
    		if (ordinalHB2.equalsIgnoreCase("2")){
    			String nombreHB2 = oForm.getDatos().get("hiddNombreHB2");
                if (nombreHB2.equals("")){
                	nombreHB2 = "-";
                }
                String linkVerHB2 = oForm.getDatos().get("hiddLinkVerHB2");
                if (linkVerHB2.equals("")){
                	linkVerHB2 = "-";
                }
                String linkConocerHB2 = oForm.getDatos().get("hiddLinkConocerHB2");
                if (linkConocerHB2.equals("")){
                	linkConocerHB2 = "-";
                }
                String linkCotizarHB2 = oForm.getDatos().get("hiddLinkCotizarHB2");
                if (linkCotizarHB2.equals("")){
                	linkCotizarHB2 = "-";
                }
                String trackerHB2 = oForm.getDatos().get("hiddTrackerHB2");
                if (trackerHB2.equals("")){
                	trackerHB2 = "-";
                }
                String imagenHB2 = oForm.getDatos().get("hiddImagenHB2");
                if (imagenHB2.equals("")){
                	imagenHB2 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanHB2");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaHB2.equals("")){
             		fechaHastaHB2 = "03/01/2001";
             	}
             	if (fechaDesdeHB2.equals("")){
             		fechaDesdeHB2 = "01/01/2001";
             	}
                String tipoBanner = subcategoria;
        		String id = "R2BS2";
             	String tipo = "BSXR";
             	oDelegate.ingresarPromocionSecundaria(id, tipo, ordinalHB2, nombreHB2, linkVerHB2, trackerHB2, imagenHB2,
            			formatoFecha.parse(fechaDesdeHB2), formatoFecha.parse(fechaHastaHB2),tipoBanner,ramaV, linkConocerHB2, linkCotizarHB2,idPlan);
    		}
    		if (ordinalHB3.equalsIgnoreCase("3")){
    			String nombreHB3 = oForm.getDatos().get("hiddNombreHB3");
                if (nombreHB3.equals("")){
                	nombreHB3 = "-";
                }
                String linkVerHB3 = oForm.getDatos().get("hiddLinkVerHB3");
                if (linkVerHB3.equals("")){
                	linkVerHB3 = "-";
                }
                String linkConocerHB3 = oForm.getDatos().get("hiddLinkConocerHB3");
                if (linkConocerHB3.equals("")){
                	linkConocerHB3 = "-";
                }
                String linkCotizarHB3 = oForm.getDatos().get("hiddLinkCotizarHB3");
                if (linkCotizarHB3.equals("")){
                	linkCotizarHB3 = "-";
                }
                String trackerHB3 = oForm.getDatos().get("hiddTrackerHB3");
                if (trackerHB3.equals("")){
                	trackerHB3 = "-";
                }
                String imagenHB3 = oForm.getDatos().get("hiddImagenHB3");
                if (imagenHB3.equals("")){
                	imagenHB3 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanHB3");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaHB3.equals("")){
             		fechaHastaHB3 = "03/01/2001";
             	}
             	if (fechaDesdeHB3.equals("")){
             		fechaDesdeHB3 = "01/01/2001";
             	}
                String tipoBanner = subcategoria;
        		String id = "R2BS3";
             	String tipo = "BSXR";
             	oDelegate.ingresarPromocionSecundaria(id, tipo, ordinalHB3, nombreHB3, linkVerHB3, trackerHB3, imagenHB3,
            			formatoFecha.parse(fechaDesdeHB3), formatoFecha.parse(fechaHastaHB3),tipoBanner,ramaV, linkConocerHB3, linkCotizarHB3,idPlan);
    		}
    		if (ordinalHB4.equalsIgnoreCase("4")){
    			String nombreHB4 = oForm.getDatos().get("hiddNombreHB4");
                if (nombreHB4.equals("")){
                	nombreHB4 = "-";
                }
                String linkVerHB4 = oForm.getDatos().get("hiddLinkVerHB4");
                if (linkVerHB4.equals("")){
                	linkVerHB4 = "-";
                }
                String linkConocerHB4 = oForm.getDatos().get("hiddLinkConocerHB4");
                if (linkConocerHB4.equals("")){
                	linkConocerHB4 = "-";
                }
                String linkCotizarHB4 = oForm.getDatos().get("hiddLinkCotizarHB4");
                if (linkCotizarHB4.equals("")){
                	linkCotizarHB4 = "-";
                }
                String trackerHB4 = oForm.getDatos().get("hiddTrackerHB4");
                if (trackerHB4.equals("")){
                	trackerHB4 = "-";
                }
                String imagenHB4 = oForm.getDatos().get("hiddImagenHB4");
                if (imagenHB4.equals("")){
                	imagenHB4 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanHB4");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaHB4.equals("")){
             		fechaHastaHB4 = "03/01/2001";
             	}
             	if (fechaDesdeHB4.equals("")){
             		fechaDesdeHB4 = "01/01/2001";
             	}
                String tipoBanner = subcategoria;
        		String id = "R2BS4";
             	String tipo = "BSXR";
             	oDelegate.ingresarPromocionSecundaria(id, tipo, ordinalHB4, nombreHB4, linkVerHB4, trackerHB4, imagenHB4,
            			formatoFecha.parse(fechaDesdeHB4), formatoFecha.parse(fechaHastaHB4),tipoBanner,ramaV, linkConocerHB4, linkCotizarHB4,idPlan);
    		}
    		if (ordinalHB5.equalsIgnoreCase("5")){
    			String nombreHB5 = oForm.getDatos().get("hiddNombreHB5");
                if (nombreHB5.equals("")){
                	nombreHB5 = "-";
                }
                String linkVerHB5 = oForm.getDatos().get("hiddLinkVerHB5");
                if (linkVerHB5.equals("")){
                	linkVerHB5 = "-";
                }
                String linkConocerHB5 = oForm.getDatos().get("hiddLinkConocerHB5");
                if (linkConocerHB5.equals("")){
                	linkConocerHB5 = "-";
                }
                String linkCotizarHB5 = oForm.getDatos().get("hiddLinkCotizarHB5");
                if (linkCotizarHB5.equals("")){
                	linkCotizarHB5 = "-";
                }
                String trackerHB5 = oForm.getDatos().get("hiddTrackerHB5");
                if (trackerHB5.equals("")){
                	trackerHB5 = "-";
                }
                String imagenHB5 = oForm.getDatos().get("hiddImagenHB5");
                if (imagenHB5.equals("")){
                	imagenHB5 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanHB5");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaHB5.equals("")){
             		fechaHastaHB5 = "03/01/2001";
             	}
             	if (fechaDesdeHB5.equals("")){
             		fechaDesdeHB5 = "01/01/2001";
             	}
                String tipoBanner = subcategoria;
        		String id = "R2BS5";
             	String tipo = "BSXR";
             	oDelegate.ingresarPromocionSecundaria(id, tipo, ordinalHB5, nombreHB5, linkVerHB5, trackerHB5, imagenHB5,
            			formatoFecha.parse(fechaDesdeHB5), formatoFecha.parse(fechaHastaHB5),tipoBanner,ramaV, linkConocerHB5, linkCotizarHB5,idPlan);
    		}
    		if (ordinalHB6.equalsIgnoreCase("6")){
    			String nombreHB6 = oForm.getDatos().get("hiddNombreHB6");
                if (nombreHB6.equals("")){
                	nombreHB6 = "-";
                }
                String linkVerHB6 = oForm.getDatos().get("hiddLinkVerHB6");
                if (linkVerHB6.equals("")){
                	linkVerHB6 = "-";
                }
                String linkConocerHB6 = oForm.getDatos().get("hiddLinkConocerHB6");
                if (linkConocerHB6.equals("")){
                	linkConocerHB6 = "-";
                }
                String linkCotizarHB6 = oForm.getDatos().get("hiddLinkCotizarHB6");
                if (linkCotizarHB6.equals("")){
                	linkCotizarHB6 = "-";
                }
                String trackerHB6 = oForm.getDatos().get("hiddTrackerHB6");
                if (trackerHB6.equals("")){
                	trackerHB6 = "-";
                }
                String imagenHB6 = oForm.getDatos().get("hiddImagenHB6");
                if (imagenHB6.equals("")){
                	imagenHB6 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanHB6");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaHB6.equals("")){
             		fechaHastaHB6 = "03/01/2001";
             	}
             	if (fechaDesdeHB6.equals("")){
             		fechaDesdeHB6 = "01/01/2001";
             	}
                String tipoBanner = subcategoria;
        		String id = "R2BS6";
             	String tipo = "BSXR";
             	oDelegate.ingresarPromocionSecundaria(id, tipo, ordinalHB6, nombreHB6, linkVerHB6, trackerHB6, imagenHB6,
            			formatoFecha.parse(fechaDesdeHB6), formatoFecha.parse(fechaHastaHB6),tipoBanner,ramaV, linkConocerHB6, linkCotizarHB6,idPlan);
    		}
         } else if (ramaV.equalsIgnoreCase("3")){
        	 String ordinalVIB1 = oForm.getDatos().get("hiddOrdinalVIB1");
     		String ordinalVIB2 = oForm.getDatos().get("hiddOrdinalVIB2");
     		String ordinalVIB3 = oForm.getDatos().get("hiddOrdinalVIB3");
     		String ordinalVIB4 = oForm.getDatos().get("hiddOrdinalVIB4");
     		String ordinalVIB5 = oForm.getDatos().get("hiddOrdinalVIB5");
     		String ordinalVIB6 = oForm.getDatos().get("hiddOrdinalVIB6");
     		if (ordinalVIB1.equalsIgnoreCase("1")){
     			String nombreVIB1 = oForm.getDatos().get("hiddNombreVIB1");
                if (nombreVIB1.equals("")){
                	nombreVIB1 = "-";
                }
                String linkVerVIB1 = oForm.getDatos().get("hiddLinkVerVIB1");
                if (linkVerVIB1.equals("")){
                	linkVerVIB1 = "-";
                }
                String linkConocerVIB1 = oForm.getDatos().get("hiddLinkConocerVIB1");
                if (linkConocerVIB1.equals("")){
                	linkConocerVIB1 = "-";
                }
                String linkCotizarVIB1 = oForm.getDatos().get("hiddLinkCotizarVIB1");
                if (linkCotizarVIB1.equals("")){
                	linkCotizarVIB1 = "-";
                }
                String trackerVIB1 = oForm.getDatos().get("hiddTrackerVIB1");
                if (trackerVIB1.equals("")){
                	trackerVIB1 = "-";
                }
                String imagenVIB1 = oForm.getDatos().get("hiddImagenVIB1");
                if (imagenVIB1.equals("")){
                	imagenVIB1 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanVIB1");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaVIB1.equals("")){
             		fechaHastaVIB1 = "03/01/2001";
             	}
             	if (fechaDesdeVIB1.equals("")){
             		fechaDesdeVIB1 = "01/01/2001";
             	}
             String tipoBanner = subcategoria;
     		String id = "R3BS1";
          	String tipo = "BSXR";
          	oDelegate.ingresarPromocionSecundaria(id, tipo, ordinalVIB1, nombreVIB1, linkVerVIB1, trackerVIB1, imagenVIB1,
         			formatoFecha.parse(fechaDesdeVIB1), formatoFecha.parse(fechaHastaVIB1),tipoBanner,ramaV, linkConocerVIB1, linkCotizarVIB1,idPlan);
     		}
     		if (ordinalVIB2.equalsIgnoreCase("2")){
     			String nombreVIB2 = oForm.getDatos().get("hiddNombreVIB2");
                if (nombreVIB2.equals("")){
                	nombreVIB2 = "-";
                }
                String linkVerVIB2 = oForm.getDatos().get("hiddLinkVerVIB2");
                if (linkVerVIB2.equals("")){
                	linkVerVIB2 = "-";
                }
                String linkConocerVIB2 = oForm.getDatos().get("hiddLinkConocerVIB2");
                if (linkConocerVIB2.equals("")){
                	linkConocerVIB2 = "-";
                }
                String linkCotizarVIB2 = oForm.getDatos().get("hiddLinkCotizarVIB2");
                if (linkCotizarVIB2.equals("")){
                	linkCotizarVIB2 = "-";
                }
                String trackerVIB2 = oForm.getDatos().get("hiddTrackerVIB2");
                if (trackerVIB2.equals("")){
                	trackerVIB2 = "-";
                }
                String imagenVIB2 = oForm.getDatos().get("hiddImagenVIB2");
                if (imagenVIB2.equals("")){
                	imagenVIB2 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanVIB2");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaVIB2.equals("")){
             		fechaHastaVIB2 = "03/01/2001";
             	}
             	if (fechaDesdeVIB2.equals("")){
             		fechaDesdeVIB2 = "01/01/2001";
             	}
                 String tipoBanner = subcategoria;
         		String id = "R3BS2";
              	String tipo = "BSXR";
              	oDelegate.ingresarPromocionSecundaria(id, tipo, ordinalVIB2, nombreVIB2, linkVerVIB2, trackerVIB2, imagenVIB2,
             			formatoFecha.parse(fechaDesdeVIB2), formatoFecha.parse(fechaHastaVIB2),tipoBanner,ramaV, linkConocerVIB2, linkCotizarVIB2,idPlan);
     		}
     		if (ordinalVIB3.equalsIgnoreCase("3")){
     			String nombreVIB3 = oForm.getDatos().get("hiddNombreVIB3");
                if (nombreVIB3.equals("")){
                	nombreVIB3 = "-";
                }
                String linkVerVIB3 = oForm.getDatos().get("hiddLinkVerVIB3");
                if (linkVerVIB3.equals("")){
                	linkVerVIB3 = "-";
                }
                String linkConocerVIB3 = oForm.getDatos().get("hiddLinkConocerVIB3");
                if (linkConocerVIB3.equals("")){
                	linkConocerVIB3 = "-";
                }
                String linkCotizarVIB3 = oForm.getDatos().get("hiddLinkCotizarVIB3");
                if (linkCotizarVIB3.equals("")){
                	linkCotizarVIB3 = "-";
                }
                String trackerVIB3 = oForm.getDatos().get("hiddTrackerVIB3");
                if (trackerVIB3.equals("")){
                	trackerVIB3 = "-";
                }
                String imagenVIB3 = oForm.getDatos().get("hiddImagenVIB3");
                if (imagenVIB3.equals("")){
                	imagenVIB3 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanVIB3");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaVIB3.equals("")){
             		fechaHastaVIB3 = "03/01/2001";
             	}
             	if (fechaDesdeVIB3.equals("")){
             		fechaDesdeVIB3 = "01/01/2001";
             	}
                 String tipoBanner = subcategoria;
         		String id = "R3BS3";
              	String tipo = "BSXR";
              	oDelegate.ingresarPromocionSecundaria(id, tipo, ordinalVIB3, nombreVIB3, linkVerVIB3, trackerVIB3, imagenVIB3,
             			formatoFecha.parse(fechaDesdeVIB3), formatoFecha.parse(fechaHastaVIB3),tipoBanner,ramaV, linkConocerVIB3, linkCotizarVIB3,idPlan);
     		}
     		if (ordinalVIB4.equalsIgnoreCase("4")){
     			String nombreVIB4 = oForm.getDatos().get("hiddNombreVIB4");
                if (nombreVIB4.equals("")){
                	nombreVIB4 = "-";
                }
                String linkVerVIB4 = oForm.getDatos().get("hiddLinkVerVIB4");
                if (linkVerVIB4.equals("")){
                	linkVerVIB4 = "-";
                }
                String linkConocerVIB4 = oForm.getDatos().get("hiddLinkConocerVIB4");
                if (linkConocerVIB4.equals("")){
                	linkConocerVIB4 = "-";
                }
                String linkCotizarVIB4 = oForm.getDatos().get("hiddLinkCotizarVIB4");
                if (linkCotizarVIB4.equals("")){
                	linkCotizarVIB4 = "-";
                }
                String trackerVIB4 = oForm.getDatos().get("hiddTrackerVIB4");
                if (trackerVIB4.equals("")){
                	trackerVIB4 = "-";
                }
                String imagenVIB4 = oForm.getDatos().get("hiddImagenVIB4");
                if (imagenVIB4.equals("")){
                	imagenVIB4 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanVIB4");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaVIB4.equals("")){
             		fechaHastaVIB4 = "03/01/2001";
             	}
             	if (fechaDesdeVIB4.equals("")){
             		fechaDesdeVIB4 = "01/01/2001";
             	}
                 String tipoBanner = subcategoria;
         		String id = "R3BS4";
              	String tipo = "BSXR";
              	oDelegate.ingresarPromocionSecundaria(id, tipo, ordinalVIB4, nombreVIB4, linkVerVIB4, trackerVIB4, imagenVIB4,
             			formatoFecha.parse(fechaDesdeVIB4), formatoFecha.parse(fechaHastaVIB4),tipoBanner,ramaV, linkConocerVIB4, linkCotizarVIB4,idPlan);
     		}
     		if (ordinalVIB5.equalsIgnoreCase("5")){
     			String nombreVIB5 = oForm.getDatos().get("hiddNombreVIB5");
                if (nombreVIB5.equals("")){
                	nombreVIB5 = "-";
                }
                String linkVerVIB5 = oForm.getDatos().get("hiddLinkVerVIB5");
                if (linkVerVIB5.equals("")){
                	linkVerVIB5 = "-";
                }
                String linkConocerVIB5 = oForm.getDatos().get("hiddLinkConocerVIB5");
                if (linkConocerVIB5.equals("")){
                	linkConocerVIB5 = "-";
                }
                String linkCotizarVIB5 = oForm.getDatos().get("hiddLinkCotizarVIB5");
                if (linkCotizarVIB5.equals("")){
                	linkCotizarVIB5 = "-";
                }
                String trackerVIB5 = oForm.getDatos().get("hiddTrackerVIB5");
                if (trackerVIB5.equals("")){
                	trackerVIB5 = "-";
                }
                String imagenVIB5 = oForm.getDatos().get("hiddImagenVIB5");
                if (imagenVIB5.equals("")){
                	imagenVIB5 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanVIB5");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaVIB5.equals("")){
             		fechaHastaVIB5 = "03/01/2001";
             	}
             	if (fechaDesdeVIB5.equals("")){
             		fechaDesdeVIB5 = "01/01/2001";
             	}
                 String tipoBanner = subcategoria;
         		String id = "R3BS5";
              	String tipo = "BSXR";
              	oDelegate.ingresarPromocionSecundaria(id, tipo, ordinalVIB5, nombreVIB5, linkVerVIB5, trackerVIB5, imagenVIB5,
             			formatoFecha.parse(fechaDesdeVIB5), formatoFecha.parse(fechaHastaVIB5),tipoBanner,ramaV, linkConocerVIB5, linkCotizarVIB5,idPlan);
     		}
     		if (ordinalVIB6.equalsIgnoreCase("6")){
     			String nombreVIB6 = oForm.getDatos().get("hiddNombreVIB6");
                if (nombreVIB6.equals("")){
                	nombreVIB6 = "-";
                }
                String linkVerVIB6 = oForm.getDatos().get("hiddLinkVerVIB6");
                if (linkVerVIB6.equals("")){
                	linkVerVIB6 = "-";
                }
                String linkConocerVIB6 = oForm.getDatos().get("hiddLinkConocerVIB6");
                if (linkConocerVIB6.equals("")){
                	linkConocerVIB6 = "-";
                }
                String linkCotizarVIB6 = oForm.getDatos().get("hiddLinkCotizarVIB6");
                if (linkCotizarVIB6.equals("")){
                	linkCotizarVIB6 = "-";
                }
                String trackerVIB6 = oForm.getDatos().get("hiddTrackerVIB6");
                if (trackerVIB6.equals("")){
                	trackerVIB6 = "-";
                }
                String imagenVIB6 = oForm.getDatos().get("hiddImagenVIB6");
                if (imagenVIB6.equals("")){
                	imagenVIB6 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanVIB6");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaVIB6.equals("")){
             		fechaHastaVIB6 = "03/01/2001";
             	}
             	if (fechaDesdeVIB6.equals("")){
             		fechaDesdeVIB6 = "01/01/2001";
             	}
                 String tipoBanner = subcategoria;
         		String id = "R3BS6";
              	String tipo = "BSXR";
              	oDelegate.ingresarPromocionSecundaria(id, tipo, ordinalVIB6, nombreVIB6, linkVerVIB6, trackerVIB6, imagenVIB6,
             			formatoFecha.parse(fechaDesdeVIB6), formatoFecha.parse(fechaHastaVIB6),tipoBanner,ramaV, linkConocerVIB6, linkCotizarVIB6,idPlan);
     		}
         } else if (ramaV.equalsIgnoreCase("4")){
        	 String ordinalSB1 = oForm.getDatos().get("hiddOrdinalSB1");
      		String ordinalSB2 = oForm.getDatos().get("hiddOrdinalSB2");
      		String ordinalSB3 = oForm.getDatos().get("hiddOrdinalSB3");
      		String ordinalSB4 = oForm.getDatos().get("hiddOrdinalSB4");
      		String ordinalSB5 = oForm.getDatos().get("hiddOrdinalSB5");
      		String ordinalSB6 = oForm.getDatos().get("hiddOrdinalSB6");
      		if (ordinalSB1.equalsIgnoreCase("1")){
      			String nombreSB1 = oForm.getDatos().get("hiddNombreSB1");
                if (nombreSB1.equals("")){
                	nombreSB1 = "-";
                }
                String linkVerSB1 = oForm.getDatos().get("hiddLinkVerSB1");
                if (linkVerSB1.equals("")){
                	linkVerSB1 = "-";
                }
                String linkConocerSB1 = oForm.getDatos().get("hiddLinkConocerSB1");
                if (linkConocerSB1.equals("")){
                	linkConocerSB1 = "-";
                }
                String linkCotizarSB1 = oForm.getDatos().get("hiddLinkCotizarSB1");
                if (linkCotizarSB1.equals("")){
                	linkCotizarSB1 = "-";
                }
                String trackerSB1 = oForm.getDatos().get("hiddTrackerSB1");
                if (trackerSB1.equals("")){
                	trackerSB1 = "-";
                }
                String imagenSB1 = oForm.getDatos().get("hiddImagenSB1");
                if (imagenSB1.equals("")){
                	imagenSB1 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanSB1");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaSB1.equals("")){
             		fechaHastaSB1 = "03/01/2001";
             	}
             	if (fechaDesdeSB1.equals("")){
             		fechaDesdeSB1 = "01/01/2001";
             	}
              String tipoBanner = subcategoria;
      		String id = "R4BS1";
           	String tipo = "BSXR";
           	oDelegate.ingresarPromocionSecundaria(id, tipo, ordinalSB1, nombreSB1, linkVerSB1, trackerSB1, imagenSB1,
          			formatoFecha.parse(fechaDesdeSB1), formatoFecha.parse(fechaHastaSB1),tipoBanner,ramaV, linkConocerSB1, linkCotizarSB1,idPlan);
      		}
      		if (ordinalSB2.equalsIgnoreCase("2")){
      			String nombreSB2 = oForm.getDatos().get("hiddNombreSB2");
                if (nombreSB2.equals("")){
                	nombreSB2 = "-";
                }
                String linkVerSB2 = oForm.getDatos().get("hiddLinkVerSB2");
                if (linkVerSB2.equals("")){
                	linkVerSB2 = "-";
                }
                String linkConocerSB2 = oForm.getDatos().get("hiddLinkConocerSB2");
                if (linkConocerSB2.equals("")){
                	linkConocerSB2 = "-";
                }
                String linkCotizarSB2 = oForm.getDatos().get("hiddLinkCotizarSB2");
                if (linkCotizarSB2.equals("")){
                	linkCotizarSB2 = "-";
                }
                String trackerSB2 = oForm.getDatos().get("hiddTrackerSB2");
                if (trackerSB2.equals("")){
                	trackerSB2 = "-";
                }
                String imagenSB2 = oForm.getDatos().get("hiddImagenSB2");
                if (imagenSB2.equals("")){
                	imagenSB2 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanSB2");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaSB2.equals("")){
             		fechaHastaSB2 = "03/01/2001";
             	}
             	if (fechaDesdeSB2.equals("")){
             		fechaDesdeSB2 = "01/01/2001";
             	}
                  String tipoBanner = subcategoria;
          		String id = "R4BS2";
               	String tipo = "BSXR";
               	oDelegate.ingresarPromocionSecundaria(id, tipo, ordinalSB2, nombreSB2, linkVerSB2, trackerSB2, imagenSB2,
              			formatoFecha.parse(fechaDesdeSB2), formatoFecha.parse(fechaHastaSB2),tipoBanner,ramaV, linkConocerSB2, linkCotizarSB2,idPlan);
      		}
      		if (ordinalSB3.equalsIgnoreCase("3")){
      			String nombreSB3 = oForm.getDatos().get("hiddNombreSB3");
                if (nombreSB3.equals("")){
                	nombreSB3 = "-";
                }
                String linkVerSB3 = oForm.getDatos().get("hiddLinkVerSB3");
                if (linkVerSB3.equals("")){
                	linkVerSB3 = "-";
                }
                String linkConocerSB3 = oForm.getDatos().get("hiddLinkConocerSB3");
                if (linkConocerSB3.equals("")){
                	linkConocerSB3 = "-";
                }
                String linkCotizarSB3 = oForm.getDatos().get("hiddLinkCotizarSB3");
                if (linkCotizarSB3.equals("")){
                	linkCotizarSB3 = "-";
                }
                String trackerSB3 = oForm.getDatos().get("hiddTrackerSB3");
                if (trackerSB3.equals("")){
                	trackerSB3 = "-";
                }
                String imagenSB3 = oForm.getDatos().get("hiddImagenSB3");
                if (imagenSB3.equals("")){
                	imagenSB3 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanSB3");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaSB3.equals("")){
             		fechaHastaSB3 = "03/01/2001";
             	}
             	if (fechaDesdeSB3.equals("")){
             		fechaDesdeSB3 = "01/01/2001";
             	}
                  String tipoBanner = subcategoria;
          		String id = "R4BS3";
               	String tipo = "BSXR";
               	oDelegate.ingresarPromocionSecundaria(id, tipo, ordinalSB3, nombreSB3, linkVerSB3, trackerSB3, imagenSB3,
              			formatoFecha.parse(fechaDesdeSB3), formatoFecha.parse(fechaHastaSB3),tipoBanner,ramaV, linkConocerSB3, linkCotizarSB3,idPlan);
      		}
      		if (ordinalSB4.equalsIgnoreCase("4")){
      			String nombreSB4 = oForm.getDatos().get("hiddNombreSB4");
                if (nombreSB4.equals("")){
                	nombreSB4 = "-";
                }
                String linkVerSB4 = oForm.getDatos().get("hiddLinkVerSB4");
                if (linkVerSB4.equals("")){
                	linkVerSB4 = "-";
                }
                String linkConocerSB4 = oForm.getDatos().get("hiddLinkConocerSB4");
                if (linkConocerSB4.equals("")){
                	linkConocerSB4 = "-";
                }
                String linkCotizarSB4 = oForm.getDatos().get("hiddLinkCotizarSB4");
                if (linkCotizarSB4.equals("")){
                	linkCotizarSB4 = "-";
                }
                String trackerSB4 = oForm.getDatos().get("hiddTrackerSB4");
                if (trackerSB4.equals("")){
                	trackerSB4 = "-";
                }
                String imagenSB4 = oForm.getDatos().get("hiddImagenSB4");
                if (imagenSB4.equals("")){
                	imagenSB4 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanSB4");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaSB4.equals("")){
             		fechaHastaSB4 = "03/01/2001";
             	}
             	if (fechaDesdeSB4.equals("")){
             		fechaDesdeSB4 = "01/01/2001";
             	}
                  String tipoBanner = subcategoria;
          		String id = "R4BS4";
               	String tipo = "BSXR";
               	oDelegate.ingresarPromocionSecundaria(id, tipo, ordinalSB4, nombreSB4, linkVerSB4, trackerSB4, imagenSB4,
              			formatoFecha.parse(fechaDesdeSB4), formatoFecha.parse(fechaHastaSB4),tipoBanner,ramaV, linkConocerSB4, linkCotizarSB4,idPlan);
      		}
      		if (ordinalSB5.equalsIgnoreCase("5")){
      			String nombreSB5 = oForm.getDatos().get("hiddNombreSB5");
                if (nombreSB5.equals("")){
                	nombreSB5 = "-";
                }
                String linkVerSB5 = oForm.getDatos().get("hiddLinkVerSB5");
                if (linkVerSB5.equals("")){
                	linkVerSB5 = "-";
                }
                String linkConocerSB5 = oForm.getDatos().get("hiddLinkConocerSB5");
                if (linkConocerSB5.equals("")){
                	linkConocerSB5 = "-";
                }
                String linkCotizarSB5 = oForm.getDatos().get("hiddLinkCotizarSB5");
                if (linkCotizarSB5.equals("")){
                	linkCotizarSB5 = "-";
                }
                String trackerSB5 = oForm.getDatos().get("hiddTrackerSB5");
                if (trackerSB5.equals("")){
                	trackerSB5 = "-";
                }
                String imagenSB5 = oForm.getDatos().get("hiddImagenSB5");
                if (imagenSB5.equals("")){
                	imagenSB5 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanSB5");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaSB5.equals("")){
             		fechaHastaSB5 = "03/01/2001";
             	}
             	if (fechaDesdeSB5.equals("")){
             		fechaDesdeSB5 = "01/01/2001";
             	}
                  String tipoBanner = subcategoria;
          		String id = "RSBS5";
               	String tipo = "BSXR";
               	oDelegate.ingresarPromocionSecundaria(id, tipo, ordinalSB5, nombreSB5, linkVerSB5, trackerSB5, imagenSB5,
              			formatoFecha.parse(fechaDesdeSB5), formatoFecha.parse(fechaHastaSB5),tipoBanner,ramaV, linkConocerSB5, linkCotizarSB5,idPlan);
      		}
      		if (ordinalSB6.equalsIgnoreCase("6")){
      			String nombreSB6 = oForm.getDatos().get("hiddNombreSB6");
                if (nombreSB6.equals("")){
                	nombreSB6 = "-";
                }
                String linkVerSB6 = oForm.getDatos().get("hiddLinkVerSB6");
                if (linkVerSB6.equals("")){
                	linkVerSB6 = "-";
                }
                String linkConocerSB6 = oForm.getDatos().get("hiddLinkConocerSB6");
                if (linkConocerSB6.equals("")){
                	linkConocerSB6 = "-";
                }
                String linkCotizarSB6 = oForm.getDatos().get("hiddLinkCotizarSB6");
                if (linkCotizarSB6.equals("")){
                	linkCotizarSB6 = "-";
                }
                String trackerSB6 = oForm.getDatos().get("hiddTrackerSB6");
                if (trackerSB6.equals("")){
                	trackerSB6 = "-";
                }
                String imagenSB6 = oForm.getDatos().get("hiddImagenSB6");
                if (imagenSB6.equals("")){
                	imagenSB6 = "-";
                }
                String idPlan = oForm.getDatos().get("hiddIdPlanSB6");
                if (idPlan.equals("")){
                	idPlan = "0";
                }
                
             	if (fechaHastaSB6.equals("")){
             		fechaHastaSB6 = "03/01/2001";
             	}
             	if (fechaDesdeSB6.equals("")){
             		fechaDesdeSB6 = "01/01/2001";
             	}
                  String tipoBanner = subcategoria;
          		String id = "RSBS6";
               	String tipo = "BSXR";
               	oDelegate.ingresarPromocionSecundaria(id, tipo, ordinalSB6, nombreSB6, linkVerSB6, trackerSB6, imagenSB6,
              			formatoFecha.parse(fechaDesdeSB6), formatoFecha.parse(fechaHastaSB6),tipoBanner,ramaV, linkConocerSB6, linkCotizarSB6,idPlan);
      		}
         }
    }
    } else {
    	int i = 0;
    	List<Map<String, Object>> datosPromo = oDelegate.obtenerPromocionesBO();
    	
    	for (i = 0; i<datosPromo.size(); i++){
    		if (!datosPromo.get(i).isEmpty()){
    		if (datosPromo.get(i).get("id").equals("R1BP")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlan","");
    			}else {
    				request.setAttribute("idPlan", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombre","");
    			}else {
    				request.setAttribute("nombre", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("link","");
    			}else {
    				request.setAttribute("link", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("tracker","");
    			}else {
    				request.setAttribute("tracker", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagen","");
    			}else {
    				request.setAttribute("imagen", datosPromo.get(i).get("imagen"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R2BP")){
    			if (datosPromo.get(i).get("idPlan") ==  null){
    				request.setAttribute("idPlanH","");
    			}else {
    				request.setAttribute("idPlanH", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreH","");
    			}else {
    				request.setAttribute("nombreH", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkH","");
    			}else {
    				request.setAttribute("linkH", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerH","");
    			}else {
    				request.setAttribute("trackerH", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenH","");
    			}else {
    				request.setAttribute("imagenH", datosPromo.get(i).get("imagen"));	
    			};
    			
    		} else if (datosPromo.get(i).get("id").equals("R3BP")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanV","");
    			}else {
    				request.setAttribute("idPlanV", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreV","");
    			}else {
    				request.setAttribute("nombreV", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkV","");
    			}else {
    				request.setAttribute("linkV", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerV","");
    			}else {
    				request.setAttribute("trackerV", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenV","");
    			}else {
    				request.setAttribute("imagenV", datosPromo.get(i).get("imagen"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R4BP")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanS","");
    			}else {
    				request.setAttribute("idPlanS", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreS","");
    			}else {
    				request.setAttribute("nombreS", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkS","");
    			}else {
    				request.setAttribute("linkS", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerS","");
    			}else {
    				request.setAttribute("trackerS", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenS","");
    			}else {
    				request.setAttribute("imagenS", datosPromo.get(i).get("imagen"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R1BS1")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanVB1","");
    			}else {
    				request.setAttribute("idPlanVB1", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreVB1","");
    			}else {
    				request.setAttribute("nombreVB1", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkCotizar") == null){
    				request.setAttribute("linkCotizarVB1","");
    			}else {
    				request.setAttribute("linkCotizarVB1", datosPromo.get(i).get("linkCotizar"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerVB1","");
    			}else {
    				request.setAttribute("trackerVB1", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenVB1","");
    			}else {
    				request.setAttribute("imagenVB1", datosPromo.get(i).get("imagen"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkVerBasesVB1","");
    			}else {
    				request.setAttribute("linkVerBasesVB1", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("linkConocer") == null){
    				request.setAttribute("linkConocerVB1","");
    			}else {
    				request.setAttribute("linkConocerVB1", datosPromo.get(i).get("linkConocer"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R1BS2")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanVB2","");
    			}else {
    				request.setAttribute("idPlanVB2", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreVB2","");
    			}else {
    				request.setAttribute("nombreVB2", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkCotizar") == null){
    				request.setAttribute("linkCotizarVB2","");
    			}else {
    				request.setAttribute("linkCotizarVB2", datosPromo.get(i).get("linkCotizar"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerVB2","");
    			}else {
    				request.setAttribute("trackerVB2", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenVB2","");
    			}else {
    				request.setAttribute("imagenVB2", datosPromo.get(i).get("imagen"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkVerBasesVB2","");
    			}else {
    				request.setAttribute("linkVerBasesVB2", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("linkConocer") == null){
    				request.setAttribute("linkConocerVB2","");
    			}else {
    				request.setAttribute("linkConocerVB2", datosPromo.get(i).get("linkConocer"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R1BS3")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanVB3","");
    			}else {
    				request.setAttribute("idPlanVB3", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreVB3","");
    			}else {
    				request.setAttribute("nombreVB3", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkCotizar") == null){
    				request.setAttribute("linkCotizarVB3","");
    			}else {
    				request.setAttribute("linkCotizarVB3", datosPromo.get(i).get("linkCotizar"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerVB3","");
    			}else {
    				request.setAttribute("trackerVB3", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenVB3","");
    			}else {
    				request.setAttribute("imagenVB3", datosPromo.get(i).get("imagen"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkVerBasesVB3","");
    			}else {
    				request.setAttribute("linkVerBasesVB3", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("linkConocer") == null){
    				request.setAttribute("linkConocerVB3","");
    			}else {
    				request.setAttribute("linkConocerVB3", datosPromo.get(i).get("linkConocer"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R1BS4")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanVB4","");
    			}else {
    				request.setAttribute("idPlanVB4", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreVB4","");
    			}else {
    				request.setAttribute("nombreVB4", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkCotizar") == null){
    				request.setAttribute("linkCotizarVB4","");
    			}else {
    				request.setAttribute("linkCotizarVB4", datosPromo.get(i).get("linkCotizar"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerVB4","");
    			}else {
    				request.setAttribute("trackerVB4", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenVB4","");
    			}else {
    				request.setAttribute("imagenVB4", datosPromo.get(i).get("imagen"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkVerBasesVB4","");
    			}else {
    				request.setAttribute("linkVerBasesVB4", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("linkConocer") == null){
    				request.setAttribute("linkConocerVB4","");
    			}else {
    				request.setAttribute("linkConocerVB4", datosPromo.get(i).get("linkConocer"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R1BS5")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanVB5","");
    			}else {
    				request.setAttribute("idPlanVB5", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreVB5","");
    			}else {
    				request.setAttribute("nombreVB5", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkCotizar") == null){
    				request.setAttribute("linkCotizarVB5","");
    			}else {
    				request.setAttribute("linkCotizarVB5", datosPromo.get(i).get("linkCotizar"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerVB5","");
    			}else {
    				request.setAttribute("trackerVB5", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenVB5","");
    			}else {
    				request.setAttribute("imagenVB5", datosPromo.get(i).get("imagen"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkVerBasesVB5","");
    			}else {
    				request.setAttribute("linkVerBasesVB5", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("linkConocer") == null){
    				request.setAttribute("linkConocerVB5","");
    			}else {
    				request.setAttribute("linkConocerVB5", datosPromo.get(i).get("linkConocer"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R1BS6")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanVB6","");
    			}else {
    				request.setAttribute("idPlanVB6", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreVB6","");
    			}else {
    				request.setAttribute("nombreVB6", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkCotizar") == null){
    				request.setAttribute("linkCotizarVB6","");
    			}else {
    				request.setAttribute("linkCotizarVB6", datosPromo.get(i).get("linkCotizar"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerVB6","");
    			}else {
    				request.setAttribute("trackerVB6", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenVB6","");
    			}else {
    				request.setAttribute("imagenVB6", datosPromo.get(i).get("imagen"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkVerBasesVB6","");
    			}else {
    				request.setAttribute("linkVerBasesVB6", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("linkConocer") == null){
    				request.setAttribute("linkConocerVB6","");
    			}else {
    				request.setAttribute("linkConocerVB6", datosPromo.get(i).get("linkConocer"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R2BS1")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanHB1","");
    			}else {
    				request.setAttribute("idPlanHB1", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreHB1","");
    			}else {
    				request.setAttribute("nombreHB1", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkCotizar") == null){
    				request.setAttribute("linkCotizarHB1","");
    			}else {
    				request.setAttribute("linkCotizarHB1", datosPromo.get(i).get("linkCotizar"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerHB1","");
    			}else {
    				request.setAttribute("trackerHB1", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenHB1","");
    			}else {
    				request.setAttribute("imagenHB1", datosPromo.get(i).get("imagen"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkVerBasesHB1","");
    			}else {
    				request.setAttribute("linkVerBasesHB1", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("linkConocer") == null){
    				request.setAttribute("linkConocerHB1","");
    			}else {
    				request.setAttribute("linkConocerHB1", datosPromo.get(i).get("linkConocer"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R2BS2")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanHB2","");
    			}else {
    				request.setAttribute("idPlanHB2", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreHB2","");
    			}else {
    				request.setAttribute("nombreHB2", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkCotizar") == null){
    				request.setAttribute("linkCotizarHB2","");
    			}else {
    				request.setAttribute("linkCotizarHB2", datosPromo.get(i).get("linkCotizar"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerHB2","");
    			}else {
    				request.setAttribute("trackerHB2", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenHB2","");
    			}else {
    				request.setAttribute("imagenHB2", datosPromo.get(i).get("imagen"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkVerBasesHB2","");
    			}else {
    				request.setAttribute("linkVerBasesHB2", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("linkConocer") == null){
    				request.setAttribute("linkConocerHB2","");
    			}else {
    				request.setAttribute("linkConocerHB2", datosPromo.get(i).get("linkConocer"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R2BS3")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanHB3","");
    			}else {
    				request.setAttribute("idPlanHB3", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreHB3","");
    			}else {
    				request.setAttribute("nombreHB3", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkCotizar") == null){
    				request.setAttribute("linkCotizarHB3","");
    			}else {
    				request.setAttribute("linkCotizarHB3", datosPromo.get(i).get("linkCotizar"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerHB3","");
    			}else {
    				request.setAttribute("trackerHB3", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenHB3","");
    			}else {
    				request.setAttribute("imagenHB3", datosPromo.get(i).get("imagen"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkVerBasesHB3","");
    			}else {
    				request.setAttribute("linkVerBasesHB3", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("linkConocer") == null){
    				request.setAttribute("linkConocerHB3","");
    			}else {
    				request.setAttribute("linkConocerHB3", datosPromo.get(i).get("linkConocer"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R2BS4")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanHB4","");
    			}else {
    				request.setAttribute("idPlanHB4", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreHB4","");
    			}else {
    				request.setAttribute("nombreHB4", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkCotizar") == null){
    				request.setAttribute("linkCotizarHB4","");
    			}else {
    				request.setAttribute("linkCotizarHB4", datosPromo.get(i).get("linkCotizar"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerHB4","");
    			}else {
    				request.setAttribute("trackerHB4", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenHB4","");
    			}else {
    				request.setAttribute("imagenHB4", datosPromo.get(i).get("imagen"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkVerBasesHB4","");
    			}else {
    				request.setAttribute("linkVerBasesHB4", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("linkConocer") == null){
    				request.setAttribute("linkConocerHB4","");
    			}else {
    				request.setAttribute("linkConocerHB4", datosPromo.get(i).get("linkConocer"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R2BS5")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanHB5","");
    			}else {
    				request.setAttribute("idPlanHB5", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreHB5","");
    			}else {
    				request.setAttribute("nombreHB5", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkCotizar") == null){
    				request.setAttribute("linkCotizarHB5","");
    			}else {
    				request.setAttribute("linkCotizarHB5", datosPromo.get(i).get("linkCotizar"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerHB5","");
    			}else {
    				request.setAttribute("trackerHB5", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenHB5","");
    			}else {
    				request.setAttribute("imagenHB5", datosPromo.get(i).get("imagen"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkVerBasesHB5","");
    			}else {
    				request.setAttribute("linkVerBasesHB5", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("linkConocer") == null){
    				request.setAttribute("linkConocerHB5","");
    			}else {
    				request.setAttribute("linkConocerHB5", datosPromo.get(i).get("linkConocer"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R2BS6")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanHB6","");
    			}else {
    				request.setAttribute("idPlanHB6", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreHB6","");
    			}else {
    				request.setAttribute("nombreHB6", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkCotizar") == null){
    				request.setAttribute("linkCotizarHB6","");
    			}else {
    				request.setAttribute("linkCotizarHB6", datosPromo.get(i).get("linkCotizar"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerHB6","");
    			}else {
    				request.setAttribute("trackerHB6", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenHB6","");
    			}else {
    				request.setAttribute("imagenHB6", datosPromo.get(i).get("imagen"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkVerBasesHB6","");
    			}else {
    				request.setAttribute("linkVerBasesHB6", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("linkConocer") == null){
    				request.setAttribute("linkConocerHB6","");
    			}else {
    				request.setAttribute("linkConocerHB6", datosPromo.get(i).get("linkConocer"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R3BS1")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanVIB1","");
    			}else {
    				request.setAttribute("idPlanVIB1", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreVIB1","");
    			}else {
    				request.setAttribute("nombreVIB1", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkCotizar") == null){
    				request.setAttribute("linkCotizarVIB1","");
    			}else {
    				request.setAttribute("linkCotizarVIB1", datosPromo.get(i).get("linkCotizar"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerVIB1","");
    			}else {
    				request.setAttribute("trackerVIB1", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenVIB1","");
    			}else {
    				request.setAttribute("imagenVIB1", datosPromo.get(i).get("imagen"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkVerBasesVIB1","");
    			}else {
    				request.setAttribute("linkVerBasesVIB1", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("linkConocer") == null){
    				request.setAttribute("linkConocerVIB1","");
    			}else {
    				request.setAttribute("linkConocerVIB1", datosPromo.get(i).get("linkConocer"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R3BS2")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanVIB2","");
    			}else {
    				request.setAttribute("idPlanVIB2", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreVIB2","");
    			}else {
    				request.setAttribute("nombreVIB2", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkCotizar") == null){
    				request.setAttribute("linkCotizarVIB2","");
    			}else {
    				request.setAttribute("linkCotizarVIB2", datosPromo.get(i).get("linkCotizar"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerVIB2","");
    			}else {
    				request.setAttribute("trackerVIB2", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenVIB2","");
    			}else {
    				request.setAttribute("imagenVIB2", datosPromo.get(i).get("imagen"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkVerBasesVIB2","");
    			}else {
    				request.setAttribute("linkVerBasesVIB2", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("linkConocer") == null){
    				request.setAttribute("linkConocerVIB2","");
    			}else {
    				request.setAttribute("linkConocerVIB2", datosPromo.get(i).get("linkConocer"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R3BS3")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanVIB3","");
    			}else {
    				request.setAttribute("idPlanVIB3", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreVIB3","");
    			}else {
    				request.setAttribute("nombreVIB3", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkCotizar") == null){
    				request.setAttribute("linkCotizarVIB3","");
    			}else {
    				request.setAttribute("linkCotizarVIB3", datosPromo.get(i).get("linkCotizar"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerVIB3","");
    			}else {
    				request.setAttribute("trackerVIB3", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenVIB3","");
    			}else {
    				request.setAttribute("imagenVIB3", datosPromo.get(i).get("imagen"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkVerBasesVIB3","");
    			}else {
    				request.setAttribute("linkVerBasesVIB3", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("linkConocer") == null){
    				request.setAttribute("linkConocerVIB3","");
    			}else {
    				request.setAttribute("linkConocerVIB3", datosPromo.get(i).get("linkConocer"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R3BS4")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanVIB4","");
    			}else {
    				request.setAttribute("idPlanVIB4", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreVIB4","");
    			}else {
    				request.setAttribute("nombreVIB4", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkCotizar") == null){
    				request.setAttribute("linkCotizarVIB4","");
    			}else {
    				request.setAttribute("linkCotizarVIB4", datosPromo.get(i).get("linkCotizar"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerVIB4","");
    			}else {
    				request.setAttribute("trackerVIB4", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenVIB4","");
    			}else {
    				request.setAttribute("imagenVIB4", datosPromo.get(i).get("imagen"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkVerBasesVIB4","");
    			}else {
    				request.setAttribute("linkVerBasesVIB4", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("linkConocer") == null){
    				request.setAttribute("linkConocerVIB4","");
    			}else {
    				request.setAttribute("linkConocerVIB4", datosPromo.get(i).get("linkConocer"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R3BS5")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanVIB5","");
    			}else {
    				request.setAttribute("idPlanVIB5", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreVIB5","");
    			}else {
    				request.setAttribute("nombreVIB5", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkCotizar") == null){
    				request.setAttribute("linkCotizarVIB5","");
    			}else {
    				request.setAttribute("linkCotizarVIB5", datosPromo.get(i).get("linkCotizar"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerVIB5","");
    			}else {
    				request.setAttribute("trackerVIB5", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenVIB5","");
    			}else {
    				request.setAttribute("imagenVIB5", datosPromo.get(i).get("imagen"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkVerBasesVIB5","");
    			}else {
    				request.setAttribute("linkVerBasesVIB5", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("linkConocer") == null){
    				request.setAttribute("linkConocerVIB5","");
    			}else {
    				request.setAttribute("linkConocerVIB5", datosPromo.get(i).get("linkConocer"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R3BS6")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanVIB6","");
    			}else {
    				request.setAttribute("idPlanVIB6", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreVIB6","");
    			}else {
    				request.setAttribute("nombreVIB6", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkCotizar") == null){
    				request.setAttribute("linkCotizarVIB6","");
    			}else {
    				request.setAttribute("linkCotizarVIB6", datosPromo.get(i).get("linkCotizar"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerVIB6","");
    			}else {
    				request.setAttribute("trackerVIB6", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenVIB6","");
    			}else {
    				request.setAttribute("imagenVIB6", datosPromo.get(i).get("imagen"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkVerBasesVIB6","");
    			}else {
    				request.setAttribute("linkVerBasesVIB6", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("linkConocer") == null){
    				request.setAttribute("linkConocerVIB6","");
    			}else {
    				request.setAttribute("linkConocerVIB6", datosPromo.get(i).get("linkConocer"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R4BS1")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanSB1","");
    			}else {
    				request.setAttribute("idPlanSB1", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreSB1","");
    			}else {
    				request.setAttribute("nombreSB1", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkCotizar") == null){
    				request.setAttribute("linkCotizarSB1","");
    			}else {
    				request.setAttribute("linkCotizarSB1", datosPromo.get(i).get("linkCotizar"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerSB1","");
    			}else {
    				request.setAttribute("trackerSB1", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenSB1","");
    			}else {
    				request.setAttribute("imagenSB1", datosPromo.get(i).get("imagen"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkVerBasesSB1","");
    			}else {
    				request.setAttribute("linkVerBasesSB1", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("linkConocer") == null){
    				request.setAttribute("linkConocerSB1","");
    			}else {
    				request.setAttribute("linkConocerSB1", datosPromo.get(i).get("linkConocer"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R4BS2")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanSB2","");
    			}else {
    				request.setAttribute("idPlanSB2", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreSB2","");
    			}else {
    				request.setAttribute("nombreSB2", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkCotizar") == null){
    				request.setAttribute("linkCotizarSB2","");
    			}else {
    				request.setAttribute("linkCotizarSB2", datosPromo.get(i).get("linkCotizar"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerSB2","");
    			}else {
    				request.setAttribute("trackerSB2", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenSB2","");
    			}else {
    				request.setAttribute("imagenSB2", datosPromo.get(i).get("imagen"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkVerBasesSB2","");
    			}else {
    				request.setAttribute("linkVerBasesSB2", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("linkConocer") == null){
    				request.setAttribute("linkConocerSB2","");
    			}else {
    				request.setAttribute("linkConocerSB2", datosPromo.get(i).get("linkConocer"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R4BS3")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanSB3","");
    			}else {
    				request.setAttribute("idPlanSB3", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreSB3","");
    			}else {
    				request.setAttribute("nombreSB3", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkCotizar") == null){
    				request.setAttribute("linkCotizarSB3","");
    			}else {
    				request.setAttribute("linkCotizarSB3", datosPromo.get(i).get("linkCotizar"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerSB3","");
    			}else {
    				request.setAttribute("trackerSB3", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenSB3","");
    			}else {
    				request.setAttribute("imagenSB3", datosPromo.get(i).get("imagen"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkVerBasesSB3","");
    			}else {
    				request.setAttribute("linkVerBasesSB3", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("linkConocer") == null){
    				request.setAttribute("linkConocerSB3","");
    			}else {
    				request.setAttribute("linkConocerSB3", datosPromo.get(i).get("linkConocer"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R4BS4")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanSB4","");
    			}else {
    				request.setAttribute("idPlanSB4", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreSB4","");
    			}else {
    				request.setAttribute("nombreSB4", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkCotizar") == null){
    				request.setAttribute("linkCotizarSB4","");
    			}else {
    				request.setAttribute("linkCotizarSB4", datosPromo.get(i).get("linkCotizar"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerSB4","");
    			}else {
    				request.setAttribute("trackerSB4", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenSB4","");
    			}else {
    				request.setAttribute("imagenSB4", datosPromo.get(i).get("imagen"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkVerBasesSB4","");
    			}else {
    				request.setAttribute("linkVerBasesSB4", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("linkConocer") == null){
    				request.setAttribute("linkConocerSB4","");
    			}else {
    				request.setAttribute("linkConocerSB4", datosPromo.get(i).get("linkConocer"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R4BS5")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanSB5","");
    			}else {
    				request.setAttribute("idPlanSB5", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreSB5","");
    			}else {
    				request.setAttribute("nombreSB5", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkCotizar") == null){
    				request.setAttribute("linkCotizarSB5","");
    			}else {
    				request.setAttribute("linkCotizarSB5", datosPromo.get(i).get("linkCotizar"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerSB5","");
    			}else {
    				request.setAttribute("trackerSB5", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenSB5","");
    			}else {
    				request.setAttribute("imagenSB5", datosPromo.get(i).get("imagen"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkVerBasesSB5","");
    			}else {
    				request.setAttribute("linkVerBasesSB5", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("linkConocer") == null){
    				request.setAttribute("linkConocerSB5","");
    			}else {
    				request.setAttribute("linkConocerSB5", datosPromo.get(i).get("linkConocer"));	
    			};
    		} else if (datosPromo.get(i).get("id").equals("R4BS6")){
    			if (datosPromo.get(i).get("idPlan") == null){
    				request.setAttribute("idPlanSB6","");
    			}else {
    				request.setAttribute("idPlanSB6", datosPromo.get(i).get("idPlan"));	
    			};
    			if (datosPromo.get(i).get("nombre") == null){
    				request.setAttribute("nombreSB6","");
    			}else {
    				request.setAttribute("nombreSB6", datosPromo.get(i).get("nombre"));	
    			};
    			if (datosPromo.get(i).get("linkCotizar") == null){
    				request.setAttribute("linkCotizarSB6","");
    			}else {
    				request.setAttribute("linkCotizarSB6", datosPromo.get(i).get("linkCotizar"));	
    			};
    			if (datosPromo.get(i).get("tracker") == null){
    				request.setAttribute("trackerSB6","");
    			}else {
    				request.setAttribute("trackerSB6", datosPromo.get(i).get("tracker"));	
    			};
    			if (datosPromo.get(i).get("imagen") == null){
    				request.setAttribute("imagenSB6","");
    			}else {
    				request.setAttribute("imagenSB6", datosPromo.get(i).get("imagen"));	
    			};
    			if (datosPromo.get(i).get("linkVer") == null){
    				request.setAttribute("linkVerBasesSB6","");
    			}else {
    				request.setAttribute("linkVerBasesSB6", datosPromo.get(i).get("linkVer"));	
    			};
    			if (datosPromo.get(i).get("linkConocer") == null){
    				request.setAttribute("linkConocerSB6","");
    			}else {
    				request.setAttribute("linkConocerSB6", datosPromo.get(i).get("linkConocer"));	
    			};
    		}    		}
    	
    	}
    }
    response.setContentType("text/html");
    if (oForm.getDatos().get("hiddSubmit") == null){
    	return mapping.findForward("continue");
    } else {
    	return mapping.findForward("inicio-promo");
    }
	
}
}