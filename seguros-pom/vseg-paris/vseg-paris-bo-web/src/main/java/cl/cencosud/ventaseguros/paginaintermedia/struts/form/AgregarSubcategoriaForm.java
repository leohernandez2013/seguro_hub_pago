package cl.cencosud.ventaseguros.paginaintermedia.struts.form;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.codehaus.jackson.map.ObjectMapper;

import com.tinet.exceptions.system.SystemException;

import cl.cencosud.ventaseguros.common.TipoRama;
import cl.cencosud.ventaseguros.delegate.PaginaIntermediaDelegate;
import cl.tinet.common.seguridad.model.Usuario;
import cl.tinet.common.seguridad.util.SeguridadUtil;
import cl.tinet.common.struts.form.BuilderActionFormBaseVSP;

public class AgregarSubcategoriaForm extends BuilderActionFormBaseVSP {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = -7574513142485271071L;
    
    private int rama;
    private int tipo;
    private String subcategoria;

    /**
     * Obtiene archivo de validación.
     * @param request HttpServletRequest
     * @return InputStream
     */
    @Override
    public InputStream getValidationRules(HttpServletRequest request) {
        return AdministrarPaginaIntermediaForm.class
            .getResourceAsStream("resource/validation-nueva-subcategoria.xml");
    }

    /**
     * @return retorna el valor del atributo rama
     */
    public int getRama() {
        return rama;
    }

    /**
     * @param rama a establecer en el atributo rama.
     */
    public void setRama(int rama) {
        this.rama = rama;
    }

    /**
     * @return retorna el valor del atributo tipo
     */
    public int getTipo() {
        return tipo;
    }

    /**
     * @param tipo a establecer en el atributo tipo.
     */
    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    /**
     * @return retorna el valor del atributo titulo
     */
    public String getSubcategoria() {
        return subcategoria;
    }

    /**
     * @param rama a establecer en el atributo rama.
     */
    public void setSubcategoria(String subcategoria) {
        this.subcategoria = subcategoria;
    }
    
    @Override
    public ActionErrors validate(ActionMapping mapping,
        HttpServletRequest request) {
        
        PaginaIntermediaDelegate delegate = new PaginaIntermediaDelegate();
        ActionErrors errors = new ActionErrors();
        
        if(this.getDatos().get("rama") != ""
            && this.getDatos().get("rama") != null){
            
            List < TipoRama > tiposRama = new ArrayList < TipoRama >();
            int idRama = Integer.parseInt((String) this.getDatos().get("rama"));
            tiposRama = delegate.obtenerTiposRama(idRama);
            if(!tiposRama.isEmpty() && ( this.getDatos().get("tipo") == null || this.getDatos().get("tipo").toString().equals("") || this.getDatos().get("tipo").toString().equals("0"))){
                errors.add("datos.tipo", new ActionMessage("errors.required"));
            }
        }
        else{
            errors.add("datos.rama", new ActionMessage("errors.required"));
        }
        if(this.getDatos().get("subcategoria") == null || this.getDatos().get("subcategoria").toString().equals("")){
            errors.add("datos.subcategoria", new ActionMessage("errors.required"));
        }
        return errors;
    }
}
