package cl.cencosud.ventaseguros.planes.struts.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import cl.cencosud.ventaseguros.asesorvirtual.model.RestriccionVida;
import cl.cencosud.ventaseguros.common.PlanLeg;
import cl.cencosud.ventaseguros.common.exception.FichaSubcategoriaException;
import cl.cencosud.ventaseguros.delegate.FichaSubcategoriaDelegate;
import cl.cencosud.ventaseguros.delegate.MantenedorClientesDelegate;

import com.tinet.exceptions.system.SystemException;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class AdministrarPlanVerAction extends Action {

    /**
     * Grupo Webpay dentro de base de datos parametros.
     */
    private final String GRUPO_WEBPAY = "WEBPAY";

    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        Map < String, Object > result = new HashMap < String, Object >();

        FichaSubcategoriaDelegate fichadelegate =
            new FichaSubcategoriaDelegate();

        if (!"".equals(request.getParameter("id_plan"))) {
            try {

                //Buscar datos del plan.
                long id_plan = Long.parseLong(request.getParameter("id_plan"));

                PlanLeg plan = fichadelegate.obtenerDatosPlan(id_plan);

                if (plan == null) {
                    FichaSubcategoriaException ex =
                        new FichaSubcategoriaException(
                            FichaSubcategoriaException.PLAN_NO_ENCONTRADO,
                            new Object[] { id_plan });
                    result.put("error", ex.getMessage());
                } else {

                    result.put("plan", BeanUtils.describe(plan));

                    //forma de pago.
                    //Obtener todas las formas de pago.
                    MantenedorClientesDelegate mantenedorDelegate =
                        new MantenedorClientesDelegate();
                    List < Map < String, ? > > lFormaPago =
                        mantenedorDelegate.obtenerParametro(GRUPO_WEBPAY);
                    String sFormaPago = "";
                    if (plan != null) {
                        sFormaPago =
                            (plan.getFormaPago() != null ? plan.getFormaPago()
                                : "");
                    }

                    StringBuffer forma_pago = new StringBuffer(sFormaPago);
                    Map < String, String > hFormaPago =
                        new HashMap < String, String >();
                    for (int i = 0; i < lFormaPago.size(); i++) {
                        Map < String, ? > forma = lFormaPago.get(i);
                        String estado = "0";
                        try {
                            if (forma_pago.charAt(i) == '1') {
                                estado = "1";
                            }
                        } catch (IndexOutOfBoundsException e) {
                            //
                        }
                        hFormaPago.put((String) forma.get("valor"), estado);
                    }

                    result.put("hFormaPago", hFormaPago);

                    //Tipo de pago
                    boolean primaUnica =
                        fichadelegate.existePrimaUnica(id_plan);
                    String tipo_pago = "1";
                    if (primaUnica) {
                        tipo_pago = "2";
                    }
                    result.put("tipo_pago", tipo_pago);

                    //fecha vigencia    (plan)

                    //asegurados adicionales.
                    //beneficiarios.
                    RestriccionVida restricciones =
                        fichadelegate.obtenerRestriccionVida(plan
                            .getId_plan_vseg());
                    result.put("restricciones", BeanUtils
                        .describe(restricciones));

                    //Inspeccion        (plan)
                    //auto_nuevo        (plan)
                    //factura           (plan)
                }

                PrintWriter pwriter = response.getWriter();
                ObjectMapper mapper = new ObjectMapper();
                StringWriter json = new StringWriter();
                mapper.writeValue(json, result);
                pwriter.write(json.toString());

            } catch (IOException e) {
                throw new SystemException(e);
            } catch (IllegalAccessException e) {
                throw new SystemException(e);
            } catch (InvocationTargetException e) {
                throw new SystemException(e);
            } catch (NoSuchMethodException e) {
                throw new SystemException(e);
            }
        }

        return null;
    }
}
