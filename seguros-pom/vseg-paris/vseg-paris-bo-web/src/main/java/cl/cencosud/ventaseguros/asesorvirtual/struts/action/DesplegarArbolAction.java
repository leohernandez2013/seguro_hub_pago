package cl.cencosud.ventaseguros.asesorvirtual.struts.action;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import cl.cencosud.ventaseguros.asesorvirtual.model.Alternativa;
import cl.cencosud.ventaseguros.asesorvirtual.model.Pregunta;
import cl.cencosud.ventaseguros.asesorvirtual.model.Preguntas;
import cl.cencosud.ventaseguros.asesorvirtual.model.Respuestas;
import cl.cencosud.ventaseguros.asesorvirtual.model.Resultado;
import cl.cencosud.ventaseguros.asesorvirtual.model.Seccion;
import cl.cencosud.ventaseguros.asesorvirtual.model.Sitio;
import cl.cencosud.ventaseguros.asesorvirtual.util.ResultadoConverter;
import cl.cencosud.ventaseguros.asesorvirtual.util.ResultadoXppDriver;
import cl.cencosud.ventaseguros.delegate.AsesorVirtualDelegate;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.tinet.exceptions.system.SystemException;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class DesplegarArbolAction extends Action {

    private static final String XML =
        "cl/cencosud/ventaseguros/asesorvirtual/struts/action/sitio.xml";

    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        Sitio sitio = null;

        try {
            XStream xstream = new XStream(new ResultadoXppDriver());

            xstream.alias("sitio", Sitio.class);

            xstream.alias("seccion", Seccion.class);
            xstream.aliasAttribute(Seccion.class, "id", "id");
            xstream.aliasAttribute(Seccion.class, "titulo", "titulo");
            xstream.addImplicitCollection(Sitio.class, "secciones",
                Seccion.class);

            xstream.alias("preguntas", Preguntas.class);
            xstream.aliasAttribute(Preguntas.class, "idPrimeraPregunta",
                "idPrimeraPregunta");
            xstream.aliasAttribute(Preguntas.class, "cantidadpreguntas",
                "cantidadpreguntas");

            xstream.alias("pregunta", Pregunta.class);
            xstream.aliasAttribute(Pregunta.class, "id", "id");
            xstream.aliasAttribute(Pregunta.class, "titulo", "titulo");
            xstream.aliasAttribute(Pregunta.class, "tipoPregunta",
                "tipoPregunta");
            xstream.addImplicitCollection(Preguntas.class, "preguntas",
                Pregunta.class);

            xstream.alias("alternativa", Alternativa.class);
            xstream.aliasAttribute(Alternativa.class, "id", "id");
            xstream.aliasAttribute(Alternativa.class, "texto0", "texto0");
            xstream.aliasAttribute(Alternativa.class, "texto1", "texto1");
            xstream.aliasAttribute(Alternativa.class, "valor0", "valor0");
            xstream.aliasAttribute(Alternativa.class, "valor1", "valor1");
            xstream.aliasAttribute(Alternativa.class, "respuesta", "respuesta");
            xstream.aliasAttribute(Alternativa.class, "proximaPregunta",
                "proximaPregunta");
            xstream.addImplicitCollection(Pregunta.class, "alternativas",
                Alternativa.class);

            xstream.alias("respuestas", Respuestas.class);

            xstream.alias("resultado", Resultado.class);
            xstream.addImplicitCollection(Respuestas.class, "resultados",
                Resultado.class);

            Converter converter = new ResultadoConverter();
            xstream.registerConverter(converter);

            if (request.getSession().getAttribute("sitio") == null) {

                //Grabar.
                AsesorVirtualDelegate asesorVirtualDelegate =
                    new AsesorVirtualDelegate();
                sitio = asesorVirtualDelegate.cargarArbol(1);

                if (sitio == null || sitio.getSecciones() == null) {
                    InputStream is =
                        this.getClass().getClassLoader().getResourceAsStream(
                            XML);

                    sitio = (Sitio) xstream.fromXML(is);
                }
                request.getSession().setAttribute("sitio", sitio);
            } else {
                sitio = (Sitio) request.getSession().getAttribute("sitio");
            }

            response.setHeader("pragma", "no-cache");
            response.setHeader("cache-control", "no-cache");
            response.setDateHeader("expires", -1);
            response.setContentType("text/html");

            PrintWriter pwriter = response.getWriter();
            ObjectMapper mapper = new ObjectMapper();
            StringWriter json = new StringWriter();
            mapper.writeValue(json, sitio);
            pwriter.write(json.toString());
        } catch (IOException e) {
            throw new SystemException(e);
        }

        return null;
    }
}
