package cl.cencosud.ventaseguros.comparador.struts.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.common.PlanLeg;
import cl.cencosud.ventaseguros.comparador.model.Caracteristica;
import cl.cencosud.ventaseguros.delegate.ComparadorDelegate;


public class ComparadorPrevisualizarAction extends Action {

	@SuppressWarnings("unchecked")
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		String oIdProducto = request.getParameter("idProducto");
		String oTxtSubcategoria = request.getParameter("txtSubcategoria");
		String oTxtProducto = request.getParameter("txtProducto");
		String oIdEstado = request.getParameter("idEstado");
		String oIdTipoCaracteristica = request.getParameter("idTipo");
		
		ComparadorDelegate oDelegate = new ComparadorDelegate();
		
		List<PlanLeg> listPlanes = oDelegate.obtenerPlanesPorProducto(Integer.valueOf(oIdEstado), Long.valueOf(oIdProducto));
		LinkedHashMap<String, List<?>> mapCarateristicas = new LinkedHashMap<String, List<?>>();
		List<PlanLeg> listPlanesTemp = new ArrayList<PlanLeg>();
		List<Caracteristica> listTempCaractComp = null;
		
		if (listPlanes != null && listPlanes.size() > 1) {
			int size = 0;
			if (listPlanes.size() > 1 && listPlanes.size() < 5) {
				size = listPlanes.size();
			} else {
				size = 4;
			}
			
			for (int i=0; i < size; i++) {
				PlanLeg plan = listPlanes.get(i);
				String idPlan = String.valueOf(plan.getId_plan());
				List<Caracteristica> list = oDelegate.getCaracteristicasPorPlan(Integer.valueOf(idPlan), Integer.valueOf(oIdEstado));				
				mapCarateristicas.put(idPlan, list);
				listPlanesTemp.add(plan);
			}
			
			listTempCaractComp = oDelegate.obtenerCaracteristicasCompartidas(Long.valueOf(oIdProducto), Integer.valueOf(oIdEstado));
		}
		
		LinkedHashMap mapResultComparador = new LinkedHashMap();
		
		mapResultComparador.put("listPlanes", listPlanesTemp);
		mapResultComparador.put("listCaracteristicas", this.getNormalizedRows(request, Integer.valueOf(oIdTipoCaracteristica), mapCarateristicas, listTempCaractComp));
		
		request.getSession().setAttribute("mapResultComparadorBack", mapResultComparador);	
		request.setAttribute("txtSubcategoria", oTxtSubcategoria);
		request.setAttribute("txtProducto", oTxtProducto);

		return mapping.findForward("continuar");
	}
	
	@SuppressWarnings("unchecked")
	private LinkedHashMap<Integer, LinkedHashMap<String, List<Caracteristica>>> getNormalizedRows(HttpServletRequest request, Integer idTipoCaracteristica, LinkedHashMap<String, List<?>> mapCarateristicas, List<Caracteristica> listCaractComp) {
		LinkedHashMap<Integer, LinkedHashMap<String, List<Caracteristica>>> mapResult = new LinkedHashMap<Integer, LinkedHashMap<String, List<Caracteristica>>>();
		List<Caracteristica> listCaract = new ArrayList<Caracteristica>();
		
		Collection c = mapCarateristicas.values();
		Iterator iter = c.iterator();
		
		while (iter.hasNext()) {
			List<Caracteristica> listTemp = (List<Caracteristica>)iter.next();
			
			if (listTemp != null && listTemp.size() > 0) {
				listCaract.addAll(listTemp);
			}
		}
		
		LinkedHashMap<Integer, List<Caracteristica>> mapPorTipo = new LinkedHashMap<Integer, List<Caracteristica>>();
		
		for (Caracteristica ca : listCaract) {
			Integer tipoCaract = ca.getTipo();
			if (mapPorTipo.containsKey(tipoCaract)) {
				List<Caracteristica> list = mapPorTipo.get(tipoCaract);
				list.add(ca);
			} else {
				List<Caracteristica> list = new ArrayList<Caracteristica>();
				list.add(ca);
				mapPorTipo.put(tipoCaract, list);
			}
		}
		
		Collection co = mapPorTipo.values();
		Iterator ite = co.iterator();
		iter = mapPorTipo.entrySet().iterator();
		
		while(ite.hasNext()) {
			//Map.Entry<Integer, List<Caracteristica>> e = (Map.Entry<Integer, List<Caracteristica>>) iter.next();
			
//			List<Caracteristica> listTemp = e.getValue();
//			Integer tipoCaract = e.getKey();
			
			List<Caracteristica> listTemp = (List<Caracteristica>)ite.next();
			Integer tipoCaract = listTemp.get(0).getTipo();
			
			LinkedHashMap<String, List<Caracteristica>> mapPorCaract = new LinkedHashMap<String, List<Caracteristica>>();
			
			if (listTemp != null && listTemp.size() > 0) {
				
				for (Caracteristica ca : listTemp) {
					String idCaract = String.valueOf(ca.getId_caracteristica());
					if (mapPorCaract.containsKey(idCaract)) {
						List<Caracteristica> list = mapPorCaract.get(idCaract);
						list.add(ca);
					} else {
						List<Caracteristica> list = new ArrayList<Caracteristica>();
						list.add(ca);
						mapPorCaract.put(idCaract, list);
					}
				}				
			}
			
			if (mapPorCaract.size() > 0) {
				mapResult.put(tipoCaract, mapPorCaract);
			}			
		}
				
		if (listCaractComp != null && listCaractComp.size() > 0) {
			LinkedHashMap<String, List<Caracteristica>> mapCaractComp = new LinkedHashMap<String, List<Caracteristica>>();
			mapCaractComp.put("caractComp", listCaractComp);
			mapResult.put(3, mapCaractComp);
		}
		
		LinkedHashMap<Integer, List<Caracteristica>> mapPlanCaracteristica = (LinkedHashMap<Integer, List<Caracteristica>>) request
		.getSession().getAttribute("cl.cencosud.ventaseguros.comparador.mapPlanCaracteristica");
		LinkedHashMap<Integer, Caracteristica> mapCaractComp = (LinkedHashMap<Integer, Caracteristica>) 
		request.getSession().getAttribute("cl.cencosud.ventaseguros.comparador.mapCaractCompartidas");
		
		if (mapPlanCaracteristica != null && mapPlanCaracteristica.size() > 0) {
			if (mapResult.containsKey(idTipoCaracteristica)) {
				mapResult.remove(idTipoCaracteristica);
			}
			mapResult.put(idTipoCaracteristica, this.getMap(mapPlanCaracteristica));
		}
		
		if (mapCaractComp != null && mapCaractComp.size() > 0 && idTipoCaracteristica == 3) {
			if (mapResult.containsKey(idTipoCaracteristica)) {
				mapResult.remove(idTipoCaracteristica);
			}
			mapResult.put(idTipoCaracteristica, this.getMapCaractComp(mapCaractComp));
		}

		return mapResult;
	}
	
	private LinkedHashMap<String, List<Caracteristica>> getMapCaractComp(LinkedHashMap<Integer, Caracteristica> mapCaractComp) {
		LinkedHashMap<String, List<Caracteristica>> result = new LinkedHashMap<String, List<Caracteristica>>();
		List<Caracteristica> listTemp = new ArrayList<Caracteristica>();
		
		Collection c = mapCaractComp.values();
		Iterator iter = c.iterator();
		
		while(iter.hasNext()) {
			Caracteristica caractTemp = (Caracteristica)iter.next();
			listTemp.add(caractTemp);
		}
		
		result.put("caractComp", listTemp);
		return result;
	}

	private LinkedHashMap<String, List<Caracteristica>> getMap(LinkedHashMap<Integer, List<Caracteristica>> mapPlanCaracteristica) {
		LinkedHashMap<String, List<Caracteristica>> result = new LinkedHashMap<String, List<Caracteristica>>();
		Iterator iter = mapPlanCaracteristica.entrySet().iterator();
		
		while (iter.hasNext()) {
			Map.Entry<Integer, List<Caracteristica>> e = (Map.Entry<Integer, List<Caracteristica>>) iter.next();
			String idCaract = String.valueOf(e.getKey());
			List<Caracteristica> listTemp = e.getValue();
			result.put(idCaract, listTemp);			
		}

		return result;
	}
	
}
