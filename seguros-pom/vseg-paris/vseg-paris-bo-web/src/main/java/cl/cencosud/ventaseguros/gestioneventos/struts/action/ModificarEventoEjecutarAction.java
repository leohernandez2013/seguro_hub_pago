package cl.cencosud.ventaseguros.gestioneventos.struts.action;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.UrlValidator;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.common.exception.MantenedorEventosException;
import cl.cencosud.ventaseguros.delegate.MantenedorEventosDelegate;
import cl.cencosud.ventaseguros.gestioneventos.struts.form.NuevoeventoForm;
import cl.tinet.common.model.exception.BusinessException;

import com.tinet.comun.util.date.DateConstant;
import com.tinet.comun.util.date.DateUtil;

public class ModificarEventoEjecutarAction extends Action {

    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws BusinessException {
        NuevoeventoForm formulario = (NuevoeventoForm) form;

        Map datos = formulario.getDatos();
        MantenedorEventosDelegate delegate = new MantenedorEventosDelegate();

        Long idEvento =
            Long.parseLong(((String) datos.get("id_evento")).trim());

        String fecha_inicio = ((String) datos.get("fecha_inicio")).trim();
        String fecha_termino = ((String) datos.get("fecha_termino")).trim();
        String descripcion = ((String) datos.get("descripcion")).trim();
        String ocurrencia = ((String) datos.get("ocurrencia")).trim();
        Integer prioridad = Integer.valueOf((String) datos.get("prioridad"));
        String url = ((String) datos.get("url")).trim();
        Date fechaInicioDate = null;
        Date fechaTerminoDate = null;

        fechaInicioDate =
            DateUtil.getFecha(fecha_inicio, DateConstant.FORMAT_SIMPLE_DATE);
        fechaTerminoDate =
            DateUtil.getFecha(fecha_termino, DateConstant.FORMAT_SIMPLE_DATE);

        if (DateUtil.esMayorQue(fechaInicioDate, fechaTerminoDate)) {
            throw new MantenedorEventosException(
                MantenedorEventosException.RANGO_DE_FECHAS_NO_VALIDO,
                MantenedorEventosException.SIN_ARGUMENTOS);
        }

        UrlValidator valida = new UrlValidator();
        if (!valida.isValid(url) && !url.contains("/")) {
            throw new MantenedorEventosException(
                MantenedorEventosException.URL_NO_VALIDA, new Object[] { url });
        }

        datos.put("fecha_inicio", fechaInicioDate);
        datos.put("fecha_termino", fechaTerminoDate);
        datos.put("descripcion", descripcion);
        datos.put("ocurrencia", ocurrencia);
        datos.put("url", url);

        if (descripcion.length() < 255) {

            if (delegate.obtenerFechaInicio(idEvento).getTime() != fechaInicioDate
                .getTime()
                || delegate.obtenerFechaTermino(idEvento).getTime() != fechaTerminoDate
                    .getTime()
                || !delegate.obtenerPrioridadEvento(idEvento).equals(prioridad)) {
                if (delegate.conflictoPrioridad(prioridad, fechaInicioDate,
                    fechaTerminoDate)) {
                    Integer nuevaSugerencia =
                        delegate.obtenerSugerenciaPrioridad(prioridad,
                            fechaInicioDate, fechaTerminoDate);
                    if (nuevaSugerencia != 0) {
                        request.getSession().setAttribute(
                            "error_prioridad",
                            "Error, ya existe un evento con la misma "
                                + "prioridad, se sugiere la prioridad : "
                                + nuevaSugerencia);
                        request.getSession().setAttribute("sugerencia",
                            nuevaSugerencia);

                        request.getSession().setAttribute("id_evento", "");
                    }
                } else {

                    if (delegate.actualizarEvento(datos)) {
                        request.setAttribute("resultado", 1);
                        request.setAttribute("id_evento", (String) datos
                            .get("id_evento"));
                        request.getSession().setAttribute(
                            "id_evento",
                            Long.parseLong(((String) datos.get("id_evento"))
                                .trim()));
                    }
                }

            } else {
                if (delegate.actualizarEvento(datos)) {
                    request.setAttribute("resultado", 1);
                    request.setAttribute("id_evento", (String) datos
                        .get("id_evento"));
                    request.getSession()
                        .setAttribute(
                            "id_evento",
                            Long.parseLong(((String) datos.get("id_evento"))
                                .trim()));
                }
            }

        } else {
            request.setAttribute("error_evento",
                "Descripcion, maximo 255 caracteres");
        }

        return mapping.findForward("continuar");
    }

}
