/*
 * Generated by MyEclipse Struts
 * Template path: templates/java/JavaClass.vtl
 */
package cl.cencosud.ventaseguros.gestioneventos.struts.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.delegate.MantenedorEventosDelegate;

/**
 * MyEclipse Struts Creation date: 09-15-2010
 *
 * XDoclet definition:
 *
 * @struts.action validate="true"
 * @struts.action-forward name="continuar"
 *                        path="/secure/mantenedor-eventos/agregar-nuevo-evento.jsp"
 */
public class FiltrarEventoAction extends Action {

    public static final String EXITO = "continuar";
    public static final String ERROR = "error";

    /**
     *
     * Method execute
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        String flujo = EXITO;

        MantenedorEventosDelegate delegate = new MantenedorEventosDelegate();
        List < Map < String, ? > > posiciones = delegate.obtenerPosiciones();
        List < Map < String, ? > > tipoPagina = delegate.obtenerTipoPagina();
        List < Map < String, ? > > prioridad = delegate.obtenerPrioridad();
        List < Map < String, ? > > tipoEvento = delegate.obtenerTipoEvento();

        request.setAttribute("posiciones", posiciones);
        request.setAttribute("tipoPagina", tipoPagina);
        request.setAttribute("prioridad", prioridad);
        request.setAttribute("tipoEvento", tipoEvento);

        request.getSession().removeAttribute("id_evento");

        return mapping.findForward(flujo);
    }
}