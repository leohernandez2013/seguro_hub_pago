package cl.cencosud.ventaseguros.gestioneventos.struts.action;

import java.sql.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.delegate.MantenedorEventosDelegate;
import cl.cencosud.ventaseguros.gestioneventos.struts.form.NuevoeventoForm;

import com.tinet.comun.util.date.DateConstant;
import com.tinet.comun.util.date.DateUtil;

/**
 * Clase relacionada con la modificacio de un evento.
 * <br/>
 * @author Ricardo
 * @version 1.0
 * @created Oct 4, 2010
 */
public class ModificarEventoAction extends Action {

    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response) {

        NuevoeventoForm formulario = (NuevoeventoForm) form;

        String id_evento = request.getParameter("id_evento");
        Long idEvento = Long.parseLong(id_evento);
        Map < String, Object > mapEvento;

        MantenedorEventosDelegate delegate = new MantenedorEventosDelegate();
        mapEvento = delegate.obtenerEvento(idEvento);

        mapEvento.put("id_evento", idEvento);
        Integer prioridadId = ((Number) mapEvento.get("prioridad")).intValue();
        Integer posicionId = ((Number) mapEvento.get("posicion")).intValue();
        Integer tipoEventoId =
            ((Number) mapEvento.get("tipo_evento")).intValue();

        List < Map < String, ? > > posiciones = delegate.obtenerPosiciones();
        List < Map < String, ? > > tipoPagina = delegate.obtenerTipoPagina();
        List < Map < String, ? > > prioridad = delegate.obtenerPrioridad();
        List < Map < String, ? > > tipoEvento = delegate.obtenerTipoEvento();

        //subir map a formulario.
        formulario.setDatos(convert(mapEvento));

        formulario.getDatos().put("posicion", String.valueOf(posicionId));
        formulario.getDatos().put("prioridad", String.valueOf(prioridadId));
        formulario.getDatos().put("tipo_evento", String.valueOf(tipoEventoId));

        request.setAttribute("evento", mapEvento);
        request.setAttribute("posiciones", posiciones);
        request.setAttribute("tipoPagina", tipoPagina);
        request.setAttribute("prioridad", prioridad);
        request.setAttribute("tipoEvento", tipoEvento);

        return mapping.findForward("continuar");
    }

    private Map < String, String > convert(Map < String, Object > origen) {
        Map < String, String > res = new HashMap < String, String >();

        if (!origen.isEmpty()) {
            Iterator < String > it = origen.keySet().iterator();
            while (it.hasNext()) {
                String key = (String) it.next();
                Object oValue = origen.get(key);
                String value = "";
                if (oValue instanceof Number) {
                    value = ((Number) oValue).toString();
                } else if (oValue instanceof Date) {
                    value =
                        DateUtil.getFechaFormateada((java.util.Date) oValue,
                            DateConstant.FORMAT_SIMPLE_DATE);
                } else {
                    value = (String) origen.get(key);
                }
                res.put(key, value);
            }
        }
        return res;
    }
}
