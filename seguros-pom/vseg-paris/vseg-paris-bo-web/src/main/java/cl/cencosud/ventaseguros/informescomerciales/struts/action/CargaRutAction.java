package cl.cencosud.ventaseguros.informescomerciales.struts.action;

import java.io.BufferedReader;
//import java.io.File;
//import java.io.FileReader;
//import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
//import java.io.PrintWriter;
//import java.io.StringWriter;
import java.util.HashMap;
//import java.util.Iterator;
import java.util.Map;

//import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
//import org.codehaus.jackson.map.ObjectMapper;

import com.tinet.exceptions.system.SystemException;

//import cl.cencosud.ventaseguros.common.exception.SubirArchivosException;
import cl.cencosud.ventaseguros.delegate.CargaRutDelegate;
//import cl.cencosud.ventaseguros.delegate.MantenedorEventosDelegate;
//import cl.cencosud.ventaseguros.gestioneventos.struts.form.SubirArchivoForm;
import cl.cencosud.ventaseguros.informescomerciales.struts.form.CargaRutForm;
//import cl.tinet.common.model.exception.BusinessException;


//import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class CargaRutAction extends Action{

/**
* @param args
*/
public ActionForward execute(ActionMapping mapping, ActionForm form,
	        HttpServletRequest request, HttpServletResponse response)
	        throws Exception {

	CargaRutDelegate odelegate = new CargaRutDelegate();
    HashMap < String, String > rpta = new HashMap < String, String >();
    Map < String, Object > datos = new HashMap < String, Object >();
    datos.put("ID_evento", request.getSession().getAttribute("id_evento"));

    Long idEvento = (Long) request.getSession().getAttribute("id_evento");

    String action = (String) request.getParameter("action");

    try {
        try {

            //String tipo = "";
            FormFile cargaRut;
            StringBuilder sb;
            String[] rut;

           // tipo = request.getParameter("tipo");

            CargaRutForm formulario = (CargaRutForm) form;
            //archivo = formulario.getArchivo();
            cargaRut = formulario.getCargaRut();

            
            //ARCHIVO DE RUTS
            if (cargaRut != null) {
                if (cargaRut.getContentType().equals("text/plain")) {
                    //subir y leer archivo de texto
                    InputStream stream = cargaRut.getInputStream();

                    try {
                        String line;
                        sb = new StringBuilder();
                        BufferedReader reader =
                            new BufferedReader(new InputStreamReader(
                                stream, "UTF-8"));
                        while ((line = reader.readLine()) != null) {
                            sb.append(line);
                        }
                        rut = String.valueOf(sb).split(";");
                        odelegate.insertarRutArchivos(rut);

                    } finally {
                        stream.close();
                    }

                    if (rut[0].length() == 0) {
                        request.getSession().setAttribute("error",
                            "Archivo sin rut");
                    } 
                } else {
                    throw new Exception("Solo archivos de texto");
                }

            }
        } catch (Exception e) {
            
        }


    } catch (Exception e) {
        throw new SystemException(e);
    }

    return mapping.findForward("continue");
	
}
}