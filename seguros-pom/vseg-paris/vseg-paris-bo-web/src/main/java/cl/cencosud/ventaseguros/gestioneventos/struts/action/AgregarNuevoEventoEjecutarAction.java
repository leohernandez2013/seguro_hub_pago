/*
 * Generated by MyEclipse Struts
 * Template path: templates/java/JavaClass.vtl
 */
package cl.cencosud.ventaseguros.gestioneventos.struts.action;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.UrlValidator;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.common.exception.MantenedorEventosException;
import cl.cencosud.ventaseguros.delegate.MantenedorEventosDelegate;
import cl.cencosud.ventaseguros.gestioneventos.struts.form.NuevoeventoForm;
import cl.tinet.common.model.exception.BusinessException;

import com.tinet.comun.util.date.DateConstant;
import com.tinet.comun.util.date.DateUtil;

/**
 * MyEclipse Struts Creation date: 09-15-2010
 *
 * XDoclet definition:
 *
 * @struts.action path="/agregarNuevoEventoEjecutar" name="nuevoeventoForm"
 *                input="/secure/mantenedor-eventos/agregar-nuevo-evento.jsp"
 *                scope="request" validate="true"
 */
public class AgregarNuevoEventoEjecutarAction extends Action {
    /*
     * Generated Methods
     */

    /**
     * Method execute
     *
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return ActionForward
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws BusinessException {
        //        NuevoeventoForm nuevoeventoForm = (NuevoeventoForm) form;
        MantenedorEventosDelegate delegate = new MantenedorEventosDelegate();
        NuevoeventoForm formulario = (NuevoeventoForm) form;
        Map datos = formulario.getDatos();

        String fechaInicio = (String) datos.get("fecha_inicio");
        String fechaTermino = (String) datos.get("fecha_termino");
        //        String archivo = request.getParameter("archivo");
        //        String cargaRut = request.getParameter("cargaRut");
        String descripcion = (String) datos.get("descripcion");
        Integer prioridad = Integer.valueOf((String) datos.get("prioridad"));
        Date fechaInicioDate = null;
        Date fechaTerminoDate = null;
        String url = ((String) datos.get("url")).trim();
        String codigo = (String) datos.get("codigo_evento");

        if (fechaInicio != null && !"".equals(fechaInicio)) {
            fechaInicioDate =
                DateUtil.getFecha(fechaInicio, DateConstant.FORMAT_SIMPLE_DATE);
        }
        if (fechaTermino != null && !"".equals(fechaTermino)) {
            fechaTerminoDate =
                DateUtil
                    .getFecha(fechaTermino, DateConstant.FORMAT_SIMPLE_DATE);
        }

        if (DateUtil.esMayorQue(fechaInicioDate, fechaTerminoDate)) {
            throw new MantenedorEventosException(
                MantenedorEventosException.RANGO_DE_FECHAS_NO_VALIDO,
                MantenedorEventosException.SIN_ARGUMENTOS);
        }

        UrlValidator valida = new UrlValidator();
        if (!valida.isValid(url) && !url.contains("/")) {
            throw new MantenedorEventosException(
                MantenedorEventosException.URL_NO_VALIDA, new Object[] { url });
        }

        datos.put("fecha_inicio", fechaInicioDate);
        datos.put("fecha_termino", fechaTerminoDate);
        datos.put("url", url);

        request.getSession().removeAttribute("sugerencia");
        request.getSession().removeAttribute("error_evento");
        request.getSession().removeAttribute("error_prioridad");

        if (descripcion.length() < 255) {

            if (delegate.conflictoPrioridad(prioridad, fechaInicioDate,
                fechaTerminoDate)) {

                Integer nuevaSugerencia =
                    delegate.obtenerSugerenciaPrioridad(prioridad,
                        fechaInicioDate, fechaTerminoDate);
                if (nuevaSugerencia != 0) {

                    request.getSession().setAttribute(
                        "error_prioridad",
                        "Error, ya existe un evento con la misma "
                            + "prioridad, se sugiere la prioridad : "
                            + nuevaSugerencia);
                    request.getSession().setAttribute("sugerencia",
                        nuevaSugerencia);

                    request.getSession().removeAttribute("id_evento");
                }

            } else {
                Integer cant = delegate.obtenerCantidadEvento(codigo);
                if (cant == 0) {
                    Long idEventoGenerado = delegate.nuevoEvento(datos);
                    if (idEventoGenerado != null) {
                        request.getSession().setAttribute("id_evento",
                            idEventoGenerado);
                    }
                } else {
                    request.getSession().removeAttribute("id_evento");

                    request.getSession().setAttribute("error_evento",
                        "Error Ya existe un evento con un mismo Codigo");

                }
            }
        } else {
            request.getSession().setAttribute("error_evento",
                "Descripcion, maximo 255 caracteres ");
        }
        return mapping.findForward("continuar");
    }
}