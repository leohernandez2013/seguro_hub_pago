package cl.cencosud.ventaseguros.informescomerciales.struts.action;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import cl.cencosud.ventaseguros.common.Parametro;
import cl.cencosud.ventaseguros.common.Rama;
import cl.cencosud.ventaseguros.common.Subcategoria;
import cl.cencosud.ventaseguros.delegate.InformesComercialesDelegate;
import cl.cencosud.ventaseguros.delegate.MantenedorClientesDelegate;
import cl.cencosud.ventaseguros.delegate.PaginaIntermediaDelegate;
import cl.cencosud.ventaseguros.informescomerciales.struts.form.InformeComercialVehiculoForm;
import cl.tinet.common.seguridad.config.SeguridadConfig;
import cl.tinet.common.util.crypto.CryptoUtil;

public class DesplegarInformeVitrineoVehiculoAction extends Action {
	
	private static Log logger =
        LogFactory.getLog(DesplegarInformeVitrineoVehiculoAction.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        InformeComercialVehiculoForm oForm =
            (InformeComercialVehiculoForm) form;

        SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
        String fechaDesde =
            oForm.getDatos().get("fechaDesde") == null ? "" : oForm.getDatos()
                .get("fechaDesde");
        String fechaHasta =
            oForm.getDatos().get("fechaHasta") == null ? "" : oForm.getDatos()
                .get("fechaHasta");

        String numeroPagina =
            oForm.getDatos().get("numeroPagina") == null ? "1" : oForm
                .getDatos().get("numeroPagina");
        
        int numerosPagina = Integer.parseInt(numeroPagina);
        
        int rama =
        	oForm.getRama();
        
        int subcategoria =
        	oForm.getSubcategoria();
        

        InformesComercialesDelegate oDelegate =
            new InformesComercialesDelegate();
        
        boolean esPaginado = true;
        String forward = "continuar";
        if ("1".equals(oForm.getDatos().get("excel"))) {
            esPaginado = false;

            SimpleDateFormat formato = new SimpleDateFormat("yyyyMMdd-hhmmss");
            String fecha = formato.format(new Date());
            response.reset();
            response.setHeader("Pragma", "no-cache");
            response.setHeader("cache-control",
                "public");
            Date hoy = new Date();
            response.setDateHeader("expires", hoy.getTime() + 30);
            response.setHeader("Content-type", "application/vnd.ms-excel");
            response.setHeader("Content-Transfer-Encoding", "binary");
            response.setHeader("Content-disposition",
                " attachment; filename=informe-vitrineo-" + fecha + ".xls");
            forward = "continuar-excel";
        }
        
        PaginaIntermediaDelegate delegate = new PaginaIntermediaDelegate();
        List < Rama > ramas = delegate.obtenerRamas();
        
        MantenedorClientesDelegate mantenedorDelegate =
            new MantenedorClientesDelegate();
        List < Map < String, ? > > lCompannias =
            mantenedorDelegate.obtenerParametro("COMPANNIA");       
        
        if (!fechaDesde.equals("") && !fechaHasta.equals("")) {

            if (!formatoFecha.parse(fechaHasta).before(
                formatoFecha.parse(fechaDesde))) {
            	logger.info("fechaDesde: " + fechaDesde);
            	logger.info("fechaHasta: " + fechaHasta);
            	logger.info("esPaginado: " + esPaginado);
            	logger.info("numeroPagina: " + Integer.parseInt(numeroPagina));
            	logger.info("rama: " + rama);
            	logger.info("subcategoria: " + subcategoria);
            	List<HashMap<String, Object>> informe =
                    oDelegate.obtenerVitrineoGeneral(fechaDesde, fechaHasta,
                            esPaginado, numerosPagina, rama, subcategoria);

                if (esPaginado) {
                    int totalPaginas =
                    	Integer
                        .parseInt(informe.get(0).get("pages").toString());

                    Parametro[] paginas = new Parametro[totalPaginas];
                    for (int i = 0; i < paginas.length; i++) {
                        paginas[i] = new Parametro();
                        paginas[i].setId(String.valueOf(i + 1));
                        paginas[i].setDescripcion(String.valueOf(i + 1));
                    }

                    request.setAttribute("listadoPaginas", paginas);
                }

                

                request.setAttribute("informe", informe);
            }
        }

        request.setAttribute("compannias", lCompannias);
        request.setAttribute("ramas", ramas);
        return mapping.findForward(forward);
    }

}
