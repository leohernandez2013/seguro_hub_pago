package cl.cencosud.ventaseguros.asesorvirtual.struts.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.ventaseguros.asesorvirtual.model.Alternativa;
import cl.cencosud.ventaseguros.asesorvirtual.model.Pregunta;
import cl.cencosud.ventaseguros.asesorvirtual.model.Preguntas;
import cl.cencosud.ventaseguros.asesorvirtual.model.Seccion;
import cl.cencosud.ventaseguros.asesorvirtual.model.Sitio;
import cl.cencosud.ventaseguros.asesorvirtual.struts.form.ArbolNodoEliminarForm;
import cl.cencosud.ventaseguros.common.exception.NodoNoEliminableException;
import cl.tinet.common.model.exception.BusinessException;

/** 
 * MyEclipse Struts.
 * Creation date: 08-23-2010
 * 
 * XDoclet definition:
 */
public class ArbolNodoEliminarAction extends Action {
    /** 
     * Method execute.
     * @param mapping ActionMapping.
     * @param form ActionForm.
     * @param request HttpServletRequest.
     * @param response HttpServletResponse.
     * @return ActionForward forward.
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws BusinessException {

        ArbolNodoEliminarForm oForm = (ArbolNodoEliminarForm) form;

        //Obtener arbol de sesion.
        Sitio sitio = (Sitio) request.getSession().getAttribute("sitio");

        if (sitio != null) {
            String actual = oForm.getActual();

            if (actual != null) {
                String tipo = actual.substring(0, 1);
                actual = actual.substring(1, actual.length() - 1);
                String[] arrActual = actual.split("_");

                int seccionPos = Integer.valueOf(arrActual[0]);

                //Eliminar nodo del arbol.
                Seccion seccion = sitio.getSecciones().get(seccionPos);

                //eliminar respuesta.
                if ("S".equals(tipo)) {
                    throw new NodoNoEliminableException(
                        NodoNoEliminableException.SECCION_NO_ELIMINABLE,
                        NodoNoEliminableException.SIN_ARGUMENTOS);
                }
                if ("R".equals(tipo)) {
                    int resultadoPos = Integer.valueOf(arrActual[3]);
                    seccion.getRespuestas().getResultados()
                        .remove(resultadoPos);
                } else if ("A".equals(tipo)) {
                    int alternativaPos = Integer.valueOf(arrActual[3]);
                    int preguntasPos = Integer.valueOf(arrActual[1]);
                    seccion.getPreguntas().getPreguntas().get(preguntasPos)
                        .getAlternativas().remove(alternativaPos);
                } else if ("P".equals(tipo)) {
                    int preguntasPos = Integer.valueOf(arrActual[1]);

                    if (preguntaAnidada(seccion, preguntasPos)) {
                        throw new NodoNoEliminableException(
                            NodoNoEliminableException.PREGUNTA_NO_ELIMINABLE,
                            NodoNoEliminableException.SIN_ARGUMENTOS);
                    }

                    seccion.getPreguntas().getPreguntas().remove(preguntasPos);
                }
            }

            //Actualzar datos
            actualizarCantidadPreguntas(sitio);

            request.setAttribute("nodo", true);
        }

        return mapping.findForward("continue");
    }

    /**
     * TODO Describir m�todo borrarPreguntaAnidada.
     * @param seccion
     * @param preguntasPos
     */
    private boolean preguntaAnidada(Seccion seccion, int preguntasPos) {
        boolean res = false;
        //Validar preguntas anidadas.
        Pregunta pregunta =
            seccion.getPreguntas().getPreguntas().get(preguntasPos);
        List < Alternativa > lAlternativas = pregunta.getAlternativas();
        for (Alternativa alternativa : lAlternativas) {
            int idPregunta = alternativa.getProximaPregunta();

            if (idPregunta > 0) {
                res = true;
            }
        }
        return res;
    }

    /**
     * Recorre el objeto sitio para setear la cantidad correcta de preguntas.
     * @param sitio objeto a recorrer.
     */
    private void actualizarCantidadPreguntas(Sitio sitio) {
        for (int i = 0; i < sitio.getSecciones().size(); i++) {

            Preguntas preguntas = sitio.getSecciones().get(i).getPreguntas();

            preguntas.setCantidadpreguntas(preguntas.getPreguntas().size());
        }
    }
}
