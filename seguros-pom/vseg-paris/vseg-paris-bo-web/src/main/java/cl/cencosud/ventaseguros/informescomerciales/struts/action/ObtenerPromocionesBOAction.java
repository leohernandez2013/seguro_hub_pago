package cl.cencosud.ventaseguros.informescomerciales.struts.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import com.tinet.exceptions.system.SystemException;

import cl.cencosud.ventaseguros.delegate.InformesComercialesDelegate;
import cl.cencosud.ventaseguros.informescomerciales.struts.form.InformeComercialTarjetasForm;


/**
 * Action encargado de desplegar los Vehiculos que el cliente registra.
 * <br/>
 * @author fgaete
 * @version 1.0
 * @created 10/11/2011
 */
public class ObtenerPromocionesBOAction extends Action {

	private static final Log logger =
        LogFactory.getLog(ObtenerPromocionesBOAction.class);
	
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {
    	//CotizacionVehiculoForm CotVehForm = (CotizacionVehiculoForm) form;
    	//String forward="continue";
        InformesComercialesDelegate delegate = new InformesComercialesDelegate();
        //Map vehiculo = new HashMap <String,Object>();
        String rama = request.getParameter("rama");
        String subcategoria = request.getParameter("subcategoria");
        Map datosPromo = new HashMap <String,Object>();
			
			logger.info("valor promo:" + datosPromo);
			
			
			
			PrintWriter pwritter = response.getWriter();
	         ObjectMapper mapper = new ObjectMapper();
	         StringWriter json = new StringWriter();
	         mapper.writeValue(json, datosPromo);
	         pwritter.write(json.toString());
		
		response.setContentType("text/html");

        return null;
    }
}
