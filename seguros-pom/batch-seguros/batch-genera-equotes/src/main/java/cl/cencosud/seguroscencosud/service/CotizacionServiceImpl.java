package cl.cencosud.seguroscencosud.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import cl.cencosud.seguroscencosud.dao.CotizacionDAOImpl;
import cl.cencosud.seguroscencosud.dto.CotizacionDTO;
import cl.cencosud.seguroscencosud.utils.Filtro;

import com.csvreader.CsvWriter;

public class CotizacionServiceImpl implements CotizacionService{
	
		/**
     * Logger de la clase
     */
	protected final static Logger logger = Logger.getLogger(CotizacionServiceImpl.class);
	private static Properties properties;
	

	public boolean obtenerArchivo(Integer idSolicitud, String rut) {
		List<CotizacionDTO> cotizaciones = null;
		CotizacionDAOImpl cotizacionDAO = new CotizacionDAOImpl();
		
		boolean flag = false;
		File archivo_e_quotes = null;
		
		cotizaciones = cotizacionDAO.obtenerArchivo(idSolicitud, rut);
		
		if (cotizaciones != null && cotizaciones.size() > 0){
			archivo_e_quotes = this.generarArchvoCSV(cotizaciones);
			logger.info("Se genero archivo: " + archivo_e_quotes.getName());
		}else{
			archivo_e_quotes = this.generarArchvoCSV();
			logger.info("Se genero archivo vacio: " + archivo_e_quotes.getName());
		}
		
		if (archivo_e_quotes != null){
			this.CopiarArchivo(archivo_e_quotes);
			this.eliminarArchivosAntiguos(archivo_e_quotes);
			flag = true;
		}
		
		
		
		return flag;
	}
	
	private File generarArchvoCSV(List<CotizacionDTO> cotizaciones){
		
		CsvWriter writer = null; 
		String delimitador = ";";
		CotizacionDTO cotizacion = null;
		
		File archivo_e_quotes = null;
		
		Iterator iterador = cotizaciones.iterator();
		
		
		try {
			String patron = "ddMMyyyy_HHmmss";
		    SimpleDateFormat formato = new SimpleDateFormat(patron);
			archivo_e_quotes = new File("e_quotes"+ formato.format(new Date())+".csv");
	        FileWriter fwriter = new FileWriter(archivo_e_quotes);
			writer = new CsvWriter (fwriter,delimitador.charAt(0));
			
			writer.write("Item Ensurance Name");
			writer.write("Item Ensurance Company");
			writer.write("Item Ensurance Date");
			writer.write("Item Car Type");
			writer.write("Item Car Brand");
			writer.write("Item Car Model");
			writer.write("Item Car Year");
			writer.write("Item Car Id");
			writer.write("Identificador");
			writer.write("Nombres");
			writer.write("Apellidos");
			writer.write("Item Price");
			writer.write("Direcci�n");
			writer.write("Divisi�n Pol�tica");
			writer.write("Pais");
			writer.write("Item Client Birthday");
			writer.write("Item Client Age");
			writer.write("Item Client State");
			writer.write("Item Client Gender");
			writer.write("Celular");
			writer.write("Correo electr�nico");
			writer.write("Subcategoria");
			writer.write("Paso");
			writer.write("   ");
//			writer.endRecord() - Termina con la linea.
			writer.endRecord();
			
			while(iterador.hasNext()){
				cotizacion = new CotizacionDTO();
				cotizacion = (CotizacionDTO) iterador.next();
				writer.write (cotizacion.getNombre_plan());
				writer.write(cotizacion.getNombre_cia());
				writer.write(this.formatearFecha(cotizacion.getFecha_creacion()));
				writer.write(cotizacion.getDescripcion_tipo());
				writer.write(cotizacion.getDescripcion_marca());
				writer.write(cotizacion.getDescripcion_modelo());
				writer.write(cotizacion.getAnnio_modelo());
				writer.write(this.ponerNull(cotizacion.getPatente()));
				writer.write(cotizacion.getRut_contratante());
				writer.write(cotizacion.getNombre_contratante());
				writer.write(cotizacion.getApellido_contratante());
				writer.write(cotizacion.getPrima_mensual());
				writer.write(this.ponerNullaDireccion(cotizacion.getDireccion()));
				writer.write(cotizacion.getComuna());
				writer.write(cotizacion.getPais());
				writer.write(this.formatearFecha(cotizacion.getFecha_nacimiento()));
				writer.write(cotizacion.getEdad());
				writer.write(cotizacion.getEstado_civil());
				writer.write(cotizacion.getSexo());
				writer.write(cotizacion.getTelefono());
				writer.write(cotizacion.getEmail());
				writer.write(cotizacion.getSubcategoria());
				writer.write(cotizacion.getDescripcion());
				writer.write("");
				writer.endRecord();
				
				this.actualizarIdSolicitud(cotizacion.getIdSolicitud());
			}	
			
			
		} catch (IOException e) {
			logger.error("Error al generar el archivo", e);	
		}finally {

			writer.close();

		}
		
		return archivo_e_quotes;
		
	}
	
	private File generarArchvoCSV(){
		
		CsvWriter writer = null; 
		String delimitador = ";";
		
		File archivo_e_quotes = null;
		
		
		
		try {
			String patron = "ddMMyyyy_HHmmss";
		    SimpleDateFormat formato = new SimpleDateFormat(patron);
			archivo_e_quotes = new File("e_quotes"+ formato.format(new Date())+".csv");
	        FileWriter fwriter = new FileWriter(archivo_e_quotes);
			writer = new CsvWriter (fwriter,delimitador.charAt(0));
			
			writer.write("Item Ensurance Name");
			writer.write("Item Ensurance Company");
			writer.write("Item Ensurance Date");
			writer.write("Item Car Type");
			writer.write("Item Car Brand");
			writer.write("Item Car Model");
			writer.write("Item Car Year");
			writer.write("Item Car Id");
			writer.write("Identificador");
			writer.write("Nombres");
			writer.write("Apellidos");
			writer.write("Item Price");
			writer.write("Direcci�n");
			writer.write("Divisi�n Pol�tica");
			writer.write("Pais");
			writer.write("Item Client Birthday");
			writer.write("Item Client Age");
			writer.write("Item Client State");
			writer.write("Item Client Gender");
			writer.write("Celular");
			writer.write("Correo electr�nico");
			writer.write("Subcategoria");
			writer.write("Paso");
			writer.write("   ");
//			writer.endRecord() - Termina con la linea.
			writer.endRecord();			
			
		} catch (IOException e) {
			logger.error("Error al generar el archivo", e);	
		}finally {

			writer.close();

		}
		
		return archivo_e_quotes;
		
	}
	
	public void CopiarArchivo(File archivoInterno) {
		
		FileInputStream fis;
		

		try {
			fis = new FileInputStream("config/config-genera-equotes.properties");
			properties = new Properties();
			properties.load(fis);
			String ruta = properties.getProperty("ruta");
			
			File outFile = new File(ruta+"e_quotes.csv");
			
			if (outFile.exists()){
				outFile.delete();
			}

			FileInputStream in = new FileInputStream(archivoInterno);
			FileOutputStream out = new FileOutputStream(outFile);

			int c;
			while( (c = in.read() ) != -1)
				out.write(c);

			in.close();
			out.close();
		} catch(IOException e) {
			logger.error("Hubo un error de entrada/salida!!!", e);
		}
	}
	
	private String ponerNull(String valor){
		
		if (valor == null || (" ").equals(valor))
			return valor = "NULL";		
		
		return valor;
		
	}
	
	 public String ponerNullaDireccion(String cad){
		 for(int i =0; i<cad.length(); i++)
			 if(cad.charAt(i) == ' '){
			 	 cad = "NULL";
			 	 break;
			 }else
				 break;
			 return cad; 	 
	 }
	
	private String formatearFecha(String fecha){
		
		String fechaFormateada = null;
		
		fechaFormateada = fecha.substring(8, 10) + "-" + fecha.substring(5, 7) + "-" + fecha.substring(0, 4);
		
		return fechaFormateada;
		
	}
	
	private void actualizarIdSolicitud(String idSolicitud){
		FileInputStream fis;
		try {
			fis = new FileInputStream("config/config-genera-equotes.properties");
			properties = new Properties();
			properties.load(fis);
			properties.setProperty("idsolicitud", idSolicitud);
			properties.store( new FileOutputStream("config/config-genera-equotes.properties"), null);
		} catch (Exception e) {
			logger.error("Hubo un error de escritura al actualizar la solicitud!!!", e);
		}
	}
	
	private void eliminarArchivosAntiguos(File archivo){
		String[] listaArchivos; 
		Date fechaDesde = null;
		Date fechaArchivo = null;
		String patron = "dd/MM/yyyy";
		File archivoEliminar; 
	    SimpleDateFormat formato = new SimpleDateFormat(patron);
		try {
			archivo=new File(".");
			listaArchivos=archivo.list(new Filtro(".csv"));
			fechaDesde = this.restarDias(7);
	        for(int i=0; i<listaArchivos.length; i++){	 
	        	fechaArchivo = formato.parse(listaArchivos[i].substring(8, 10) + "/" + listaArchivos[i].substring(10, 12)+ "/" + listaArchivos[i].substring(12, 16));
            	if (fechaArchivo.before(fechaDesde)){
            		archivoEliminar = new File (listaArchivos[i]);
            		if (archivoEliminar.delete())
            			logger.info("El fichero " + listaArchivos[i] + " ha sido borrado satisfactoriamente");
            	}	            
	        }
		} catch (Exception e) {
			logger.error("Hubo un error de escritura al actualizar la solicitud!!!", e);
		}
	}
	
	private Date restarDias(int diferencia){
		Date fecha = null; 
		try {
			Date fechaActual = Calendar.getInstance().getTime();
			long tiempoActual = fechaActual.getTime();
			long unDia = diferencia * 24 * 60 * 60 * 1000;
			fecha = new Date(tiempoActual - unDia);
		} catch (Exception e) {
			logger.error("Hubo un error de escritura al actualizar la solicitud!!!", e);
		}
		return fecha;
	}

}
