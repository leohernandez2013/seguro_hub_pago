package cl.cencosud.seguroscencosud.service;

import java.util.List;

import cl.cencosud.seguroscencosud.dto.CotizacionDTO;

public interface CotizacionService {
	
	public boolean obtenerArchivo(Integer idSolicitud, String rut);

}
