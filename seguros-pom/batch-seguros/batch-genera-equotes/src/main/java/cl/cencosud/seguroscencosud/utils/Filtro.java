package cl.cencosud.seguroscencosud.utils;

import java.io.*;

public class Filtro implements FilenameFilter{
    String extension;
    public Filtro(String extension){
        this.extension=extension;
    }
    public boolean accept(File dir, String name){
        return name.endsWith(extension);
    }
}