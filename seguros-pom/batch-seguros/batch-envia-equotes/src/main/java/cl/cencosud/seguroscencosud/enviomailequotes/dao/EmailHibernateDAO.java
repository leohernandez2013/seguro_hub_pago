package cl.cencosud.seguroscencosud.enviomailequotes.dao;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import cl.cencosud.seguroscencosud.enviomailequotes.dao.EmailDAO;
import cl.cencosud.seguroscencosud.enviomailequotes.model.Email;

public class EmailHibernateDAO extends HibernateDaoSupport implements EmailDAO {
	
	public void add(Email email) {
		try {
			getHibernateTemplate().save(email);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List getList() {
		return getSession().createQuery("select e from Email e where emlsource = 'SOLICITUD_TARJETA'").list();
	}

	public void update(Email email) {
		getHibernateTemplate().update(email);
	}
}
