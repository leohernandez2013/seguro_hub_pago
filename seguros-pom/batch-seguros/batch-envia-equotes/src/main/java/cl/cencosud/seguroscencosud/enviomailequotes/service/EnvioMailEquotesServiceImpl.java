package cl.cencosud.seguroscencosud.enviomailequotes.service;

import java.io.File;
import java.io.FileInputStream;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;

import cl.cencosud.seguroscencosud.enviomailequotes.dao.EmailDAO;
import cl.cencosud.seguroscencosud.enviomailequotes.model.Email;



public class EnvioMailEquotesServiceImpl implements EnvioMailEquotesService{
	
	protected final static Logger logger = Logger.getLogger(EnvioMailEquotesServiceImpl.class);
	private static Properties properties;
	
	private EmailDAO emailDAO;
	
	public void setEmailDAO(EmailDAO emailDAO) {
		this.emailDAO = emailDAO;
	}
	
	
	public boolean enviarCorreo(File file) {		
		FileInputStream fis;
		try {
			fis = new FileInputStream("config/config-envia-equotes.properties");
			properties = new Properties();
			properties.load(fis);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			fis = new FileInputStream(file);
			byte[] attach = new byte[(int)file.length()];
			fis.read(attach);
			
			String[] mails = properties.getProperty("mailList").split(", ");
			for (String d : mails) {
				logger.info("Se genera correo electr�nico enviado a ["+d+"]");
				Email email = new Email();
				email.setEmlsource("Informe E-quotes");
				email.setEmlattach(attach);
				email.setEmlattachname("informaci�n e_quotes2.csv");
				email.setEmlfrom("equotes@seguroscencosud.cl");
				email.setEmlto(d);
				email.setEmlsubject("Equotes ");
				email.setEmlbody("Proceso de Env�o Informe Equotes por Email");
				email.setEmlcontenttype("text/html");
				email.setEmlcountry("Chile");
				email.setEmlstate(2);
				email.setEmlsndtimes(0);
				email.setEmldatecreate(new Date());
				email.setEmldatesend(new Date());
				email.setEmlchannel("INTERNETCL");
				emailDAO.add(email);
			}
			return true;		
		} catch (Exception e) {
			logger.info("Exception enviando correos ", e);
		}
		return false;
	}
}
