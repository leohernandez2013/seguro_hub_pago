package cl.cencosud.seguroscencosud.enviomailequotes.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "EMAILS")
public class Email implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 447094114138239284L;
	private int id;
	private String emlsource;
	private String emlfrom;
	private String emlto;
	private String emlsubject;
	private String emlbody;
	private String emlcontenttype;
	private byte[] emlattach = null;
	private String emlattachname;
	private int emlstate;
	private int emlsndtimes; 
	private Date emldatecreate;
	private Date emldatesend;
	private String emlcountry;
	private String emlchannel;
	
	
	@Lob
	public byte[] getEmlattach() {
		return emlattach;
	}
	public void setEmlattach(byte[] emlattach) {
		this.emlattach = emlattach;
	}
	public String getEmlattachname() {
		return emlattachname;
	}
	public void setEmlattachname(String emlattachname) {
		this.emlattachname = emlattachname;
	}
	public String getEmlbody() {
		return emlbody;
	}
	public void setEmlbody(String emlbody) {
		this.emlbody = emlbody;
	}
	public String getEmlchannel() {
		return emlchannel;
	}
	public void setEmlchannel(String emlchannel) {
		this.emlchannel = emlchannel;
	}
	public String getEmlcontenttype() {
		return emlcontenttype;
	}
	public void setEmlcontenttype(String emlcontenttype) {
		this.emlcontenttype = emlcontenttype;
	}
	public String getEmlcountry() {
		return emlcountry;
	}
	public void setEmlcountry(String emlcountry) {
		this.emlcountry = emlcountry;
	}
	public Date getEmldatecreate() {
		return emldatecreate;
	}
	public void setEmldatecreate(Date emldatecreate) {
		this.emldatecreate = emldatecreate;
	}
	public Date getEmldatesend() {
		return emldatesend;
	}
	public void setEmldatesend(Date emldatesend) {
		this.emldatesend = emldatesend;
	}
	public String getEmlfrom() {
		return emlfrom;
	}
	public void setEmlfrom(String emlfrom) {
		this.emlfrom = emlfrom;
	}
	public int getEmlsndtimes() {
		return emlsndtimes;
	}
	public void setEmlsndtimes(int emlsndtimes) {
		this.emlsndtimes = emlsndtimes;
	}
	public String getEmlsource() {
		return emlsource;
	}
	public void setEmlsource(String emlsource) {
		this.emlsource = emlsource;
	}
	public int getEmlstate() {
		return emlstate;
	}
	public void setEmlstate(int emlstate) {
		this.emlstate = emlstate;
	}
	public String getEmlsubject() {
		return emlsubject;
	}
	public void setEmlsubject(String emlsubject) {
		this.emlsubject = emlsubject;
	}
	public String getEmlto() {
		return emlto;
	}
	public void setEmlto(String emlto) {
		this.emlto = emlto;
	}
	
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
}

