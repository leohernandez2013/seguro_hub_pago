package cl.cencosud.seguroscencosud.enviomailequotes.service;

import java.io.File;

public interface EnvioMailEquotesService {

	public boolean enviarCorreo(File file);
}
