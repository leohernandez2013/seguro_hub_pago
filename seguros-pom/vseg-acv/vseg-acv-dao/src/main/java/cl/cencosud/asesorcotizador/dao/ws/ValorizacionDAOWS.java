package cl.cencosud.asesorcotizador.dao.ws;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.xml.rpc.Stub;
import javax.xml.rpc.soap.SOAPFaultException;
import javax.xml.soap.Detail;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import cl.cencosud.acv.common.Contratante;
import cl.cencosud.acv.common.config.ACVConfig;
import cl.cencosud.acv.common.exception.ValorizacionException;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida;
import cl.cencosud.acv.common.valorizacion.ValorizacionSeguro;
import cl.cencosud.asesorcotizador.dao.ACVConfigDAO;
import cl.cencosud.asesorcotizador.dao.ACVDAOFactory;
import cl.cencosud.asesorcotizador.dao.ValorizacionDAO;

/*import cl.cencosud.bigsa.cotizacion.grabar.webservice.client.was.ParentAseg;*/
import com.bigsa.ws.gestion.grabar.cotizacion.impl.ParentAseg;

import cl.cencosud.bigsa.cotizacion.hogar.webservice.client.PrimaHogarVO;
import cl.cencosud.bigsa.cotizacion.hogar.webservice.client.WsBigsaCalculoPrimaHogarImplDelegate;
import cl.cencosud.bigsa.cotizacion.hogar.webservice.client.WsBigsaCalculoPrimaHogar_Impl;
import cl.cencosud.bigsa.cotizacion.vehiculo.webservice.client.PrimaVehiculoVO;
import cl.cencosud.bigsa.cotizacion.vehiculo.webservice.client.RespuestaScoringVO;
import cl.cencosud.bigsa.cotizacion.vehiculo.webservice.client.WsBigsaCalculoPrimaVehiculoImplDelegate;
import cl.cencosud.bigsa.cotizacion.vehiculo.webservice.client.WsBigsaCalculoPrimaVehiculo_Impl;
import cl.cencosud.bigsa.cotizacion.vida.webservice.client.AseguradoVO;
import cl.cencosud.bigsa.cotizacion.vida.webservice.client.PrimaVidaVO;
import cl.cencosud.bigsa.cotizacion.vida.webservice.client.WsBigsaCalculoPrimaVidaDelegate;
import cl.cencosud.bigsa.cotizacion.vida.webservice.client.WsBigsaCalculoPrimaVida_Impl;
import cl.tinet.common.config.BaseConfigurable;
import cl.tinet.common.util.resource.ResourceLeakUtil;

import com.tinet.exceptions.system.SystemException;

public class ValorizacionDAOWS extends BaseConfigurable implements
    ValorizacionDAO {

    private static Log logger = LogFactory.getLog(ValorizacionDAOWS.class);

    private static final String SERVICE_ENDPOINT_KEY = "SERVICE_ENDPOINT_KEY";

    private static final String WSDL_VALORIZAR_PLAN_HOGAR =
        "cl.cencosud.asesorcotizador.dao.ValorizacionDAOWS.wsdl.valorizar.hogar";
    private static final String WSDL_VALORIZAR_PLAN_VEHICULO =
        "cl.cencosud.asesorcotizador.dao.ValorizacionDAOWS.wsdl.valorizar.vehiculo";
    private static final String WSDL_VALORIZAR_PLAN_VIDA =
        "cl.cencosud.asesorcotizador.dao.ValorizacionDAOWS.wsdl.valorizar.vida";

    private static final String SCORING_VEHICULO_ESTADO_CIVIL =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.estadoCivil";

    private static final String SCORING_VEHICULO_MEDIO_INFORMATIVO =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.medioInformativo";

    private static final String SCORING_VEHICULO_RESPUESTA_MEDIO_INFORMATIVO =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.respuesta.medioInformativo";

    private static final String SCORING_VEHICULO_NUMERO_PUERTAS =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.nroPuertas";

    private static final String SCORING_VEHICULO_RESPUESTA_NUMERO_PUERTAS_DE_2_A_3 =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.respuesta.nroPuertas.de2a3";

    private static final String SCORING_VEHICULO_EDAD_CONTRATANTE =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.edadContratante";

    private static final String SCORING_VEHICULO_SEXO_CONTRATANTE =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.sexoContratante";

    private static final String SCORING_VEHICULO_RESPUESTA_SEXO_MASCULINO =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.respuesta.sexoContratante.masculino";

    private static final String SCORING_VEHICULO_RESPUESTA_SEXO_FEMENINO =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.respuesta.sexoContratante.femenino";

    private static final String SCORING_VEHICULO_CODIGO_COMUNA =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.codigoComuna";

    private static final String SCORING_VEHICULO_EDAD_CONDUCTOR =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.edadConductor";

    private static final String CODIGO_SCORING_ANTIGUEDAD =
        "cl.cencosud.asesorcotizador.scoring.hogar.pregunta.antiguedad";

    private static final String CODIGO_SCORING_VERANEO =
        "cl.cencosud.asesorcotizador.scoring.hogar.pregunta.veraneo";

    private static final String VALOR_SCORING_VERANEO =
        "cl.cencosud.asesorcotizador.scoring.hogar.valor.veraneo";

    private static final String CODIGO_SCORING_HOGAR =
        "cl.cencosud.asesorcotizador.scoring.hogar.pregunta.comuna";

    /**
     * TODO Describir m�todo valorizarPlanHogar.
     * @param primaHogar
     * @return
     * @throws ValorizacionException
     */
    public ValorizacionSeguro valorizarPlanHogar(
        CotizacionSeguroHogar cotizacionHogar) throws ValorizacionException {

        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);

        Map < String, String > params = new HashMap < String, String >();
        params.put(SERVICE_ENDPOINT_KEY, dao
            .getString(WSDL_VALORIZAR_PLAN_HOGAR));

        logger.debug("WS: CalculoPrimaHogar");
        logger.debug("Antes de intanciar IMPL.");
        WsBigsaCalculoPrimaHogar_Impl svc = new WsBigsaCalculoPrimaHogar_Impl();

        // Inicializa el servicio (URL).
        logger.debug("Antes de Ininicalizar el servicio.");
        WsBigsaCalculoPrimaHogarImplDelegate ws =
            this.inicializarWS(svc.getWsBigsaCalculoPrimaHogarImplPort(),
                params);

        PrimaHogarVO result = null;
        ValorizacionSeguro valorizacion = null;

        try {

            int codigoPlan = (int) cotizacionHogar.getCodigoPlan();
            int rutCliente = (int) cotizacionHogar.getRutCliente();
            float montoAseguradoEdificio =
                cotizacionHogar.getMontoAseguradoEdificio();
            float montoAseguradoContenido =
                cotizacionHogar.getMontoAseguradoContenido();
            float montoAseguradoRobo = cotizacionHogar.getMontoAseguradoRobo();

            // Pregunta Scoring COMUNA
            cl.cencosud.bigsa.cotizacion.hogar.webservice.client.RespuestaScoringVO rptaComuna =
                new cl.cencosud.bigsa.cotizacion.hogar.webservice.client.RespuestaScoringVO();
            int codigoPregunta =
                Integer.valueOf(ACVConfig.getInstance().getString(
                    CODIGO_SCORING_HOGAR));
            rptaComuna.setCodigoPregunta(codigoPregunta);
            rptaComuna.setValorNumeral(Float.valueOf(cotizacionHogar
                .getComuna()));
            //Para evitar null pointer.
            rptaComuna.setValorSN("S");

            //Pregunta scoring ANTIGUEDAD
            cl.cencosud.bigsa.cotizacion.hogar.webservice.client.RespuestaScoringVO rptaAntiguedad =
                new cl.cencosud.bigsa.cotizacion.hogar.webservice.client.RespuestaScoringVO();
            int codigoAntiguedad =
                Integer.valueOf(ACVConfig.getInstance().getString(
                    CODIGO_SCORING_ANTIGUEDAD));
            rptaAntiguedad.setCodigoPregunta(codigoAntiguedad);
            rptaAntiguedad.setValorNumeral(Float.valueOf(cotizacionHogar
                .getAntiguedadVivienda()));
            //Para evitar null pointer.
            rptaAntiguedad.setValorSN("S");

            //Pregunta scoring VERANEO
            cl.cencosud.bigsa.cotizacion.hogar.webservice.client.RespuestaScoringVO rptaVeraneo =
                new cl.cencosud.bigsa.cotizacion.hogar.webservice.client.RespuestaScoringVO();
            int codigoVeraneo =
                Integer.valueOf(ACVConfig.getInstance().getString(
                    CODIGO_SCORING_VERANEO));
            rptaVeraneo.setCodigoPregunta(codigoVeraneo);
            rptaVeraneo.setValorSN(ACVConfig.getInstance().getString(
                VALOR_SCORING_VERANEO));
            //Para evitar null pointer.
            rptaVeraneo.setValorSN("S");

            cl.cencosud.bigsa.cotizacion.hogar.webservice.client.RespuestaScoringVO[] oRespuestaScoring =
                new cl.cencosud.bigsa.cotizacion.hogar.webservice.client.RespuestaScoringVO[] {
                    rptaComuna, rptaAntiguedad, rptaVeraneo };

            logger.debug("Antes de consultar WS Prima Hogar...");
            logger.debug("Parametros Enviados: " + cotizacionHogar.toString());

            // Log Datos de Entrada.
            logger.info("VALORIZACION HOGAR JBOSS");
            logger.info("Datos de Entrada");
            logger.info("----------------");
            logger.info("Codigo Plan: " + codigoPlan);
            logger.info("Rut Cliente: " + rutCliente);
            logger.info("Monto Asegurado Edificio: " + montoAseguradoEdificio);
            logger
                .info("Monto Asegurado Contenido: " + montoAseguradoContenido);
            logger.info("Monto Asegurado Robo: " + montoAseguradoRobo);
            logger.info("Codigo Preg. comuna: " + codigoPregunta);
            logger.info("Val. Numeral Scoring: " + cotizacionHogar.getComuna());

            logger.info("Codigo Preg. antiguedad: "
                + rptaAntiguedad.getCodigoPregunta());
            logger.info("Val. Numeral antiguedad: "
                + rptaAntiguedad.getValorNumeral());

            logger.info("Codigo Preg. veraneo: "
                + rptaVeraneo.getCodigoPregunta());
            logger.info("Val. S/N veraneo: " + rptaVeraneo.getValorSN());

            logger.info("----------------");

            result =
                ws.calcularPrimaHogar(codigoPlan, rutCliente,
                    montoAseguradoEdificio, montoAseguradoContenido,
                    montoAseguradoRobo, oRespuestaScoring);

            if (result != null) {

                // Log Datos de Salida.
                logger.info("Datos de Salida");
                logger.info("----------------");
                logger.info("Prima Anual: " + result.getPrimaAnual());
                logger
                    .info("Prima Anual Pesos: " + result.getPrimaAnualPesos());
                logger.info("Prima Mensual: " + result.getPrimaMensual());
                logger.info("Prima Mensual Pesos: "
                    + result.getPrimaMensualPesos());
                logger.info("----------------");

                logger.debug("Valorizacion Obtenida.");
                valorizacion = new ValorizacionSeguro();
                valorizacion.setPrimaAnual(result.getPrimaAnual());
                valorizacion.setPrimaAnualPesos(Math.round(result
                    .getPrimaAnualPesos()));
                valorizacion.setPrimaMensual(result.getPrimaMensual());
                valorizacion.setPrimaMensualPesos(Math.round(result
                    .getPrimaMensualPesos()));
            }
        } catch (RemoteException e) {
            logger.debug("Error durante invocando WS de valorizacion.", e);
            logger.debug("Detalle: " + e.detail);
            obtenerExcepcionWS(e);
            logger.error("Excepcion remota durante invocacion a WS.", e.detail);
            throw new ValorizacionException(
                ValorizacionException.ERROR_VALORIZACION_KEY, 0);
        } catch (cl.cencosud.bigsa.cotizacion.hogar.webservice.client.ErrorInternoException e) {
            logger.error("Error durante invocando WS de valorizacion.", e);
            throw new ValorizacionException(
                ValorizacionException.ERROR_VALORIZACION_KEY, 0);
        } finally {
            ResourceLeakUtil.close(dao);
        }

        return valorizacion;
    }

    /**
     * TODO Describir m�todo valorizarPlanVehiculo.
     * @param cotizacionVehiculo
     * @return
     * @throws ValorizacionException
     */
    public ValorizacionSeguro valorizarPlanVehiculo(
        CotizacionSeguroVehiculo cotizacionVehiculo)
        throws ValorizacionException {

        Map < String, String > params = new HashMap < String, String >();

        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);

        String url =  dao.getString(WSDL_VALORIZAR_PLAN_VEHICULO);
        
        params.put(SERVICE_ENDPOINT_KEY, url);

        logger.debug("WS: CalculoPrima Vehiculo");
        logger.debug("Antes de intanciar IMPL.");
        WsBigsaCalculoPrimaVehiculo_Impl svc =
            new WsBigsaCalculoPrimaVehiculo_Impl();

        // Inicializa el servicio (URL).
        logger.debug("Antes de Ininicalizar el servicio.");
        WsBigsaCalculoPrimaVehiculoImplDelegate ws =
            this.inicializarWS(svc.getWsBigsaCalculoPrimaVehiculoImplPort(),
                params);

        PrimaVehiculoVO result = null;
        ValorizacionSeguro valorizacion = null;

        try {

            int codigoPlan = (int) cotizacionVehiculo.getCodigoPlan();
            int marca = (int) cotizacionVehiculo.getCodigoMarca();
            int tipoVehiculo = (int) cotizacionVehiculo.getCodigoTipoVehiculo();
            int codigoModelo = (int) cotizacionVehiculo.getCodigoModelo();
            int anyoVehiculo = (int) cotizacionVehiculo.getAnyoVehiculo();
            int montoAsegurado = (int) cotizacionVehiculo.getMontoAsegurado();
            int rutCliente = (int) cotizacionVehiculo.getRutCliente();

            String estadoCivilAsegurado = "";
            int edadAsegurado = -1;
            int edadAseguradoDuenyo = -1;
            String sexoAsegurado = "";
            String idComunaAsegurado = "";

            if (cotizacionVehiculo.isClienteEsAsegurado()) {
                Contratante contratante = cotizacionVehiculo.getContratante();
                estadoCivilAsegurado = contratante.getEstadoCivil();
                //                edadAsegurado =
                //                    calcularEdadContratante(contratante.getFechaDeNacimiento());
                sexoAsegurado = contratante.getSexo();
                idComunaAsegurado = contratante.getIdComuna();
            } else {
                Contratante asegurado = cotizacionVehiculo.getAsegurado();
                estadoCivilAsegurado = asegurado.getEstadoCivil();
                                edadAseguradoDuenyo =
                                    calcularEdadContratante(asegurado.getFechaDeNacimiento());
                sexoAsegurado = asegurado.getSexo();
                idComunaAsegurado = asegurado.getIdComuna();
                rutCliente =
                    Integer.valueOf(String.valueOf(asegurado.getRut()));
            }

            edadAsegurado = (int) cotizacionVehiculo.getEdadConductor();
            Date fechaNacimiento = cotizacionVehiculo.getFechaNacimiento();
            
            //PREGUNTAS DE SCORING
            //42: -> (Valor alternativa) CASADO, SOLTERO, VIUDO, OTRO.
            RespuestaScoringVO respuestaEstadoCivil = new RespuestaScoringVO();
            int codigoPregunta =
                ACVConfig.getInstance().getInt(SCORING_VEHICULO_ESTADO_CIVIL);
            respuestaEstadoCivil.setCodigoPregunta(codigoPregunta);
            respuestaEstadoCivil
                .setValorAlternativa(obtenerEstadoCivil(estadoCivilAsegurado));

            //106: -> (Valor alternativa) Fijo 6
            RespuestaScoringVO respuestaMedioInformativo =
                new RespuestaScoringVO();
            codigoPregunta =
                ACVConfig.getInstance().getInt(
                    SCORING_VEHICULO_MEDIO_INFORMATIVO);
            respuestaMedioInformativo.setCodigoPregunta(codigoPregunta);
            String valorMedioInformativo =
                ACVConfig.getInstance().getString(
                    SCORING_VEHICULO_RESPUESTA_MEDIO_INFORMATIVO);
            respuestaMedioInformativo
                .setValorAlternativa(valorMedioInformativo); //FIJO

            //100: -> (Valor alternativa) De 2 a 3, 4, 5
            RespuestaScoringVO respuestaNumeroPuertas =
                new RespuestaScoringVO();
            codigoPregunta =
                ACVConfig.getInstance().getInt(SCORING_VEHICULO_NUMERO_PUERTAS);
            respuestaNumeroPuertas.setCodigoPregunta(codigoPregunta);
            String nroPuertas =
                ACVConfig.getInstance().getString(
                    SCORING_VEHICULO_RESPUESTA_NUMERO_PUERTAS_DE_2_A_3);
            if (cotizacionVehiculo.getNumeroPuertas() > 3) {
                nroPuertas =
                    String.valueOf(cotizacionVehiculo.getNumeroPuertas());
            }
            respuestaNumeroPuertas.setValorAlternativa(nroPuertas);

            //101: -> (Valor alternativa) Mayores a 28 a�os, Mayores a 23 a�os, Mayores a 18 a�os
            RespuestaScoringVO respuestaEdadAsegurado =
                new RespuestaScoringVO();
            codigoPregunta =
                ACVConfig.getInstance().getInt(
                    SCORING_VEHICULO_EDAD_CONTRATANTE);
            respuestaEdadAsegurado.setCodigoPregunta(codigoPregunta);
            respuestaEdadAsegurado
                .setValorAlternativa(obtenerRangoEdadAsegurado(edadAsegurado));

            //59: -> (Valor alternativa) Masculino, Femenino
            RespuestaScoringVO respuestaSexoAsegurado =
                new RespuestaScoringVO();
            codigoPregunta =
                ACVConfig.getInstance().getInt(
                    SCORING_VEHICULO_SEXO_CONTRATANTE);
            respuestaSexoAsegurado.setCodigoPregunta(codigoPregunta);
            String descSexoAsegurado =
                ACVConfig.getInstance().getString(
                    SCORING_VEHICULO_RESPUESTA_SEXO_MASCULINO);
            if (sexoAsegurado.equals("F")) {
                descSexoAsegurado =
                    ACVConfig.getInstance().getString(
                        SCORING_VEHICULO_RESPUESTA_SEXO_FEMENINO);
            }
            respuestaSexoAsegurado.setValorAlternativa(descSexoAsegurado);

            //79: -> (Codigo Comuna) Codigo de comuna
            RespuestaScoringVO respuestaCodigoComuna = new RespuestaScoringVO();
            codigoPregunta =
                ACVConfig.getInstance().getInt(SCORING_VEHICULO_CODIGO_COMUNA);
            respuestaCodigoComuna.setCodigoPregunta(codigoPregunta);
            //int codigoComuna = Integer.valueOf(idComunaAsegurado);
            respuestaCodigoComuna.setCodigoComuna(631);
            
            
            cl.cencosud.bigsa.coberturas.vehiculo.webservice.client.was.RespuestaScoringVO respuestaEdad = null;
            //54: -> (Valor numeral) Edad conductores del vehiculo 
            if (cotizacionVehiculo.isClienteEsAsegurado()== false){
            	respuestaEdad =
                    new cl.cencosud.bigsa.coberturas.vehiculo.webservice.client.was.RespuestaScoringVO();
                codigoPregunta =
                    ACVConfig.getInstance().getInt(SCORING_VEHICULO_EDAD_CONDUCTOR);
                respuestaEdad.setCodigoPregunta(codigoPregunta);
                respuestaEdad.setValorNumeral(Float.valueOf(edadAseguradoDuenyo));
            }
            else {
            respuestaEdad =
                new cl.cencosud.bigsa.coberturas.vehiculo.webservice.client.was.RespuestaScoringVO();
            codigoPregunta =
                ACVConfig.getInstance().getInt(SCORING_VEHICULO_EDAD_CONDUCTOR);
            respuestaEdad.setCodigoPregunta(codigoPregunta);
            respuestaEdad.setValorNumeral(Float.valueOf(calcularEdadContratante(fechaNacimiento)));
            }

            RespuestaScoringVO[] oRespuestas =
                new RespuestaScoringVO[] { respuestaEstadoCivil,
                    respuestaMedioInformativo, respuestaNumeroPuertas,
                    respuestaEdadAsegurado, respuestaSexoAsegurado,
                    respuestaCodigoComuna};

            logger.debug("Antes de consultar WS Prima Vehiculo...");
            logger.debug("Parametros Enviados: "
                + cotizacionVehiculo.toString());

            // LOG datos Entrada
            logger.info("VALORIZACION VEHICULO");
            logger.info("Datos de Entrada");
            logger.info("----------------");
            logger.info("Codigo Plan: " + codigoPlan);
            logger.info("Marca Vehiculo: " + marca);
            logger.info("Tipo Vehiculo: " + tipoVehiculo);
            logger.info("Modelo Vehiculo: " + codigoModelo);
            logger.info("A�o Vehiculo: " + anyoVehiculo);
            logger.info("Monto Asegurado: " + montoAsegurado);
            logger.info("Rut: " + rutCliente);
            logger.info("****************");
            logger.info("SCORING:");
            logger.info("codigo pregunta: "
                + respuestaEstadoCivil.getCodigoPregunta());
            logger.info("valor: " + respuestaEstadoCivil.getValorAlternativa());
            logger.info("codigo pregunta: "
                + respuestaMedioInformativo.getCodigoPregunta());
            logger.info("valor: "
                + respuestaMedioInformativo.getValorAlternativa());
            logger.info("codigo pregunta: "
                + respuestaNumeroPuertas.getCodigoPregunta());
            logger.info("valor: "
                + respuestaNumeroPuertas.getValorAlternativa());
            logger.info("codigo pregunta: "
                + respuestaEdadAsegurado.getCodigoPregunta());
            logger.info("valor: "
                + respuestaEdadAsegurado.getValorAlternativa());
            logger.info("codigo pregunta: "
                + respuestaSexoAsegurado.getCodigoPregunta());
            logger.info("valor: "
                + respuestaSexoAsegurado.getValorAlternativa());
            logger.info("codigo pregunta: "
                + respuestaCodigoComuna.getCodigoPregunta());
            logger.info("valor: " + respuestaCodigoComuna.getCodigoComuna());
            logger.info("codigo pregunta: "
                + respuestaEdad.getCodigoPregunta());
            logger.info("valor: " + respuestaEdad.getValorNumeral());
            logger.info("****************");
            logger.info("----------------");

            logger.debug("Calculando Prima Seguro Vehiculo.");
            result =
                ws.calcularPrimaVehiculo(codigoPlan, marca, tipoVehiculo,
                    codigoModelo, anyoVehiculo, montoAsegurado, rutCliente,
                    oRespuestas);
            
            logger.debug("Calculada Prima Seguro Vehiculo.");

            if (result != null) {

                // Log Datos de Salida.
                logger.info("Datos de Salida");
                logger.info("----------------");
                logger.info("Prima Anual: " + result.getPrimaAnual());
                logger
                    .info("Prima Anual Pesos: " + result.getPrimaAnualPesos());
                logger.info("Prima Mensual: " + result.getPrimaMensual());
                logger.info("Prima Mensual Pesos: "
                    + result.getPrimaMensualPesos());
                logger.info("----------------");

                logger.debug("Valorizacion Obtenida.");
                valorizacion = new ValorizacionSeguro();
                valorizacion.setPrimaAnual(result.getPrimaAnual());
                valorizacion.setPrimaAnualPesos(Math.round(result
                    .getPrimaAnualPesos()));
                valorizacion.setPrimaMensual(result.getPrimaMensual());
                valorizacion.setPrimaMensualPesos(Math.round(result
                    .getPrimaMensualPesos()));
                valorizacion.setIdProducto(cotizacionVehiculo.getCodigoProducto());
            }

        } catch (RemoteException e) {
            logger.debug("Error durante invocando WS de valorizacion.", e);
            logger.debug("Detalle: " + e.detail);
            obtenerExcepcionWS(e);
            logger.error("Excepcion remota durante invocacion a WS.", e.detail);
            throw new ValorizacionException(
                ValorizacionException.ERROR_VALORIZACION_KEY, 0);
        } catch (cl.cencosud.bigsa.cotizacion.vehiculo.webservice.client.ErrorInternoException e) {
            logger.error("Error durante invocando WS de valorizacion.", e);
            throw new ValorizacionException(
                ValorizacionException.ERROR_VALORIZACION_KEY, 0);
        } finally {
            ResourceLeakUtil.close(dao);
        }

        return valorizacion;
    }

    /**
     * TODO Describir m�todo obtenerRangoEdadAsegurado.
     * @param edadAsegurado
     * @return
     */
    private String obtenerRangoEdadAsegurado(int edadAsegurado) {
        String rangoEdadAsegurado = "";
        if (edadAsegurado >= 18 && edadAsegurado < 23) {
            rangoEdadAsegurado = "Mayores a 18";
        } else if (edadAsegurado >= 23 && edadAsegurado < 28) {
            rangoEdadAsegurado = "Mayores a 23";
        } else if (edadAsegurado >= 28) {
            rangoEdadAsegurado = "Mayores a 28";
        }
        return rangoEdadAsegurado;
    }

    /**
     * TODO Describir m�todo calcularEdad.
     * @param cotizacionVehiculo
     * @return
     */
    private int calcularEdadContratante(Date fechaNacimiento) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(fechaNacimiento);
        GregorianCalendar now = new GregorianCalendar();
        int res = now.get(Calendar.YEAR) - cal.get(Calendar.YEAR);
        if ((cal.get(Calendar.MONTH) > now.get(Calendar.MONTH))
            || (cal.get(Calendar.MONTH) == now.get(Calendar.MONTH) && cal
                .get(Calendar.DAY_OF_MONTH) > now.get(Calendar.DAY_OF_MONTH))) {
            res--;
        }
        return res;
    }

    /**
     * TODO Describir m�todo obtenerEstadoCivil.
     * @param cotizacionVehiculo
     * @return
     */
    private String obtenerEstadoCivil(String sEstadoCivil) {
        String estadoCivil = "OTRO";
        if (sEstadoCivil.equals("1")) {
            estadoCivil = "SOLTERO";
        } else if (sEstadoCivil.equals("2")) {
            estadoCivil = "CASADO";
        } else if (sEstadoCivil.equals("3")) {
            estadoCivil = "VIUDO";
        }
        return estadoCivil;
    }

    public ValorizacionSeguro valorizarPlanVida(
        CotizacionSeguroVida cotizacionVida) throws ValorizacionException {

        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);

        Map < String, String > params = new HashMap < String, String >();
        params.put(SERVICE_ENDPOINT_KEY, dao
            .getString(WSDL_VALORIZAR_PLAN_VIDA));

        logger.debug("WS: CalculoPrima Vida");
        logger.debug("Antes de intanciar IMPL.");
        WsBigsaCalculoPrimaVida_Impl svc = new WsBigsaCalculoPrimaVida_Impl();

        // Inicializa el servicio (URL).
        logger.debug("Antes de Ininicalizar el servicio.");
        WsBigsaCalculoPrimaVidaDelegate ws =
            this.inicializarWS(svc.getWsBigsaCalculoPrimaVidaPort(), params);

        PrimaVidaVO result = null;
        ValorizacionSeguro valorizacion = null;

        try {
            logger.debug("Calculando Prima Seguro Vida.");

            int codigoPlan = (int) cotizacionVida.getCodigoPlan();
            int rutCliente = (int) cotizacionVida.getRutCliente();
            String fechaNacimiento =
                new SimpleDateFormat("ddMMyyyy").format(cotizacionVida
                    .getFechaNacimiento());
            float baseCalculo = cotizacionVida.getBaseCalculo();

            cl.cencosud.bigsa.cotizacion.vida.webservice.client.RespuestaScoringVO respuesta =
                new cl.cencosud.bigsa.cotizacion.vida.webservice.client.RespuestaScoringVO();
            int codigoPregunta =
                Integer
                    .valueOf(ACVConfig
                        .getInstance()
                        .getString(
                            "cl.cencosud.asesorcotizador.scoring.vida.pregunta.estadoCivil"));
            respuesta.setCodigoPregunta(codigoPregunta);

            String estadoCivil = "OTRO";
            if (cotizacionVida.getEstadoCivil().equals("1")) {
                estadoCivil = "SOLTERO";
            } else if (cotizacionVida.getEstadoCivil().equals("2")) {
                estadoCivil = "CASADO";
            } else if (cotizacionVida.getEstadoCivil().equals("3")) {
                estadoCivil = "VIUDO";
            }

            respuesta.setValorSN(estadoCivil);
            respuesta.setValorAlternativa(estadoCivil);

            cl.cencosud.bigsa.cotizacion.vida.webservice.client.RespuestaScoringVO[] oRespuestas =
                new cl.cencosud.bigsa.cotizacion.vida.webservice.client.RespuestaScoringVO[] { respuesta };

            // Log Datos de Entrada
            logger.info("VALORIZACION VIDA");
            logger.info("Datos de Entrada");
            logger.info("----------------");
            logger.info("Codigo Plan: " + codigoPlan);
            logger.info("Rut: " + rutCliente);
            logger.info("Fecha de Nacimient: " + fechaNacimiento);
            logger.info("Base Calculo: " + baseCalculo);

            AseguradoVO asegurado = new AseguradoVO();
            asegurado.setFechaNacimiento(fechaNacimiento);
            asegurado.setParentesco(ParentAseg._value1);
            asegurado.setRutAsegurado(rutCliente);

            AseguradoVO[] listaAsegurados = new AseguradoVO[] { asegurado };

            result =
                ws.calcularPrimaVida(codigoPlan, rutCliente, fechaNacimiento,
                    baseCalculo, listaAsegurados, oRespuestas);

            if (result != null) {

                // Log Datos de Salida.
                logger.info("Datos de Salida");
                logger.info("----------------");
                logger.info("Prima Anual: " + result.getPrimaAnual());
                logger
                    .info("Prima Anual Pesos: " + result.getPrimaAnualPesos());
                logger.info("Prima Mensual: " + result.getPrimaMensual());
                logger.info("Prima Mensual Pesos: "
                    + result.getPrimaMensualPesos());
                logger.info("----------------");

                logger.debug("Valorizacion Obtenida.");
                valorizacion = new ValorizacionSeguro();
                valorizacion.setPrimaAnual(result.getPrimaAnual());
                valorizacion.setPrimaAnualPesos(Math.round(result
                    .getPrimaAnualPesos()));
                valorizacion.setPrimaMensual(result.getPrimaMensual());
                valorizacion.setPrimaMensualPesos(Math.round(result
                    .getPrimaMensualPesos()));
            }

        } catch (RemoteException e) {
            logger.debug("Error durante invocando WS de valorizacion.", e);
            logger.debug("Detalle: " + e.detail);
            obtenerExcepcionWS(e);
            logger.error("Excepcion remota durante invocacion a WS.", e.detail);
            throw new ValorizacionException(
                ValorizacionException.ERROR_VALORIZACION_KEY, 0);
        } catch (cl.cencosud.bigsa.cotizacion.vida.webservice.client.ErrorInternoException e) {
            logger.error("Error durante invocando WS de valorizacion.", e);
            throw new ValorizacionException(
                ValorizacionException.ERROR_VALORIZACION_KEY, 0);
        } finally {
            ResourceLeakUtil.close(dao);
        }

        return valorizacion;
    }

    public void close() {
        // TODO Auto-generated method stub

    }

    public Class getDAOInterface() {
        // TODO Auto-generated method stub
        return null;
    }

    public void setDAOInterface(Class interfazDAO) {
        // TODO Auto-generated method stub

    }

    /**
     * TODO Describir m�todo inicializarServicio.
     * @param stub
     * @param param
     * @throws ExtraccionDatosException 
     */
    private < T > T inicializarWS(T stub, Map < String, ? > param)
        throws SystemException {
        if (stub == null) {
            throw new NullPointerException("Debe especificar un stub no nulo.");
        }
        if (param == null) {
            logger.warn("No se especific� URL destino del servicio.");
        }
        if (param.containsKey(SERVICE_ENDPOINT_KEY)) {
            Object endpoint = param.get(SERVICE_ENDPOINT_KEY);
            if (logger.isDebugEnabled()) {
                logger.debug("Intentando establecer URL destino: " + endpoint);
            }
            try {
                Method method =
                    stub.getClass().getMethod("_setProperty", String.class,
                        Object.class);
                logger.debug("M�todo encontrado. Invocando...");
                method.invoke(stub, Stub.ENDPOINT_ADDRESS_PROPERTY, param
                    .get(SERVICE_ENDPOINT_KEY));
                logger.debug("Propiedad establecida exitosamente.");
            } catch (SecurityException se) {
                throw new SystemException(se);
            } catch (NoSuchMethodException nsme) {
                throw new SystemException(nsme);
            } catch (IllegalArgumentException iae) {
                throw new SystemException(iae);
            } catch (IllegalAccessException iae) {
                throw new SystemException(iae);
            } catch (InvocationTargetException ite) {
                throw new SystemException(ite);
            }
        } else {
            logger.warn("No se especific� URL destino del servicio.");
        }
        return stub;
    }

    /**
     * TODO Describir m�todo obtenerExcepcionWS.
     * @param e
     * @throws ValorizacionException
     */
    private void obtenerExcepcionWS(RemoteException e)
        throws ValorizacionException {
        if (e.detail instanceof SOAPFaultException) {
            SOAPFaultException sfe = (SOAPFaultException) e.detail;
            Detail detail = sfe.getDetail();
            if (detail != null) {
                ValorizacionException vex =
                    obtenerErrorValorizacion(detail.getChildNodes());
                if (vex != null) {
                    logger.debug("Error de negocio durante invocacion a WS.",
                        vex);
                    throw vex;
                }
            }
        }
    }

    /**
     * TODO Describir m�todo obtenerErrorValorizacion.
     * @param nodes
     * @return
     */
    private ValorizacionException obtenerErrorValorizacion(NodeList nodes) {
        if (nodes == null) {
            return null;
        }
        ValorizacionException vex = null;
        String message = null;
        String codigo = null;
        for (int i = 0; i < nodes.getLength(); i++) {
            Node item = nodes.item(i);
            vex = obtenerErrorValorizacion(item.getChildNodes());
            if (logger.isDebugEnabled()) {
                logger.debug(i + ") item : " + item);
                logger.debug(i + ") name : " + item.getNodeName());
                logger.debug(i + ") value: " + item.getNodeValue());
                logger.debug(i + ") eie  : " + vex);
            }
            if (vex == null) {
                if ("mensaje".equals(item.getNodeName())) {
                    message = item.getFirstChild().getNodeValue();
                } else if ("codigo".equals(item.getNodeName())) {
                    codigo = item.getFirstChild().getNodeValue();
                }
            } else {
                return vex;
            }
        }
        if ((message != null) && (codigo != null)) {
            logger.debug("Descripcion encontrada. Se crea excepcion.");
            vex = new ValorizacionException(message, Integer.valueOf(codigo));
        }
        return vex;
    }
}
