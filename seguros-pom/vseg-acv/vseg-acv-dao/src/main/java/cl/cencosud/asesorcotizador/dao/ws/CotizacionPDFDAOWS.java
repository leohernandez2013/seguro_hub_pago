package cl.cencosud.asesorcotizador.dao.ws;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.xml.rpc.Stub;
import javax.xml.rpc.soap.SOAPFaultException;
import javax.xml.soap.Detail;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import cl.cencosud.acv.common.Contratante;
import cl.cencosud.acv.common.EstadoCivilEnum;
import cl.cencosud.acv.common.Solicitud;
import cl.cencosud.acv.common.config.ACVConfig;
import cl.cencosud.acv.common.exception.ValorizacionException;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida;
import cl.cencosud.asesorcotizador.dao.ACVConfigDAO;
import cl.cencosud.asesorcotizador.dao.ACVDAOFactory;
import cl.cencosud.asesorcotizador.dao.CotizacionPDFDAO;
import cl.cencosud.bigsa.cotizacion.consulta.webservice.client.CotizacionHogarVO;
import cl.cencosud.bigsa.cotizacion.consulta.webservice.client.CotizacionVehiculoVO;
import cl.cencosud.bigsa.cotizacion.consulta.webservice.client.CotizacionVidaVO;
import cl.cencosud.bigsa.cotizacion.consulta.webservice.client.DatosAseguradoVO;
import cl.cencosud.bigsa.cotizacion.consulta.webservice.client.DatosBenficiarioVO;
import cl.cencosud.bigsa.cotizacion.consulta.webservice.client.ErrorInternoException;
import cl.cencosud.bigsa.cotizacion.consulta.webservice.client.ObtenerCotizacionVidaPDFCotizacionVidaAseguradoVidaParentAseg;
import cl.cencosud.bigsa.cotizacion.consulta.webservice.client.SeguroHogarVO;
import cl.cencosud.bigsa.cotizacion.consulta.webservice.client.SeguroVehiculoVO;
import cl.cencosud.bigsa.cotizacion.consulta.webservice.client.SeguroVidaVO;
import cl.cencosud.bigsa.cotizacion.consulta.webservice.client.TipoEstadoCivil;
import cl.cencosud.bigsa.cotizacion.consulta.webservice.client.TipoPersonaVO;
import cl.cencosud.bigsa.cotizacion.consulta.webservice.client.WsBigsaCotizacionPDFDelegate;
import cl.cencosud.bigsa.cotizacion.consulta.webservice.client.WsBigsaCotizacionPDF_Impl;
import cl.tinet.common.config.BaseConfigurable;
import cl.tinet.common.util.resource.ResourceLeakUtil;

import com.tinet.exceptions.system.SystemException;

/**
 * TODO Falta descripcion de clase CotizacionPDFDAOWS.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 05/11/2010
 */
public class CotizacionPDFDAOWS extends BaseConfigurable implements
    CotizacionPDFDAO {

    private static Log logger =
        LogFactory.getLog(RegistrarCotizacionDAOWS.class);

    private static final String SERVICE_ENDPOINT_KEY = "SERVICE_ENDPOINT_KEY";

    private static final String USER_WS =
        "cl.cencosud.asesorcotizador.cotizador.registro.bigsa.usuario";

    private static final String WSDL_COTIZACION_PDF =
        "cl.cencosud.asesorcotizador.dao.CotizacionPDFDAOWS.wsdl.cotizacion.pdf";

    public void close() {
        // TODO Auto-generated method stub

    }

    public Class getDAOInterface() {
        // TODO Auto-generated method stub
        return null;
    }

    public void setDAOInterface(Class interfazDAO) {
        // TODO Auto-generated method stub
    }

    /**
     * TODO Describir m�todo inicializarServicio.
     * @param stub
     * @param param
     * @throws ExtraccionDatosException 
     */
    private < T > T inicializarWS(T stub, Map < String, ? > param)
        throws SystemException {
        if (stub == null) {
            throw new NullPointerException("Debe especificar un stub no nulo.");
        }
        if (param == null) {
            logger.warn("No se especific� URL destino del servicio.");
        }
        if (param.containsKey(SERVICE_ENDPOINT_KEY)) {
            Object endpoint = param.get(SERVICE_ENDPOINT_KEY);
            if (logger.isDebugEnabled()) {
                logger.debug("Intentando establecer URL destino: " + endpoint);
            }
            try {
                Method method =
                    stub.getClass().getMethod("_setProperty", String.class,
                        Object.class);
                logger.debug("M�todo encontrado. Invocando...");
                method.invoke(stub, Stub.ENDPOINT_ADDRESS_PROPERTY, param
                    .get(SERVICE_ENDPOINT_KEY));
                logger.debug("Propiedad establecida exitosamente.");
            } catch (SecurityException se) {
                throw new SystemException(se);
            } catch (NoSuchMethodException nsme) {
                throw new SystemException(nsme);
            } catch (IllegalArgumentException iae) {
                throw new SystemException(iae);
            } catch (IllegalAccessException iae) {
                throw new SystemException(iae);
            } catch (InvocationTargetException ite) {
                throw new SystemException(ite);
            }
        } else {
            logger.warn("No se especific� URL destino del servicio.");
        }
        return stub;
    }

    /**
     * TODO Describir m�todo obtenerExcepcionWS.
     * @param e
     * @throws ValorizacionException
     */
    private void obtenerExcepcionWS(RemoteException e)
        throws ValorizacionException {
        if (e.detail instanceof SOAPFaultException) {
            SOAPFaultException sfe = (SOAPFaultException) e.detail;
            Detail detail = sfe.getDetail();
            if (detail != null) {
                ValorizacionException vex =
                    obtenerErrorValorizacion(detail.getChildNodes());
                if (vex != null) {
                    logger.debug("Error de negocio durante invocacion a WS.",
                        vex);
                    throw vex;
                }
            }
        }
    }

    /**
     * TODO Describir m�todo obtenerErrorValorizacion.
     * @param nodes
     * @return
     */
    private ValorizacionException obtenerErrorValorizacion(NodeList nodes) {
        if (nodes == null) {
            return null;
        }
        ValorizacionException vex = null;
        String message = null;
        String codigo = null;
        for (int i = 0; i < nodes.getLength(); i++) {
            Node item = nodes.item(i);
            vex = obtenerErrorValorizacion(item.getChildNodes());
            if (logger.isDebugEnabled()) {
                logger.debug(i + ") item : " + item);
                logger.debug(i + ") name : " + item.getNodeName());
                logger.debug(i + ") value: " + item.getNodeValue());
                logger.debug(i + ") eie  : " + vex);
            }
            if (vex == null) {
                if ("mensaje".equals(item.getNodeName())) {
                    message = item.getFirstChild().getNodeValue();
                } else if ("codigo".equals(item.getNodeName())) {
                    codigo = item.getFirstChild().getNodeValue();
                }
            } else {
                return vex;
            }
        }
        if ((message != null) && (codigo != null)) {
            logger.debug("Descripcion encontrada. Se crea excepcion.");
            vex = new ValorizacionException(message, Integer.valueOf(codigo));
        }
        return vex;
    }

    /**
     * Obtiene PDF con cotizacion de hogar.
     * @param cotHogar Datos de cotizacion.
     * @param solicitud Datos de la solicitud.
     * @return PDF obtenido.
     */
    public byte[] obtenerCotizacionHogarPDF(CotizacionSeguroHogar cotHogar,
        Solicitud solicitud) {

        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);

        Map < String, String > params = new HashMap < String, String >();
        params.put(SERVICE_ENDPOINT_KEY, dao.getString(
            WSDL_COTIZACION_PDF));

        WsBigsaCotizacionPDF_Impl svc = new WsBigsaCotizacionPDF_Impl();
        WsBigsaCotizacionPDFDelegate ws =
            this.inicializarWS(svc.getWsBigsaCotizacionPDFPort(), params);

        byte[] response = null;

        try {
            String sRutCliente = String.valueOf(cotHogar.getRutCliente());
            int rutCliente = Integer.parseInt(sRutCliente);

            CotizacionHogarVO cotizacion = new CotizacionHogarVO();
            cotizacion.setUsuario(ACVConfig.getInstance().getString(USER_WS));

            cotizacion.setCodigoPlan(new Long(cotHogar.getCodigoPlan())
                .intValue());

            Contratante datos = cotHogar.getContratante();
            TipoPersonaVO contratante = new TipoPersonaVO();
            contratante.setRutPersona(new Long(datos.getRut()).intValue());
            contratante.setDigitoPersona(datos.getDv());
            contratante.setApePatPersona(datos.getApellidoPaterno());
            contratante.setApeMatPersona(datos.getApellidoMaterno());
            contratante.setNombresPersona(datos.getNombre());
            contratante.setFecNacPersona(new SimpleDateFormat("ddMMyyyy")
                .format(datos.getFechaDeNacimiento()));

            String estadoCivil =
                EstadoCivilEnum.valueOf("_" + datos.getEstadoCivil())
                    .getDescripcion();
            contratante.setEstCivilPersona(TipoEstadoCivil
                .fromString(estadoCivil));

            contratante.setSexoPersona(datos.getSexo());
            contratante.setDirCallePersona(datos.getCalleDireccion());
            contratante.setDirNroPersona(datos.getNumeroDireccion());
            contratante.setDirDeptoPersona(datos
                .getNumeroDepartamentoDireccion());
            contratante.setFono1Persona(datos.getTelefono1());
            contratante.setFono2Persona(datos.getTelefono2());

            cotizacion.setContratante(contratante);

            if (cotHogar.isClienteEsAsegurado()) {
                cotizacion.setAsegurado(contratante);
                
            } else {

                Contratante asegurado = cotHogar.getAsegurado();
                TipoPersonaVO aseguradoLeg = new TipoPersonaVO();

                aseguradoLeg.setRutPersona(new Long(asegurado.getRut())
                    .intValue());
                aseguradoLeg.setDigitoPersona(asegurado.getDv());
                aseguradoLeg.setApePatPersona(asegurado.getApellidoPaterno());
                aseguradoLeg.setApeMatPersona(asegurado.getApellidoMaterno());
                aseguradoLeg.setNombresPersona(asegurado.getNombre());
                aseguradoLeg.setFecNacPersona(new SimpleDateFormat("ddMMyyyy")
                    .format(asegurado.getFechaDeNacimiento()));
                
                // Se deja por defecto ya que no se tiene estado civil asegurado.
                estadoCivil =
                    EstadoCivilEnum.valueOf("_" + 1)
                        .getDescripcion();
                contratante.setEstCivilPersona(TipoEstadoCivil
                    .fromString(estadoCivil));

                aseguradoLeg.setSexoPersona(asegurado.getSexo());
                aseguradoLeg.setDirCallePersona(asegurado.getCalleDireccion());
                aseguradoLeg.setDirNroPersona(asegurado.getNumeroDireccion());
                aseguradoLeg.setDirDeptoPersona(asegurado
                    .getNumeroDepartamentoDireccion());

                aseguradoLeg.setFono1Persona(asegurado.getTelefono1());

                cotizacion.setAsegurado(aseguradoLeg);

            }

            cotizacion.setFecIniVig(new SimpleDateFormat("ddMMyyyy")
                .format(solicitud.getFecha_creacion()));

            Calendar cal = Calendar.getInstance();
            cal.setTime(solicitud.getFecha_creacion());
            cal.add(Calendar.MONTH, 1);
            cotizacion.setFecTerVig(new SimpleDateFormat("ddMMyyyy").format(cal
                .getTime()));

            cotizacion.setPrimaAnual(solicitud.getPrima_anual_uf());

            SeguroHogarVO hogar = new SeguroHogarVO();
            hogar.setDireccionRiesgo(cotHogar.getDireccion());
            hogar.setCodigoRegionRiesgo(Integer.valueOf(cotHogar.getRegion()));
            hogar.setCodigoComunaRiesgo(Integer.valueOf(cotHogar.getComuna()));
            hogar.setCodigoCiudadRiesgo(Integer.valueOf(cotHogar.getCiudad()));

            cotizacion.setHogar(hogar);

            response = ws.obtenerCotizacionHogarPDF(rutCliente, cotizacion);

        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            try {
                logger.info("Remote Exception: ", e);
                this.obtenerExcepcionWS(e);
            } catch (ValorizacionException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            e.printStackTrace();
        } catch (ErrorInternoException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } ResourceLeakUtil.close(dao);

        return response;
    }

    /**
     * TODO Describir m�todo obtenerCotizacionVehiculoPDF.
     * @param vehiculo
     * @param solicitud
     * @return
     */
    public byte[] obtenerCotizacionVehiculoPDF(
        CotizacionSeguroVehiculo vehiculo, Solicitud solicitud) {

        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);

        String url = dao.getString(WSDL_COTIZACION_PDF);
        Map < String, String > params = new HashMap < String, String >();
        params.put(SERVICE_ENDPOINT_KEY, url);

        byte[] response = null;

        try {
            String sRutCliente = String.valueOf(vehiculo.getRutCliente());
            int rutCliente = Integer.parseInt(sRutCliente);

            WsBigsaCotizacionPDF_Impl svc = new WsBigsaCotizacionPDF_Impl();
            WsBigsaCotizacionPDFDelegate ws =
                this.inicializarWS(svc.getWsBigsaCotizacionPDFPort(), params);

            CotizacionVehiculoVO cotizacion = new CotizacionVehiculoVO();

            cotizacion.setUsuario(ACVConfig.getInstance().getString(USER_WS)); //
            cotizacion.setCodigoPlan(new Long(vehiculo.getCodigoPlan()) //
                .intValue());

            Contratante datos = vehiculo.getContratante();

            TipoPersonaVO contratante = new TipoPersonaVO();
            contratante.setRutPersona(new Long(datos.getRut()).intValue()); //
            contratante.setDigitoPersona(datos.getDv()); //
            contratante.setApePatPersona(datos.getApellidoPaterno()); //
            contratante.setApeMatPersona(datos.getApellidoMaterno()); //
            contratante.setNombresPersona(datos.getNombre()); //
            contratante.setFecNacPersona(new SimpleDateFormat("ddMMyyyy")
                .format(datos.getFechaDeNacimiento()));

            String estadoCivil =
                EstadoCivilEnum.valueOf("_" + datos.getEstadoCivil())
                    .getDescripcion();
            contratante.setEstCivilPersona(TipoEstadoCivil
                .fromString(estadoCivil));

            contratante.setSexoPersona(datos.getSexo());

            contratante.setCodigoRegionPersona(Integer.parseInt(datos
                .getIdRegion()));
            contratante.setCodigoComunaPersona(Integer.parseInt(datos
                .getIdComuna()));
            contratante.setFono1Persona(vehiculo.getNumeroTelefono1()); //
            contratante.setEmailPersona(datos.getEmail()); //

            cotizacion.setContratante(contratante);

            if (vehiculo.isClienteEsAsegurado()) {
                cotizacion.setAsegurado(contratante);
            } else {
                Contratante asegurado = vehiculo.getAsegurado();
                TipoPersonaVO aseguradoLeg = new TipoPersonaVO();

                aseguradoLeg.setRutPersona(new Long(asegurado.getRut())
                    .intValue());
                aseguradoLeg.setDigitoPersona(asegurado.getDv());
                aseguradoLeg.setApePatPersona(asegurado.getApellidoPaterno());
                aseguradoLeg.setApeMatPersona(asegurado.getApellidoMaterno());
                aseguradoLeg.setNombresPersona(asegurado.getNombre());
                aseguradoLeg.setFecNacPersona(new SimpleDateFormat("ddMMyyyy")
                    .format(asegurado.getFechaDeNacimiento()));

                estadoCivil =
                    EstadoCivilEnum.valueOf("_" + asegurado.getEstadoCivil())
                        .getDescripcion();
                aseguradoLeg.setEstCivilPersona(TipoEstadoCivil
                    .fromString(estadoCivil));

                aseguradoLeg.setSexoPersona(asegurado.getSexo());

                aseguradoLeg.setCodigoRegionPersona(Integer.parseInt(asegurado
                    .getIdRegion()));
                aseguradoLeg.setCodigoComunaPersona(Integer.parseInt(asegurado
                    .getIdComuna()));

                aseguradoLeg.setFono1Persona(asegurado.getTelefono1());

                cotizacion.setAsegurado(aseguradoLeg);

            }
            cotizacion.setFecIniVig(new SimpleDateFormat("ddMMyyyy")
                .format(solicitud.getFecha_creacion())); //

            Calendar cal = Calendar.getInstance();
            cal.setTime(solicitud.getFecha_creacion());
            cal.add(Calendar.MONTH, 1);
            cotizacion.setFecTerVig(new SimpleDateFormat("ddMMyyyy").format(cal
                .getTime())); //

            cotizacion.setPrimaAnual(solicitud.getPrima_anual_uf()); //

            SeguroVehiculoVO vehiculoLeg = new SeguroVehiculoVO();
            vehiculoLeg.setCodigoMarca(vehiculo.getCodigoMarca()); //
            vehiculoLeg.setCodigoModelo(vehiculo.getCodigoModelo()); //
            vehiculoLeg.setCodigoTipoVehiculo(vehiculo.getCodigoTipoVehiculo());//
            vehiculoLeg.setAnoVehiculo(vehiculo.getAnyoVehiculo()); //
            vehiculoLeg.setPatente(vehiculo.getPatente()); //

            cotizacion.setVehiculo(vehiculoLeg);

            response = ws.obtenerCotizacionVehiculoPDF(rutCliente, cotizacion);

        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            try {
                this.obtenerExcepcionWS(e);
            } catch (ValorizacionException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            e.printStackTrace();
        } catch (ErrorInternoException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            ResourceLeakUtil.close(dao);
        }
        return response;
    }

    public byte[] obtenerCotizacionVidaPDF(CotizacionSeguroVida cotVida,
        Solicitud solicitud) {

        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);

        String url = dao.getString(WSDL_COTIZACION_PDF);
        Map < String, String > params = new HashMap < String, String >();
        params.put(SERVICE_ENDPOINT_KEY, url);

        byte[] response = null;

        try {

            String sRutCliente = String.valueOf(cotVida.getRutCliente());
            int rutCliente = Integer.parseInt(sRutCliente);

            WsBigsaCotizacionPDF_Impl svc = new WsBigsaCotizacionPDF_Impl();
            WsBigsaCotizacionPDFDelegate ws =
                this.inicializarWS(svc.getWsBigsaCotizacionPDFPort(), params);
            CotizacionVidaVO cotizacion = new CotizacionVidaVO();

            cotizacion.setUsuario(ACVConfig.getInstance().getString(USER_WS));

            cotizacion.setCodigoPlan(new Long(cotVida.getCodigoPlan()).intValue());

            Contratante datos = cotVida.getContratante();
            TipoPersonaVO contratante = new TipoPersonaVO();
            contratante.setRutPersona(new Long(datos.getRut()).intValue());
            contratante.setDigitoPersona(datos.getDv());
            contratante.setApePatPersona(datos.getApellidoPaterno());
            contratante.setApeMatPersona(datos.getApellidoMaterno());
            contratante.setNombresPersona(datos.getNombre());
            contratante.setFecNacPersona(new SimpleDateFormat("ddMMyyyy")
                .format(datos.getFechaDeNacimiento()));

            String estadoCivil =
                EstadoCivilEnum.valueOf("_" + datos.getEstadoCivil())
                    .getDescripcion();
            
            contratante.setEstCivilPersona(TipoEstadoCivil
                .fromString(estadoCivil));
            
            
            contratante.setSexoPersona(datos.getSexo());
            contratante.setDirCallePersona(datos.getCalleDireccion());
            contratante.setDirNroPersona(datos.getNumeroDireccion());
            contratante.setDirDeptoPersona(datos
                .getNumeroDepartamentoDireccion());

            
            contratante.setFono1Persona(cotVida.getNumeroTelefono1());

            cotizacion.setContratante(contratante);
            cotizacion.setAsegurado(contratante);
            
            cotizacion.setFecIniVig(new SimpleDateFormat("ddMMyyyy")
                .format(solicitud.getFecha_creacion()));

            Calendar cal = Calendar.getInstance();
            cal.setTime(solicitud.getFecha_creacion());
            cal.add(Calendar.MONTH, 1);
            cotizacion.setFecTerVig(new SimpleDateFormat("ddMMyyyy").format(cal
                .getTime()));

            cotizacion.setPrimaAnual(solicitud.getPrima_anual_uf());
            
            SeguroVidaVO vida = new SeguroVidaVO();
            
            vida.setValorBaseCalculo(365f);
            DatosAseguradoVO aseguradoVida = new DatosAseguradoVO();
            aseguradoVida.setRutAseg(contratante.getRutPersona());
            aseguradoVida.setDigitoAseg(contratante.getDigitoPersona());
            aseguradoVida.setApePaternoAseg(contratante.getApePatPersona());
            aseguradoVida.setApeMaternoAseg(contratante.getApeMatPersona());
            aseguradoVida.setNombresAseg(contratante.getNombresPersona());
            aseguradoVida.setFecNacAseg(contratante.getFecNacPersona());
            
            aseguradoVida.setParentAseg(ObtenerCotizacionVidaPDFCotizacionVidaAseguradoVidaParentAseg.value1);
            aseguradoVida.setSexoAseg(contratante.getSexoPersona());
            
            
            DatosBenficiarioVO oBeneficiario = new DatosBenficiarioVO();
            oBeneficiario.setRutBene(contratante.getRutPersona());
            oBeneficiario.setDigitoBene(contratante.getDigitoPersona());
            oBeneficiario.setApePaternoBene(contratante.getApePatPersona());
            oBeneficiario.setApeMaternoBene(contratante.getApeMatPersona());
            oBeneficiario.setNombresBene(contratante.getNombresPersona());
            oBeneficiario.setFecNacBene(contratante.getFecNacPersona());
            oBeneficiario.setParentBene(aseguradoVida.getParentAseg().toString());
            oBeneficiario.setSexoBene(contratante.getSexoPersona());
            oBeneficiario.setPorcBene(100f);
            
            aseguradoVida.setBeneficiarioVida(new DatosBenficiarioVO[]{oBeneficiario});
            vida.setAseguradoVida(new DatosAseguradoVO[]{aseguradoVida});
            
            cotizacion.setVida(vida);

            response = ws.obtenerCotizacionVidaPDF(rutCliente, cotizacion);

        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            try {
                this.obtenerExcepcionWS(e);
            } catch (ValorizacionException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            e.printStackTrace();
        } catch (ErrorInternoException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            ResourceLeakUtil.close(dao);
        }
        return response;
    }
}
