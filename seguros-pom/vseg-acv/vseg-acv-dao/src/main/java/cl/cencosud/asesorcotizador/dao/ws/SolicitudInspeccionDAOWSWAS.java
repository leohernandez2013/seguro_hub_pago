package cl.cencosud.asesorcotizador.dao.ws;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.rpc.ServiceException;
import javax.xml.rpc.Stub;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import cl.cencosud.acv.common.SolicitudInspeccion;
import cl.cencosud.acv.common.config.ACVConfig;
import cl.cencosud.asesorcotizador.dao.ACVConfigDAO;
import cl.cencosud.asesorcotizador.dao.ACVDAOFactory;
import cl.cencosud.asesorcotizador.dao.SolicitudInspeccionDAO;
import cl.cencosud.bsp.inspeccion.webservice.client.was.WS_InspeccionLocator;
import cl.cencosud.bsp.inspeccion.webservice.client.was.WS_InspeccionSoap;
import cl.tinet.common.config.BaseConfigurable;
import cl.tinet.common.util.resource.ResourceLeakUtil;

import com.tinet.exceptions.system.SystemException;

public class SolicitudInspeccionDAOWSWAS extends BaseConfigurable implements
    SolicitudInspeccionDAO {

    private static Log logger =
        LogFactory.getLog(SolicitudInspeccionDAOWSWAS.class);

    private static final String SERVICE_ENDPOINT_KEY = "SERVICE_ENDPOINT_KEY";

    private static final String WSDL_INSPECCION_BSP =
        "cl.cencosud.asesorcotizador.dao.ws.SolicitudInspeccionDAOWS.wsdl.solicitud.inspeccion";
    
    private static final String TIPO_INSPECCION_CODE = "1";

    /**
     * TODO Describir m�todo solicitarInspeccion.
     * @param solicitudInspeccion
     * @return
     */
    public String solicitarInspeccion(SolicitudInspeccion solicitudInspeccion) {

        logger.info("ENTRANDO A WS INSPECCION");
        
        logger.info("WS: Inspeccion BSP");

        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);

        Context context = null;

        logger.info("Antes de Ininicalizar el servicio.");

        try {

            context = new InitialContext();

            WS_InspeccionLocator srv =
                (WS_InspeccionLocator) context
                    .lookup("java:comp/env/service/WS_Inspeccion");
            WS_InspeccionSoap ws =
                srv.getWS_InspeccionSoap(new URL(dao.getString(WSDL_INSPECCION_BSP)));

            String inspeccionXml = "";

            logger.info("EN DAO WAS");
            
            inspeccionXml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
            inspeccionXml += "<INSPECCION>";
            inspeccionXml += "	<COMPA�IA>" + solicitudInspeccion.getCod_compannia_inspeccion() + "</COMPA�IA>";
            inspeccionXml += "	<NOMBRE_ASEGURADO>" + solicitudInspeccion.getNombre_asegurado() + "</NOMBRE_ASEGURADO>";
            inspeccionXml += "	<RUT_ASEGURADO>" + solicitudInspeccion.getRut_asegurado() + "</RUT_ASEGURADO>";
            inspeccionXml += "  <NUMERO_DE_PATENTE>" + solicitudInspeccion.getNumero_patente() + "</NUMERO_DE_PATENTE>";
            // inspeccionXml += "	<MARCA>MARCA01</MARCA>";
            // inspeccionXml += "	<MODELO>MODELO01</MODELO>";
            inspeccionXml += "	<NOMBRE_CORREDOR>" + solicitudInspeccion.getNombre_corredor() + "</NOMBRE_CORREDOR>";
            inspeccionXml += "  <REFERENCIA_INTERNA>" + solicitudInspeccion.getReferencia_interna() + "</REFERENCIA_INTERNA>";
            inspeccionXml += "	<NOMBRE_CONTACTO>" + solicitudInspeccion.getNombre_contacto() + "</NOMBRE_CONTACTO>";
            inspeccionXml += "	<DIRECCION>" + solicitudInspeccion.getDireccion() + "</DIRECCION>";
            inspeccionXml += "	<COMUNA>" + solicitudInspeccion.getComuna() + "</COMUNA>";
            inspeccionXml += "	<TELEFONO_CONTACTO>" + solicitudInspeccion.getTelefono_contacto() + "</TELEFONO_CONTACTO>";
            if(solicitudInspeccion.getCelular()!=null) {
                inspeccionXml += "	<CELULAR>"+solicitudInspeccion.getCelular()+"</CELULAR>";
            }
            inspeccionXml += "	<USUARIO>" + solicitudInspeccion.getUsuario_interno_bsp() + "</USUARIO>";
            //inspeccionXml += "	<TIPO_INSPECCION>" + solicitudInspeccion.getTipo_inspeccion() + "</TIPO_INSPECCION>";
            inspeccionXml += "  <TIPO_INSPECCION>" + TIPO_INSPECCION_CODE + "</TIPO_INSPECCION>";
            inspeccionXml += "	<OBSERVACIONES>" + solicitudInspeccion.getObservaciones() + "</OBSERVACIONES>";
            inspeccionXml += "	<RAMO TIPO=\"" + solicitudInspeccion.getRamo() + "\"/>";
            inspeccionXml += "</INSPECCION>";

            logger.info("LLAMAR WS DE INSPECCION");
            logger.info(inspeccionXml);
                       
            String result = "";
            if (solicitudInspeccion.getFactura() == null){
            	logger.info("ENTRO AL IF");
                result = ws.insertarInspeccion(inspeccionXml);
            	logger.info("VALOR result: "+result);
            }else{
            	logger.info("ENTRO AL ELSE");
                result = ws.insertarInspeccionConArchivo(inspeccionXml, "factura.pdf", solicitudInspeccion.getFactura());
            	logger.info("VALOR result: "+result);
            }
            
            logger.info("RESPUESTA OBTENIDA WS INSPECCION " + result);

            if (result != null) {
            	logger.info("ENTRO AL IF result != null");
                String idSolicitud = null;
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document document = db.parse(new ByteArrayInputStream(result.getBytes()));
                logger.info("VALOR document: " + document);
                NodeList elements = document.getElementsByTagName("NUMERO_SOLICITUD");
                logger.info("VALOR ELEMENTS: " + elements);
                logger.info("VALOR ELEMENTS ITEM 0: " + elements.item(0));
                logger.info("VALOR ELEMENTS ITEM 1: " + elements.item(1));
                
                logger.info("ANTES DEL IF ELEMENTS: " + elements.getLength());
                if (elements.getLength() > 0) {
                	logger.info("ENTRO AL IF ELEMENTS");
                    Node node = elements.item(0);
                    logger.info("idSolicitud con getNodeValue" + node.getNodeValue());
                    logger.info("idSolicitud con getFirstChild" + node.getFirstChild().getNodeValue()); 
                    logger.info("idSolicitud con getTextContent" + node.getTextContent());
                    //idSolicitud = node.getNodeValue();
                    idSolicitud = node.getTextContent();
                    if (idSolicitud.equals("0")) {
                    	logger.info("ENTRO AL IF idSolicitud: " + idSolicitud);
                        NodeList elementos = document.getElementsByTagName("ERROR");
                        if (elementos.getLength() > 0) {
                            Node nodeError = elementos.item(0);
                            logger.info("ERROR: Respuesta WS BSP: " + nodeError.getNodeValue());
                        }
                    }
                    logger.info("RESPUESTA DE BSP " + idSolicitud);
                    return idSolicitud;
                }
            }

            //Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(result.getBytes()));

        } catch (SystemException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ServiceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (RemoteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NamingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            ResourceLeakUtil.close(context);
            ResourceLeakUtil.close(dao);
        }

        return null;
    }

    /**
    * TODO Describir m�todo inicializarServicio.
    * @param stub
    * @param param
    * @throws ExtraccionDatosException 
    */
    private < T > T inicializarWS(T stub, Map < String, ? > param)
        throws SystemException {
        if (stub == null) {
            throw new NullPointerException("Debe especificar un stub no nulo.");
        }
        if (param == null) {
            logger.warn("No se especific� URL destino del servicio.");
        }
        if (param.containsKey(SERVICE_ENDPOINT_KEY)) {
            Object endpoint = param.get(SERVICE_ENDPOINT_KEY);
            if (logger.isDebugEnabled()) {
                logger.debug("Intentando establecer URL destino: " + endpoint);
            }
            try {
                Method method =
                    stub.getClass().getMethod("_setProperty", String.class,
                        Object.class);
                logger.debug("M�todo encontrado. Invocando...");
                method.invoke(stub, Stub.ENDPOINT_ADDRESS_PROPERTY, param
                    .get(SERVICE_ENDPOINT_KEY));
                logger.debug("Propiedad establecida exitosamente.");
            } catch (SecurityException se) {
                throw new SystemException(se);
            } catch (NoSuchMethodException nsme) {
                throw new SystemException(nsme);
            } catch (IllegalArgumentException iae) {
                throw new SystemException(iae);
            } catch (IllegalAccessException iae) {
                throw new SystemException(iae);
            } catch (InvocationTargetException ite) {
                throw new SystemException(ite);
            }
        } else {
            logger.warn("No se especific� URL destino del servicio.");
        }
        return stub;
    }

    public void close() {
        // TODO Auto-generated method stub

    }

    public Class getDAOInterface() {
        // TODO Auto-generated method stub
        return null;
    }

    public void setDAOInterface(Class interfazDAO) {
        // TODO Auto-generated method stub

    }
}
