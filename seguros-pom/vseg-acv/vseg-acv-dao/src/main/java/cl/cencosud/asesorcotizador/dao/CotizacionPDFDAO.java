package cl.cencosud.asesorcotizador.dao;

import cl.cencosud.acv.common.Solicitud;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida;
import cl.tinet.common.dao.jdbc.BaseDAO;

/**
 * Interfaz que contiene servicios para el m�dulo de Cotizaci�n.
 * <br/>
 * @author miguelgarcia
 * @version 1.0
 * @created 30/09/2010
 */
public interface CotizacionPDFDAO extends BaseDAO {

    /**
     * TODO Describir m�todo obtenerCotizacionHogarPDF.
     * @param cotHogar
     * @param solicitud
     * @return
     */
    byte[] obtenerCotizacionHogarPDF(CotizacionSeguroHogar cotHogar,
        Solicitud solicitud);

    /**
     * TODO Describir m�todo obtenerCotizacionVehiculoPDF.
     * @param vehiculo
     * @param solicitud
     * @return
     */
    byte[] obtenerCotizacionVehiculoPDF(CotizacionSeguroVehiculo vehiculo,
        Solicitud solicitud);

    /**
     * TODO Describir m�todo obtenerCotizacionVidaPDF.
     * @param cotVida
     * @param solicitud
     * @return
     */
    byte[] obtenerCotizacionVidaPDF(CotizacionSeguroVida cotVida,
        Solicitud solicitud);

}
