package cl.cencosud.asesorcotizador.dao.ws;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.xml.rpc.ServiceException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.bigsa.ws.cotizacion.ramos.impl.ErrorInternoException;
import com.bigsa.ws.cotizacion.ramos.impl.WsBigsaCoberturas;
import com.bigsa.ws.cotizacion.ramos.impl.WsBigsaCoberturasImplDelegate;
import com.tinet.exceptions.system.SystemException;

import cl.cencosud.acv.common.CoberturaSeg;
import cl.cencosud.asesorcotizador.dao.ACVConfigDAO;
import cl.cencosud.asesorcotizador.dao.ACVDAOFactory;
import cl.tinet.common.config.BaseConfigurable;

public class ConsultaCoberturaDAOWSWAS extends BaseConfigurable implements
		ConsultarCoberturaDAOWS {
	
	private static Log logger = LogFactory.getLog(ConsultaCoberturaDAOWSWAS.class);
	private static final String SERVICE_ENDPOINT_KEY = "SERVICE_ENDPOINT_KEY";

	public CoberturaSeg[] obtenerCoberturas(Integer cod) {
		 ACVDAOFactory factory = ACVDAOFactory.getInstance();
		  ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);
		  Context context = null;
		  com.bigsa.ws.cotizacion.ramos.impl.CoberturaVO[] coberturasVO = null;
		  CoberturaSeg[] coberturas = null;
			
			  try {
				context = new InitialContext();
				  WsBigsaCoberturas svc = (WsBigsaCoberturas) context
							.lookup("java:comp/env/service/WsBigsaCoberturas");
				  WsBigsaCoberturasImplDelegate delegate = //vc.getWsBigsaCoberturasImplPort(new URL("http://172.18.148.127:8080/SegurosParisBigsaWeb/WsBigsaCoberturasImplPort?wsdl"));
						  				svc.getWsBigsaCoberturasImplPort(new URL(dao.getString(WSDL_CONSULTA_COBERTURAS)));
				  
				  coberturasVO = delegate.obtenerCoberturas(cod);
				  
				   coberturas = new CoberturaSeg[coberturasVO.length];
				  
				  for(int i = 0;i<coberturasVO.length;i++){
					  CoberturaSeg cob = new CoberturaSeg();
					  cob.setCodigoCobertura(coberturasVO[i].getCodigoCobertura());
					  cob.setCodigoPlan(coberturasVO[i].getCodigoPlan());
					  cob.setDescripcion(coberturasVO[i].getDescripcion());
					  cob.setDescripDeducible(coberturasVO[i].getDescripDeducible());
					  cob.setExplicacion(coberturasVO[i].getExplicacion());
					  cob.setIva(coberturasVO[i].getIva());
					  coberturas[i] = cob;
					  
				  }
				  
				  
				  
			} catch (NamingException e) {
							
				logger.error("error al obtener jndi", e);
				
				throw new SystemException(e);
			} catch (MalformedURLException e) {
				logger.error("URL mal formada", e);
				
				throw new SystemException(e);
			} catch (ServiceException e) {
				
				logger.error("error de servicio", e);
				throw new SystemException(e);
			} catch (RemoteException e) {
				
				logger.error("error remoto", e);
				throw new SystemException(e);
			} catch (ErrorInternoException e) {
				logger.error("error interno", e);
				throw new SystemException(e);
				
			}
			  return  coberturas; 
			  
				
		
	}

	public void close() {
		// TODO Apéndice de método generado automáticamente

	}

	public void setDAOInterface(Class interfazDAO) {
		// TODO Apéndice de método generado automáticamente

	}

	public Class getDAOInterface() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

}
