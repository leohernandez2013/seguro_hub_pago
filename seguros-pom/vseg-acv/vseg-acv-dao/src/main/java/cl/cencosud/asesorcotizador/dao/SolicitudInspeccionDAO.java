package cl.cencosud.asesorcotizador.dao;

import cl.cencosud.acv.common.SolicitudInspeccion;
import cl.tinet.common.dao.jdbc.BaseDAO;

public interface SolicitudInspeccionDAO extends BaseDAO  {

	String solicitarInspeccion (SolicitudInspeccion solicitudInspeccion);
}
