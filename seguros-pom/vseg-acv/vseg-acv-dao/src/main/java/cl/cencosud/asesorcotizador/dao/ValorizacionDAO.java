package cl.cencosud.asesorcotizador.dao;

import cl.cencosud.acv.common.exception.ValorizacionException;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida;
import cl.cencosud.acv.common.valorizacion.ValorizacionSeguro;
import cl.tinet.common.dao.jdbc.BaseDAO;

/**
 * Interfaz que contiene los servicios para realizar Valorizaci�n 
 * de Cotizaciones.
 * <br/>
 * @author miguelgarcia
 * @version 1.0
 * @created 04/10/2010
 */
public interface ValorizacionDAO extends BaseDAO {

    /**
     * Calcula la prima Anual y Mensual (Pesos y UF) para un plan determinado,
     * seg�n una cotizaci�n de tipo Hogar.
     * @param cotizacionHogar Cotizacion de tipo Hogar.
     * @return Valorizacion de la cotizaci�n.
     */
    public ValorizacionSeguro valorizarPlanHogar(
        CotizacionSeguroHogar cotizacionHogar) throws ValorizacionException;

    /**
     * Calcula la prima Anual y Mensual (Pesos y UF) para un plan determinado,
     * seg�n una cotizaci�n Vehiculo.
     * @param cotizacionVehiculo Cotizacion de tipo Vehiculo.
     * @return Valorizacion de la cotizaci�n.
     * @throws ValorizacionException 
     */
    public ValorizacionSeguro valorizarPlanVehiculo(
        CotizacionSeguroVehiculo cotizacionVehiculo)
        throws ValorizacionException;

    /**
     * Calcula la prima Anual y Mensual (Pesos y UF) para un plan determinado,
     * seg�n una cotizaci�n Vida.
     * @param cotizacionVida Cotizacion de tipo Vida.
     * @return Valorizacion de la cotizaci�n.
     */
    public ValorizacionSeguro valorizarPlanVida(
        CotizacionSeguroVida cotizacionVida) throws ValorizacionException;

}
