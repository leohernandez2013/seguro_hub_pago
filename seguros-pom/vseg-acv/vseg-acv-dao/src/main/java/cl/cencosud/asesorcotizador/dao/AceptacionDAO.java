package cl.cencosud.asesorcotizador.dao;

import cl.cencosud.acv.common.Transaccion;
import cl.cencosud.acv.common.exception.ValorizacionException;
import cl.tinet.common.dao.jdbc.BaseDAO;

/**
 * TODO Falta descripcion de clase PagosDAO.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 30/10/2010
 */
public interface AceptacionDAO extends BaseDAO {

    /**
     * TODO Describir m�todo aceptaTransaccion.
     * @param transaccion
     * @throws ValorizacionException
     */
    boolean aceptaTransaccion(Transaccion transaccion)
        throws ValorizacionException;

}
