package cl.cencosud.asesorcotizador.dao.ws;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.rpc.ServiceException;
import javax.xml.rpc.Stub;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.bigsa.ws.cotizacion.ramos.clientepreferente.impl.ClientePreferenteVO;
import com.bigsa.ws.cotizacion.ramos.clientepreferente.impl.ErrorInternoException;
import com.bigsa.ws.cotizacion.ramos.clientepreferente.impl.WsBigsaClientePreferenteImplDelegate;
import com.bigsa.ws.cotizacion.ramos.clientepreferente.impl.WsBigsaClientePreferenteLocator;
import com.tinet.exceptions.system.SystemException;

import cl.cencosud.asesorcotizador.dao.ACVConfigDAO;
import cl.cencosud.asesorcotizador.dao.ACVDAOFactory;
import cl.cencosud.asesorcotizador.dao.ClientePreferenteDAO;
import cl.tinet.common.config.BaseConfigurable;

public class ClientePreferenteDAOWS extends BaseConfigurable implements ClientePreferenteDAO{
	
	private static Log logger = LogFactory.getLog(ClientePreferenteDAOWS.class);
    private static final String WSDL_CLIENTE_PREFERENTE= "cl.cencosud.asesorcotizador.dao.ClientePreferenteDAOWS.wsdl.cliente.preferente";
    private static final String SERVICE_ENDPOINT_KEY = "SERVICE_ENDPOINT_KEY";

	public String consultarDescuento(int rut) throws RemoteException {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);
        
        Map < String, String > params = new HashMap < String, String >();
        String url =  dao.getString(WSDL_CLIENTE_PREFERENTE);
        logger.info("URL WsBigsaClientePreferenteImplPort: "+url);
        
        params.put(SERVICE_ENDPOINT_KEY, url);
        
        WsBigsaClientePreferenteLocator locator = new WsBigsaClientePreferenteLocator();
        ClientePreferenteVO result = null;
        String glosaRespuesta = null;
        int[] plan = {1};
        
        try {
			WsBigsaClientePreferenteImplDelegate delegate = this.inicializarWS(locator.getWsBigsaClientePreferenteImplPort(), params);
			result = delegate.obtenerClientePreferente(rut, plan);
			
			if (result != null) {
				glosaRespuesta = result.getPopUp();
			}
			
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (ServiceException e) {
			e.printStackTrace();
		} catch (ErrorInternoException e) {
			e.printStackTrace();
		}
		
		return glosaRespuesta;
	}
	
    /**
     * TODO Describir m�todo inicializarServicio.
     * @param stub
     * @param param
     * @throws ExtraccionDatosException 
     */
    private < T > T inicializarWS(T stub, Map < String, ? > param)
        throws SystemException {
        if (stub == null) {
            throw new NullPointerException("Debe especificar un stub no nulo.");
        }
        if (param == null) {
            logger.warn("No se especific� URL destino del servicio.");
        }
        if (param.containsKey(SERVICE_ENDPOINT_KEY)) {
            Object endpoint = param.get(SERVICE_ENDPOINT_KEY);
            if (logger.isDebugEnabled()) {
                logger.debug("Intentando establecer URL destino: " + endpoint);
            }
            try {
                Method method = stub.getClass().getMethod("_setProperty", String.class, Object.class);
                logger.debug("M�todo encontrado. Invocando...");
                method.invoke(stub, Stub.ENDPOINT_ADDRESS_PROPERTY, param.get(SERVICE_ENDPOINT_KEY));
                logger.debug("Propiedad establecida exitosamente.");
            } catch (SecurityException se) {
                throw new SystemException(se);
            } catch (NoSuchMethodException nsme) {
                throw new SystemException(nsme);
            } catch (IllegalArgumentException iae) {
                throw new SystemException(iae);
            } catch (IllegalAccessException iae) {
                throw new SystemException(iae);
            } catch (InvocationTargetException ite) {
                throw new SystemException(ite);
            }
        } else {
            logger.warn("No se especific� URL destino del servicio.");
        }
        return stub;
    }
    
	public void close() {
	}

	public void setDAOInterface(Class interfazDAO) {		
	}

	public Class getDAOInterface() {
		return null;
	}

}
