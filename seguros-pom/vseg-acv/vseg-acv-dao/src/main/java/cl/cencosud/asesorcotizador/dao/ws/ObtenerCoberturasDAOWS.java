package cl.cencosud.asesorcotizador.dao.ws;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.xml.rpc.Stub;
import javax.xml.rpc.soap.SOAPFaultException;
import javax.xml.soap.Detail;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import cl.cencosud.acv.common.Cobertura;
import cl.cencosud.acv.common.config.ACVConfig;
import cl.cencosud.acv.common.exception.ValorizacionException;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida;
import cl.cencosud.asesorcotizador.dao.ACVConfigDAO;
import cl.cencosud.asesorcotizador.dao.ACVDAOFactory;
import cl.cencosud.asesorcotizador.dao.ObtenerCoberturasDAO;
import cl.cencosud.asesorcotizador.dao.ValorizacionDAO;
import cl.cencosud.bigsa.coberturas.hogar.webservice.client.CoberturaVO;
import cl.cencosud.bigsa.coberturas.hogar.webservice.client.ErrorInternoException;
import cl.cencosud.bigsa.coberturas.hogar.webservice.client.RespuestaScoringVO;
import cl.cencosud.bigsa.coberturas.hogar.webservice.client.WsBigsaCoberturasHogarImplDelegate;
import cl.cencosud.bigsa.coberturas.hogar.webservice.client.WsBigsaCoberturasHogar_Impl;
import cl.cencosud.bigsa.coberturas.vehiculo.webservice.client.WsBigsaCoberturasVehiculosImplDelegate;
import cl.cencosud.bigsa.coberturas.vehiculo.webservice.client.WsBigsaCoberturasVehiculos_Impl;
import cl.cencosud.bigsa.coberturas.vida.webservice.client.WsBigsaCoberturasVidaImplDelegate;
import cl.cencosud.bigsa.coberturas.vida.webservice.client.WsBigsaCoberturasVida_Impl;
import cl.tinet.common.config.BaseConfigurable;
import cl.tinet.common.util.resource.ResourceLeakUtil;

import com.tinet.exceptions.system.SystemException;

public class ObtenerCoberturasDAOWS extends BaseConfigurable implements
    ObtenerCoberturasDAO {

    private static Log logger =
        LogFactory.getLog(RegistrarCotizacionDAOWS.class);

    private static final String SERVICE_ENDPOINT_KEY = "SERVICE_ENDPOINT_KEY";

    private static final String WSDL_OBTENER_COB_HOGAR =
        "cl.cencosud.asesorcotizador.dao.ws.ObtenerCoberturasDAOWS.wsdl.coberturas.hogar";

    private static final String WSDL_OBTENER_COB_VIDA =
        "cl.cencosud.asesorcotizador.dao.ws.ObtenerCoberturasDAOWS.wsdl.coberturas.vida";

    private static final String WSDL_OBTENER_COB_VEHICULO =
        "cl.cencosud.asesorcotizador.dao.ws.ObtenerCoberturasDAOWS.wsdl.coberturas.vehiculo";

    private static final String USER_WS =
        "cl.cencosud.asesorcotizador.cotizador.registro.bigsa.usuario";

    private static final String SCORING_VEHICULO_ESTADO_CIVIL =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.estadoCivil";

    private static final String SCORING_VEHICULO_MEDIO_INFORMATIVO =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.medioInformativo";

    private static final String SCORING_VEHICULO_RESPUESTA_MEDIO_INFORMATIVO_CODE =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.respuesta.medioInformativoCode";

    private static final String SCORING_VEHICULO_NUMERO_PUERTAS =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.nroPuertas";

    private static final String SCORING_VEHICULO_EDAD_CONTRATANTE =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.edadContratante";

    private static final String SCORING_VEHICULO_SEXO_CONTRATANTE =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.sexoContratante";

    private static final String SCORING_VEHICULO_RESPUESTA_SEXO_MASCULINO_CODE =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.respuesta.sexoContratante.masculinoCode";

    private static final String SCORING_VEHICULO_RESPUESTA_SEXO_FEMENINO_CODE =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.respuesta.sexoContratante.femeninoCode";

    private static final String SCORING_VEHICULO_CODIGO_COMUNA =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.codigoComuna";

    private static final String SCORING_VEHICULO_EDAD_CONDUCTOR =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.pregunta.edadConductor";

    private static final String SCORING_VEHICULO_RESPUESTA_SEXO_MASCULINO =
        "cl.cencosud.asesorcotizador.scoring.vehiculo.respuesta.sexoContratante.masculino";

    public void close() {
        logger.debug("No existen recursos por cerrar en DAO.");
    }

    /**
     * TODO Describir m�todo getDAOInterface.
     * @return
     */
    public Class < ? > getDAOInterface() {
        return ValorizacionDAO.class;
    }

    /**
     * TODO Describir m�todo setDAOInterface.
     * @param interfazDAO
     */
    public void setDAOInterface(Class interfazDAO) {
        if (logger.isDebugEnabled()) {
            logger.debug("No se establece interfaz DAO " + interfazDAO);
        }
    }

    /**
     * TODO Describir m�todo obtenerExcepcionWS.
     * @param e
     * @throws ValorizacionException
     */
    private void obtenerExcepcionWS(RemoteException e)
        throws ValorizacionException {
        if (e.detail instanceof SOAPFaultException) {
            SOAPFaultException sfe = (SOAPFaultException) e.detail;
            Detail detail = sfe.getDetail();
            if (detail != null) {
                ValorizacionException vex =
                    obtenerErrorValorizacion(detail.getChildNodes());
                if (vex != null) {
                    logger.debug("Error de negocio durante invocacion a WS.",
                        vex);
                    throw vex;
                }
            }
        }
    }

    /**
     * TODO Describir m�todo obtenerErrorValorizacion.
     * @param nodes
     * @return
     */
    private ValorizacionException obtenerErrorValorizacion(NodeList nodes) {
        if (nodes == null) {
            return null;
        }
        ValorizacionException vex = null;
        String message = null;
        String codigo = null;
        for (int i = 0; i < nodes.getLength(); i++) {
            Node item = nodes.item(i);
            vex = obtenerErrorValorizacion(item.getChildNodes());
            if (logger.isDebugEnabled()) {
                logger.debug(i + ") item : " + item);
                logger.debug(i + ") name : " + item.getNodeName());
                logger.debug(i + ") value: " + item.getNodeValue());
                logger.debug(i + ") eie  : " + vex);
            }
            if (vex == null) {
                if ("mensaje".equals(item.getNodeName())) {
                    message = item.getFirstChild().getNodeValue();
                } else if ("codigo".equals(item.getNodeName())) {
                    codigo = item.getFirstChild().getNodeValue();
                }
            } else {
                return vex;
            }
        }
        if ((message != null) && (codigo != null)) {
            logger.debug("Descripcion encontrada. Se crea excepcion.");
            vex = new ValorizacionException(message, Integer.valueOf(codigo));
        }
        return vex;
    }

    /**
     * TODO Describir m�todo inicializarServicio.
     * 
     * @param stub
     * @param param
     * @throws ExtraccionDatosException
     */
    private < T > T inicializarWS(T stub, Map < String, ? > param)
        throws SystemException {
        if (stub == null) {
            throw new NullPointerException("Debe especificar un stub no nulo.");
        }
        if (param == null) {
            logger.warn("No se especific� URL destino del servicio.");
        }
        if (param.containsKey(SERVICE_ENDPOINT_KEY)) {
            Object endpoint = param.get(SERVICE_ENDPOINT_KEY);
            if (logger.isDebugEnabled()) {
                logger.debug("Intentando establecer URL destino: " + endpoint);
            }
            try {
                Method method =
                    stub.getClass().getMethod("_setProperty", String.class,
                        Object.class);
                logger.debug("M�todo encontrado. Invocando...");
                method.invoke(stub, Stub.ENDPOINT_ADDRESS_PROPERTY, param
                    .get(SERVICE_ENDPOINT_KEY));
                logger.debug("Propiedad establecida exitosamente.");
            } catch (SecurityException se) {
                throw new SystemException(se);
            } catch (NoSuchMethodException nsme) {
                throw new SystemException(nsme);
            } catch (IllegalArgumentException iae) {
                throw new SystemException(iae);
            } catch (IllegalAccessException iae) {
                throw new SystemException(iae);
            } catch (InvocationTargetException ite) {
                throw new SystemException(ite);
            }
        } else {
            logger.warn("No se especific� URL destino del servicio.");
        }
        return stub;
    }

    public Cobertura[] obtenerCoberturasHogar(CotizacionSeguroHogar primaHogar)
        throws ValorizacionException {

        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);

        Map < String, String > params = new HashMap < String, String >();
        params.put(SERVICE_ENDPOINT_KEY, dao.getString(WSDL_OBTENER_COB_HOGAR));
        WsBigsaCoberturasHogar_Impl svc = new WsBigsaCoberturasHogar_Impl();

        WsBigsaCoberturasHogarImplDelegate ws =
            this.inicializarWS(svc.getWsBigsaCoberturasHogarImplPort(), params);

        Cobertura[] coberturas = null;
        CoberturaVO[] coberturasVo = null;

        try {
            logger.debug("Calculando Prima Seguro Hogar.");

            // Pregunta Scoring HOGAR
            RespuestaScoringVO respuesta = new RespuestaScoringVO();
            int codigoPregunta =
                Integer
                    .valueOf(ACVConfig
                        .getInstance()
                        .getString(
                            "cl.cencosud.asesorcotizador.scoring.hogar.pregunta.comuna"));
            respuesta.setCodigoPregunta(codigoPregunta);
            // respuesta.setValorNumeral(Float.parseFloat(primaHogar.getComuna()));
            // respuesta.setCodigoComuna(Integer.parseInt(primaHogar.getComuna()));
            respuesta.setValorAlternativa(primaHogar.getComuna());
            respuesta.setValorSN(primaHogar.getComuna());

            RespuestaScoringVO[] oRespuestaScoring =
                new RespuestaScoringVO[] { respuesta };

            coberturasVo =
                ws.obtenerCoberturas((int) primaHogar.getCodigoPlan(),
                    (int) primaHogar.getRutCliente(), 0F, 0F, 0F,
                    oRespuestaScoring);

            if (coberturasVo != null) {

                coberturas = new Cobertura[coberturasVo.length];
                int i = 0;

                for (CoberturaVO cobertura : coberturasVo) {

                    coberturas[i] = new Cobertura();
                    coberturas[i]
                        .setDescripcion(cobertura.getNombreCobertura());
                    coberturas[i].setIdCobertura(cobertura.getCodigoCobertura()
                        + "");
                    coberturas[i].setMonto(cobertura.getMontoAsegurado());

                    i++;
                }
            }

        } catch (RemoteException e) {
            logger.debug("Error durante invocando WS de valorizacion.", e);
            logger.debug("Detalle: " + e.detail);
            obtenerExcepcionWS(e);
            logger.error("Excepcion remota durante invocacion a WS.", e.detail);
            throw new SystemException(new Exception(e.detail.toString()));
        } catch (ErrorInternoException e) {
            logger.error("Error durante invocando WS de valorizacion.", e);
            throw new SystemException(e);
        } finally {
            ResourceLeakUtil.close(dao);
        }

        return coberturas;
    }

    /**
     * TODO Describir m�todo obtenerCoberturasVehiculo.
     * @param cotizacionVehiculo
     * @return
     * @throws ValorizacionException
     */
    public Cobertura[] obtenerCoberturasVehiculo(
        CotizacionSeguroVehiculo cotizacionVehiculo)
        throws ValorizacionException {

        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);

        String url = dao.getString(WSDL_OBTENER_COB_VEHICULO);
        Map < String, String > params = new HashMap < String, String >();
        params.put(SERVICE_ENDPOINT_KEY, url);
        WsBigsaCoberturasVehiculos_Impl svc =
            new WsBigsaCoberturasVehiculos_Impl();

        WsBigsaCoberturasVehiculosImplDelegate ws =
            this.inicializarWS(svc.getWsBigsaCoberturasVehiculosImplPort(),
                params);

        Cobertura[] coberturas = null;
        cl.cencosud.bigsa.coberturas.vehiculo.webservice.client.CoberturaVO[] coberturasVo =
            null;

        try {
            logger.debug("Calculando coberturas Vehiculo.");

            int codigoPlan = (int) cotizacionVehiculo.getCodigoPlan();
            int marca = (int) cotizacionVehiculo.getCodigoMarca();
            int tipoVehiculo = (int) cotizacionVehiculo.getCodigoTipoVehiculo();
            int codigoModelo = (int) cotizacionVehiculo.getCodigoModelo();
            int anyoVehiculo = (int) cotizacionVehiculo.getAnyoVehiculo();
            int montoAsegurado = (int) cotizacionVehiculo.getMontoAsegurado();
            int rutCliente = (int) cotizacionVehiculo.getRutCliente();
            Date fechaNacimiento = cotizacionVehiculo.getFechaNacimiento();
            Date fecnac = cotizacionVehiculo.getAsegurado().getFechaDeNacimiento();   
            

            //RESPUESTAS DE SCORING
            //100: -> (Valor alternativa) 2, 3, 4, 5
            cl.cencosud.bigsa.coberturas.vehiculo.webservice.client.was.RespuestaScoringVO respuestaNumeroPuertas =
                new cl.cencosud.bigsa.coberturas.vehiculo.webservice.client.was.RespuestaScoringVO();
            int codigoPregunta =
                ACVConfig.getInstance().getInt(SCORING_VEHICULO_NUMERO_PUERTAS);
            respuestaNumeroPuertas.setCodigoPregunta(codigoPregunta);
            respuestaNumeroPuertas.setValorNumeral(cotizacionVehiculo.getNumeroPuertas());

            cl.cencosud.bigsa.coberturas.vehiculo.webservice.client.was.RespuestaScoringVO respuestaEdad = null;
            //54: -> (Valor numeral) Edad conductores del vehiculo 
            if (cotizacionVehiculo.isClienteEsAsegurado()== false) {
            	respuestaEdad =
                    new cl.cencosud.bigsa.coberturas.vehiculo.webservice.client.was.RespuestaScoringVO();
                codigoPregunta =
                    ACVConfig.getInstance().getInt(SCORING_VEHICULO_EDAD_CONDUCTOR);
                respuestaEdad.setCodigoPregunta(codigoPregunta);
                respuestaEdad.setValorNumeral(Float.valueOf(calcularEdadContratante(fecnac)));
            }
            else {
            respuestaEdad =
                new cl.cencosud.bigsa.coberturas.vehiculo.webservice.client.was.RespuestaScoringVO();
            codigoPregunta =
                ACVConfig.getInstance().getInt(SCORING_VEHICULO_EDAD_CONDUCTOR);
            respuestaEdad.setCodigoPregunta(codigoPregunta);
            respuestaEdad.setValorNumeral(Float.valueOf(calcularEdadContratante(fechaNacimiento)));
            }

            //79: -> (Codigo Comuna) Codigo de comuna
            cl.cencosud.bigsa.coberturas.vehiculo.webservice.client.was.RespuestaScoringVO respuestaCodigoComuna =
                new cl.cencosud.bigsa.coberturas.vehiculo.webservice.client.was.RespuestaScoringVO();
            codigoPregunta =
                ACVConfig.getInstance().getInt(SCORING_VEHICULO_CODIGO_COMUNA);
            respuestaCodigoComuna.setCodigoPregunta(codigoPregunta);
            //respuestaCodigoComuna.setCodigoComuna(Integer.parseInt(cotizacionVehiculo.getComuna()));
            respuestaCodigoComuna.setCodigoComuna(631);
            
            //59: -> (Valor alternativa) M: Masculino, F: Femenino
            
            cl.cencosud.bigsa.coberturas.vehiculo.webservice.client.was.RespuestaScoringVO respuestaSexoAsegurado =
                new cl.cencosud.bigsa.coberturas.vehiculo.webservice.client.was.RespuestaScoringVO();
            codigoPregunta =
                ACVConfig.getInstance().getInt(
                    SCORING_VEHICULO_SEXO_CONTRATANTE);
            respuestaSexoAsegurado.setCodigoPregunta(codigoPregunta);
            String descSexoAsegurado =
                ACVConfig.getInstance().getString(
                    SCORING_VEHICULO_RESPUESTA_SEXO_MASCULINO);
            respuestaSexoAsegurado.setValorAlternativa(descSexoAsegurado);

            cl.cencosud.bigsa.coberturas.vehiculo.webservice.client.was.RespuestaScoringVO[] oRespuestas =
                new cl.cencosud.bigsa.coberturas.vehiculo.webservice.client.was.RespuestaScoringVO[] {
                    //respuestaEstadoCivil, respuestaMedioInformativo,
                    respuestaNumeroPuertas, //respuestaEdadAsegurado,
                    respuestaSexoAsegurado, respuestaCodigoComuna,
                    respuestaEdad };

            // LOG datos Entrada
            logger.info("COBERTURA VEHICULO");
            logger.info("Datos");
            logger.info("----------------");
            logger.info("Codigo Plan: " + codigoPlan);
            logger.info("Marca Vehiculo: " + marca);
            logger.info("Tipo Vehiculo: " + tipoVehiculo);
            logger.info("Modelo Vehiculo: " + codigoModelo);
            logger.info("Anno Vehiculo: " + anyoVehiculo);
            logger.info("Monto Asegurado: " + montoAsegurado);
            logger.info("Rut: " + rutCliente);
            logger.info("****************");
            logger.info("SCORING COBERTURA:");
            logger.info("codigo pregunta: "
                + respuestaNumeroPuertas.getCodigoPregunta());
            logger.info("valor numeral: "
                + respuestaNumeroPuertas.getValorNumeral());
            logger.info("codigo pregunta: "
                + respuestaSexoAsegurado.getCodigoPregunta());
            logger.info("valor alternativa: "
                + respuestaSexoAsegurado.getValorAlternativa());
            logger.info("codigo pregunta: "
                + respuestaCodigoComuna.getCodigoPregunta());
            logger.info("valor comuna: " + respuestaCodigoComuna.getCodigoComuna());
            logger.info("codigo pregunta: "
                + respuestaEdad.getCodigoPregunta());
            logger.info("valor numeral: " + respuestaEdad.getValorNumeral());
            logger.info("****************");
            logger.info("----------------");
            logger.debug("Calculando Cobertura ");

            coberturasVo =
                ws.obtenerCoberturas(codigoPlan, marca, tipoVehiculo,
                    codigoModelo, anyoVehiculo, montoAsegurado, rutCliente,
                    null);

            if (coberturasVo != null) {

                coberturas = new Cobertura[coberturasVo.length];
                int i = 0;

                for (cl.cencosud.bigsa.coberturas.vehiculo.webservice.client.CoberturaVO cobertura : coberturasVo) {

                    coberturas[i] = new Cobertura();
                    coberturas[i]
                        .setDescripcion(cobertura.getNombreCobertura());
                    coberturas[i].setIdCobertura(cobertura.getCodigoCobertura()
                        + "");
                    coberturas[i].setMonto(cobertura.getMontoAsegurado());

                    i++;
                }
            }

        } catch (RemoteException e) {
            logger.debug("Error durante invocando WS de valorizacion.", e);
            logger.debug("Detalle: " + e.detail);
            obtenerExcepcionWS(e);
            logger.error("Excepcion remota durante invocacion a WS.", e.detail);
            throw new SystemException(new Exception(e.detail.toString()));
        } catch (cl.cencosud.bigsa.coberturas.vehiculo.webservice.client.ErrorInternoException e) {
            logger.error("Error durante invocando WS de valorizacion.", e);
            throw new SystemException(e);
        } finally {
            ResourceLeakUtil.close(dao);
        }

        return coberturas;
    }

    /**
     * TODO Describir m�todo obtenerRangoEdadAsegurado.
     * @param edadAsegurado
     * @return
     */
    private String obtenerRangoEdadAsegurado(int edadAsegurado) {
        String rangoEdadAsegurado = "";
        if (edadAsegurado >= 18 && edadAsegurado < 23) {
            rangoEdadAsegurado = "Mayores a 18 a�os";
        } else if (edadAsegurado >= 23 && edadAsegurado < 28) {
            rangoEdadAsegurado = "Mayores a 23 a�os";
        } else if (edadAsegurado >= 28) {
            rangoEdadAsegurado = "Mayores a 28 a�os";
        }
        return rangoEdadAsegurado;
    }

    /**
     * TODO Describir m�todo calcularEdad.
     * @param cotizacionVehiculo
     * @return
     */
    private int calcularEdadContratante(Date fechaNacimiento) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(fechaNacimiento);
        GregorianCalendar now = new GregorianCalendar();
        int res = now.get(Calendar.YEAR) - cal.get(Calendar.YEAR);
        if ((cal.get(Calendar.MONTH) > now.get(Calendar.MONTH))
            || (cal.get(Calendar.MONTH) == now.get(Calendar.MONTH) && cal
                .get(Calendar.DAY_OF_MONTH) > now.get(Calendar.DAY_OF_MONTH))) {
            res--;
        }
        return res;
    }

    /**
     * TODO Describir m�todo obtenerEstadoCivil.
     * @param cotizacionVehiculo
     * @return
     */
    private String obtenerEstadoCivilCode(String sEstadoCivil) {
        String estadoCivil = "O";
        if (sEstadoCivil.equals("1")) {
            estadoCivil = "S";
        } else if (sEstadoCivil.equals("2")) {
            estadoCivil = "C";
        } else if (sEstadoCivil.equals("3")) {
            estadoCivil = "V";
        }
        return estadoCivil;
    }

    public Cobertura[] obtenerCoberturasVida(CotizacionSeguroVida cotizacionVida)
        throws ValorizacionException {
        
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        ACVConfigDAO dao = factory.getVentaSegurosDAO(ACVConfigDAO.class);

        Map < String, String > params = new HashMap < String, String >();
        params.put(SERVICE_ENDPOINT_KEY, dao.getString(WSDL_OBTENER_COB_VIDA));
        WsBigsaCoberturasVida_Impl svc = new WsBigsaCoberturasVida_Impl();

        WsBigsaCoberturasVidaImplDelegate ws =
            this.inicializarWS(svc.getWsBigsaCoberturasVidaImplPort(), params);

        Cobertura[] coberturas = null;
        cl.cencosud.bigsa.coberturas.vida.webservice.client.CoberturaVO[] coberturasVo =
            null;

        try {
            logger.debug("Calculando Prima Seguro Hogar.");

            cl.cencosud.bigsa.coberturas.vida.webservice.client.RespuestaScoringVO respuesta =
                new cl.cencosud.bigsa.coberturas.vida.webservice.client.RespuestaScoringVO();
            int codigoPregunta =
                Integer
                    .valueOf(ACVConfig
                        .getInstance()
                        .getString(
                            "cl.cencosud.asesorcotizador.scoring.vida.pregunta.estadoCivil"));
            respuesta.setCodigoPregunta(codigoPregunta);

            String estadoCivil = "OTRO";
            if (cotizacionVida.getEstadoCivil().equals("1")) {
                estadoCivil = "SOLTERO";
            } else if (cotizacionVida.getEstadoCivil().equals("2")) {
                estadoCivil = "CASADO";
            } else if (cotizacionVida.getEstadoCivil().equals("3")) {
                estadoCivil = "VIUDO";
            }

            respuesta.setValorSN(estadoCivil);
            respuesta.setValorAlternativa(estadoCivil);

            cl.cencosud.bigsa.coberturas.vida.webservice.client.RespuestaScoringVO[] oRespuestas =
                new cl.cencosud.bigsa.coberturas.vida.webservice.client.RespuestaScoringVO[] { respuesta };

            String fechaNacimiento =
                new SimpleDateFormat("ddMMyyyy").format(cotizacionVida
                    .getFechaNacimiento());

            coberturasVo =
                ws.obtenerCoberturas((int) cotizacionVida.getCodigoPlan(),
                    (int) cotizacionVida.getRutCliente(), fechaNacimiento,
                    cotizacionVida.getBaseCalculo(), oRespuestas);

            if (coberturasVo != null) {

                coberturas = new Cobertura[coberturasVo.length];
                int i = 0;

                for (cl.cencosud.bigsa.coberturas.vida.webservice.client.CoberturaVO cobertura : coberturasVo) {

                    coberturas[i] = new Cobertura();
                    coberturas[i]
                        .setDescripcion(cobertura.getNombreCobertura());
                    coberturas[i].setIdCobertura(cobertura.getCodigoCobertura()
                        + "");
                    coberturas[i].setMonto(cobertura.getMontoAsegurado());

                    i++;
                }
            }

        } catch (RemoteException e) {
            logger.debug("Error durante invocando WS de valorizacion.", e);
            logger.debug("Detalle: " + e.detail);
            obtenerExcepcionWS(e);
            logger.error("Excepcion remota durante invocacion a WS.", e.detail);
            throw new SystemException(new Exception(e.detail.toString()));
        } catch (cl.cencosud.bigsa.coberturas.vida.webservice.client.ErrorInternoException e) {
            logger.error("Error durante invocando WS de valorizacion.", e);
            throw new SystemException(e);
        } finally {
            ResourceLeakUtil.close(dao);
        }

        return coberturas;
    }
}
