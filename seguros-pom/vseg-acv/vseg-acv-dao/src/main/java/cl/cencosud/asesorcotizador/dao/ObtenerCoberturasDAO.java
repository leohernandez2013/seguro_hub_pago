package cl.cencosud.asesorcotizador.dao;

import cl.cencosud.acv.common.Cobertura;
import cl.cencosud.acv.common.exception.ValorizacionException;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida;
import cl.tinet.common.dao.jdbc.BaseDAO;

public interface ObtenerCoberturasDAO extends BaseDAO {
    
	Cobertura[] obtenerCoberturasVida(CotizacionSeguroVida cotizacionVida) throws ValorizacionException ;
    
	Cobertura[] obtenerCoberturasHogar(CotizacionSeguroHogar primaHogar) throws ValorizacionException ;
    
	Cobertura[] obtenerCoberturasVehiculo(CotizacionSeguroVehiculo cotizacionVehiculo) throws ValorizacionException ;
	
}
