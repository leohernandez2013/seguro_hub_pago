package cl.cencosud.asesorcotizador.dao.jdbc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.tinet.exceptions.system.SystemException;

import cl.cencosud.acv.common.Actividad;
import cl.cencosud.acv.common.Beneficiario;
import cl.cencosud.acv.common.Cobertura;
import cl.cencosud.acv.common.ColorVehiculo;
import cl.cencosud.acv.common.Contratante;
import cl.cencosud.acv.common.EstadoCivil;
import cl.cencosud.acv.common.ParametroCotizacion;
import cl.cencosud.acv.common.ParentescoAsegurado;
import cl.cencosud.acv.common.PatenteVehiculo;
import cl.cencosud.acv.common.Plan;
import cl.cencosud.acv.common.Producto;
import cl.cencosud.acv.common.Rama;
import cl.cencosud.acv.common.RestriccionVida;
import cl.cencosud.acv.common.Solicitud;
import cl.cencosud.acv.common.Subcategoria;
import cl.cencosud.acv.common.Vehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida;
import cl.cencosud.asesorcotizador.dao.CotizacionDAO;
import cl.tinet.common.dao.jdbc.managed.ManagedBaseDAOJDBC;

/**
 * Dao encargado de implementar los servicios para el m�dulo de Cotizaci�n. <br/>
 * 
 * @author miguelgarcia
 * @version 1.0
 * @created 30/09/2010
 */
public class CotizacionDAOJDBC extends ManagedBaseDAOJDBC implements
		CotizacionDAO {

	/**
	 * Logger de la clase
	 */
	private static final Log logger = LogFactory
			.getLog(CotizacionDAOJDBC.class);

	/**
	 * Consulta para obtener los datos de un Contratante.
	 */
	private static final String SQL_OBTENER_DATOS_CONTRATANTE = "select ID_CONTRATANTE as idContratante, ID_CUIDAD as "
			+ "idCiudad, RUT_CONTRATANTE as rut, DV_CONTRATANTE as dv, "
			+ "NOMBRE as nombre, APELLIDO_PATERNO as apellidoPaterno, "
			+ "APELLIDO_MATERNO as apellidoMaterno, FECHA_NACIMIENTO as "
			+ "fechaDeNacimiento, SEXO as sexo, TELEFONO_2 as telefono2, "
			+ "TELEFONO_1 as telefono1, EMAIL as email, NUMERO_DEPARTAMENTO "
			+ "as numeroDepartamentoDireccion, NUMERO as numeroDireccion, CALLE as "
			+ "calleDireccion, TIPO_TELEFONO1 as tipoTelefono1, TIPO_TELEFONO2 as "
			+ "tipoTelefono2, ID_REGION as idRegion, ID_COMUNA as idComuna, ESTADO_CIVIL as estadoCivil "
			+ "from CONTRATANTE where ID_SOLICITUD = ?";

	private static final String SQL_OBTENER_DATOS_ASEGURADO = "select ID_ASEGURADO as idContratante, ID_CUIDAD as "
			+ "idCiudad, RUT_ASEGURADO as rut, DV_ASEGURADO as dv, NOMBRE "
			+ "as nombre, APELLIDO_PATERNO as apellidoPaterno, APELLIDO_MATERNO "
			+ "as apellidoMaterno, FECHA_NACIMIENTO as fechaDeNacimiento,SEXO as sexo, "
			+ "TELEFONO_2 as telefono2, TELEFONO_1 as telefono1, EMAIL as email, "
			+ "NUMERO_DEPARTAMENTO as numeroDepartamentoDireccion, NUMERO as "
			+ "numeroDireccion, CALLE as calleDireccion, TIPO_TELEFONO1 as "
			+ "tipoTelefono1, TIPO_TELEFONO2 as tipoTelefono2, ID_REGION as idRegion, "
			+ "ID_COMUNA as idComuna, ESTADO_CIVIL as estadoCivil from ASEGURADO "
			+ "where ID_SOLICITUD = ?";

	private static final String SQL_OBTENER_TIPOS_VEHICULOS = "SELECT ID_TIPO_VEHICULO AS idTipoVehiculo, DESCRIPCION_TIPO AS descripcionTipoVehiculo "
			+ "FROM TIPO_VEHICULO_LEG  WHERE ES_ACTIVO = 1";

	private static final String SQL_OBTENER_TIPOS_VEHICULOS_TIPORAMA = "SELECT ID_TIPO_VEHICULO AS idTipoVehiculo, DESCRIPCION_TIPO AS descripcionTipoVehiculo "
			+ "FROM TIPO_VEHICULO_LEG  WHERE ES_ACTIVO = 1 AND ID_TIPO_RAMA = ?";

	private static final String SQL_OBTENER_MARCA_VEHICULOS = "SELECT ID_MARCA_VEHICULO as idMarcaVehiculo, DESCRIPCION_MARCA as marcaVehiculo "
			+ "FROM MARCA_VEHICULO_LEG WHERE ES_ACTIVO = 1 order by DESCRIPCION_MARCA";

	private static final String SQL_OBTENER_MARCA_POR_TIPO_VEHICULO = "SELECT MA.ID_MARCA_VEHICULO as idMarcaVehiculo, MA.DESCRIPCION_MARCA as marcaVehiculo "
			+ "FROM MARCA_VEHICULO_LEG MA WHERE MA.ES_ACTIVO = 1 "
			+ "AND EXISTS (SELECT 1 FROM MODELO_VEHICULO_LEG MO "
			+ "WHERE MA.CODIGO_MARCA_VEHI_LEG = MO.CODIGO_MARCA_VEHI_LEG "
			+ "AND MO.ES_ACTIVO = 1 AND MO.CODIGO_TIPO_VEHI_LEG = "
			+ "(SELECT CODIGO_TIPO_VEHI_LEG FROM TIPO_VEHICULO_LEG WHERE ID_TIPO_VEHICULO = ?) ) "
			+ "order by MA.DESCRIPCION_MARCA";

	private static final String SQL_OBTENER_MODELO_VEHICULO = "SELECT ID_MODELO_VEHICULO as idModeloVehiculo, DESCRIPCION_MODELO as modeloVehiculo "
			+ "FROM MODELO_VEHICULO_LEG MO, MARCA_VEHICULO_LEG MA, TIPO_VEHICULO_LEG T "
			+ "WHERE MA.ID_MARCA_VEHICULO = ? "
			+ "AND T.ID_TIPO_VEHICULO = ? "
			+ "AND MO.CODIGO_MARCA_VEHI_LEG = MA.CODIGO_MARCA_VEHI_LEG "
			+ "AND MO.CODIGO_TIPO_VEHI_LEG = T.CODIGO_TIPO_VEHI_LEG "
			+ "AND MO.ES_ACTIVO = 1 AND MA.ES_ACTIVO = 1 AND T.ES_ACTIVO = 1 "
			+ "order by DESCRIPCION_MODELO";

	private static final String SQL_OBTENER_PLANES_COTIZACION = "SELECT DISTINCT P3.ID_PLAN AS idPlan, P4.nombre_cia_leg as compannia, P3.NOMBRE as nombrePlan, "
			+ "P3.CODIGO_PLAN_LEG as idPlanLegacy,P1.ID_PRODUCTO as idProducto, "
			+ "CASE "
			+ "WHEN P3.ID_PLAN = PS.ID_PLAN AND PS.TIPO LIKE 'BSXR' THEN PS.IMAGEN "
			+ "ELSE NULL "
			+ "END as imagenPS "
			+ "FROM PRODUCTO  P1, PRODUCTO_LEG P2, PLAN_LEG P3, PLAN_COMPANNIA p4, PROMOCION_SECUNDARIA PS "
			+ "WHERE P1.ID_PRODUCTO = ? "
			+ "AND P1.ID_PRODUCTO_LEG = P2.ID_PRODUCTO "
			+ "AND P2.CODIGO_TIPO_PROD_LEG = P3.CODIGO_TIPO_PROD_LEG "
			+ "AND P2.CODIGO_SUB_TIPO_PROD_LEG = P3.CODIGO_SUB_TIPO_PROD_LEG "
			+ "AND P2.CODIGO_PRODUCTO_LEG = P3.CODIGO_PRODUCTO_LEG "
			+ "AND P3.CODIGO_TIPO_PROD_LEG = P4.CODIGO_TIPO_PROD_LEG "
			+ "AND P3.CODIGO_SUB_TIPO_PROD_LEG = P4.CODIGO_SUB_TIPO_PROD_LEG "
			+ "AND P3.CODIGO_PRODUCTO_LEG = P4.CODIGO_PRODUCTO_LEG "
			+ "AND P3.CODIGO_PLAN_LEG = P4.CODIGO_PLAN_LEG AND P3.ES_ACTIVO=1 ";

	private static final String SQL_OBTENER_PRODUCTOS = "SELECT PRODUCTO.ID_PRODUCTO as idProducto, PRODUCTO_LEG.NOMBRE as nombreProducto "
			+ "FROM PRODUCTO, PRODUCTO_LEG "
			+ "WHERE PRODUCTO.ID_SUBCATEGORIA = (SELECT ID_SUBCATEGORIA FROM SUBCATEGORIA WHERE ID_SUBCATEGORIA = ? AND ID_RAMA = ?) "
			+ "AND PRODUCTO_LEG.ID_PRODUCTO = PRODUCTO.ID_PRODUCTO_LEG "
			+ "AND PRODUCTO.ES_ACTIVO = 1 "
			+ "AND PRODUCTO_LEG.ES_ACTIVO = 1 "
			+ "AND PRODUCTO.ES_ELIMINADO = 0 ";

	private static final String SQL_OBTENER_COBERTURAS_PLAN = "SELECT C.ID_COBERTURA AS idCobertura, C.DESCRIPCION AS descripcion, C.MONTO AS monto  "
			+ "FROM COBERTURA_LEG C " + "WHERE C.ID_PLAN = ?";

	private static final String SQL_OBTENER_ACTIVIDADES = "SELECT ID_ACTIVIDAD AS id, DESCRIPCION_ACTIVIDAD AS descripcion "
			+ "FROM ACTIVIDAD_LEG WHERE ES_ACTIVO = 1 ORDER BY DESCRIPCION";

	private static final String SQL_OBTENER_ESTADO_CIVIL = "SELECT ID_ESTADO_CIVIL as id, NOMBRE_ESTADO_CIVIL as descripcion "
			+ "FROM ESTADO_CIVIL_LEG WHERE ES_ACTIVO = 1";

	private static final String SQL_OBTENER_PAGINA_INTERMEDIA = "SELECT S.ID_SUBCATEGORIA AS id, S.TITULO_SUBCATEGORIA AS tituloSubcategoria, F.DESCRIPCION_PAGINA as descripcion "
			+ "FROM SUBCATEGORIA S, FICHA F "
			+ "WHERE S.ID_RAMA = ? "
			+ "AND S.ID_SUBCATEGORIA = F.ID_SUBCATEGORIA "
			+ "AND S.ES_ELIMINADO = 0 " + "AND F.ESTADO_FICHA = 1";

	private static final String SQL_OBTENER_REGIONES = "SELECT ID_REGION AS id, DESCRIPCION_REGION as descripcion "
			+ "FROM REGION_LEG WHERE ES_ACTIVO = 1";

	private static final String SQL_OBTENER_COMUNAS = "SELECT C.ID_COMUNA as id, C.DESCRIPCION_COMUNA as descripcion, C.CODIGO_COMUNA_LEG as idLegacy "
			+ "FROM COMUNA_LEG C, REGION_LEG R "
			+ "WHERE R.ID_REGION = ? "
			+ "AND C.CODIGO_REGION_LEG = R.CODIGO_REGION_LEG "
			+ "AND R.ES_ACTIVO = 1 AND C.ES_ACTIVO = 1 order by DESCRIPCION_COMUNA";

	private static final String SQL_OBTENER_CIUDADES = "SELECT CI.ID_CIUDAD as id, CI.DESCRIPCION_CIUDAD as descripcion "
			+ "FROM COMUNA_LEG C, CIUDAD_LEG CI  WHERE C.ID_COMUNA = ? "
			+ "AND C.CODIGO_COMUNA_LEG = CI.CODIGO_COMUNA_LEG AND C.ES_ACTIVO = 1 "
			+ "AND CI.ES_ACTIVO = 1 order by DESCRIPCION_CIUDAD";

	private static final String SQL_OBTENER_PARAMETRO = "select ID_PARAMETRO, GRUPO, NOMBRE, DESCRIPCION, VALOR "
			+ "from PARAMETRO_SISTEMA where GRUPO = ?";

	private static final String SQL_OBTENER_VEHICULO_LEG = "SELECT MOV.CODIGO_TIPO_VEHI_LEG as idTipoVehiculo, "
			+ "MOV.CODIGO_MARCA_VEHI_LEG as idMarcaVehiculo, "
			+ "MOV.CODIGO_MODELO_VEHI_LEG as idModeloVehiculo  "
			+ "FROM TIPO_VEHICULO_LEG TV, MARCA_VEHICULO_LEG MAV, MODELO_VEHICULO_LEG MOV "
			+ "WHERE TV.ID_TIPO_VEHICULO = ? AND MAV.ID_MARCA_VEHICULO = ? "
			+ "AND MOV.ID_MODELO_VEHICULO = ? AND MOV.CODIGO_MARCA_VEHI_LEG = MAV.CODIGO_MARCA_VEHI_LEG "
			+ "AND MOV.CODIGO_TIPO_VEHI_LEG = TV.CODIGO_TIPO_VEHI_LEG";

	private static final String SQL_AGREGAR_SOLICITUD = "insert into SOLICITUD (ID_SOLICITUD, ID_PLAN, "
			+ "CLIENTE_ES_ASEGURADO, ID_RAMA, ID_PRODUCTO, "
			+ "ESTADO_SOLICITUD, NRO_TARJETA_REFERENCIA, "
			+ "FECHA_CREACION, FECHA_MODIFICACION, ID_PAIS, RUT_USUARIO, PRIMA_ANUAL_UF, PRIMA_ANUAL_PESOS, "
			+ "PRIMA_MESUAL_UF, PRIMA_MENSUAL_PESOS, ID_VITRINEO)  values "
			+ "(solicitud_seq.nextval, ?, ?, ?, ?, ?, ?,{fn CURTIME()} , {fn CURTIME()}, 1, ?, ?, ?, ?, ?, ?)";

	private static final String SQL_AGREGAR_ASEGURADO = "insert into ASEGURADO (ID_ASEGURADO, ID_SOLICITUD, ID_CUIDAD, RUT_ASEGURADO, "
			+ "DV_ASEGURADO, NOMBRE, APELLIDO_PATERNO, APELLIDO_MATERNO, FECHA_NACIMIENTO, SEXO, TELEFONO_2, "
			+ "TELEFONO_1, EMAIL, NUMERO_DEPARTAMENTO, NUMERO, CALLE, FECHA_CREACION, FECHA_MODIFICACION, ID_PAIS, "
			+ "TIPO_TELEFONO1, TIPO_TELEFONO2, ID_REGION, ID_COMUNA, ESTADO_CIVIL) "
			+ "values(asegurado_seq.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,{fn CURDATE()},{fn CURDATE()},1, ?, ?, ?, ?, ?)";

	private static final String SQL_AGREGAR_CONTRATANTE = "insert into CONTRATANTE (ID_CONTRATANTE, ID_SOLICITUD, ID_CUIDAD, RUT_CONTRATANTE, "
			+ "DV_CONTRATANTE, NOMBRE, APELLIDO_PATERNO, APELLIDO_MATERNO, FECHA_NACIMIENTO, SEXO, TELEFONO_2, "
			+ "TELEFONO_1, EMAIL, NUMERO_DEPARTAMENTO, NUMERO, CALLE, FECHA_CREACION, FECHA_MODIFICACION, ID_PAIS, "
			+ "TIPO_TELEFONO1, TIPO_TELEFONO2, ID_REGION, ID_COMUNA, ESTADO_CIVIL) "
			+ "values(contratante_seq.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,{fn CURDATE()},{fn CURDATE()},1,?, ?, ?, ?, ?)";

	private static final String SQL_AGREGAR_MAT_VEHICULO = "insert into MATERIA_ASEGURADA_VEHICULO (ID_MAT_ASEG_VEHICULO, ID_SOLICITUD, ID_MODELO_VEHICULO, ID_MARCA_VEHICULO, ID_TIPO_VEHICULO, "
			+ "ANNIO_VEHICULO, VALOR_COMERCIAL, NUMERO_PUERTAS, PATENTE, NUMERO_MOTOR, NUMERO_CHASIS, "
			+ "ID_COLOR_VEHICULO, ES_USO_PARTICULAR, EDAD_CONDUCTOR, FECHA_CREACION, FECHA_MODIFICACION, ID_PAIS) "
			+ "values (materia_asegurada_vehiculo_seq.nextval, ?,?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, {fn CURDATE()}, {fn CURDATE()}, 1)";

	private static final String SQL_ACTUALIZAR_ESTADO_SOLICITUD = "update SOLICITUD set ESTADO_SOLICITUD = ?, FECHA_MODIFICACION = {fn CURDATE()} where ID_SOLICITUD = ?";

	private static final String SQL_ACTUALIZAR_CONTRATANTE = "update CONTRATANTE set TIPO_TELEFONO1 = ?, TELEFONO_1 = ?, "
			+ "TIPO_TELEFONO2 = ?, TELEFONO_2 = ?, NOMBRE = ?, "
			+ "APELLIDO_PATERNO = ?, APELLIDO_MATERNO = ?,"
			+ " ID_REGION = ?, EMAIL = ?,"
			+ "ID_COMUNA = ?, ID_CUIDAD = ?, CALLE = ?, NUMERO = ?, "
			+ "NUMERO_DEPARTAMENTO = ?, FECHA_MODIFICACION = {fn CURDATE()} where ID_SOLICITUD = ?";

	private static final String SQL_ACTUALIZAR_ASEGURADO = "update ASEGURADO set TIPO_TELEFONO1 = ?, TELEFONO_1 = ?, "
			+ "TIPO_TELEFONO2 = ?, TELEFONO_2 = ?,"// NOMBRE = ?, "
			// + "APELLIDO_PATERNO = ?, APELLIDO_MATERNO = ?,
			+ " ID_REGION = ?, "
			+ "ID_COMUNA = ?, ID_CUIDAD = ?, CALLE = ?, NUMERO = ?, "
			+ "NUMERO_DEPARTAMENTO = ?, FECHA_MODIFICACION = {fn CURDATE()} , FECHA_NACIMIENTO = ? where ID_SOLICITUD = ?";

	private static final String SQL_ACTUALIZAR_MAT_VEH = "update MATERIA_ASEGURADA_VEHICULO set PATENTE = ?, NUMERO_MOTOR = ?, "
			+ "NUMERO_CHASIS = ?, ID_COLOR_VEHICULO = ?, "
			+ "TIPO_INSPECCION = ?, INSPECCION_BSP = ?, "
			+ "FECHA_MODIFICACION = {fn CURDATE()} where ID_SOLICITUD = ?";

	private static final String SQL_OBTENER_COLOR = "select ID_COLOR_VEHICULO as idColor, CODIGO_COLOR_VEHI_LEG as idColorLeg, "
			+ "DESCRIPCION_COLOR as descripcion from COLOR_VEHICULO_LEG where ES_ACTIVO = 1 "
			+ "and ID_PAIS = 1 order by DESCRIPCION_COLOR";

	private static final String SQL_AGREGAR_MAT_VIDA = "insert into MATERIA_ASEGURADA_VIDA (ID_MAT_ASEG_VIDA, ID_SOLICITUD, "
			+ "ID_ACTIVIDAD, FECHA_CREACION, FECHA_MODIFICACION, RUT_CLIENTE, ID_PAIS) "
			+ "values (materia_asegurada_vida_seq.nextval, ?, ?, {fn CURDATE()}, {fn CURDATE()}, ?, 1)";

	private static final String SQL_AGREGAR_BENEFICIARIO = "insert into BENEFICIARIO (ID_BENEFICIARIO, ID_MAT_ASEG_VIDA, NOMBRE, "
			+ "APELLIDO_PATERNO, APELLIDO_MATERNO, FECHA_NACIMIENTO, PARENTESCO, SEXO, "
			+ "PORCENTAJE_DISTRIBUCION, FECHA_CREACION, FECHA_MODIFICACION,ID_PAIS) "
			+ "values (beneficiario_seq.nextval, ?, ?, ?, ?, ?, ?, "
			+ "?, ?, {fn CURDATE()}, {fn CURDATE()}, 1)";

	private static final String SQL_AGREGAR_ASEGURADO_ADICIONAL = "insert into ASEGURADO_ADICIONAL(ID_ASEGURADO_ADICIONAL, ID_MAT_ASEG_VIDA, "
			+ "RUT, DV, NOMBRE, APELLIDO_PATERNO,APELLIDO_MATERNO,FECHA_NACIMIENTO,"
			+ "PARENTESCO,SEXO,FECHA_CREACION,FECHA_MODIFICACION,ID_PAIS) "
			+ "values (asegurado_adicional_seq.nextval, ?, ?, ?, ?, ?, "
			+ "?, ?, ?, ?, {fn CURDATE()}, {fn CURDATE()}, 1)";

	private static final String SQL_AGREGAR_MAT_HOGAR = "insert into MATERIA_ASEGURADA_HOGAR (ID_MAT_ASEG_HOGAR, ID_SOLICITUD, "
			+ "ID_CUIDAD, DIRECCION, NUMERO_DIRECCION, NUMERO_DEPARTAMENTO, "
			+ "MONTO_ASEGURADO_EDIFICIO, MONTO_ASEGURADO_CONTENIDO,MONTO_ASEGURADO_ROBO,"
			+ "FECHA_CREACION,FECHA_MODIFICACION,ID_PAIS) values (materia_asegurada_hogar_seq.nextval, "
			+ "?, ?, ?, ?, ?, ?, ?,?,{fn CURDATE()}, {fn CURDATE()}, 1)";

	private static final String SQL_OBTENER_PARENTESCO_REST = "select ID_PARENTESCO as idParentesco, ID_RESTRICCION as idRestriccion, "
			+ "CODIGO_LEG_PAREN as codigoLegParentesco, NOMBRE_PARENTESCO as nombre from "
			+ "PARENTESCO_ASEGURADO_LEG where ID_RESTRICCION = ?";

	private static final String SQL_OBTENER_RESTRICCION_VIDA = "select ID_RESTRICCION as idRestriccion, ID_PLAN as idPlan, REQ_GRUPO_FAMILIAR "
			+ "as requiereGrupoFamiliar, REQ_CAPITAL as requiereCapital, EDAD_MIN_INGRESO as "
			+ "edadMinimaIngreso, EDAD_MAX_HIJO as edadMaximaHijo, REQ_VALIDAR_PLAN as requiereValidarPlan, "
			+ "REQ_PREG_SCORING as requierePreguntaScoring, REQ_BENEFICIARIOS as requiereBeneficiarios"
			+ " from RESTRICCION_VIDA_LEG where ID_PLAN = ?";

	private static final String SQL_OBTENER_SOLICITUD = "select ID_SOLICITUD, ID_PLAN, CLIENTE_ES_ASEGURADO, ID_RAMA, "
			+ "ID_PRODUCTO, ESTADO_SOLICITUD, NRO_TARJETA_REFERENCIA, FECHA_CREACION, "
			+ "FECHA_MODIFICACION, RUT_USUARIO, PRIMA_ANUAL_UF, PRIMA_ANUAL_PESOS, "
			+ "PRIMA_MESUAL_UF, PRIMA_MENSUAL_PESOS from SOLICITUD where ID_SOLICITUD = ?";

	private static final String SQL_OBTENER_RAMA_POR_ID = "SELECT ID_RAMA, ID_TIPO_PRODUCTO, TITULO_RAMA, FECHA_CREACION, "
			+ "FECHA_MODIFICACION, ID_PAIS, DESCRIPCION_RAMA FROM RAMA WHERE ID_RAMA = ?";

	private static final String SQL_OBTENER_MAT_VEHICULO = "select MA.ID_MAT_ASEG_VEHICULO as idMateriaVehiculo, MA.ID_MODELO_VEHICULO as "
			+ "codigoModelo, MA.ID_MARCA_VEHICULO as codigoMarca, MA.ID_TIPO_VEHICULO as codigoTipoVehiculo, "
			+ "MA.ANNIO_VEHICULO as anyoVehiculo, MA.VALOR_COMERCIAL as montoAsegurado, "
			+ "MA.NUMERO_PUERTAS as numeroPuertas, MA.PATENTE as patente, MA.NUMERO_MOTOR as numeroMotor, "
			+ "MA.NUMERO_CHASIS as numeroChasis, MA.ID_COLOR_VEHICULO  as idColor, MA.ES_USO_PARTICULAR as "
			+ "esParticular, MA.EDAD_CONDUCTOR AS edadConductor, MA.TIPO_INSPECCION AS tipoInspeccion, FA.FACTURA "
			+ "from MATERIA_ASEGURADA_VEHICULO MA LEFT JOIN FACTURA FA "
			+ "ON FA.ID_MAT_ASEG_VEHICULO = MA.ID_MAT_ASEG_VEHICULO "
			+ "where MA.ID_SOLICITUD = ?";

	private static final String SQL_INGRESAR_FACTURA = "insert into FACTURA (ID_FACTURA, ID_MAT_ASEG_VEHICULO, "
			+ "FACTURA, FECHA_CREACION, FECHA_MODIFICACION, ID_PAIS) "
			+ "values (factura_seq.nextval, ?, ?, {fn CURDATE()}, {fn CURDATE()}, 1)";

	private static final String SQL_OBTENER_DATOS_PLAN = "SELECT P3.ID_PLAN AS idPlan, P4.nombre_cia_leg as compannia, P3.NOMBRE as nombrePlan, "
			+ "P3.CODIGO_PLAN_LEG as idPlanLegacy, p4.CODIGO_CIA_INSPECCION as codigoCompanniaInspeccion, "
			+ "P1.ID_PRODUCTO as idProducto,P3.INSPECCIONBSP as inspeccionVehi, P3.MAX_DIAS_INI_VIG as maxDiaIniVig,  "
			+ "P3.SOLICITA_NUEVO_USADO as solicitaAutoNuevo, P3.SOLICITA_FACTURA as solicitaFactura, "
			+ "P3.FORMA_PAGO as formaPago "
			+ "FROM PRODUCTO P1, PRODUCTO_LEG P2, PLAN_LEG P3, PLAN_COMPANNIA p4 "
			+ "WHERE P3.ID_PLAN = ? "
			+ "AND P1.ID_PRODUCTO_LEG = P2.ID_PRODUCTO "
			+ "AND P2.CODIGO_TIPO_PROD_LEG = P3.CODIGO_TIPO_PROD_LEG "
			+ "AND P2.CODIGO_SUB_TIPO_PROD_LEG = P3.CODIGO_SUB_TIPO_PROD_LEG "
			+ "AND P2.CODIGO_PRODUCTO_LEG = P3.CODIGO_PRODUCTO_LEG "
			+ "AND P3.CODIGO_TIPO_PROD_LEG = P4.CODIGO_TIPO_PROD_LEG "
			+ "AND P3.CODIGO_SUB_TIPO_PROD_LEG = P4.CODIGO_SUB_TIPO_PROD_LEG "
			+ "AND P3.CODIGO_PRODUCTO_LEG = P4.CODIGO_PRODUCTO_LEG "
			+ "AND P3.CODIGO_PLAN_LEG = P4.CODIGO_PLAN_LEG AND P1.ES_ACTIVO = 1";

	private static final String SQL_EXISTE_PRIMA_UNICA = "SELECT COUNT(PRI.ID_PRIMA) as existeprima FROM PLAN_LEG_PRIMA PRI, PLAN_LEG PL "
			+ "WHERE PRI.CODIGO_TIPO_PROD_LEG = PL.CODIGO_TIPO_PROD_LEG "
			+ "AND PRI.CODIGO_SUB_TIPO_PRO_LEG = PL.CODIGO_SUB_TIPO_PROD_LEG "
			+ "AND PRI.CODIGO_PRODUCTO_LEG = PL.CODIGO_PRODUCTO_LEG "
			+ "AND PRI.CODIGO_PLAN_LEG = PL.CODIGO_PLAN_LEG "
			+ "AND PRI.ID_PAIS = PL.ID_PAIS " + "AND PL.ID_PLAN = ? ";

	private static final String SQL_OBTENER_MAT_VIDA = "select ID_MAT_ASEG_VIDA as idMateriaVida, "
			+ "ID_ACTIVIDAD as actividad, RUT_CLIENTE as rutCliente "
			+ "from MATERIA_ASEGURADA_VIDA where ID_SOLICITUD = ?";

	private static final String SQL_OBTENER_MAT_HOGAR = "select ID_MAT_ASEG_HOGAR as idMateriaHogar, ID_CUIDAD as ciudad, "
			+ "DIRECCION as direccion, NUMERO_DIRECCION as numeroDireccion, "
			+ "NUMERO_DEPARTAMENTO as numeroDepto,MONTO_ASEGURADO_EDIFICIO as montoAseguradoEdificio, "
			+ "MONTO_ASEGURADO_CONTENIDO as montoAseguradoContenido, MONTO_ASEGURADO_ROBO "
			+ "as montoAseguradoRobo from MATERIA_ASEGURADA_HOGAR where ID_SOLICITUD = ?";

	private static final String SQL_ACTUALIZAR_MAT_VID = "update MATERIA_ASEGURADA_VIDA set ID_ACTIVIDAD = ?, "
			+ "RUT_CLIENTE = ? where ID_SOLICITUD = ?";

	private static final String SQL_ACTUALIZAR_MAT_HOGAR = "update MATERIA_ASEGURADA_HOGAR set ID_CUIDAD = ?, DIRECCION = ?, "
			+ "NUMERO_DIRECCION = ?, NUMERO_DEPARTAMENTO = ?, MONTO_ASEGURADO_EDIFICIO = ?, "
			+ "MONTO_ASEGURADO_CONTENIDO = ?, MONTO_ASEGURADO_ROBO = ? where ID_SOLICITUD = ?";

	private static final String OBTENER_REGION_POR_ID = "SELECT ID_REGION, CODIGO_REGION_LEG, DESCRIPCION_REGION, FECHA_CREACION, "
			+ "FECHA_MODIFICACION, ID_PAIS, ES_ACTIVO FROM REGION_LEG WHERE ID_REGION = ? ";

	private static final String OBTENER_COMUNA_POR_ID = "SELECT ID_COMUNA, CODIGO_COMUNA_LEG, CODIGO_REGION_LEG, DESCRIPCION_COMUNA, "
			+ " FECHA_CREACION, FECHA_MODIFICACION, ID_PAIS, ES_ACTIVO "
			+ "FROM COMUNA_LEG WHERE ID_COMUNA = ? ";

	private static final String OBTENER_CIUDAD_POR_ID = "SELECT ID_CIUDAD, CODIGO_CIUDAD_LEG, CODIGO_COMUNA_LEG, DESCRIPCION_CIUDAD, "
			+ "FECHA_CREACION, FECHA_MODIFICACION, ID_PAIS, ES_ACTIVO FROM CIUDAD_LEG "
			+ "WHERE ID_CIUDAD = ? ";

	private static final String SQL_OBTENER_ID_SUBCATEGORIA = "select SUBCATEGORIA.ID_SUBCATEGORIA as id from SUBCATEGORIA, "
			+ "PRODUCTO, PLAN_LEG where PRODUCTO.ID_PRODUCTO = ? and "
			+ "SUBCATEGORIA.ID_RAMA = ? and SUBCATEGORIA.ID_SUBCATEGORIA = PRODUCTO.ID_SUBCATEGORIA";

	private static final String SQL_OBTENER_SUBCAT_POR_ID = "select ID_SUBCATEGORIA as id, TITULO_SUBCATEGORIA  as tituloSubcategoria, ID_TIPO as idTipo "
			+ "from SUBCATEGORIA where ID_SUBCATEGORIA = ? and ES_ELIMINADO = 0";

	private static final String SQL_OBTENER_CLIENTE_SIN_CAPTCHA = "select 1 as existe from rutsincaptcha where RUT_USR = ?";

	private static final String SQL_GUARDAR_VITRINEO_GENERAL = "insert into VITRINEO_GENERAL (ID_VITRINEO,FECHA_CREACION, RUT, DV, NOMBRE, APELLIDO_PATERNO, APELLIDO_MATERNO,"
			+ "TIPO_TELEFONO, COD_TELEFONO, TELEFONO, MAIL, REGION, COMUNA, DIA_NACIMIENTO,MES_NACIMIENTO,"
			+ "ANYO_NACIMIENTO,ESTADO_CIVIL, SEXO, EDAD_CONDUCTOR, TIPO_VEHICULO,MARCA_VEHICULO,"
			+ "MODELO_VEHICULO, ANYO_VEHICULO, NUM_PUERTAS, ES_DUENYO, CIUDAD_HOG,DIRECCION_HOG, NUM_DIR_HOG,"
			+ "NUM_DEPTO_HOG, ANYO_HOG, RAMA, SUBCATEGORIA, PRODUCTO,RUT_DUENYO, DV_DUENYO,NOMBRE_DUENYO,APELLIDO_PATERNO_DUENYO,"
			+ "APELLIDO_MATERNO_DUENYO, DIA_NACIMIENTO_DUENYO, MES_NACIMIENTO_DUENYO, ANYO_NACIMIENTO_DUENYO, ESTADO_CIVIL_DUENYO,"
			+ "COMUNA_RESIDENCIA_DUENYO,REGION_RESIDENCIA_DUENYO, VALOR_COMERCIAL, ID_BATCH) "
			+ "values (?,{fn CURDATE()},?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, sq_id_batch_vitrineo.nextval)";

	private static final String SQL_OBTENER_VEHICULOS_SOAP = "SELECT MARCA||','||MODELO||','||CAST(ANIO_FABRICACION AS char (4)) as Vehiculo FROM VEHICULO WHERE RUT = ?";

	private static final String SQL_OBTENER_VEHICULOS_COMPLETO = "SELECT TIPO as Tipo, MARCA as Marca, MODELO as Modelo, ANIO_FABRICACION as anio FROM VEHICULO WHERE RUT = ? and MODELO = ?";

	private static final String SQL_OBTENER_DATOS_VEHICULOS_COMPLETO = "SELECT T.DESCRIPCION_TIPO as TipoDato, MR.ID_MARCA_VEHICULO as MarcaDato, MO.ID_MODELO_VEHICULO as ModeloDato "
			+ "from TIPO_VEHICULO_LEG T, MARCA_VEHICULO_LEG MR, MODELO_VEHICULO_LEG MO WHERE "
			+ "T.CODIGO_TIPO_VEHI_LEG = ? AND MR.DESCRIPCION_MARCA = ? AND MO.DESCRIPCION_MODELO = ?";

	private static final String SQL_OBTENER_DATOS_CLIENTE_VITRINEO = "SELECT RUT as rutVitrineo, DV as dvVitrineo, NOMBRE as nombreCliente, APELLIDO_PATERNO as apellidoPaterno,"
			+ "APELLIDO_MATERNO as apellidoMaterno, TIPO_TELEFONO as tipoTelefono, COD_TELEFONO as codTelefono, "
			+ "TELEFONO as telefono, MAIL as mail, REGION as region, COMUNA as comuna, DIA_NACIMIENTO as diaNac,"
			+ "MES_NACIMIENTO as mesNac, ANYO_NACIMIENTO as anyoNac, ESTADO_CIVIL as estadoCivil, SEXO as sexo,"
			+ "EDAD_CONDUCTOR as edadConductor, TIPO_VEHICULO as tipoVehiculo, MARCA_VEHICULO as marcaVehiculo,"
			+ "MODELO_VEHICULO as modeloVehiculo, ANYO_VEHICULO as anyoVehiculo, NUM_PUERTAS as numPuertas,"
			+ "ES_DUENYO as esDuenyo, CIUDAD_HOG as ciudadHog, DIRECCION_HOG as direccionHog, NUM_DIR_HOG as numDirHog, "
			+ "NUM_DEPTO_HOG as  numDeptoHog, ANYO_HOG as anyoHog, RUT_DUENYO as rutDuenyo, DV_DUENYO as dvDuenyo,"
			+ "NOMBRE_DUENYO as nombreDuenyo, APELLIDO_PATERNO_DUENYO as apellidoPaternoDuenyo, RAMA as rama, "
			+ "APELLIDO_MATERNO_DUENYO as apellidoMaternoDuenyo, DIA_NACIMIENTO_DUENYO as diaNacDuenyo,"
			+ "MES_NACIMIENTO_DUENYO as mesNacDuenyo, ANYO_NACIMIENTO_DUENYO as anyoNacDuenyo, ESTADO_CIVIL_DUENYO as estadoCivilDuenyo,"
			+ "COMUNA_RESIDENCIA_DUENYO as comunaResidenciaDuenyo, REGION_RESIDENCIA_DUENYO as regionResidenciaDuenyo,"
			+ "VALOR_COMERCIAL as valorComercial FROM VITRINEO_GENERAL where ID_VITRINEO = ?";

	private static final String SQL_OBTENER_PLANES_COTIZACION_COMPARADOR = "SELECT DISTINCT "
			+ "P3.ID_PLAN AS idPlan,"
			+ "P4.nombre_cia_leg as compannia,"
			+ "P3.NOMBRE as nombrePlan,P3.CODIGO_PLAN_LEG as idPlanLegacy,"
			+ "P1.ID_PRODUCTO as idProducto, "
			+ "CASE "
			+ "WHEN P3.ID_PLAN = PS.ID_PLAN THEN PS.IMAGEN "
			+ "ELSE NULL "
			+ "END as imagenPS "
			+ "FROM PRODUCTO  P1, PRODUCTO_LEG P2, PLAN_LEG P3, PLAN_COMPANNIA p4, PROMOCION_SECUNDARIA PS "
			+ "WHERE ((P1.ID_PRODUCTO BETWEEN 188 and 192) or (P1.ID_PRODUCTO = 88)) "
			+ "AND P1.ID_PRODUCTO_LEG = P2.ID_PRODUCTO "
			+ "AND P2.CODIGO_TIPO_PROD_LEG = P3.CODIGO_TIPO_PROD_LEG "
			+ "AND P2.CODIGO_SUB_TIPO_PROD_LEG = P3.CODIGO_SUB_TIPO_PROD_LEG "
			+ "AND P2.CODIGO_PRODUCTO_LEG = P3.CODIGO_PRODUCTO_LEG "
			+ "AND P3.CODIGO_TIPO_PROD_LEG = P4.CODIGO_TIPO_PROD_LEG "
			+ "AND P3.CODIGO_SUB_TIPO_PROD_LEG = P4.CODIGO_SUB_TIPO_PROD_LEG "
			+ "AND P3.CODIGO_PRODUCTO_LEG = P4.CODIGO_PRODUCTO_LEG "
			+ "AND P3.CODIGO_PLAN_LEG = P4.CODIGO_PLAN_LEG AND P3.ES_ACTIVO = 1";

	private static final String SQL_OBTENER_PLANES_COTIZACION_COMPARADOR_PROMOCION = "SELECT P3.ID_PLAN AS idPlan, P4.nombre_cia_leg as compannia, P3.NOMBRE as nombrePlan, "
			+ "P3.CODIGO_PLAN_LEG as idPlanLegacy, P1.ID_PRODUCTO as idProducto, PS.IMAGEN as imagenPS "
			+ "FROM PRODUCTO  P1, PRODUCTO_LEG P2, PLAN_LEG P3, PLAN_COMPANNIA p4, PROMOCION_SECUNDARIA PS "
			+ "WHERE ((P1.ID_PRODUCTO BETWEEN 188 and 192) or (P1.ID_PRODUCTO = 88))"
			+ "AND P1.ID_PRODUCTO_LEG = P2.ID_PRODUCTO "
			+ "AND P2.CODIGO_TIPO_PROD_LEG = P3.CODIGO_TIPO_PROD_LEG "
			+ "AND P2.CODIGO_SUB_TIPO_PROD_LEG = P3.CODIGO_SUB_TIPO_PROD_LEG "
			+ "AND P2.CODIGO_PRODUCTO_LEG = P3.CODIGO_PRODUCTO_LEG "
			+ "AND P3.CODIGO_TIPO_PROD_LEG = P4.CODIGO_TIPO_PROD_LEG "
			+ "AND P3.CODIGO_SUB_TIPO_PROD_LEG = P4.CODIGO_SUB_TIPO_PROD_LEG "
			+ "AND P3.CODIGO_PRODUCTO_LEG = P4.CODIGO_PRODUCTO_LEG "
			+ "AND P3.CODIGO_PLAN_LEG = P4.CODIGO_PLAN_LEG AND P3.ES_ACTIVO=1"
			+ " AND P3.ID_PLAN = PS.ID_PLAN";

	private static final String SQL_OBTENER_PLANES_COTIZACION_DEDUCIBLES = "SELECT DISTINCT P3.ID_PLAN AS idPlan, P4.nombre_cia_leg as compannia, P3.NOMBRE as nombrePlan, "
			+ "P3.CODIGO_PLAN_LEG as idPlanLegacy,P1.ID_PRODUCTO as idProducto "
			// + "CASE "
			// +
			// "WHEN P3.ID_PLAN = PS.ID_PLAN AND PS.TIPO LIKE 'BSXR' THEN PS.IMAGEN "
			// + "ELSE NULL "
			// + "END as imagenPS "
			+ "FROM PRODUCTO  P1, PRODUCTO_LEG P2, PLAN_LEG P3, PLAN_COMPANNIA p4, PROMOCION_SECUNDARIA PS "
			+ "WHERE "
			+ "P1.ID_PRODUCTO_LEG = P2.ID_PRODUCTO "
			+ "AND P2.CODIGO_TIPO_PROD_LEG = P3.CODIGO_TIPO_PROD_LEG "
			+ "AND P2.CODIGO_SUB_TIPO_PROD_LEG = P3.CODIGO_SUB_TIPO_PROD_LEG "
			+ "AND P2.CODIGO_PRODUCTO_LEG = P3.CODIGO_PRODUCTO_LEG "
			+ "AND P3.CODIGO_TIPO_PROD_LEG = P4.CODIGO_TIPO_PROD_LEG "
			+ "AND P3.CODIGO_SUB_TIPO_PROD_LEG = P4.CODIGO_SUB_TIPO_PROD_LEG "
			+ "AND P3.CODIGO_PRODUCTO_LEG = P4.CODIGO_PRODUCTO_LEG "
			+ "AND P3.CODIGO_PLAN_LEG = P4.CODIGO_PLAN_LEG AND P3.ES_ACTIVO=1 "
			+ "AND P1.ID_PRODUCTO IN (?,?,?,?,?,?) ";

	private static final String SQL_OBTENER_PLANES_COTIZACION_DEDUCIBLES2 = "SELECT * FROM ("
			+ "SELECT DISTINCT P3.ID_PLAN AS idPlan, P4.nombre_cia_leg as compannia, P3.NOMBRE as nombrePlan, "
			+ "P3.CODIGO_PLAN_LEG as idPlanLegacy,P1.ID_PRODUCTO as idProducto, "
			+ "CASE "
			+ "WHEN P3.ID_PLAN = PS.ID_PLAN AND PS.TIPO LIKE 'BSXR' THEN PS.IMAGEN "
			+ "ELSE NULL "
			+ "END as imagenPS "
			+ "FROM PRODUCTO  P1, PRODUCTO_LEG P2, PLAN_LEG P3, PLAN_COMPANNIA p4, PROMOCION_SECUNDARIA PS "
			+ "WHERE "
			+ "P1.ID_PRODUCTO_LEG = P2.ID_PRODUCTO "
			+ "AND P2.CODIGO_TIPO_PROD_LEG = P3.CODIGO_TIPO_PROD_LEG "
			+ "AND P2.CODIGO_SUB_TIPO_PROD_LEG = P3.CODIGO_SUB_TIPO_PROD_LEG "
			+ "AND P2.CODIGO_PRODUCTO_LEG = P3.CODIGO_PRODUCTO_LEG "
			+ "AND P3.CODIGO_TIPO_PROD_LEG = P4.CODIGO_TIPO_PROD_LEG "
			+ "AND P3.CODIGO_SUB_TIPO_PROD_LEG = P4.CODIGO_SUB_TIPO_PROD_LEG "
			+ "AND P3.CODIGO_PRODUCTO_LEG = P4.CODIGO_PRODUCTO_LEG "
			+ "AND P3.CODIGO_PLAN_LEG = P4.CODIGO_PLAN_LEG AND P3.ES_ACTIVO=1 "
			+ "AND P1.ID_PRODUCTO IN (?,?,?,?,?,?) and p3.id_plan = ? )cons ORDER BY imagenps";
	
	private static final String SQL_GUARDAR_MASCOTA = "INSERT INTO SEGURO_MASCOTAS " +
			"(ID_MASCOTA, RUT_CLIENTE, MAS_NOMBRE, MAS_TIPO, MAS_RAZA, MAS_COLOR, MAS_EDAD, MAS_SEXO, MAS_DIRECCION, FECHA_CREACION) " +
			"VALUES (SQ_ID_MASCOTA.NEXTVAL,?,?,?,?,?,?,?,?,{fn CURDATE()})";
	
	private static final String SQL_OBTENER_ID_FICHA = "SELECT FI.ID_FICHA FROM FICHA FI, SUBCATEGORIA SC "+
			"WHERE FI.ESTADO_FICHA = 1 "+
			"AND FI.ID_SUBCATEGORIA = SC.ID_SUBCATEGORIA "+
			"AND SC.ID_SUBCATEGORIA = ?";
	
	//Pleyasoft - Patente Vehiculo
	private static final String SQL_OBTENER_PATENTE_VEHICULO = "SELECT PV.PATENTE, PV.TIPOVEHICULO, PV.MARCA, PV.MODELO, PV.ANIO "+
			"FROM VEHICULO PV "+
			"WHERE PV.PATENTE = ?";
	
	
	
	private static final String SQL_AGREGAR_PATENTE_VEHICULO = "INSERT INTO VEHICULO "+
			"(PATENTE, TIPOVEHICULO, MARCA, MODELO, ANIO, FECHAREGISTRO) "+
			"VALUES(?, ?, ?, ?, ?, ?) ";
	
	private static final String SQL_ACTUALIZAR_PATENTE_VEHICULO = "UPDATE VEHICULO "+
			"SET TIPOVEHICULO = ?, MARCA = ?, MODELO = ?, ANIO = ?, FECHAMODIFICACION = ? "+
			"WHERE PATENTE = ?";
	//Pleyasoft - Patente Vehiculo
	
	/**
	 * @see CotizacionDAO#obtenerDatosContratante(long, char)
	 */
	public Contratante obtenerDatosContratante(long idSolicitud) {
		Contratante contratante = new Contratante();
		Object[] params = new Object[] { idSolicitud };
		List<Contratante> resultado = this.query(Contratante.class,
				SQL_OBTENER_DATOS_CONTRATANTE, params);
		if (resultado != null && resultado.size() > 0) {
			logger.debug("Datos del contratante obtenidos...");
			contratante = resultado.get(0);
		}
		return contratante;
	}

	/**
	 * @see CotizacionDAO#obtenerDatosAsegurado(long, char)
	 */
	public Contratante obtenerDatosAsegurado(long idSolicitud) {
		Object[] params = new Object[] { idSolicitud };
		List<Contratante> resultado = (ArrayList<Contratante>) this.query(
				Contratante.class, SQL_OBTENER_DATOS_ASEGURADO, params);
		Contratante asegurado = null;
		if (resultado != null) {
			logger.debug("Datos del contratante obtenidos...");
			asegurado = resultado.get(0);
		}
		return asegurado;
	}

	/**
	 * @see CotizacionDAO#obtenerMarcasVehiculos()
	 */
	public Vehiculo[] obtenerMarcasVehiculos() {
		Object[] params = new Object[] {};
		logger.debug("Ejecutando SQL obtenerMarcasVehiculos ...");
		List<Vehiculo> resultado = (ArrayList<Vehiculo>) this.query(
				Vehiculo.class, SQL_OBTENER_MARCA_VEHICULOS, params);
		logger.debug("obtenerMarcasVehiculos Resultado ...");
		return resultado.toArray(new Vehiculo[resultado.size()]);
	}

	/**
	 * @see CotizacionDAO#obtenerModelosVehiculos(String, String)
	 */
	public Vehiculo[] obtenerModelosVehiculos(String idTipo, String idMarca) {
		Object[] params = new Object[] { idMarca, idTipo };
		logger.debug("Antes de Ejecutar SQL obtenerModelosVehiculos ...");
		List<Vehiculo> resultado = (ArrayList<Vehiculo>) this.query(
				Vehiculo.class, SQL_OBTENER_MODELO_VEHICULO, params);
		logger.debug("Despues de Ejecutar SQL obtenerModelosVehiculos ...");
		return resultado.toArray(new Vehiculo[resultado.size()]);
	}

	/**
	 * @see CotizacionDAO#obtenerTiposVehiculos()
	 */
	public Vehiculo[] obtenerTiposVehiculos() {
		Object[] params = new Object[] {};
		logger.info("SQL_OBTENER_TIPOS_VEHICULOS");
		List<Vehiculo> resultado = (ArrayList<Vehiculo>) this.query(
				Vehiculo.class, SQL_OBTENER_TIPOS_VEHICULOS, params);
		logger.debug("obtenerTiposVehiculos Resultado ...");
		Vehiculo[] vehiculos = new Vehiculo[resultado.size()];
		return resultado.toArray(vehiculos);
	}

	/**
	 * @see CotizacionDAO#obtenerTiposVehiculos(int)
	 */
	public Vehiculo[] obtenerTiposVehiculos(int idTipo) {
		Object[] params = new Object[] { idTipo };
		logger.info("SQL_OBTENER_TIPOS_VEHICULOS_TIPORAMA");
		List<Vehiculo> resultado = (ArrayList<Vehiculo>) this.query(
				Vehiculo.class, SQL_OBTENER_TIPOS_VEHICULOS_TIPORAMA, params);
		Vehiculo[] vehiculos = new Vehiculo[resultado.size()];
		return resultado.toArray(vehiculos);
	}

	/**
	 * @see CotizacionDAO#obtenerPlanesCotizacion(String)
	 */
	public Plan[] obtenerPlanesCotizacion(String idProducto,
			String idPlanPromocion) {

		List<Plan> resultado = null;
		logger.debug("Ejecutando SQL obtenerPlanesCotizacion ..." + idProducto
				+ idPlanPromocion);
		if (idPlanPromocion != null) {

			Object[] params = new Object[] { idProducto, idPlanPromocion };
			String sql = SQL_OBTENER_PLANES_COTIZACION;
			sql += "AND P3.ID_PLAN = ?";
			resultado = (ArrayList<Plan>) this.query(Plan.class, sql, params);
		} else {

			Object[] params = new Object[] { idProducto };
			String sql = SQL_OBTENER_PLANES_COTIZACION;
			// sql +=
			// "AND P3.ID_PLAN not in (select ID_PLAN from FICHA where ID_PLAN is not null and ES_SUBCATEGORIA = 0 and ESTADO_FICHA = 1)";

			resultado = (ArrayList<Plan>) this.query(Plan.class, sql, params);

		}
		logger.debug("Resultado SQL obtenerPlanesCotizacion ..."
				+ resultado.toArray(new Plan[resultado.size()]));
		return resultado.toArray(new Plan[resultado.size()]);
	}

	/**
	 * @see CotizacionDAO#obtenerPlanPromocion(String, String)
	 */
	public Plan[] obtenerPlanPromocion(String idProducto, String idSubcategoria) {
		// Implementar SQL.
		return null;
	}

	/**
	 * @see CotizacionDAO#obtenerProductos(String, String, String)
	 */
	public Producto[] obtenerProductos(String idSubcategoria, String idRama,
			String idPlan) {

		List<Producto> result = null;

		if (idPlan != null && idPlan.length() > 0) {
			Object[] params = new Object[] { idSubcategoria, idRama, idPlan };

			String sql = SQL_OBTENER_PRODUCTOS;
			sql += "AND PRODUCTO_LEG.CODIGO_PRODUCTO_LEG = (select CODIGO_PRODUCTO_LEG from PLAN_LEG where ID_PLAN = ?) order by PRODUCTO.FECHA_ACTUALIZACION";

			result = (ArrayList<Producto>) this.query(Producto.class, sql,
					params);

		} else {
			Object[] params = new Object[] { idSubcategoria, idRama };
			String sql = SQL_OBTENER_PRODUCTOS;
			sql += " order by PRODUCTO.FECHA_ACTUALIZACION";
			result = (ArrayList<Producto>) this.query(Producto.class, sql,
					params);
		}

		return result.toArray(new Producto[result.size()]);
	}

	/**
	 * @see CotizacionDAO#obtenerCoberturas(String)
	 */
	public Cobertura[] obtenerCoberturas(String idPlan) {
		Object[] params = new Object[] { idPlan };
		List<Cobertura> result = (ArrayList<Cobertura>) this.query(
				Cobertura.class, SQL_OBTENER_COBERTURAS_PLAN, params);
		return result.toArray(new Cobertura[result.size()]);
	}

	/**
	 * @see CotizacionDAO#obtenerActividades()
	 */
	public Actividad[] obtenerActividades() {
		Object[] params = new Object[] {};
		List<Actividad> result = (ArrayList<Actividad>) this.query(
				Actividad.class, SQL_OBTENER_ACTIVIDADES, params);
		return result.toArray(new Actividad[result.size()]);
	}

	/**
	 * @see CotizacionDAO#obtenerEstadosCiviles()
	 */
	public EstadoCivil[] obtenerEstadosCiviles() {
		Object[] params = new Object[] {};
		List<EstadoCivil> result = (ArrayList<EstadoCivil>) this.query(
				EstadoCivil.class, SQL_OBTENER_ESTADO_CIVIL, params);
		return result.toArray(new EstadoCivil[result.size()]);
	}

	/**
	 * @see CotizacionDAO#obtenerComunas(String)
	 */
	public ParametroCotizacion[] obtenerComunas(String idRegion) {
		Object[] params = new Object[] { idRegion };
		List<ParametroCotizacion> result = (ArrayList<ParametroCotizacion>) this
				.query(ParametroCotizacion.class, SQL_OBTENER_COMUNAS, params);
		return result.toArray(new ParametroCotizacion[result.size()]);
	}

	/**
	 * @see CotizacionDAO#obtenerRegiones()
	 */
	public ParametroCotizacion[] obtenerRegiones() {
		Object[] params = new Object[] {};
		List<ParametroCotizacion> result = (ArrayList<ParametroCotizacion>) this
				.query(ParametroCotizacion.class, SQL_OBTENER_REGIONES, params);
		return result.toArray(new ParametroCotizacion[result.size()]);
	}

	/**
	 * @see CotizacionDAO#obtenerCiudades(String)
	 */
	public ParametroCotizacion[] obtenerCiudades(String idComuna) {
		Object[] params = new Object[] { idComuna };
		List<ParametroCotizacion> result = (ArrayList<ParametroCotizacion>) this
				.query(ParametroCotizacion.class, SQL_OBTENER_CIUDADES, params);
		return result.toArray(new ParametroCotizacion[result.size()]);
	}

	/**
	 * @see CotizacionDAO#obtenerParametro(String)
	 */
	public Map<String, ?> obtenerParametro(String idGrupo, String idParametro) {
		Object[] params = new Object[] { idGrupo, idParametro };
		String sql = SQL_OBTENER_PARAMETRO;
		sql += " and NOMBRE = ? order by ORDEN_LISTADO";

		Map<String, ?> result = (Map<String, ?>) this.find(Map.class, sql,
				params);

		return result;
	}

	/**
	 * @see CotizacionDAO#obtenerParametro(String, String)
	 */
	public List<Map<String, ?>> obtenerParametro(String idGrupo) {
		Object[] params = new Object[] { idGrupo };
		String sql = SQL_OBTENER_PARAMETRO;
		sql += " order by ORDEN_LISTADO";
		List<Map<String, ?>> result = (ArrayList<Map<String, ?>>) this.query(
				Map.class, sql, params);
		return result;
	}

	public Plan[] obtenerPlanCotizacion(String idProducto, String idPlan) {

		Object[] params = new Object[] { idProducto, idPlan };
		String sql = SQL_OBTENER_PLANES_COTIZACION;
		sql += " AND P3.ID_PLAN = ?";
		List<Plan> result = (ArrayList<Plan>) this.query(Plan.class, sql,
				params);

		Plan[] plan = result.toArray(new Plan[result.size()]);

		return plan;

	}

	public Vehiculo obtenerVehiculoLegacy(String idTipo, String idMarca,
			String idModelo) {
		Object[] params = new Object[] { idTipo, idMarca, idModelo };
		String sql = SQL_OBTENER_VEHICULO_LEG;
		List<Vehiculo> result = (ArrayList<Vehiculo>) this.query(
				Vehiculo.class, sql, params);
		if (result == null || result.size() == 0) {
			return null;
		}
		return result.toArray(new Vehiculo[result.size()])[0];
	}

	public long agregarDatosSolicitud(Plan plan, int clienteEsAsegurado,
			String idRama, String idProducto, String estadoSolicitud,
			String nroTarjeta, long rut, String idVitrineo) {
		Object[] params = new Object[] { plan.getIdPlan(), clienteEsAsegurado,
				idRama, idProducto, estadoSolicitud, nroTarjeta, rut,
				plan.getPrimaAnualUF(), plan.getPrimaAnualPesos(),
				plan.getPrimaMensualUF(), plan.getPrimaMensualPesos(),
				idVitrineo };
		String sql = SQL_AGREGAR_SOLICITUD;
		long res = this.insert(sql, params);
		return res;
	}

	public long agregarDatosAsegurado(long idSolicitud, Contratante asegurado) {

		Object[] params = new Object[] {
				idSolicitud,
				asegurado.getIdCiudad() == null ? null : asegurado
						.getIdCiudad(), asegurado.getRut(),
				String.valueOf(asegurado.getDv()), asegurado.getNombre(),
				asegurado.getApellidoPaterno(), asegurado.getApellidoMaterno(),
				asegurado.getFechaDeNacimiento(), asegurado.getSexo(),
				asegurado.getTelefono2(), asegurado.getTelefono1(),
				asegurado.getEmail(),
				asegurado.getNumeroDepartamentoDireccion(),
				asegurado.getNumeroDireccion(), asegurado.getCalleDireccion(),
				asegurado.getTelefono1(), asegurado.getTelefono2(),
				asegurado.getIdRegion(), asegurado.getIdComuna(),
				asegurado.getEstadoCivil() };

		long res = this.insert(SQL_AGREGAR_ASEGURADO, params);
		return res;
	}

	public long agregarDatosContratante(long idSolicitud,
			CotizacionSeguro cotizacion) {

		Contratante contratante = cotizacion.getContratante();

		Object[] params = new Object[] {
				idSolicitud,
				contratante.getIdCiudad() == null ? null : contratante
						.getIdCiudad(), contratante.getRut(),
				String.valueOf(contratante.getDv()), contratante.getNombre(),
				contratante.getApellidoPaterno(),
				contratante.getApellidoMaterno(),
				contratante.getFechaDeNacimiento(), contratante.getSexo(),
				contratante.getTelefono2(), contratante.getTelefono1(),
				contratante.getEmail(),
				contratante.getNumeroDepartamentoDireccion(),
				contratante.getNumeroDireccion(), cotizacion.getDireccion(),
				contratante.getTipoTelefono1(), contratante.getTipoTelefono2(),
				contratante.getIdRegion(), contratante.getIdComuna(),
				contratante.getEstadoCivil() };

		long res = this.insert(SQL_AGREGAR_CONTRATANTE, params);
		return res;
	}

	public long agregarDatosMateriaHogar(CotizacionSeguroHogar datos,
			long idSolicitud) {
		Object[] params = new Object[] { idSolicitud, datos.getCiudad(),
				datos.getDireccion(), datos.getNumeroDireccion(),
				datos.getNumeroDepto(), datos.getMontoAseguradoEdificio(),
				datos.getMontoAseguradoContenido(),
				datos.getMontoAseguradoRobo() };

		long res = this.insert(SQL_AGREGAR_MAT_HOGAR, params);
		return res;
	}

	public long agregarDatosMateriaVehiculo(CotizacionSeguroVehiculo datos,
			long idSolicitud) {
		Object[] params = new Object[] { idSolicitud, datos.getCodigoModelo(),
				datos.getCodigoMarca(), datos.getCodigoTipoVehiculo(),
				datos.getAnyoVehiculo(), datos.getMontoAsegurado(),
				datos.getNumeroPuertas(), datos.getPatente(),
				datos.getNumeroMotor(), datos.getNumeroChasis(),
				datos.getIdColor() == 0 ? null : datos.getIdColor(),
				datos.getEsParticular(), datos.getEdadConductor() };
		logger.info("Monto Asegurado:" + datos.getMontoAsegurado());
		long res = this.insert(SQL_AGREGAR_MAT_VEHICULO, params);
		return res;
	}

	public long agregarDatosMateriaVida(CotizacionSeguroVida cotizacion,
			long idSolicitud) {
		Object[] params = new Object[] { idSolicitud,
				cotizacion.getActividad(), cotizacion.getRutCliente() };

		long res = this.insert(SQL_AGREGAR_MAT_VIDA, params);
		return res;
	}

	public long agregarDatosVehiculoFactura() {
		// TODO Auto-generated method stub
		return 0;
	}

	public long agregarDatosVidaAdicional(long idMateria, Beneficiario adicional) {

		String dv = "";
		if (adicional.getDv() != '\u0000') {
			dv = String.valueOf(adicional.getDv());
		} else {
			dv = "0";
		}
		Object[] params = new Object[] { idMateria, adicional.getRut(), dv,
				adicional.getNombre(), adicional.getApellidoPaterno(),
				adicional.getApellidoMaterno(), adicional.getFechaNacimiento(),
				adicional.getParentesco(), adicional.getSexo() };

		long res = this.insert(SQL_AGREGAR_ASEGURADO_ADICIONAL, params);
		return res;
	}

	public long agregarDatosVidaBeneficiario(long idMateria,
			Beneficiario beneficiario) {
		
		Object[] params = new Object[] { 
				idMateria, 
				beneficiario.getNombre(),
				beneficiario.getApellidoPaterno(),
				beneficiario.getApellidoMaterno(),
				beneficiario.getFechaNacimiento(),
				beneficiario.getParentesco(), 
				beneficiario.getSexo(),
				beneficiario.getDistribucion() };

		long res = this.insert(SQL_AGREGAR_BENEFICIARIO, params);
		logger.info("Registro de Beneficiario en BD");
		return res;
	}

	public void actualizarAsegurado(long idSolicitud, Contratante datos) {
		Object[] params = new Object[] { datos.getTipoTelefono1(),
				datos.getTelefono1(), datos.getTipoTelefono2(),
				datos.getTelefono2(), datos.getIdRegion(), datos.getIdComuna(),
				datos.getIdCiudad(), datos.getCalleDireccion(),
				datos.getNumeroDireccion(),
				datos.getNumeroDepartamentoDireccion(),
				datos.getFechaDeNacimiento(), idSolicitud };
		this.update(SQL_ACTUALIZAR_ASEGURADO, params);
	}

	public void actualizarContratante(long idSolicitud, CotizacionSeguro datos) {
		Contratante contratante = datos.getContratante();
		Object[] params = new Object[] { contratante.getTipoTelefono1(),
				contratante.getTelefono1(), contratante.getTipoTelefono2(),
				contratante.getTelefono2(), contratante.getNombre(),
				contratante.getApellidoPaterno(),
				contratante.getApellidoMaterno(), contratante.getIdRegion(),
				contratante.getEmail(), contratante.getIdComuna(),
				contratante.getIdCiudad(), contratante.getCalleDireccion(),
				contratante.getNumeroDireccion(),
				contratante.getNumeroDepartamentoDireccion(), idSolicitud };
		this.update(SQL_ACTUALIZAR_CONTRATANTE, params);
	}

	public void actualizarFactura(long idSolicitud, CotizacionSeguro datos) {
		// TODO Auto-generated method stub

	}

	public void actualizarMateriaVehiculo(long idSolicitud,
			CotizacionSeguroVehiculo datos) {
		Object[] params = new Object[] { datos.getPatente(),
				datos.getNumeroMotor(), datos.getNumeroChasis(),
				datos.getIdColor(), datos.getTipoInspeccion(),
				datos.getInspeccionBSP(), idSolicitud };
		this.update(SQL_ACTUALIZAR_MAT_VEH, params);

	}

	public void actualizarEstadoSolicitud(long idSolicitud, String idEstado) {
		Object[] params = new Object[] { idEstado, idSolicitud };
		this.update(SQL_ACTUALIZAR_ESTADO_SOLICITUD, params);
	}

	public void actualizarMateriaHogar(long idSolicitud,
			CotizacionSeguroHogar datos) {
		Object[] params = new Object[] { datos.getCiudad(),
				datos.getDireccion(), datos.getNumeroDireccion(),
				datos.getNumeroDepto(), datos.getMontoAseguradoEdificio(),
				datos.getMontoAseguradoContenido(),
				datos.getMontoAseguradoRobo(), idSolicitud };
		this.update(SQL_ACTUALIZAR_MAT_HOGAR, params);
	}

	public void actualizarMateriaVida(long idSolicitud,
			CotizacionSeguroVida datos) {
		Object[] params = new Object[] { datos.getActividad(),
				datos.getRutCliente(), idSolicitud };
		this.update(SQL_ACTUALIZAR_MAT_VID, params);
	}

	public ColorVehiculo[] obtenerColoresVehiculo() {
		List<ColorVehiculo> result = this.query(ColorVehiculo.class,
				SQL_OBTENER_COLOR, new Object[] {});
		ColorVehiculo[] colores = (ColorVehiculo[]) result
				.toArray(new ColorVehiculo[result.size()]);
		return colores;
	}

	public ParentescoAsegurado[] obtenerParentescoRestriccion(long idRestriccion) {
		Object[] params = new Object[] { idRestriccion };
		List<ParentescoAsegurado> result = this.query(
				ParentescoAsegurado.class, SQL_OBTENER_PARENTESCO_REST, params);

		ParentescoAsegurado[] parentesco = (ParentescoAsegurado[]) result
				.toArray(new ParentescoAsegurado[result.size()]);
		return parentesco;
	}

	public RestriccionVida obtenerRestriccionVida(long idPlan) {
		Object[] params = new Object[] { idPlan };
		List<RestriccionVida> result = this.query(RestriccionVida.class,
				SQL_OBTENER_RESTRICCION_VIDA, params);

		RestriccionVida parentesco;
		if (result.size() > 0) {
			parentesco = (RestriccionVida) result.get(0);
		} else {
			parentesco = new RestriccionVida();
		}

		return parentesco;
	}

	public Solicitud obtenerDatosSolicitud(long idSolicitud) {
		Object[] params = new Object[] { idSolicitud };
		List<Solicitud> result = this.query(Solicitud.class,
				SQL_OBTENER_SOLICITUD, params);
		CotizacionSeguro cotizacion = new CotizacionSeguro();
		return result.get(0);
	}

	/**
	 * TODO Describir m�todo obtenerRamaPorId.
	 * 
	 * @param idRama
	 * @return
	 */
	public Rama obtenerRamaPorId(long idRama) {
		Rama rama = new Rama();
		Object[] params = new Object[] { idRama };
		List<Rama> result = this.query(Rama.class, SQL_OBTENER_RAMA_POR_ID,
				params);

		if (result.size() > 0) {
			rama = result.get(0);
		}

		return rama;
	}

	public CotizacionSeguroHogar obtenerDatosMateriaHogar(long idSolicitud) {
		Object[] params = new Object[] { idSolicitud };
		CotizacionSeguroHogar result = (CotizacionSeguroHogar) this.find(
				CotizacionSeguroHogar.class, SQL_OBTENER_MAT_HOGAR, params);
		return result;
	}

	public CotizacionSeguroVehiculo obtenerDatosMateriaVehiculo(long idSolicitud) {
		Object[] params = new Object[] { idSolicitud };
		List<CotizacionSeguroVehiculo> result = this.query(
				CotizacionSeguroVehiculo.class, SQL_OBTENER_MAT_VEHICULO,
				params);
		return result.get(0);
	}

	public CotizacionSeguroVida obtenerDatosMateriaVida(long idSolicitud) {
		Object[] params = new Object[] { idSolicitud };
		CotizacionSeguroVida result = (CotizacionSeguroVida) this.find(
				CotizacionSeguroVida.class, SQL_OBTENER_MAT_VIDA, params);
		return result;
	}

	public long ingresarFacturaVehiculo(long idMateriaVehiculo, byte[] factura) {
		Object[] params = new Object[] { idMateriaVehiculo, factura };
		return this.insert(SQL_INGRESAR_FACTURA, params);
	}

	/**
	 * TODO Describir m�todo obtenerDatosPlan.
	 * 
	 * @param idPlan
	 * @return
	 */
	public Plan obtenerDatosPlan(long idPlan) {
		Object[] params = new Object[] { idPlan };
		return (Plan) this.find(Plan.class, SQL_OBTENER_DATOS_PLAN, params);
	}

	public void actualizarAsegurado(long idSolicitud, CotizacionSeguro datos) {
		Contratante asegurado = datos.getAsegurado();
		Object[] params = new Object[] { asegurado.getTipoTelefono1(),
				asegurado.getTelefono1(), asegurado.getTipoTelefono2(),
				asegurado.getTelefono2(), asegurado.getIdRegion(),
				asegurado.getIdComuna(), asegurado.getIdCiudad(),
				asegurado.getCalleDireccion(), asegurado.getNumeroDireccion(),
				asegurado.getNumeroDepartamentoDireccion(),
				asegurado.getFechaDeNacimiento(), idSolicitud };
		this.update(SQL_ACTUALIZAR_ASEGURADO, params);
	}

	public boolean existePrimaUnica(long idPlan) {
		boolean existe = false;
		Object[] params = new Object[] { idPlan };
		Map map = (Map) this.find(Map.class, SQL_EXISTE_PRIMA_UNICA, params);

		if (map.containsKey("existeprima")) {
			Object opt = (Object) map.get("existeprima");
			Integer count = new Integer(opt.toString());
			if (count != null && count > 0) {
				existe = true;
			}
		}

		return existe;
	}

	/**
	 * @see CotizacionDAO#obtenerRegionPorId
	 */
	public Map<String, Object> obtenerRegionPorId(String idRegion) {
		Object[] params = new Object[] { idRegion };
		return (Map<String, Object>) this.find(Map.class,
				OBTENER_REGION_POR_ID, params);
	}

	/**
	 * @see CotizacionDAO#obtenerComunaPorId
	 */
	public Map<String, Object> obtenerComunaPorId(String idComuna) {
		Object[] params = new Object[] { idComuna };
		return (Map<String, Object>) this.find(Map.class,
				OBTENER_COMUNA_POR_ID, params);
	}

	/**
	 * @see CotizacionDAO#obtenerCiudadPorId
	 */
	public Map<String, Object> obtenerCiudadPorId(String idCiudad) {
		Object[] params = new Object[] { idCiudad };
		return (Map<String, Object>) this.find(Map.class,
				OBTENER_CIUDAD_POR_ID, params);
	}

	public long obtenerSubcategoriaAsociada(String idProducto, String idRama) {
		Object[] params = new Object[] { idProducto, idRama };
		Subcategoria result = (Subcategoria) this.find(Subcategoria.class,
				SQL_OBTENER_ID_SUBCATEGORIA, params);
		return Long.parseLong(result.getId());
	}

	public Subcategoria obtenerSubcategoriaPorId(long idSubcategoria) {
		Object[] params = new Object[] { idSubcategoria };
		Subcategoria result = (Subcategoria) this.find(Subcategoria.class,
				SQL_OBTENER_SUBCAT_POR_ID, params);
		return result;
	}

	/**
	 * @see CotizacionDAO#obtenerMarcasVehiculos(String)
	 */
	public Vehiculo[] obtenerMarcasPorTipoVehiculos(String idTipo) {
		Object[] params = new Object[] { idTipo };
		logger.debug("Ejecutando SQL obtenerMarcasPorTipoVehiculos ...");
		List<Vehiculo> resultado = (ArrayList<Vehiculo>) this.query(
				Vehiculo.class, SQL_OBTENER_MARCA_POR_TIPO_VEHICULO, params);
		logger.debug("obtenerMarcasPorTipoVehiculos Resultado ...");
		return resultado.toArray(new Vehiculo[resultado.size()]);
	}

	/**
	 * @see CotizacionDAO#obtenerRutSinCaptcha(long)
	 */
	public int obtenerRutSinCaptcha(long rutsincap) {

		Object[] params = new Object[] { rutsincap };
		int existe = this.queryCaptcha(SQL_OBTENER_CLIENTE_SIN_CAPTCHA, params);
		logger.info("valor de existe: " + existe);
		return existe;
	}

	public long grabarVitrineoGeneral(String pswd, String rutVitrineo,
			String dvVitrineo, String nombre, String apellidoPaterno,
			String apellidoMaterno, String tipoTelefono, String codTelefono,
			String telefono, String email, int region, int comuna,
			String diaNac, String mesNac, String anyoNac, String estadoCivil,
			String sexo, long edadConductor, int tipoVehiculo,
			int idMarcaVitrineo, int idModeloVitrineo, String idAnyoVitrineo,
			int numPuertas, String esDuenyo, String ciudadHogar,
			String direccionHogar, String numeroDireccionHogar,
			String numeroDeptoHogar, String anyoHogar, int rama,
			int subcategoria, int productoVitrineo, String rutDuenyo,
			String dvDuenyo, String nombreDuenyo, String apellidoPaternoDuenyo,
			String apellidoMaternoDuenyo, String diaNacimientoDuenyo,
			String mesNacimientoDuenyo, String anyoNacimientoDuenyo,
			String estadoCivilDuenyo, String regionResidenciaDuenyo,
			String comunaResidenciaDuenyo, String valorComercial) {
		Object[] params = new Object[] { pswd, rutVitrineo, dvVitrineo, nombre,
				apellidoPaterno, apellidoMaterno, tipoTelefono, codTelefono,
				telefono, email, region, comuna, diaNac, mesNac, anyoNac,
				estadoCivil, sexo, edadConductor, tipoVehiculo,
				idMarcaVitrineo, idModeloVitrineo, idAnyoVitrineo, numPuertas,
				esDuenyo, ciudadHogar, direccionHogar, numeroDireccionHogar,
				numeroDeptoHogar, anyoHogar, rama, subcategoria,
				productoVitrineo, rutDuenyo, dvDuenyo, nombreDuenyo,
				apellidoPaternoDuenyo, apellidoMaternoDuenyo,
				diaNacimientoDuenyo, mesNacimientoDuenyo, anyoNacimientoDuenyo,
				estadoCivilDuenyo, regionResidenciaDuenyo,
				comunaResidenciaDuenyo, valorComercial };
		logger.info("valor de parametros:" + params.length);
		return this.insertrut(SQL_GUARDAR_VITRINEO_GENERAL, params);
	}

	public List<HashMap<String, String>> obtenerVehiculos(String rut) {
		Object[] params = new Object[] { Integer.parseInt(rut) };
		logger.info("Rut in JDBC:" + params);
		return (List<HashMap<String, String>>) this
				.query(this
						.getConnection("cl.cencosud.asesorcotizador.dao.jdbc.CotizacionDAOJDBC.dataSource"),
						Map.class, SQL_OBTENER_VEHICULOS_SOAP, params);
	}

	public Map<String, Object> obtenerVehiculosCompleto(String rut,
			String modelo) {
		Object[] params = new Object[] { Integer.parseInt(rut), modelo };
		return (Map<String, Object>) this
				.find(this
						.getConnection("cl.cencosud.asesorcotizador.dao.jdbc.CotizacionDAOJDBC.dataSource"),
						Map.class, SQL_OBTENER_VEHICULOS_COMPLETO, params);
	}

	public Map<String, Object> obtenerDatosVehiculosCompleto(String tipo,
			String marca, String modelo) {
		Object[] params = new Object[] { tipo, marca, modelo };
		return (Map<String, Object>) this.find(Map.class,
				SQL_OBTENER_DATOS_VEHICULOS_COMPLETO, params);
	}

	public Map<String, Object> obtenerDatosVitrineo(String claveVitrineo) {
		Object[] params = new Object[] { claveVitrineo };
		return (Map<String, Object>) this.find(Map.class,
				SQL_OBTENER_DATOS_CLIENTE_VITRINEO, params);
	}

	/**
	 * @see CotizacionDAO#obtenerPlanesDeducibles(String)
	 */
	public Plan[] obtenerPlanesDeducibles(String idProducto2,
			String idProducto3, String idProducto4, String idProducto5,
			String idProducto6, String idProducto7, String idPlanPromocion) {

		List<Plan> resultado = null;

		if (idPlanPromocion != null) {

			Object[] params = new Object[] { idProducto2, idProducto3,
					idProducto4, idProducto5, idProducto6, idProducto7,
					idPlanPromocion };
			String sql = SQL_OBTENER_PLANES_COTIZACION_DEDUCIBLES;
			sql += "AND P3.ID_PLAN = ? order by p3.ID_PLAN";
			resultado = (ArrayList<Plan>) this.query(Plan.class, sql, params);
		} else {
			logger.info("Entrando a la consulta");
			Object[] params = new Object[] { idProducto2, idProducto3,
					idProducto4, idProducto5, idProducto6, idProducto7 };
			logger.info("parametros:" + params);
			String sql = SQL_OBTENER_PLANES_COTIZACION_DEDUCIBLES;
			sql += "order by p3.ID_PLAN";
			logger.info("SQL:" + sql);
			resultado = (ArrayList<Plan>) this.query(Plan.class, sql, params);
			logger.info("Saliendo de la consulta");
		}

		logger.debug("Terminando consulta de obtenerPlanesCotizacion en la BD");
		return resultado.toArray(new Plan[resultado.size()]);
	}

	public Plan[] obtenerPlanCotizacionDeducibles(String idProducto2,
			String idProducto3, String idProducto4, String idProducto5,
			String idProducto6, String idProducto7, String idPlan) {

		Object[] params = new Object[] { idProducto2, idProducto3, idProducto4,
				idProducto5, idProducto6, idProducto7, idPlan };
		String sql = SQL_OBTENER_PLANES_COTIZACION_DEDUCIBLES2;
		List<Plan> result = (ArrayList<Plan>) this.query(Plan.class, sql,
				params);
		Plan[] plan = result.toArray(new Plan[result.size()]);
		logger.debug("Terminando consulta de obtenerPlanCotizacion en la BD");
		return plan;
	}

	public void guardarMascota(String rutDuenoMascota, String nombreMascota,
			String tipoMascota, String razaMascota, String colorMascota,
			String edadMascota, String sexoMascota, String direccionMascota) {

		Object[] params = new Object[] { rutDuenoMascota, nombreMascota,
				tipoMascota, razaMascota, colorMascota, edadMascota, sexoMascota, direccionMascota };

		this.insertWithoutGeneratedKey(SQL_GUARDAR_MASCOTA, params);
		logger.info("Mascota registrada en la BD");
	}
	
	public String obtenerIdFicha(int idSubcategoria) {
		Object[] params = new Object[] { idSubcategoria };
		Map<String, Object> consulta = (Map<String, Object>)
				this.find(Map.class, SQL_OBTENER_ID_FICHA, params);
		String idFicha = null;
		
		Iterator it = consulta.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry e = (Map.Entry)it.next();
			idFicha = e.getValue().toString();	
		}
		logger.info("idFicha BD "+idFicha);
		return idFicha;
	}
	
	//Pleyasoft - Patente Vehiculo
	/**
	 * @see CotizacionDAO#obtenerDatosPatente(String)
	 */
	public PatenteVehiculo obtenerDatosPatente(String idPatente) {
		Object[] params = new Object[] { idPatente };
		PatenteVehiculo resultado = (PatenteVehiculo) this.find(
				PatenteVehiculo.class, SQL_OBTENER_PATENTE_VEHICULO, params);
		PatenteVehiculo asegurado = null;
		if (resultado != null) {
			logger.debug("Datos patente obtenidos...");
			asegurado = resultado;
		}
		return asegurado;
	}
	
	/**
	 * @see CotizacionDAO#agregarDatosPatente(PatenteVehiculo)
	 */
	public long agregarDatosPatente(PatenteVehiculo p) {
		Object[] params = new Object[] { p.getPatente(), 
										 p.getTipoVehiculo(), 
										 p.getMarca(), 
										 p.getModelo(), 
										 p.getAnio(),
										 p.getFechaRegistro()
										};
		this.insertWithoutGeneratedKey(SQL_AGREGAR_PATENTE_VEHICULO, params);
		return 1;
	}
	
	/**
	 * @see CotizacionDAO#actualizarDatosPatente(PatenteVehiculo)
	 */
	public long actualizarDatosPatente(PatenteVehiculo p) {
		Object[] params = new Object[] { p.getTipoVehiculo(), 
										 p.getMarca(), 
										 p.getModelo(), 
										 p.getAnio(),
										 p.getFechaModificacion(),
										 p.getPatente()
										};
		return this.update(SQL_ACTUALIZAR_PATENTE_VEHICULO, params);
	}
	//Pleyasoft - Patente Vehiculo
}
