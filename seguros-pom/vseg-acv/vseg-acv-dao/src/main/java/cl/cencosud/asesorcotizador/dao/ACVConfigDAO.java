package cl.cencosud.asesorcotizador.dao;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.tinet.common.config.AbstractConfigurator;
import cl.tinet.common.dao.jdbc.BaseDAO;

/**
 * Clase ACVConfigDAO, permite obtener parametros desde la base de datos
 * <br/>
 * 
 * @author fmendoza
 * @version 1.0
 * @created Aug 20, 2010
 */
public interface ACVConfigDAO extends BaseDAO {
    
    /**
     * Atributo de log de la aplicación.
     */
    Log logger = LogFactory.getLog(AbstractConfigurator.class);

    public String getString(final String llave);
    
    public String getString(String grupo, String descripcion);
    
}
