package cl.cencosud.asesorcotizador.dao;

import cl.cencosud.acv.common.Solicitud;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida;
import cl.tinet.common.dao.jdbc.BaseDAO;

public interface RegistrarCotizacionDAO extends BaseDAO {
    

    public int registrarCotizacionVehiculo(CotizacionSeguroVehiculo vehiculo, Solicitud solicitud);
    
    public int registrarCotizacionVida(CotizacionSeguroVida cotVida, Solicitud solicitud);
    
    public int registrarCotizacionHogar(CotizacionSeguroHogar cotHogar, Solicitud solicitud);
}
