// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:18
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.tinet.common.captcha.servlet.util;

import cl.tinet.common.captcha.servlet.util.CaptchaServiceSingleton;
import com.octo.captcha.service.CaptchaServiceException;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ImageCaptchaServlet extends HttpServlet {

   private static final long serialVersionUID = 1L;


   public void init(ServletConfig servletConfig) throws ServletException {
      super.init(servletConfig);
   }

   protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
      try {
         String e = httpServletRequest.getSession().getId();
         BufferedImage challenge = CaptchaServiceSingleton.getInstance().getImageChallengeForID(e, httpServletRequest.getLocale());
         ServletOutputStream responseOutputStream = httpServletResponse.getOutputStream();
         httpServletResponse.setHeader("Cache-Control", "no-store");
         httpServletResponse.setHeader("Pragma", "no-cache");
         httpServletResponse.setDateHeader("Expires", 0L);
         httpServletResponse.setContentType("image/png");
         ImageIO.write(challenge, "png", responseOutputStream);
         responseOutputStream.flush();
         responseOutputStream.close();
      } catch (IllegalArgumentException var6) {
         httpServletResponse.sendError(404);
      } catch (CaptchaServiceException var7) {
         httpServletResponse.sendError(500);
      }
   }
}
