// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:18
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.ventaseguros.comparador.struts.action;

import cl.cencosud.acv.common.Plan;
import cl.cencosud.acv.common.Producto;
import cl.cencosud.acv.common.Subcategoria;
import cl.cencosud.acv.common.Vehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.cencosud.ventaseguros.comparador.model.Caracteristica;
import cl.tinet.common.seguridad.model.UsuarioExterno;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class EnviarComparadorAction extends Action {

   private static final int ID_BENEFICIO = 1;
   private static final int ID_COB_PART = 2;
   private static final int ID_COB_COMP = 3;
   private static final int ID_EXCLUSION = 4;
   private static final String LOGIN_USUARIO = "cl.tinet.common.seguridad.USUARIO_CONECTADO";
   private static final Log logger = LogFactory.getLog(EnviarComparadorAction.class);


   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
      LinkedHashMap mapResultComparador = (LinkedHashMap)request.getSession().getAttribute("mapResultComparador");
      CotizacionSeguro oCotizacionSeguro = (CotizacionSeguro)request.getSession().getAttribute("datosCotizacion");
      CotizacionSeguroVehiculo oCotizacionSeguroVehiculo = (CotizacionSeguroVehiculo)request.getSession().getAttribute("datosCotizacion");
      if(mapResultComparador != null && mapResultComparador.size() > 0) {
         String ruta = request.getRequestURL().toString();
         ruta = ruta.substring(0, ruta.indexOf(request.getRequestURI()));
         String idProducto = String.valueOf(oCotizacionSeguro.getCodigoProducto());
         String descSubcategoria = "";
         String descProducto = "";
         String idVitrineo = (String)request.getSession().getAttribute("claveVitrineo");
         CotizacionDelegate oDelegate = new CotizacionDelegate();
         String idSubcategoria = (String)request.getSession().getAttribute("idSubcategoria");
         String idRama = (String)request.getSession().getAttribute("idRama");
         Subcategoria subcategoria = oDelegate.obtenerSubcategoriaPorId(Long.parseLong(idSubcategoria));
         if(subcategoria != null) {
            descSubcategoria = subcategoria.getTituloSubcategoria();
         }

         Producto[] productos = oDelegate.obtenerProductos(idSubcategoria, idRama, (String)null);
         if(productos != null && productos.length > 0) {
            Producto[] var21 = productos;
            int var20 = productos.length;

            for(int var19 = 0; var19 < var20; ++var19) {
               Producto html = var21[var19];
               if(html.getIdProducto().trim().equals(idProducto.trim())) {
                  descProducto = html.getNombreProducto();
                  break;
               }
            }
         }

         StringBuffer var22 = this.getHTML(ruta, descSubcategoria, descProducto, Integer.valueOf(idRama), Integer.valueOf(idSubcategoria), oCotizacionSeguro, mapResultComparador, request, oDelegate, idVitrineo);
         logger.debug("TEXTO HTML CORREO : " + var22.toString());
         oDelegate.enviarComparador(oCotizacionSeguro.getEmail(), var22.toString());
      }

      return null;
   }

   private StringBuffer getHTML(String pathBase, String descSubcategoria, String descProducto, Integer idRama, Integer idSubcategoria, CotizacionSeguro oCotizacionSeguro, LinkedHashMap mapResultComparador, HttpServletRequest request, CotizacionDelegate oDelegate, String idVitrineo) {
      List listPlanes = (List)mapResultComparador.get("listPlanes");
      LinkedHashMap mapCaracteristicas = (LinkedHashMap)mapResultComparador.get("listCaracteristicas");
      LinkedHashMap mapBeneficio = null;
      LinkedHashMap mapCoberturaPart = null;
      LinkedHashMap mapCoberturaComp = null;
      LinkedHashMap mapExclusion = null;
      if(mapCaracteristicas.containsKey(Integer.valueOf(1))) {
         mapBeneficio = (LinkedHashMap)mapCaracteristicas.get(Integer.valueOf(1));
      }

      if(mapCaracteristicas.containsKey(Integer.valueOf(2))) {
         mapCoberturaPart = (LinkedHashMap)mapCaracteristicas.get(Integer.valueOf(2));
      }

      if(mapCaracteristicas.containsKey(Integer.valueOf(3))) {
         mapCoberturaComp = (LinkedHashMap)mapCaracteristicas.get(Integer.valueOf(3));
      }

      if(mapCaracteristicas.containsKey(Integer.valueOf(4))) {
         mapExclusion = (LinkedHashMap)mapCaracteristicas.get(Integer.valueOf(4));
      }

      LinkedHashMap mapCompannia = this.getMapCompannias();
      StringBuffer html = new StringBuffer();
      html.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
      html.append("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
      html.append("<head>");
      html.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\" />");
      html.append("<title>Comparador de Planes</title>");
      String imagen = "";
      if(listPlanes.size() == 2) {
         imagen = pathBase + "/cotizador/images/comparador/barra-seguros-2opciones.gif";
      } else if(listPlanes.size() == 3) {
         imagen = pathBase + "/cotizador/images/comparador/barra-seguros-3opciones.gif";
      } else {
         imagen = pathBase + "/cotizador/images/comparador/barra-seguros.gif";
      }

      html.append("</head>");
      html.append("<body style=\"margin:0;padding:0;background-color:#FFF;border:none;font-family:Arial, Helvetica, sans-serif;\">");
      html.append("<table width=\"600\" height=\"auto\" align=\"center\">");
      html.append("<tr>");
      html.append("<td >");
      html.append("<a href=\"" + pathBase + "/vseg-paris/index.jsp\">");
      html.append("<img src=\"" + pathBase + "/cotizador/images/comparador/header.jpg\" width=\"685\" align=\"center\" border=\"0\" style=\"margin-left: 24px; margin-bottom: 0px; margin-top: 15px;\" />");
      html.append("</a>");
      html.append("</td>");
      html.append("</tr>");
      html.append("</table>");
      html.append("<table width=\"660\" height=\"auto\" align=\"center\">");
      html.append("<tr>");
      html.append("<td valign=\"top\">");
      html.append("<table cellpadding=\"0\" cellspacing=\"0\" width=\"660\">");
      html.append("<tr>");
      html.append("<td width=\"296\"><h1 style=\"margin-bottom:-20px; margin-left:0px; font-size:22px; font-weight:normal;color: #4F4C4C;\">Comparador de <span style=\"color: #2D8CC4;\"> seguros</span></h1></td>");
      html.append("<td width=\"352\" valign=\"bottom\" align=\"right\" ><p style=\"padding-top:8px; color: #494949; font-size:11px;\" >Las mejores ofertas para ti: <span style=\"color:#3BBEEB\"><strong>");
      html.append(oCotizacionSeguro.getNombre() + " " + oCotizacionSeguro.getApellidoPaterno() + " " + oCotizacionSeguro.getApellidoMaterno());
      html.append("</strong>");
      html.append("</span>");
      html.append("</p>");
      html.append("</td>");
      html.append("</tr>");
      html.append("<tr>");
      html.append("<td colspan=\"2\" style=\"height: 40px;margin-bottom: 15px;\"><div style=\"background-color: #F5F5F5;height: 20px;margin-top: 12px;padding-left: 10px;padding-top: 8px;width: 645px;\"><strong style=\"color:#3BBEEB; font-size:15px;\">");
      if(idSubcategoria.intValue() == 22) {
         html.append(descSubcategoria);
      } else {
         html.append(descSubcategoria + "&nbsp;" + descProducto);
      }

      html.append("</strong></div></td>");
      html.append("</tr>");
      html.append("</table>");
      String anchoDesc = "199";
      String anchoValue = "114";
      String colspan = "5";
      if(listPlanes.size() == 2) {
         anchoDesc = "255";
         anchoValue = "200";
         colspan = "3";
      } else if(listPlanes.size() == 3) {
         anchoDesc = "199";
         anchoValue = "150";
         colspan = "4";
      }

      html.append("<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"660\" height=\"90\" style=\"background: url(" + imagen + ") no-repeat;\"  background=\"" + imagen + "\">");
      html.append("<tr>");
      html.append("<td width=\"" + anchoDesc + "\" valign=\"top\">");
      if(idRama.intValue() == 1) {
         html.append("<p style=\"padding:5px;font-size:12px;color:#868686;\">");
         html.append(this.getData(request, oDelegate));
         html.append("</p>");
      } else if(idRama.intValue() == 2) {
         if(oCotizacionSeguro.getDireccion() != null && !oCotizacionSeguro.getDireccion().trim().equals("")) {
            html.append("<p style=\"padding:5px;font-size:12px;color:#868686;\"><span>");
            html.append("Vivienda : <br /> Dirección :");
            html.append("</span>");
            html.append(oCotizacionSeguro.getDireccion());
            html.append("&nbsp;");
            html.append(oCotizacionSeguro.getNumeroDireccion());
            if(oCotizacionSeguro.getNumeroDepto() != null && !oCotizacionSeguro.getNumeroDepto().trim().equals("")) {
               html.append(oCotizacionSeguro.getNumeroDepto());
            }

            html.append("<br />");
            Map arrCobComp;
            if(oCotizacionSeguro.getComuna() != null) {
               arrCobComp = oDelegate.obtenerComunaPorId(oCotizacionSeguro.getComuna());
               html.append(arrCobComp.get("descripcion_comuna"));
               html.append(",&nbsp;");
            }

            if(oCotizacionSeguro.getCiudad() != null) {
               arrCobComp = oDelegate.obtenerCiudadPorId(oCotizacionSeguro.getCiudad());
               html.append(arrCobComp.get("descripcion_ciudad"));
            }

            html.append("</p>");
         }
      } else {
         html.append("<p style=\"padding:5px;font-size:12px;color:#868686;\">Contratante :<br />");
         html.append(oCotizacionSeguro.getNombre() + "&nbsp;");
         html.append(oCotizacionSeguro.getApellidoPaterno() + "&nbsp;");
         html.append(oCotizacionSeguro.getApellidoMaterno());
         html.append("<br />");
         html.append(oCotizacionSeguro.getRutCliente());
         html.append("-");
         html.append(oCotizacionSeguro.getDv());
         html.append("</p>");
      }

      html.append("</td> ");
      Iterator var24 = listPlanes.iterator();

      while(var24.hasNext()) {
         Plan arrCobComp1 = (Plan)var24.next();
         String logoCompannia = mapCompannia.containsKey(arrCobComp1.getCompannia())?(String)mapCompannia.get(arrCobComp1.getCompannia()):"";
         NumberFormat nf = NumberFormat.getInstance();
         html.append("<td width=" + anchoValue + " valign=\"top\">");
         html.append("<img src=\"" + pathBase + "/vseg-paris/images/" + logoCompannia + "\" width=\"85\" height=\"30\" style=\"margin-top:10px;\"/><br />");
         html.append("<span style=\"font-size:16px;color:#3bbeeb;font-weight:bold;\">" + nf.format(arrCobComp1.getPrimaMensualPesos()) + " / mes</span><br />");
         html.append("<span style=\"font-size:12px;color:#868686;\">" + nf.format((double)arrCobComp1.getPrimaMensualUF()) + " / mes</span>");
         html.append("</td>");
      }

      html.append("</tr>");
      html.append("</table>");
      if(mapBeneficio != null && mapBeneficio.size() > 0) {
         html.append("<table cellpadding=\"2\" cellspacing=\"2\" border=\"0\" width=\"660\" style=\"margin-top:20px;font-size:12px;color:#8E8E8E;\">");
         html.append("<tr>");
         html.append("<td colspan=\"" + colspan + "\" style=\"background-color:#FFFFFF;\"><p style=\"padding:5px; margin:0px; padding-bottom:0px; padding-top:0px; color:#3BBEEB\"><strong>Beneficios y condiciones :</strong></p></td>");
         html.append("</tr>");
         html.append(this.generarHTML(mapBeneficio, listPlanes));
         html.append("</table>");
      }

      if(mapCoberturaPart != null && mapCoberturaPart.size() > 0) {
         html.append("<table cellpadding=\"2\" cellspacing=\"2\" border=\"0\" width=\"660\" style=\"margin-top:20px;font-size:12px;color:#8E8E8E;\">");
         html.append("<tr>");
         html.append("<td colspan=\"" + colspan + "\" style=\"background-color:#FFFFFF;\"><p style=\"padding:5px; margin:0px; padding-bottom:0px; padding-top:0px; color:#3BBEEB\"><strong>Coberturas por plan :</strong></p></td>");
         html.append("</tr>");
         html.append(this.generarHTML(mapCoberturaPart, listPlanes));
         html.append("</table>");
      }

      if(mapExclusion != null && mapExclusion.size() > 0) {
         html.append("<table cellpadding=\"2\" cellspacing=\"2\" border=\"0\" width=\"660\" style=\"margin-top:20px;font-size:12px;color:#8E8E8E;\">");
         html.append("<tr>");
         html.append("<td colspan=\"" + colspan + "\" style=\"background-color:#FFFFFF;\"><p style=\"padding:5px; margin:0px; padding-bottom:0px; padding-top:0px; color:#3BBEEB\"><strong>Exclusiones :</strong></p></td>");
         html.append("</tr>");
         html.append(this.generarHTML(mapExclusion, listPlanes));
         html.append("</table>");
      }

      html.append("<table cellpadding=\"0\" cellspacing=\"0\" width=\"660\" border=\"0\">");
      html.append("<tr>");
      html.append("<td no-repeat;\" height=\"60\">");
      html.append("<a target=\"_blank\" href=\"" + pathBase + "/cotizador/desplegar-cotizacion.do?idRama=" + idRama + "&idSubcategoria=" + idSubcategoria + "&token=" + idVitrineo + "\"><img src=\"" + pathBase + "/cotizador/images/comparador/continuar_boton.jpg\" width=\"660\" border=\"0\" align=\"right\" style=\"margin-top:-26px;\" onmouseover=\"this.src = \'" + pathBase + "/cotizador/images/comparador/btn-continuar-hover.png\'\" /></a></span>");
      html.append("</td>");
      html.append("</tr>");
      html.append("</table>");
      if(mapCoberturaComp != null && mapCoberturaComp.size() > 0) {
         String[] arrCobComp2 = this.generarHTMLComun(mapCoberturaComp, listPlanes);
         html.append("<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"660\" style=\"margin-top:20px;\">");
         html.append("<tr>");
         html.append("<td colspan=\"2\"><p style=\"padding:5px; margin:0px; padding-bottom:0px; padding-top:0px; color:#3BBEEB\"><strong>Coberturas comunes para todos :</strong></p></td>");
         html.append("</tr>");
         html.append("<tr>");
         html.append("<td>");
         html.append("<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"660\"  style=\"border:1px solid #e2e2e2; margin-top:20px;\">");
         html.append("<tr>");
         html.append("<td width=\"25%\" style=\"color: #8F8F8F;font-size: 11px;line-height: 180%;padding-left:10px;\" valign=\"top\">");
         html.append(arrCobComp2[0]);
         html.append("</td>");
         html.append("<td width=\"25%\" style=\"color: #8F8F8F;font-size: 11px;line-height: 180%;padding-left:10px;\" valign=\"top\">");
         html.append(arrCobComp2[1]);
         html.append("</td>");
         html.append("<td width=\"25%\" style=\"color: #8F8F8F;font-size: 11px;line-height: 180%;padding-left:10px;\" valign=\"top\">");
         html.append(arrCobComp2[2]);
         html.append("</td>");
         html.append("<td width=\"25%\" style=\"color: #8F8F8F;font-size: 11px;line-height: 180%;padding-left:10px;\" valign=\"top\">");
         html.append(arrCobComp2[3]);
         html.append("</td>");
         html.append("</tr>");
         html.append("</table>");
         html.append("</td>");
         html.append("</tr>");
         html.append("</table>");
      }

      html.append("</td>");
      html.append("</tr>");
      html.append("</table>");
      html.append("<table width=\"600\" height=\"auto\" align=\"center\" style=\"margin-top: -30px; margin-bottom: 15px;\">");
      html.append("<tr>");
      html.append("<td >&nbsp; &nbsp; &nbsp; &nbsp;</td>");
      html.append("<td style=\"font-family:Arial, Helvetica, sans-serif; color:#000000; font-size:10px; font-weight:bold; \">Te Ayudamos:</td>");
      html.append("<td >&nbsp; &nbsp; </td>");
      html.append("<td ><u><div id=\"footer\" style=\"font-family:Arial,Helvetica,sans-serif;color:#666;font-size:10px;font-weight:normal;text-decoration: none;\"> <a href=\"" + pathBase + "/vseg-paris/html/preguntas-frecuentes.html\" >Preguntas Frecuentes</a> </div></u></td>");
      html.append("<td >&nbsp; </td>");
      html.append("<td ><u><div id=\"footer\" style=\"font-family:Arial,Helvetica,sans-serif;color:#666;font-size:10px;font-weight:normal;text-decoration: none;\"> <a href=\"" + pathBase + "/vseg-paris/html/caso-siniestro.html\" >&iquest;Qu&eacute; hacer en caso de Siniestro?</a> </div> </u></td>");
      html.append("<td ><u><div id=\"footer\" style=\"font-family:Arial,Helvetica,sans-serif;color:#666;font-size:10px;font-weight:normal;text-decoration: none;\"> <a href=\"" + pathBase + "/vseg-paris/html/glosario.html\" >Glosario de T&eacute;rminos</a> </div> </u></td>");
      html.append("<td >&nbsp;</td>");
      html.append("</tr>");
      html.append("</table>");
      html.append("</body>");
      html.append("</html>");
      return html;
   }

   private String[] generarHTMLComun(LinkedHashMap mapCoberturaComp, List listPlanes) {
      String[] result = new String[4];
      String key = "caractComp";
      if(mapCoberturaComp.containsKey(key)) {
         List list = (List)mapCoberturaComp.get(key);
         StringBuffer blockDesc = new StringBuffer();
         StringBuffer blockValue = new StringBuffer();
         boolean opt = false;
         int var11;
         if(list.size() % 2 > 0) {
            var11 = 1 + list.size() / 2;
         } else {
            var11 = list.size() / 2;
         }

         int i;
         for(i = 0; i < var11; ++i) {
            blockDesc.append(((Caracteristica)list.get(i)).getDescripcion() + "<br />");
            blockValue.append(((Caracteristica)list.get(i)).getValor() + "<br />");
         }

         result[0] = blockDesc.toString();
         result[1] = blockValue.toString();
         blockDesc = new StringBuffer();
         blockValue = new StringBuffer();

         for(i = var11 + 1; i <= list.size(); ++i) {
            int index = i - 1;
            blockDesc.append(((Caracteristica)list.get(index)).getDescripcion() + "<br />");
            blockValue.append(((Caracteristica)list.get(index)).getValor() + "<br />");
         }

         result[2] = blockDesc.toString();
         result[3] = blockValue.toString();
      }

      return result;
   }

   private StringBuffer generarHTML(LinkedHashMap mapCaract, List listPlanes) {
      new LinkedHashMap();
      StringBuffer titulo = new StringBuffer();
      String anchoDesc = "199";
      String anchoValue = "114";
      if(listPlanes.size() == 2) {
         anchoDesc = "255";
         anchoValue = "200";
      } else if(listPlanes.size() == 3) {
         anchoDesc = "199";
         anchoValue = "150";
      }

      Collection c = mapCaract.values();
      Iterator iter = c.iterator();

      while(iter.hasNext()) {
         List list = (List)iter.next();
         titulo.append("<tr>");
         if(list != null && list.size() > 0) {
            titulo.append("<th width=\"" + anchoDesc + "\" style=\"background-color:#F5F5F5;font-variant:small-caps;font-weight: bold;text-align:left;font-size:11px;padding:5px;width:208px;\">" + ((Caracteristica)list.get(0)).getDescripcion());
            titulo.append("</th>");
         }

         for(int i = 0; i < listPlanes.size(); ++i) {
            Plan item = (Plan)listPlanes.get(i);

            for(int j = 0; j < list.size(); ++j) {
               if(item.getIdPlan().trim().equals(String.valueOf(((Caracteristica)list.get(j)).getId_plan()))) {
                  titulo.append("<td width=\"" + anchoValue + "\" style=\"background-color:#F5F5F5;font-family: arial,Verdana,Helvetica,sans-serif;font-size:11px;padding: 5px 5px 5px 10px;\">" + ((Caracteristica)list.get(j)).getValor() + "</td>");
                  break;
               }
            }
         }

         titulo.append("</tr>");
      }

      return titulo;
   }

   private LinkedHashMap getMapCompannias() {
      LinkedHashMap mapCompannia = new LinkedHashMap();
      mapCompannia.put("LIBERTY GENERALES", "logo-liberty.gif");
      mapCompannia.put("MAGALLANES S.A", "logo-magallanes.gif");
      mapCompannia.put("CHARTIS", "logo-chartis.gif");
      mapCompannia.put("RSA SEGUROS (CHILE)", "logo-rsa.gif");
      mapCompannia.put("CARDIF", "logo-cardif.gif");
      mapCompannia.put("ZENIT", "logo-zenit.gif");
      mapCompannia.put("INTERAMERICANA", "logo-metlife.gif");
      mapCompannia.put("CONSORCIO GENERALES", "logo-consorcio.gif");
      return mapCompannia;
   }

   private String getData(HttpServletRequest request, CotizacionDelegate oDelegate) {
      String result = "";
      if(request.getSession().getAttribute("datosCotizacion") instanceof CotizacionSeguroVehiculo) {
         CotizacionSeguroVehiculo datosCot = (CotizacionSeguroVehiculo)request.getSession().getAttribute("datosCotizacion");
         Vehiculo[] tipos = oDelegate.obtenerTiposVehiculos();
         Vehiculo[] marcas = oDelegate.obtenerMarcasPorTipoVehiculos(String.valueOf(datosCot.getCodigoTipoVehiculo()));
         Vehiculo[] modelos = oDelegate.obtenerModelosVehiculos(String.valueOf(datosCot.getCodigoTipoVehiculo()), String.valueOf(datosCot.getCodigoMarca()));
         String tipoDesc = "";
         String marcaDesc = "";
         String modeloDes = "";

         int i;
         for(i = 0; i < tipos.length; ++i) {
            if(tipos[i].getIdTipoVehiculo().equals(String.valueOf(datosCot.getCodigoTipoVehiculo()))) {
               tipoDesc = tipos[i].getDescripcionTipoVehiculo();
            }
         }

         for(i = 0; i < marcas.length; ++i) {
            if(marcas[i].getIdMarcaVehiculo().equals(String.valueOf(datosCot.getCodigoMarca()))) {
               marcaDesc = marcas[i].getMarcaVehiculo();
            }
         }

         for(i = 0; i < modelos.length; ++i) {
            if(modelos[i].getIdModeloVehiculo().equals(String.valueOf(datosCot.getCodigoModelo()))) {
               modeloDes = modelos[i].getModeloVehiculo();
            }
         }

         result = tipoDesc + "&nbsp;:<br /><strong>" + marcaDesc + "&nbsp;" + modeloDes + "</strong><br />Año: " + datosCot.getAnyoVehiculo();
      }

      return result;
   }

   private String getUserName(HttpServletRequest request) {
      HttpSession session = request.getSession();
      String username = "";
      if(session != null && session.getAttribute("cl.tinet.common.seguridad.USUARIO_CONECTADO") != null) {
         UsuarioExterno usuario = (UsuarioExterno)session.getAttribute("cl.tinet.common.seguridad.USUARIO_CONECTADO");
         username = usuario.getUsername();
      }

      return username;
   }
}
