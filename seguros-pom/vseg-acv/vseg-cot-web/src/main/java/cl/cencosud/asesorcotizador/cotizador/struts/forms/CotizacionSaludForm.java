// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:21
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.forms;

import cl.tinet.common.struts.form.BuilderActionFormBaseCOT;
import cl.tinet.common.util.validate.ValidacionUtil;
import java.io.InputStream;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class CotizacionSaludForm extends BuilderActionFormBaseCOT {

   private static final long serialVersionUID = -5838078249856020124L;


   public InputStream getValidationRules(HttpServletRequest request) {
      return CotizacionSaludForm.class.getResourceAsStream("resource/validation-salud.xml");
   }

   public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
      ActionErrors errors = super.validate(mapping, request);
      long date;
      if(!errors.get("datos.dv").hasNext() && !errors.get("datos.rut").hasNext() && !((String)this.getDatos().get("rut")).equals("") && !((String)this.getDatos().get("dv")).equals("")) {
         date = Long.parseLong((String)this.getDatos().get("rut"));
         char digitoVerificador = ((String)this.getDatos().get("dv")).length() == 0?32:((String)this.getDatos().get("dv")).charAt(0);

         try {
            if(!ValidacionUtil.isValidoRUT(date, digitoVerificador)) {
               errors.add("datos.rutdv", new ActionMessage("errors.validacion"));
               errors.add("datos.dv", new ActionMessage("errors.validacion"));
               errors.add("datos.rut", new ActionMessage("errors.validacion", true));
            }
         } catch (IllegalArgumentException var9) {
            errors.add("datos.rutdv", new ActionMessage("errors.validacion"));
            errors.add("datos.dv", new ActionMessage("errors.validacion"));
            errors.add("datos.rut", new ActionMessage("errors.validacion"));
         }
      } else {
         errors.add("datos.rutdv", new ActionMessage("errors.required"));
      }

      if(!((String)this.getDatos().get("nombre")).equals("") && !((String)this.getDatos().get("apellidoPaterno")).equals("") && !((String)this.getDatos().get("apellidoMaterno")).equals("")) {
         if(errors.get("datos.nombre").hasNext() || errors.get("datos.apellidoPaterno").hasNext() || errors.get("datos.apellidoMaterno").hasNext()) {
            errors.add("datos.nombres", new ActionMessage("errors.validacion"));
         }
      } else {
         errors.add("datos.nombres", new ActionMessage("errors.required"));
      }

      if(!((String)this.getDatos().get("tipoTelefono")).equals("") && !((String)this.getDatos().get("codigoTelefono")).equals("") && !((String)this.getDatos().get("numeroTelefono")).equals("")) {
         try {
            date = Long.parseLong((String)this.getDatos().get("numeroTelefono"));
            if(date <= 0L) {
               errors.add("telefono", new ActionMessage("errors.required"));
               errors.add("datos.tipoTelefono", new ActionMessage("errors.validacion"));
               errors.add("datos.codigoTelefono", new ActionMessage("errors.validacion"));
               errors.add("datos.numeroTelefono", new ActionMessage("errors.validacion"));
            }
         } catch (NumberFormatException var8) {
            errors.add("telefono", new ActionMessage("errors.required"));
            errors.add("datos.tipoTelefono", new ActionMessage("errors.validacion"));
            errors.add("datos.codigoTelefono", new ActionMessage("errors.validacion"));
            errors.add("datos.numeroTelefono", new ActionMessage("errors.validacion"));
         }
      } else {
         errors.add("telefono", new ActionMessage("errors.required"));
         errors.add("datos.tipoTelefono", new ActionMessage("errors.required"));
         errors.add("datos.codigoTelefono", new ActionMessage("errors.required"));
         errors.add("datos.numeroTelefono", new ActionMessage("errors.required"));
      }

      if(!((String)this.getDatos().get("diaFechaNacimiento")).equals("") && !((String)this.getDatos().get("mesFechaNacimiento")).equals("") && !((String)this.getDatos().get("anyoFechaNacimiento")).equals("")) {
         String date1 = ((String)this.getDatos().get("diaFechaNacimiento")).length() < 2?"0" + (String)this.getDatos().get("diaFechaNacimiento"):(String)this.getDatos().get("diaFechaNacimiento");
         date1 = date1 + (((String)this.getDatos().get("mesFechaNacimiento")).length() < 2?"0" + (String)this.getDatos().get("mesFechaNacimiento"):(String)this.getDatos().get("mesFechaNacimiento"));
         date1 = date1 + (String)this.getDatos().get("anyoFechaNacimiento");
         if(!ValidacionUtil.isFechaValida(date1, "ddMMyyyy")) {
            errors.add("datos.fechaNacimiento", new ActionMessage("errors.validacion"));
            errors.add("diasFecha", new ActionMessage("errors.validacion"));
            errors.add("mesFecha", new ActionMessage("errors.validacion"));
            errors.add("anyosFecha", new ActionMessage("errors.validacion"));
         }
      } else {
         errors.add("datos.fechaNacimiento", new ActionMessage("errors.required"));
         errors.add("diasFecha", new ActionMessage("errors.required"));
         errors.add("mesFecha", new ActionMessage("errors.required"));
         errors.add("anyosFecha", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("producto")).equals("")) {
         errors.add("producto", new ActionMessage("errors.required"));
      }

      request.setAttribute("cl.tinet.common.struts.validator.PROPERTY_NAMES", errors.properties());
      return errors;
   }
}
