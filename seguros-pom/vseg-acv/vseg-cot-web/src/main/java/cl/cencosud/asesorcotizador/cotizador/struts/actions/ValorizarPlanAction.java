// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:19
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import cl.cencosud.acv.common.Actividad;
import cl.cencosud.acv.common.Contratante;
import cl.cencosud.acv.common.Plan;
import cl.cencosud.acv.common.config.ACVConfig;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida;
import cl.cencosud.asesorcotizador.cotizador.struts.forms.CotizacionForm;
import cl.cencosud.asesorcotizador.cotizador.struts.forms.CotizacionHogarForm;
import cl.cencosud.asesorcotizador.cotizador.struts.forms.CotizacionSaludForm;
import cl.cencosud.asesorcotizador.cotizador.struts.forms.CotizacionVehiculoForm;
import cl.cencosud.asesorcotizador.cotizador.struts.forms.CotizacionVidaForm;
import cl.cencosud.asesorcotizador.cotizador.struts.forms.PruebaThreads;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.tinet.common.struts.form.BuilderActionFormBaseCOT;
import com.tinet.exceptions.system.SystemException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

public class ValorizarPlanAction extends Action {

	public static final Log logger = LogFactory
			.getLog(ValorizarPlanAction.class);
	private static final String BASE_CALCULO = "cl.cencosud.asesorcotizador.scoring.vida.pregunta.baseCalculo";

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		long combinaSeguros = 0L;
		String idProducto4 = "1";
		String idProducto6 = "1";
		PruebaThreads[] p = (PruebaThreads[]) null;
		CotizacionDelegate delegate = new CotizacionDelegate();
		String[] idPlanes = (String[]) null;

		try {
			logger.info("Iniciar Proceso Valorizacion");
			HashMap e = null;
			logger.info("Obteniendo Mapa de parametros");
			e = this.obtenerMapaFormulario(form);
			String idProducto = "";
			String actividad = "";
			Plan[] plan = (Plan[]) null;
			if (e != null) {
				idProducto = (String) e.get("producto");
				actividad = (String) e.get("actividad");
			}
			
			
			String idPlan = request.getParameter("idPlanValorizacion");
			idPlanes = idPlan.split(",");
			CotizacionSeguro cotizacion = this.obtenerDatosSeguro((BuilderActionFormBaseCOT) form);
			cotizacion.setCodigoProducto(Long.parseLong(idProducto));
			cotizacion.setActividad(actividad);

			// GUARDAR TRAMO DE ASISTENCIA EN VIAJE
		
			
			// Inicio hogar vacaciones
			
			cotizacion.setTramoDias((String) e.get("tramo"));
			cotizacion.setIntegrante((String) e.get("integrantes"));
			cotizacion.setContratanteEsAsegurado((String) e.get("contratanteAsegurado"));
			
			cotizacion.setNumSubcategoria((String) request.getSession().getAttribute("idSubcategoria"));
			
			// fin hogar vacaciones
			
			
			request.getSession().setAttribute("datosCotizacion", cotizacion);
			request.setAttribute("subcategoria", request.getSession()
					.getAttribute("subcategoria"));
			int contador;
			if (actividad != null && actividad.length() > 0) {
				Actividad[] i = delegate.obtenerActividades();

				for (contador = 0; contador < i.length; ++contador) {
					if (actividad.equals(i[contador].getId())) {
						request.getSession().setAttribute("actividadDesc",
								i[contador].getDescripcion());
					}
				}
			}

			
			
			System.out.println("Tramo dias : " + cotizacion.getTramoDias() );
			
			boolean var27 = false;
			plan = new Plan[idPlanes.length];
			p = new PruebaThreads[idPlanes.length];

			int var26;
			for (var26 = 0; var26 < idPlanes.length; ++var26) {
				p[var26] = new PruebaThreads();
				plan[var26] = new Plan();
				combinaSeguros = 0L;
				logger.info("Comenzando valorizacion plan: "
						+ idPlanes[var26] + " para producto: " + idProducto);
				if (!idProducto.equals("188") && !idProducto.equals("189")
						&& !idProducto.equals("190")
						&& !idProducto.equals("191")
						&& !idProducto.equals("192")
						&& !idProducto.equals("88")) {
					p[var26].setIdProducto(idProducto);
					p[var26].setCotizacion(cotizacion);
					p[var26].setIdPlan(idPlanes[var26]);
					p[var26].setNumero(var26);
					p[var26].requestData();
				} else {
					String var29 = (String) e.get("hiddProducto2");
					String contadorSalida = (String) e.get("hiddProducto3");
					idProducto4 = (String) e.get("hiddProducto4");
					String estado = (String) e.get("hiddProducto5");
					idProducto6 = (String) e.get("hiddProducto6");
					String pwritter = (String) e.get("hiddProducto7");
					if (idProducto.equals("188")) {
						var29 = idProducto;
					} else if (idProducto.equals("189")) {
						contadorSalida = idProducto;
					} else if (idProducto.equals("190")) {
						idProducto4 = idProducto;
					} else if (idProducto.equals("191")) {
						estado = idProducto;
					} else if (idProducto.equals("192")) {
						idProducto6 = idProducto;
					} else if (idProducto.equals("88")) {
						pwritter = idProducto;
					}

					if (var29.compareToIgnoreCase("1") > 0) {
						++combinaSeguros;
					}

					if (contadorSalida.compareToIgnoreCase("1") > 0) {
						++combinaSeguros;
					}

					if (idProducto4.compareToIgnoreCase("1") > 0) {
						++combinaSeguros;
					}

					if (estado.compareToIgnoreCase("1") > 0) {
						++combinaSeguros;
					}

					if (idProducto6.compareToIgnoreCase("1") > 0) {
						++combinaSeguros;
					}

					if (pwritter.compareToIgnoreCase("1") > 0) {
						++combinaSeguros;
					}

					p[var26].setIdProducto(idProducto);
					p[var26].setIdProducto2(var29);
					p[var26].setIdProducto3(contadorSalida);
					p[var26].setIdProducto4(idProducto4);
					p[var26].setIdProducto5(estado);
					p[var26].setIdProducto6(idProducto6);
					p[var26].setIdProducto7(pwritter);
					p[var26].setCotizacion(cotizacion);
					p[var26].setIdPlan(idPlanes[var26]);
					p[var26].setNumero(var26);
					p[var26].requestData();
				}
			}

			boolean var28 = false;
			int var31 = 0;
			Boolean var32 = Boolean.valueOf(true);

			while (var32.booleanValue()) {
				++var31;
				contador = 0;
				Thread.sleep(1000L); // 16-10-13 Genera un subproceso para la
										// llamada al WS (tiempo en
										// milisegundos)
				if (var31 > 100) {
					break;
				}

				for (var26 = 0; var26 < idPlanes.length; ++var26) {
					if (p[var26].getEstado().booleanValue()) {
						++contador;
						if (contador == p.length) {
							var32 = Boolean.valueOf(false);
						}
					}
				}
			}

			for (var26 = 0; var26 < idPlanes.length; ++var26) {
				plan[var26] = p[var26].getData();
			}

			logger.info("Terminando valorizacion planes: "
					+ idPlan + " para producto: " + idProducto);
			response.setContentType("text/html");
			if (plan != null) {
				request.getSession().setAttribute("plan", plan);
			}

			PrintWriter var30 = response.getWriter();
			ObjectMapper mapper = new ObjectMapper();
			StringWriter json = new StringWriter();
			mapper.writeValue(json, plan);
			var30.write(json.toString());
			return null;
		} catch (IOException var25) {
			throw new SystemException(var25);
		}
	}

	private HashMap obtenerMapaFormulario(ActionForm form) {
		HashMap datos = null;
		if (form instanceof CotizacionHogarForm) {
			datos = (HashMap) ((CotizacionHogarForm) form).getDatos();
		} else if (form instanceof CotizacionVehiculoForm) {
			datos = (HashMap) ((CotizacionVehiculoForm) form).getDatos();
		} else if (form instanceof CotizacionVidaForm) {
			datos = (HashMap) ((CotizacionVidaForm) form).getDatos();
		} else if (form instanceof CotizacionSaludForm) {
			datos = (HashMap) ((CotizacionSaludForm) form).getDatos();
		} else {
			datos = (HashMap) ((CotizacionForm) form).getDatos();
		}

		return datos;
	}

	private CotizacionSeguro obtenerDatosSeguro(BuilderActionFormBaseCOT form) {
		Object cotizacion = null;
		if (form instanceof CotizacionVehiculoForm) {
			CotizacionSeguroVehiculo cotizacion1 = new CotizacionSeguroVehiculo();
			this.obtenerDatosGeneralesCotizacionPaso1(cotizacion1, form);
			cotizacion = this.obtenerDatosSeguroVehiculo(
					(CotizacionVehiculoForm) form,
					(CotizacionSeguroVehiculo) cotizacion1);
		} else if (form instanceof CotizacionHogarForm) {
			CotizacionSeguroHogar cotizacion2 = new CotizacionSeguroHogar();
			this.obtenerDatosGeneralesCotizacionPaso1(cotizacion2, form);
			cotizacion = this.obtenerDatosSeguroHogar(
					(CotizacionHogarForm) form,
					(CotizacionSeguroHogar) cotizacion2);
		} else {
			CotizacionSeguroVida cotizacion3;
			if (form instanceof CotizacionVidaForm) {
				cotizacion3 = new CotizacionSeguroVida();
				this.obtenerDatosGeneralesCotizacionPaso1(cotizacion3, form);
				cotizacion = this.obtenerDatosSeguroVida(
						(CotizacionVidaForm) form,
						(CotizacionSeguroVida) cotizacion3);
			} else if (form instanceof CotizacionSaludForm) {
				cotizacion3 = new CotizacionSeguroVida();
				this.obtenerDatosGeneralesCotizacionPaso1(cotizacion3, form);
				cotizacion = this.obtenerDatosSeguroSalud(
						(CotizacionSaludForm) form,
						(CotizacionSeguroVida) cotizacion3);
			} else if (form instanceof CotizacionForm) {
				cotizacion3 = new CotizacionSeguroVida();
				this.obtenerDatosGeneralesCotizacionPaso1(cotizacion3, form);
				cotizacion = this.obtenerDatosSeguro((CotizacionForm) form,
						(CotizacionSeguroVida) cotizacion3);		
			}
		}

		return (CotizacionSeguro) cotizacion;
	}

	private CotizacionSeguroVehiculo obtenerDatosSeguroVehiculo(
			CotizacionVehiculoForm form, CotizacionSeguroVehiculo cotizacion) {
		cotizacion.setEdadConductor(0);
		cotizacion.setCodigoProducto(Long.parseLong((String) form.getDatos()
				.get("producto")));
		cotizacion.setCodigoMarca(Integer.parseInt((String) form.getDatos()
				.get("marcaVehiculo")));
		cotizacion.setCodigoTipoVehiculo(Integer.parseInt((String) form
				.getDatos().get("tipoVehiculo")));
		cotizacion.setCodigoModelo(Integer.parseInt((String) form.getDatos()
				.get("modeloVehiculo")));
		if (((String) form.getDatos().get("valorComercial")).equals("")) {
			form.getDatos().put("valorComercial", "0.0");
		}

		cotizacion.setEstadoCivil((String) form.getDatos().get("estadoCivil"));
		cotizacion.setNumeroPuertas(Integer.valueOf(
				(String) form.getDatos().get("numeroPuertas")).intValue());
		cotizacion.setMontoAsegurado(Float.parseFloat((String) form.getDatos()
				.get("valorComercial")));
		cotizacion.setAnyoVehiculo(Integer.parseInt((String) form.getDatos()
				.get("anyoVehiculo")));
		Contratante contratante = new Contratante();
		contratante.setRut(cotizacion.getRutCliente());
		contratante.setDv(String.valueOf(cotizacion.getDv()));
		contratante.setNombre(cotizacion.getNombre());
		contratante.setApellidoMaterno(cotizacion.getApellidoMaterno());
		contratante.setApellidoPaterno(cotizacion.getApellidoPaterno());
		contratante.setEmail(cotizacion.getEmail());
		contratante.setSexo(cotizacion.getSexo());
		contratante.setEstadoCivil(cotizacion.getEstadoCivil());
		contratante.setTelefono1(cotizacion.getNumeroTelefono1());
		contratante.setTipoTelefono1(cotizacion.getTipoTelefono1());
		contratante.setCalleDireccion(cotizacion.getDireccion());
		contratante.setNumeroDireccion(cotizacion.getNumeroDireccion());
		contratante.setNumeroDepartamentoDireccion(cotizacion.getNumeroDepto());
		contratante.setFechaDeNacimiento(cotizacion.getFechaNacimiento());
		contratante.setEstadoCivil(cotizacion.getEstadoCivil());
		cotizacion.setContratante(contratante);
		Contratante asegurado;
		if (form.getDatos().get("contratanteEsDuenyo") != null
				&& ((String) form.getDatos().get("contratanteEsDuenyo"))
						.equals("false")) {
			cotizacion.setClienteEsAsegurado(false);
			asegurado = new Contratante();

			Date e;
			try {
				String e1 = (String) form.getDatos().get(
						"anyoFechaNacimientoDuenyo");
				String dia = ((String) form.getDatos().get(
						"diaFechaNacimientoDuenyo")).length() == 1 ? "0"
						.concat((String) form.getDatos().get(
								"diaFechaNacimientoDuenyo")) : (String) form
						.getDatos().get("diaFechaNacimientoDuenyo");
				String mes = ((String) form.getDatos().get(
						"mesFechaNacimientoDuenyo")).length() == 1 ? "0"
						.concat((String) form.getDatos().get(
								"mesFechaNacimientoDuenyo")) : (String) form
						.getDatos().get("mesFechaNacimientoDuenyo");
				e = (new SimpleDateFormat("ddMMyyyy")).parse(dia + mes + e1);
			} catch (ParseException var11) {
				throw new SystemException(var11);
			}

			asegurado.setFechaDeNacimiento(e);
			asegurado.setEstadoCivil((String) form.getDatos().get(
					"estadoCivilDuenyo"));
			asegurado.setSexo((String) form.getDatos().get("sexoDuenyo"));
			asegurado.setRut(Long.parseLong((String) form.getDatos().get(
					"rutDuenyo")));
			asegurado.setDv((String) form.getDatos().get("dvDuenyo"));
			asegurado.setNombre((String) form.getDatos().get("nombreDuenyo"));
			asegurado.setApellidoPaterno((String) form.getDatos().get(
					"apellidoPaternoDuenyo"));
			asegurado.setApellidoMaterno((String) form.getDatos().get(
					"apellidoMaternoDuenyo"));
			cotizacion.setAsegurado(asegurado);
		} else {
			cotizacion.setClienteEsAsegurado(true);
			asegurado = new Contratante();

			try {
				BeanUtils.copyProperties(asegurado, contratante);
			} catch (IllegalAccessException var9) {
				logger.error("Error ilegal al copiar propiedades", var9);
			} catch (InvocationTargetException var10) {
				logger.error("Error de invocacion al copiar propiedades", var10);
			}

			cotizacion.setAsegurado(asegurado);
		}

		return cotizacion;
	}

	private CotizacionSeguroHogar obtenerDatosSeguroHogar(
			CotizacionHogarForm form, CotizacionSeguroHogar cotizacion) {
		
		cotizacion.setCodigoProducto(Long.parseLong((String) form.getDatos()
				.get("producto")));
		String contenido = form.getDatos().get("montoAseguradoContenido") == null ? "0"
				: (String) form.getDatos().get("montoAseguradoContenido");
		String edificio = form.getDatos().get("montoAseguradoEdificio") == null ? "0"
				: (String) form.getDatos().get("montoAseguradoEdificio");
		String robo = form.getDatos().get("montoAseguradoRobo") == null ? "0"
				: (String) form.getDatos().get("montoAseguradoRobo");
		
		cotizacion.setMontoAseguradoContenido(Float.parseFloat(contenido));
		cotizacion.setMontoAseguradoEdificio(Float.parseFloat(edificio));
		cotizacion.setMontoAseguradoRobo(Float.parseFloat(robo));
		cotizacion.setRegion((String) form.getDatos().get("regionResidenciaDuenyo"));
		cotizacion.setComuna((String) form.getDatos().get("comunaResidenciaDuenyo"));
		cotizacion.setCiudad((String) form.getDatos().get("ciudadResidenciaDuenyo"));
		cotizacion.setAntiguedadVivienda((String) form.getDatos().get("antiguedadVivienda"));
		Contratante contratante = new Contratante();
		contratante.setRut(cotizacion.getRutCliente());
		contratante.setDv(String.valueOf(cotizacion.getDv()));
		contratante.setNombre(cotizacion.getNombre());
		contratante.setApellidoMaterno(cotizacion.getApellidoMaterno());
		contratante.setApellidoPaterno(cotizacion.getApellidoPaterno());
		contratante.setEmail(cotizacion.getEmail());
		contratante.setSexo(cotizacion.getSexo());
		contratante.setEstadoCivil(cotizacion.getEstadoCivil());
		contratante.setTelefono1(cotizacion.getNumeroTelefono1());
		contratante.setTipoTelefono1(cotizacion.getTipoTelefono1());
		contratante.setCalleDireccion(cotizacion.getDireccion());
		contratante.setNumeroDireccion(cotizacion.getNumeroDireccion());
		contratante.setNumeroDepartamentoDireccion(cotizacion.getNumeroDepto());
		contratante.setFechaDeNacimiento(cotizacion.getFechaNacimiento());
		contratante.setEstadoCivil(cotizacion.getEstadoCivil());
		cotizacion.setContratante(contratante);
		Contratante asegurado;
		if (form.getDatos().get("contratanteEsDuenyo") != null
				&& ((String) form.getDatos().get("contratanteEsDuenyo"))
						.equals("false")) {
			cotizacion.setClienteEsAsegurado(false);
			asegurado = new Contratante();
			asegurado.setRut(Long.parseLong((String) form.getDatos().get(
					"rutDuenyo")));
			asegurado.setDv((String) form.getDatos().get("dvRutDuenyo"));
			asegurado.setNombre((String) form.getDatos().get("nombreDuenyo"));
			asegurado.setApellidoMaterno((String) form.getDatos().get(
					"apellidoMaternoDuenyo"));
			asegurado.setApellidoPaterno((String) form.getDatos().get(
					"apellidoPaternoDuenyo"));
			cotizacion.setAsegurado(asegurado);
		} else {
			cotizacion.setClienteEsAsegurado(true);
			asegurado = new Contratante();

			try {
				BeanUtils.copyProperties(asegurado, contratante);
			} catch (IllegalAccessException var9) {
				logger.error("Error ilegal al copiar propiedades", var9);
			} catch (InvocationTargetException var10) {
				logger.error("Error de invocacion al copiar propiedades", var10);
			}

			cotizacion.setAsegurado(asegurado);
		}
			
		return cotizacion;
	}

	private CotizacionSeguroVida obtenerDatosSeguroVida(
			CotizacionVidaForm form, CotizacionSeguroVida cotizacion) {
		cotizacion.setCodigoProducto(Long.parseLong((String) form.getDatos()
				.get("producto")));

		Date fechaNacimiento;
		try {
			String baseCalculo = (String) form.getDatos().get(
					"anyoFechaNacimiento");
			String contratante = ((String) form.getDatos().get(
					"diaFechaNacimiento")).length() == 1 ? "0"
					.concat((String) form.getDatos().get("diaFechaNacimiento"))
					: (String) form.getDatos().get("diaFechaNacimiento");
			String asegurado = ((String) form.getDatos().get(
					"mesFechaNacimiento")).length() == 1 ? "0"
					.concat((String) form.getDatos().get("mesFechaNacimiento"))
					: (String) form.getDatos().get("mesFechaNacimiento");
			fechaNacimiento = (new SimpleDateFormat("dd-MM-yyyy"))
					.parse(contratante + "-" + asegurado + "-" + baseCalculo);
		} catch (ParseException var10) {
			throw new SystemException(var10);
		}

		float baseCalculo1 = Float
				.valueOf(
						ACVConfig
								.getInstance()
								.getString(
										"cl.cencosud.asesorcotizador.scoring.vida.pregunta.baseCalculo"))
				.floatValue();
		cotizacion.setBaseCalculo(baseCalculo1);
		cotizacion.setEstadoCivil((String) form.getDatos().get("estadoCivil"));
		cotizacion.setFechaNacimiento(fechaNacimiento);
		cotizacion.setActividad((String) form.getDatos().get("actividad"));
		Contratante contratante1 = new Contratante();
		contratante1.setRut(cotizacion.getRutCliente());
		contratante1.setDv(String.valueOf(cotizacion.getDv()));
		contratante1.setNombre(cotizacion.getNombre());
		contratante1.setApellidoMaterno(cotizacion.getApellidoMaterno());
		contratante1.setApellidoPaterno(cotizacion.getApellidoPaterno());
		contratante1.setEmail(cotizacion.getEmail());
		contratante1.setSexo(cotizacion.getSexo());
		contratante1.setEstadoCivil(cotizacion.getEstadoCivil());
		contratante1.setTelefono1(cotizacion.getNumeroTelefono1());
		contratante1.setTipoTelefono1(cotizacion.getTipoTelefono1());
		contratante1.setCalleDireccion(cotizacion.getDireccion());
		contratante1.setNumeroDireccion(cotizacion.getNumeroDireccion());
		contratante1
				.setNumeroDepartamentoDireccion(cotizacion.getNumeroDepto());
		contratante1.setFechaDeNacimiento(cotizacion.getFechaNacimiento());
		contratante1.setEstadoCivil(cotizacion.getEstadoCivil());
		cotizacion.setContratante(contratante1);
		Contratante asegurado1 = new Contratante();

		try {
			BeanUtils.copyProperties(asegurado1, contratante1);
		} catch (IllegalAccessException var8) {
			logger.error("Error ilegal al copiar propiedades", var8);
		} catch (InvocationTargetException var9) {
			logger.error("Error de invocacion al copiar propiedades", var9);
		}

		cotizacion.setAsegurado(asegurado1);
		cotizacion.setClienteEsAsegurado(true);
		return cotizacion;
	}

	private CotizacionSeguroVida obtenerDatosSeguroSalud(
			CotizacionSaludForm form, CotizacionSeguroVida cotizacion) {
		cotizacion.setCodigoProducto(Long.parseLong((String) form.getDatos()
				.get("producto")));

		Date fechaNacimiento;
		try {
			String baseCalculo = (String) form.getDatos().get(
					"anyoFechaNacimiento");
			String contratante = ((String) form.getDatos().get(
					"diaFechaNacimiento")).length() == 1 ? "0"
					.concat((String) form.getDatos().get("diaFechaNacimiento"))
					: (String) form.getDatos().get("diaFechaNacimiento");
			String asegurado = ((String) form.getDatos().get(
					"mesFechaNacimiento")).length() == 1 ? "0"
					.concat((String) form.getDatos().get("mesFechaNacimiento"))
					: (String) form.getDatos().get("mesFechaNacimiento");
			fechaNacimiento = (new SimpleDateFormat("dd-MM-yyyy"))
					.parse(contratante + "-" + asegurado + "-" + baseCalculo);
		} catch (ParseException var10) {
			throw new SystemException(var10);
		}

		float baseCalculo1 = Float
				.valueOf(
						ACVConfig
								.getInstance()
								.getString(
										"cl.cencosud.asesorcotizador.scoring.vida.pregunta.baseCalculo"))
				.floatValue();
		cotizacion.setBaseCalculo(baseCalculo1);
		cotizacion.setEstadoCivil((String) form.getDatos().get("estadoCivil"));
		cotizacion.setFechaNacimiento(fechaNacimiento);
		cotizacion.setActividad((String) form.getDatos().get("actividad"));
		Contratante contratante1 = new Contratante();
		contratante1.setRut(cotizacion.getRutCliente());
		contratante1.setDv(String.valueOf(cotizacion.getDv()));
		contratante1.setNombre(cotizacion.getNombre());
		contratante1.setApellidoMaterno(cotizacion.getApellidoMaterno());
		contratante1.setApellidoPaterno(cotizacion.getApellidoPaterno());
		contratante1.setEmail(cotizacion.getEmail());
		contratante1.setSexo(cotizacion.getSexo());
		contratante1.setEstadoCivil(cotizacion.getEstadoCivil());
		contratante1.setTelefono1(cotizacion.getNumeroTelefono1());
		contratante1.setTipoTelefono1(cotizacion.getTipoTelefono1());
		contratante1.setCalleDireccion(cotizacion.getDireccion());
		contratante1.setNumeroDireccion(cotizacion.getNumeroDireccion());
		contratante1
				.setNumeroDepartamentoDireccion(cotizacion.getNumeroDepto());
		contratante1.setFechaDeNacimiento(cotizacion.getFechaNacimiento());
		contratante1.setEstadoCivil(cotizacion.getEstadoCivil());
		cotizacion.setContratante(contratante1);
		Contratante asegurado1 = new Contratante();

		try {
			BeanUtils.copyProperties(asegurado1, contratante1);
		} catch (IllegalAccessException var8) {
			logger.error("Error ilegal al copiar propiedades", var8);
		} catch (InvocationTargetException var9) {
			logger.error("Error de invocacion al copiar propiedades", var9);
		}

		cotizacion.setAsegurado(asegurado1);
		cotizacion.setClienteEsAsegurado(true);
		return cotizacion;
	}

	private CotizacionSeguroVida obtenerDatosSeguro(CotizacionForm form,
			CotizacionSeguroVida cotizacion) {
		cotizacion.setRutCliente((long) Integer.parseInt((String) form
				.getDatos().get("rut")));
		cotizacion.setCodigoProducto(Long.parseLong((String) form.getDatos()
				.get("producto")));

		Date fechaNacimiento;
		try {
			String baseCalculo = (String) form.getDatos().get(
					"anyoFechaNacimiento");
			String contratante = ((String) form.getDatos().get(
					"diaFechaNacimiento")).length() == 1 ? "0"
					.concat((String) form.getDatos().get("diaFechaNacimiento"))
					: (String) form.getDatos().get("diaFechaNacimiento");
			String asegurado = ((String) form.getDatos().get(
					"mesFechaNacimiento")).length() == 1 ? "0"
					.concat((String) form.getDatos().get("mesFechaNacimiento"))
					: (String) form.getDatos().get("mesFechaNacimiento");
			fechaNacimiento = (new SimpleDateFormat("dd-MM-yyyy"))
					.parse(contratante + "-" + asegurado + "-" + baseCalculo);
		} catch (ParseException var10) {
			throw new SystemException(var10);
		}

		float baseCalculo1 = Float
				.valueOf(
						ACVConfig
								.getInstance()
								.getString(
										"cl.cencosud.asesorcotizador.scoring.vida.pregunta.baseCalculo"))
				.floatValue();
		cotizacion.setBaseCalculo(baseCalculo1);
		cotizacion.setEstadoCivil((String) form.getDatos().get("estadoCivil"));
		cotizacion.setFechaNacimiento(fechaNacimiento);
		Contratante contratante1 = new Contratante();
		contratante1.setRut(cotizacion.getRutCliente());
		contratante1.setDv(String.valueOf(cotizacion.getDv()));
		contratante1.setNombre(cotizacion.getNombre());
		contratante1.setApellidoMaterno(cotizacion.getApellidoMaterno());
		contratante1.setApellidoPaterno(cotizacion.getApellidoPaterno());
		contratante1.setEmail(cotizacion.getEmail());
		contratante1.setSexo(cotizacion.getSexo());
		contratante1.setEstadoCivil(cotizacion.getEstadoCivil());
		contratante1.setTelefono1(cotizacion.getNumeroTelefono1());
		contratante1.setTipoTelefono1(cotizacion.getTipoTelefono1());
		contratante1.setCalleDireccion(cotizacion.getDireccion());
		contratante1.setNumeroDireccion(cotizacion.getNumeroDireccion());
		contratante1
				.setNumeroDepartamentoDireccion(cotizacion.getNumeroDepto());
		contratante1.setFechaDeNacimiento(cotizacion.getFechaNacimiento());
		contratante1.setEstadoCivil(cotizacion.getEstadoCivil());
		cotizacion.setContratante(contratante1);
		Contratante asegurado1 = new Contratante();

		// INICIO CONTRATACION TERCEROS
		if (form.getDatos().get("contratanteEsDuenyo") != null
				&& ((String) form.getDatos().get("contratanteEsDuenyo"))
						.equals("false")) {
			cotizacion.setClienteEsAsegurado(false);
			asegurado1.setIdRegion((String) form.getDatos().get(
					"regionResidenciaDuenyo"));
			asegurado1.setIdComuna((String) form.getDatos().get(
					"comunaResidenciaDuenyo"));

			Date e;
			try {
				String e1 = (String) form.getDatos().get(
						"anyoFechaNacimientoDuenyo");
				String dia = ((String) form.getDatos().get(
						"diaFechaNacimientoDuenyo")).length() == 1 ? "0"
						.concat((String) form.getDatos().get(
								"diaFechaNacimientoDuenyo")) : (String) form
						.getDatos().get("diaFechaNacimientoDuenyo");
				String mes = ((String) form.getDatos().get(
						"mesFechaNacimientoDuenyo")).length() == 1 ? "0"
						.concat((String) form.getDatos().get(
								"mesFechaNacimientoDuenyo")) : (String) form
						.getDatos().get("mesFechaNacimientoDuenyo");
				e = (new SimpleDateFormat("ddMMyyyy")).parse(dia + mes + e1);
			} catch (ParseException var11) {
				throw new SystemException(var11);
			}

			asegurado1.setFechaDeNacimiento(e);
			asegurado1.setEstadoCivil((String) form.getDatos().get(
					"estadoCivilDuenyo"));
			asegurado1.setSexo((String) form.getDatos().get("sexoDuenyo"));
			asegurado1.setRut(Long.parseLong((String) form.getDatos().get(
					"rutDuenyo")));
			asegurado1.setDv((String) form.getDatos().get("dvDuenyo"));
			asegurado1.setNombre((String) form.getDatos().get("nombreDuenyo"));
			asegurado1.setApellidoPaterno((String) form.getDatos().get(
					"apellidoPaternoDuenyo"));
			asegurado1.setApellidoMaterno((String) form.getDatos().get(
					"apellidoMaternoDuenyo"));
			cotizacion.setAsegurado(asegurado1);
		} else {
			cotizacion.setClienteEsAsegurado(true);
			asegurado1 = new Contratante();

			try {
				BeanUtils.copyProperties(asegurado1, contratante1);
			} catch (IllegalAccessException var8) {
				logger.error("Error ilegal al copiar propiedades", var8);
			} catch (InvocationTargetException var9) {
				logger.error("Error de invocacion al copiar propiedades", var9);
			}
			cotizacion.setAsegurado(asegurado1);
		}
		// FIN CONTRATACION TERCEROS
		
		// INICIO MASCOTA
		String numSubcategoria = form.getDatos().get("numSubcategoria") == null ? ""
				: (String) form.getDatos().get("numSubcategoria");
		
		if(numSubcategoria.equals("140")){
			logger.info("Datos mascota");
			cotizacion.setNumSubcategoria(numSubcategoria);
			cotizacion.setTipoMascota((String) form.getDatos().get("tipoMascota"));
			cotizacion.setRazaMascota((String) form.getDatos().get("raza"));
			cotizacion.setSexoMascota((String) form.getDatos().get("sexoMascota"));
			cotizacion.setEdadMascota((String) form.getDatos().get("edadMascota"));
		}
		// FIN MASCOTA

		return cotizacion;
	}
	
	private void obtenerDatosGeneralesCotizacionPaso1(
			CotizacionSeguro cotizacion, BuilderActionFormBaseCOT form) {
		if (cotizacion != null) {
			logger.info("Obtener datos generales");
			
			cotizacion.setRutCliente((long) Integer.parseInt((String) form
					.getDatos().get("rut")));
			cotizacion.setDv(((String) form.getDatos().get("dv")).charAt(0));
			cotizacion.setNombre((String) form.getDatos().get("nombre"));
			cotizacion.setApellidoMaterno((String) form.getDatos().get(
					"apellidoMaterno"));
			cotizacion.setApellidoPaterno((String) form.getDatos().get(
					"apellidoPaterno"));
			cotizacion.setEmail((String) form.getDatos().get("email"));
			cotizacion.setSexo((String) form.getDatos().get("sexo"));
			cotizacion.setEstadoCivil((String) form.getDatos().get(
					"estadoCivil"));
			cotizacion.setNumeroTelefono1((String) form.getDatos().get(
					"codigoTelefono")
					+ (String) form.getDatos().get("numeroTelefono"));
			cotizacion.setTipoTelefono1((String) form.getDatos().get(
					"tipoTelefono"));
			cotizacion.setTipoTelefono2((String) form.getDatos().get(
					"tipoTelefono2"));
			cotizacion.setDireccion((String) form.getDatos().get("direccion"));
			cotizacion.setNumeroDireccion((String) form.getDatos().get(
					"numeroDireccion"));
			cotizacion.setNumeroDepto((String) form.getDatos().get(
					"numeroDepto"));

			Date fechaNacimiento;
			try {
				String e = (String) form.getDatos().get("anyoFechaNacimiento");
				String dia = ((String) form.getDatos()
						.get("diaFechaNacimiento")).length() == 1 ? "0"
						.concat((String) form.getDatos().get(
								"diaFechaNacimiento")) : (String) form
						.getDatos().get("diaFechaNacimiento");
				String mes = ((String) form.getDatos()
						.get("mesFechaNacimiento")).length() == 1 ? "0"
						.concat((String) form.getDatos().get(
								"mesFechaNacimiento")) : (String) form
						.getDatos().get("mesFechaNacimiento");
				fechaNacimiento = (new SimpleDateFormat("dd-MM-yyyy"))
						.parse(dia + "-" + mes + "-" + e);
			} catch (ParseException var7) {
				throw new SystemException(var7);
			}

			cotizacion.setFechaNacimiento(fechaNacimiento);
		}

	}

	private long obtenerSleepTime(String producto, long combinaSeguros,
			String idProducto4, String idProducto6) {
		long sleepTime = 0L;
		if (("188".equals(producto) && "190".equals(idProducto4)
				|| "188".equals(producto) && "192".equals(idProducto6)
				|| "188".equals(producto) && "190".equals("1") || "188"
				.equals(producto) && "192".equals("1"))
				&& combinaSeguros <= 1L) {
			sleepTime = 50000L;
		} else if (("189".equals(producto) && "190".equals(idProducto4)
				|| "189".equals(producto) && "192".equals(idProducto6)
				|| "189".equals(producto) && "190".equals("1") || "189"
				.equals(producto) && "192".equals("1"))
				&& combinaSeguros <= 1L) {
			sleepTime = 34000L;
		} else if (("191".equals(producto) && "190".equals(idProducto4)
				|| "191".equals(producto) && "192".equals(idProducto6)
				|| "191".equals(producto) && "190".equals("1") || "191"
				.equals(producto) && "192".equals("1"))
				&& combinaSeguros <= 1L) {
			sleepTime = 48000L;
		} else if (("88".equals(producto) && "190".equals(idProducto4)
				|| "88".equals(producto) && "192".equals(idProducto6)
				|| "88".equals(producto) && "190".equals("1") || "88"
				.equals(producto) && "192".equals("1"))
				&& combinaSeguros <= 1L) {
			sleepTime = 33000L;
		} else if ("148".equals(producto)) {
			sleepTime = 3500L;
		} else if (combinaSeguros > 1L) {
			sleepTime = 50000L;
		} else {
			sleepTime = 3500L;
		}

		return sleepTime;
	}
}
