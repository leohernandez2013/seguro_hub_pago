// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:19
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import cl.cencosud.acv.common.Actividad;
import cl.cencosud.acv.common.EstadoCivil;
import cl.cencosud.acv.common.ParametroCotizacion;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DesplegarDatosCotizacionAction extends Action {

   public static final Log logger = LogFactory.getLog(DesplegarDatosCotizacionAction.class);


   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
      CotizacionSeguro cotizacionSeguro = (CotizacionSeguro)request.getSession().getAttribute("datosCotizacion");
      if(cotizacionSeguro instanceof CotizacionSeguroHogar) {
         CotizacionSeguroHogar oDelegate = (CotizacionSeguroHogar)cotizacionSeguro;
      } else if(cotizacionSeguro instanceof CotizacionSeguroVida) {
         CotizacionSeguroVida var30 = (CotizacionSeguroVida)cotizacionSeguro;
         var30.getBeneficiarios();
         var30.getAdicionales();
      } else if(cotizacionSeguro instanceof CotizacionSeguroVehiculo) {
         CotizacionSeguroVehiculo var29 = (CotizacionSeguroVehiculo)cotizacionSeguro;
      }

      CotizacionDelegate var31 = new CotizacionDelegate();
      String descSexo = "";
      String descEstadoCivil = "";
      String descActividad = "";
      String descRegion = "";
      String descComuna = "";
      String descCiudad = "";
      String sexo = cotizacionSeguro.getSexo();
      String estadoCivil = cotizacionSeguro.getEstadoCivil();
      String actividad = cotizacionSeguro.getActividad();
      String region = cotizacionSeguro.getRegion();
      String comuna = cotizacionSeguro.getComuna();
      String ciudad = cotizacionSeguro.getCiudad();
      Map sexoAux = var31.obtenerParametro("SEXO", sexo);
      descSexo = (String)sexoAux.get("descripcion");
      EstadoCivil[] estadosCiviles = var31.obtenerEstadosCiviles();
      int comunas;
      if(estadosCiviles != null) {
         EstadoCivil[] actividades = estadosCiviles;
         int regiones = estadosCiviles.length;

         for(comunas = 0; comunas < regiones; ++comunas) {
            EstadoCivil ciudades = actividades[comunas];
            if(ciudades.getId().equalsIgnoreCase(estadoCivil)) {
               descEstadoCivil = ciudades.getDescripcion();
            }
         }
      }

      Actividad[] var34 = var31.obtenerActividades();
      int var38;
      if(var34 != null) {
         Actividad[] var32 = var34;
         comunas = var34.length;

         for(var38 = 0; var38 < comunas; ++var38) {
            Actividad arr$ = var32[var38];
            if(arr$.getId().equalsIgnoreCase(actividad)) {
               descActividad = arr$.getDescripcion();
            }
         }
      }

      ParametroCotizacion[] var33 = var31.obtenerRegiones();
      int var35;
      ParametroCotizacion[] var37;
      if(var33 != null) {
         var37 = var33;
         var38 = var33.length;

         for(var35 = 0; var35 < var38; ++var35) {
            ParametroCotizacion len$ = var37[var35];
            if(len$.getId().equalsIgnoreCase(region)) {
               descRegion = len$.getDescripcion();
            }
         }
      }

      var37 = var31.obtenerComunas(region);
      int var39;
      ParametroCotizacion[] var36;
      if(var37 != null) {
         var36 = var37;
         var35 = var37.length;

         for(var39 = 0; var39 < var35; ++var39) {
            ParametroCotizacion i$ = var36[var39];
            if(i$.getId().equalsIgnoreCase(comuna)) {
               descComuna = i$.getDescripcion();
            }
         }
      }

      var36 = var31.obtenerCiudades(comuna);
      if(var36 != null) {
         ParametroCotizacion[] var40 = var36;
         var39 = var36.length;

         for(int var41 = 0; var41 < var39; ++var41) {
            ParametroCotizacion ciudadAux = var40[var41];
            if(ciudadAux.getId().equalsIgnoreCase(ciudad)) {
               descCiudad = ciudadAux.getDescripcion();
            }
         }
      }

      request.setAttribute("descSexo", descSexo);
      request.setAttribute("descEstadoCivil", descEstadoCivil);
      request.setAttribute("descActividad", descActividad);
      request.setAttribute("descRegion", descRegion);
      request.setAttribute("descComuna", descComuna);
      request.setAttribute("descCiudad", descCiudad);
      return mapping.findForward("continuar");
   }

}
