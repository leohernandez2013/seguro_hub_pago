// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:19
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.acv.common.Asegurado;
import cl.cencosud.acv.common.ColorVehiculo;
import cl.cencosud.acv.common.Contratante;
import cl.cencosud.acv.common.EstadoCivil;
import cl.cencosud.acv.common.ParametroCotizacion;
import cl.cencosud.acv.common.Plan;
import cl.cencosud.acv.common.RestriccionVida;
import cl.cencosud.acv.common.Solicitud;
import cl.cencosud.acv.common.Vehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.cencosud.ventaseguros.common.Parametro;
import cl.tinet.common.model.exception.BusinessException;
import cl.tinet.common.seguridad.model.UsuarioExterno;
import cl.tinet.common.struts.form.BuilderActionFormBaseCOT;

public class DesplegarIngresoDatosAction extends Action {

   private static final String LOGIN_USUARIO = "cl.tinet.common.seguridad.USUARIO_CONECTADO";
   private static final String GRUPO_SIN_TEXTO_PROVISORIO = "SUBCAT_SINTEXTO_PROV";
   private static final int facturaDias = 2;


   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws BusinessException {
      CotizacionDelegate oDelegate = new CotizacionDelegate();
      List tipoTelefono = oDelegate.obtenerParametro("TIPO_TELEFONO");
      request.setAttribute("tipoTelefono", tipoTelefono);
      List codigoArea = oDelegate.obtenerParametro("TEL_CODIGO_AREA");
      request.setAttribute("codigoArea", codigoArea);
      ParametroCotizacion[] regiones = oDelegate.obtenerRegiones();
      request.setAttribute("regiones", regiones);
      List mesFecha = oDelegate.obtenerParametro("MESES");
      request.setAttribute("mesFecha", mesFecha);
      List anyosFecha = oDelegate.obtenerParametro("ANYOS");
      request.setAttribute("anyosFecha", anyosFecha);
      List sexo = oDelegate.obtenerParametro("SEXO");
      request.setAttribute("sexo", sexo);
      EstadoCivil[] estadoCivil = oDelegate.obtenerEstadosCiviles();
      request.setAttribute("estadosCiviles", estadoCivil);
      ColorVehiculo[] colores = oDelegate.obtenerColoresVehiculo();
      request.setAttribute("colorVehiculo", colores);
      long idSolicitud = Long.valueOf(String.valueOf(request.getSession().getAttribute("idSolicitud"))).longValue();
      Solicitud solicitud = oDelegate.obtenerDatosSolicitud(idSolicitud);
      if(solicitud.getCliente_es_asegurado() > 0L) {
         request.setAttribute("esAsegurado", "true");
      }

      Plan plan = (Plan)request.getSession().getAttribute("datosPlan");
      CotizacionSeguro oCotizacion = (CotizacionSeguro)request.getSession().getAttribute("datosCotizacion");
      Map esPrimaUnica;
      if(oCotizacion.getRegion() != null) {
         esPrimaUnica = oDelegate.obtenerRegionPorId(oCotizacion.getRegion());
         request.setAttribute("regionDescripcion", esPrimaUnica.get("descripcion_region"));
      }

      if(oCotizacion.getCiudad() != null) {
         esPrimaUnica = oDelegate.obtenerCiudadPorId(oCotizacion.getCiudad());
         request.setAttribute("ciudadDescripcion", esPrimaUnica.get("descripcion_ciudad"));
      }

      if(oCotizacion.getComuna() != null) {
         esPrimaUnica = oDelegate.obtenerComunaPorId(oCotizacion.getComuna());
         request.setAttribute("comunaDescripcion", esPrimaUnica.get("descripcion_comuna"));
      }

      if(oCotizacion instanceof CotizacionSeguroVida) {
         RestriccionVida var37 = oDelegate.obtenerRestriccionVida(Long.parseLong(plan.getIdPlan()));
         if(var37 != null) {
            request.setAttribute("restricciones", var37);
            request.setAttribute("parentesco", var37.getParentescos());
         }      
      }

      String var36;
      if(request.getSession().getAttribute("datosCotizacion") != null) {
         if(request.getSession().getAttribute("datosCotizacion") instanceof CotizacionSeguroVehiculo) {
            CotizacionSeguroVehiculo var35 = (CotizacionSeguroVehiculo)request.getSession().getAttribute("datosCotizacion");
            Vehiculo[] telefono1 = oDelegate.obtenerTiposVehiculos();
            Vehiculo[] ciudadCte = oDelegate.obtenerMarcasPorTipoVehiculos(String.valueOf(var35.getCodigoTipoVehiculo()));
            Vehiculo[] calleCte = oDelegate.obtenerModelosVehiculos(String.valueOf(var35.getCodigoTipoVehiculo()), String.valueOf(var35.getCodigoMarca()));

            int numeroCalleCte;
            for(numeroCalleCte = 0; numeroCalleCte < telefono1.length; ++numeroCalleCte) {
               if(telefono1[numeroCalleCte].getIdTipoVehiculo().equals(String.valueOf(var35.getCodigoTipoVehiculo()))) {
                  request.setAttribute("tipoDesc", telefono1[numeroCalleCte].getDescripcionTipoVehiculo());
               }
            }

            for(numeroCalleCte = 0; numeroCalleCte < ciudadCte.length; ++numeroCalleCte) {
               if(ciudadCte[numeroCalleCte].getIdMarcaVehiculo().equals(String.valueOf(var35.getCodigoMarca()))) {
                  request.setAttribute("marcaDesc", ciudadCte[numeroCalleCte].getMarcaVehiculo());
               }
            }

            for(numeroCalleCte = 0; numeroCalleCte < calleCte.length; ++numeroCalleCte) {
               if(calleCte[numeroCalleCte].getIdModeloVehiculo().equals(String.valueOf(var35.getCodigoModelo()))) {
                  request.setAttribute("modeloDes", calleCte[numeroCalleCte].getModeloVehiculo());
               }
            }

            if(var35.getContratante().getIdRegion() != null && var35.getContratante().getIdRegion().length() > 0) {
               ((BuilderActionFormBaseCOT)form).getDatos().put("cteRegion", var35.getContratante().getIdRegion());
            }

            if(var35.getContratante().getIdComuna() != null && var35.getContratante().getIdComuna().length() > 0) {
               ((BuilderActionFormBaseCOT)form).getDatos().put("cteComuna", var35.getContratante().getIdComuna());
            }

            List var42 = oDelegate.obtenerParametro("SUBCAT_SINTEXTO_PROV");
            Iterator numeroDeptoCte = var42.iterator();

            while(numeroDeptoCte.hasNext()) {
               String currentDate = ((Map)numeroDeptoCte.next()).get("VALOR").toString();
               if(currentDate.equals(request.getSession().getAttribute("idSubcategoria"))) {
                  request.setAttribute("sinTextoProvisorio", "true");
               }
            }

            List var45 = oDelegate.obtenerParametro("ANYOFACTURA");
            request.setAttribute("anyosVehiculo", var45);
            Calendar var43 = Calendar.getInstance();
            Calendar pastDate = (Calendar)var43.clone();
            pastDate.add(5, -2);
            HashMap day = new HashMap();
            HashMap month = new HashMap();
            HashMap year = new HashMap();
            boolean d = false;
            boolean m = false;
            boolean y = false;

            while(var43.compareTo(pastDate) >= 0) {
               int var47 = pastDate.get(5);
               int var48 = pastDate.get(2) + 1;
               int var49 = pastDate.get(1);
               day.put(Integer.valueOf(var47), Integer.valueOf(var47));
               month.put(Integer.valueOf(var48), Integer.valueOf(var48));
               year.put(Integer.valueOf(var49), Integer.valueOf(var49));
               pastDate.add(5, 1);
            }

            request.setAttribute("diasVehiculo", day);
            request.setAttribute("mesesVehiculo", month);
            request.setAttribute("anyosVehiculo", year);
            Map tipoInspeccion = oDelegate.obtenerParametro("PASO_2_VEH", "EXISTE_TIPO_INSPECCION");
            if(tipoInspeccion != null) {
               request.setAttribute("existeTipoInspeccion", tipoInspeccion.get("valor"));
            }

            request.setAttribute("solicitaInspeccion", Integer.valueOf(plan.getInspeccionVehi()));
            request.setAttribute("solicitaAutoNuevo", plan.getSolicitaAutoNuevo());
            request.setAttribute("solicitaFactura", plan.getSolicitaFactura());
         }

         if(request.getSession().getAttribute("datosCotizacion") instanceof CotizacionSeguroHogar) {
            String var34 = (String)((BuilderActionFormBaseCOT)form).getDatos().get("cteRegion");
            if(var34 == null || var34.equals("")) {
               ((BuilderActionFormBaseCOT)form).getDatos().put("cteRegion", oCotizacion.getRegion());
            }

            var36 = (String)((BuilderActionFormBaseCOT)form).getDatos().get("cteComuna");
            if(var36 == null || var36.equals("")) {
               ((BuilderActionFormBaseCOT)form).getDatos().put("cteComuna", oCotizacion.getComuna());
            }

            String var40 = (String)((BuilderActionFormBaseCOT)form).getDatos().get("cteCiudad");
            if(var40 == null || var40.equals("")) {
               ((BuilderActionFormBaseCOT)form).getDatos().put("cteCiudad", oCotizacion.getCiudad());
            }

            String var38 = (String)((BuilderActionFormBaseCOT)form).getDatos().get("cteCalle");
            if(var38 == null || var38.equals("")) {
               ((BuilderActionFormBaseCOT)form).getDatos().put("cteCalle", oCotizacion.getDireccion());
            }

            String var44 = (String)((BuilderActionFormBaseCOT)form).getDatos().get("cteNumeroCalle");
            if(var44 == null || var44.equals("")) {
               ((BuilderActionFormBaseCOT)form).getDatos().put("cteNumeroCalle", oCotizacion.getNumeroDireccion());
            }

            String var46 = (String)((BuilderActionFormBaseCOT)form).getDatos().get("cteNumeroDepto");
            if(var46 == null || var46.equals("")) {
               ((BuilderActionFormBaseCOT)form).getDatos().put("cteNumeroDepto", oCotizacion.getNumeroDepto());
            }
         }
         
         
         if(oCotizacion instanceof CotizacionSeguroVida) {
        	 CotizacionSeguroVida oCotizacionVida = (CotizacionSeguroVida)request.getSession().getAttribute("datosCotizacion");
             Calendar fecha = Calendar.getInstance();
             Integer a�oActual = fecha.get(Calendar.YEAR);
             request.setAttribute("a�oActual", a�oActual.toString());
             
             Contratante asegurado = new Contratante();
             if(oCotizacionVida != null && oCotizacionVida.getAsegurado() != null){
            	 asegurado = oCotizacionVida.getAsegurado();
            	 request.setAttribute("aseguradoRut", asegurado.getRut()+asegurado.getDv());
            	 request.setAttribute("aseguradoNombre", asegurado.getNombre());
            	 request.setAttribute("aseguradoApellidoP", asegurado.getApellidoPaterno());
            	 request.setAttribute("aseguradoApellidoM", asegurado.getApellidoMaterno());
            	 SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            	 String fechaNacimiento = formatter.format(asegurado.getFechaDeNacimiento());
            	 request.setAttribute("aseguradoFechaNacimiento", fechaNacimiento);
            	 request.setAttribute("aseguradoSexo", asegurado.getSexo());
             }
             
             
             if (oCotizacionVida.isClienteEsAsegurado()){
            	 ((BuilderActionFormBaseCOT)form).getDatos().put("clienteEsAsegurado", oCotizacionVida.isClienteEsAsegurado());
                 request.setAttribute("contratanteAsegurado", "1");
                 System.out.println("Asegurado viaja? si");
             } else{
            	 request.setAttribute("contratanteAsegurado", "1");
            	 System.out.println("Asegurado viaja? no");
             }
             
             if(oCotizacionVida.getIntegrante() != null && !("").equals(oCotizacionVida.getIntegrante())) {
                 ((BuilderActionFormBaseCOT)form).getDatos().put("integrante", oCotizacionVida.getIntegrante());
                 request.setAttribute("integrantes", oCotizacionVida.getIntegrante());
                 System.out.println("numero de integrantes "+oCotizacionVida.getIntegrante());
                 
              }
             
             if(oCotizacionVida.getRegion() != null && !oCotizacionVida.getRegion().equals("")) {
                ((BuilderActionFormBaseCOT)form).getDatos().put("cteRegion", oCotizacionVida.getRegion());
             }
             
             if(oCotizacionVida.getComuna() != null && !oCotizacionVida.getComuna().equals("")) {
                ((BuilderActionFormBaseCOT)form).getDatos().put("cteComuna", oCotizacionVida.getComuna());
             }

             
             if(oCotizacionVida.getCiudad() != null && !oCotizacionVida.getCiudad().equals("")) {
                ((BuilderActionFormBaseCOT)form).getDatos().put("cteCiudad", oCotizacionVida.getCiudad());
             }
             
             if(oCotizacionVida.getDireccion() != null && !oCotizacionVida.getDireccion().equals("")) {
                ((BuilderActionFormBaseCOT)form).getDatos().put("cteCalle", oCotizacionVida.getDireccion());
             }

             if(oCotizacionVida.getNumeroDireccion() != null && !oCotizacionVida.getNumeroDireccion().equals("")) {
                ((BuilderActionFormBaseCOT)form).getDatos().put("cteNumeroCalle", oCotizacionVida.getNumeroDireccion());
             }

             if(oCotizacionVida.getNumeroDepto() != null && !oCotizacionVida.getNumeroDepto().equals("")) {
                ((BuilderActionFormBaseCOT)form).getDatos().put("cteNumeroDepto", oCotizacionVida.getNumeroDepto());
             }       
             
             
             if(oCotizacionVida.getFecIniVig() != null && !oCotizacionVida.getFecIniVig().equals("")) {
                 ((BuilderActionFormBaseCOT)form).getDatos().put("fechaInicio", oCotizacionVida.getFecIniVig());
             }       
              
          }
         
      }

      if(request.getSession().getAttribute("cl.tinet.common.seguridad.USUARIO_CONECTADO") != null && 
    		    request.getSession().getAttribute("paso2") == null) {
         ((BuilderActionFormBaseCOT)form).getDatos().putAll(this.obtenerDatosClienteSessionPaso2(request, ((BuilderActionFormBaseCOT)form).getDatos()));
      } else {
         CotizacionSeguro var39 = (CotizacionSeguro)request.getSession().getAttribute("datosCotizacion");
         if(((BuilderActionFormBaseCOT)form).getDatos().get("esNuevo") == null) {
            ((BuilderActionFormBaseCOT)form).getDatos().put("esNuevo", "0");
         }

         var36 = var39.getNumeroTelefono1();
         if(var36 != null) {
            var36 = var36.trim();
            if(var36.substring(0, 1).equals("2")) {
               ((BuilderActionFormBaseCOT)form).getDatos().put("cteCodigoTelefono1", "02");
               ((BuilderActionFormBaseCOT)form).getDatos().put("cteNumeroTelefono1", var36.substring(1, var36.length()));
               ((BuilderActionFormBaseCOT)form).getDatos().put("codigoTelefono1", "02");
               ((BuilderActionFormBaseCOT)form).getDatos().put("numeroTelefono1", var36.substring(1, var36.length()));
            } else {
               ((BuilderActionFormBaseCOT)form).getDatos().put("cteCodigoTelefono1", var36.substring(0, 2));
               ((BuilderActionFormBaseCOT)form).getDatos().put("cteNumeroTelefono1", var36.substring(2, var36.length()));
               ((BuilderActionFormBaseCOT)form).getDatos().put("codigoTelefono1", var36.substring(0, 2));
               ((BuilderActionFormBaseCOT)form).getDatos().put("numeroTelefono1", var36.substring(2, var36.length()));
            }
         }
         

         String numeroTelefono2 = var39.getNumeroTelefono2();
         
         if(numeroTelefono2 != null) {
        	 numeroTelefono2 = numeroTelefono2.trim();
            if(numeroTelefono2.substring(0, 1).equals("2")) {
               ((BuilderActionFormBaseCOT)form).getDatos().put("codigoTelefono2", "02");
               ((BuilderActionFormBaseCOT)form).getDatos().put("numeroTelefono2", numeroTelefono2.substring(1, numeroTelefono2.length()));
            } else {
               ((BuilderActionFormBaseCOT)form).getDatos().put("codigoTelefono2", numeroTelefono2.substring(0, 2));
               ((BuilderActionFormBaseCOT)form).getDatos().put("numeroTelefono2", numeroTelefono2.substring(2, numeroTelefono2.length()));
            }
         }       

         ((BuilderActionFormBaseCOT)form).getDatos().put("cteTipoTelefono1", var39.getTipoTelefono1());
         ((BuilderActionFormBaseCOT)form).getDatos().put("tipoTelefono1", var39.getTipoTelefono1());
         ((BuilderActionFormBaseCOT)form).getDatos().put("contactoNombre", var39.getNombre());
         ((BuilderActionFormBaseCOT)form).getDatos().put("contactoApellidoPaterno", var39.getApellidoPaterno());
         ((BuilderActionFormBaseCOT)form).getDatos().put("contactoApellidoMaterno", var39.getApellidoMaterno());
         ((BuilderActionFormBaseCOT)form).getDatos().put("tipoTelefono2", var39.getTipoTelefono2());
      }

      ((BuilderActionFormBaseCOT)form).getDatos().put("tipoTelefono1", ((BuilderActionFormBaseCOT)form).getDatos().get("cteTipoTelefono1"));
      ((BuilderActionFormBaseCOT)form).getDatos().put("codigoTelefono1", ((BuilderActionFormBaseCOT)form).getDatos().get("cteCodigoTelefono1"));
      ((BuilderActionFormBaseCOT)form).getDatos().put("numeroTelefono1", ((BuilderActionFormBaseCOT)form).getDatos().get("cteNumeroTelefono1"));
      ((BuilderActionFormBaseCOT)form).getDatos().put("region", ((BuilderActionFormBaseCOT)form).getDatos().get("cteRegion"));
      ((BuilderActionFormBaseCOT)form).getDatos().put("comuna", ((BuilderActionFormBaseCOT)form).getDatos().get("cteComuna"));
      ((BuilderActionFormBaseCOT)form).getDatos().put("ciudad", ((BuilderActionFormBaseCOT)form).getDatos().get("cteCiudad"));
      ((BuilderActionFormBaseCOT)form).getDatos().put("direccion", ((BuilderActionFormBaseCOT)form).getDatos().get("cteCalle"));
      ((BuilderActionFormBaseCOT)form).getDatos().put("direccionNumero", ((BuilderActionFormBaseCOT)form).getDatos().get("cteNumeroCalle"));
      ((BuilderActionFormBaseCOT)form).getDatos().put("direccionNroDepto", ((BuilderActionFormBaseCOT)form).getDatos().get("cteNumeroDepto"));
      ((BuilderActionFormBaseCOT)form).getDatos().put("dueRegion", oCotizacion.getAsegurado().getIdRegion());
      ((BuilderActionFormBaseCOT)form).getDatos().put("dueComuna", oCotizacion.getAsegurado().getIdComuna());
      if(request.getParameter("volverPaso2") != null && request.getSession().getAttribute("datosVolverPaso2") != null) {
         ((BuilderActionFormBaseCOT)form).getDatos().putAll((HashMap)request.getSession().getAttribute("datosVolverPaso2"));
         request.getSession().removeAttribute("datosVolverPaso2");
      }

      boolean var41 = oDelegate.existePrimaUnica((new Long(plan.getIdPlan())).longValue());
      if(var41) {
         request.setAttribute("esPrimaUnica", Boolean.valueOf(var41));
      }

      request.setAttribute("paso2", "1");
      return mapping.findForward("continuar");
   }

   private HashMap obtenerDatosClienteSessionPaso2(HttpServletRequest request, Map datosOrig) {
      HashMap datos = null;
      HttpSession session = request.getSession();
      if(session != null && session.getAttribute("cl.tinet.common.seguridad.USUARIO_CONECTADO") != null) {
         datos = new HashMap();
         UsuarioExterno usuario = (UsuarioExterno)session.getAttribute("cl.tinet.common.seguridad.USUARIO_CONECTADO");
         String telefono1 = usuario.getTelefono_1();
         if(telefono1 != null) {
            telefono1 = telefono1.trim();
            if(telefono1.substring(0, 1).equals("2")) {
               datos.put("cteCodigoTelefono1", "02");
               datos.put("cteNumeroTelefono1", telefono1.substring(1, telefono1.length()));
            } else {
               datos.put("cteCodigoTelefono1", telefono1.substring(0, 2));
               datos.put("cteNumeroTelefono1", telefono1.substring(2, telefono1.length()));
            }
         }

         datos.put("cteTipoTelefono1", usuario.getTipo_telefono_1());
         datos.put("cteCalle", usuario.getCalle());
         datos.put("cteNumeroCalle", usuario.getNumero());
         datos.put("cteNumeroDepto", usuario.getNumero_departamento());
         datos.put("direccion", (String)datosOrig.get("cteCalle"));
         datos.put("direccionNumero", (String)datosOrig.get("cteNumeroCalle"));
         datos.put("direccionNroDepto", (String)datosOrig.get("cteNumeroDepto"));
         datos.put("codigoTelefono1", (String)datosOrig.get("cteCodigoTelefono1"));
         datos.put("numeroTelefono1", (String)datosOrig.get("cteNumeroTelefono1"));
         datos.put("tipoTelefono1", (String)datosOrig.get("cteTipoTelefono1"));
         CotizacionDelegate oDelegate = new CotizacionDelegate();
         Parametro[] comuna = oDelegate.obtenerComunaPorCiudad(usuario.getId_ciudad());
         datos.put("cteCiudad", usuario.getId_ciudad());
         if(comuna != null && comuna.length > 0) {
            Parametro[] telefonoContacto = oDelegate.obtenerRegionPorComuna(comuna[0].getId());
            datos.put("cteComuna", comuna[0].getId());
            if(telefonoContacto != null && telefonoContacto.length > 0) {
               datos.put("cteRegion", telefonoContacto[0].getId());
               datos.put("region", (String)datosOrig.get("cteRegion"));
               datos.put("comuna", (String)datosOrig.get("cteComuna"));
               datos.put("ciudad", (String)datosOrig.get("cteCiudad"));
            }
         }

         datos.put("contactoNombre", usuario.getNombre());
         datos.put("contactoApellidoPaterno", usuario.getApellido_paterno());
         datos.put("contactoApellidoMaterno", usuario.getApellido_materno());
         String telefonoContacto1 = usuario.getTelefono_1();
         if(telefonoContacto1 != null) {
            telefonoContacto1 = telefonoContacto1.trim();
            if(telefonoContacto1.substring(0, 1).equals("2")) {
               datos.put("contactoCodigoTelefono1", "02");
               datos.put("contactoNumeroTelefono1", telefonoContacto1.substring(1, telefonoContacto1.length()));
            } else {
               datos.put("contactoCodigoTelefono1", telefonoContacto1.substring(0, 2));
               datos.put("contactoNumeroTelefono1", telefonoContacto1.substring(2, telefonoContacto1.length()));
            }
         }

         datos.put("contactoTipoTelefono1", usuario.getTipo_telefono_1());
         if(datosOrig.get("esNuevo") == null) {
            datos.put("esNuevo", "0");
         }

         if(datosOrig.get("tipoInspeccion") == null) {
            datos.put("tipoInspeccion", "1");
         }

         if(datosOrig.get("esParticular") == null) {
            datos.put("esParticular", "1");
         }

         if(request.getSession().getAttribute("datosCotizacion") instanceof CotizacionSeguroVehiculo) {
            CotizacionSeguroVehiculo cotVehiculo = (CotizacionSeguroVehiculo)request.getSession().getAttribute("datosCotizacion");
            if(!cotVehiculo.isClienteEsAsegurado()) {
               datos.put("dueRegion", cotVehiculo.getAsegurado().getIdRegion());
               datos.put("dueComuna", cotVehiculo.getAsegurado().getIdComuna());
            }
         }
      }

      return datos;
   }
}
