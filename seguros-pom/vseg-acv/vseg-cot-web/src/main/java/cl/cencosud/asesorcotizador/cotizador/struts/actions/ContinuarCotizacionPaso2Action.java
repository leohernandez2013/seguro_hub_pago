// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:20
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.tinet.common.model.exception.BusinessException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ContinuarCotizacionPaso2Action extends Action {

   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws BusinessException {
      String forward = "continuar-vida";
      CotizacionSeguro cot = (CotizacionSeguro)request.getSession().getAttribute("datosCotizacion");
      if(cot instanceof CotizacionSeguroVehiculo) {
         forward = "continuar-vehiculo";
      } else if(cot instanceof CotizacionSeguroHogar) {
         forward = "continuar-hogar";
      }

      return mapping.findForward(forward);
   }
}
