package cl.cencosud.asesorcotizador.cotizador.paso3.struts.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class WebpayTestAction extends Action{
	
	/**
	 * 
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		 
		Integer nroRequest =(Integer) (request.getSession().getAttribute("nroRequest") == null ?  0: request.getSession().getAttribute("nroRequest"));
		 
		 if(0 == nroRequest){
			 System.out.println("Primera Ejecución");
			 request.getSession().setAttribute("nroRequest",1);
		 
		 }else if (nroRequest == 1){
			 System.out.println("Segunda ejecución");
			 request.getSession().setAttribute("nroRequest",0);
		 }
		 
		 System.out.println(nroRequest);
		
		return mapping.findForward("exito");
		
	}

}
