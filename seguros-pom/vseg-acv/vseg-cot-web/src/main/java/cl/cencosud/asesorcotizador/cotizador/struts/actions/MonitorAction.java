// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:19
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import cl.cencosud.acv.common.ParametroCotizacion;
import cl.cencosud.acv.common.config.ACVConfig;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.cencosud.ventaseguros.common.config.VSPConfig;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class MonitorAction extends Action {

   private static Log logger = LogFactory.getLog(MonitorAction.class);
   private static final long CONST_MILIS = 1000L;
   private static final long CONST_HOUR = 60L;
   private static final long CONST_MIN = 60L;
   private static final String WSDL_INSPECCION_BSP = "cl.cencosud.asesorcotizador.dao.ws.SolicitudInspeccionDAOWS.wsdl.solicitud.inspeccion";
   private static final String WSDL_ICOMERCIAL = "cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWS.wsdl.verificacionCedula";


   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
      logger.debug("Iniciando monitoreo...");
      request.setAttribute("inicio", (new GregorianCalendar()).getTime());
      this.obtenerRegiones(request);
      this.obtenerPolizaHtml(request);
      this.validarBSP(request);
      this.validarEquifax(request);
      request.setAttribute("fin", (new GregorianCalendar()).getTime());
      logger.debug("Fin de monitoreo.");
      this.calcularTiempo(request);
      return mapping.findForward("continue");
   }

   private void obtenerRegiones(HttpServletRequest request) {
      CotizacionDelegate delegate = new CotizacionDelegate();
      logger.debug("Obtener regiones");

      try {
         ParametroCotizacion[] e = new ParametroCotizacion[0];
         e = delegate.obtenerRegiones();
         logger.debug("Regiones obtenidas: " + e);
         request.setAttribute("regiones", e);
      } catch (Exception var4) {
         logger.error("Error al obtener regiones", var4);
      }

   }

   private void obtenerPolizaHtml(HttpServletRequest request) {
      CotizacionDelegate delegate = new CotizacionDelegate();
      logger.debug("Obtener poliza HTML");
      byte[] poliza = (byte[])null;

      try {
         int e = 15364288;
         int numeroSolicitud = 6919223; // PLAN MAGA FULLSERVICIO PLUS UF03DED
         poliza = delegate.obtenerPolizaHtml(e, numeroSolicitud);
         if(poliza.length > 0) {
            logger.debug("Poliza obtenida");
            request.setAttribute("poliza", new String(poliza));
         }

         poliza = (byte[])null;
      } catch (Exception var9) {
         logger.error("Error al obtener poliza html", var9);
      } finally {
         poliza = (byte[])null;
      }

   }

   private void validarBSP(HttpServletRequest request) {
      logger.debug("Validar acceso a BSP");

      try {
         String e = ACVConfig.getInstance().getString("cl.cencosud.asesorcotizador.dao.ws.SolicitudInspeccionDAOWS.wsdl.solicitud.inspeccion");
         logger.info("URL BSP :" + e);
         StringBuffer pagina = new StringBuffer("");
         URL url = new URL(e);
         BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));

         String line;
         while((line = reader.readLine()) != null) {
            pagina.append(line);
         }

         reader.close();
         request.setAttribute("paginaBSP", pagina);
         logger.debug("URL BSP accesada");
      } catch (MalformedURLException var7) {
         request.setAttribute("errorBSP", var7.getLocalizedMessage());
         logger.error("Error al accesar WS de BSP", var7);
      } catch (IOException var8) {
         request.setAttribute("errorBSP", var8.getLocalizedMessage());
         logger.error("Error al accesar WS de BSP", var8);
      } catch (Exception var9) {
         request.setAttribute("errorBSP", "Error general al accesar el WebService");
         logger.error("Error al accesar WS de BSP", var9);
      }

   }

   private void validarEquifax(HttpServletRequest request) {
      logger.info("Validar acceso a SIISA");

      try {
         String e = VSPConfig.getInstance().getString("cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWS.wsdl.verificacionCedula");
         logger.info("WSDL_ICOMERCIAL:cl.cencosud.ventaseguros.mantenedorclientes.dao.ws.MantenedorClientesWSDAOWS.wsdl.verificacionCedula");
         logger.info("URL:" + e);
         StringBuffer pagina = new StringBuffer("");
         URL url = new URL(e);
         BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));

         String line;
         while((line = reader.readLine()) != null) {
            pagina.append(line);
         }

         reader.close();
         request.setAttribute("paginaEQUIFAX", pagina);
         logger.info("URL SIISA accesada");
      } catch (MalformedURLException var7) {
         request.setAttribute("errorEQUIFAX", var7.getLocalizedMessage());
         logger.info("Error al accesar WS de SIISA", var7);
      } catch (IOException var8) {
         request.setAttribute("errorEQUIFAX", var8.getLocalizedMessage());
         logger.info("Error al accesar WS de SIISA", var8);
      } catch (Exception var9) {
         request.setAttribute("errorEQUIFAX", "Error general al accesar el WebService");
         logger.info("Error al accesar WS de SIISA", var9);
      }

   }

   private void calcularTiempo(HttpServletRequest request) {
      Date inicio = (Date)request.getAttribute("inicio");
      Date fin = (Date)request.getAttribute("fin");
      long diff = fin.getTime() - inicio.getTime();
      long horas = diff / 3600000L;
      long minutos = diff / 60000L;
      long segundos = diff / 1000L;
      request.setAttribute("horas", Long.valueOf(horas));
      request.setAttribute("minutos", Long.valueOf(minutos));
      request.setAttribute("segundos", Long.valueOf(segundos));
      long milis = diff - segundos * 1000L;
      request.setAttribute("milis", Long.valueOf(milis));
   }
}
