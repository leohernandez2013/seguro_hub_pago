package cl.cencosud.asesorcotizador.registrocliente.struts.action;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.acv.common.Beneficiario;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida;
import cl.cencosud.asesorcotizador.delegate.MantenedorClientesDelegateRemote;
import cl.cencosud.asesorcotizador.registrocliente.struts.forms.RegistroClienteForm;
import cl.cencosud.ventaseguros.common.EstadoCivil;
import cl.cencosud.ventaseguros.common.Parametro;




public class RegistroDeClienteAction extends Action {
	
	private static final Log logger = LogFactory.getLog(RegistroDeClienteAction.class);

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		RegistroClienteForm oForm = (RegistroClienteForm)form;
		
		String rutCliente = request.getParameter("rut_cliente");
		String dvCliente = request.getParameter("dv_cliente");
		String val_diaFechaNacimiento = request.getParameter("diaFechaNacimiento");
		String val_mesFechaNacimiento = request.getParameter("mesFechaNacimiento");
		String val_anoFechaNacimiento = request.getParameter("anoFechaNacimiento");
		String val_sexo = request.getParameter("val_sexo");
		String val_estadoCivil = request.getParameter("val_estadoCivil");
		String val_nombre = request.getParameter("val_nombre");
		String val_apellidoP = request.getParameter("val_apellidoP");
		String val_apellidoM = request.getParameter("val_apellidoM");
		String tipo_telefono = request.getParameter("val_tipo_cel");
		String codigo_telefono = request.getParameter("val_codigo_cel");
		String numero_telefono = request.getParameter("val_numero_cel");
		String val_email = request.getParameter("val_email");
		String tipo_telefono2 = request.getParameter("val_tipo_cel2");
		String codigo_telefono2 = request.getParameter("val_codigo_cel2");
		String numero_telefono2 = request.getParameter("val_numero_cel2");
		String cteRegion = request.getParameter("val_region");
		String cteComuna = request.getParameter("val_comuna");
		String cteCiudad = request.getParameter("val_ciudad");
		String cteCalle = request.getParameter("val_direccion");
		String cteNumeroCalle = request.getParameter("val_direccion_num");
		String cteNumeroDepto = request.getParameter("val_direccion_dep");
		String arrayBeneficiarios = request.getParameter("val_beneficiarios");
		String fechaInicio = request.getParameter("val_fecha_inicio");
		
		MantenedorClientesDelegateRemote delegate = new MantenedorClientesDelegateRemote();
		
		Map<String, String> datos = oForm.getDatos();
		datos.put("maxDias", "31");
		
		if (rutCliente != null && dvCliente != null && !rutCliente.equals("") && !dvCliente.equals("")) {
			datos.put("rut_cliente", rutCliente);
			datos.put("dv_cliente", dvCliente);
			request.setAttribute("sinRut", "false");
		}
		
		if (val_diaFechaNacimiento != null && val_mesFechaNacimiento != null && val_anoFechaNacimiento != null && !val_diaFechaNacimiento.equals("") && !val_mesFechaNacimiento.equals("") && !val_anoFechaNacimiento.equals("")) {
			datos.put("val_diaFechaNacimiento", Integer.valueOf(val_diaFechaNacimiento).toString());
			datos.put("val_mesFechaNacimiento", Integer.valueOf(val_mesFechaNacimiento).toString());
			datos.put("val_anoFechaNacimiento", val_anoFechaNacimiento);
		}	
		
		if (val_sexo != null && !val_sexo.equals("")) {
			datos.put("val_sexo", val_sexo);
		}	
		
		if (val_estadoCivil != null && !val_estadoCivil.equals("")) {
			datos.put("val_estadoCivil", val_estadoCivil);
		}		
		
		if (val_nombre != null && !val_nombre.equals("") ) {
			datos.put("val_nombre", val_nombre);
		}	
		
		if (val_apellidoP != null && !val_apellidoP.equals("")) {
			datos.put("val_apellidoP", val_apellidoP);
		}	
		
		if (val_apellidoM != null && !val_apellidoM.equals("")) {
			datos.put("val_apellidoM", val_apellidoM);
		}			
				
		if (tipo_telefono != null && codigo_telefono != null && numero_telefono != null && !tipo_telefono.equals("") && !codigo_telefono.equals("") && !numero_telefono.equals("")) {
			datos.put("tipo_telefono_1", tipo_telefono);
			datos.put("codigo_area", codigo_telefono);
			datos.put("telefono_1", numero_telefono);
		}				
		
		
		if (val_email != null && !val_email.equals("")) {
			datos.put("val_email", val_email);
		}			
		
		//Guarda los datos anexos cuando es seguro Vida o Mas seguros
		if (request.getSession().getAttribute("datosCotizacion")!=null){
			if(request.getSession().getAttribute("datosCotizacion") instanceof CotizacionSeguroVida) {
				CotizacionSeguroVida cotizacionSeguro = (CotizacionSeguroVida)request.getSession().getAttribute("datosCotizacion");

				if (tipo_telefono2 != null && !tipo_telefono2.equals("")) {
					cotizacionSeguro.setTipoTelefono2(tipo_telefono2);
				}else{
					cotizacionSeguro.setTipoTelefono2(null);
				}			
				if (numero_telefono2 != null && !numero_telefono2.equals("")) {
					cotizacionSeguro.setNumeroTelefono2(codigo_telefono2 + numero_telefono2);
				}else{
					cotizacionSeguro.setNumeroTelefono2(null);
				}							
				
				if (cteRegion != null && !cteRegion.equals("")) {
					cotizacionSeguro.setRegion(cteRegion);
				}else{
					cotizacionSeguro.setRegion(null);
				}			
				
				if (cteComuna != null && !cteComuna.equals("")) {
					cotizacionSeguro.setComuna(cteComuna);
				}else{
					cotizacionSeguro.setComuna(null);
				}	
				
				if (cteCiudad != null && !cteCiudad.equals("")) {
					cotizacionSeguro.setCiudad(cteCiudad);
				}else{
					cotizacionSeguro.setCiudad(null);
				}					
				
				if (cteCalle != null && !cteCalle.equals("")) {
					cotizacionSeguro.setDireccion(cteCalle);
				}else{
					cotizacionSeguro.setDireccion(null);
				}
				
				if (cteNumeroCalle != null && !cteNumeroCalle.equals("")) {
					cotizacionSeguro.setNumeroDireccion(cteNumeroCalle);
				}else{
					cotizacionSeguro.setNumeroDireccion(null);
				}	
				
				if (cteNumeroDepto != null && !cteNumeroDepto.equals("")) {
					cotizacionSeguro.setNumeroDepto(cteNumeroDepto);
				}else{
					cotizacionSeguro.setNumeroDepto(null);
				}		
				
				
				if (fechaInicio != null && !fechaInicio.equals("")) {
					cotizacionSeguro.setFecIniVig(fechaInicio);
				}else{
					cotizacionSeguro.setFecIniVig(null);
				}	
				
				if (arrayBeneficiarios != null && arrayBeneficiarios.toString().trim().length() > 0){		
					String[] arrayBeneficiario = arrayBeneficiarios.split("\\~");
					Beneficiario[] beneficiarios = new Beneficiario[arrayBeneficiario.length];
					Beneficiario beneficiario;
					for (int i = 0; i < arrayBeneficiario.length; i++) {
						String[] fila = arrayBeneficiario[i].split("\\^");
						beneficiario = new Beneficiario();
						
						String dia = null;
						String mes = null;
						String ano = null;
						
						if (fila[4].trim() != null && !fila[4].trim().equals("")){
							dia = fila[4].trim();
						}else{
							dia = "00";
						}
						
						if (fila[5].trim() != null && !fila[5].trim().equals("")){
							mes = fila[5].trim();
						}else{
							mes = "00";
						}						
						
						if (fila[6].trim() != null && !fila[6].trim().equals("")){
							ano = fila[6].trim();
						}else{
							ano = "0000";
						}						
						
						float distribucion = 0;
						if (fila[9].trim() != null && !fila[9].trim().equals("")){
							distribucion = (float) Float.parseFloat(fila[9]);
						}
						
						beneficiario.setNombre(fila[1].trim());
						beneficiario.setApellidoPaterno(fila[2].trim());
						beneficiario.setApellidoMaterno(fila[3].trim());
						beneficiario.setFechaNacimiento(new SimpleDateFormat("dd/MM/yyyy").parse(dia +"/"+ mes + "/ " + ano));
						beneficiario.setDescParentesco(ano+"."+mes+"."+dia);
						
						beneficiario.setParentesco(fila[7].trim());
						beneficiario.setSexo(fila[8].trim());
						beneficiario.setDistribucion(distribucion);
						beneficiarios[i] = beneficiario;
					} 
					cotizacionSeguro.setBeneficiarios(beneficiarios);
				}
				
				request.getSession().setAttribute("datosCotizacion",cotizacionSeguro);
			}
		}
		
		
		oForm.setDatos(datos);
		logger.debug(datos);
		
		//Tipos de telefono.
		List<Map<String, ?>> tiposTelefono = delegate.obtenerParametro("TIPO_TELEFONO");
		request.setAttribute("tiposTelefono", tiposTelefono);
		logger.debug(tiposTelefono);

		//Codigos de area.
		List<Map<String, ?>> codigosArea = delegate.obtenerParametro("TEL_CODIGO_AREA");
		request.setAttribute("codigosArea", codigosArea);
		logger.debug(codigosArea);

		//Meses
		List<Map<String, ?>> meses = delegate.obtenerParametro("MESES");
		request.setAttribute("meses", meses.iterator());
		logger.debug(meses);

		//Agnos
		List<Map<String, ?>> anyos = delegate.obtenerParametro("ANYOS");
		request.setAttribute("anyos", anyos.iterator());
		logger.debug(anyos);
		
		//Estado civil.
		EstadoCivil[] estadosCiviles = delegate.obtenerEstadosCiviles();
		request.setAttribute("estadosCiviles", estadosCiviles);
		logger.debug(estadosCiviles);

		//Sexo
		List<Map<String, ?>> sexos = delegate.obtenerParametro("SEXO");
		request.setAttribute("sexos", sexos.iterator());
		logger.debug(sexos);
		
		//Region
		Parametro[] regiones = delegate.obtenerRegiones();
		request.setAttribute("regiones", regiones);
		logger.debug(regiones);
		
		String idRegion = "-1";
		String idComuna = "-1";
		
		//Comuna
		Parametro[] comunas = delegate.obtenerComunas(idRegion);
		request.setAttribute("comunas", comunas);
		logger.debug(comunas);
		
		//Ciudad
		Parametro[] ciudades = delegate.obtenerCiudades(idComuna);
		request.setAttribute("ciudades", ciudades);
		logger.debug(ciudades);
		
		return mapping.findForward("continuar");
	}

}
