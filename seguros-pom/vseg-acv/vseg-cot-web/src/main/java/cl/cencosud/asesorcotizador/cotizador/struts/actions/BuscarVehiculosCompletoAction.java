// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:20
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import cl.cencosud.asesorcotizador.cotizador.struts.actions.DesplegarCotizacionAction;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

public class BuscarVehiculosCompletoAction extends DesplegarCotizacionAction {

   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
      new CotizacionDelegate();
      HashMap clienteSession = this.obtenerDatosClienteSession(request);
      String modeloCombo = request.getParameter("modelo");
      HashMap vehiculosCompleto = new HashMap();
      HashMap datosVehiculosCompleto = new HashMap();
      if(clienteSession.get("rut") != null) {
         String rut = (String)clienteSession.get("rut");
         String tipo = String.valueOf(vehiculosCompleto.get("tipo"));
         String marca = String.valueOf(vehiculosCompleto.get("marca"));
         String modelo = String.valueOf(vehiculosCompleto.get("modelo"));
         vehiculosCompleto.put("tipoVeh", datosVehiculosCompleto.get("tipodato"));
         vehiculosCompleto.put("marcaVeh", datosVehiculosCompleto.get("marcadato"));
         vehiculosCompleto.put("modeloVeh", datosVehiculosCompleto.get("modelodato"));
         PrintWriter pwritter = response.getWriter();
         ObjectMapper mapper = new ObjectMapper();
         StringWriter json = new StringWriter();
         mapper.writeValue(json, vehiculosCompleto);
         pwritter.write(json.toString());
      }

      response.setContentType("text/html");
      return null;
   }
}
