// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:21
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.forms;

import cl.tinet.common.struts.form.BuilderActionFormBaseCOT;
import cl.tinet.common.util.validate.ValidacionUtil;
import java.io.InputStream;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class CotizacionHogarForm extends BuilderActionFormBaseCOT {

   private static final long serialVersionUID = -7691164962566894866L;
   private static final String VALIDATOR_HOGAR = "resource/validation-hogar.xml";
   private static final String VALIDATOR_HOGAR_DUENYO = "resource/validation-hogar-duenyo.xml";


   public InputStream getValidationRules(HttpServletRequest request) {
      String resource = "resource/validation-hogar.xml";
      if(this.getDatos().get("contratanteEsDuenyo") != null && ((String)this.getDatos().get("contratanteEsDuenyo")).equals("false")) {
         resource = "resource/validation-hogar-duenyo.xml";
      }

      return CotizacionHogarForm.class.getResourceAsStream(resource);
   }

   public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
      ActionErrors errors = super.validate(mapping, request);
      long numRUT;
      char digitoVerificador;
      if(!errors.get("datos.dv").hasNext() && !errors.get("datos.rut").hasNext() && !((String)this.getDatos().get("rut")).equals("") && !((String)this.getDatos().get("dv")).equals("")) {
         numRUT = Long.parseLong((String)this.getDatos().get("rut"));
         digitoVerificador = ((String)this.getDatos().get("dv")).length() == 0?32:((String)this.getDatos().get("dv")).charAt(0);

         try {
            if(!ValidacionUtil.isValidoRUT(numRUT, digitoVerificador)) {
               errors.add("datos.rutdv", new ActionMessage("errors.validacion"));
               errors.add("datos.dv", new ActionMessage("errors.validacion"));
               errors.add("datos.rut", new ActionMessage("errors.validacion", true));
            }
         } catch (IllegalArgumentException var11) {
            errors.add("datos.rutdv", new ActionMessage("errors.validacion"));
            errors.add("datos.dv", new ActionMessage("errors.validacion"));
            errors.add("datos.rut", new ActionMessage("errors.validacion"));
         }
      } else {
         errors.add("datos.rutdv", new ActionMessage("errors.required"));
      }

      if(!((String)this.getDatos().get("nombre")).equals("") && !((String)this.getDatos().get("apellidoPaterno")).equals("") && !((String)this.getDatos().get("apellidoMaterno")).equals("")) {
         if(errors.get("datos.nombre").hasNext() || errors.get("datos.apellidoPaterno").hasNext() || errors.get("datos.apellidoMaterno").hasNext()) {
            errors.add("datos.nombres", new ActionMessage("errors.validacion"));
         }
      } else {
         errors.add("datos.nombres", new ActionMessage("errors.required"));
      }

      if(!((String)this.getDatos().get("tipoTelefono")).equals("") && !((String)this.getDatos().get("codigoTelefono")).equals("") && !((String)this.getDatos().get("numeroTelefono")).equals("")) {
         try {
            numRUT = Long.parseLong((String)this.getDatos().get("numeroTelefono"));
            if(numRUT <= 0L) {
               errors.add("telefono", new ActionMessage("errors.required"));
               errors.add("datos.tipoTelefono", new ActionMessage("errors.validacion"));
               errors.add("datos.codigoTelefono", new ActionMessage("errors.validacion"));
               errors.add("datos.numeroTelefono", new ActionMessage("errors.validacion"));
            }
         } catch (NumberFormatException var10) {
            errors.add("telefono", new ActionMessage("errors.required"));
            errors.add("datos.tipoTelefono", new ActionMessage("errors.validacion"));
            errors.add("datos.codigoTelefono", new ActionMessage("errors.validacion"));
            errors.add("datos.numeroTelefono", new ActionMessage("errors.validacion"));
         }
      } else {
         errors.add("telefono", new ActionMessage("errors.required"));
         errors.add("datos.tipoTelefono", new ActionMessage("errors.required"));
         errors.add("datos.codigoTelefono", new ActionMessage("errors.required"));
         errors.add("datos.numeroTelefono", new ActionMessage("errors.required"));
      }

      if(!((String)this.getDatos().get("diaFechaNacimiento")).equals("") && !((String)this.getDatos().get("mesFechaNacimiento")).equals("") && !((String)this.getDatos().get("anyoFechaNacimiento")).equals("")) {
         String numRUT1 = ((String)this.getDatos().get("diaFechaNacimiento")).length() < 2?"0" + (String)this.getDatos().get("diaFechaNacimiento"):(String)this.getDatos().get("diaFechaNacimiento");
         numRUT1 = numRUT1 + (((String)this.getDatos().get("mesFechaNacimiento")).length() < 2?"0" + (String)this.getDatos().get("mesFechaNacimiento"):(String)this.getDatos().get("mesFechaNacimiento"));
         numRUT1 = numRUT1 + (String)this.getDatos().get("anyoFechaNacimiento");
         if(!ValidacionUtil.isFechaValida(numRUT1, "ddMMyyyy")) {
            errors.add("datos.fechaNacimiento", new ActionMessage("errors.validacion"));
            errors.add("diasFecha", new ActionMessage("errors.validacion"));
            errors.add("mesFecha", new ActionMessage("errors.validacion"));
            errors.add("anyosFecha", new ActionMessage("errors.validacion"));
         }
      } else {
         errors.add("datos.fechaNacimiento", new ActionMessage("errors.required"));
         errors.add("diasFecha", new ActionMessage("errors.required"));
         errors.add("mesFecha", new ActionMessage("errors.required"));
         errors.add("anyosFecha", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("regionResidenciaDuenyo")).equals("")) {
         errors.add("regionResidencia", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("comunaResidenciaDuenyo")).equals("")) {
         errors.add("comunaResidencia", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("ciudadResidenciaDuenyo")).equals("")) {
         errors.add("ciudadResidencia", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("antiguedadVivienda")).equals("")) {
         errors.add("antiguedadVivienda", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("direccion")).equals("") || ((String)this.getDatos().get("numeroDireccion")).equals("")) {
         errors.add("datos.direccion", new ActionMessage("errors.required"));
         errors.add("datos.direccionNumero", new ActionMessage("errors.required"));
         errors.add("datos.direccionNroDepto", new ActionMessage("errors.required"));
         errors.add("datos.direccionVivienda", new ActionMessage("errors.required"));
      }

      if(!((String)this.getDatos().get("numeroDireccion")).equals("") && !((String)this.getDatos().get("numeroDepto")).equals("")) {
         try {
            Long.parseLong((String)this.getDatos().get("numeroDireccion"));
         } catch (NumberFormatException var9) {
            errors.add("datos.direccion", new ActionMessage("errors.validacion"));
            errors.add("datos.direccionNumero", new ActionMessage("errors.validacion"));
            errors.add("datos.direccionNroDepto", new ActionMessage("errors.validacion"));
            errors.add("datos.direccionVivienda", new ActionMessage("errors.validacion"));
         }
      }

      if(((String)this.getDatos().get("producto")).equals("")) {
         errors.add("producto", new ActionMessage("errors.required"));
      }

      if(this.getDatos().get("contratanteEsDuenyo") != null && ((String)this.getDatos().get("contratanteEsDuenyo")).equals("false")) {
         if(!errors.get("datos.dvRutDuenyo").hasNext() && !errors.get("datos.rutDuenyo").hasNext() && !((String)this.getDatos().get("rutDuenyo")).equals("") && !((String)this.getDatos().get("dvRutDuenyo")).equals("")) {
            numRUT = Long.parseLong((String)this.getDatos().get("rutDuenyo"));
            digitoVerificador = ((String)this.getDatos().get("dvRutDuenyo")).length() == 0?32:((String)this.getDatos().get("dvRutDuenyo")).charAt(0);

            try {
               if(!ValidacionUtil.isValidoRUT(numRUT, digitoVerificador)) {
                  errors.add("datos.rutDvDuenyo", new ActionMessage("errors.validacion"));
                  errors.add("datos.rutDuenyo", new ActionMessage("errors.validacion"));
                  errors.add("datos.dvRutDuenyo", new ActionMessage("errors.validacion"));
               }
            } catch (IllegalArgumentException var8) {
               errors.add("datos.rutDvDuenyo", new ActionMessage("errors.validacion"));
               errors.add("datos.rutDuenyo", new ActionMessage("errors.validacion"));
               errors.add("datos.dvRutDuenyo", new ActionMessage("errors.validacion"));
            }
         } else {
            errors.add("datos.rutDvDuenyo", new ActionMessage("errors.required"));
         }

         if(!((String)this.getDatos().get("nombreDuenyo")).equals("") && !((String)this.getDatos().get("apellidoPaternoDuenyo")).equals("") && !((String)this.getDatos().get("apellidoMaternoDuenyo")).equals("")) {
            if(errors.get("datos.nombreDuenyo").hasNext() || errors.get("datos.apellidoPaternoDuenyo").hasNext() || errors.get("datos.apellidoMaternoDuenyo").hasNext()) {
               errors.add("datos.nombresDuenyo", new ActionMessage("errors.validacion"));
            }
         } else {
            errors.add("datos.nombresDuenyo", new ActionMessage("errors.required"));
         }
      }

      request.setAttribute("cl.tinet.common.struts.validator.PROPERTY_NAMES", errors.properties());
      return errors;
   }
}
