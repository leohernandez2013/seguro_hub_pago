// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:21
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.forms;

import cl.tinet.common.struts.form.BuilderActionFormBaseCOT;
import cl.tinet.common.util.validate.ValidacionUtil;
import java.io.InputStream;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class CotizacionForm extends BuilderActionFormBaseCOT {

	private static final long serialVersionUID = -1656721547994569635L;
	private String vehiculos;
	private String vehiculo;

	public InputStream getValidationRules(HttpServletRequest request) {
		HashMap datos = (HashMap) this.getDatos();
		String esDuenyo = datos.get("contratanteEsDuenyo") == null ? ""
				: (String) datos.get("contratanteEsDuenyo");
		
		String validatorFile;
		if (esDuenyo.equals("false")) {
			validatorFile = "resource/validation-asistencia-viaje.xml";
		} else {
			validatorFile = "resource/validation.xml";
		}

		return CotizacionVehiculoForm.class.getResourceAsStream(validatorFile);
	}

	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);
		long date;
		if (!errors.get("datos.dv").hasNext()
				&& !errors.get("datos.rut").hasNext()
				&& !((String) this.getDatos().get("rut")).equals("")
				&& !((String) this.getDatos().get("dv")).equals("")) {
			date = Long.parseLong((String) this.getDatos().get("rut"));
			char digitoVerificador = ((String) this.getDatos().get("dv"))
					.length() == 0 ? 32 : ((String) this.getDatos().get("dv"))
					.charAt(0);

			try {
				if (!ValidacionUtil.isValidoRUT(date, digitoVerificador)) {
					errors.add("datos.rutdv", new ActionMessage(
							"errors.validacion"));
					errors.add("datos.dv", new ActionMessage(
							"errors.validacion"));
					errors.add("datos.rut", new ActionMessage(
							"errors.validacion", true));
				}
			} catch (IllegalArgumentException var9) {
				errors.add("datos.rutdv",
						new ActionMessage("errors.validacion"));
				errors.add("datos.dv", new ActionMessage("errors.validacion"));
				errors.add("datos.rut", new ActionMessage("errors.validacion"));
			}
		} else {
			errors.add("datos.rutdv", new ActionMessage("errors.required"));
		}

		if (!((String) this.getDatos().get("nombre")).equals("")
				&& !((String) this.getDatos().get("apellidoPaterno"))
						.equals("")
				&& !((String) this.getDatos().get("apellidoMaterno"))
						.equals("")) {
			if (errors.get("datos.nombre").hasNext()
					|| errors.get("datos.apellidoPaterno").hasNext()
					|| errors.get("datos.apellidoMaterno").hasNext()) {
				errors.add("datos.nombres", new ActionMessage(
						"errors.validacion"));
			}
		} else {
			errors.add("datos.nombres", new ActionMessage("errors.required"));
		}

		if (!((String) this.getDatos().get("tipoTelefono")).equals("")
				&& !((String) this.getDatos().get("codigoTelefono")).equals("")
				&& !((String) this.getDatos().get("numeroTelefono")).equals("")) {
			try {
				date = Long.parseLong((String) this.getDatos().get(
						"numeroTelefono"));
				if (date <= 0L) {
					errors.add("datos.telefono", new ActionMessage(
							"errors.required"));
					errors.add("datos.tipoTelefono", new ActionMessage(
							"errors.validacion"));
					errors.add("datos.codigoTelefono", new ActionMessage(
							"errors.validacion"));
					errors.add("datos.numeroTelefono", new ActionMessage(
							"errors.validacion"));
				}
			} catch (NumberFormatException var8) {
				errors.add("datos.telefono", new ActionMessage(
						"errors.required"));
				errors.add("datos.tipoTelefono", new ActionMessage(
						"errors.validacion"));
				errors.add("datos.codigoTelefono", new ActionMessage(
						"errors.validacion"));
				errors.add("datos.numeroTelefono", new ActionMessage(
						"errors.validacion"));
			}
		} else {
			errors.add("datos.telefono", new ActionMessage("errors.required"));
			errors.add("datos.tipoTelefono", new ActionMessage(
					"errors.required"));
			errors.add("datos.codigoTelefono", new ActionMessage(
					"errors.required"));
			errors.add("datos.numeroTelefono", new ActionMessage(
					"errors.required"));
		}

		if (!((String) this.getDatos().get("diaFechaNacimiento")).equals("")
				&& !((String) this.getDatos().get("mesFechaNacimiento"))
						.equals("")
				&& !((String) this.getDatos().get("anyoFechaNacimiento"))
						.equals("")) {
			String date1 = ((String) this.getDatos().get("diaFechaNacimiento"))
					.length() < 2 ? "0"
					+ (String) this.getDatos().get("diaFechaNacimiento")
					: (String) this.getDatos().get("diaFechaNacimiento");
			date1 = date1
					+ (((String) this.getDatos().get("mesFechaNacimiento"))
							.length() < 2 ? "0"
							+ (String) this.getDatos()
									.get("mesFechaNacimiento") : (String) this
							.getDatos().get("mesFechaNacimiento"));
			date1 = date1 + (String) this.getDatos().get("anyoFechaNacimiento");
			if (!ValidacionUtil.isFechaValida(date1, "ddMMyyyy")) {
				errors.add("datos.fechaNacimiento", new ActionMessage(
						"errors.validacion"));
				errors.add("diasFecha", new ActionMessage("errors.validacion"));
				errors.add("mesFecha", new ActionMessage("errors.validacion"));
				errors.add("anyosFecha", new ActionMessage("errors.validacion"));
			}
		} else {
			errors.add("datos.fechaNacimiento", new ActionMessage(
					"errors.required"));
			errors.add("diasFecha", new ActionMessage("errors.required"));
			errors.add("mesFecha", new ActionMessage("errors.required"));
			errors.add("anyosFecha", new ActionMessage("errors.required"));
		}

		if (((String) this.getDatos().get("producto")).equals("")) {
			errors.add("producto", new ActionMessage("errors.required"));
		}

		// INICIO CONTRATACION TERCEROS
		if (this.getDatos().get("contratanteEsDuenyo") != null
				&& ((String) this.getDatos().get("contratanteEsDuenyo"))
						.equals("false")) {
			if (((String) this.getDatos().get("regionResidenciaDuenyo"))
					.equals("")) {
				errors.add("regionResidencia", new ActionMessage(
						"errors.required"));
			}

			if (((String) this.getDatos().get("comunaResidenciaDuenyo"))
					.equals("")) {
				errors.add("comunaResidencia", new ActionMessage(
						"errors.required"));
			}

			if (!((String) this.getDatos().get("nombreDuenyo")).equals("")
					&& !((String) this.getDatos().get("apellidoPaternoDuenyo"))
							.equals("")
					&& !((String) this.getDatos().get("apellidoMaternoDuenyo"))
							.equals("")) {
				if (errors.get("datos.nombreDuenyo").hasNext()
						|| errors.get("datos.apellidoPaternoDuenyo").hasNext()
						|| errors.get("datos.apellidoPaternoDuenyo").hasNext()) {
					errors.add("datos.nombreDuenyo", new ActionMessage(
							"errors.validacion"));
					errors.add("datos.apellidoPaternoDuenyo",
							new ActionMessage("errors.validacion"));
					errors.add("datos.apellidoMaternoDuenyo",
							new ActionMessage("errors.validacion"));
					errors.add("datos.grupoNombreDuenyo", new ActionMessage(
							"errors.validacion"));
				}
			} else {
				errors.add("datos.nombreDuenyo", new ActionMessage(
						"errors.required"));
				errors.add("datos.apellidoPaternoDuenyo", new ActionMessage(
						"errors.required"));
				errors.add("datos.apellidoMaternoDuenyo", new ActionMessage(
						"errors.required"));
				errors.add("datos.grupoNombreDuenyo", new ActionMessage(
						"errors.required"));
			}

			if (!((String) this.getDatos().get("diaFechaNacimientoDuenyo"))
					.equals("")
					&& !((String) this.getDatos().get(
							"mesFechaNacimientoDuenyo")).equals("")
					&& !((String) this.getDatos().get(
							"anyoFechaNacimientoDuenyo")).equals("")) {
				String ex1;
				ex1 = ((String) this.getDatos().get("diaFechaNacimientoDuenyo"))
						.length() < 2 ? "0"
						+ (String) this.getDatos().get(
								"diaFechaNacimientoDuenyo") : (String) this
						.getDatos().get("diaFechaNacimientoDuenyo");
				ex1 = ex1
						+ (((String) this.getDatos().get(
								"mesFechaNacimientoDuenyo")).length() < 2 ? "0"
								+ (String) this.getDatos().get(
										"mesFechaNacimientoDuenyo")
								: (String) this.getDatos().get(
										"mesFechaNacimientoDuenyo"));
				ex1 = ex1
						+ (String) this.getDatos().get(
								"anyoFechaNacimientoDuenyo");
				if (!ValidacionUtil.isFechaValida(ex1, "ddMMyyyy")) {
					errors.add("datos.fechaNacimientoDuenyo",
							new ActionMessage("errors.validacion"));
					errors.add("diasFechaDuenyo", new ActionMessage(
							"errors.validacion"));
					errors.add("mesFechaDuenyo", new ActionMessage(
							"errors.validacion"));
					errors.add("anyosFechaDuenyo", new ActionMessage(
							"errors.validacion"));
				}
			} else {
				errors.add("datos.fechaNacimientoDuenyo", new ActionMessage(
						"errors.required"));
				errors.add("diasFechaDuenyo", new ActionMessage(
						"errors.required"));
				errors.add("mesFechaDuenyo", new ActionMessage(
						"errors.required"));
				errors.add("anyosFechaDuenyo", new ActionMessage(
						"errors.required"));
			}
		}
		// FIN CONTRATACION TERCEROS

		// INICIO MASCOTA
		String numSubcategoria = this.getDatos().get("numSubcategoria") == null ? ""
				: (String) this.getDatos().get("numSubcategoria");
		
		if(numSubcategoria.equals("140")){

			// VALIDACION NOMBRE
			if (!((String) this.getDatos().get("nombreMascota")).equals("")) {
				if (errors.get("datos.nombreMascota").hasNext()) {
					errors.add("datos.nombreMascota", new ActionMessage("errors.validacion"));
				}
			} else {
				errors.add("datos.nombreMascota", new ActionMessage("errors.required"));
			}

			// VALIDACION EDAD
			if (this.getDatos().get("edadMascota") == null
					|| ((String) this.getDatos().get("edadMascota")).equals("")) {
				errors.add("datos.edadMascota", new ActionMessage("errors.required"));
			}

			// VALIDACION SEXO
			if (this.getDatos().get("sexoMascota") == null
					|| ((String) this.getDatos().get("sexoMascota")).equals("")) {
				errors.add("datos.sexoMascota", new ActionMessage("errors.required"));
			}

			// VALIDACION TIPO
			if (this.getDatos().get("tipoMascota") == null
					|| ((String) this.getDatos().get("tipoMascota")).equals("")) {
				errors.add("datos.tipoMascota", new ActionMessage("errors.required"));
			}

			// VALIDACION RAZA
			if (this.getDatos().get("raza") == null
					|| ((String) this.getDatos().get("raza")).equals("")) {
				errors.add("datos.raza", new ActionMessage("errors.required"));
			}

			// VALIDACION COLOR
			if (this.getDatos().get("colorMascota") == null
					|| ((String) this.getDatos().get("colorMascota")).equals("")) {
				errors.add("datos.colorMascota", new ActionMessage("errors.required"));
			}

			// VALIDACION DIRECCION
			if (((String) this.getDatos().get("direccionMascota")).equals("")
					|| ((String) this.getDatos().get("numeroDirMascota"))
							.equals("")) {
				errors.add("datos.direccionMascota", new ActionMessage("errors.required"));
				errors.add("datos.numeroDirMascota", new ActionMessage("errors.required"));
				errors.add("datos.numeroDeptoMascota", new ActionMessage("errors.required"));
				errors.add("datos.direCompletaMascota", new ActionMessage("errors.required"));
			}

			if (!((String) this.getDatos().get("numeroDirMascota")).equals("")
					&& !((String) this.getDatos().get("numeroDeptoMascota")).equals("")) {
				try {
					Long.parseLong((String) this.getDatos().get("numeroDirMascota"));
				} catch (NumberFormatException var9) {
					errors.add("datos.direccionMascota", new ActionMessage("errors.validacion"));
					errors.add("datos.numeroDirMascota", new ActionMessage("errors.validacion"));
					errors.add("datos.numeroDeptoMascota", new ActionMessage("errors.validacion"));
					errors.add("datos.direCompletaMascota", new ActionMessage("errors.validacion"));
				}
			}
		}
		// FIN MASCOTA

		request.setAttribute("cl.tinet.common.struts.validator.PROPERTY_NAMES",
				errors.properties());
		return errors;
	}

	public void setVehiculos(String vehiculos) {
		this.vehiculos = vehiculos;
	}

	public String getVehiculos() {
		return this.vehiculos;
	}

	public void setVehiculo(String vehiculo) {
		this.vehiculo = vehiculo;
	}

	public String getVehiculo() {
		return this.vehiculo;
	}
}
