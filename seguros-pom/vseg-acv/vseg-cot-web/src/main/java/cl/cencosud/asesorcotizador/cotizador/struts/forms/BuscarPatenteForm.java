package cl.cencosud.asesorcotizador.cotizador.struts.forms;
import org.apache.struts.action.ActionForm;

public class BuscarPatenteForm extends ActionForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String idPatente;
	private String idTipoVehiculo;
	private String marca;
	private String modelo;
	private String anio;
	
	public String getIdPatente() {
		return idPatente;
	}
	public void setIdPatente(String idPatente) {
		this.idPatente = idPatente;
	}
	public String getIdTipoVehiculo() {
		return idTipoVehiculo;
	}
	public void setIdTipoVehiculo(String idTipoVehiculo) {
		this.idTipoVehiculo = idTipoVehiculo;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getAnio() {
		return anio;
	}
	public void setAnio(String anio) {
		this.anio = anio;
	}
}
