// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:19
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import cl.cencosud.acv.common.Contratante;
import cl.cencosud.acv.common.Solicitud;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.tinet.common.model.exception.BusinessException;
import cl.tinet.common.seguridad.model.Usuario;
import cl.tinet.common.seguridad.util.SeguridadUtil;
import com.tinet.exceptions.system.SystemException;
import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ObtenerCotizacionPDFAction extends Action {

   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws BusinessException {
      Usuario usuario = SeguridadUtil.getUsuario(request);
      CotizacionSeguro cot = (CotizacionSeguro)request.getSession().getAttribute("datosCotizacion");
      long idSolicitud = 0L;
      if(request.getSession().getAttribute("idSolicitud") != null) {
         idSolicitud = Long.parseLong(String.valueOf(request.getSession().getAttribute("idSolicitud")));
      }

      CotizacionDelegate delegate = new CotizacionDelegate();
      Solicitud solicitud = delegate.obtenerDatosSolicitud(idSolicitud);
      byte[] pdf = null;
      Contratante contratante = new Contratante();
      contratante.setRut(cot.getRutCliente());
      contratante.setDv(cot.getDv());
      contratante.setApellidoPaterno(cot.getApellidoPaterno());
      contratante.setApellidoMaterno(cot.getApellidoMaterno());
      contratante.setNombre(cot.getNombre());
      contratante.setFechaDeNacimiento(cot.getFechaNacimiento());
      contratante.setEstadoCivil(cot.getEstadoCivil());
      contratante.setSexo(cot.getSexo());
      contratante.setCalleDireccion(cot.getDireccion());
      contratante.setNumeroDireccion(cot.getNumeroDireccion());
      contratante.setNumeroDepartamentoDireccion(cot.getNumeroDepto());
      cot.setContratante(contratante);
      if(cot instanceof CotizacionSeguroVehiculo) {
         pdf = delegate.obtenerCotizacionVehiculoPDF((CotizacionSeguroVehiculo)cot, solicitud, usuario);
      } else if(cot instanceof CotizacionSeguroHogar) {
         pdf = delegate.obtenerCotizacionHogarPDF((CotizacionSeguroHogar)cot, solicitud, usuario);
      } else if(cot instanceof CotizacionSeguroVida) {
         pdf = delegate.obtenerCotizacionVidaPDF((CotizacionSeguroVida)cot, solicitud, usuario);
      }

      try {
         ServletOutputStream e = response.getOutputStream();
         response.setHeader("Pragma", "public");
         response.setHeader("cache-control", "max-age=0");
         Date hoy = new Date();
         response.setDateHeader("expires", hoy.getTime() + 30L);
         response.setContentLength(pdf.length);
         response.setContentType("application/pdf");
         response.setHeader("Content-Transfer-Encoding", "binary");
         response.setHeader("Content-Disposition", " attachment;filename=cotizacion-" + System.currentTimeMillis() + ".pdf");
         if(pdf != null) {
            e.write(pdf, 0, pdf.length);
         }

         e.flush();
         e.close();
         return null;
      } catch (IOException var15) {
         throw new SystemException(var15);
      }
   }
}
