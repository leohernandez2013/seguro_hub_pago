// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:20
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import cl.cencosud.acv.common.Actividad;
import cl.cencosud.acv.common.EstadoCivil;
import cl.cencosud.acv.common.Producto;
import cl.cencosud.acv.common.Solicitud;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida;
import cl.cencosud.asesorcotizador.cotizador.struts.actions.DesplegarCotizacionAction;
import cl.cencosud.asesorcotizador.cotizador.struts.forms.CotizacionVehiculoForm;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.tinet.common.struts.form.BuilderActionFormBaseCOT;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DesplegarCotizacionCesantiaAction extends DesplegarCotizacionAction {

   private static final Log logger = LogFactory.getLog(DesplegarCotizacionCesantiaAction.class);


   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
      HttpSession session = request.getSession();
      CotizacionDelegate delegate = new CotizacionDelegate();
      boolean sw = false;
      String idSubcategoria = "";
      if(request.getParameter("idSubcategoria") != null) {
         request.getSession().setAttribute("idSubcategoria", request.getParameter("idSubcategoria"));
         idSubcategoria = request.getParameter("idSubcategoria");
      } else if(request.getSession() != null && request.getSession().getAttribute("idSubcategoria") != null) {
         idSubcategoria = request.getSession().getAttribute("idSubcategoria").toString();
         sw = true;
      }

      String idRama = "";
      if(request.getParameter("idRama") != null) {
         request.getSession().setAttribute("idRama", request.getParameter("idRama"));
         idRama = request.getParameter("idRama");
      } else if(request.getSession() != null && request.getSession().getAttribute("idRama") != null) {
         idRama = request.getSession().getAttribute("idRama").toString();
      }

      String idPlan = request.getParameter("idPlan");
      if(idPlan == null) {
         if(request.getSession().getAttribute("idPlan") != null) {
            idPlan = request.getSession().getAttribute("idPlan").toString();
         } else {
            idPlan = null;
         }
      } else {
         request.getSession().setAttribute("idPlan", idPlan);
      }

      Producto[] productos = delegate.obtenerProductos(idSubcategoria, idRama, idPlan);
      if(productos != null && productos.length > 0) {
         request.setAttribute("productos", productos);
      }

      HashMap clienteSession = null;
      byte vaCaptcha = 1;
      session.setAttribute("vaCaptcha", Integer.valueOf(vaCaptcha));
      String actividades;
      if(idSubcategoria.equals("22")) {
         actividades = (String)((CotizacionVehiculoForm)form).getDatos().get("hiddCaptcha");
         if(actividades == null) {
            clienteSession = this.obtenerDatosClienteSession(request);
         } else {
            vaCaptcha = 0;
            session.setAttribute("vaCaptcha", Integer.valueOf(vaCaptcha));
            clienteSession = this.obtenerDatosClienteSession(request);
         }
      } else {
         clienteSession = this.obtenerDatosClienteSession(request);
      }

      if(((BuilderActionFormBaseCOT)form).getDatos().isEmpty() && clienteSession != null) {
         ((BuilderActionFormBaseCOT)form).getDatos().putAll(clienteSession);
      }

      if(request.getAttribute("vitrineo") != null && !"si".equals((String)request.getAttribute("vitrineo"))) {
         logger.info("Desplegar Cotizacion Cesantia: No se guarda Vitrineo ...");
      } else {
         logger.info("Desplegar Cotizacion Cesantia: Guardando Vitrineo ...");
         actividades = (String)((BuilderActionFormBaseCOT)form).getDatos().get("claveVitrineo");
         session.setAttribute("claveVitrineo", actividades);
         String estadosCiviles = (String)((BuilderActionFormBaseCOT)form).getDatos().get("rut");
         String tipoTelefono = (String)((BuilderActionFormBaseCOT)form).getDatos().get("dv");
         String codArea = (String)((BuilderActionFormBaseCOT)form).getDatos().get("tipoTelefono");
         String meses = (String)((BuilderActionFormBaseCOT)form).getDatos().get("codigoTelefono");
         String anyos = (String)((BuilderActionFormBaseCOT)form).getDatos().get("estadoCivil");
         String solicitud = (String)((BuilderActionFormBaseCOT)form).getDatos().get("sexo");
         byte cotVida = 0;
         byte vida = 0;
         Object esDuenyo = null;
         byte numPuertas = 0;
         short idModeloVitrineo = 8888;
         Object idAnyoVitrineo = null;
         short idMarcaVitrineo = 8888;
         int productoVitrineo = Integer.parseInt((String)((BuilderActionFormBaseCOT)form).getDatos().get("producto"));
         String nombre = (String)((BuilderActionFormBaseCOT)form).getDatos().get("nombre");
         String apellidoPaterno = (String)((BuilderActionFormBaseCOT)form).getDatos().get("apellidoPaterno");
         String apellidoMaterno = (String)((BuilderActionFormBaseCOT)form).getDatos().get("apellidoMaterno");
         String diaNac = (String)((BuilderActionFormBaseCOT)form).getDatos().get("diaFechaNacimiento");
         String mesNac = (String)((BuilderActionFormBaseCOT)form).getDatos().get("mesFechaNacimiento");
         String anyoNac = (String)((BuilderActionFormBaseCOT)form).getDatos().get("anyoFechaNacimiento");
         String telefono = (String)((BuilderActionFormBaseCOT)form).getDatos().get("numeroTelefono");
         String email = (String)((BuilderActionFormBaseCOT)form).getDatos().get("email");
         byte comuna = 0;
         byte region = 0;
         int rama = Integer.parseInt(idRama);
         int subcategoria = Integer.parseInt(idSubcategoria);
         Object ciudadHogar = null;
         Object direccionHogar = null;
         Object numeroDireccionHogar = null;
         Object numeroDeptoHogar = null;
         Object anyoHogar = null;
         Object rutDuenyo = null;
         Object dvDuenyo = null;
         Object nombreDuenyo = null;
         Object apellidoPaternoDuenyo = null;
         Object apellidoMaternoDuenyo = null;
         Object diaNacimientoDuenyo = null;
         Object mesNacimientoDuenyo = null;
         Object anyoNacimientoDuenyo = null;
         Object estadoCivilDuenyo = null;
         Object valorComercial = null;
         Object regionResidenciaDuenyo = null;
         Object comunaResidenciaDuenyo = null;
         delegate.grabarVitrineoGeneral(actividades, estadosCiviles, tipoTelefono, nombre, apellidoPaterno, apellidoMaterno, codArea, meses, telefono, email, region, comuna, diaNac, mesNac, anyoNac, anyos, solicitud, (long)cotVida, vida, idMarcaVitrineo, idModeloVitrineo, (String)idAnyoVitrineo, numPuertas, (String)esDuenyo, (String)ciudadHogar, (String)direccionHogar, (String)numeroDireccionHogar, (String)numeroDeptoHogar, (String)anyoHogar, rama, subcategoria, productoVitrineo, (String)rutDuenyo, (String)dvDuenyo, (String)nombreDuenyo, (String)apellidoPaternoDuenyo, (String)apellidoMaternoDuenyo, (String)diaNacimientoDuenyo, (String)mesNacimientoDuenyo, (String)anyoNacimientoDuenyo, (String)estadoCivilDuenyo, (String)regionResidenciaDuenyo, (String)comunaResidenciaDuenyo, (String)valorComercial);
         logger.info("Vitrineo guardado");
      }

      Actividad[] actividades1 = delegate.obtenerActividades();
      request.setAttribute("actividades", actividades1);
      EstadoCivil[] estadosCiviles1 = delegate.obtenerEstadosCiviles();
      request.setAttribute("estadosCiviles", estadosCiviles1);
      List tipoTelefono1 = delegate.obtenerParametro("TIPO_TELEFONO");
      request.setAttribute("tipoTelefono", tipoTelefono1.iterator());
      List codArea1 = delegate.obtenerParametro("TEL_CODIGO_AREA");
      request.setAttribute("codigoArea", codArea1.iterator());
      List meses1 = delegate.obtenerParametro("MESES");
      request.setAttribute("meses", meses1.iterator());
      List anyos1 = delegate.obtenerParametro("ANYOS");
      request.setAttribute("anyos", anyos1.iterator());
      if(request.getAttribute("retomaCotizacion") != null) {
         Solicitud solicitud1 = (Solicitud)request.getAttribute("retomaCotizacion");
         logger.info("Desplegar Cotizacion Cesantia: Se retoma cotizacion:" + solicitud1 + "");
         CotizacionSeguroVida cotVida1 = delegate.obtenerDatosMateriaVida(solicitud1.getId_solicitud());
         solicitud1.setDatosCotizacion(cotVida1);
         if(solicitud1 != null) {
            CotizacionSeguroVida vida1 = (CotizacionSeguroVida)solicitud1.getDatosCotizacion();
            ((BuilderActionFormBaseCOT)form).getDatos().put("actividad", vida1.getActividad());
            ((BuilderActionFormBaseCOT)form).getDatos().put("producto", String.valueOf(solicitud1.getId_producto()));
         }
      }

      if(request.getParameter("idProducto") != null && !request.getParameter("idProducto").equals("")) {
         ((BuilderActionFormBaseCOT)form).getDatos().put("producto", request.getParameter("idProducto"));
      }

      if(request.getParameter("volverPaso1") != null && request.getSession().getAttribute("datosVolverPaso1") != null) {
         ((BuilderActionFormBaseCOT)form).getDatos().putAll((HashMap)request.getSession().getAttribute("datosVolverPaso1"));
         logger.info("Desplegar Cotizacion Cesantia: Volviendo al paso 1");
         request.getSession().removeAttribute("datosVolverPaso1");
      }

      if(request.getParameter("idPlan") != null) {
         request.getSession().setAttribute("isPromocion", "true");
      } else {
         request.getSession().removeAttribute("isPromocion");
      }

      return mapping.findForward("desplegarFormularioCesantia");
   }

}
