// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:19
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import cl.cencosud.acv.common.Plan;
import cl.cencosud.asesorcotizador.cotizador.struts.forms.CotizacionForm;
import cl.cencosud.asesorcotizador.cotizador.struts.forms.CotizacionHogarForm;
import cl.cencosud.asesorcotizador.cotizador.struts.forms.CotizacionSaludForm;
import cl.cencosud.asesorcotizador.cotizador.struts.forms.CotizacionVehiculoForm;
import cl.cencosud.asesorcotizador.cotizador.struts.forms.CotizacionVidaForm;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import com.tinet.exceptions.system.SystemException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

public class ObtenerPlanesAction extends Action {

	public static final Log logger = LogFactory
			.getLog(ObtenerPlanesAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		CotizacionDelegate delegate = new CotizacionDelegate();

		try {
			logger.info("Iniciando obtencion de planes.");
			HashMap e = null;
			logger.info("Obteniendo Mapa de parametros");
			e = this.obtenerMapaFormulario(form);
			String idProducto = "";

			// Creamos arreglos para cada uno de los planes
			Plan[] planesCotizados = (Plan[]) null;
			/*Plan[] planesCotizados00 = (Plan[]) null;
			Plan[] planesCotizados01 = (Plan[]) null;
			Plan[] planesCotizados02 = (Plan[]) null;
			Plan[] planesCotizados03 = (Plan[]) null;
			Plan[] planesCotizados04 = (Plan[]) null;
			Plan[] planesCotizados05 = (Plan[]) null;
			Plan[] planesCotizados06 = (Plan[]) null;*/

			if (e != null) {
				idProducto = (String) e.get("producto");
				logger.info("idProducto: " + idProducto);
			}

			String idPlanPromocion = null;
			if (request.getSession().getAttribute("idPlan") != null) {
				idPlanPromocion = request.getSession().getAttribute("idPlan")
						.toString();
			}

			String isProductoHabilitadoComparador;
			String result;
			String pwritter;
			String mapper;
			String json;
			String idProducto7;
			if (idPlanPromocion != null && idPlanPromocion.length() > 0
					&& !idPlanPromocion.equals("-1")) {
				logger.info("Es un plan en promocion... Obteniendo planes");
				if (!idProducto.equals("188") && !idProducto.equals("189")
						&& !idProducto.equals("190")
						&& !idProducto.equals("191")
						&& !idProducto.equals("192")
						&& !idProducto.equals("88")) {
					planesCotizados = delegate.obtenerPlanes(idProducto,
							idPlanPromocion);
				} else {
					isProductoHabilitadoComparador = (String) e
							.get("hiddProducto2");
					result = (String) e.get("hiddProducto3");
					pwritter = (String) e.get("hiddProducto4");
					mapper = (String) e.get("hiddProducto5");
					json = (String) e.get("hiddProducto6");
					idProducto7 = (String) e.get("hiddProducto7");

					if (idProducto.equals("188")) {
						isProductoHabilitadoComparador = idProducto;
					}

					if (idProducto.equals("189")) {
						result = idProducto;
					}

					if (idProducto.equals("190")) {
						pwritter = idProducto;
					}

					if (idProducto.equals("191")) {
						mapper = idProducto;
					}

					if (idProducto.equals("192")) {
						json = idProducto;
					}

					if (idProducto.equals("88")) {
						idProducto7 = idProducto;
					}

					logger.info("Producto 1: " + isProductoHabilitadoComparador);
					logger.info("Producto 2: " + result);
					logger.info("Producto 3: " + pwritter);
					logger.info("Producto 4: " + mapper);
					logger.info("Producto 5: " + json);
					logger.info("Producto 6: " + idProducto7);
					planesCotizados = delegate.obtenerPlanesDeducibles(
							isProductoHabilitadoComparador, result, pwritter,
							mapper, json, idProducto7, idPlanPromocion);
				}
			} else {
				logger.info("No Es un plan en promocion... Obteniendo planes");
				if (!idProducto.equals("188") && !idProducto.equals("189")
						&& !idProducto.equals("190")
						&& !idProducto.equals("191")
						&& !idProducto.equals("192")
						&& !idProducto.equals("88")) {
					planesCotizados = delegate.obtenerPlanes(idProducto,
							(String) null);
				} else {

					isProductoHabilitadoComparador = (String) e
							.get("hiddProducto2");
					result = (String) e.get("hiddProducto3");
					pwritter = (String) e.get("hiddProducto4");
					mapper = (String) e.get("hiddProducto5");
					json = (String) e.get("hiddProducto6");
					idProducto7 = (String) e.get("hiddProducto7");

					if (idProducto.equals("188")) {
						isProductoHabilitadoComparador = idProducto;
					}

					if (idProducto.equals("189")) {
						result = idProducto;
					}

					if (idProducto.equals("190")) {
						pwritter = idProducto;
					}

					if (idProducto.equals("191")) {
						mapper = idProducto;
					}

					if (idProducto.equals("192")) {
						json = idProducto;
					}

					if (idProducto.equals("88")) {
						idProducto7 = idProducto;
					}

					if (idProducto.equals("188")
							|| isProductoHabilitadoComparador.equals("188")) {
						if (!idProducto.equals(isProductoHabilitadoComparador)) {
							request.getSession().setAttribute("chkPlan00", "");
							request.getSession().setAttribute("chkPlan00", "1");
						} else {
							request.getSession().setAttribute("chkPlan00", "");
							request.getSession().setAttribute("chkPlan00", "1");
						}
					}
					if (idProducto.equals("189") || result.equals("189")) {
						if (!idProducto.equals(result)) {
							request.getSession().setAttribute("chkPlan03", "");
							request.getSession().setAttribute("chkPlan03", "2");
						} else {
							request.getSession().setAttribute("chkPlan03", "");
							request.getSession().setAttribute("chkPlan03", "2");
						}
					}

					if (idProducto.equals("190") || pwritter.equals("190")) {
						if (!idProducto.equals(pwritter)) {
							request.getSession().setAttribute("chkPlan04", "");
							request.getSession().setAttribute("chkPlan04", "3");
						} else {
							request.getSession().setAttribute("chkPlan04", "");
							request.getSession().setAttribute("chkPlan04", "3");
						}
					}

					if (idProducto.equals("191") || mapper.equals("191")) {
						if (!idProducto.equals(mapper)) {
							request.getSession().setAttribute("chkPlan05", "");
							request.getSession().setAttribute("chkPlan05", "4");
						} else {
							request.getSession().setAttribute("chkPlan05", "");
							request.getSession().setAttribute("chkPlan05", "4");
						}
					}

					if (idProducto.equals("192") || json.equals("192")) {
						if (!idProducto.equals(json)) {
							request.getSession().setAttribute("chkPlan06", "");
							request.getSession().setAttribute("chkPlan08", "5");
						} else {
							request.getSession().setAttribute("chkPlan06", "");
							request.getSession().setAttribute("chkPlan08", "5");
						}
					}

					if (idProducto.equals("88") || idProducto7.equals("88")) {
						if (!idProducto.equals(idProducto7)) {
							request.getSession().setAttribute("chkPlan10", "");
							request.getSession().setAttribute("chkPlan10", "6");
						} else {
							request.getSession().setAttribute("chkPlan10", "");
							request.getSession().setAttribute("chkPlan10", "6");
						}
					}

					// Pleyasoft
			    	 String[] pDeducibles;
			    	 pDeducibles = String.valueOf(delegate.obtenerParametro("PRODUCTOS", "DEDUCIBLES").get("VALOR")).split(",");
			    	
					
					planesCotizados = delegate.obtenerPlanesDeducibles(
							pDeducibles[0], pDeducibles[1], pDeducibles[2],
							pDeducibles[3], pDeducibles[4], pDeducibles[5], (String) null);
					
					logger.info("Producto 1: " + isProductoHabilitadoComparador);
					logger.info("Producto 2: " + result);
					logger.info("Producto 3: " + pwritter);
					logger.info("Producto 4: " + mapper);
					logger.info("Producto 5: " + json);
					logger.info("Producto 6: " + idProducto7);
					/*
					planesCotizados = delegate.obtenerPlanesDeducibles(
							isProductoHabilitadoComparador, result, pwritter,
							mapper, json, idProducto7, (String) null);
							*/
					logger.info("Termino consulta de planes");

				}
			}

			boolean isProductoHabilitadoComparador1 = delegate
					.isComparadorHabilitado(Long.valueOf(idProducto));
			HashMap result1 = new HashMap();

			

			isProductoHabilitadoComparador = (String) e.get("hiddProducto2");
			result = (String) e.get("hiddProducto3");
			pwritter = (String) e.get("hiddProducto4");
			mapper = (String) e.get("hiddProducto5");
			json = (String) e.get("hiddProducto6");
			idProducto7 = (String) e.get("hiddProducto7");

			int numPlanes = 0;

			/*
			if (idProducto.equals("188")
					|| isProductoHabilitadoComparador.equals("188")) {
				planesCotizados00 = delegate
						.obtenerPlanes("188", (String) null);
				numPlanes += planesCotizados00.length;
			}*/
			/*
			if (idProducto.equals("189") || result.equals("189")) {
				planesCotizados01 = delegate
						.obtenerPlanes("189", (String) null);
				numPlanes += planesCotizados01.length;
			}
			if (idProducto.equals("190") || pwritter.equals("190")) {
				planesCotizados02 = delegate
						.obtenerPlanes("190", (String) null);
				numPlanes += planesCotizados02.length;
			}
			if (idProducto.equals("191") || mapper.equals("191")) {
				planesCotizados03 = delegate
						.obtenerPlanes("191", (String) null);
				numPlanes += planesCotizados03.length;
			}
			if (idProducto.equals("192") || json.equals("192")) {
				planesCotizados04 = delegate
						.obtenerPlanes("192", (String) null);
				numPlanes += planesCotizados04.length;
			}
			if (idProducto.equals("88") || idProducto7.equals("88")) {
				planesCotizados05 = delegate.obtenerPlanes("88", (String) null);
				numPlanes += planesCotizados05.length;
			}

			Plan[] planesCotizadosAll = new Plan[numPlanes];

			int i = 0;
			if (planesCotizados00 != null)
				for (Plan plan : planesCotizados00) {
					planesCotizadosAll[i] = plan;
					System.out.print(plan.getIdProducto());
					i++;
				}
			/*
			if (planesCotizados01 != null)
				for (Plan plan : planesCotizados01) {
					planesCotizadosAll[i] = plan;
					System.out.print(plan.getIdProducto());
					i++;
				}
			if (planesCotizados02 != null)
				for (Plan plan : planesCotizados02) {
					planesCotizadosAll[i] = plan;
					System.out.print(plan.getIdProducto());
					i++;
				}
			if (planesCotizados03 != null)
				for (Plan plan : planesCotizados03) {
					planesCotizadosAll[i] = plan;
					System.out.print(plan.getIdProducto());
					i++;
				}
			if (planesCotizados04 != null)
				for (Plan plan : planesCotizados04) {
					planesCotizadosAll[i] = plan;
					System.out.print(plan.getIdProducto());
					i++;
				}
			if (planesCotizados05 != null)
				for (Plan plan : planesCotizados05) {
					planesCotizadosAll[i] = plan;
					System.out.print(plan.getIdProducto());
					i++;
				}*/
			
			result1.put("planesCotizados", planesCotizados);
			result1.put("isComparadorHabilitado",
					Boolean.valueOf(isProductoHabilitadoComparador1));
			result1.put("deducible", idProducto);
			response.setContentType("text/html");
			if (result1 != null) {
				request.getSession().setAttribute("result", result1);
			}

			PrintWriter pwritter1 = response.getWriter();
			ObjectMapper mapper1 = new ObjectMapper();
			StringWriter json1 = new StringWriter();

			mapper1.writeValue(json1, result1);
			pwritter1.write(json1.toString());
			return null;
		} catch (IOException var16) {
			throw new SystemException(var16);
		}
	}

	private HashMap obtenerMapaFormulario(ActionForm form) {
		HashMap datos = null;
		if (form instanceof CotizacionHogarForm) {
			datos = (HashMap) ((CotizacionHogarForm) form).getDatos();
		} else if (form instanceof CotizacionVehiculoForm) {
			datos = (HashMap) ((CotizacionVehiculoForm) form).getDatos();
		} else if (form instanceof CotizacionVidaForm) {
			datos = (HashMap) ((CotizacionVidaForm) form).getDatos();
		} else if (form instanceof CotizacionSaludForm) {
			datos = (HashMap) ((CotizacionSaludForm) form).getDatos();
		} else {
			datos = (HashMap) ((CotizacionForm) form).getDatos();
		}

		return datos;
	}
}
