// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:20
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DesplegarCambioClaveTemporalAction extends Action {

   private static final Log logger = LogFactory.getLog(DesplegarCambioClaveTemporalAction.class);
   public static final String URI_EXITO_PARAM_KEY = "cl.tinet.common.seguridad.servlet.URI_EXITO";


   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
      String destino = request.getSession().getAttribute("exito").toString();
      logger.info("ingreso a DesplegarCambioClaveTemporalAction");
      if(request.getParameter("desplegarClaveTemporal") == null) {
         logger.info("temporal es null");
         request.getSession().setAttribute("frameClaveTemporal", "true");
      } else {
         logger.info("temporal no es null");
         request.setAttribute("esCambioClaveTemporal", "true");
         request.getSession().removeAttribute("frameClaveTemporal");
      }

      logger.info("redirect: " + destino);
      destino = request.getContextPath().concat(destino);
      response.sendRedirect(destino);
      return null;
   }

}
