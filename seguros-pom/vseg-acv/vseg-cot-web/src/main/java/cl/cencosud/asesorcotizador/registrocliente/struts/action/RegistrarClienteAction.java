package cl.cencosud.asesorcotizador.registrocliente.struts.action;

import java.lang.reflect.InvocationTargetException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.SystemException;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.asesorcotizador.delegate.MantenedorClientesDelegateRemote;
import cl.cencosud.asesorcotizador.registrocliente.struts.forms.RegistroClienteForm;
import cl.cencosud.ventaseguros.common.config.VSPErrorsConfig;
import cl.cencosud.ventaseguros.common.exception.ValidacionUsuarioException;
import cl.tinet.common.captcha.servlet.util.PrettyFacesWrappedRequest;
import cl.tinet.common.model.exception.BusinessException;
import cl.tinet.common.seguridad.config.SeguridadConfig;
import cl.tinet.common.seguridad.exception.AutenticacionException;
import cl.tinet.common.seguridad.model.UsuarioExterno;
import cl.tinet.common.seguridad.util.SeguridadUtil;
import cl.tinet.common.util.crypto.CryptoUtil;

public class RegistrarClienteAction extends Action {

	private static final Log logger = LogFactory.getLog(RegistrarClienteAction.class);

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws BusinessException, SystemException {
		String forward = "continuar";
        logger.info("Iniciar Proceso de Registro de Cliente");

        VSPErrorsConfig vspErrors = VSPErrorsConfig.getInstance();

		try {
			if (this.esCaptchaValido(request)) {
				RegistroClienteForm formulario = (RegistroClienteForm) form;
				Map<String, String> datos = formulario.getDatos();

				CryptoUtil cryptoUtil = new CryptoUtil(SeguridadConfig.getInstance());
				String clave = datos.get("clave");
				String confirmaClave = datos.get("confirma_clave");
				
				if (clave.length() < 4) {
                    throw new ValidacionUsuarioException(ValidacionUsuarioException.CLAVE_INCORRECTA, new Object[] {});
                }else if(!clave.equals(confirmaClave)){
                	throw new ValidacionUsuarioException(ValidacionUsuarioException.CLAVE_NO_COINCIDE, new Object[] {});
				} else {
					String pass = CryptoUtil.toBase64String(cryptoUtil.getEncrypted(clave), true);
					UsuarioExterno externo = new UsuarioExterno();
					BeanUtils.populate(externo, datos);

					// Concatenar el codigo de area al numero de telefono.
					externo.setTelefono_1(datos.get("codigo_area") + datos.get("telefono_1"));

					Calendar cal = new GregorianCalendar();
					int year = Integer.valueOf(datos.get("anyoFechaNacimiento")).intValue();
					int month = Integer.valueOf(datos.get("mesFechaNacimiento")).intValue();
					int date = Integer.valueOf(datos.get("diaFechaNacimiento")).intValue();
					cal.set(year, month - 1, date);
					Date fechaNacimiento = cal.getTime();
					externo.setFecha_nacimiento(fechaNacimiento);

					MantenedorClientesDelegateRemote delegate = new MantenedorClientesDelegateRemote();
					delegate.registrarCliente(externo, pass);		
					request.getSession().setAttribute("paso2", "ok");
					
					Map<String, String[]> extraParams = new TreeMap<String, String[]>();
					extraParams.put("username",  new String[]{externo.getRut_cliente() + "-" + externo.getDv_cliente()});
					extraParams.put("password",  new String[]{clave});
					
				    HttpServletRequest wrappedRequest = new PrettyFacesWrappedRequest(request, extraParams);
					this.verificarAutenticacion(wrappedRequest, response);
				}

			} else {
				throw new ValidacionUsuarioException(ValidacionUsuarioException.CAPTCHA_INVALIDO, new Object[] {});
			}

        } catch (IllegalAccessException e) {
            logger.info("IllegalAccessException ", e);
            throw new com.tinet.exceptions.system.SystemException(e);
            
        } catch (InvocationTargetException e) {
            logger.info("InvocationTargetException ", e);
            throw new com.tinet.exceptions.system.SystemException(e);
            
        }catch (ValidacionUsuarioException e) {
            logger.info("ValidacionUsuarioException " + e.getMessage());
            
            if(e.getMessage().equals(vspErrors.getString(ValidacionUsuarioException.NRO_SERIE_NO_VALIDO))){
                request.setAttribute("MensajeError", e.getMessage());
                throw new ValidacionUsuarioException(ValidacionUsuarioException.NRO_SERIE_NO_VALIDO, new Object[] {});
                
            }else if(e.getMessage().equals(vspErrors.getString(ValidacionUsuarioException.CAPTCHA_INVALIDO))){
                request.setAttribute("MensajeError", e.getMessage());
                throw new ValidacionUsuarioException(ValidacionUsuarioException.CAPTCHA_INVALIDO, new Object[] {});
                
            }else if(e.getMessage().equals(vspErrors.getString(ValidacionUsuarioException.CLAVE_INCORRECTA))){
            	request.setAttribute("MensajeError", e.getMessage());
                throw new ValidacionUsuarioException(ValidacionUsuarioException.CLAVE_INCORRECTA, new Object[] {});
                
            }else if(e.getMessage().equals(vspErrors.getString(ValidacionUsuarioException.CLAVE_NO_COINCIDE))){
            	request.setAttribute("MensajeError", e.getMessage());
                throw new ValidacionUsuarioException(ValidacionUsuarioException.CLAVE_NO_COINCIDE, new Object[] {});
            }
        }
		return mapping.findForward(forward);
	}

    private boolean esCaptchaValido(HttpServletRequest request){
    	logger.info("Validando Captcha");
    	String captcha = request.getParameter("g-recaptcha-response");
    	boolean isResponseCorrect = false;
    	if (captcha == null || ("").equals(captcha.trim())){
		   isResponseCorrect = false;
		   logger.info("Captcha: Invalido");
		}else{
			isResponseCorrect = true;
			logger.info("Captcha: Valido");
		}
		return isResponseCorrect;
    }

	protected void verificarAutenticacion(HttpServletRequest request, HttpServletResponse response)
			throws AutenticacionException {
		if (!SeguridadUtil.isAutenticado(request)) {
			logger.debug("Usuario no autenticado. Autenticando.");
			SeguridadUtil.login(request, response, 1);
		} else {
			logger.debug("Usuario autenticado, se continua normalmente.");
		}

	}

}
