// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:19
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import cl.cencosud.acv.common.Plan;
import cl.cencosud.acv.common.Solicitud;
import cl.cencosud.acv.common.config.ACVConfig;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.tinet.common.model.exception.BusinessException;
import cl.tinet.common.seguridad.model.Usuario;
import cl.tinet.common.seguridad.util.SeguridadUtil;
import cl.tinet.common.struts.form.BuilderActionFormBaseCOT;
import com.tinet.exceptions.system.SystemException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class GuardarDatosCotizacionAction extends Action {

   private static Log logger = LogFactory.getLog(GuardarDatosCotizacionAction.class);
   private static final String VIDA_COOKIE_SEGUNDOS = "cl.cencosud.asesorcotizador.solicitud.vidaCookie.segundos";


   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws BusinessException {
      HttpSession session = request.getSession();
      String forward = "";
      CotizacionDelegate oDelegate = new CotizacionDelegate();
      BuilderActionFormBaseCOT oForm = (BuilderActionFormBaseCOT)form;
      String enviar = (String)oForm.getDatos().get("enviar");
      String idPlan = (String)oForm.getDatos().get("idPlan");
      CotizacionSeguro oCotizacionSeguro = (CotizacionSeguro)request.getSession().getAttribute("datosCotizacion");
      String idProducto = String.valueOf(oCotizacionSeguro.getCodigoProducto());
      String idRama = "";
      Usuario usuario = SeguridadUtil.getUsuario(request);
      oCotizacionSeguro.setCodigoPlan(Long.parseLong(idPlan));
      idRama = String.valueOf(session.getAttribute("idRama"));
      if("null".equals(idRama)) {
         throw new SystemException(new Exception("Rama vacia."));
      } else {
         Plan plan = oDelegate.obtenerDatosPlan(Long.valueOf(idPlan).longValue());
         Plan oPlan = new Plan();
         oPlan.setIdPlanLegacy(plan.getIdPlanLegacy());
         if(!oPlan.getIdPlanLegacy().equalsIgnoreCase("")) {
            request.getSession().setAttribute("idPlanLegacyPaso2", oPlan.getIdPlanLegacy());
         }

         oPlan.setCodigoCompanniaInspeccion(plan.getCodigoCompanniaInspeccion());
         oPlan.setIdPlan(idPlan);
         oPlan.setNombrePlan((String)oForm.getDatos().get("nombrePlan"));
         oPlan.setPrimaMensualPesos(Long.parseLong((String)oForm.getDatos().get("primaPlanPesos")));
         oPlan.setInspeccionVehi(plan.getInspeccionVehi());
         oPlan.setMaxDiaIniVig(plan.getMaxDiaIniVig());
         session.setAttribute("reqInspeccion", Integer.valueOf(oPlan.getInspeccionVehi()));
         logger.debug("Nombre Plan:" + oPlan.getNombrePlan() 
			 + ", Requiere Inspeccion:" + oPlan.getInspeccionVehi() 
			 + "" + ", Maximo dias inicio vigencia:" + oPlan.getMaxDiaIniVig() + "");
         String primaMensualUF = String.valueOf(oForm.getDatos().get("primaPlanUF"));
         String primaAnualUF = String.valueOf(oForm.getDatos().get("primaAnualUF"));
         oPlan.setPrimaMensualUF(Float.parseFloat(primaMensualUF.replaceAll(",", ".")));
         oPlan.setPrimaAnualUF(Float.parseFloat(primaAnualUF.replaceAll(",", ".")));
         oPlan.setPrimaAnualPesos(Long.parseLong((String)oForm.getDatos().get("primaAnualPesos")));
         oCotizacionSeguro.setNumDiaMaxIniVig(oPlan.getMaxDiaIniVig());
         request.getSession().setAttribute("maxFechaVig", oCotizacionSeguro.getNumDiaMaxIniVig());
         request.setAttribute("maxFechaVig", oCotizacionSeguro.getNumDiaMaxIniVig());
         System.out.println("FECHA LIMITE: "+oCotizacionSeguro.getNumDiaMaxIniVig());
         session.setAttribute("datosCotizacion", oCotizacionSeguro);
         long idSolicitud = 0L;
         Solicitud cookie;
         String idVitrineo = (String)request.getSession().getAttribute("claveVitrineo");  
         if(oCotizacionSeguro instanceof CotizacionSeguroVehiculo) {
            idSolicitud = oDelegate.ingresarCotizacionVehiculo((CotizacionSeguroVehiculo)oCotizacionSeguro, 
            	idRama, idProducto, oPlan, idVitrineo);
            logger.debug("Nombre Plan:" + oPlan.getNombrePlan() + ",  Compannia:" + oPlan.getCompannia() + "");
            forward = "continuar-vehiculo";
            if(enviar != null && enviar.length() > 0) {
               cookie = oDelegate.obtenerDatosSolicitud(idSolicitud);
               oDelegate.enviarCotizacionVehiculoPDF((CotizacionSeguroVehiculo)oCotizacionSeguro, cookie, usuario);
               forward = "continuar-email";
            }
         } else if(oCotizacionSeguro instanceof CotizacionSeguroVida) {
            idSolicitud = oDelegate.ingresarCotizacionVida((CotizacionSeguroVida)oCotizacionSeguro, idRama, idProducto, oPlan, idVitrineo);
            forward = "continuar-vida";
            if(enviar != null && enviar.length() > 0) {
               cookie = oDelegate.obtenerDatosSolicitud(idSolicitud);
               oDelegate.enviarCotizacionVidaPDF((CotizacionSeguroVida)oCotizacionSeguro, cookie, usuario);
               forward = "continuar-email";
            }
         } else if(oCotizacionSeguro instanceof CotizacionSeguroHogar) {
            oCotizacionSeguro.getContratante().setIdCiudad(oCotizacionSeguro.getCiudad());
            oCotizacionSeguro.getContratante().setIdComuna(oCotizacionSeguro.getComuna());
            oCotizacionSeguro.getContratante().setIdRegion(oCotizacionSeguro.getRegion());
            idSolicitud = oDelegate.ingresarCotizacionHogar((CotizacionSeguroHogar)oCotizacionSeguro, idRama, idProducto, oPlan, idVitrineo);
            forward = "continuar-hogar";
            if(enviar != null && enviar.length() > 0) {
               logger.info("ENVIAR GUARDAR COTIZACION: " + enviar + "");
               cookie = oDelegate.obtenerDatosSolicitud(idSolicitud);
               oDelegate.enviarCotizacionHogarPDF((CotizacionSeguroHogar)oCotizacionSeguro, cookie, usuario);
               forward = "continuar-email";
            }

            logger.info("FORWARD GUARDAR COTIZACION: " + forward + "");
         }

         Cookie cookie1 = new Cookie("idSolicitudCookie", String.valueOf(idSolicitud));
         int vidaCookie = Integer.parseInt(ACVConfig.getInstance().getString("cl.cencosud.asesorcotizador.solicitud.vidaCookie.segundos"));
         cookie1.setMaxAge(vidaCookie);
         response.addCookie(cookie1);
         oPlan.setFormaPago(plan.getFormaPago());
         oPlan.setSolicitaAutoNuevo(plan.getSolicitaAutoNuevo());
         oPlan.setSolicitaFactura(plan.getSolicitaFactura());
         session.setAttribute("idSolicitud", Long.valueOf(idSolicitud));
         session.setAttribute("datosPlan", oPlan);
         return mapping.findForward(forward);
      }
   }

}
