// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:20
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import cl.cencosud.acv.common.Vehiculo;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import com.tinet.exceptions.system.SystemException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

public class BuscarMarcasVehiculoAction extends Action {

   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
      CotizacionDelegate delegate = new CotizacionDelegate();

      try {
         Vehiculo[] e = new Vehiculo[0];
         if(request.getParameter("idTipoVehiculo") != null && request.getParameter("idTipoVehiculo").length() > 0) {
            String pwritter = request.getParameter("idTipoVehiculo");
            e = delegate.obtenerMarcasPorTipoVehiculos(pwritter);
         }

         response.setContentType("text/html");
         PrintWriter pwritter1 = response.getWriter();
         ObjectMapper mapper = new ObjectMapper();
         StringWriter json = new StringWriter();
         mapper.writeValue(json, e);
         pwritter1.write(json.toString());
         return null;
      } catch (IOException var10) {
         throw new SystemException(var10);
      }
   }
}
