// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:19
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

public class RevisarSesionAction extends Action {

   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
      String estado = "";
      if(request.getAttribute("STATE") != null) {
         estado = request.getAttribute("STATE").toString();
      } else {
         estado = "activa";
      }

      response.setContentType("text/html");
      PrintWriter pwritter = response.getWriter();
      ObjectMapper mapper = new ObjectMapper();
      StringWriter json = new StringWriter();
      mapper.writeValue(json, estado);
      pwritter.write(json.toString());
      return null;
   }
}
