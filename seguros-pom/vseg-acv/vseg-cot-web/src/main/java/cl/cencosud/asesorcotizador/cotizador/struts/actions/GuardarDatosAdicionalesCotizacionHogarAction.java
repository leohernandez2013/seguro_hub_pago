// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:19
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import cl.cencosud.acv.common.Contratante;
import cl.cencosud.acv.common.exception.ProcesoCotizacionException;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.tinet.common.model.exception.BusinessException;
import cl.tinet.common.seguridad.exception.AutenticacionException;
import cl.tinet.common.seguridad.model.Usuario;
import cl.tinet.common.seguridad.util.SeguridadUtil;
import cl.tinet.common.struts.form.BuilderActionFormBaseCOT;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class GuardarDatosAdicionalesCotizacionHogarAction extends Action {

   private static Log logger = LogFactory.getLog(GuardarDatosAdicionalesCotizacionHogarAction.class);


   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws BusinessException, ParseException {
      try {
         if(request.getSession().getAttribute("idSolicitud") == null) {
            throw new ProcesoCotizacionException("cl.cencosud.ventaseguros.cotizacion.exception.NO_SOLICITUD", new Object[0]);
         }

     	// Validar si corresponde a compra sin registro
     	if(request.getParameter("sinRegistro") == null) {
    		request.getSession().removeAttribute("sinRegistro");
    		request.getSession().removeAttribute("rutSinRegistro");
    		
            this.verificarAutenticacion(request, response);
            Usuario forward = SeguridadUtil.getUsuario(request);
            if(forward.isCambioClaveRequerido()) {
               request.getSession().setAttribute("frameClaveTemporal", "true");
               return mapping.getInputForward();
            }     		
     	}else{
        	String sinRegistro = request.getParameter("sinRegistro");
        	String rutSinRegistro = request.getParameter("rutSinRegistro");
    		request.getSession().setAttribute("sinRegistro", sinRegistro);
    		request.getSession().setAttribute("rutSinRegistro", rutSinRegistro);
    	}

         long idSolicitud = Long.parseLong(String.valueOf(request.getSession().getAttribute("idSolicitud")));
         logger.info("Guardar Datos Adicionales Hogar: solicitud -> " + idSolicitud + "");
         BuilderActionFormBaseCOT oForm = (BuilderActionFormBaseCOT)form;
         CotizacionSeguroHogar oCotizacionSeguro = new CotizacionSeguroHogar();
         CotizacionSeguroHogar cotHogar = (CotizacionSeguroHogar)request.getSession().getAttribute("datosCotizacion");

         try {
            BeanUtils.copyProperties(oCotizacionSeguro, cotHogar);
         } catch (IllegalAccessException var13) {
            logger.error("Error al copiar datos", var13);
         } catch (InvocationTargetException var14) {
            logger.error("Error al copiar datos 2", var14);
         }

         oCotizacionSeguro.setContratante(cotHogar.getContratante());
         this.obtenerDatosAdicionalesHogar(oForm, oCotizacionSeguro);
         oCotizacionSeguro.setClienteEsAsegurado(cotHogar.isClienteEsAsegurado());
         oCotizacionSeguro.setTramoDias(cotHogar.getTramoDias());
         
         //inicio hogar vacaciones
         
         oCotizacionSeguro.setFecIniVig((String)oForm.getDatos().get("fechaInicio"));
         
         //fin hogar vacaciones
         
         CotizacionDelegate oDelegate = new CotizacionDelegate();
         logger.info("Guardar Datos Adicionales Vehiculo: INICIO GUARDAR DATOS BIGSA");
         int resultNroBigsa = oDelegate.ingresarDatosAdicionalesHogar(idSolicitud, oCotizacionSeguro);
         logger.info("Guardar Datos Adicionales Hogar: TERMINO GUARDAR DATOS BIGSA, NUMERO GENERADO: " + resultNroBigsa + "");
         request.getSession().setAttribute("resultNroBigsa", Integer.valueOf(resultNroBigsa));
      } catch (AutenticacionException var15) {
         logger.info("Error autenticando usuario.", var15);
         request.setAttribute("error-login", var15.getMessage(request.getLocale()));
         return mapping.getInputForward();
      }

      String forward1 = "continuar";
      request.getSession().setAttribute("datosVolverPaso2", ((BuilderActionFormBaseCOT)form).getDatos());
      return mapping.findForward(forward1);
   }

   private void obtenerDatosAdicionalesHogar(BuilderActionFormBaseCOT form, CotizacionSeguroHogar oCotizacion) throws ParseException {
      String numeroTelefono1 = (String)form.getDatos().get("cteCodigoTelefono1") + (String)form.getDatos().get("cteNumeroTelefono1");
      String numeroTelefono2 = (String)form.getDatos().get("cteCoditoTelefono2") + (String)form.getDatos().get("cteNumeroTelefono2");
      Contratante contratante = oCotizacion.getContratante();
      contratante.setTipoTelefono1(oCotizacion.getTipoTelefono1());
      contratante.setTelefono1(numeroTelefono1);
      contratante.setTipoTelefono2(oCotizacion.getTipoTelefono2());
      contratante.setTelefono2(numeroTelefono2);
      contratante.setIdCiudad((String)form.getDatos().get("cteCiudad"));
      contratante.setIdComuna((String)form.getDatos().get("cteComuna"));
      contratante.setIdRegion((String)form.getDatos().get("cteRegion"));
      contratante.setCalleDireccion((String)form.getDatos().get("cteCalle"));
      contratante.setNumeroDireccion((String)form.getDatos().get("cteNumeroCalle"));
      contratante.setNumeroDepartamentoDireccion((String)form.getDatos().get("cteNumeroDepto"));
      oCotizacion.setContratante(contratante);
      if(form.getDatos().get("asegRegion") != null) {
         Contratante asegurado = new Contratante();
         asegurado.setIdRegion((String)form.getDatos().get("asegRegion"));
         asegurado.setIdComuna((String)form.getDatos().get("asegComuna"));
         asegurado.setIdCiudad((String)form.getDatos().get("asegCiudad"));
         oCotizacion.getAsegurado().setIdCiudad(asegurado.getIdCiudad());
         oCotizacion.getAsegurado().setIdComuna(asegurado.getIdComuna());
         oCotizacion.getAsegurado().setIdRegion(asegurado.getIdRegion());
         asegurado.setCalleDireccion((String)form.getDatos().get("asegCalle"));
         asegurado.setNumeroDireccion((String)form.getDatos().get("asegNumeroCalle"));
         asegurado.setNumeroDepartamentoDireccion((String)form.getDatos().get("asegNumeroDepto"));
         String anyo = (String)form.getDatos().get("asegAnyoNac");
         String dia = ((String)form.getDatos().get("asegDiaNac")).length() == 1?"0".concat((String)form.getDatos().get("asegDiaNac")):(String)form.getDatos().get("asegDiaNac");
         String mes = ((String)form.getDatos().get("asegMesNac")).length() == 1?"0".concat((String)form.getDatos().get("asegMesNac")):(String)form.getDatos().get("asegMesNac");
         Date fechaNacAseg = (new SimpleDateFormat("dd-MM-yyyy")).parse(dia + "-" + mes + "-" + anyo);
         asegurado.setFechaDeNacimiento(fechaNacAseg);
         asegurado.setEstadoCivil((String)form.getDatos().get("estadoCivil"));
         asegurado.setSexo((String)form.getDatos().get("asegSexo"));
         asegurado.setTipoTelefono1((String)form.getDatos().get("asegTipoTelefono1"));
         numeroTelefono1 = (String)form.getDatos().get("asegCodigoTelefono1") + (String)form.getDatos().get("asegNumeroTelefono1");
         asegurado.setTelefono1(numeroTelefono1);
         asegurado.setTipoTelefono2((String)form.getDatos().get("asegTipoTelefono2"));
         numeroTelefono2 = (String)form.getDatos().get("asegCoditoTelefono2") + (String)form.getDatos().get("asegNumeroTelefono2");
         asegurado.setTelefono2(numeroTelefono2);
         oCotizacion.setAsegurado(asegurado);
      }

   }

   protected void verificarAutenticacion(HttpServletRequest request, HttpServletResponse response) throws AutenticacionException {
      if(!SeguridadUtil.isAutenticado(request)) {
         logger.debug("Usuario no autenticado. Autenticando.");
         SeguridadUtil.login(request, response, 1);
      } else {
         logger.debug("Usuario autenticado, se continua normalmente.");
      }

   }

}
