// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:18
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.paso3.struts.actions;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import cencosud.cl.paymentHub.payment.model.RequestInitPayment;
import cencosud.cl.paymentHub.payment.model.ResponseInitPayment;
import cencosud.cl.paymentHub.payment.service.IPaymentService;
import cencosud.cl.paymentHub.payment.service.PaymentServiceImpl;
import cl.cencosud.acv.common.Plan;
import cl.cencosud.acv.common.Transaccion;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.cencosud.asesorcotizador.delegate.PagosDelegate;
import cl.tinet.common.seguridad.config.SeguridadConfig;
import cl.tinet.common.util.crypto.CryptoUtil;

public class GrabarDatosPagoAction extends Action {

   private static Log logger = LogFactory.getLog(GrabarDatosPagoAction.class);
   private static String KEY_URL_RETURN_AFTER_PAYMENT = "cencosud.cl.return.payment";
   private static String URL_WSDL_PAYMENT = "cencosud.cl.paymentHub.payment.wsdl";
   private static final String GROUP_HUB ="CONFIG_HUB";


   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
      logger.debug("Grabar datos Pago");
      HashMap res = new HashMap();

      try {
    	 
         long pwriter = ((Long)request.getSession().getAttribute("idSolicitud")).longValue();
         int json = Long.valueOf(pwriter).intValue();
         String nroTarjetaEntrada = request.getParameter("nroTarjeta");
         CryptoUtil cryptoUtil = new CryptoUtil(SeguridadConfig.getInstance());
         nroTarjetaEntrada = CryptoUtil.toBase64String(cryptoUtil.getEncrypted(nroTarjetaEntrada), true);
         Integer numeroOrdenCompra = (Integer)request.getSession().getAttribute("resultNroBigsa");
         String cargoRecurrente = request.getParameter("cargoRecurrente");
         int tipoCargo = cargoRecurrente != null?1:0;
         String tipoTarjeta = request.getParameter("tipoTarjeta");
         /**
          *  Se agrega nuevo medio de pago obtenido desde Hub de pago 
          *  CONTROL DE CAMBIO KCC
          */
         String fopId = request.getParameter("fopId"); 
         CotizacionDelegate delegate = new CotizacionDelegate();
         Map mTipoTarjeta = delegate.obtenerParametro("WEBPAY", tipoTarjeta);
         int tipoMedioPago = Integer.valueOf((String)mTipoTarjeta.get("valor")).intValue();
         PagosDelegate pagosDelegate = new PagosDelegate();
         Transaccion transaccion = new Transaccion();
         transaccion.setEstado_transaccion("PREVIA_WEBPAY");
         transaccion.setId_solicitud((long)json);
         transaccion.setNro_tarjeta_entrada(nroTarjetaEntrada);
         transaccion.setNumero_orden_compra(String.valueOf(numeroOrdenCompra));
         transaccion.setTipo_cargo(tipoCargo);
         transaccion.setTipo_medio_pago(tipoMedioPago);
         Plan datosPlan = (Plan)request.getSession().getAttribute("datosPlan");
         logger.debug("TRANSACCION: " + transaccion.getId_transaccion());
         logger.debug("SOLICITUD: " + transaccion.getId_solicitud());
         
         if("WEBPAY".equals(tipoTarjeta) && "1".equals(cargoRecurrente)) {
            logger.debug("PAGO CON WEBPAY, SE MODIFICA MONTO A PROPORCIONAL.");
            Long primaAnualPesos = Long.valueOf(datosPlan.getPrimaAnualPesos());
            transaccion.setMonto(this.calculoPrimaProporcional(primaAnualPesos.longValue()));
            logger.info("PROPORCIONAL CALCULADO: " + transaccion.getMonto());
         } else if(!"1".equals(cargoRecurrente)) {
            logger.debug("PRIMA UNICA MONTO A PAGAR ES LA PRIMA MENSUAL");
            transaccion.setMonto(datosPlan.getPrimaMensualPesos());
            logger.debug("PRIMA MENSUAL: " + transaccion.getMonto());
         }
         
        
         
         //INICIO COMPRA SIN REGISTRO
         String aceptoSinRegistro = request.getParameter("aceptoSinRegistro") == null ? "" : request.getParameter("aceptoSinRegistro");
         if(aceptoSinRegistro.equals("aceptaInfo")){
        	 CotizacionSeguro cotizacionSeguro = (CotizacionSeguro)request.getSession().getAttribute("datosCotizacion");
        	 String rutCliente = cotizacionSeguro.getRutCliente()+"-"+cotizacionSeguro.getDv();
        	 String nombreCliente = cotizacionSeguro.getNombre()+" "+cotizacionSeguro.getApellidoPaterno()+" "+cotizacionSeguro.getApellidoMaterno(); 
        	 String mailCliente = cotizacionSeguro.getEmail();
        	 String tipoSeguro = String.valueOf(request.getSession().getAttribute("subcategoriaDesc"));     
        	 
        	 pagosDelegate.guardarNoRegistrado(rutCliente, nombreCliente, mailCliente, tipoSeguro);
        	  
         }//FIN COMPRA SIN REGISTRO
      
         /**  
          * KCC: Incorporacion de Hub de pago 
          */
         if("WEBPAY".equals(tipoTarjeta) || "REDCOMPRA".equals(tipoTarjeta)){
        	 CotizacionSeguro cotizacionSeguro = (CotizacionSeguro)request.getSession().getAttribute("datosCotizacion");
        	 ResponseInitPayment responseWS = iniciarPago(transaccion.getNumero_orden_compra(),cotizacionSeguro.getNombre(),cotizacionSeguro.getApellidoPaterno(),String.valueOf(transaccion.getMonto()), "",fopId);
        	 
        	String urlWebpay = responseWS.getPaymentUrl() +"&fopId="+fopId;
     		res.put("paymentId", responseWS.getPaymentId());
        	res.put("urlWebpay", urlWebpay);
        	request.getSession().setAttribute("paymentId", responseWS.getPaymentId());
         }
         
         pagosDelegate.actualizarTransaccion(transaccion);
         res.put("status", "ok");
      } catch (Exception var22) {
         logger.error("ERROR AL ACTUALIZAR TRANSACCION", var22);
         res.put("status", "error");
      }

      response.setHeader("pragma", "no-cache");
      response.setHeader("cache-control", "no-cache");
      response.setDateHeader("expires", -1L);
      response.setContentType("text/html");
      PrintWriter pwriter1 = response.getWriter();
      ObjectMapper mapper = new ObjectMapper();
      StringWriter json1 = new StringWriter();
      mapper.writeValue(json1, res);
      pwriter1.write(json1.toString());
      return null;
   }

	private long calculoPrimaProporcional(long primaMensual) {
		GregorianCalendar cal = new GregorianCalendar();
		int diasMesVenta = cal.getActualMaximum(5);
		int diasProporcion = diasMesVenta - cal.get(5);
		long primaDia = primaMensual / 12L / (long) diasMesVenta;
		long primaProporcional = primaDia * (long) (diasProporcion + 1);
		return primaProporcional;
	}


	/**
	 * Iniciar Pago
	 */
	private ResponseInitPayment iniciarPago(String orderId, String nombre,
			String apellido, String monto, String currency, String fopId) {
	
		PagosDelegate pagosDelegate = new PagosDelegate();
		String urlRetorno = pagosDelegate.obtenerParametroSistema(GROUP_HUB, KEY_URL_RETURN_AFTER_PAYMENT);
		String urlWsdl =  pagosDelegate.obtenerParametroSistema(GROUP_HUB, URL_WSDL_PAYMENT);
		RequestInitPayment request = new RequestInitPayment();
		request.setCommerceSessionId(orderId);
		request.setOrderId(orderId);
		request.setReturnUrl(urlRetorno);
		request.getClientName().setFirstName(nombre);
		request.getClientName().setLastName(apellido);
		request.setAmount(monto);
		request.setFopId(fopId);
		
		IPaymentService paymentService = new PaymentServiceImpl();
		
		return paymentService.initPayment(urlWsdl,request);
	
	}
  
}
