// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:20
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.forms;

import cl.cencosud.acv.common.Plan;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.tinet.common.seguridad.model.Usuario;
import cl.tinet.common.seguridad.util.SeguridadUtil;
import cl.tinet.common.struts.form.BuilderActionFormBaseCOT;
import cl.tinet.common.util.validate.ValidacionUtil;
import com.tinet.comun.util.date.DateUtil;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

public class IngresoDatosVehiculoForm extends BuilderActionFormBaseCOT {

   private static final long serialVersionUID = 7741487498032766019L;
   private FormFile archivoFactura;


   public FormFile getArchivoFactura() {
      return this.archivoFactura;
   }

   public void setArchivoFactura(FormFile archivoFactura) {
      this.archivoFactura = archivoFactura;
   }

   public InputStream getValidationRules(HttpServletRequest request) {
      return IngresoDatosVehiculoForm.class.getResourceAsStream("resource/validation-datos-vehiculo.xml");
   }

   public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
      Usuario usuario = SeguridadUtil.getUsuario(request);
      ActionErrors errores = new ActionErrors();
      //INICIO REDUCCION DE CAMPOS TF
      if(!((String)this.getDatos().get("nombre")).equals("") && !((String)this.getDatos().get("apellidoPaterno")).equals("") && !((String)this.getDatos().get("apellidoMaterno")).equals("")) {
          if(errores.get("datos.nombre").hasNext() || errores.get("datos.apellidoPaterno").hasNext() || errores.get("datos.apellidoMaterno").hasNext()) {
        	  errores.add("datos.nombres", new ActionMessage("errors.validacion"));
          }
       } else {
    	   errores.add("datos.nombres", new ActionMessage("errors.required"));
       }
      //FIN REDUCCION DE CAMPOS TF
      
      if(!((String)this.getDatos().get("cteTipoTelefono1")).equals("") && !((String)this.getDatos().get("cteCodigoTelefono1")).equals("") && !((String)this.getDatos().get("cteNumeroTelefono1")).equals("")) {
         try {
            if(Long.parseLong((String)this.getDatos().get("cteNumeroTelefono1")) <= 0L) {
               errores.add("datos.telefono1_1", new ActionMessage("errors.validacion"));
               errores.add("datos.telefono1_2", new ActionMessage("errors.validacion"));
               errores.add("datos.telefono3", new ActionMessage("errors.validacion"));
            }
         } catch (NumberFormatException var17) {
            errores.add("datos.telefono1_1", new ActionMessage("errors.validacion"));
            errores.add("datos.telefono1_2", new ActionMessage("errors.validacion"));
            errores.add("datos.telefono1_3", new ActionMessage("errors.validacion"));
         }
      } else {
         errores.add("datos.telefono1_1", new ActionMessage("errors.required"));
         errores.add("datos.telefono1_2", new ActionMessage("errors.required"));
         errores.add("datos.telefono1_3", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("cteRegion")).equals("")) {
         errores.add("cteRegion", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("cteComuna")).equals("")) {
         errores.add("cteComuna", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("cteCiudad")).equals("")) {
         errores.add("cteCiudad", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("cteNumeroCalle")).equals("") || ((String)this.getDatos().get("cteCalle")).equals("")) {
         errores.add("datos.calle1", new ActionMessage("errors.required"));
         errores.add("datos.nro1", new ActionMessage("errors.required"));
         errores.add("datos.depto1", new ActionMessage("errors.required"));
      }

      if(!((String)this.getDatos().get("cteNumeroCalle")).equals("")) {
         try {
            Long.parseLong((String)this.getDatos().get("cteNumeroCalle"));
         } catch (NumberFormatException var16) {
            errores.add("datos.calle1", new ActionMessage("errors.validacion"));
            errores.add("datos.nro1", new ActionMessage("errors.validacion"));
            errores.add("datos.depto1", new ActionMessage("errors.validacion"));
         }
      }

      GregorianCalendar fechaHoy;
      Date e;
      if(this.getDatos().get("esNuevo") != null && !((String)this.getDatos().get("esNuevo")).equals("")) {
         Plan datosCotizacion = (Plan)request.getSession().getAttribute("datosPlan");
         if(datosCotizacion.getSolicitaFactura().intValue() == 1 && ((String)this.getDatos().get("esNuevo")).equalsIgnoreCase("1")) {
            String ne = this.getArchivoFactura().getFileName();
            String fechaInicio = "";
            if(ne.split("\\.").length > 1) {
               String[] fechaTermino = ne.split("\\.");
               fechaInicio = fechaTermino[fechaTermino.length - 1];
            }

            if(this.getArchivoFactura().getFileName().equals("")) {
               errores.add("factura", new ActionMessage("errors.required"));
            } else if(!fechaInicio.equalsIgnoreCase("jpg") && !fechaInicio.equalsIgnoreCase("png") && !fechaInicio.equalsIgnoreCase("tiff") && !fechaInicio.equalsIgnoreCase("gif") && !fechaInicio.equalsIgnoreCase("bmp") && !fechaInicio.equalsIgnoreCase("pdf")) {
               errores.add("factura", new ActionMessage("errors.filetype.wrong"));
            } else if(this.getArchivoFactura().getFileSize() < 5120) {
               errores.add("factura", new ActionMessage("errors.filesize.low"));
            }

            if(!((String)this.getDatos().get("diaFactura")).equals("") && !((String)this.getDatos().get("mesFactura")).equals("") && !((String)this.getDatos().get("anyoFactura")).equals("")) {
               String fechaTermino1 = ((String)this.getDatos().get("diaFactura")).length() < 2?"0" + (String)this.getDatos().get("diaFactura"):(String)this.getDatos().get("diaFactura");
               fechaTermino1 = fechaTermino1 + (((String)this.getDatos().get("mesFactura")).length() < 2?"0" + (String)this.getDatos().get("mesFactura"):(String)this.getDatos().get("mesFactura"));
               fechaTermino1 = fechaTermino1 + (String)this.getDatos().get("anyoFactura");
               if(!ValidacionUtil.isFechaValida(fechaTermino1, "ddMMyyyy")) {
                  errores.add("fechaFactura", new ActionMessage("errors.validacion"));
                  errores.add("diasFecha", new ActionMessage("errors.validacion"));
                  errores.add("mesFecha", new ActionMessage("errors.validacion"));
                  errores.add("anyoFecha", new ActionMessage("errors.validacion"));
               }

               if(!errores.get("fechaFactura").hasNext()) {
                  Date fechaMaxima = DateUtil.getFecha(fechaTermino1, "ddMMyyyy");
                  fechaHoy = new GregorianCalendar();
                  fechaHoy.set(11, 0);
                  fechaHoy.set(12, 0);
                  fechaHoy.set(13, 0);
                  e = fechaHoy.getTime();
                  fechaHoy.add(5, -3);
                  Date fechaPresente = fechaHoy.getTime();
                  if(!DateUtil.esMenorQue(fechaPresente, fechaMaxima) || !DateUtil.esMenorQue(fechaMaxima, e)) {
                     errores.add("fechaFactura", new ActionMessage("errors.validacion.48horas"));
                     errores.add("diasFecha", new ActionMessage("errors.validacion"));
                     errores.add("mesFecha", new ActionMessage("errors.validacion"));
                     errores.add("anyoFecha", new ActionMessage("errors.validacion"));
                  }
               }
            } else {
               errores.add("fechaFactura", new ActionMessage("errors.required"));
               errores.add("diasFecha", new ActionMessage("errors.required"));
               errores.add("mesFecha", new ActionMessage("errors.required"));
               errores.add("anyoFecha", new ActionMessage("errors.required"));
            }
         } else if(((String)this.getDatos().get("patente")).equals("")) {
            errores.add("patente", new ActionMessage("errors.required"));
         } else if(((String)this.getDatos().get("patente")).length() < 5 || ((String)this.getDatos().get("patente")).length() > 6) {
            errores.add("patente", new ActionMessage("errors.validacion"));
         }
      } else {
         errores.add("datos.esNuevo", new ActionMessage("errors.required"));
      }

      if(this.getDatos().get("tipoInspeccion") != null) {
         if(((String)this.getDatos().get("tipoInspeccion")).equals("")) {
            errores.add("tipoInspeccion", new ActionMessage("errors.required"));
         } else if(((String)this.getDatos().get("tipoInspeccion")).length() < 1 || ((String)this.getDatos().get("tipoInspeccion")).length() > 2) {
            errores.add("tipoInspeccion", new ActionMessage("errors.validacion"));
         }
      }

      CotizacionSeguro datosCotizacion1;
      if(this.getDatos().get("fechaInicio") != null) {
         if(((String)this.getDatos().get("fechaInicio")).equals("")) {
            errores.add("diaIniVig", new ActionMessage("errors.required"));
         } else {
            datosCotizacion1 = (CotizacionSeguro)request.getSession().getAttribute("datosCotizacion");
            SimpleDateFormat ne1 = new SimpleDateFormat("dd/MM/yyyy");
            GregorianCalendar fechaInicio1 = new GregorianCalendar();
            GregorianCalendar fechaTermino2 = new GregorianCalendar();
            GregorianCalendar fechaMaxima1 = new GregorianCalendar();
            fechaHoy = new GregorianCalendar();

            try {
               e = ne1.parse((String)this.getDatos().get("fechaInicio"));
               String fechaPresente1 = ne1.format(fechaHoy.getTime());
               String fechaIni = (String)this.getDatos().get("fechaInicio");
               fechaInicio1.setTime(e);
               fechaTermino2.setTime(e);
               fechaMaxima1.add(5, datosCotizacion1.getNumDiaMaxIniVig());
               if(!fechaHoy.before(fechaInicio1) && !fechaPresente1.equals(fechaIni)) {
                  errores.add("diaIniVig", new ActionMessage("errors.fechaAnterior"));
               } else if(!fechaInicio1.before(fechaMaxima1)) {
                  errores.add("diaIniVig", new ActionMessage("errors.fechaSuperaMax"));
               } else {
                  ne1 = new SimpleDateFormat("ddMMyyyy");
                  fechaTermino2.add(2, 1);
                  request.getSession().setAttribute("fechaInicioVig", ne1.format(fechaInicio1.getTime()));
                  request.getSession().setAttribute("fechaTerminoVig", ne1.format(fechaTermino2.getTime()));
               }
            } catch (ParseException var18) {
               errores.add("diaIniVig", new ActionMessage("errors.fechaErronea"));
            }
         }
      }

      if(((String)this.getDatos().get("numeroMotor")).equals("")) {
         errores.add("numeroMotor", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("numeroChasis")).equals("")) {
         errores.add("numeroChasis", new ActionMessage("errors.required"));
      }

      if(((String)this.getDatos().get("colorVehiculo")).equals("")) {
         errores.add("color", new ActionMessage("errors.required"));
      }

      if(this.getDatos().get("esParticular") == null || ((String)this.getDatos().get("esParticular")).equals("")) {
         errores.add("esParticular", new ActionMessage("errors.required"));
      }

      // Validar si corresponde a compra sin registro
      if(request.getParameter("sinRegistro") == null) {
	      if(usuario == null && request.getParameter("password") == null || String.valueOf(request.getParameter("password")).equals("")) {
	          errores.add("clavepaso2", new ActionMessage("errors.required"));
	       }
      }

      datosCotizacion1 = (CotizacionSeguro)request.getSession().getAttribute("datosCotizacion");
      if(datosCotizacion1 != null && !datosCotizacion1.isClienteEsAsegurado()) {
         if(!((String)this.getDatos().get("dueTipoTelefono1")).equals("") && !((String)this.getDatos().get("dueCodigoTelefono1")).equals("") && !((String)this.getDatos().get("dueNumeroTelefono1")).equals("")) {
            try {
               if(Long.parseLong((String)this.getDatos().get("dueNumeroTelefono1")) <= 0L) {
                  errores.add("dueTipoTelefono1", new ActionMessage("errors.required"));
                  errores.add("dueCodigoTelefono1", new ActionMessage("errors.required"));
                  errores.add("dueNumeroTelefono1", new ActionMessage("errors.required"));
               }
            } catch (NumberFormatException var15) {
               errores.add("dueTipoTelefono1", new ActionMessage("errors.required"));
               errores.add("dueCodigoTelefono1", new ActionMessage("errors.required"));
               errores.add("dueNumeroTelefono1", new ActionMessage("errors.required"));
            }
         } else {
            errores.add("dueTipoTelefono1", new ActionMessage("errors.required"));
            errores.add("dueCodigoTelefono1", new ActionMessage("errors.required"));
            errores.add("dueNumeroTelefono1", new ActionMessage("errors.required"));
         }

         if(((String)this.getDatos().get("dueRegion")).equals("")) {
            errores.add("dueRegion", new ActionMessage("errors.required"));
         }

         if(((String)this.getDatos().get("dueComuna")).equals("")) {
            errores.add("dueComuna", new ActionMessage("errors.required"));
         }

         if(((String)this.getDatos().get("dueCiudad")).equals("")) {
            errores.add("dueCiudad", new ActionMessage("errors.required"));
         }

         if(((String)this.getDatos().get("dueCalle")).equals("") || ((String)this.getDatos().get("dueNumeroCalle")).equals("")) {
            errores.add("dueCalle", new ActionMessage("errors.required"));
            errores.add("dueNumeroCalle", new ActionMessage("errors.required"));
            errores.add("dueNumeroDepto", new ActionMessage("errors.required"));
         }

         if(!((String)this.getDatos().get("dueNumeroCalle")).equals("")) {
            try {
               Long.parseLong((String)this.getDatos().get("dueNumeroCalle"));
            } catch (NumberFormatException var14) {
               errores.add("dueCalle", new ActionMessage("errors.validacion"));
               errores.add("dueNumeroCalle", new ActionMessage("errors.validacion"));
               errores.add("dueNumeroDepto", new ActionMessage("errors.validacion"));
            }
         }
      }

      request.setAttribute("cl.tinet.common.struts.validator.PROPERTY_NAMES", errores.properties());
      return errores;
   }
}
