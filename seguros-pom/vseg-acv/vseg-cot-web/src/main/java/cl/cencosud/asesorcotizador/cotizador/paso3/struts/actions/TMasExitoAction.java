// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:18
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.paso3.struts.actions;

import cl.cencosud.acv.common.Contratante;
import cl.cencosud.acv.common.ParametroCotizacion;
import cl.cencosud.acv.common.Plan;
import cl.cencosud.acv.common.SolicitudInspeccion;
import cl.cencosud.acv.common.Transaccion;
import cl.cencosud.acv.common.config.ACVConfig;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.cencosud.asesorcotizador.delegate.PagosDelegate;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class TMasExitoAction extends Action {

   private static Log logger = LogFactory.getLog(TMasExitoAction.class);
   private static String TIPO_RAMO = "cl.cencosud.asesorcotizador.cotizador.paso3.struts.actions.WebPayExitoAction.TIPO_RAMO";
   private static String USER_BSP = "cl.cencosud.asesorcotizador.cotizador.paso3.struts.actions.WebPayExitoAction.USER_BSP";
   private static String TIPO_INSPECCION = "cl.cencosud.asesorcotizador.cotizador.paso3.struts.actions.WebPayExitoAction.TIPO_INSPECCION";
   private static String NOMBRE_CORREDOR = "cl.cencosud.asesorcotizador.cotizador.paso3.struts.actions.WebPayExitoAction.NOMBRE_CORREDOR";


   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
      logger.info("PAGINA DE EXITO TMAS ACCESADA.");
      logger.info("Enviar correo con pdf");
      PagosDelegate delegate = new PagosDelegate();
      Transaccion transaccion = new Transaccion();
      CotizacionSeguro cotizacionSeguro = (CotizacionSeguro)request.getSession().getAttribute("datosCotizacion");
      logger.info("cotizacionSeguro Encontrado en sesion: " + cotizacionSeguro);
      Plan plan = (Plan)request.getSession().getAttribute("datosPlan");
      Contratante contratante = cotizacionSeguro.getAsegurado();
      Integer inumeroOrdenCompra = (Integer)request.getSession().getAttribute("resultNroBigsa");
      String numeroOrdenCompra = String.valueOf(inumeroOrdenCompra);
      logger.info("orden compra: " + numeroOrdenCompra);
      String idRama = (String)request.getSession().getAttribute("idRama");
      logger.info("idRama: " + idRama);
      delegate.enviarEmail(cotizacionSeguro, numeroOrdenCompra, idRama);
      logger.info("Actualizar estado de solicitud: EXITO");
      CotizacionDelegate cotizacionDelegate = new CotizacionDelegate();
      long lIdSolicitud = ((Long)request.getSession().getAttribute("idSolicitud")).longValue();
      cotizacionDelegate.actualizarEstadoSolicitud(lIdSolicitud, "COMPRADA");
      logger.info("RESPUESTA FINAL TMAS: ACEPTADO");
      transaccion.setId_solicitud(lIdSolicitud);
      transaccion.setNumero_orden_compra(numeroOrdenCompra);
      transaccion.setEstado_transaccion("RESPUESTA A TMAS: ACEPTADO");
      logger.info("ACTUALIZANDO ESTADO TRANSACCION TMAS: " + numeroOrdenCompra);
      delegate.actualizarEstadoTransaccion(transaccion);
      logger.info("TERMINO ACTUALIZACION ESTADO TRANSACCION TMAS: " + numeroOrdenCompra);
      logger.info("Registrando solicitud de Inspeccion utilizando WS BSP");
      ACVConfig config = ACVConfig.getInstance();
      ParametroCotizacion[] comunas = cotizacionDelegate.obtenerComunas(contratante.getIdRegion());
      String descComuna = "";
      ParametroCotizacion[] direccion = comunas;
      int parametro = 0;

      for(int htmlCampanna = comunas.length; parametro < htmlCampanna; ++parametro) {
         ParametroCotizacion idSolicitud = direccion[parametro];
         if(idSolicitud.getId().trim().equals(contratante.getIdComuna())) {
            descComuna = idSolicitud.getDescripcion();
            break;
         }
      }

      String var24 = String.valueOf(lIdSolicitud);
      logger.info("Validar tipo de cotizacion.");
    
	/* Se elimina solicitud de inspecci�n a trav�s de WS -- CBM 02-11 --*/
      if(cotizacionSeguro instanceof CotizacionSeguroVehiculo && plan.getInspeccionVehi() == 1) {
     
         logger.info("Es cotizacion de vehiculo.");
         CotizacionSeguroVehiculo var28 = (CotizacionSeguroVehiculo)cotizacionSeguro;
         logger.info("No tiene factura, es auto usado");
         SolicitudInspeccion var26 = new SolicitudInspeccion();
         var26.setId_solicitud(var24);
         var26.setNombre_corredor(config.getString(NOMBRE_CORREDOR));
         var26.setRamo(config.getString(TIPO_RAMO));
         var26.setCod_compannia_inspeccion(plan.getCodigoCompanniaInspeccion());
         var26.setNombre_asegurado(contratante.getNombreCompleto());
         var26.setRut_asegurado(contratante.getRut() + "-" + contratante.getDv());
         var26.setNombre_contacto(contratante.getNombreCompleto());
         String var23 = contratante.getCalleDireccion() + " " + contratante.getNumeroDireccion() + " " + contratante.getNumeroDepartamentoDireccion();
         var26.setDireccion(var23);
         var26.setComuna(descComuna);
         var26.setTelefono_contacto(contratante.getTelefono1());
         var26.setUsuario_interno_bsp(config.getString(USER_BSP));
         var26.setTipo_inspeccion(config.getString(var28.getTipoInspeccion()));
         var26.setObservaciones(plan.getNombrePlan());
         var26.setNumero_patente(var28.getPatente());
         var26.setReferencia_interna(numeroOrdenCompra);
         if(contratante.getTelefono2() != null) {
            var26.setCelular(contratante.getTelefono2());
         }

         var26.setFactura(var28.getFactura());
         logger.debug("Solicitar inspeccion...");
         String idSolicitudInspeccion = delegate.solicitarInspeccion(var26);
         logger.debug("Se obtiene codigo de inspeccion: " + idSolicitudInspeccion);
         request.setAttribute("nroSolicitudInspeccion", idSolicitudInspeccion);
      }


      Map var27 = cotizacionDelegate.obtenerParametro("CAMPANNA", plan.getIdPlan());
      if(var27 != null) {
         logger.info("PARAMETRO CAMPANNA OBTENIDO: " + var27.get("VALOR"));
         String var25 = (String)var27.get("VALOR");
         if(var25 != null && !"".equals(var25)) {
            request.setAttribute("HTML_CAMPANNA", var27.get("VALOR"));
         }
      }

      return mapping.findForward("continuar");
   }
}
