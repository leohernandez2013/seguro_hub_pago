// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:18
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.paso3.struts.actions;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cencosud.cl.paymentHub.payment.model.ResponseGetPayment;
import cencosud.cl.paymentHub.payment.service.IPaymentService;
import cencosud.cl.paymentHub.payment.service.PaymentServiceImpl;
import cl.cencosud.acv.common.Contratante;
import cl.cencosud.acv.common.ParametroCotizacion;
import cl.cencosud.acv.common.Plan;
import cl.cencosud.acv.common.RespuestaTBK;
import cl.cencosud.acv.common.Solicitud;
import cl.cencosud.acv.common.SolicitudInspeccion;
import cl.cencosud.acv.common.Transaccion;
import cl.cencosud.acv.common.config.ACVConfig;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.cencosud.asesorcotizador.delegate.PagosDelegate;

/**
 * 
 * @author Virtual
 * 
 *         Clase encargada de checkear el estado del pago al momento de ir a
 *         transabank
 * 
 * 
 */
public class WebPayCheckPagoAction extends Action {

	private static Log logger = LogFactory.getLog(WebPayCheckPagoAction.class);
	private static String URL_WSDL_PAYMENT = "cencosud.cl.paymentHub.payment.wsdl";
	private static final String GROUP_HUB ="CONFIG_HUB";
	private static final String REDIRECT_PAGO_OK = "payment_successful";
	private static final String REDIRECT_PAGO_FALLIDO = "payment_failed";
	private static final String CODIGO_PAGO_EXITOSO = "1";
	private static final String TIPO_TRANSACCION_NORMAL = "TR_NORMAL";
	private static final ACVConfig config = ACVConfig.getInstance();
	private static String NOMBRE_CORREDOR = "cl.cencosud.asesorcotizador.cotizador.paso3.struts.actions.WebPayExitoAction.NOMBRE_CORREDOR";
	private static String TIPO_RAMO = "cl.cencosud.asesorcotizador.cotizador.paso3.struts.actions.WebPayExitoAction.TIPO_RAMO";
	private static String USER_BSP = "cl.cencosud.asesorcotizador.cotizador.paso3.struts.actions.WebPayExitoAction.USER_BSP";

	/**
	 * Ejecucion de checkeo de pago
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		RespuestaTBK respuestaTbk;
		String urlRedirect = REDIRECT_PAGO_FALLIDO;
		Integer inumeroOrdenCompra = (Integer) request.getSession().getAttribute("resultNroBigsa");
		String paymentId = (String) request.getSession().getAttribute("paymentId");
	    long idSolicitud = ((Long)request.getSession().getAttribute("idSolicitud")).longValue();
		String numeroOrdenCompra = String.valueOf(inumeroOrdenCompra);
		Transaccion transaccion = new Transaccion();
		PagosDelegate delegate = new PagosDelegate();
		

		// Se consulta el estado del pago
		String urlWsdl = delegate.obtenerParametroSistema(GROUP_HUB, URL_WSDL_PAYMENT);
		IPaymentService paymentService = new PaymentServiceImpl();
		ResponseGetPayment responseService = paymentService.getPaymentStatusByPaymentId(urlWsdl,paymentId);
		respuestaTbk = delegate.obtenerDatosRespuesta(Long.valueOf(responseService.getSessionId()!=null?responseService.getSessionId():"0"));
		
		// chequea si el pago esta ingresado en bd
		// si esta ingresado no realiza el flujo normal (esto es un bypass por problema con doble ejecucion del action
		if(respuestaTbk != null){
			if(CODIGO_PAGO_EXITOSO.equalsIgnoreCase(responseService
					.getStatus())){
				request.setAttribute("respuesta", respuestaTbk);
				
				CotizacionDelegate cotizacionDelegate = new CotizacionDelegate();
				Plan plan = (Plan) request.getSession().getAttribute("datosPlan");
				logger.info("orden compra: " + numeroOrdenCompra);
				CotizacionSeguro cotizacionSeguro = (CotizacionSeguro) request
						.getSession().getAttribute("datosCotizacion");
				logger.debug("cotizacionSeguro Encontrado en sesion: "
						+ cotizacionSeguro);
				logger.debug("VALIDAR CAMPANNA DEL PLAN: " + plan.getIdPlan());
				Map parametro = cotizacionDelegate.obtenerParametro("CAMPANNA", plan.getIdPlan());
				if (parametro != null) { 
					logger.debug("PARAMETRO CAMPANNA OBTENIDO: " + parametro.get("VALOR"));
					String htmlCampanna = (String) parametro.get("VALOR");
					if (htmlCampanna != null && !"".equals(htmlCampanna)) {
						request.setAttribute("HTML_CAMPANNA",parametro.get("VALOR"));
					}
				}
				return mapping.findForward(REDIRECT_PAGO_OK);
			}else{
				return mapping.findForward(REDIRECT_PAGO_FALLIDO);
			}
		}
		
		
		String respuesta = "RECHAZADO";
		
		// Se actualiza la transaccion por primera vez
		transaccion.setEstado_transaccion("ESTADO_WEBPAY");
		transaccion.setId_solicitud(idSolicitud);
		transaccion.setNumero_orden_compra((responseService.getBuyOrder()!= null && !responseService.getBuyOrder().equalsIgnoreCase(""))? responseService.getBuyOrder() : numeroOrdenCompra );
		transaccion.setNro_tarjeta_entrada(responseService.getCardNumber());
		transaccion.setRespuesta_medio(responseService.getResponseCode());
		delegate.actualizarEstadoTransaccion(transaccion);
		
		try {
			if (CODIGO_PAGO_EXITOSO.equalsIgnoreCase(responseService
					.getStatus())) {
				logger.info("PRIMER ESTADO TRX: " + respuesta);
				if (this.grabarDatosRespuesta(responseService)) {
					respuesta = "APROBADO";
					boolean aceptaTrx = this.aceptaTransaccion(transaccion,
							responseService);
					logger.info("COMPRA REGISTRADA EN PCBS: " + aceptaTrx);
					urlRedirect = REDIRECT_PAGO_OK;
					if (!aceptaTrx) {
						respuesta = "RECHAZADO";
						logger.info("RECHAZADO POR PCBS");
						urlRedirect = REDIRECT_PAGO_FALLIDO;
					}
				} else {
					respuesta = "RECHAZADO";
					logger.info("TRANSACCION RECHAZADA");
					urlRedirect = REDIRECT_PAGO_FALLIDO;
				}
				logger.info("SEGUNDO ESTADO TRX: " + respuesta);
			} else {
				respuesta = "RECHAZADO";
			}
			logger.info("TERCER ESTADO TRX: " + respuesta);
			

			transaccion.setEstado_transaccion("RESPUESTA A WEBPAY: "
					+ respuesta);
			delegate.actualizarEstadoTransaccion(transaccion);

			CotizacionDelegate cotizacionDelegate = new CotizacionDelegate();
			Plan plan = (Plan) request.getSession().getAttribute("datosPlan");

			logger.info("orden compra: " + numeroOrdenCompra);
			CotizacionSeguro cotizacionSeguro = (CotizacionSeguro) request
					.getSession().getAttribute("datosCotizacion");
			logger.debug("cotizacionSeguro Encontrado en sesion: "
					+ cotizacionSeguro);

			/**
			 * cambio de respuesta en KCC
			 */
			respuestaTbk = delegate.obtenerDatosRespuesta(Long.valueOf(responseService.getSessionId()!=null?responseService.getSessionId():"0"));
			request.setAttribute("respuesta", respuestaTbk);
			logger.debug("VALIDAR CAMPANNA DEL PLAN: " + plan.getIdPlan());
			Map parametro = cotizacionDelegate.obtenerParametro("CAMPANNA", plan.getIdPlan());
			if (parametro != null) { 
				logger.debug("PARAMETRO CAMPANNA OBTENIDO: " + parametro.get("VALOR"));
				String htmlCampanna = (String) parametro.get("VALOR");
				if (htmlCampanna != null && !"".equals(htmlCampanna)) {
					request.setAttribute("HTML_CAMPANNA",parametro.get("VALOR"));
				}
			}

			logger.info("LEVANTAR PAGINA DE EXITO PARA: " + numeroOrdenCompra);
			return mapping.findForward(urlRedirect);

		} catch (Exception e) {
			logger.error("Error enviando respuesta a transbank: ", e);
		} finally {
			logger.info("FIN CerrarWebPayAction");
		}

		return mapping.findForward(REDIRECT_PAGO_FALLIDO);
	}


	private boolean aceptaTransaccion(Transaccion transaccion,
			ResponseGetPayment response) {
		boolean respuestaPCBS = false;
		CotizacionDelegate cotizacionDelegate = new CotizacionDelegate();
		long lIdSolicitud = Long.valueOf(transaccion.getId_solicitud()).longValue();

		try {
			PagosDelegate pagos = new PagosDelegate();
			respuestaPCBS = pagos.aceptaTransaccion(transaccion); // Grabar trx
																	// en PCBS
																	// WsBigsaAceptaCotizacion
			logger.info("Resultado de aceptacion PCBS: " + respuestaPCBS);

			if (respuestaPCBS) {
				logger.info("Marcar compra exitosa");
				this.marcarCompraExitosa(response,lIdSolicitud);
			} else {
				logger.info("Actualizar estado de solicitud: Rechazada");
				cotizacionDelegate.actualizarEstadoSolicitud(lIdSolicitud,
						"CANCELADA");
				respuestaPCBS = false;
			}
		} catch (Exception var5) {
			logger.error("Error al aceptar transaccion", var5);
			respuestaPCBS = false;
			cotizacionDelegate.actualizarEstadoSolicitud(lIdSolicitud,
					"CANCELADA");
		}
		return respuestaPCBS;
	}

	/**
	 * Se conserva Logica de KCC
	 * 
	 * @param request
	 * @return
	 */
	private boolean marcarCompraExitosa(ResponseGetPayment response, long lIdSolicitud) {
		PagosDelegate delegate = new PagosDelegate();
		CotizacionDelegate cotizacionDelegate = new CotizacionDelegate();
		String numeroOrdenCompra = response.getBuyOrder();
//		long lIdSolicitud = Long.valueOf(solicitudId).longValue();
		logger.info("Orden compra: " + numeroOrdenCompra);
		logger.info("Id solicitud: " + lIdSolicitud);
		Object cotizacionSeguro = null;

		try {
			Solicitud solicitud = cotizacionDelegate
					.obtenerDatosSolicitud(lIdSolicitud);
			logger.info("Rama de la solicitud: " + solicitud.getId_rama());

			if (solicitud.getId_rama().equals("1")
					|| solicitud.getId_rama().equals("8")) {
				cotizacionSeguro = cotizacionDelegate
						.obtenerDatosMateriaVehiculo(lIdSolicitud);
			} else if (solicitud.getId_rama().equals("2")) {
				cotizacionSeguro = cotizacionDelegate
						.obtenerDatosMateriaHogar(lIdSolicitud);
			} else {
				cotizacionSeguro = cotizacionDelegate
						.obtenerDatosMateriaVida(lIdSolicitud);
			}

			logger.info("Obtener contratante");
			((CotizacionSeguro) cotizacionSeguro)
					.setContratante(cotizacionDelegate
							.obtenerDatosContratante(lIdSolicitud));
			((CotizacionSeguro) cotizacionSeguro).setCodigoProducto(Long
					.parseLong(solicitud.getId_producto()));

			logger.info("Obtener datos del plan");
			Plan e = cotizacionDelegate
					.obtenerDatosPlan(solicitud.getId_plan());
			Contratante contratante = ((CotizacionSeguro) cotizacionSeguro)
					.getContratante();
			((CotizacionSeguro) cotizacionSeguro).setCodigoPlan(solicitud
					.getId_plan());
			((CotizacionSeguro) cotizacionSeguro).setEmail(contratante
					.getEmail());
			((CotizacionSeguro) cotizacionSeguro).setNombre(contratante
					.getNombre());
			((CotizacionSeguro) cotizacionSeguro).setRutCliente(contratante
					.getRut());

			// Envia Mail
			logger.info("Enviar Mail");
			delegate.enviarEmail((CotizacionSeguro) cotizacionSeguro,
					numeroOrdenCompra, solicitud.getId_rama());

			logger.info("Actualizar estado de solicitud: COMPRADA");
			cotizacionDelegate.actualizarEstadoSolicitud(lIdSolicitud,
					"COMPRADA");

			ACVConfig config = ACVConfig.getInstance();
			ParametroCotizacion[] comunas = cotizacionDelegate
					.obtenerComunas(contratante.getIdRegion());
			String descComuna = "";
			ParametroCotizacion[] direccion = comunas;
			int vehiculo = 0;
			for (int solicitudInspeccion = comunas.length; vehiculo < solicitudInspeccion; ++vehiculo) {
				ParametroCotizacion idSolicitud = direccion[vehiculo];
				if (idSolicitud.getId().trim()
						.equals(contratante.getIdComuna())) {
					descComuna = idSolicitud.getDescripcion();
					break;
				}
			}

			String SidSolicitud = String.valueOf(lIdSolicitud);

			/* Se elimina solicitud de inspecci�n a trav�s de WS -- CBM 02-11 -- */
			if (cotizacionSeguro instanceof CotizacionSeguroVehiculo
					&& e.getInspeccionVehi() == 1) {
				logger.info("Es cotizacion de vehiculo");
				CotizacionSeguroVehiculo var22 = (CotizacionSeguroVehiculo) cotizacionSeguro;
				logger.info("No tiene factura, es auto usado");
				SolicitudInspeccion solicitudBSP = new SolicitudInspeccion();
				solicitudBSP.setId_solicitud(SidSolicitud);
				solicitudBSP.setNombre_corredor(config
						.getString(NOMBRE_CORREDOR));
				solicitudBSP.setRamo(config.getString(TIPO_RAMO));
				solicitudBSP.setCod_compannia_inspeccion(e
						.getCodigoCompanniaInspeccion());
				solicitudBSP.setNombre_asegurado(contratante
						.getNombreCompleto());
				solicitudBSP.setRut_asegurado(contratante.getRut() + "-"
						+ contratante.getDv());
				solicitudBSP
						.setNombre_contacto(contratante.getNombreCompleto());
				String direccionCompleta = contratante.getCalleDireccion()
						+ " " + contratante.getNumeroDireccion() + " "
						+ contratante.getNumeroDepartamentoDireccion();
				solicitudBSP.setDireccion(direccionCompleta);
				solicitudBSP.setComuna(descComuna);
				solicitudBSP.setTelefono_contacto(contratante.getTelefono1());
				solicitudBSP.setUsuario_interno_bsp(config.getString(USER_BSP));
				solicitudBSP.setTipo_inspeccion(var22.getTipoInspeccion());
				solicitudBSP.setObservaciones(e.getNombrePlan());
				solicitudBSP.setNumero_patente(var22.getPatente());
				solicitudBSP.setReferencia_interna(numeroOrdenCompra);
				if (contratante.getTelefono2() != null) {
					solicitudBSP.setCelular(contratante.getTelefono2());
				}
				solicitudBSP.setFactura(var22.getFactura());
				logger.info("Solicitar inspeccion");
				String idSolicitudInspeccion = delegate
						.solicitarInspeccion(solicitudBSP);
			}

			return true;
		} catch (Exception var19) {
			var19.printStackTrace();
			logger.error("traza:" + var19);
			logger.error("ERROR AL MARCAR COMPRA EXITOSA: " + numeroOrdenCompra);
			cotizacionDelegate.actualizarEstadoSolicitud(lIdSolicitud,
					"ERROR_POST_WEBPAY");
			return false;
		}
	}

	/**
	 * 
	 * @param response
	 * @return
	 */
	private boolean grabarDatosRespuesta(ResponseGetPayment responseService) {
		boolean res = false;
		try {
			PagosDelegate e = new PagosDelegate();
			RespuestaTBK respuesta = new RespuestaTBK();
			respuesta.setTBK_CODIGO_AUTORIZACION(responseService.getAuthorizationCode());
			respuesta.setTBK_FECHA_CONTABLE(Integer.parseInt(responseService.getAccountingDate()));
			respuesta.setTBK_FECHA_TRANSACCION(Integer.parseInt(getFechaTransaccion(responseService.getTransactionDate())));		
			respuesta.setTBK_FINAL_NUMERO_TARJETA(Integer.parseInt(responseService.getCardNumber()));
			respuesta.setTBK_HORA_TRANSACCION(Integer.parseInt(getHoraTransaccion(responseService.getTransactionDate())));
			respuesta.setTBK_ID_SESION(responseService.getSessionId());
			respuesta.setTBK_ID_TRANSACCION(Long.parseLong(responseService.getPaymentId()));
			respuesta.setTBK_MAC("0");
			respuesta.setTBK_MONTO(new Double(responseService.getAmount()).intValue());
			respuesta.setTBK_NUMERO_CUOTAS(Integer.parseInt(responseService.getSharesNumber()));
			respuesta.setTBK_ORDEN_COMPRA(responseService.getBuyOrder());
			respuesta.setTBK_RESPUESTA(Integer.parseInt(responseService.getResponseCode()));
			respuesta.setTBK_TASA_INTERES_MAX(0);
			respuesta.setTBK_TIPO_PAGO(responseService.getPaymentType());
			respuesta.setTBK_TIPO_TRANSACCION(TIPO_TRANSACCION_NORMAL);

			e.grabarDatosRespuesta(respuesta); // Guardar tabla RESPUESTA_TBK
			res = true;
		} catch (Exception var5) {
			logger.error("Error al grabar datos de transaccion en BD", var5);
			var5.printStackTrace();
			System.out.println(1);
		}
		logger.info("RESULTADO GRABAR BD: " + res);
		return res;
	}
	
	
	/**
	 * 
	 * @param fechaTransaccion
	 * @return
	 */
	private static String getFechaTransaccion(String fechaTrx){
		return fechaTrx.split("T")[0].replace("-", "").substring(4, 8);
	}
	
	/**
	 * 
	 * @param fechaTransaccion
	 * @return
	 */
	private static String getHoraTransaccion(String fechaTrx){
		return fechaTrx.split("T")[1].split("\\.")[0].replace(":", "");
	}

}
