// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:18
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.ventaseguros.comparador.struts.action;

import cl.cencosud.acv.common.Plan;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.cencosud.ventaseguros.comparador.model.Caracteristica;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class GuardarComparadorAction extends Action {

   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
      LinkedHashMap mapResultComparador = (LinkedHashMap)request.getSession().getAttribute("mapResultComparador");
      CotizacionSeguro oCotizacionSeguro = (CotizacionSeguro)request.getSession().getAttribute("datosCotizacion");
      long rutCliente = oCotizacionSeguro.getRutCliente();
      if(mapResultComparador != null && mapResultComparador.size() > 0) {
         mapResultComparador.put("listPlanes", this.getListCaracteristica((List)mapResultComparador.get("listPlanes")));
         CotizacionDelegate delegate = new CotizacionDelegate();
         delegate.guardarComparadorFront(rutCliente, mapResultComparador);
      }

      return null;
   }

   private List getListCaracteristica(List listPlanes) {
      ArrayList list = new ArrayList();
      Iterator i$ = listPlanes.iterator();

      while(i$.hasNext()) {
         Plan plan = (Plan)i$.next();
         Caracteristica ca = new Caracteristica();
         ca.setId_plan(Long.valueOf(plan.getIdPlan()).longValue());
         ca.setPrimaMensualPesos(plan.getPrimaMensualPesos());
         ca.setPrimaMensualUF(plan.getPrimaMensualUF());
         ca.setIdPlanCompannia(plan.getCompannia());
         list.add(ca);
      }

      return list;
   }
}
