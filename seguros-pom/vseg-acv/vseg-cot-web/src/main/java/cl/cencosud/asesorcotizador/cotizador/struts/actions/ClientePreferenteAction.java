package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import cl.cencosud.asesorcotizador.cotizador.struts.forms.ClientePreferenteForm;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;

public class ClientePreferenteAction extends Action{
	private static Log logger = LogFactory.getLog(ClientePreferenteAction.class);
	
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
			throws Exception {

		CotizacionDelegate delegate = new CotizacionDelegate();
		String rut=(String)((ClientePreferenteForm)form).getDatos().get("rut");	
		String dv=(String)((ClientePreferenteForm)form).getDatos().get("dv");

		if(rut != null && !("").equals(rut.trim()) && validarRut(rut, dv)){
					
			String glosaDescuento = delegate.consultarDescuento(Integer.valueOf(rut));
			logger.info("glosaDescuento: "+glosaDescuento);

			if(glosaDescuento !=null && !("").equals(glosaDescuento)){
				request.getSession().setAttribute("glosaDescuento", glosaDescuento);
			}else{
				request.getSession().removeAttribute("glosaDescuento");
			}
			request.getSession().setAttribute("rutDescuento", rut);
			request.getSession().setAttribute("dvDescuento", dv);
		}
		return mapping.findForward("continuar");

	}
	
	/**
	 * @author Gerardo Catrileo
	 * Valida formato de un rut con modulo 11
	 * @param paramRut
	 * @param paramDv
	 * @return boolean 
	 */
	public static boolean validarRut(String paramRut, String paramDv) {
		boolean validacion = false;
		try {
			String paramRutAux =  paramRut + paramDv.toUpperCase();
			int rutAux = Integer.parseInt(paramRutAux.substring(0, paramRutAux.length() - 1));

			char dv = paramRutAux.charAt(paramRutAux.length() - 1);

			int m = 0, s = 1;
			for (; rutAux != 0; rutAux /= 10) {
				s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
			}
			if (dv == (char) (s != 0 ? s + 47 : 75)) {
				validacion = true;
				logger.info("ConsultarDescuento Rut: "+paramRut+" dv:"+paramDv);
			}else{
				logger.info("ConsultarDescuento Rut Invalido : "+paramRut+" dv:"+paramDv);
			}

		} catch (java.lang.NumberFormatException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return validacion;
	}
	
}
