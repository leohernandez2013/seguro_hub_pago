package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.codehaus.jackson.map.ObjectMapper;

import cl.cencosud.acv.common.PatenteVehiculo;
import cl.cencosud.acv.common.Vehiculo;
import cl.cencosud.asesorcotizador.cotizador.struts.forms.BuscarPatenteForm;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.cencosud.asesorcotizador.ejb.CotizacionBean;
import com.tinet.exceptions.system.SystemException;

public class BuscarPatenteVehiculoAction extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
			throws Exception {
	     // System.out.println("--- PATENTE VEHICULO ---");
	      BuscarPatenteForm patenteForm = (BuscarPatenteForm) form;
	     // System.out.println(patenteForm.getIdPatente());

	      if(patenteForm.getIdPatente() != null){
	    	  CotizacionDelegate delegate = new CotizacionDelegate();
    		  try{
		    	  PatenteVehiculo vehiculo = delegate.obtenerDatosPatente(patenteForm.getIdPatente());
		    	  //System.out.println(vehiculo.getPatente()+","+vehiculo.getMarca()+","+vehiculo.getModelo());
		    	  
		    	  PrintWriter var30 = response.getWriter();
		    	  ObjectMapper mapper = new ObjectMapper();
		    	  StringWriter json = new StringWriter();
		    	  mapper.writeValue(json, vehiculo);
		    	  var30.write(json.toString());
		      }catch(Exception e){
		    	  e.printStackTrace();
		      }
	      }
	      return null;
	   }
}
