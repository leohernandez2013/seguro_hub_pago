// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:20
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import cl.cencosud.acv.common.Actividad;
import cl.cencosud.acv.common.EstadoCivil;
import cl.cencosud.acv.common.ParametroCotizacion;
import cl.cencosud.acv.common.Producto;
import cl.cencosud.acv.common.Solicitud;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida;
import cl.cencosud.asesorcotizador.cotizador.struts.actions.DesplegarCotizacionAction;
import cl.cencosud.asesorcotizador.cotizador.struts.forms.CotizacionVehiculoForm;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.tinet.common.struts.form.BuilderActionFormBaseCOT;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DesplegarCotizacionFraudeAction extends DesplegarCotizacionAction {

	private static final Log logger = LogFactory.getLog(DesplegarCotizacionFraudeAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		HttpSession session = request.getSession();
		CotizacionDelegate delegate = new CotizacionDelegate();
		boolean sw = false;
		String idSubcategoria = "";
		if (request.getParameter("idSubcategoria") != null) {
			request.getSession().setAttribute("idSubcategoria",
					request.getParameter("idSubcategoria"));
			idSubcategoria = request.getParameter("idSubcategoria");
		} else if (request.getSession() != null
				&& request.getSession().getAttribute("idSubcategoria") != null) {
			idSubcategoria = request.getSession().getAttribute("idSubcategoria").toString();
			sw = true;
		}

		String idRama = "";
		if (request.getParameter("idRama") != null) {
			request.getSession().setAttribute("idRama", request.getParameter("idRama"));
			idRama = request.getParameter("idRama");
		} else if (request.getSession() != null
				&& request.getSession().getAttribute("idRama") != null) {
			idRama = request.getSession().getAttribute("idRama").toString();
		}

		String idPlan = request.getParameter("idPlan");
		if (idPlan == null) {
			if (request.getSession().getAttribute("idPlan") != null) {
				idPlan = request.getSession().getAttribute("idPlan").toString();
			} else {
				idPlan = null;
			}
		} else {
			request.getSession().setAttribute("idPlan", idPlan);
		}

		Producto[] productos = delegate.obtenerProductos(idSubcategoria,
				idRama, idPlan);
		if (productos != null && productos.length > 0) {
			request.setAttribute("productos", productos);
		}

		HashMap clienteSession = null;
		byte vaCaptcha = 1;
		session.setAttribute("vaCaptcha", Integer.valueOf(vaCaptcha));
		String keyVitrineo;
		if (idSubcategoria.equals("22")) {
			keyVitrineo = (String) ((CotizacionVehiculoForm) form).getDatos()
					.get("hiddCaptcha");
			if (keyVitrineo == null) {
				clienteSession = this.obtenerDatosClienteSession(request);
			} else {
				vaCaptcha = 0;
				session.setAttribute("vaCaptcha", Integer.valueOf(vaCaptcha));
				clienteSession = this.obtenerDatosClienteSession(request);
			}
		} else {
			clienteSession = this.obtenerDatosClienteSession(request);
		}

		if (((BuilderActionFormBaseCOT) form).getDatos().isEmpty()
				&& clienteSession != null) {
			((BuilderActionFormBaseCOT) form).getDatos().putAll(clienteSession);
		}

		if (request.getAttribute("vitrineo") != null
				&& !"si".equals((String) request.getAttribute("vitrineo"))) {
			logger.info("No se guarda Vitrineo");
		} else {
			logger.info("Guardando Vitrineo");
			keyVitrineo = (String) ((BuilderActionFormBaseCOT) form).getDatos()
					.get("claveVitrineo");
			session.setAttribute("claveVitrineo", keyVitrineo);
			String rut = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("rut");
			String dv = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("dv");
			String tipoTelefono = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("tipoTelefono");
			String codigoTelefono = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("codigoTelefono");
			String estadoCivil = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("estadoCivil");
			String sexo = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("sexo");
			
			int productoVitrineo = Integer
					.parseInt((String) ((BuilderActionFormBaseCOT) form)
							.getDatos().get("producto"));
			String nombre = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("nombre");
			String apellidoPaterno = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("apellidoPaterno");
			String apellidoMaterno = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("apellidoMaterno");
			String diaNac = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("diaFechaNacimiento");
			String mesNac = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("mesFechaNacimiento");
			String anyoNac = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("anyoFechaNacimiento");
			String telefono = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("numeroTelefono");
			String email = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("email");


			// INICIO CONTRATACION TERCEROS
			String esDuenyo = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("contratanteEsDuenyo");
			String rutDuenyo = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("rutDuenyo");
			String dvDuenyo = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("dvDuenyo");
			String nombreDuenyo = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("nombreDuenyo");
			String apellidoPaternoDuenyo = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("apellidoPaternoDuenyo");
			String apellidoMaternoDuenyo = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("apellidoMaternoDuenyo");
			String diaNacimientoDuenyo = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("diaFechaNacimientoDuenyo");
			String mesNacimientoDuenyo = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("mesFechaNacimientoDuenyo");
			String anyoNacimientoDuenyo = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("anyoFechaNacimientoDuenyo");
			String estadoCivilDuenyo = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("estadoCivilDuenyo");
			String regionResidenciaDuenyo = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("regionResidenciaDuenyo");
			String comunaResidenciaDuenyo = (String) ((BuilderActionFormBaseCOT) form)
					.getDatos().get("comunaResidenciaDuenyo");
			// FIN CONTRATACION TERCEROS

			//Campos obligatorios para el metodo grabarVitrineoGeneral
			int comuna = 0;
			int region = 0;
			long edadCondutor = 0;
			int tipoVehiculo = 0;
			int idMarcaVitrineo = 8888;
			int idModeloVitrineo = 8888;
			String idAnyoVitrineo = null;
			int numPuertas = 0;
			String ciudadHogar = null;
			String direccionHogar = null;
			String numeroDireccionHogar = null;
			String numeroDeptoHogar = null;
			String anyoHogar = null;
			int rama = Integer.parseInt(idRama);
			int subcategoria = Integer.parseInt(idSubcategoria);
			String valorComercial = null;
			
			if(keyVitrineo != null && !keyVitrineo.equals("") && 
					rut != null && !rut.equals("") && 
					dv != null && !dv.equals("") && 
					nombre != null && !nombre.equals("") && 
					apellidoPaterno != null && !apellidoPaterno.equals("") && 
					apellidoMaterno != null && !apellidoMaterno.equals("") && 
					tipoTelefono != null && !tipoTelefono.equals("") && 
					codigoTelefono != null && !codigoTelefono.equals("") && 
					telefono != null && !telefono.equals("") && 
					diaNac != null && !diaNac.equals("") && 
					mesNac != null && !mesNac.equals("") && 
					anyoNac != null && !anyoNac.equals("") && 
					estadoCivil != null && !estadoCivil.equals("") && 
					sexo != null && !sexo.equals("")){
				
				logger.info("Guardando Vitrineo");
				delegate.grabarVitrineoGeneral(keyVitrineo, rut,
						dv, nombre, apellidoPaterno, apellidoMaterno,
						tipoTelefono, codigoTelefono, telefono, email, region, comuna, diaNac,
						mesNac, anyoNac, estadoCivil, sexo, edadCondutor, tipoVehiculo,
						idMarcaVitrineo, idModeloVitrineo, idAnyoVitrineo,
						numPuertas, esDuenyo, ciudadHogar, direccionHogar, 
						numeroDireccionHogar,
						numeroDeptoHogar, anyoHogar, rama,
						subcategoria, productoVitrineo, rutDuenyo, dvDuenyo,
						nombreDuenyo, apellidoPaternoDuenyo, apellidoMaternoDuenyo,
						diaNacimientoDuenyo, mesNacimientoDuenyo,
						anyoNacimientoDuenyo, estadoCivilDuenyo,
						regionResidenciaDuenyo, comunaResidenciaDuenyo,
						(String) valorComercial);
			}

			// INICIO MASCOTA
			if (subcategoria == 140){
				String rutDuenoMascota = rut+"-"+dv;
				String nombreMascota = (String) ((BuilderActionFormBaseCOT) form)
						.getDatos().get("nombreMascota");
				String tipoMascota = (String) ((BuilderActionFormBaseCOT) form)
						.getDatos().get("tipoMascota");
				String razaMascota = (String) ((BuilderActionFormBaseCOT) form)
						.getDatos().get("raza");
				String colorMascota = (String) ((BuilderActionFormBaseCOT) form)
						.getDatos().get("colorMascota");
				String edadMascota = (String) ((BuilderActionFormBaseCOT) form)
						.getDatos().get("edadMascota");		
				String sexoMascota = (String) ((BuilderActionFormBaseCOT) form)
						.getDatos().get("sexoMascota");		
				String direccionMascota = (String) ((BuilderActionFormBaseCOT) form)
						.getDatos().get("direccionMascota")+" "+
						(String) ((BuilderActionFormBaseCOT) form)
						.getDatos().get("numeroDirMascota")+" "+
						(String) ((BuilderActionFormBaseCOT) form)
						.getDatos().get("numeroDeptoMascota");		
				
				if(nombreMascota != null && !nombreMascota.equals("") && 
						tipoMascota != null && !tipoMascota.equals("") && 
						razaMascota != null && !razaMascota.equals("") && 
						colorMascota != null && !colorMascota.equals("") && 
						edadMascota != null && !edadMascota.equals("") &&
						sexoMascota != null && !sexoMascota.equals("") && 
						direccionMascota != null && !direccionMascota.equals("")){
					
					logger.info("Guardando datos mascota");
					delegate.guardarMascota(rutDuenoMascota, nombreMascota, tipoMascota,
							razaMascota, colorMascota, edadMascota, sexoMascota, direccionMascota);				
				}				
			}
			// FIN MASCOTA
		}

		Actividad[] actividades = delegate.obtenerActividades();
		request.setAttribute("actividades", actividades);
		
		EstadoCivil[] estadosCiviles = delegate.obtenerEstadosCiviles();
		request.setAttribute("estadosCiviles", estadosCiviles);
		
		ParametroCotizacion[] regiones = delegate.obtenerRegiones();
		request.setAttribute("regiones", regiones);
		
		List tipoTelefono1 = delegate.obtenerParametro("TIPO_TELEFONO");
		request.setAttribute("tipoTelefono", tipoTelefono1.iterator());
		
		List codArea1 = delegate.obtenerParametro("TEL_CODIGO_AREA");
		request.setAttribute("codigoArea", codArea1.iterator());
		
		List meses = delegate.obtenerParametro("MESES");
		request.setAttribute("meses", meses);
		
		List anyos = delegate.obtenerParametro("ANYOS");
		request.setAttribute("anyos", anyos);
		
		if (request.getAttribute("retomaCotizacion") != null) {
			Solicitud solicitud = (Solicitud) request
					.getAttribute("retomaCotizacion");
			logger.info("Se retoma cotizacion:"
					+ solicitud + "");
			CotizacionSeguroVida cotVida1 = delegate
					.obtenerDatosMateriaVida(solicitud.getId_solicitud());
			solicitud.setDatosCotizacion(cotVida1);
			if (solicitud != null) {
				CotizacionSeguroVida vida1 = (CotizacionSeguroVida) solicitud
						.getDatosCotizacion();
				((BuilderActionFormBaseCOT) form).getDatos().put("actividad",
						vida1.getActividad());
				((BuilderActionFormBaseCOT) form).getDatos().put("producto",
						String.valueOf(solicitud.getId_producto()));
				if (solicitud.getCliente_es_asegurado() < 1L) {
					((BuilderActionFormBaseCOT) form).getDatos().put(
							"contratanteEsDuenyo", Boolean.FALSE.toString());
				}
			}
		}

		if (((BuilderActionFormBaseCOT) form).getDatos().get(
				"contratanteEsDuenyo") == null) {
			((BuilderActionFormBaseCOT) form).getDatos().put(
					"contratanteEsDuenyo", Boolean.TRUE.toString());
		}

		if (request.getParameter("idProducto") != null
				&& !request.getParameter("idProducto").equals("")) {
			((BuilderActionFormBaseCOT) form).getDatos().put("producto",
					request.getParameter("idProducto"));
		}

		if (request.getParameter("volverPaso1") != null
				&& request.getSession().getAttribute("datosVolverPaso1") != null) {
			((BuilderActionFormBaseCOT) form).getDatos().putAll(
					(HashMap) request.getSession().getAttribute(
							"datosVolverPaso1"));
			logger.info("Volviendo al paso 1");
			request.getSession().removeAttribute("datosVolverPaso1");
		}

		if (request.getParameter("idPlan") != null) {
			request.getSession().setAttribute("isPromocion", "true");
		} else {
			request.getSession().removeAttribute("isPromocion");
		}

		return mapping.findForward("desplegarFormularioFraude");
	}

}
