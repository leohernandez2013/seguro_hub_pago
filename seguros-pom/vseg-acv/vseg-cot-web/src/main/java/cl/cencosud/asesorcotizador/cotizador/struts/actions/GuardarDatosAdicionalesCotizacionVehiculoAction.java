// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:19
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import cl.cencosud.acv.common.Contratante;
import cl.cencosud.acv.common.Plan;
import cl.cencosud.acv.common.exception.ProcesoCotizacionException;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.asesorcotizador.cotizador.struts.forms.IngresoDatosVehiculoForm;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.tinet.common.model.exception.BusinessException;
import cl.tinet.common.seguridad.exception.AutenticacionException;
import cl.tinet.common.seguridad.model.Usuario;
import cl.tinet.common.seguridad.util.SeguridadUtil;
import cl.tinet.common.struts.form.BuilderActionFormBaseCOT;
import com.tinet.exceptions.system.SystemException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

public class GuardarDatosAdicionalesCotizacionVehiculoAction extends Action {

   private static Log logger = LogFactory.getLog(GuardarDatosAdicionalesCotizacionVehiculoAction.class);


   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws BusinessException, ParseException {
      logger.info("INICIO GUARDAR DATOS ADICIONALES COTIZACION VEHICULO");
      long tiempoInicio = System.currentTimeMillis();
      if(request.getSession().getAttribute("idSolicitud") == null) {
         throw new ProcesoCotizacionException("cl.cencosud.ventaseguros.cotizacion.exception.NO_SOLICITUD", new Object[0]);
      } else {
         try {
        	// Validar si corresponde a compra sin registro
        	if(request.getParameter("sinRegistro") == null) {
        		request.getSession().removeAttribute("sinRegistro");
        		request.getSession().removeAttribute("rutSinRegistro");
        		
	            this.verificarAutenticacion(request, response);
	            Usuario forward = SeguridadUtil.getUsuario(request);
	            if(forward.isCambioClaveRequerido()) {
	               request.getSession().setAttribute("frameClaveTemporal", "true");
	               return mapping.getInputForward();
	            }
        	}else{
            	String sinRegistro = request.getParameter("sinRegistro");
            	String rutSinRegistro = request.getParameter("rutSinRegistro");
        		request.getSession().setAttribute("sinRegistro", sinRegistro);
        		request.getSession().setAttribute("rutSinRegistro", rutSinRegistro);
        	}

            long idSolicitud = Long.parseLong(String.valueOf(request.getSession().getAttribute("idSolicitud")));
            logger.info("Solicitud -> " + idSolicitud);
            BuilderActionFormBaseCOT oForm = (BuilderActionFormBaseCOT)form;
            CotizacionSeguro cotizacionSeguro = (CotizacionSeguro)request.getSession().getAttribute("datosCotizacion");
            CotizacionSeguroVehiculo oCotizacionSeguro = new CotizacionSeguroVehiculo();

            try {
               BeanUtils.copyProperties(oCotizacionSeguro, cotizacionSeguro);
               oCotizacionSeguro.setAsegurado(cotizacionSeguro.getAsegurado());
               oCotizacionSeguro.setNumDiaMaxIniVig(cotizacionSeguro.getNumDiaMaxIniVig());
               oCotizacionSeguro.setContratante(cotizacionSeguro.getContratante());
            } catch (IllegalAccessException var34) {
               logger.error("Error al copiar datos.", var34);
            } catch (InvocationTargetException var35) {
               logger.error("Error al copiar datos 2.", var35);
            }

            this.obtenerDatosAdicionalesVehiculo(oForm, oCotizacionSeguro, request);
            oCotizacionSeguro.setEstadoCivil(cotizacionSeguro.getEstadoCivil());
            oCotizacionSeguro.setClienteEsAsegurado(cotizacionSeguro.isClienteEsAsegurado());
            if(cotizacionSeguro.isClienteEsAsegurado()) {
               oCotizacionSeguro.setAsegurado(oCotizacionSeguro.getContratante());
            }

            FormFile archivo = ((IngresoDatosVehiculoForm)form).getArchivoFactura();
            if(!archivo.getFileName().equals("")) {
               ByteArrayOutputStream oDelegate = new ByteArrayOutputStream();
               InputStream resultNroBigsa = archivo.getInputStream();
               byte[] buffer = new byte[8192];
               boolean bytesLeidos = false;

               int bytesLeidos1;
               while((bytesLeidos1 = resultNroBigsa.read(buffer, 0, 8192)) != -1) {
                  oDelegate.write(buffer, 0, bytesLeidos1);
               }

               oCotizacionSeguro.setFactura(oDelegate.toByteArray());
               oDelegate.close();
               oDelegate = null;
            } else {
               logger.info("Factura no requerida");
               oCotizacionSeguro.setFactura((byte[])null);
            }
            String numSubcategoria = (String)request.getSession().getAttribute("idSubcategoria");
            oCotizacionSeguro.setNumSubcategoria(numSubcategoria);
            
            request.getSession().setAttribute("datosCotizacion", oCotizacionSeguro);
            logger.info("Inspeccion BSP: " + oCotizacionSeguro.getInspeccionBSP());
            CotizacionDelegate oDelegate1 = new CotizacionDelegate();
            logger.info("INICIO GUARDAR DATOS BIGSA");
            logger.info("Cotizacion Seguro:" + oCotizacionSeguro.getIdColor());
            int resultNroBigsa1 = oDelegate1.ingresarDatosAdicionalesVehiculo(idSolicitud, oCotizacionSeguro);
            logger.info("TERMINO GUARDAR DATOS BIGSA, NUMERO GENERADO: " + resultNroBigsa1);
            request.getSession().setAttribute("resultNroBigsa", Integer.valueOf(resultNroBigsa1));
            long tiempoTermino = System.currentTimeMillis();
            long totalTiempo = tiempoTermino - tiempoInicio;
            long hora = totalTiempo / 3600000L;
            long restohora = totalTiempo % 3600000L;
            long minuto = restohora / 60000L;
            long restominuto = restohora % 60000L;
            long segundo = restominuto / 1000L;
            long restosegundo = restominuto % 1000L;
            logger.info("GUARDAR DATOS ADICIONALES VEHICULOS DEMORO horas:" + hora + "Minutos:" + minuto + "Segundos:" + segundo + "." + restosegundo);
         } catch (FileNotFoundException var36) {
            throw new SystemException(var36);
         } catch (IOException var37) {
            throw new SystemException(var37);
         } catch (AutenticacionException var38) {
            logger.info("Error autenticando usuario " + var38);
            request.setAttribute("error-login", var38.getMessage(request.getLocale()));
            return mapping.getInputForward();
         }

         String forward1 = "continuar";
         request.getSession().setAttribute("datosVolverPaso2", ((BuilderActionFormBaseCOT)form).getDatos());
         return mapping.findForward(forward1);
      }
   }

   private void obtenerDatosAdicionalesVehiculo(BuilderActionFormBaseCOT form, CotizacionSeguroVehiculo oCotizacion, HttpServletRequest request) throws ParseException {
	  //INICIO REDUCCION DE CAMPOS TF
	  oCotizacion.setNombre((String)form.getDatos().get("nombre"));
	  oCotizacion.setApellidoPaterno((String)form.getDatos().get("apellidoPaterno"));
	  oCotizacion.setApellidoMaterno((String)form.getDatos().get("apellidoMaterno"));
	  oCotizacion.setEmail((String)form.getDatos().get("email"));
      if(form.getDatos().get("hiddNumeroFono") != null && !((String)form.getDatos().get("hiddNumeroFono")).equals("1")) {
          oCotizacion.setNumeroTelefono1((String)form.getDatos().get("hiddNumeroFono"));
       }
	  //FIN REDUCCION DE CAMPOS TF
	  
	  oCotizacion.setTipoTelefono1((String)form.getDatos().get("cteTipoTelefono1"));
      String numeroTelefono1 = (String)form.getDatos().get("cteCodigoTelefono1") + (String)form.getDatos().get("cteNumeroTelefono1");
      oCotizacion.setNumeroTelefono1(numeroTelefono1);
      oCotizacion.setTipoTelefono2((String)form.getDatos().get("cteTipoTelefono2"));
      String numeroTelefono2 = (String)form.getDatos().get("cteCoditoTelefono2") + (String)form.getDatos().get("cteNumeroTelefono2");
      oCotizacion.setNumeroTelefono2(numeroTelefono2);
      oCotizacion.setRegion((String)form.getDatos().get("cteRegion"));
      oCotizacion.setComuna((String)form.getDatos().get("cteComuna"));
      oCotizacion.setCiudad((String)form.getDatos().get("cteCiudad"));
      oCotizacion.setDireccion((String)form.getDatos().get("cteCalle"));
      oCotizacion.setNumeroDireccion((String)form.getDatos().get("cteNumeroCalle"));
      oCotizacion.setNumeroDepto((String)form.getDatos().get("cteNumeroDepto"));
      Contratante contratante = oCotizacion.getContratante();
      
      //INICIO REDUCCION DE CAMPOS TF
      contratante.setNombre(oCotizacion.getNombre());
      contratante.setApellidoPaterno(oCotizacion.getApellidoPaterno());
      contratante.setApellidoMaterno(oCotizacion.getApellidoMaterno());
      contratante.setEmail(oCotizacion.getEmail());
      contratante.setEstadoCivil(oCotizacion.getEstadoCivil());
      contratante.setSexo(oCotizacion.getSexo());
      //FIN REDUCCION DE CAMPOS TF
      
      contratante.setIdCiudad(oCotizacion.getCiudad());
      contratante.setIdComuna(oCotizacion.getComuna());
      contratante.setIdRegion(oCotizacion.getRegion());
      contratante.setCalleDireccion(oCotizacion.getDireccion());
      contratante.setNumeroDireccion(oCotizacion.getNumeroDireccion());
      contratante.setNumeroDepartamentoDireccion(oCotizacion.getNumeroDepto());
      contratante.setTelefono1(oCotizacion.getNumeroTelefono1());
      Contratante duenyo = new Contratante();

      try {
         BeanUtils.copyProperties(duenyo, oCotizacion.getAsegurado());
      } catch (IllegalAccessException var15) {
         logger.error("error illegal al copiar datos ", var15);
      } catch (InvocationTargetException var16) {
         logger.error("error invocacion al copiar datos ", var16);
      }

      duenyo.setTipoTelefono1((String)form.getDatos().get("dueTipoTelefono1"));
      numeroTelefono1 = (String)form.getDatos().get("dueCodigoTelefono1") + (String)form.getDatos().get("dueNumeroTelefono1");
      duenyo.setTelefono1(numeroTelefono1);
      duenyo.setTipoTelefono2((String)form.getDatos().get("dueTipoTelefono2"));
      numeroTelefono2 = (String)form.getDatos().get("dueCoditoTelefono2") + (String)form.getDatos().get("dueNumeroTelefono2");
      duenyo.setTelefono2(numeroTelefono2);
      CotizacionSeguro cotvehiculo = (CotizacionSeguro)request.getSession().getAttribute("datosCotizacion");
      duenyo.setNombre(cotvehiculo.getAsegurado().getNombre());
      if(cotvehiculo.isClienteEsAsegurado()){
    	  duenyo.setApellidoPaterno(oCotizacion.getApellidoPaterno());
          duenyo.setApellidoMaterno(oCotizacion.getApellidoMaterno());
      }else{
          duenyo.setApellidoPaterno(cotvehiculo.getAsegurado().getApellidoPaterno());
          duenyo.setApellidoMaterno(cotvehiculo.getAsegurado().getApellidoMaterno());
      }
      
      duenyo.setIdRegion((String)form.getDatos().get("dueRegion"));
      duenyo.setIdComuna((String)form.getDatos().get("dueComuna"));
      duenyo.setIdCiudad((String)form.getDatos().get("dueCiudad"));
      duenyo.setCalleDireccion((String)form.getDatos().get("dueCalle"));
      duenyo.setNumeroDireccion((String)form.getDatos().get("dueNumeroCalle"));
      duenyo.setNumeroDepartamentoDireccion((String)form.getDatos().get("dueNumeroDepto"));
      duenyo.setEstadoCivil(oCotizacion.getAsegurado().getEstadoCivil());
      oCotizacion.setAsegurado(duenyo);
      Plan plan = (Plan)request.getSession().getAttribute("datosPlan");
      if(plan.getSolicitaFactura().intValue() == 1 && ((String)form.getDatos().get("esNuevo")).equals("1")) {
         String anyo = (String)form.getDatos().get("anyoFactura");
         String dia = ((String)form.getDatos().get("diaFactura")).length() == 1?"0".concat((String)form.getDatos().get("diaFactura")):(String)form.getDatos().get("diaFactura");
         String mes = ((String)form.getDatos().get("mesFactura")).length() == 1?"0".concat((String)form.getDatos().get("mesFactura")):(String)form.getDatos().get("mesFactura");
         Date fechaFactura = (new SimpleDateFormat("dd-MM-yyyy")).parse(dia + "-" + mes + "-" + anyo);
         oCotizacion.setFechaFactura(fechaFactura);
      }

      logger.info("Tipo Inspeccion:" + (String)form.getDatos().get("tipoInspeccion"));
      logger.info("Necesita Inspeccion:" + (String)form.getDatos().get("reqInspeccion"));
      oCotizacion.setTipoInspeccion((String)form.getDatos().get("tipoInspeccion"));
      oCotizacion.setPatente((String)form.getDatos().get("patente"));
      oCotizacion.setNumeroMotor((String)form.getDatos().get("numeroMotor"));
      oCotizacion.setNumeroChasis((String)form.getDatos().get("numeroChasis"));
      oCotizacion.setIdColor(Long.parseLong((String)form.getDatos().get("colorVehiculo")));
      logger.info("Color Vehiculo: " + Long.parseLong((String)form.getDatos().get("colorVehiculo")));
      logger.info("EsParticular:" + Integer.parseInt((String)form.getDatos().get("esParticular")));
      oCotizacion.setEsParticular(Integer.parseInt((String)form.getDatos().get("esParticular")));
      logger.info("Requiere Inspeccion:" + (Integer)request.getSession().getAttribute("reqInspeccion"));
      int reqInspeccion = ((Integer)request.getSession().getAttribute("reqInspeccion")).intValue();
      oCotizacion.setInspeccionBSP(reqInspeccion);

      logger.info("Numero Hidden:" + (String)form.getDatos().get("hiddNumeroFono"));
      oCotizacion.setFecIniVig((String)request.getSession().getAttribute("fechaInicioVig"));
      logger.info("Fecha Inicio Vigencia:" + (String)request.getSession().getAttribute("fechaInicioVig"));
      oCotizacion.setFecTerVig((String)request.getSession().getAttribute("fechaTerminoVig"));
      logger.info("Fecha Termino Vigencia:" + (String)request.getSession().getAttribute("fechaTerminoVig"));
   }

   protected void verificarAutenticacion(HttpServletRequest request, HttpServletResponse response) throws AutenticacionException {
      if(!SeguridadUtil.isAutenticado(request)) {
         logger.debug("Usuario no autenticado. Autenticando.");
         SeguridadUtil.login(request, response, 1);
      } else {
         logger.debug("Usuario autenticado, se continua normalmente.");
      }
   }
}
