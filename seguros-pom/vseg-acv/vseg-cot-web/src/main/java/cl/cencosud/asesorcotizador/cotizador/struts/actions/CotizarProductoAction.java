// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:20
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import cl.cencosud.acv.common.exception.ProcesoCotizacionException;
import cl.tinet.common.captcha.servlet.util.CaptchaServiceSingleton;
import cl.tinet.common.model.exception.BusinessException;
import cl.tinet.common.struts.form.BuilderActionFormBaseCOT;
import com.octo.captcha.service.CaptchaServiceException;
import com.octo.captcha.service.image.ImageCaptchaService;
import com.tinet.exceptions.system.SystemException;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class CotizarProductoAction extends Action {

   public static final Log logger = LogFactory.getLog(CotizarProductoAction.class);
   private static final String BASE_CALCULO = "cl.cencosud.asesorcotizador.scoring.vida.pregunta.baseCalculo";


   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) 
		   throws BusinessException, SystemException {
      String forward = "captchaInvalido";
      long inicio = 0L;
      long fin = 0L;
      if(this.esCaptchaValido(request)) {
         forward = "desplegarValores";
         request.setAttribute("planesCotizados", "true");
         fin = Calendar.getInstance().getTimeInMillis();
         logger.info("TIEMPO TOTAL DE VALORIZACION: " + (fin - inicio) / 1000L);
         request.getSession().setAttribute("datosVolverPaso1", ((BuilderActionFormBaseCOT)form).getDatos());
		 request.setAttribute("CAPTCHA_INVALIDO", "false");
         return mapping.findForward(forward);
      } else {
		 request.setAttribute("CAPTCHA_INVALIDO", "true");
         throw new ProcesoCotizacionException("cl.cencosud.ventaseguros.cotizacion.exception.CAPTCHA_INVALIDO", new Object[0]);
      }
   }
   
   private boolean esCaptchaValido(HttpServletRequest request){
	   logger.info("Validando Captcha");
	   String captcha = request.getParameter("g-recaptcha-response");
	   boolean isResponseCorrect = false;
	   if (captcha == null || ("").equals(captcha.trim())){
		   isResponseCorrect = false;
		   logger.info("Captcha: Invalido");
		}
		else{
			isResponseCorrect = true;
			logger.info("Captcha: Valido");
		}
		return isResponseCorrect;
   }
   
   
/*
   private boolean esCaptchaValido(HttpServletRequest request) {
      logger.info("Validando Captcha");
      boolean correcto = false;
      String captchaId = request.getSession().getId();
      String respCaptcha = request.getParameter("captcha");
     
      if(request.getSession().getAttribute("idSubcategoria").equals("22") 
    		  && request.getSession().getAttribute("vaCaptcha").equals(Integer.valueOf(0))) {
         correcto = true;
      }else{
    	  try {
    	         ImageCaptchaService cse = CaptchaServiceSingleton.getInstance();
    	         //correcto = cse.validateResponseForID(captchaId, respCaptcha).booleanValue();
    	         correcto = true;
    	         if(correcto==true){
    	        	 request.getSession().setAttribute("hiddCaptcha", 0);
    	        	 logger.error("valor hiddCaptcha en CPA:" + request.getSession().getAttribute("hiddCaptcha"));
    	         }
    	         if(logger.isDebugEnabled()) {
    	            logger.debug("Captcha validado. Respuesta: " + correcto);
    	         } else {
    	            logger.info("Captcha validado. Respuesta: " + correcto);
    	         }
    	      } catch (CaptchaServiceException var6) {
    	         logger.error("Error captcha. Se asume captcha invalido.", var6);
    	      }
      }

      logger.error("valor vacaptcha:" + request.getSession().getAttribute("vaCaptcha"));
      return correcto;
   }
   */
}
