// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:18
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.paso3.struts.actions;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


import cencosud.cl.paymentHub.fop.model.FopResponse;
import cencosud.cl.paymentHub.fop.service.FopServiceClientImpl;
import cencosud.cl.paymentHub.fop.service.IFopServiceClient;
import cl.cencosud.acv.common.Plan;
import cl.cencosud.acv.common.Rama;
import cl.cencosud.acv.common.Transaccion;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.asesorcotizador.dao.ACVConfigDAO;
import cl.cencosud.asesorcotizador.dao.ACVDAOFactory;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.cencosud.asesorcotizador.delegate.PagosDelegate;
import cl.tinet.common.seguridad.model.UsuarioExterno;
import cl.tinet.common.seguridad.util.SeguridadUtil;

public class DesplegarCotizacionAction extends Action {

   private static Log logger = LogFactory.getLog(DesplegarCotizacionAction.class);
   private final String GRUPO_WEBPAY = "WEBPAY";
   private final String STORE_CODE = "7002";
   public static final String KEY_WSDL_FOP_SERVICE ="cencosud.cl.paymentHub.fop.wsdl";
   public static final String GROUP_HUB ="CONFIG_HUB";

   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
      String idRama = (String)request.getSession().getAttribute("idRama");
      CotizacionDelegate delegate = new CotizacionDelegate();
      PagosDelegate pagosDelegate = new PagosDelegate();
      Rama rama = delegate.obtenerRamaPorId(Long.parseLong(idRama));
      CotizacionSeguro cotizacionSeguro = (CotizacionSeguro)request.getSession().getAttribute("datosCotizacion");
      Plan datosPlan = (Plan)request.getSession().getAttribute("datosPlan");
      request.setAttribute("idPlan", datosPlan.getIdPlanLegacy());
      long lIdSolicitud = ((Long)request.getSession().getAttribute("idSolicitud")).longValue();
      int idSolicitud = Long.valueOf(lIdSolicitud).intValue();
      Integer numeroOrdenCompra = (Integer)request.getSession().getAttribute("resultNroBigsa");
      request.setAttribute("idRama", idRama);
      request.setAttribute("ramaDescripcion", rama.getDescripcion_rama());
      UsuarioExterno usuario = (UsuarioExterno)SeguridadUtil.getUsuario(request);
      Transaccion transaccion = new Transaccion();
      transaccion.setId_solicitud((long)idSolicitud);
      transaccion.setNumero_orden_compra(String.valueOf(numeroOrdenCompra));
      transaccion.setEstado_transaccion("INICIO");
      
      // Valida si el usuario corresponde a compra sin registro    
      long lRut = 0;
      if(request.getSession().getAttribute("sinRegistro") == null){
    	  lRut = Long.parseLong(usuario.getRut_cliente());
      }else{
    	  String rutSinRegistro = String.valueOf(request.getSession().getAttribute("rutSinRegistro"));
    	  lRut = Long.parseLong(rutSinRegistro); 
      }    
      transaccion.setRut_cliente(lRut);
      pagosDelegate.generarTransaccion(transaccion);
      Long primaNormal = Long.valueOf(datosPlan.getPrimaAnualPesos());
      Long primaProporcional = Long.valueOf(this.calculoPrimaProporcional(primaNormal.longValue()));
      transaccion.setMonto(primaNormal.longValue());
      boolean esPrimaUnica = delegate.existePrimaUnica((new Long(datosPlan.getIdPlan())).longValue());
      if(esPrimaUnica) {
         request.setAttribute("existePrimaUnica", Boolean.valueOf(esPrimaUnica));
         primaProporcional = Long.valueOf(this.calculoPrimaProporcional(datosPlan.getPrimaMensualPesos()));
         primaNormal = Long.valueOf(datosPlan.getPrimaMensualPesos());
      }

      request.setAttribute("ordenCompra", numeroOrdenCompra);
      request.setAttribute("idSesion", Long.valueOf(lIdSolicitud));
      request.setAttribute("monto", Long.valueOf(primaNormal.longValue() * 100L));
      request.setAttribute("montoProporcional", Long.valueOf(primaProporcional.longValue() * 100L));
      Map region = delegate.obtenerRegionPorId(cotizacionSeguro.getRegion());
      request.setAttribute("regionDescripcion", region.get("descripcion_region"));
      Map ciudad = delegate.obtenerCiudadPorId(cotizacionSeguro.getCiudad());
      request.setAttribute("ciudadDescripcion", ciudad.get("descripcion_ciudad"));
      Map comuna = delegate.obtenerComunaPorId(cotizacionSeguro.getComuna());
      request.setAttribute("comunaDescripcion", comuna.get("descripcion_comuna"));
      logger.info("EN paso 3, idRama: " + idRama);
      logger.info("idPlan: " + datosPlan.getIdPlanLegacy());
      logger.info("idSolicitud: " + idSolicitud);
      String sFormaPago = "";
      if(datosPlan != null) {
         sFormaPago = datosPlan.getFormaPago() != null?datosPlan.getFormaPago():"";
      }

      StringBuffer forma_pago = new StringBuffer(sFormaPago);
      List lFormaPago = delegate.obtenerParametro("WEBPAY");
      HashMap hFormaPago = new HashMap();
      
      /**
       * KCC -Hub de pago: Se agrega nueva forma de traer los pagos
       */
      PagosDelegate pagoDelegate = new PagosDelegate();
      String urlWdsl = pagoDelegate.obtenerParametroSistema(GROUP_HUB,KEY_WSDL_FOP_SERVICE);
      IFopServiceClient fop = new FopServiceClientImpl();
      List<FopResponse> listaFop = fop.getWebpayPaymentTypeByStoreNumber(urlWdsl,STORE_CODE);
      request.setAttribute("fopId", listaFop.get(0).getId());

      for(int i = 0; i < lFormaPago.size(); ++i) {
         String estado = "0";
         Map forma = (Map)lFormaPago.get(i);

         try {
            if(forma_pago.charAt(i) == 49) {
               estado = "1";
            }
         } catch (IndexOutOfBoundsException var33) {
            ;
         }

         hFormaPago.put((String)forma.get("nombre"), estado);
      }

      request.setAttribute("hFormaPago", hFormaPago);
      return mapping.findForward("continuar");
   }

   private long calculoPrimaProporcional(long primaMensual) {
      GregorianCalendar cal = new GregorianCalendar();
      int diasMesVenta = cal.getActualMaximum(5);
      int diasProporcion = diasMesVenta - cal.get(5);
      long primaDia = primaMensual / 12L / (long)diasMesVenta;
      long primaProporcional = primaDia * (long)(diasProporcion + 1);
      return primaProporcional;
   }
   

}
