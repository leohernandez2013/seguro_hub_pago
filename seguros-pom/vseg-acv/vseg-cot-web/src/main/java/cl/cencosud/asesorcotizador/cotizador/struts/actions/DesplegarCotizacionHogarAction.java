// Decompiled by:       Fernflower v0.8.6
// Date:                13.09.2013 14:43:20
// Copyright:           2008-2012, Stiver
// Home page:           http://www.neshkov.com/ac_decompiler.html

package cl.cencosud.asesorcotizador.cotizador.struts.actions;

import cl.cencosud.acv.common.Contratante;
import cl.cencosud.acv.common.EstadoCivil;
import cl.cencosud.acv.common.ParametroCotizacion;
import cl.cencosud.acv.common.Producto;
import cl.cencosud.acv.common.Solicitud;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar;
import cl.cencosud.asesorcotizador.cotizador.struts.actions.DesplegarCotizacionAction;
import cl.cencosud.asesorcotizador.cotizador.struts.forms.CotizacionHogarForm;
import cl.cencosud.asesorcotizador.cotizador.struts.forms.CotizacionVehiculoForm;
import cl.cencosud.asesorcotizador.delegate.CotizacionDelegate;
import cl.tinet.common.model.exception.BusinessException;
import cl.tinet.common.struts.form.BuilderActionFormBaseCOT;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DesplegarCotizacionHogarAction extends DesplegarCotizacionAction {

   private static final Log logger = LogFactory.getLog(DesplegarCotizacionHogarAction.class);


   public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws BusinessException, NumberFormatException, RemoteException {
      HttpSession session = request.getSession();
      CotizacionDelegate delegate = new CotizacionDelegate();
      String idSubcategoria = "";
      if(request.getParameter("idSubcategoria") != null) {
         request.getSession().setAttribute("idSubcategoria", request.getParameter("idSubcategoria"));
         idSubcategoria = request.getParameter("idSubcategoria");
      } else if(request.getSession() != null && request.getSession().getAttribute("idSubcategoria") != null) {
         idSubcategoria = request.getSession().getAttribute("idSubcategoria").toString();
      }

      String idRama = "";
      if(request.getParameter("idRama") != null) {
         request.getSession().setAttribute("idRama", request.getParameter("idRama"));
         idRama = request.getParameter("idRama");
      } else if(request.getSession() != null && request.getSession().getAttribute("idRama") != null) {
         idRama = request.getSession().getAttribute("idRama").toString();
      }

      String idPlan = request.getParameter("idPlan");
      if(idPlan == null) {
         if(request.getSession().getAttribute("idPlan") != null) {
            idPlan = request.getSession().getAttribute("idPlan").toString();
         } else {
            idPlan = null;
         }
      } else {
         request.getSession().setAttribute("idPlan", idPlan);
      }

      Producto[] productos = delegate.obtenerProductos(idSubcategoria, idRama, idPlan);
      if(productos != null && productos.length > 0) {
         request.setAttribute("productos", productos);
      }

      HashMap clienteSession = null;
      byte vaCaptcha = 1;
      session.setAttribute("vaCaptcha", Integer.valueOf(vaCaptcha));
      String estadosCiviles;
      if(idSubcategoria.equals("22")) {
         estadosCiviles = (String)((CotizacionVehiculoForm)form).getDatos().get("hiddCaptcha");
         if(estadosCiviles == null) {
            clienteSession = this.obtenerDatosClienteSession(request);
         } else {
            vaCaptcha = 0;
            session.setAttribute("vaCaptcha", Integer.valueOf(vaCaptcha));
            clienteSession = this.obtenerDatosClienteSession(request);
         }
      } else {
         clienteSession = this.obtenerDatosClienteSession(request);
      }

      if(((CotizacionHogarForm)form).getDatos().isEmpty() && clienteSession != null) {
         ((CotizacionHogarForm)form).getDatos().putAll(clienteSession);
      }

      if(request.getAttribute("vitrineo") != null && !"si".equals((String)request.getAttribute("vitrineo"))) {
         logger.info("Desplegar Cotizacion Hogar: No se guarda Vitrineo ...");
      } else {
         logger.info("Desplegar Cotizacion Hogar: Guardando Vitrineo ...");
         estadosCiviles = (String)((CotizacionHogarForm)form).getDatos().get("claveVitrineo");
         session.setAttribute("claveVitrineo", estadosCiviles);
         String regiones = (String)((CotizacionHogarForm)form).getDatos().get("rut");
         String tipoTelefono = (String)((CotizacionHogarForm)form).getDatos().get("dv");
         String codArea = (String)((CotizacionHogarForm)form).getDatos().get("tipoTelefono");
         String meses = (String)((CotizacionHogarForm)form).getDatos().get("codigoTelefono");
         String anyos = (String)((CotizacionHogarForm)form).getDatos().get("estadoCivil");
         String antiguedadVivienda = (String)((CotizacionHogarForm)form).getDatos().get("sexo");
         byte contratanteEsDuenyo = 0;
         byte cotHogar = 0;
         String hogar = (String)((CotizacionHogarForm)form).getDatos().get("contratanteEsDuenyo");
         byte cte = 0;
         short idModeloVitrineo = 8888;
         Object idAnyoVitrineo = null;
         short idMarcaVitrineo = 8888;
         int productoVitrineo = Integer.parseInt((String)((CotizacionHogarForm)form).getDatos().get("producto"));
         String nombre = (String)((CotizacionHogarForm)form).getDatos().get("nombre");
         String apellidoPaterno = (String)((CotizacionHogarForm)form).getDatos().get("apellidoPaterno");
         String apellidoMaterno = (String)((CotizacionHogarForm)form).getDatos().get("apellidoMaterno");
         String diaNac = (String)((CotizacionHogarForm)form).getDatos().get("diaFechaNacimiento");
         String mesNac = (String)((CotizacionHogarForm)form).getDatos().get("mesFechaNacimiento");
         String anyoNac = (String)((CotizacionHogarForm)form).getDatos().get("anyoFechaNacimiento");
         String telefono = (String)((CotizacionHogarForm)form).getDatos().get("numeroTelefono");
         String email = (String)((CotizacionHogarForm)form).getDatos().get("email");
         byte comuna = 0;
         byte region = 0;
         int rama = Integer.parseInt(idRama);
         int subcategoria = Integer.parseInt(idSubcategoria);
         String ciudadHogar = (String)((CotizacionHogarForm)form).getDatos().get("ciudadResidenciaDuenyo");
         String direccionHogar = (String)((CotizacionHogarForm)form).getDatos().get("direccion");
         String numeroDireccionHogar = (String)((CotizacionHogarForm)form).getDatos().get("numeroDireccion");
         String numeroDeptoHogar = (String)((CotizacionHogarForm)form).getDatos().get("numeroDepto");
         String anyoHogar = (String)((CotizacionHogarForm)form).getDatos().get("antiguedadVivienda");
         String rutDuenyo = (String)((CotizacionHogarForm)form).getDatos().get("rutDuenyo");
         String dvDuenyo = (String)((CotizacionHogarForm)form).getDatos().get("dvRutDuenyo");
         String nombreDuenyo = (String)((CotizacionHogarForm)form).getDatos().get("nombreDuenyo");
         String apellidoPaternoDuenyo = (String)((CotizacionHogarForm)form).getDatos().get("apellidoPaternoDuenyo");
         String apellidoMaternoDuenyo = (String)((CotizacionHogarForm)form).getDatos().get("apellidoMaternoDuenyo");
         Object diaNacimientoDuenyo = null;
         Object mesNacimientoDuenyo = null;
         Object anyoNacimientoDuenyo = null;
         Object estadoCivilDuenyo = null;
         Object valorComercial = null;
         String regionResidenciaDuenyo = (String)((CotizacionHogarForm)form).getDatos().get("regionResidenciaDuenyo");
         String comunaResidenciaDuenyo = (String)((CotizacionHogarForm)form).getDatos().get("comunaResidenciaDuenyo");
         delegate.grabarVitrineoGeneral(estadosCiviles, regiones, tipoTelefono, nombre, apellidoPaterno, apellidoMaterno, codArea, meses, telefono, email, region, comuna, diaNac, mesNac, anyoNac, anyos, antiguedadVivienda, (long)contratanteEsDuenyo, cotHogar, idMarcaVitrineo, idModeloVitrineo, (String)idAnyoVitrineo, cte, hogar, ciudadHogar, direccionHogar, numeroDireccionHogar, numeroDeptoHogar, anyoHogar, rama, subcategoria, productoVitrineo, rutDuenyo, dvDuenyo, nombreDuenyo, apellidoPaternoDuenyo, apellidoMaternoDuenyo, (String)diaNacimientoDuenyo, (String)mesNacimientoDuenyo, (String)anyoNacimientoDuenyo, (String)estadoCivilDuenyo, regionResidenciaDuenyo, comunaResidenciaDuenyo, (String)valorComercial);
         logger.info("Vitrineo guardado");
      }

      EstadoCivil[] estadosCiviles1 = delegate.obtenerEstadosCiviles();
      request.setAttribute("estadosCiviles", estadosCiviles1);
      ParametroCotizacion[] regiones1 = delegate.obtenerRegiones();
      request.setAttribute("regiones", regiones1);
      List tipoTelefono1 = delegate.obtenerParametro("TIPO_TELEFONO");
      request.setAttribute("tipoTelefono", tipoTelefono1.iterator());
      List codArea1 = delegate.obtenerParametro("TEL_CODIGO_AREA");
      request.setAttribute("codigoArea", codArea1.iterator());
      List meses1 = delegate.obtenerParametro("MESES");
      request.setAttribute("meses", meses1.iterator());
      List anyos1 = delegate.obtenerParametro("ANYOS");
      request.setAttribute("anyos", anyos1.iterator());
      List antiguedadVivienda1 = delegate.obtenerParametro("ANTIG_VIV");
      request.setAttribute("antiguedadVivienda", antiguedadVivienda1.iterator());
      if(request.getAttribute("retomaCotizacion") != null) {
         Solicitud contratanteEsDuenyo2 = (Solicitud)request.getAttribute("retomaCotizacion");
         logger.info("Desplegar Cotizacion Hogar: Se retoma cotizacion:" + contratanteEsDuenyo2 + "");
         CotizacionSeguroHogar cotHogar1 = delegate.obtenerDatosMateriaHogar(contratanteEsDuenyo2.getId_solicitud());
         contratanteEsDuenyo2.setDatosCotizacion(cotHogar1);
         if(contratanteEsDuenyo2 != null) {
            CotizacionSeguroHogar hogar1 = (CotizacionSeguroHogar)contratanteEsDuenyo2.getDatosCotizacion();
            Contratante cte1 = delegate.obtenerDatosContratante(contratanteEsDuenyo2.getId_solicitud());
            ((BuilderActionFormBaseCOT)form).getDatos().put("regionResidenciaDuenyo", cte1.getIdRegion());
            ((BuilderActionFormBaseCOT)form).getDatos().put("comunaResidenciaDuenyo", cte1.getIdComuna());
            ((BuilderActionFormBaseCOT)form).getDatos().put("ciudadResidenciaDuenyo", cte1.getIdCiudad());
            ((BuilderActionFormBaseCOT)form).getDatos().put("direccion", hogar1.getDireccion());
            ((BuilderActionFormBaseCOT)form).getDatos().put("numeroDireccion", hogar1.getNumeroDireccion());
            ((BuilderActionFormBaseCOT)form).getDatos().put("numeroDepto", hogar1.getNumeroDepto());
            ((BuilderActionFormBaseCOT)form).getDatos().put("producto", String.valueOf(contratanteEsDuenyo2.getId_producto()));
         }
      }

      if(request.getParameter("idProducto") != null && !request.getParameter("idProducto").equals("")) {
         ((BuilderActionFormBaseCOT)form).getDatos().put("producto", request.getParameter("idProducto"));
      }

      if(request.getParameter("volverPaso1") != null && request.getSession().getAttribute("datosVolverPaso1") != null) {
         ((BuilderActionFormBaseCOT)form).getDatos().putAll((HashMap)request.getSession().getAttribute("datosVolverPaso1"));
         logger.info("Desplegar Cotizacion Hogar: Volviendo al paso 1");
         request.getSession().removeAttribute("datosVolverPaso1");
      }

      String contratanteEsDuenyo1 = (String)((CotizacionHogarForm)form).getDatos().get("contratanteEsDuenyo");
      if(contratanteEsDuenyo1 == null) {
         ((CotizacionHogarForm)form).getDatos().put("contratanteEsDuenyo", Boolean.TRUE.toString());
      }

      if(request.getParameter("idPlan") != null) {
         request.getSession().setAttribute("isPromocion", "true");
         if(this.checkSpotLight(Integer.parseInt(request.getParameter("idPlan").toString()))) {
            request.getSession().setAttribute("spotLightOn", "true");
         }
      } else {
         request.getSession().removeAttribute("isPromocion");
         request.getSession().removeAttribute("spotLightOn");
      }

      return mapping.findForward("desplegarFormularioHogar");
   }

   private boolean checkSpotLight(int idPlanPromocion) {
      return idPlanPromocion == 1455;
   }

}
