
/* -------------------------------------------------------------------
FORMULARIO TARJETA TRANSBANK NACIONAL
---------------------------------------------------------------------*/

function isValidCreditCard(type, ccnum) {

   if (type == "Visa") {
      // Visa: length 16, prefix 4, dashes optional.
      var re = /^4\d{3}-?\d{4}-?\d{4}-?\d{4}$/;
   } else if (type == "MasterCard") {
      // Mastercard: length 16, prefix 51-55, dashes optional.
      var re = /^5[1-5]\d{2}-?\d{4}-?\d{4}-?\d{4}$/;
   } else if (type == "Disc") {
      // Discover: length 16, prefix 6011, dashes optional.
      var re = /^6011-?\d{4}-?\d{4}-?\d{4}$/;
   } else if (type == "American Express") {
      // American Express: length 15, prefix 34 or 37.
      var re = /^3[4,7]\d{13}$/;
   } else if (type == "Diners Club") {
      // Diners: length 14, prefix 30, 36, or 38.
      var re = /^3[0,6,8]\d{12}$/;
   }

   if (!re.test(ccnum)) return false;
   // Remove all dashes for the checksum checks to eliminate negative numbers
   ccnum = ccnum.split("-").join("");
   // Checksum ("Mod 10")
   // Add even digits in even length strings or odd digits in odd length strings.
   var checksum = 0;
   for (var i=(2-(ccnum.length % 2)); i<=ccnum.length; i+=2) {
      checksum += parseInt(ccnum.charAt(i-1));
   }
   // Analyze odd digits in even length strings or even digits in odd length strings.
   for (var i=(ccnum.length % 2) + 1; i<ccnum.length; i+=2) {
      var digit = parseInt(ccnum.charAt(i-1)) * 2;
      if (digit < 10) { checksum += digit; } else { checksum += (digit-9); }
   }
   if ((checksum % 10) == 0) return true; else return false;
}


function valida_prefijo_tarjeta2(data, tipo_tarjeta, Tarjeta){
	  	
   if(tipo_tarjeta == "Visa" && Tarjeta.substring(0, 1) != 4){
       alert("N� Tarjeta Inv�lida, Favor Reintente el ingreso");
       data.focus();
       data.select();
       return false;
   }
   if(tipo_tarjeta == "MasterCard" && Tarjeta.substring(0, 2) != 51 && Tarjeta.substring(0, 2) != 52
       && Tarjeta.substring(0, 2) != 53 && Tarjeta.substring(0, 2) != 54 && Tarjeta.substring(0, 2) != 55){
       alert("N� Tarjeta Inv�lida, Favor Reintente el ingreso.");
       data.focus();
       data.select();
       return false;
   }
   if(tipo_tarjeta == "Diners Club" && Tarjeta.substring(0, 2) != 30 && Tarjeta.substring(0, 2) != 36
       && Tarjeta.substring(0, 2) != 38){
       alert("N� Tarjeta Inv�lida, Favor Reintente el ingreso.");
       data.focus();
       data.select();
       return false;
   }
   return true;
}

function getFecha(fecha){
return fecha;
}

function uneDir(dir, vil, num){
var espacio="                                                            ";
var aux_dir=dir;
var aux_vil=vil;
var aux_num=num;
var l_aux_dir=aux_dir.length;
var l_aux_vil=aux_vil.length;
var l_aux_num=aux_num.length;
var l1=30-l_aux_dir;
var l2=23-l_aux_vil;
var l3=7-l_aux_num;
var cadena=dir+espacio.substring(0, l1)+vil+espacio.substring(0, l2)+num;
return cadena ;
}

function esNumero(data){
var cadnum='0123456789';
var estecaracter;
var contar=0;
var y=0;
for (var z=0; z<data.length; z++){
 y=z
 estecaracter=data.substring(y, y++);
  if (cadnum.indexOf(estecaracter) != -1){
    contar++;
  }
 }
 if (contar != data.length){
   return false;
 }
return true;
}

function validaBancnum(data){

var Tarjeta=data.cardNumber.value;
var entrada=data.cardNumber.value;
var largo=entrada.length;
var numtar=data.cardNumber.value;
var nblanco=numtar.indexOf(" ");
//var t_ban=data.banco_dt.value;
//var tipo_tarjeta=data.cardBrand[data.cardBrand.selectedIndex].text;
var tipo_tarjeta=$("input[name='mediopago']:checked").val();
var Medio="0.0";
//var cuota=data.numcuotas[data.numcuotas.selectedIndex].value;


 
if (Medio == '0.0')
{
//   var mm_exp=data.cardExpiryMonth[data.cardExpiryMonth.selectedIndex].value;
//   var aa_exp=data.cardExpiryYear[data.cardExpiryYear.selectedIndex].value;
//   if (mm_exp == "-1"){ 
//     alert("Debes ingresar el Mes de expiraci�n de tu tarjeta de cr�dito");
//     data.cardExpiryMonth.focus();
//     return false;  
//   }
//
//   if (aa_exp == "-1"){
//     alert("Debes ingresar el A�o de expiraci�n de tu tarjeta de cr�dito");
//     data.cardExpiryYear.focus();
//     return false;  
//   }
//
//  var mm_expiracion=""
//  var largo_menu = data.cardExpiryMonth.options.length;
//  for(var n=0; n<largo_menu; n++){
//     if (data.cardExpiryMonth.options[n].selected){
//        mm_expiracion=data.cardExpiryMonth.options[n].value;
//        break;
//     }
//  }
//  var aa_expiracion=""
//  var largo_menu = data.cardExpiryYear.options.length;
//  for(var n=0; n<largo_menu; n++){
//     if (data.cardExpiryYear.options[n].selected){
//        aa_expiracion=data.cardExpiryYear.options[n].value;
//        break;
//     }
//  }
//
//  if (Medio == '0.0' && (aa_expiracion < data.anovar.value))
//  {
//    alert("Su tarjeta expir�, lamentablemente no podr� efectuar la compra a trav�s de este medio.");
//    data.cardExpiryMonth.focus();
//    return (false);
//  }
//
//  //if (Medio == '0.0' && (aa_expiracion == data.anovar.value) && (mm_expiracion < data.mesvar.value))
//  if (Medio == '0.0' && (aa_expiracion == data.anovar.value) && (parseFloat(mm_expiracion) < parseFloat(data.mesvar.value)))
//  {
//    alert("Su tarjeta expir�, lamentablemente no podr� efectuar la compra a trav�s de este medio.");
//    data.cardExpiryMonth.focus();
//    return (false);
//  }
}









  var blancos=0;
 

//--------------------------------------------------------------------


//

if ((Medio == '1.0' || Medio == '2.0' || Medio == '3.0') && data.cardNumber.value == ""){
   data.cardNumber.value='0000000000000000';
   return true;}

if ((Medio == '0.0'|| Medio=="1.0" || Medio=="2.0" || Medio=="3.0") && nblanco != -1 && largo <= 14){
     alert("Debe ingresar un n�mero de tarjeta v�lido1");
       data.cardNumber.focus();
       data.cardNumber.select();
     return false;}

if ((Medio == '0.0'|| Medio=="1.0" || Medio=="2.0" || Medio=="3.0") && !esNumero(data.cardNumber.value)){
    alert("Debe ingresar un n�mero de tarjeta v�lido 2");
       data.cardNumber.focus();
       data.cardNumber.select();
     return false;}

if (Medio == '0.0' &&  Tarjeta=="0000000000000000")
{
       alert("N� Tarjeta Inv�lida, Favor Reintente el ingreso");
       data.cardNumber.focus();
       data.cardNumber.select();
       return false;
}


if (!valida_prefijo_tarjeta2(data.cardNumber, tipo_tarjeta, Tarjeta))
       return false;


if (Tarjeta.length == 14)
 {
  Tarjeta = "00"+Tarjeta
  data.cardNumber.value = '00'+data.cardNumber.value
 }
if (Tarjeta.length == 15)
 {
  Tarjeta = "0"+Tarjeta
  data.cardNumber.value = '0'+data.cardNumber.value
 }

if (Medio == '0.0'|| Medio=="1.0" || Medio=="2.0" || Medio=="3.0")
  {
     digito=Tarjeta.charAt(15);
     largo=Tarjeta.length
     Tarjeta=Tarjeta.substring(0, largo-1);
     largo=Tarjeta.length;
     var temporal=0;
     var suma=0;
     var mult=2;
     var cal=0;
     var j=0;
     var temp_char = "";
     var temp_Num = 0;
     for (i=largo-1;i>=0;i--){
         c=Tarjeta.charAt(i);
         temporal=parseInt(c,10)*mult;
         temp_char="0"+temporal;
         for (j=0;j<=temp_char.length-1;j++){
             temp_Num=parseInt(temp_char.charAt(j));
             suma+=parseInt(temp_Num,10);
         }
         mult++;
         if (mult==3) mult=1;
     }
     var calculado=10-suma%10;
     if (calculado==11) calculado=10-calculado;
     if (calculado==10) calculado=0;
     if(parseInt(digito)!=calculado){
       alert("N� Tarjeta Inv�lida, Favor Reintente el ingreso");
       data.cardNumber.focus();
       data.cardNumber.select();
       return false;
     }

     //return true;

  }
if (Medio == '0.0' && largo == 0 && nblanco == -1){
          alert("Debe ingresar un n�mero de tarjeta v�lido");
          return false;}
          
// ********** Valido el nro de cuotas

//if (cuota == "-1"){
//   alert("Debes seleccionar las cuotas");
//   return false;  
//}          
          
//*************  valido banco
// for (var r=0; r < data.banco_dt.value.length; r++){
//      if (data.banco_dt.value.substring(r, r+1) == "'"){
//         alert("No se permite el ingreso de comillas , por favor reemplaze por otro caracter e intente nuevamente");
//         data.banco_dt.focus();
//         data.banco_dt.select();
//         return false;
//      }
//      if (data.banco_dt.value.substring(r, r+1) == " "){
//         blancos++
//      }
//  }
//
//  if (Medio == '0.0' && ((data.banco_dt.value.length < 1) || (data.banco_dt.value.length == blancos)))
//     {
//     alert("Escriba el valor para el campo \"Banco\" .\n Recuerde Ingresar todos los campos Obligatorios");
//     data.banco_dt.focus();
//     data.banco_dt.select();
//     return (false);
//  } 

	return true;
//return false;
}


function validaBancBCI(data){
data.ccnum.value=data.ccnum_a.value;
var Tarjeta=data.ccnum_a.value;
var entrada=data.ccnum.value;
var largo=entrada.length;
var numtar=data.ccnum.value;
var nblanco=numtar.indexOf(" ");
var Medio="0.0";

  var mm_expiracion=""
  var largo_menu = data.ccxmonth.options.length;
  for(var n=0; n<largo_menu; n++){
     if (data.ccxmonth.options[n].selected){
        mm_expiracion=data.ccxmonth.options[n].value;
        break;
     }
  }
  var aa_expiracion=""
  var largo_menu = data.ccxyear.options.length;
  for(var n=0; n<largo_menu; n++){
     if (data.ccxyear.options[n].selected){
        aa_expiracion=data.ccxyear.options[n].value;
        break;
     }
  }

  if (Medio == '0.0' && (aa_expiracion < data.anovar.value))
  {
    alert("Su tarjeta expir�, lamentablemente no podr� efectuar la compra a trav�s de este medio.");
    data.ccxmonth.focus();
    return (false);
  }

  if (Medio == '0.0' && (aa_expiracion == data.anovar.value) && (mm_expiracion < data.mesvar.value))
  {
    alert("Su tarjeta expir�, lamentablemente no podr� efectuar la compra a trav�s de este medio.");
    data.ccxmonth.focus();
    return (false);
  }


//--------------------------------------------------------------------
if ((Medio == '1.0' || Medio == '2.0' || Medio == '3.0') && data.ccnum.value == ""){
   data.ccnum.value='0000000000000000';
   return true;}


if ((Medio == '0.0'|| Medio=="1.0" || Medio=="2.0" || Medio=="3.0") && nblanco != -1 && largo <= 14){
     alert("Debe ingresar un n�mero de tarjeta v�lido1");
       data.ccnum_a.focus();
       data.ccnum_a.select();
     return false;}

if ((Medio == '0.0'|| Medio=="1.0" || Medio=="2.0" || Medio=="3.0") && !esNumero(data.ccnum.value)){
    alert("Debe ingresar un n�mero de tarjeta v�lido 2");
       data.ccnum_a.focus();
       data.ccnum_a.select();
     return false;}

if (Medio == '0.0' &&  Tarjeta=="0000000000000000")
{
       alert("N� Tarjeta Inv�lida, Favor Reintente el ingreso");
       data.ccnum_a.focus();
       data.ccnum_a.select();
       return false;
}

if (!valida_prefijo_tarjeta2(data.ccnum_a, tipo_tarjeta, Tarjeta))
       return false;

if (Tarjeta.length == 14)
 {
   Tarjeta = "00"+Tarjeta
   data.ccnum.value = '00'+data.ccnum.value
 }
if (Tarjeta.length == 15)
 {
   Tarjeta = "0"+Tarjeta
   data.ccnum.value = '0'+data.ccnum.value
 }

if (Medio == '0.0'|| Medio=="1.0" || Medio=="2.0" || Medio=="3.0")
  {
     digito=Tarjeta.charAt(15);
     largo=Tarjeta.length
     Tarjeta=Tarjeta.substring(0, largo-1);
     largo=Tarjeta.length;
     var temporal=0;
     var suma=0;
     var mult=2;
     var cal=0;
     var j=0;
     var temp_char = "";
     var temp_Num = 0;
     for (i=largo-1;i>=0;i--){
         c=Tarjeta.charAt(i);
         temporal=parseInt(c,10)*mult;
         temp_char="0"+temporal;
         for (j=0;j<=temp_char.length-1;j++){
             temp_Num=parseInt(temp_char.charAt(j));
             suma+=parseInt(temp_Num,10);
         }
         mult++;
         if (mult==3) mult=1;
     }
     var calculado=10-suma%10;
     if (calculado==11) calculado=10-calculado;
     if (calculado==10) calculado=0;
     if(parseInt(digito)!=calculado){
       alert("N� Tarjeta Inv�lida, Favor Reintente el ingreso");
       data.ccnum_a.focus();
       data.ccnum_a.select();
       return false;
     }

     return true;

  }

if (Medio == '0.0' && largo == 0 && nblanco == -1){
          alert("Debe ingresar un n�mero de tarjeta v�lido");
          return false;}

return false;
}




function validaBancBSAN(data){
data.ccnum.value=data.ccnum_a.value;
var Tarjeta=data.ccnum_a.value;
var entrada=data.ccnum.value;
var largo=entrada.length;
var numtar=data.ccnum.value;
var nblanco=numtar.indexOf(" ");
var t_n1=data.nombre_dt.value;
var t_n2=data.ape_pat_dt.value;
var t_n3=data.ape_mat_dt.value;
var t_dir=data.dir1.value;
var t_num=data.num1.value;
var t_vil=data.vil1.value;
var tipo_tarjeta=data.cctype[data.cctype.selectedIndex].value;
var Medio="0.0";

  var mm_expiracion=""
  var largo_menu = data.ccxmonth.options.length;
  for(var n=0; n<largo_menu; n++){
     if (data.ccxmonth.options[n].selected){
        mm_expiracion=data.ccxmonth.options[n].value;
        break;
     }
  }
  var aa_expiracion=""
  var largo_menu = data.ccxyear.options.length;
  for(var n=0; n<largo_menu; n++){
     if (data.ccxyear.options[n].selected){
        aa_expiracion=data.ccxyear.options[n].value;
        break;
     }
  }

  if (Medio == '0.0' && (aa_expiracion < data.anovar.value))
  {
    alert("Su tarjeta expir�, lamentablemente no podr� efectuar la compra a trav�s de este medio.");
    data.ccxmonth.focus();
    return (false);
  }

  if (Medio == '0.0' && (aa_expiracion == data.anovar.value) && (mm_expiracion < data.mesvar.value))
  {
    alert("Su tarjeta expir�, lamentablemente no podr� efectuar la compra a trav�s de este medio.");
    data.ccxmonth.focus();
    return (false);
  }

if ((Medio == '1.0' || Medio == '2.0' || Medio == '3.0')){
  if(t_n1.indexOf("",1)==0 || t_n1.indexOf("",1)==-1){
     data.nombre_dt.value="0";
  }
  if(t_n2.indexOf("",1)==0 || t_n2.indexOf("",1)==-1){
     data.ape_pat_dt.value="0";
  }
  if(t_n3.indexOf("",1)==0 || t_n3.indexOf("",1)==-1){
     data.ape_mat_dt.value="0";
  }
  if(t_dir.indexOf("",1)==0 || t_dir.indexOf("",1)==-1){
     data.dir1.value="0";
  }
  if(t_num.indexOf("",1)==0 || t_num.indexOf("",1)==-1){
     data.num1.value="0";
  }
  if(t_vil.indexOf("",1)==0 || t_vil.indexOf("",1)==-1){
     data.vil1.value="0";
  }
  if(t_dir.indexOf("",1)==0 && t_vil.indexOf("",1)==0 && t_num.indexOf("",1)==0){
     data.dir1.value="0";
     data.num1.value="0";
     data.vil1.value="0";
     data.direccion_dt.value="0                             0                     0      ";
  }
  if(t_dir.indexOf("",1)==-1 && t_vil.indexOf("",1)==-1 && t_num.indexOf("",1)==-1){
     data.dir1.value="0";
     data.num1.value="0";
     data.vil1.value="0";
     data.direccion_dt.value="0                             0                     0      ";
  }
}

//--------------------------------------------------------------------
if ((Medio == '1.0' || Medio == '2.0' || Medio == '3.0') && data.ccnum.value == ""){
   data.ccnum.value='0000000000000000';
   return true;}


if ((Medio == '0.0'|| Medio=="1.0" || Medio=="2.0" || Medio=="3.0") && nblanco != -1 && largo <= 14){
     alert("Debe ingresar un n�mero de tarjeta v�lido");
       data.ccnum_a.focus();
       data.ccnum_a.select();
     return false;}

if ((Medio == '0.0'|| Medio=="1.0" || Medio=="2.0" || Medio=="3.0") && !esNumero(data.ccnum.value)){
    alert("Debe ingresar un n�mero de tarjeta v�lido");
       data.ccnum_a.focus();
       data.ccnum_a.select();
     return false;}

if (Medio == '0.0' &&  Tarjeta=="0000000000000000")
{
       alert("N� Tarjeta Inv�lida, Favor Reintente el ingreso");
       data.ccnum_a.focus();
       data.ccnum_a.select();
       return false;
}

if (!valida_prefijo_tarjeta2(data.ccnum_a, tipo_tarjeta, Tarjeta))
       return false;

if (Tarjeta.length == 14)
 {
   Tarjeta = "00"+Tarjeta
   data.ccnum.value = '00'+data.ccnum.value
 }
if (Tarjeta.length == 15)
 {
   Tarjeta = "0"+Tarjeta
   data.ccnum.value = '0'+data.ccnum.value
 }

if (Medio == '0.0'|| Medio=="1.0" || Medio=="2.0" || Medio=="3.0")
  {
     digito=Tarjeta.charAt(15);
     largo=Tarjeta.length
     Tarjeta=Tarjeta.substring(0, largo-1);
     largo=Tarjeta.length;
     var temporal=0;
     var suma=0;
     var mult=2;
     var cal=0;
     var j=0;
     var temp_char = "";
     var temp_Num = 0;
     for (i=largo-1;i>=0;i--){
         c=Tarjeta.charAt(i);
         temporal=parseInt(c,10)*mult;
         temp_char="0"+temporal;
         for (j=0;j<=temp_char.length-1;j++){
             temp_Num=parseInt(temp_char.charAt(j));
             suma+=parseInt(temp_Num,10);
         }
         mult++;
         if (mult==3) mult=1;
     }
     var calculado=10-suma%10;
     if (calculado==11) calculado=10-calculado;
     if (calculado==10) calculado=0;
     if(parseInt(digito)!=calculado){
       alert("N� Tarjeta Inv�lida, Favor Reintente el ingreso");
       data.ccnum_a.focus();
       data.ccnum_a.select();
       return false;
     }

     return true;

  }

if (Medio == '0.0' && largo == 0 && nblanco == -1){
          alert("Debe ingresar un n�mero de tarjeta v�lido");
          return false;}

return false;
}
//-->
// INICIO DBRAVO
function ventana_popup_scroll(url,w,h)
{	
	window.open(url,"","status=no,resizable=no,toolbar=no,location=no,scrollbars=yes,menubar=0,width=" + w + ",height=" + h + ",top=10,left=10");
}
// DBRAVO