	$( '#formularioVehiculo' ).validate({
		console.log("valida");
		rules: {
			tipoVehiculo: {
				required:true
			},
			marcaVehiculo: {
				required:true
			},
			modeloVehiculo: {
				required:true
			},
			anyoVehiculo: {
				required:true
			},
			rutCompleto: {
				required:true,
				rut:true
			},
			idFechaDia: {
				required:true
			},
			idFechaMes: {
				required:true
			},
			idFechaAnyo: {
				required:true
			},
			estadoCivil: {
				required:true
			},
			sexo: {
				required:true
			},
			email: {
				required:true,
				email:true
			},
			tipoTelefono: {
				required:true
			},
			codigoTelefono: {
				required:true
			},
			numeroTelefono: {
				required:true
			},
			contratanteEsDuenyo: {
				required:true
			}
		},
		groups: {
			date_of_birth: 'idFechaDia idFechaMes idFechaAnyo',
			phone: 'phone_type phone_number'
		},
		messages: {
			tipoVehiculo: {
				required: 'Este campo es requerido'
			},
			marcaVehiculo: {
				required: 'Este campo es requerido'
			},
			modeloVehiculo: {
				required: 'Este campo es requerido'
			},
			anyoVehiculo: {
				required: 'Este campo es requerido'
			},
			rutCompleto: {
				required: 'Este campo es requerido',
				rut: 'El rut ingresado no es válido'
			},
			idFechaDia: {
				required: 'Este campo es requerido'
			},
			idFechaMes: {
				required: 'Este campo es requerido'
			},
			idFechaAnyo: {
				required: 'Este campo es requerido'
			},
			estadoCivil: {
				required: 'Este campo es requerido'
			},
			sexo: {
				required: 'Este campo es requerido'
			},
			email: {
				required: 'Este campo es requerido',
				email: 'El email ingresado no es válido'
			},
			tipoTelefono: {
				required: 'Este campo es requerido'
			},
			codigoTelefono: {
				required: 'Este campo es requerido'
			},
			numeroTelefono: {
				required: 'Este campo es requerido'
			},
			contratanteEsDuenyo: {
				required: 'Este campo es requerido'
			}
		},
		highlight: function(element) {
			$(element).parent().addClass("is-error");
			$(element).parent().removeClass("is-ok");
		},
		unhighlight: function(element) {
			$(element).parent().removeClass("is-error");
			$(element).parent().addClass("is-ok");
		},
		errorPlacement: function ($error, $element) {
			$element.siblings('.o-form__message').append($error);
			if ($element.attr("name") == "idFechaDia" || $element.attr("name") == "idFechaMes" || $element.attr("name") == "idFechaAnyo"){
				$("#year_of_birth.o-form__message").append($error);
			}
			if ($element.attr("name") == "tipoTelefono" || $element.attr("name") == "numeroTelefono" || $element.attr("name") == "codigoTelefono"){
				$("#phone.o-form__message").append($error);
			}
		},
		submitHandler: function(form) {
			return false;
		}
	});