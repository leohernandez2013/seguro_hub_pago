<!DOCTYPE html>
<%@page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="cl.tinet.common.struts.form.ValidableActionForm"%>
<%@page import="org.apache.struts.Globals"%>
<%@page import="cl.cencosud.acv.common.Producto"%>
<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>
<%@page import="cl.tinet.common.seguridad.servlet.AutenticacionServlet"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<html lang="en">
<head>
<%@ include file="./includes/cotizador-head.jsp"%>

<!-- <link rel="stylesheet" -->
<!-- 	href="//code.jquery.com/ui/1.11.4/themes/redmond/jquery-ui.css" /> -->
<!-- <script src="//code.jquery.com/jquery-1.10.2.js"></script> -->
<!-- <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->
<!-- <link rel="stylesheet" href="/resources/demos/style.css" /> -->

<script type="text/javascript">

$(function() {
   $('.datos-calle-input').attr('placeholder','Direcci�n');
   $('.datos-direccion-input').attr('placeholder','N�mero');
   $('.datos-depto-input').attr('placeholder','Dpto.');
});

function registroRemoto(){
	var rut_cliente =  "<bean:write name='datosCotizacion' property='rutCliente'/>";
	var dv_cliente = "<bean:write name='datosCotizacion' property='dv'/>";
	var diaFechaNacimiento = "<bean:write name='datosCotizacion' property='fechaNacimiento' format='dd'/>";
	var mesFechaNacimiento = "<bean:write name='datosCotizacion' property='fechaNacimiento' format='MM'/>";
	var anoFechaNacimiento = "<bean:write name='datosCotizacion' property='fechaNacimiento' format='yyyy'/>";
	var val_sexo = "<bean:write name='datosCotizacion' property='sexo'/>";
	var val_estadoCivil= "<bean:write name='datosCotizacion' property='estadoCivil'/>";
	var val_nombre = "<bean:write name='datosCotizacion' property='nombre'/>";
	var val_apellidoP = "";
	var val_apellidoM = "";
	var val_email = "";
	var val_codigo_cel = "";
	var val_tipo_cel = "";
	var val_numero_cel = "";

	<logic:notEmpty name="datosCotizacion" property="apellidoPaterno">
		val_apellidoP = "<bean:write name='datosCotizacion' property='apellidoPaterno'/>";
	</logic:notEmpty>
	
	<logic:notEmpty name="datosCotizacion" property="apellidoMaterno">
		val_apellidoM = "<bean:write name='datosCotizacion' property='apellidoMaterno'/>";
	</logic:notEmpty>
	
	<logic:present property="email" name="datosCotizacion">
		val_email = "<bean:write name='datosCotizacion' property='email'/>";
	</logic:present>			

	<logic:present property="tipoTelefono1" name="datosCotizacion">
		val_tipo_cel = "<bean:write name='datosCotizacion' property='tipoTelefono1'/>";
	</logic:present>		
	
	<logic:present property="datos(cteCodigoTelefono1)" name="ingresarDatosVehiculo">
		val_codigo_cel = "<bean:write name='ingresarDatosVehiculo' property='datos(cteCodigoTelefono1)'/>";
	</logic:present>				
	
	<logic:present property="datos(numeroTelefono1)" name="ingresarDatosVehiculo">
		val_numero_cel = "<bean:write name='ingresarDatosVehiculo' property='datos(numeroTelefono1)'/>";
	</logic:present>				
					
	var url = '/cotizador/registroClienteRemote.do';
	var params = "?rut_cliente=" + rut_cliente;
	params += "&dv_cliente=" + dv_cliente;
	params += "&diaFechaNacimiento=" + diaFechaNacimiento;
	params += "&mesFechaNacimiento=" + mesFechaNacimiento;
	params += "&anoFechaNacimiento=" + anoFechaNacimiento;
	params += "&val_sexo=" + val_sexo;
	params += "&val_estadoCivil=" + val_estadoCivil;
	params += "&val_nombre=" + val_nombre;
	params += "&val_apellidoP=" + val_apellidoP;
	params += "&val_apellidoM=" + val_apellidoM;
	params += "&val_email=" + val_email;
	params += "&val_codigo_cel=" + val_codigo_cel;
	params += "&val_tipo_cel=" + val_tipo_cel;
	params += "&val_numero_cel=" + val_numero_cel;		

	$("#cargaRegistrate").attr('src',url+params);
	$("#modalRegistrate").modal({
	backdrop: 'static',
	show: true
	});
}
</script>

<script type="text/javascript">
		$(document).ready(function(){
		$("#inputIniVig").datepicker({
		minDate: 0,
		dateFormat: 'dd/mm/yy',
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
		'Junio', 'Julio', 'Agosto', 'Septiembre',
		'Octubre', 'Noviembre', 'Diciembre'],
		monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
		'May', 'Jun', 'Jul', 'Ago',
		'Sep', 'Oct', 'Nov', 'Dic']
		}); 
		});
		</script>

<script type="text/javascript">

$(document).ready(function(){
	bar(0,2);
	paso2bar = 50;
	cargaDia('idFechaDia');
	cargaMes('idFechaMes');
	cargaAnyo('idFechaAnyo');
});
</script>

<script type="text/javascript">	


			function autocompletarfechaHogar(input) {
			
				if(input && input.value && input.value.length && input.value.length >= 8) {
								
										var value = replaceAll(input.value, "/", "" );
										var dia = value.substring(0,2);
										var mes = value.substring(2,4);
										var anio = value.substring(4,8);
    				    				input.value = dia + "/" + mes + "/" + anio;
				}
			  
    		}

			
			
			$(document).ready(function(){
				$("#paso2").hide();
				
				if($(".divtexnovalido").length) {
				$("#fecha-invalida").show("fast");
				}
				
				regionToRoman('cteRegion');
				regionToRoman('asegRegion');
				$('.datos-calle-input').attr('placeholder','calle');
				$('.datos-direccion-input').attr('placeholder','n�');
				$('.datos-depto-input').attr('placeholder','n� depto/casa');
				
				<logic:present property="datos(cteCodigoTelefono1)" name="ingresarDatosHogar">
					var val_codigo_cel = "<bean:write name='ingresarDatosHogar' property='datos(cteCodigoTelefono1)'/>";
					$("select[name='datos(cteCodigoTelefono1)']").val(val_codigo_cel);
				</logic:present>
				
				<logic:present property="datos(cteCodigoTelefono2)" name="ingresarDatosHogar">
					var val_codigo_cel = "<bean:write name='ingresarDatosHogar' property='datos(cteCodigoTelefono2)'/>";
					$("select[name='datos(cteCodigoTelefono2)']").val(val_codigo_cel);
				</logic:present>
				
				<logic:present property="datos(asegCodigoTelefono1)" name="ingresarDatosHogar">
					var val_codigo_cel = "<bean:write name='ingresarDatosHogar' property='datos(asegCodigoTelefono1)'/>";
					$("select[name='datos(asegCodigoTelefono1)']").val(val_codigo_cel);
				</logic:present>
				
				<logic:present property="datos(asegCodigoTelefono2)" name="ingresarDatosHogar">
					var val_codigo_cel = "<bean:write name='ingresarDatosHogar' property='datos(asegCodigoTelefono2)'/>";
					$("select[name='datos(asegCodigoTelefono2)']").val(val_codigo_cel);
				</logic:present>
			
				if($("select#cteRegion").val() != null && $("select#cteRegion").val() != "" ) {
					var valor = "";
					$.getJSON('/cotizador/buscar-comunas.do?idRegion=' + $("select#cteRegion").val(), function(data){
						$('select#cteComuna option').remove();
						var options = '';
						options += '<option value="">Seleccione</option>';
						<logic:present property="datos(cteComuna)" name="ingresarDatosHogar">
							valor = "<bean:write name='ingresarDatosHogar' property='datos(cteComuna)'/>";
						</logic:present>
						
						for(var i = 0; i < data.length; i++) {
							if(valor != '' && valor == data[i].id) {
								options += '<option value="' + data[i].id + '" selected="selected">' + data[i].descripcion + '</option>';						
							} else {
								options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
							}
						}
						$('select#cteComuna').html(options);
						recargarCiudadesPaso2Hogar(valor);
					});
				}

				$('#clavepaso2').keypress(function(e){
					if(e.which == 13){
						e.preventDefault();
						submitPaso2Hogar();
						return false;
					}
				});
				
				$("select#cteRegion").change(function() {
					$.getJSON('/cotizador/buscar-comunas.do?idRegion=' + this.value, function(data){
						$('select#cteComuna option').remove();
						var options = '';
						options += '<option value="">Seleccione</option>';

						for(var i = 0; i < data.length; i++) {
							options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
						}
						$('select#cteComuna').html(options);
						$('select#cteCiudad').html('<option value="">Seleccione</option>');
					});
				});
				
				$("select#cteComuna").change(function() {
					$.getJSON('/cotizador/buscar-ciudades.do?idComuna=' + this.value, function(data){
						$('select#cteCiudad option').remove();
						var options = '';
						options += '<option value="">Seleccione</option>';

						for(var i = 0; i < data.length; i++) {
							options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
						}
						$('select#cteCiudad').html(options);
					});
				});

				$("#asegRegion").change(function() {
					$.getJSON('/cotizador/buscar-comunas.do?idRegion=' + this.value, function(data){
						$('select#asegComuna option').remove();
						var options = '';
						options += '<option value="">Seleccione</option>';

						for(var i = 0; i < data.length; i++) {
							options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
						}
						$('select#asegComuna').html(options);
						$("select#asegCiudad").html('<option value="">Seleccione</option>');
					});
				});

				$("select#asegComuna").change(function() {
					$.getJSON('/cotizador/buscar-ciudades.do?idComuna=' + this.value, function(data){
						$('select#asegCiudad option').remove();
						var options = '';
						options += '<option value="">Seleccione</option>';

						for(var i = 0; i < data.length; i++) {
							options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
						}
						$('select#asegCiudad').html(options);
					});
				});
				
				if($("#asegRegion").val() != null && $("#asegRegion").val() != "" ) {
					var valor = "";
					$.getJSON('/cotizador/buscar-comunas.do?idRegion=' + $("#asegRegion").val(), function(data){
						$('select#asegComuna option').remove();
						var options = '';
						options += '<option value="">Seleccione</option>';

						<logic:present property="datos(asegComuna)" name="ingresarDatosHogar">
							valor = "<bean:write name='ingresarDatosHogar' property='datos(asegComuna)'/>";
						</logic:present>

						for(var i = 0; i < data.length; i++) {
							if(valor!='' && valor==data[i].id){
								options += '<option value="' + data[i].id + '" selected="selected">' + data[i].descripcion + '</option>';
							} else {
								options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
							}
						}
						$('select#asegComuna').html(options);
						recargarCiudades2(valor);
					});
				}
				
			});
						
			function recargarCiudadesPaso2Hogar(valor) {
				$.getJSON('/cotizador/buscar-ciudades.do?idComuna=' + valor, function(data){
					var valorciudad = '';
					$('select#cteCiudad option').remove();
					var options = '';
					options += '<option value="">Seleccione</option>';
					<logic:present property="datos(cteCiudad)" name="ingresarDatosHogar">
						var valorciudad = '<bean:write name='ingresarDatosHogar' property='datos(cteCiudad)'/>';
					</logic:present>

					for(var i = 0; i < data.length; i++) {
						if(valorciudad!='' && valorciudad == data[i].id) {
							options += '<option value="' + data[i].id + '" selected="selected">' + data[i].descripcion + '</option>';
						} else {
							options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
						}
					}
					$('select#cteCiudad').html(options);
				});
			}
			
			function recargarCiudades2(valor) {
				$.getJSON('/cotizador/buscar-ciudades.do?idComuna=' + valor, function(data){
					var valorCiudad = '';
					$('select#asegCiudad option').remove();
					var options = '';
					options += '<option value="">Seleccione</option>';

					<logic:present property="datos(asegCiudad)" name="ingresarDatosHogar">
						var valor = "<bean:write name='ingresarDatosHogar' property='datos(asegCiudad)'/>";
					</logic:present>

					for(var i = 0; i < data.length; i++) {
						if(valorCiudad!='' && valorCiudad == data[i].id) {
							options += '<option value="' + data[i].id + '" selected="selected">' + data[i].descripcion + '</option>';
						} else {
							options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
						}
					}
					$('select#asegCiudad').html(options);
				});
			}
						
			function submitPaso2Hogar(){  
				$('.grupo_direccion :input').each(function(){
					if($(this).val() == "calle" || $(this).val() == "n�" || $(this).val() == "n� depto/casa" ) {
						$(this).val("");
					}
				});
				
				<logic:present property="datos(contratanteEsDuenyo)" name="cotizarProductoHogar">
					var valorEsDuenyo = "<bean:write property="datos(contratanteEsDuenyo)" name="cotizarProductoHogar"/>";
					if(valorEsDuenyo == "true") {
						var fechaCompleta = document.getElementById('fechaCompleta').value.split('-');
						document.getElementById('diasFecha').value = fechaCompleta[2];
						document.getElementById('mesFecha').value = fechaCompleta[1];
						document.getElementById('anyoFecha').value = fechaCompleta[0];
					}
				</logic:present>
				
				<logic:present property="clienteEsAsegurado" name="datosCotizacion">
					<logic:equal value="false" name="datosCotizacion" property="clienteEsAsegurado">
						var fechaCompleta = document.getElementById('fechaCompleta').value.split('-');
						document.getElementById('diasFecha').value = fechaCompleta[0];
						document.getElementById('mesFecha').value = fechaCompleta[1];
						document.getElementById('anyosFecha').value = fechaCompleta[2];			
					</logic:equal>
				</logic:present>
				
				
				
				$("#waitModal").modal({backdrop: 'static', keyboard: false});				
				$("#paso2HogarForm").submit();
			}
			
			function sinRegistro(){
				validateInput("validaForm");
				if(document.getElementById('fechaCompleta')) {
	    			var fechaCompleta = document.getElementById('fechaCompleta').value.split('-');
					console.log("fechaCompleta[0] "+fechaCompleta[0]);
					console.log("fechaCompleta[1] "+fechaCompleta[1]);
					console.log("fechaCompleta[2] "+fechaCompleta[2]);
	    			if(fechaCompleta.length === 3) {
	    				document.getElementById('diasFecha').value = fechaCompleta[0];
	    				document.getElementById('mesFecha').value = fechaCompleta[1];
	    				document.getElementById('anyosFecha').value = fechaCompleta[2];
	    			}
	    		}
			
				var rutSinRegistro = "<bean:write name='datosCotizacion' property='rutCliente'/>";
				document.getElementById('paso2HogarForm').action="/cotizador/guardar-datos-adicionales-hogar.do?sinRegistro=1&rutSinRegistro="+rutSinRegistro;
				submitPaso2Hogar();
			}
			
			function conRegistro(){
				validateInput("paso2");
				if(validacionInput == 0) {
					submitPaso2Hogar();
				}
			}
			
			function showModal() {
						var result = validateInput("paso2");
	    				if(validacionInput == 0){
		    					var inst = $('[data-remodal-id=continueModal]').remodal();
								inst.open();
	    				}
					}
					
			function closeModal(){
						$('[data-remodal-id=continueModal]').remodal().close();
					}
		</script>
</head>

<body onload="desplegarErrores()">
	<%@ include file="/google-analytics/google-analytics.jsp"%>
	<%@ include file="./includes/cotizador-header.jsp"%>

	<div class="container contenedor-sitio">
		<%@ include file="./includes/cotizador-breadcrumb.jsp"%>
		<!-- INICIO  CONTENIDOS -->
		<div class="row">
			<html:form action="/guardar-datos-adicionales-hogar" method="post"
				styleId="paso2HogarForm">
				<html:hidden property="datos(asegDiaNac)" value=""
					styleId="diasFecha" />
				<html:hidden property="datos(asegMesNac)" value=""
					styleId="mesFecha" />
				<html:hidden property="datos(asegAnyoNac)" value=""
					styleId="anyosFecha" />
					<div id="contenido-desplegado-cotizador">
						<div id="paso1">
						<main role="main">
      <div class="remodal-bg">
        <section class="container">
          <div class="row o-form o-form--standard o-form--linear o-form--small">
            <div class="col-12">
				<logic:equal name="idRama" value="1">
				      	<logic:equal name="idSubcategoria" value="22"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Full Cobertura</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="21"><h1 class="o-title o-title--primary">Cotizaci�n Seguro P�rdida Total</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="36"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Obligatorio Mercosur</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="79"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Robo Contenido</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="2">
				      	<logic:equal name="idSubcategoria" value="24"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Sismo / Estructura</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="43"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Sismo / Contenido</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="44"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Robo</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="25"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio, Robo y Sismo</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="26"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Premio a la Permanencia</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="201"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Full Asistencia</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="220"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Vacaciones</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="3">
				      	<logic:equal name="idSubcategoria" value="29"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Viaje Grupal</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="5">
				      	<logic:equal name="idSubcategoria" value="139"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Fraude</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="159"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Fraude con devoluci�n</h1></logic:equal>
			      	</logic:equal>
            		<logic:equal name="idRama" value="6">
				      	<logic:equal name="idSubcategoria" value="119"><h1 class="o-title o-title--primary">Asistencia Viaje</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="140"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Mascota</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="221"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Viaje Grupal</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="7">
						<logic:equal name="idSubcategoria" value="161"><h1 class="o-title o-title--primary">Hospitalizaci�n con devoluci�n</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="162"><h1 class="o-title o-title--primary">Accidentes Personales con devoluci�n</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="164"><h1 class="o-title o-title--primary">Oncol�gico con devoluci�n</h1></logic:equal>
			      	</logic:equal>
			      	<logic:equal name="idRama" value="8">
				      	<logic:equal name="idSubcategoria" value="239"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Obligatorio Mercosur</h1></logic:equal>
			      	</logic:equal>
			</div>
            <div class="col-12">
              <section class="o-box o-box--recruiting u-mtb50">
                <div class="o-steps">
                  <div class="o-steps__item o-steps__item--current"><span class="o-steps__milestone"><span class="o-steps__value">1</span></span>
                    <h4 class="o-steps__title">Informaci�n contratante</h4>
                  </div>
                  <div class="o-steps__item"><span class="o-steps__milestone"><span class="o-steps__value">2</span></span>
                    <h4 class="o-steps__title">Datos del seguro</h4>
                  </div>
                  <div class="o-steps__item"><span class="o-steps__milestone"><span class="o-steps__value">3</span></span>
                    <h4 class="o-steps__title">Confirmaci�n y pago</h4>
                  </div>
                </div>
                <div class="u-pad20">
                  <div class="row">
                    <div class="col-lg-8 u-mobile_second">
                      <h2 class="o-title o-title--subtitle u-mb10">Informaci�n contratante</h2>
                      <p class="o text"></p>
                      <form class="o-form o-form--standard o-form--linear o-form--small" id="form_recruiting_step1" action="">
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="o-form__field">
                              <label class="o-form__label">Tel�fono</label>
                              <div class="row">
                                <div class="col-4">
                                <div class="o-form__field o-form__field--select">
                                  <html:select property="datos(cteTipoTelefono1)"
													styleClass="o-form__select" styleId="datos.telefono1_1">
													<html:option value="">Seleccione</html:option>
													<html:options collection="tipoTelefono" property="valor"
														labelProperty="descripcion" />
					   			 </html:select><span class="o-form__line"></span>
					   			 </div>
                                </div>
                                <div class="col-3">
                                <div class="o-form__field o-form__field--select">
                                  <html:select property="datos(cteCodigoTelefono1)"
													styleClass="o-form__select" styleId="datos.telefono1_2">
													<html:option value="">Seleccione</html:option>
													<html:options collection="codigoArea" property="valor"
														labelProperty="descripcion" />
								  </html:select><span class="o-form__line"></span>
								  </div>
                                </div>
                                <div class="col-5">
                                <div class="o-form__field o-form__field--select">
                                   <html:text property="datos(cteNumeroTelefono1)"
													styleClass="o-form__input" size="10" maxlength="10"
													styleId="datos.telefono1_3">
								   </html:text><span class="o-form__line"></span>
								   </div>
                                </div>
                              </div><span class="o-form__message" id="phone--recruiting"></span>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="o-form__field o-form__field--select">
                              <label class="o-form__label">Regi�n</label>
                              <html:select property="datos(cteRegion)"
												styleClass="o-form__select" styleId="cteRegion">
												<html:option value="">Seleccione</html:option>
												<html:options collection="regiones" property="id"
													labelProperty="descripcion" />
							  </html:select><span class="o-form__line"></span><span class="o-form__message"></span>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="o-form__field o-form__field--select">
                              <label class="o-form__label">Comuna</label>
                              <html:select property="datos(cteComuna)"
												styleClass="o-form__select" styleId="cteComuna">
												<html:option value="">Seleccione</html:option>
							  </html:select><span class="o-form__line"></span><span class="o-form__message"></span>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="o-form__field o-form__field--select">
                              <label class="o-form__label">Ciudad</label>
                              <html:select property="datos(cteCiudad)"
												styleClass="o-form__select" styleId="cteCiudad">
												<html:option value="">Seleccione</html:option>
							  </html:select><span class="o-form__line"></span><span class="o-form__message"></span>
                            </div>
                          </div>
                        </div>
						<div class="row">
						<div class="col-lg-6">
                            <div class="o-form__field">
                              <label class="o-form__label">Direcci�n</label>
                              <div class="row">
                                <div class="col-6">
                                  <html:text property="datos(cteCalle)"
												styleClass="o-form__input datos-calle-input" size="25"
												styleId="datos.calle1" maxlength="20" onkeypress="return validar(event)"> 
								  </html:text><span class="o-form__line"></span>
                                </div>
                                <div class="col-3">
                                  <html:text property="datos(cteNumeroCalle)"
												styleClass="o-form__input datos-direccion-input" size="5"
												styleId="datos.nro1" maxlength="5"
												onkeypress="return isNumberKey(event)">
											</html:text><span class="o-form__line"></span>
                                </div>
                                <div class="col-3">
                                  <html:text property="datos(cteNumeroDepto)"
												styleClass="o-form__input datos-depto-input optional"
												size="10" styleId="datos.depto1" maxlength="5"
												onkeypress="return isNumberKey(event)">
											</html:text><span class="o-form__line"></span>
                                </div>
                              </div><span class="o-form__message" id="direction--recruiting"></span>
                            </div>
                          </div>
                          <div class="col-lg-6">
                          <logic:equal name="subcategoriaDesc" value="Hogar Vacaciones">
											<logic:notEqual name="datosPlan" property="maxDiaIniVig"
												value="0">
												<div id="casilla-datos-cotizador">
															<div id="bloque-casilla">
																<div id="bloque-dato-usuario" style="padding-top: 4px;">
																	<p>Fecha de inicio de viaje *</p>
																	<div id="bloque-dato-usuario"
																		style="padding-top: 2.5px;">
																		<p>
																			<input class="form-control"
																			placeholder="30/01/1980" name="datos(fechaInicio)"
																			id="inputIniVig" onBlur="autocompletarfechaHogar(this);"
																			onFocus="autocompletarfechaHogar(this);"
																			maxlength="10"/>
																		</p>
																	</div>
																	<div class="col-sm-12" id="div-mensaje-error"
																		style="display: none">
																		<div class="form-group">
																			<div class="alert alert-warning">
																				<p id="error-fecha-viaje" align="center">
																					<html:errors property='inputIniVig' />
																				</p>
																			</div>
																		</div>
																	</div>
																	<script>
																		$().ready(function() {
																			if($(".divtexnovalido").length) {
																				$("#div-mensaje-error").show("fast");
																			}
																		});
																	</script>
																</div>
															</div>
														</div>
											</logic:notEqual>
										</logic:equal>
                          </div>
						</div>
                        <div class="row o-actions o-actions--flex">
                          <div class="col-lg-3">
                            <div class="o-form__field is-last"><a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="/cotizador/desplegar-cotizacion.do?idRama=<bean:write name='idRama'/>&idSubcategoria=<bean:write name='idSubcategoria'/>&volverPaso1=true"><i class="mx-arrow-left"></i> Volver</a></div>
                          </div>
                          <div class="col-lg-3 offset-lg-6">
                            <div class="o-form__field is-last">
                              <button class="o-btn o-btn--primary o-btn o-btn--icon" id="send_form_recruiting_step1" type="button" onclick="switchForm(true);">Siguiente<i class="o-icon">
                                  <svg width="13px" height="14px" viewbox="0 0 13 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                      <g id="botones/orangeButton+rightArrow" transform="translate(-113.000000, -19.000000)" fill-rule="nonzero" fill="#FFFFFF">
                                        <g id="orangeButton+rightArrow">
                                          <g id="arrows" transform="translate(113.000000, 19.000000)">
                                            <path id="Shape" d="M12.6595914,6.18869213 L6.92029216,0.341285393 C6.69713064,0.113730337 6.42956532,0 6.11805938,0 C5.81235867,0 5.54791211,0.113730337 5.32456532,0.341285393 L4.66341805,1.01492135 C4.44004038,1.23052135 4.32838242,1.49998202 4.32838242,1.82336629 C4.32838242,2.14668764 4.44004038,2.41614831 4.66341805,2.63181124 L7.24652732,5.27252584 L1.03990736,5.27252584 C0.734361045,5.27252584 0.486033254,5.38477753 0.295016627,5.60937528 C0.104,5.83397303 0.00849168646,6.10494382 0.00849168646,6.42222472 L0.00849168646,7.57198652 C0.00849168646,7.8893618 0.104030879,8.1602382 0.295016627,8.38483596 C0.486002375,8.60943371 0.734361045,8.72159101 1.03990736,8.72159101 L7.2464038,8.72159101 L4.66332542,11.3535281 C4.43994774,11.5809258 4.32828979,11.8534382 4.32828979,12.1709079 C4.32828979,12.4883146 4.43994774,12.7607955 4.66332542,12.9881933 L5.32447268,13.6618292 C5.5537791,13.8834067 5.81822565,13.9941798 6.11796675,13.9941798 C6.42363658,13.9941798 6.69114014,13.8834067 6.9202304,13.6618292 L12.6594988,7.81442247 C12.8769786,7.59284494 12.9857957,7.32045843 12.9857957,6.9970427 C12.9857957,6.66774382 12.8769786,6.3981573 12.6595914,6.18869213 Z"></path>
                                          </g>
                                        </g>
                                      </g>
                                    </g>
                                  </svg></i></button>
                            </div>
                          </div>
                        </div>
                        <!--<div class="row u-hidden_desktop">
                          <div class="col-12">
                            <div class="c-assistance u-mb30">
                              <h4 class="c-assistance__title">�Necesitas asistencia?</h4><a class="c-assistance__button o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="#">Te llamamos gratis</a>
                              <p class="c-assistance__text">O ll�manos gratis al<strong class="c-assistance__outstanding"> 600 500 5000</strong></p>
                            </div>
                          </div>
                        </div>-->
                      </form>
                    </div>
                    <div class="col-lg-4">
                      <section class="c-summary" id="summary_sure">
                        <h2 class="c-summary__title">Resumen de tu seguro</h2>
                        <div class="c-summary__company"></div>
                        <h3 class="c-summary__subtitle"><bean:write name="datosPlan" property="nombrePlan" /></h3>
                        <div class="row">
                          <div class="col-12">
                            <div class="c-summary__cost">
                              <div class="c-summary__label">Monto Total:</div>
                              <div class="c-summary__price"><sup>$</sup><bean:write name="datosPlan" property="primaMensualPesos"
								format="##,##0" locale="currentLocale" /><sub>/ Mes*</sub></div>
                            </div>
                            <p class="c-summary__legal"></p>
                          </div>
                          <div class="col-12 u-mobile_second">
                          </div>
                        </div>
                        <!-- 
                        <div class="c-assistance u-mb30 u-hidden_mobile">
                          <h4 class="c-assistance__title">�Necesitas asistencia?</h4><a class="c-assistance__button o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="#">Te llamamos gratis</a>
                          <p class="c-assistance__text">O ll�manos gratis al<strong class="c-assistance__outstanding"> 600 500 5000</strong></p>
                        </div> -->
                      </section>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </section>
      </div>
    </main>
							
						</div>
						<!--INICIO PASO2-->
						<div id="paso2">
							<div id="validaForm">
							<main role="main">
      <div class="remodal-bg">
        <section class="container">
          <div class="row">
            <div class="col-12">
				<logic:equal name="idRama" value="1">
				      	<logic:equal name="idSubcategoria" value="22"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Full Cobertura</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="21"><h1 class="o-title o-title--primary">Cotizaci�n Seguro P�rdida Total</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="36"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Obligatorio Mercosur</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="79"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Robo Contenido</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="2">
				      	<logic:equal name="idSubcategoria" value="24"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Sismo / Estructura</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="43"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Sismo / Contenido</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="44"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Robo</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="25"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio, Robo y Sismo</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="26"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Premio a la Permanencia</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="201"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Full Asistencia</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="220"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Vacaciones</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="3">
				      	<logic:equal name="idSubcategoria" value="29"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Viaje Grupal</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="5">
				      	<logic:equal name="idSubcategoria" value="139"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Fraude</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="159"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Fraude con devoluci�n</h1></logic:equal>
			      	</logic:equal>
            		<logic:equal name="idRama" value="6">
				      	<logic:equal name="idSubcategoria" value="119"><h1 class="o-title o-title--primary">Asistencia Viaje</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="140"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Mascota</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="221"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Viaje Grupal</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="7">
						<logic:equal name="idSubcategoria" value="161"><h1 class="o-title o-title--primary">Hospitalizaci�n con devoluci�n</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="162"><h1 class="o-title o-title--primary">Accidentes Personales con devoluci�n</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="164"><h1 class="o-title o-title--primary">Oncol�gico con devoluci�n</h1></logic:equal>
			      	</logic:equal>
			      	<logic:equal name="idRama" value="8">
				      	<logic:equal name="idSubcategoria" value="239"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Obligatorio Mercosur</h1></logic:equal>
			      	</logic:equal>
			</div>
            <div class="col-12">
              <section class="o-box o-box--recruiting u-mtb50">
                <div class="o-steps">
                  <div class="o-steps__item o-steps__item--Completed"><span class="o-steps__milestone"><span class="o-steps__value">1</span></span>
                    <h4 class="o-steps__title">Informaci�n contratante</h4>
                  </div>
                  <div class="o-steps__item o-steps__item--current"><span class="o-steps__milestone"><span class="o-steps__value">2</span></span>
                    <h4 class="o-steps__title">Datos del seguro</h4>
                  </div>
                  <div class="o-steps__item"><span class="o-steps__milestone"><span class="o-steps__value">3</span></span>
                    <h4 class="o-steps__title">Confirmaci�n y pago</h4>
                  </div>
                </div>
                <div class="u-pad20">
                  <div class="row">
                    <div class="col-lg-8 u-mobile_second">
                      <h2 class="o-title o-title--subtitle u-mb10">Datos del Seguro</h2>
                      <p class="o text"></p>
                      <form class="o-form o-form--standard o-form--linear o-form--small" id="form_recruiting_step2" action="">
                      
                        <div class="row">
                          <div class="col-lg-9"><strong class="o-text">�Su vivienda se encuentra en una zona rural o urbana? *</strong></div>
                          <div class="col-lg-3">
                            <div class="o-form__field--inline u-mmt10">
                              <select class="o-form__select" id="pregunta1" name="pregunta1"
												onchange="respuestaExcluyente(this.value==='true', 1)">
												<option value="false">Rural</option>
												<option selected value="true">Urbana</option>
											</select>
                              <div class="u-clearfix"></div><span class="o-form__message"></span>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-9"><strong class="o-text">�Su vivienda est� construida parcial o totalmente de adobe? *</strong></div>
                          <div class="col-lg-3">
                            <div class="o-form__field--inline u-mmt10">
                              <select class="o-form__select" id="pregunta2" name="pregunta2"
												onchange="respuestaExcluyente(this.value==='true', 2)">
												<option value="false">S�</option>
												<option selected value="true">No</option>
											</select>
                              <div class="u-clearfix"></div><span class="o-form__message"></span>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-9"><strong class="o-text">�Su vivienda corresponde a su lugar permanente de residencia? *</strong></div>
                          <div class="col-lg-3">
                            <div class="o-form__field--inline u-mmt10">
                              <select class="o-form__select" id="pregunta3" name="pregunta3"
												onchange="respuestaExcluyente(this.value==='true', 3)">
												<option selected value="true">S�</option>
												<option value="false">No</option>
											</select>
                              <div class="u-clearfix"></div><span class="o-form__message"></span>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-9"><strong class="o-text">Seguridad: �Cuenta con rejas en ventanas o alarma o guardia / Conserje 24 hrs? *</strong></div>
                          <div class="col-lg-3">
                            <div class="o-form__field--inline u-mmt10">
                              <select class="o-form__select" id="pregunta4" name="pregunta4"
												onchange="respuestaExcluyente(this.value==='true', 4)">
												<option selected value="true">S�</option>
												<option value="false">No</option>
											</select>
                              <div class="u-clearfix"></div><span class="o-form__message"></span>
                            </div>
                            <script type="text/javascript">
											function respuestaExcluyente(valor, pregunta){
												if($("select[name='pregunta1']").val()=='false'||
														$("select[name='pregunta2']").val()=='false'||
														$("select[name='pregunta3']").val()=='false'||
														$("select[name='pregunta4']").val()=='false') {
													$('#divAviso').show();
													<logic:notPresent name="usuario">					
													$('#opciones-compra').hide();
													</logic:notPresent>
													<logic:present name="usuario">
														$('#continuar-compra').hide();
													</logic:present>
												}else{
													$('#divAviso').hide();
													<logic:notPresent name="usuario">					
														$('#opciones-compra').show();
													</logic:notPresent>
													<logic:present name="usuario">
														$('#continuar-compra').show();
													</logic:present>
												}
											};
											</script>
                          </div>
                        </div>
                        
                        
                        <div class="row u-hidden_desktop">
                          <div class="col-12">
                          <!--  
                            <div class="c-assistance u-mb30">
                              <h4 class="c-assistance__title">�Necesitas asistencia?</h4><a class="c-assistance__button o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="#">Te llamamos gratis</a>
                              <p class="c-assistance__text">O ll�manos gratis al<strong class="c-assistance__outstanding"> 600 500 5000</strong></p>
                            </div>-->
                          </div>
                        </div>
                        
                      </form>
                      
                    
                    <div class="o-form o-form--standard o-form--linear o-form--small">
                      <logic:equal value="false" name="datosCotizacion"
								property="clienteEsAsegurado">
					<div class="row u-mt40">
                          <div class="col-lg-6">
                            <div class="o-form__field">
                              <label class="o-form__label">Fecha de Nacimiento</label>
                              <div class="row">
											<div class="col-4">
											<div class="o-form__field o-form__field--select">
					                            <select fecha='fecha' name='idFechaDia' id='idFechaDia' class='o-form__select' onchange="concatenarFechaCompleta('idFechaDia', 'idFechaMes', 'idFechaAnyo', 'fechaCompleta')">
																	</select> 
												<span class="o-form__line"></span>
												</div>
					                          </div>
					                          <div class="col-4">
					                          <div class="o-form__field o-form__field--select">
												<select name='idFechaMes' id='idFechaMes' class='o-form__select' onchange="concatenarFechaCompleta('idFechaDia', 'idFechaMes', 'idFechaAnyo', 'fechaCompleta')">
																	</select>
												<span class="o-form__line"></span>
												</div>
					                          </div>
					                          <div class="col-4">
					                          <div class="o-form__field o-form__field--select">
												<select name='idFechaAnyo' id='idFechaAnyo' class='o-form__select' onchange="concatenarFechaCompleta('idFechaDia', 'idFechaMes', 'idFechaAnyo', 'fechaCompleta')">
																	</select><span class="o-form__line"></span>
												</div>
					                          </div>
								</div><span class="o-form__message" id="year_of_birth--recruiting"></span>
                              <input type="hidden" value="" id="fechaCompleta" fecha="fecha"/>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="o-form__field o-form__field--select">
                              <label class="o-form__label">Estado Civil*</label>
                              <html:select property="datos(estadoCivil)"
													styleClass="o-form__select" size="1"
													styleId="datos.estadoCivil">
													<html:option value="">Seleccione</html:option>
													<html:options collection="estadosCiviles" property="id"
														labelProperty="descripcion" />
												</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="o-form__field o-form__field--select">
                              <label class="o-form__label">Sexo*</label>
                              <select name="datos(asegSexo)" class="o-form__select" id="datos.sexo">
													<option value="" selected>Seleccione</option>
													<option value="F">Femenino</option>
													<option value="M">Masculino</option>
												</select><span class="o-form__line"></span><span class="o-form__message"></span>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="o-form__field">
                              <label class="o-form__label">Tel�fono</label>
                              <div class="row">
                                <div class="col-4">
                                <div class="o-form__field o-form__field--select">
                                  <html:select property="datos(asegTipoTelefono1)"
														styleClass="o-form__select" styleId="datos.asegTelefono1_1">
														<html:option value="">Seleccione</html:option>
														<html:options collection="tipoTelefono" property="valor"
															labelProperty="descripcion" />
													</html:select><span class="o-form__line"></span>
								 </div>
                                </div>
                                <div class="col-3">
                                <div class="o-form__field o-form__field--select">
                                  <html:select property="datos(asegCodigoTelefono1)"
														styleClass="o-form__select" styleId="datos.asegTelefono1_2">
														<html:option value="">Seleccione</html:option>
														<html:options collection="codigoArea" property="valor"
															labelProperty="descripcion" />
													</html:select><span class="o-form__line"></span>
								 </div>
                                </div>
                                <div class="col-5">
                                <div class="o-form__field o-form__field--select">
                                  <html:text property="datos(asegNumeroTelefono1)"
														styleClass="o-form__input" size="10" maxlength="10"
														styleId="datos.asegTelefono1_3">
													</html:text><span class="o-form__line"></span>
								 </div>
                                </div>
                              </div><span class="o-form__message" id="phone--recruiting"></span>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="o-form__field o-form__field--select">
                              <label class="o-form__label">Region*</label>
                              <html:select property="datos(asegRegion)"
													styleClass="o-form__select" styleId="asegRegion">
													<html:option value="">Seleccione</html:option>
													<html:options collection="regiones" property="id"
														labelProperty="descripcion" />
												</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="o-form__field o-form__field--select">
                              <label class="o-form__label">Comuna*</label>
                              <html:select property="datos(asegComuna)"
													styleClass="o-form__select" styleId="asegComuna">
													<html:option value="">Seleccione</html:option>
												</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="o-form__field o-form__field--select">
                              <label class="o-form__label">Ciudad*</label>
                              <html:select property="datos(asegCiudad)"
													styleClass="o-form__select" styleId="asegCiudad">
													<html:option value="">Seleccione</html:option>
												</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="o-form__field">
                              <label class="o-form__label">Direcci�n</label>
                              <div class="row">
                                <div class="col-6">
                                  <html:text property="datos(asegCalle)"
													styleClass="o-form__input datos-calle-input" size="25"
													styleId="asegCalle" maxlength="20" onkeypress="return validar(event)">
												</html:text><span class="o-form__line"></span>
                                </div>
                                <div class="col-3">
                                  <html:text property="datos(asegNumeroCalle)"
													styleClass="o-form__input datos-direccion-input" size="5"
													styleId="asegNumeroCalle" maxlength="5">
												</html:text><span class="o-form__line"></span>
                                </div>
                                <div class="col-3">
                                  <html:text property="datos(asegNumeroDepto)"
													styleClass="o-form__input datos-depto-input optional" size="10"
													styleId="asegNumeroDepto" maxlength="5">
												</html:text><span class="o-form__line"></span>
                                </div>
                              </div><span class="o-form__message" id="direction--recruiting"></span>
                            </div>
                          </div>
                          <div class="col-12 u-mb30">
                            <hr class="u-line u-line--gray">
                          </div>
                        </div>
					</logic:equal>
                      </div>
                      <!-- BOTONES -->
                        <logic:notPresent name="usuario">
                    <div class="row o-actions o-actions--flex">
                          <div class="col-lg-3"> 
                            <div class="o-form__field is-last"><a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" onclick="switchForm(false)"><i class="mx-arrow-left"></i> Volver </a></div>
                          </div>
                          <div class="col-lg-3 offset-lg-6">
                            <div class="o-form__field is-last">
                              <button class="o-btn o-btn--primary o-btn o-btn--icon" id="send_form_recruiting_step2" type="button" onclick="showModal()">Siguiente<i class="o-icon">
                                  <svg width="13px" height="14px" viewbox="0 0 13 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                      <g id="botones/orangeButton+rightArrow" transform="translate(-113.000000, -19.000000)" fill-rule="nonzero" fill="#FFFFFF">
                                        <g id="orangeButton+rightArrow">
                                          <g id="arrows" transform="translate(113.000000, 19.000000)">
                                            <path id="Shape" d="M12.6595914,6.18869213 L6.92029216,0.341285393 C6.69713064,0.113730337 6.42956532,0 6.11805938,0 C5.81235867,0 5.54791211,0.113730337 5.32456532,0.341285393 L4.66341805,1.01492135 C4.44004038,1.23052135 4.32838242,1.49998202 4.32838242,1.82336629 C4.32838242,2.14668764 4.44004038,2.41614831 4.66341805,2.63181124 L7.24652732,5.27252584 L1.03990736,5.27252584 C0.734361045,5.27252584 0.486033254,5.38477753 0.295016627,5.60937528 C0.104,5.83397303 0.00849168646,6.10494382 0.00849168646,6.42222472 L0.00849168646,7.57198652 C0.00849168646,7.8893618 0.104030879,8.1602382 0.295016627,8.38483596 C0.486002375,8.60943371 0.734361045,8.72159101 1.03990736,8.72159101 L7.2464038,8.72159101 L4.66332542,11.3535281 C4.43994774,11.5809258 4.32828979,11.8534382 4.32828979,12.1709079 C4.32828979,12.4883146 4.43994774,12.7607955 4.66332542,12.9881933 L5.32447268,13.6618292 C5.5537791,13.8834067 5.81822565,13.9941798 6.11796675,13.9941798 C6.42363658,13.9941798 6.69114014,13.8834067 6.9202304,13.6618292 L12.6594988,7.81442247 C12.8769786,7.59284494 12.9857957,7.32045843 12.9857957,6.9970427 C12.9857957,6.66774382 12.8769786,6.3981573 12.6595914,6.18869213 Z"></path>
                                          </g>
                                        </g>
                                      </g>
                                    </g>
                                  </svg></i></button>
                            </div>
                          </div>
                        </div>
                    </logic:notPresent>
                    <div class="col-lg-12">
								<logic:notPresent name="usuario">
													
								<!-- BOTONES COLAPSABLES -->
									<div class="form-group" id="opciones-compra">
										<div class="row">
										  <div class="panel-group" id="accordion-botones" role="tablist" aria-multiselectable="true">
											<!--<div class="panel">
											  <div class="panel-heading" role="tab" id="1">
												<div class="panel-title2"> <a class="btn btn-primary collapsed" role="button" data-toggle="collapse" 
												data-parent="#accordion-botones" aria-expanded="true" href="#collapsebot1"> 
												COMPRA SIN REGISTRARTE </a> </div>
											  </div>
											  <div id="collapsebot1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="1">
												<div class="panel-body">
												  <p class="text-center">Puedes realizar tus compras sin necesidad de estar registrado.</p>
												  <a class="btn btn-primary btn-block" href="javascript:sinRegistro();" role="button">CONTINUAR COMPRA</a> </div>
											  </div>
											</div>-->
											<div class="top15 hidden-lg"></div>
											<div class="panel">
											  <div  role="tab" id="2">
												<!--<div class="panel-title2"> <a class="btn btn-primary collapsed" role="button" data-toggle="collapse" 
												data-parent="#accordion-botones" aria-expanded="false" href="#collapsebot2"> 
												USUARIO REGISTRADO </a> </div>-->
												
											  </div>
											  <div id="collapsebot2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="2">
												<div class="panel-body">
												<!-- NUEVO MODAL -->
												
												<div class="remodal c-modal c-modal--modal-options" data-remodal-id="continueModal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">   
        <button class="remodal-close" data-remodal-action="close" aria-label="Close">  </button>
        <div class="c-modal__wrap">
          <div class="c-modal__modalHead">
            <div class="c-modal__modalHead-inner"> 
              <h2>Selecciona como deseas continuar tu compra</h2>
            </div>
          </div>
          <div class="c-modal__modalBody">
            <div class="row">
              <div class="col-md-12"> 
                <div class="item guest">
                  <h3>Continuar como invitado</h3>
                  <button class="o-btn o-btn--primary o-btn--medium" id="enter" type="button" onclick="sinRegistro()">Comprar</button>
                  <p>Podr�s realizar tus compras sin estar registrado y crear tu contrase�a despu�s. </p>
                </div>
              </div>
              <!--<div class="col-md-6"> 
                <div class="item login">
                  <h3>O como usuario registrado</h3>
                  <form class="o-form--loginForm" id="login">
                  <input type="hidden" name="username" id="username" value="" /> 
												<input type="hidden" name="password" id="password" value="" /> 
												<input type="hidden" name="rutpaso2" id="rutpaso2" value="" /> 
												<input type="hidden" name="dvpaso2" id="dvpaso2" value="" />
                    <div class="row">
                      <div class="col-lg-12">
                        <div class="o-form__field">
                          <label class="o-form__label">RUT</label>
                          <input class="o-form__input" id="rutpaso2completo" maxlength="10" required size="30" type="text"><span class="o-form__message"></span>
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <div class="o-form__field">
                          <label class="o-form__label">Contrase�a</label>
                          <html:password property="datos(clavepaso2)" styleClass="o-form__input" maxlength="6" styleId="clavepaso2"/><span class="o-form__message"></span>
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <div class="o-form__field">
                          <button class="o-btn o-btn--primary o-btn--medium" type="button" 
												  onclick="javascript:submitPaso2Hogar();">Ingresar y continuar  </button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>-->
            </div>
          </div>
        </div>
      </div>
												
												<!-- FIN NUEVO MODAL -->
												  <!--  <p>
													<input class="form-control" id="rutpaso2completo" maxlength="10" required size="30" type="text">
													<br>
													<html:password property="datos(clavepaso2)" styleClass="form-control" maxlength="6" styleId="clavepaso2"/>
													<br>
												  </p>
												  <button type="button" class="btn btn-primary btn-block" id="submitPaso2Vehiculo" name="submitPaso2Vehiculo" 
												  onclick="javascript:conRegistro();">INGRESAR</button>
												  
												  <a class="small" href="javascript:olvidasteClave();">�Olvidaste tu clave?</a> -->
												  <script type="text/javascript">
													<logic:present name="datosCotizacion">
														$("#rutpaso2").val("<bean:write name='datosCotizacion' property='rutCliente'/>");	
														$("#dvpaso2").val("<bean:write name='datosCotizacion' property='dv'/>");
														$("#rutpaso2completo").val($("#rutpaso2").val()+'-'+$("#dvpaso2").val());
														$("#rutpaso2completo").attr("readonly", "true");
													</logic:present>
												  </script>
												</div>
											  </div>
											</div>
											<!-- <div class="top15 hidden-lg"></div> -->
											<!--<div class="panel panel-default col-md-4" style="margin-top:0">
											  <div class="panel-heading" role="tab" id="3">
												<div class="panel-title2"> 
													<a class="btn btn-primary" role="button" data-toggle="collapse" data-parent="#accordion-botones" 
													aria-expanded="false" href="#collapsebot3"> �ERES NUEVO? </a> 
												</div>
											  </div>
											  <div id="collapsebot3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="3">
												<div class="panel-body">
												  <p class="text-center">
													Reg�strate aqu� y podr�s revisar tus p�lizas contratadas, cotizaciones y cotizar de manera m�s r�pida
												  </p>
												  <a class="btn btn-primary btn-block" onclick="javascript:registroRemoto();" role="button">AQU�</a> </div>
											  </div>
											</div>-->
										  </div>
										</div>
									</div>
									<!--<div class="col-md-3 hidden-xs">
										<button type="button" class="btn btn-primary"
											onclick="switchForm(false)">Volver</button>
									</div>
									<div class="col-md-12 hidden-lg text-center">
										<button type="button" class="btn btn-primary top15"
											onclick="switchForm(false)">Volver</button>
									</div>-->
								<!-- FIN BOTONES COLAPSABLES --> 
								</logic:notPresent>

								<logic:present name="usuario">	
									<div class="row top15">
										<div class="col-md-12" id="continuar-compra">
											<div class="col-md-4 hidden-xs pull-right">
												<button type="button" class="btn btn-primary btn-block"
													onclick="javascript:submitPaso2Hogar();">CONTINUAR</button>
											</div>
											<div class="col-md-4 hidden-lg bot15">
												<button type="button" class="btn btn-primary btn-block"
													onclick="javascript:submitPaso2Hogar();">CONTINUAR</button>
											</div>
										</div>
										<div class="col-md-3 hidden-xs">
											<button type="button" class="btn btn-primary"
												onclick="switchForm(false)">Volver</button>
										</div>
										<div class="col-md-12 hidden-lg text-center">
											<button type="button" class="btn btn-primary top15"
												onclick="switchForm(false)">Volver</button>
										</div>
									</div>
								</logic:present>
								</div>
                        <!-- FIN BOTONES -->
                   
                    </div>
                     <div class="col-lg-4 u-mobile_first">
                      <section class="c-summary" id="summary_sure">
                        <h2 class="c-summary__title">Resumen de tu seguro</h2>
                        <div class="c-summary__company"></div>
                        <h3 class="c-summary__subtitle"><bean:write name="datosPlan" property="nombrePlan" /></h3>
                        <div class="row">
                          <div class="col-12">
                            <div class="c-summary__cost">
                              <div class="c-summary__label">Monto Total:</div>
                              <div class="c-summary__price"><sup>$</sup><bean:write name="datosPlan" property="primaMensualPesos"
								format="##,##0" locale="currentLocale" /><sub>/Mes*</sub></div>
                            </div>
                            <p class="c-summary__legal"></p>
                          </div>
                          <div class="col-12 u-mobile_second">
                          </div>
                        </div>
                        <!--  <div class="c-assistance u-mb30 u-hidden_mobile">
                          <h4 class="c-assistance__title">�Necesitas asistencia?</h4><a class="c-assistance__button o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="#">Te llamamos gratis</a>
                          <p class="c-assistance__text">O ll�manos gratis al<strong class="c-assistance__outstanding"> 600 500 5000</strong></p>
                        </div>-->
                      </section>
                    </div>
                  </div>
                  <!-- PUNTOOOOOOOOOOOOOOOOOOOOOOOOOOOOO -->
					
                </div>
              </section>
            </div>
          </div>
          
        </section>
      
							
							
								</div>
    							</main>
							</div>
						</div>
						
						<!--FIN PASO2-->
					</div>
			</html:form>
		</div>
	</div>
	<!-- INICIO ModalRegistroRemoto -->
	<div class="modal fade" id="modalRegistrate" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4>Registrate</h4>
				</div>
				<div class="modal-body">
					<iframe class="" frameborder="0" id="cargaRegistrate" width="100%" height="500px">
					</iframe>	
				</div>
			</div>
		</div>
	</div> 
	<!-- FIN ModalRegistroRemoto -->	
	<!-- FIN CONTENIDOS -->
	<%@ include file="./includes/cotizador-footer.jsp"%>
</body>
</html>