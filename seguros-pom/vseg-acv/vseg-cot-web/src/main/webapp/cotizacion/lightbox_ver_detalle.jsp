<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	</head>

	<link href="/vseg-paris/css/estilo-index.css" rel="stylesheet" type="text/css" media="all">
	<script type="text/javascript" src="/vseg-paris/js/jquery/jquery-1.4.2.js"></script>

	<script type="text/javascript"> 
		function cargarFichas() {
			$("#tab1").load('/vseg-paris-adm/obtener-archivo.do?tipo=beneficio&id_ficha=<bean:write name="idFicha" />');
			$("#tab2").load('/vseg-paris-adm/obtener-archivo.do?tipo=cobertura&id_ficha=<bean:write name="idFicha" />');
			$("#tab3").load('/vseg-paris-adm/obtener-archivo.do?tipo=exclusion&id_ficha=<bean:write name="idFicha" />');
			$("#tab4").load('/vseg-paris-adm/obtener-archivo.do?tipo=aspecto_legal&id_ficha=<bean:write name="idFicha" />');
			
		}
		
		$(document).ready(function() {
			//Default Action
			$(".tab_content").hide(); //Hide all content
			$("ul.tabs li:first").addClass("active").show(); //Activate first tab
			$(".tab_content:first").show(); //Show first tab content
			
			//On Click Event
			$("ul.tabs li").click(function() {
				$("ul.tabs li").removeClass("active"); //Remove any "active" class
				$(this).addClass("active"); //Add "active" class to selected tab
				$(".tab_content").hide(); //Hide all tab content
				var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
				$(activeTab).show(); //Fade in the active content
				return false;
			});
		});
	</script>

	<body onload="cargarFichas();">
		<div class="container">
			<ul class="tabs">
				<li><a href="#tab1">beneficios</a></li>
				<li><a href="#tab2">coberturas</a></li>
				<li><a href="#tab3">exclusiones</a></li>
				<li><a href="#tab4">condiciones</a></li>
			</ul>
			<ul class="tabs">
				<div class="espacio-linea"></div>
			</ul>
			<div class="tab_container" >
				<div id="tab1" class="tab_content">
					<h3><div id="elegir-seguro-fichas">
					</div></h3>
				</div>
				<div id="tab2" class="tab_content"></div>
				<div id="tab3" class="tab_content"></div>
				<div id="tab4" class="tab_content"></div>
			</div>
		</div>
	</body>	
</html>
