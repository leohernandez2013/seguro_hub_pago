<!DOCTYPE html>
<%@page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="cl.tinet.common.struts.form.ValidableActionForm"%>
<%@page import="org.apache.struts.Globals"%>
<%@page import="cl.cencosud.acv.common.Producto"%>
<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>
<%@page import="cl.tinet.common.seguridad.servlet.AutenticacionServlet"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<html lang="es">
<head>
<%@ include file="./includes/cotizador-head.jsp"%> 
<style type="text/css">
.ui-datepicker {
	color: #000000 width :       216px;
	height: auto;
	margin: 5px auto 0;
	font: 9pt Arial, sans-serif;
	-webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
	-moz-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
	box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
}

.ui-datepicker-month {
	color: #000000
}

.ui-datepicker-year {
	color: #000000
}
</style>

<script type="text/javascript">	
		
		$(document).ready(function(){
			$("select#regionResidencia").change(function() {
				$.getJSON('/cotizador/buscar-comunas.do?idRegion=' + this.value, function(data){
					$('select#comunaResidencia option').remove();
					var options = '';
					options += '<option value="">Seleccione</option>';				
					for(var i = 0; i < data.length; i++) {
						options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
					}
					$('select#comunaResidencia').html(options);

				});
			});
			
			// INI cambio calendario
    		// GC:24/01/17
    		//se llaman las funciones declaradas en cotizador-head.jsp
    		cargaDia('idFechaDia');
 			cargaMes('idFechaMes');
 			cargaAnyo('idFechaAnyo'); 
               
            /*Cargamos campos para el "contratante no es el mismo que el asegurado", en cotizador-head.jsp*/ 	   
 			cargaDiaNoAseg('idFechaDia1');
 			cargaMesNoAseg('idFechaMes1');
 			cargaAnyoNoAseg('idFechaAnyo1'); 
 			
    		// FIN cambio calendario
			
		});
		
		</script>

<script type="text/javascript">
			function obtenerDescuento() {
				personalInfo();
				document.getElementById('cotizarProductoForm').action="/cotizador/preferente-fraude.do";
				document.getElementById('cotizarProductoForm').method="post";
				document.getElementById('cotizarProductoForm').submit();
			}
			
			$(document).ready(function(){
				$("#paso3").hide();
				aseguradoViaje('true');
				$("#paso2").hide();
				$('.datos-nombre-input').attr('placeholder','Ingrese su nombre');
				$('.datos-paterno-input').attr('placeholder','Ingrese su apellido paterno');
				$('.datos-materno-input').attr('placeholder','Ingrese su apellido materno');
				$('.datos-rut-input').attr('placeholder','Ej: 1234567-9');
				$('.datos-email-input').attr('placeholder','ejemplo@correo.cl');
				$('.datos-telefono-input').attr('placeholder','00 0000 00 00');
				$('.datos-calle-input').attr('placeholder','calle');
				$('.datos-direccion-input').attr('placeholder','n�');
				$('.datos-depto-input').attr('placeholder','n� depto/casa');
				$('.datos-nombre-mascota-input').attr('placeholder','Nombre Mascota');
				$('.datos-raza-input').attr('placeholder','Raza Mascota');
				$('.datos-color-input').attr('placeholder','Color Mascota');
				regionToRoman('regionResidencia');
				
				var val_num_subcategoria = "<bean:write name="idSubcategoria"/>";
				$("input[name='datos(numSubcategoria)']").val(val_num_subcategoria);
				
				var val_codigo_cel = "<bean:write name='cotizarProducto' property='datos(codigoTelefono)'/>";
				$("select[name='datos(codigoTelefono)']").val(val_codigo_cel);
				
															
				if($("select#regionResidencia").val() != null && $("select#regionResidencia").val() != "" ) {	
					var valor = "";
					$.getJSON('/cotizador/buscar-comunas.do?idRegion=' + $("select#regionResidencia").val(), function(data){
						$('select#comunaResidencia option').remove();
						var options = '';
						options += '<option value="">Seleccione</option>';
						
						for(var i = 0; i < data.length; i++) {
							options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
						}
						$('select#comunaResidencia').html(options);

						<logic:present property="datos(comunaResidenciaDuenyo)" name="cotizarProducto">
							valor = "<bean:write name='cotizarProducto' property='datos(comunaResidenciaDuenyo)'/>";
							$("select#comunaResidencia").val(valor);		
						</logic:present>	
					});	
				}
				
			});
		
			function aseguradoViaje(val) {
				if(val==='true') {
					$(".form-asegurado-viaje").hide();
				} else {
					$(".form-asegurado-viaje").show();
					var aTag = $("a[name='datosAsegurado']");
					$('html,body').animate({scrollTop: aTag.offset().top},'slow');
				}
			}	
			
			function submitFraude(){	
				<logic:equal value="6" name="idRama">
					<logic:equal name="idSubcategoria" value="221">
						personalInfo();
						validateInput("paso1");	
					</logic:equal>
					<logic:notEqual name="idSubcategoria" value="221">
						validateInput("paso2");	
					</logic:notEqual>
				</logic:equal>
				<logic:notEqual value="6" name="idRama">
					personalInfo();
					validateInput("paso1");
				</logic:notEqual>
				
				var edadCorrectaAsegurado = true;
				<logic:equal name="idSubcategoria" value="221">
					var fechaCompleta = $("#fechaCompleta").val();
					if ((fechaCompleta != "")&&(fechaCompleta != null )){
					 	if(validarFormatoFecha(fechaCompleta)){
							if (!calcularEdadFecha(fechaCompleta,18,75)){
								$("#incomplete-text-paso1").html("La edad requerida es mayor de 18 y menor de 75 a�os");
								$("#incomplete-paso1").show("fast");
								$("#idFechaDia").css("border-color","red");
								$("#idFechaMes").css("border-color","red");
								$("#idFechaAnyo").css("border-color","red");
								$("#fechaCompleta").focus();
								console.log(this.paso1 + ' erroneo');
								validacionInput = 1;
							}
						}else{
							$("#incomplete-text-paso1").html("La fecha de nacimiento para el contratante no es valida");
							$("#incomplete-paso1").show("fast");
							$("#idFechaDia").css("border-color","red");
							$("#idFechaMes").css("border-color","red");
							$("#idFechaAnyo").css("border-color","red");
							console.log(this.paso1 + ' erroneo');
							validacionInput = 1;
							return false;
						}
					}
				</logic:equal>
				
				if(validacionInput == 0) {
					var cientodiecinueve = 0;
					var docientosveintiuno = 0;
					<logic:equal name="idSubcategoria" value="119">
						docientosveintiuno = 1;
					</logic:equal>
					
					<logic:equal name="idSubcategoria" value="221">
						docientosveintiuno = 1;
					</logic:equal>

					if(cientodiecinueve != 0 || docientosveintiuno!= 0) {					
						var rutCompleto = document.getElementById('datos.rutDuenyoCompleto').value.split('-');
						document.getElementById('datos.rutDuenyo').value = rutCompleto[0];
						
						if(rutCompleto.length > 1) {
							document.getElementById('datos.dvDuenyo').value = rutCompleto[1];
						}
						
						
						var fechaCompleta = document.getElementById('fechaCompleta1').value.split('-');
						document.getElementById('diasFechaDuenyo').value = fechaCompleta[0];
						document.getElementById('mesFechaDuenyo').value = fechaCompleta[1];
						document.getElementById('anyosFechaDuenyo').value = fechaCompleta[2];
						
						if($("select[name='datos(contratanteEsDuenyo)']").val() == "false"){
							if(validarFormatoFecha($("#fechaCompleta").val())){
								if ((fechaCompleta != "")&&(fechaCompleta != null )){						
									if (!calcularEdadFecha(document.getElementById('fechaCompleta1').value,-1,75)){
										edadCorrectaAsegurado =	false;
									}
								}
							}else{
								$("#incomplete-text-paso1").html("La fecha de nacimiento para el asegurado no es valida");
								$("#incomplete-paso2").show("fast");
								$("#idFechaDia1").css("border-color","red");
								$("#idFechaMes1").css("border-color","red");
								$("#idFechaAnyo1").css("border-color","red");
								console.log(this.paso1 + ' erroneo');
								validacionInput = 1;
								return false;
							}
						}
							
					}
				
					if (!edadCorrectaAsegurado){
						$("#incomplete-text-paso2").html("La edad requerida debe ser menor a 75 a�os");
						$("#incomplete-paso2").show("fast");
						$("#fechaCompleta1").css("border-color","red");
						$("#fechaCompleta1").focus();
						console.log(this.paso2 + ' erroneo');
						validacionInput = 1;		
					}else{			
						$('html, body').animate({scrollTop: 0}, 0);
						$('#waitModal').modal({backdrop: 'static', keyboard: false});
						bar(66.7,1);
						
						<!--$('#grupo_valorizacion_iframe').hide();-->
						var caracter="1234567890";
						caracter+="QWERTYUIOPASDFGHJKLZXCVBNM";
						caracter+="qwertyuioplkjhgfdsazxcvbnm";
						var numero_caracteres=10;
						var total=caracter.length;
						var clave="";
						for(a=0;a<numero_caracteres;a++){
							clave+=caracter.charAt(parseInt(total*Math.random(1)));
						}
						document.getElementById('claveVitrineo').value = clave;	
						document.getElementById('hiddCaptcha').value = "0";
						document.getElementById('cotizarProductoForm').action="/cotizador/cotizar-producto-fraude.do";
						document.getElementById('cotizarProductoForm').method="post";
						document.getElementById('cotizarProductoForm').submit();				
					}
				}				
			}
		</script>
<%
	String glosaDescuento = (String) request.getSession().getAttribute(
			"glosaDescuento");
	if (glosaDescuento != null) {
%>
<script type="text/javascript">
				if(parent.$("input[name='captcha']").val()==""){
					parent.$("#descuento").trigger("click");	
				}
			</script>
<%
	}
%>

<script type="text/javascript">
$(document).ready(function(){
$('.datos-rut-input').blur(function(){
	var rut = $('.datos-rut-input').val(); 
	var cadena =rut.substr(0,1);
	if (cadena >=4 && cadena <= 9){
		if (rut != null){
			if (rut.length < 9){
				var cuerpo = rut.slice(0,-1);
				var dv = rut.slice(-1).toUpperCase(); 
				var completo = cuerpo + '-' + dv;
				
				$('.datos-rut-input').val("");
				$('.datos-rut-input').val(completo);
			}
		}
	}else if(cadena >= 1 && cadena <=3)
	if (rut != null){
		if (rut.length < 10){
		var cuerpo = rut.slice(0,-1);
		var dv = rut.slice(-1).toUpperCase(); 
		var completo = cuerpo + '-' + dv;
		
		$('.datos-rut-input').val("");
		$('.datos-rut-input').val(completo);
		}
	
	
	}else{
		$('.datos-rut-input').attr('placeholder','12345678-9');
	}
   });
   
   $('.datos-rut-input-duenho').attr('placeholder','12345678-9');
});
</script>
 
</head>
<body onload="desplegarErrores()">
	<%@ include file="/google-analytics/google-analytics.jsp"%>
	<%@ include file="./includes/cotizador-header.jsp"%>

	<!--  <div class="container contenedor-sitio"> -->
		<%@ include file="./includes/cotizador-breadcrumb.jsp"%>
		<!-- INICIO  CONTENIDOS -->
	<main role="main">
      <div class="remodal-bg">	
		<section class="container">
          <div class="row">
            <div class="col-12">
				<logic:equal name="idRama" value="1">
				      	<logic:equal name="idSubcategoria" value="22"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Full Cobertura</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="21"><h1 class="o-title o-title--primary">Cotizaci�n Seguro P�rdida Total</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="36"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Obligatorio Mercosur</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="79"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Robo Contenido</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="2">
				      	<logic:equal name="idSubcategoria" value="24"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Sismo / Estructura</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="43"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Sismo / Contenido</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="44"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Robo</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="25"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio, Robo y Sismo</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="26"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Premio a la Permanencia</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="201"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Full Asistencia</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="220"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Vacaciones</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="3">
				      	<logic:equal name="idSubcategoria" value="29"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Viaje Grupal</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="5">
				      	<logic:equal name="idSubcategoria" value="139"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Fraude</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="159"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Fraude con devoluci�n</h1></logic:equal>
			      	</logic:equal>
            		<logic:equal name="idRama" value="6">
				      	<logic:equal name="idSubcategoria" value="119"><h1 class="o-title o-title--primary">Asistencia Viaje</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="140"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Mascota</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="221"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Viaje Grupal</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="7">
						<logic:equal name="idSubcategoria" value="161"><h1 class="o-title o-title--primary">Hospitalizaci�n con devoluci�n</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="162"><h1 class="o-title o-title--primary">Accidentes Personales con devoluci�n</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="164"><h1 class="o-title o-title--primary">Oncol�gico con devoluci�n</h1></logic:equal>
			      	</logic:equal>
			      	<logic:equal name="idRama" value="8">
				      	<logic:equal name="idSubcategoria" value="239"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Obligatorio Mercosur</h1></logic:equal>
			      	</logic:equal>
			</div>
			<!--   include file="./includes/cotizador-resumen.jsp" -->
			<html:form action="cotizar-producto-fraude.do" method="post"
				styleId="cotizarProductoForm" styleClass="o-form o-form--standard o-form--linear o-form--small" target="iframe_tabla">
				<html:hidden property="datos(numSubcategoria)" styleId="numSubcategoria" />
				<html:hidden property="datos(rut)" value="" styleId="datos.rut" />
				<html:hidden property="datos(dv)" value="" styleId="datos.dv" />
				<html:hidden property="datos(diaFechaNacimiento)" value="" styleId="diasFecha" />
				<html:hidden property="datos(mesFechaNacimiento)" value="" styleId="mesFecha" />
				<html:hidden property="datos(anyoFechaNacimiento)" value="" styleId="anyosFecha" />
				<html:hidden property="datos(rutDuenyo)" value="" styleId="datos.rutDuenyo" />
				<html:hidden property="datos(dvDuenyo)" value="" styleId="datos.dvDuenyo" />
				<html:hidden property="datos(diaFechaNacimientoDuenyo)" value="" styleId="diasFechaDuenyo" />
				<html:hidden property="datos(mesFechaNacimientoDuenyo)" value="" styleId="mesFechaDuenyo" />
				<html:hidden property="datos(anyoFechaNacimientoDuenyo)" value="" styleId="anyosFechaDuenyo" />
				<html:hidden property="datos(claveVitrineo)" value="1" styleId="claveVitrineo" />
				<html:hidden property="datos(hiddCaptcha)" value="1" styleId="hiddCaptcha" />
				

				
				<input type="hidden" name="idPlanValorizacion"
					id="idPlanValorizacion" value="" />

				<%
					String idPlan = (String) request.getAttribute("idPlan");
						idPlan = (idPlan != null ? idPlan : "-1");
				%>
				<html:hidden property="datos(idPlan)" value="<%=idPlan %>"
					styleId="datos(idPlan)" />

			<!--	<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"> -->
					<!--  include file="./includes/cotizador-percent-bar.jsp" -->
			<!-- 	<div id="contenido-desplegado-cotizador"> -->
			<!--		<div id="globalExceptionDivPlanes" class=""></div> -->
						
		<div id="contenido-desplegado-cotizador">
					<div id="globalExceptionDivPlanes" class="">
						</div>
					
			<div id="paso1">
						
            
            <div class="col-12">
              <section class="o-box o-box--quotation u-mt20 u-mb40">
                <h2 class="o-title o-title--subtitle">Queremos saber de t�.</h2>
                  <div class="row u-mb50">
                    <div class="col-lg-4">
                      <div class="o-form__field">
                        <label class="o-form__label">Nombres</label>
                        <html:text property="datos(nombre)"
												styleClass="o-form__input datos-nombre-input" size="16"
												styleId="datos.nombre" maxlength="25"
												onkeypress="return validar(event)" />
												<span class="o-form__line"></span><span class="o-form__message"> </span>
                      </div>
                    </div>

                    <div class="col-lg-4">
                      <div class="o-form__field">
                        <label class="o-form__label">Apellido paterno</label>
                        <html:text property="datos(apellidoPaterno)"
												styleClass="o-form__input datos-paterno-input"
												styleId="datos.apellidoPaterno" size="16" maxlength="25"
												onkeypress="return validar(event)" />
												<span class="o-form__line"></span><span class="o-form__message"> </span>
                    </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="o-form__field">
                        <label class="o-form__label">Apellido materno</label>
                        <html:text property="datos(apellidoMaterno)"
												styleClass="o-form__input datos-materno-input"
												styleId="datos.apellidoMaterno" size="16" maxlength="25"
												onkeypress="return validar(event)" />
												<span class="o-form__line"></span><span class="o-form__message"> </span>
                      </div>
                    </div>
                  </div>
                  <div class="row u-mb50">
                    <div class="col-lg-4">
                      <div class="o-form__field">
                        <label class="o-form__label">RUT</label>
                        <logic:present name="usuario">
												<input type="text" class="o-form__input rut-completo" 
													placeholder="Ej: 1234567-9" disabled="disabled" id="datos.rutcompleto"
													value="<bean:write name='usuario' property='usuarioExterno.rut_cliente' />-<bean:write name='usuario' property='usuarioExterno.dv_cliente' />">
												
											</logic:present>
											<logic:notPresent name="usuario">
												<html:text styleClass="o-form__input datos-rut-input"
													property="datos(rutCompleto)" maxlength="10"
													styleId="datos.rutcompleto" onblur="obtenerDescuento();"
													onkeypress="return isRutKey(event)"	
													 />
													 <span class="o-form__line"></span><span class="o-form__message"> </span>
											</logic:notPresent>
                      </div>
                    </div>
					<!--INI  Fecha de nacimiento-->  
                    			<div class='col-lg-4'>
										<div class='o-form__field'>
											<label class="o-form__label">Fecha de Nacimiento</label>
											<div class="row">
											<div class="col-4">
												<div class='o-form__field o-form__field--select'>
					                            <select fecha='fecha' name='idFechaDia' id='idFechaDia' class='o-form__select is-children' onchange="concatenarFechaCompleta('idFechaDia', 'idFechaMes', 'idFechaAnyo', 'fechaCompleta')">
																	</select> 
												<span class="o-form__line"></span>
												</div>
					                          </div>
					                          <div class="col-4">
					                          <div class='o-form__field o-form__field--select'>
												<select name='idFechaMes' id='idFechaMes' class='o-form__select is-children' onchange="concatenarFechaCompleta('idFechaDia', 'idFechaMes', 'idFechaAnyo', 'fechaCompleta')">
																	</select>
												<span class="o-form__line"></span>
												</div>
					                          </div>
					                          <div class="col-4">
					                           <div class='o-form__field o-form__field--select'>
												<select name='idFechaAnyo' id='idFechaAnyo' class='o-form__select is-children' onchange="concatenarFechaCompleta('idFechaDia', 'idFechaMes', 'idFechaAnyo', 'fechaCompleta')">
																	</select><span class="o-form__line"></span>
												</div>
					                          </div>
											</div><span class="o-form__message" id="year_of_birth"></span>
											
											
											<!-- Input hiden para guardar la fecha-->
											<logic:notPresent name="usuario">
												<input type="hidden" value="" id="fechaCompleta" fecha="fecha"/>
											</logic:notPresent>
											<logic:present name="usuario">
												<input type="hidden"  id="fechaCompleta" fecha="fecha" value="<bean:write name='usuario' property='usuarioExterno.fecha_nacimiento' format='dd-MM-yyyy' />"/>
												<script>
													$(document).ready(function(){
														cargarSelectFechaNacimiento('fechaCompleta', 'idFechaDia', 'idFechaMes', 'idFechaAnyo');
													});
												</script>
											</logic:present>
										</div>
									</div>
					<!--FIN  Fecha de nacimiento-->
                    <div class="col-lg-4">
                      <div class="o-form__field o-form__field--select">
                        <label class="o-form__label">Estado Civil</label>
                        <html:select property="datos(estadoCivil)"
												styleClass="o-form__select"
												styleId="datos.estadoCivil">
												<html:option value="">Seleccione estado civil</html:option>
												<html:options collection="estadosCiviles" property="id"
													labelProperty="descripcion" />
						</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
                      </div>
                    </div>
                  </div>
                  <div class="row u-mb50">
                    <div class="col-lg-4">
                      <div class="o-form__field o-form__field--select">
                        <label class="o-form__label">Genero</label>
                        <html:select styleClass="o-form__select"
												property="datos(sexo)" styleId="datos.sexo">
												<html:option value="">Seleccione Sexo</html:option>
												<html:option value="F">Femenino</html:option>
												<html:option value="M">Masculino</html:option>
						</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="o-form__field">
                        <label class="o-form__label">Email</label>
                        <html:text property="datos(email)"
												styleClass="o-form__input datos-email-input"
												styleId="datos.email" maxlength="50"
												onkeypress="return isCaracterEmail(event);"></html:text>
												<span class="o-form__line"></span><span class="o-form__message"></span>
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="o-form__field">
                        <label class="o-form__label">Tel�fono</label>
                        <div class="row">
                          <div class="col-4">
                           <div class="o-form__field o-form__field--select">
                            <html:select property="datos(tipoTelefono)"
													styleClass="o-form__select is-children" styleId="datos.tipoTelefono">
													<html:option value="">Seleccione</html:option>
													<html:optionsCollection name="tipoTelefono"
														label="descripcion" value="valor" />
												</html:select><span class="o-form__line"></span>
							</div>
                          </div>
                          <div class="col-3">
                           <div class="o-form__field o-form__field--select">
                          	<html:select property="datos(codigoTelefono)"
													styleClass="o-form__select is-children" styleId="datos.codigoTelefono">
													<html:option value="">Seleccione</html:option>
													<html:optionsCollection name="codigoArea"
														label="descripcion" value="valor" />
												</html:select>
												<span class="o-form__line"></span><span class="o-form__message"></span>
							</div>
						</div>
						<div class="col-5">
                            <html:text property="datos(numeroTelefono)"
													styleClass="o-form__input datos-telefono-input is-children"
													maxlength="8" styleId="datos.numeroTelefono"
													onkeypress="return isNumberKey(event)">
												</html:text>
												<span class="o-form__line"></span><span class="o-form__message"></span>
                          </div>
                        </div><span class="o-form__message" id="phone"></span>
                      </div>
                    </div>
                  </div>
								
				  <div class="row" id="incomplete-paso1" style="display: none">
						<div class="col-lg-12 col-xs-12">
							<div class="alert alert-warning">
								<p align="center" id="incomplete-text-paso1"></p>
							</div>
						</div>
				  </div>
						

						
				  <logic:equal value="6" name="idRama">
									<logic:equal name="idSubcategoria" value="221">
										<%
										Producto[] productos = (Producto[]) request.getAttribute("productos");
										String cantProductos = String.valueOf(productos.length);
										%>
										<bean:define id="cantProd" value="<%=cantProductos%>" scope="page" />
                  <div class="row u-mb50">
				  
					<div class="col-lg-4">
							<div class="o-form__field o-form__field--select">
													<label class="o-form__label">Tramo d�as de viaje</label>
													<logic:iterate id="producto" name="productos">
														<%
														String idProducto = ((Producto) producto).getIdProducto();
														%>
														<html:hidden property="datos(producto)" styleId="producto" value="<%=idProducto%>" />
													</logic:iterate>
	
													<html:select property="datos(tramo)" styleClass="o-form__select" styleId="tramo">
														<option value="">Seleccione tramo</option>
														<option value="DE 1 A 8 DIAS">De 1 a 8 d�as</option>
														<option value="DE 9 A 15 DIAS">De 9 a 15 d�as</option>
														<option value="DE 16 A 22 DIAS">De 16 a 22 d�as</option>
														<option value="DE 23 A 30 DIAS">De 23 a 30 d�as</option>
														<option value="DE 31 A 45 DIAS">De 31 a 45 d�as</option>
														<option value="46 A 60 DIAS">De 46 a 60 d�as</option>
														<option value="61 A 90 DIAS">De 61 a 90 d�as</option>
														<option value="91 A 120 DIAS">De 91 a 120 d�as</option>
														<option value="121 A 180 DIAS">De 121 a 180 d�as</option>
														<option value="181 A 365 DIAS">De 181 a 365 d�as</option>
													</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
						</div>
					</div>
					<div class="col-lg-4">
												<div class="o-form__field o-form__field--select">
													<label class="o-form__label">Indicar cantidad de integrantes</label>
													<html:select property="datos(integrantes)" styleClass="o-form__select" styleId="integrantes">
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
													</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
												</div>
											</div>
                    <div class="col-lg-4">
                      <div class="o-form__field o-form__field--select">
                        <label class="o-form__label"><span class="o-help">El contratante es el mismo que el asegurado?</span></label>
								<html:select property="datos(contratanteEsDuenyo)" styleClass="o-form__select" styleId="contratanteAsegurado" 
													onchange="javascript:aseguradoViaje(this.value);">
														<option value="true" selected>Si</option>
														<option value="false">No</option>
								</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
                      </div>
                    </div>
               
                  </div>	  
				  <div class="form-asegurado-viaje row u-mb50">
					<div class="col-lg-12" >
					<h2 class="o-title o-title--subtitle">Datos del asegurado (Persona que viaja)</h2>
							
						<div class="row u-mb50">
												<div class='col-lg-4'>
													<div class='o-form__field'>
														<label class="o-form__label">Nombre</label>
														<html:text property="datos(nombreDuenyo)"
															styleClass="o-form__input datos-nombre-input" size="16"
															styleId="datos.nombreDuenyo" maxlength="25"
															onkeypress="return validar(event)" />
															<span class="o-form__line"></span><span class="o-form__message"></span>
													</div>
												</div>
												
												<div class='col-lg-4'>
													<div class='o-form__field'>
														<label class="o-form__label">Apellido paterno</label>
														<html:text property="datos(apellidoPaternoDuenyo)"
															styleClass="o-form__input datos-paterno-input"
															styleId="datos.apellidoPaternoDuenyo" size="16"
															maxlength="30" onkeypress="return validar(event)" />
															<span class="o-form__line"></span><span class="o-form__message"></span>
													</div>
												</div>
												<div class='col-lg-4'>
													<div class='o-form__field'>
														<label class="o-form__label">Apellido materno</label>
														<html:text property="datos(apellidoMaternoDuenyo)"
															styleClass="o-form__input datos-materno-input"
															styleId="datos.apellidoMaternoDuenyo" size="16"
															maxlength="30" onkeypress="return validar(event)" />
															<span class="o-form__line"></span><span class="o-form__message"></span>
													</div>
												</div>
										
						</div>
						
						<div class="row u-mb50">
											
												<div class="col-lg-4">
													<div class='o-form__field'>
														<label class="o-form__label">RUT</label>
														<html:text property="datos(rutDuenyoCompleto)"
															maxlength="10" styleClass="o-form__input datos-rut-input-duenho"
															styleId="datos.rutDuenyoCompleto"
															onkeypress="return isRutKey(event)"
															onkeyup="autocompletarrut(this)"/>
															<span class="o-form__line"></span><span class="o-form__message"></span>
													</div>
												</div>
												
												<!--INI  Fecha de nacimiento-->        
												<div class='col-lg-4' id="divFechaNacimiento">                        
													<div class="o-form__field">
														<label class="o-form__label">Fecha de Nacimiento</label>
														
														<div class="row">
														<div class='col-4'>
														<div class="o-form__field o-form__field--select">
															<select name='idFechaDia1' id='idFechaDia1' class='o-form__select is-children' onchange="concatenarFechaCompleta('idFechaDia1', 'idFechaMes1', 'idFechaAnyo1', 'fechaCompleta1')">
																	</select>
															<span class="o-form__line"></span>
															</div>
														</div>
														
														<div class='col-4'>
														<div class="o-form__field o-form__field--select">
															<select name='idFechaMes1' id='idFechaMes1' class='o-form__select is-children' onchange="concatenarFechaCompleta('idFechaDia1', 'idFechaMes1', 'idFechaAnyo1', 'fechaCompleta1')">
																	</select>
															<span class="o-form__line"></span>
															</div>
														</div>
														<div class='col-4'>
														<div class="o-form__field o-form__field--select">
															<select name='idFechaAnyo1' id='idFechaAnyo1' class='o-form__select is-children' onchange="concatenarFechaCompleta('idFechaDia1', 'idFechaMes1', 'idFechaAnyo1', 'fechaCompleta1')">
																	</select>
															<span class="o-form__line"></span>
															</div>
														</div>
														</div><span class="o-form__message" id="year_of_birth"></span>
														<!-- Input hiden para guardar la fecha-->
														<input type="hidden" value="" id="fechaCompleta1" fecha="fecha"/>
													</div>
												</div>                 
												<!--FIN  Fecha de nacimiento-->
												
												<div class='col-lg-4'>
													<div class="o-form__field o-form__field--select">
														<label class="o-form__label">Estado Civil</label>
														<html:select property="datos(estadoCivilDuenyo)"
															styleClass="o-form__select"
															styleId="datos.estadoCivilDuenyo">
															<html:option value="">Seleccione estado civil</html:option>
															<html:options collection="estadosCiviles" property="id"
																labelProperty="descripcion" />
														</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
													</div>
												</div>
												
												
						</div>
						
						<div class="row u-mb50">
											
												<div class='col-lg-4'>
													<div class="o-form__field o-form__field--select">
														<label class="o-form__label">Genero</label>
														<html:select styleClass="o-form__select"
															property="datos(sexoDuenyo)" styleId="datos.sexoDuenyo">
															<html:option value="">Seleccione Sexo</html:option>
															<html:option value="F">Femenino</html:option>
															<html:option value="M">Masculino</html:option>
														</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
													</div>
												</div>

												<div class="col-lg-4">
													<div class="o-form__field o-form__field--select">
														<label class="o-form__label">Region</label>
														<html:select property="datos(regionResidenciaDuenyo)"
															styleClass="o-form__select" styleId="regionResidencia">
															<html:option value="">Seleccione regi�n</html:option>
															<html:options collection="regiones" property="id"
																labelProperty="descripcion" />
														</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
													</div>
												</div>
												<div class="col-lg-4">
													<div class="o-form__field o-form__field--select">
														<label class="o-form__label">Comuna</label>
														<html:select property="datos(comunaResidenciaDuenyo)"
															styleClass="o-form__select" styleId="comunaResidencia">
															<html:option value="">Seleccione comuna</html:option>
														</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
													</div>
												</div>
						</div>
						
						
					
					</div>
				  </div>
				  
				  <logic:notEqual name="vaCaptcha" value="0">
						<%@ include file="./includes/cotizador-captcha.jsp"%>
				    </logic:notEqual>	
				  
				    
				<!-- <div class="row o-actions o-actions--flex" id="botonCotizarDiv">
										 		<div class="hidden-xs">
												<div class="col-lg-3">
													<div class="o-form__field is-last"><a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon"	href="/vseg-paris/desplegar-ficha.do?idRama=<bean:write name='idRama'/>&idSubcategoria=<bean:write name='idSubcategoria'/>" type="button"><i class="mx-arrow-left"></i> Volver</a></div>
												</div>
											</div>
												
											 <div class="col-lg-3">												
												<div class="o-form__field is-last"><a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon"	href="/vseg-paris/desplegar-ficha.do?idRama=<bean:write name='idRama'/>&idSubcategoria=<bean:write name='idSubcategoria'/>" type="button"><i class="mx-arrow-left"></i> Volver base</a></div>											                    
											</div> 
	
					</div> -->
					
					
				  </logic:equal>
				  <logic:notEqual name="idSubcategoria" value="221">
										<div class="row o-actions o-actions--flex">
											<!--  <div class="col-lg-3"> -->									
												<!-- <div class="o-form__field is-last"><a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon"	href="/vseg-paris/desplegar-ficha.do?idRama=<bean:write name='idRama'/>&idSubcategoria=<bean:write name='idSubcategoria'/>"><i class="mx-arrow-left"></i> Volver base</a></div> -->									                    
											<!-- </div> -->
											
										<!-- <div class="hidden-xs">
												<div class="col-md-3">
													<a	href="/vseg-paris/desplegar-ficha.do?idRama=<bean:write name='idRama'/>&idSubcategoria=<bean:write name='idSubcategoria'/>" type="button" class="btn btn-primary">Volver 221</a>
												</div>
											</div> -->
											
											
										<!--	
										<div class="col-md-3 col-md-offset-6">
												<button type="button"
													class="btn btn-primary btn-block pull-right"
													onclick="switchForm(true);">CONTINUAR</button>
											</div> -->
										<!-- 	<div class="col-lg-3">
												<div class="o-form__field is-last"><a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="/vseg-paris/desplegar-ficha.do?idRama=<bean:write name='idRama'/>&idSubcategoria=<bean:write name='idSubcategoria'/>" type="button" ><i class="mx-arrow-left"></i> Volver 221 2</a></div>						
											</div> -->
											
											<!-- Continuar cotizacion  -->
											<div class="col-lg-3 offset-lg-9 u-text-right">
									                      <div class="o-form__field is-last">
									                        <button class="o-btn o-btn--primary o-btn o-btn--icon" id="full_paso1" type="button" onclick="switchForm(true);">Cotizar<i class="o-icon">
									                            <svg width="13px" height="14px" viewbox="0 0 13 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
									                              <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									                                <g id="botones/orangeButton+rightArrow" transform="translate(-113.000000, -19.000000)" fill-rule="nonzero" fill="#FFFFFF">
									                                  <g id="orangeButton+rightArrow">
									                                    <g id="arrows" transform="translate(113.000000, 19.000000)">
									                                      <path id="Shape" d="M12.6595914,6.18869213 L6.92029216,0.341285393 C6.69713064,0.113730337 6.42956532,0 6.11805938,0 C5.81235867,0 5.54791211,0.113730337 5.32456532,0.341285393 L4.66341805,1.01492135 C4.44004038,1.23052135 4.32838242,1.49998202 4.32838242,1.82336629 C4.32838242,2.14668764 4.44004038,2.41614831 4.66341805,2.63181124 L7.24652732,5.27252584 L1.03990736,5.27252584 C0.734361045,5.27252584 0.486033254,5.38477753 0.295016627,5.60937528 C0.104,5.83397303 0.00849168646,6.10494382 0.00849168646,6.42222472 L0.00849168646,7.57198652 C0.00849168646,7.8893618 0.104030879,8.1602382 0.295016627,8.38483596 C0.486002375,8.60943371 0.734361045,8.72159101 1.03990736,8.72159101 L7.2464038,8.72159101 L4.66332542,11.3535281 C4.43994774,11.5809258 4.32828979,11.8534382 4.32828979,12.1709079 C4.32828979,12.4883146 4.43994774,12.7607955 4.66332542,12.9881933 L5.32447268,13.6618292 C5.5537791,13.8834067 5.81822565,13.9941798 6.11796675,13.9941798 C6.42363658,13.9941798 6.69114014,13.8834067 6.9202304,13.6618292 L12.6594988,7.81442247 C12.8769786,7.59284494 12.9857957,7.32045843 12.9857957,6.9970427 C12.9857957,6.66774382 12.8769786,6.3981573 12.6595914,6.18869213 Z"></path>
									                                    </g>
									                                  </g>
									                                </g>
									                              </g>
									                            </svg></i></button>
									                          </div>
									               </div>
											
											
										</div>
				   </logic:notEqual>
				   </logic:equal>
				   <logic:notEqual value="6" name="idRama">
									<logic:notEqual name="vaCaptcha" value="0">
										<%@ include file="./includes/cotizador-captcha.jsp"%>
									</logic:notEqual>
									
								<!--  <div class="row top15" id="botonCotizarDiv">
										<div class="hidden-xs">
											<div class="col-md-3">
												<a	href="/vseg-paris/desplegar-ficha.do?idRama=<bean:write name='idRama'/>&idSubcategoria=<bean:write name='idSubcategoria'/>" type="button" class="btn btn-primary">Volver rama6</a>
											</div>
										</div>
										
										<div class="col-md-3 top15 hidden-lg">
											<a	href="/vseg-paris/desplegar-ficha.do?idRama=<bean:write name='idRama'/>&idSubcategoria=<bean:write name='idSubcategoria'/>" type="button" class="btn btn-primary center-block">Volver rama 6 2</a>								
										</div>
									</div> -->
								</logic:notEqual>
							
				  
				  
          </section>       
          </div>    
          </div>
   
        
        

							
						
		
				<div id="paso2">
						<div class="col-lg-12">
							<div class="row">
								<main role="main">
									<div class="remodal-bg">
										<section class="container">
											<div class="row">
												<div class="col-12">
													<section class="o-box o-box--quotation u-mt20 u-mb40">
														<!--INICIO CONTRATACION TERCEROS-->
																	<logic:equal name="subcategoriaDesc" value="Asistencia en Viaje">
																		<div class='form-group'>
																			<label>El contratante es el mismo que el asegurado?</label>
																			<html:select styleClass="form-control"
																				property="datos(contratanteEsDuenyo)"
																				onchange="javascript:aseguradoViaje(this.value);">
																				<option value="true" selected>Si</option>
																				<option value="false">No</option>
																			</html:select>
																		</div>
																	</logic:equal>
																	<!--FIN CONTRATACION TERCEROS-->
																	<logic:equal name="subcategoriaDesc" value="Mascotas">
																	
																		<h2 class="o-title o-title--subtitle">Datos de la Mascota</h2>
																	</logic:equal>
																	
				
															<div class='form-group'>
																		<%
																		Producto[] productos = (Producto[]) request.getAttribute("productos");
																		String cantProductos = String.valueOf(productos.length);
																		%>
																		<bean:define id="cantProd" value="<%=cantProductos%>" scope="page" />
							
																		<!--INICIO ASISTENCIA EN VIAJE-->
																		<logic:equal name="subcategoriaDesc" value="Asistencia en Viaje">
																			<label class="o-form__label">Tramo d�as de viaje</label>
																			<logic:iterate id="producto" name="productos">
																				<%
																				String idProducto = ((Producto) producto).getIdProducto();
																				%>
																				<html:hidden property="datos(producto)" styleId="producto" value="<%=idProducto%>" />
																			</logic:iterate>
							
																			<html:select property="datos(tramo)" styleClass="o-form__select" styleId="tramo">
																				<option value="">Seleccione tramo</option>
																				<option value="DE 1 A 8 DIAS">De 1 a 8 d�as</option>
																				<option value="DE 9 A 15 DIAS">De 9 a 15 d�as</option>
																				<option value="DE 16 A 22 DIAS">De 16 a 22 d�as</option>
																				<option value="DE 23 A 30 DIAS">De 23 a 30 d�as</option>
																				<option value="DE 31 A 45 DIAS">De 31 a 45 d�as</option>
																				<option value="DE 46 A 60 DIAS">De 46 a 60 d�as</option>
																			</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
																		</logic:equal>
																		<!--FIN ASISTENCIA EN VIAJE-->
							
																		<logic:empty name="planesCotizados">
																			<logic:greaterThan value="1" name="cantProd" scope="page">
																				<label class="o-form__label">Elige tu seguro</label>
																				<html:select property="datos(producto)"
																					styleClass="o-form__select" styleId="producto">
																					<option value="">seleccione</option>
																					<html:options collection="productos" property="idProducto"
																						labelProperty="nombreProducto" />
																				</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
																			</logic:greaterThan>
																		</logic:empty>
																		<logic:lessEqual value="1" name="cantProd" scope="page">
																			<!--label>Tu Seguro:</label-->
																			<logic:iterate id="producto" name="productos">
																				<%
																					String idProducto = ((Producto) producto)
																										.getIdProducto();
																				%>
																				<html:hidden property="datos(producto)" styleId="producto"
																					value="<%=idProducto%>" />
																				<!--label><bean:write name="producto" property="nombreProducto" /></label-->
																			</logic:iterate>
																		</logic:lessEqual>
										
								   
															</div>
															
															<logic:equal name="subcategoriaDesc" value="Mascotas">
																 <div class="row">
																	<div class="col-12">

																		<div class="row u-mb50">
																							   
																			<div class="col-lg-4">
																				<div class="o-form__field">
																					<label class="o-form__label">Nombre</label>
																					<html:text property="datos(nombreMascota)"
																						styleClass="o-form__input datos-nombre-mascota-input"
																						size="16" styleId="datos.nombreMascota" maxlength="25"
																						onkeypress="return validar(event)" />
																						<span class="o-form__line"></span><span class="o-form__message"> </span>
																				</div>
																			</div>
																			<div class='col-lg-4'>
																				<div class='o-form__field o-form__field--select'>
																					<label class="o-form__label">Tipo</label>
																					<html:select property="datos(tipoMascota)"
																						styleClass="o-form__select"
																						styleId="datos.tipoMascota">
																						<html:option value="">Tipo Mascota</html:option>
																						<html:option value="Perro">Perro</html:option>
																						<html:option value="Gato">Gato</html:option>
																					</html:select>
																					<span class="o-form__line"></span><span class="o-form__message"></span>
																				</div>
																			</div>
																			<div class='col-lg-4'>
																				<div class='o-form__field'>
																					<label class="o-form__label">Raza</label>
																					<html:text property="datos(raza)"
																						styleClass="o-form__input datos-raza-input"
																						styleId="datos.raza" size="16" maxlength="30"
																						onkeypress="return validar(event)" />
																					<span class="o-form__line"></span><span class="o-form__message"></span>
																				</div>
																			</div>
																		</div>
																		<div class="row u-mb50">
																			<div class='col-lg-4'>
																				<div class='o-form__field'>
																					<label class="o-form__label">Color</label>
																					<html:text property="datos(colorMascota)"
																						styleClass="o-form__input datos-color-input"
																						styleId="datos.colorMascota" size="16" maxlength="30"
																						onkeypress="return validar(event)" />
																					<span class="o-form__line"></span><span class="o-form__message"></span>
																				</div>
																			</div>
																			<div class='col-lg-4'>
																				<div class='o-form__field o-form__field--select'>
																					<label class="o-form__label">Edad</label>
																					<html:select property="datos(edadMascota)"
																						styleClass="o-form__select datos-edad-input"
																						styleId="datos.edadMascota">
																						<html:option value="">Seleccione edad</html:option>
																						<html:option value="1">1</html:option>
																						<html:option value="2">2</html:option>
																						<html:option value="3">3</html:option>
																						<html:option value="4">4</html:option>
																						<html:option value="5">5</html:option>
																						<html:option value="6">6</html:option>
																						<html:option value="7">7</html:option>
																						<html:option value="8">8</html:option>
																						<html:option value="9">9</html:option>
																						<html:option value="10">10</html:option>
																						<html:option value="11">11</html:option>
																						<html:option value="12">12</html:option>
																						<html:option value="13">13</html:option>
																						<html:option value="14">14</html:option>
																						<html:option value="15">15</html:option>
																						<html:option value="16">16</html:option>
																						<html:option value="17">17</html:option>
																						<html:option value="18">18</html:option>
																						<html:option value="19">19</html:option>
																						<html:option value="20">20</html:option>
																					</html:select>
																					<span class="o-form__line"></span><span class="o-form__message"></span>
																				</div>
																			</div>
																			<div class='col-lg-4'>
																				<div class='o-form__field o-form__field--select'>
																					<label class="o-form__label">Sexo</label>
																					<html:select styleClass="o-form__select"
																						property="datos(sexoMascota)" styleId="datos.sexoMascota">
																						<html:option value="">Seleccione Sexo</html:option>
																						<html:option value="F">Femenino</html:option>
																						<html:option value="M">Masculino</html:option>
																					</html:select>
																					<span class="o-form__line"></span><span class="o-form__message"></span>
																				</div>
																			</div>
																		</div>
							
																		<div class="row u-mb50">
																			<div class='col-lg-4'>
																				<div class='form-group'>
																					<label class="o-form__label">Direcci�n</label>
																					<html:text property="datos(direccionMascota)"
																						styleClass="o-form__input datos-calle-input"
																						maxlength="25" styleId="datos.direccionMascota" onkeypress="return validar(event)"/>
																																	  
																			   
																						<span class="o-form__line"></span><span class="o-form__message"></span>
															   
																				</div>
											
																			</div>
																			<div class='col-lg-4'>
																				<div class='form-group'>
																					<label class="o-form__label">N�mero</label>
																					<html:text property="datos(numeroDirMascota)"
																						styleClass="o-form__input datos-direccion-input"
																						maxlength="5" size="5" styleId="datos.numeroDirMascota"
																						onkeypress="return isNumberKey(event)" />
																						<span class="o-form__line"></span><span class="o-form__message"></span>
																				</div>
																			</div>
																			<div class='col-lg-4'>
																				<div class='form-group'>
																					<label class="o-form__label">N�mero depto</label>
																					<html:text property="datos(numeroDeptoMascota)"
																						styleClass="o-form__input datos-depto-input optional" maxlength="5"
																						size="10" styleId="datos.numeroDeptoMascota"
																						onkeypress="return isNumberKey(event)" />
																						<span class="o-form__line"></span><span class="o-form__message"></span>
																				</div>
																				
																			</div>
																		</div>
																	</div>
															</div> 
															</logic:equal>
															
															<logic:equal name="subcategoriaDesc" value="Asistencia en Viaje">
														<div class="row">
																	<div class="form-asegurado-viaje col-lg-12">
																	
																		<h2 class="o-title o-title--subtitle">Datos del asegurado (Persona que viaja)</h2>
																		<div class="row u-mb50">
																			
																			<div class='col-lg-4'>
																				<div class='form-group'>
																					<label class="o-form__label">Nombre</label>
																					<html:text property="datos(nombreDuenyo)"
																						styleClass="o-form__input datos-nombre-input" size="16"
																						styleId="datos.nombreDuenyo" maxlength="25"
																						onkeypress="return validar(event)" />
																
																  
																				</div>
																			</div>
								   
								  
																			<div class='col-lg-4'>
																				<div class='form-group'>
																					<label class="o-form__label">Apellido paterno</label>
																					<html:text property="datos(apellidoPaternoDuenyo)"
																						styleClass="o-form__input datos-paterno-input"
																						styleId="datos.apellidoPaternoDuenyo" size="16"
																						maxlength="30" onkeypress="return validar(event)" />
																				</div>
																			</div>
																			<div class='col-sm-6'>
																				<div class='form-group'>
																					<label class="o-form__label">Apellido materno</label>
																					<html:text property="datos(apellidoMaternoDuenyo)"
																						styleClass="o-form__input datos-materno-input"
																						styleId="datos.apellidoMaternoDuenyo" size="16"
																						maxlength="30" onkeypress="return validar(event)" />
																				</div>
																			</div>
																		</div>
							
																		<div class="row u-mb50">
																		
																			<div class='col-lg-4'>
																				<div class='o-form__field'>
																					<label class="o-form__label">RUT</label>
																					<html:text property="datos(rutDuenyoCompleto)"
																						maxlength="10" styleClass="o-form__input datos-rut-input"
																						styleId="datos.rutDuenyoCompleto"
																						onkeypress="return isRutKey(event)"
																						onkeyup="autocompletarrut(this)"></html:text>
																				</div>
																			</div>
																			
																			<!--INI  Fecha de nacimiento-->        
																			<div class='col-lg-4' id="divFechaNacimiento">                        
																				<div class="o-form__field">
																					<label class="o-form__label">Fecha de Nacimiento</label>
																					<div class="row">
																					<div class='col-4'>
																						<input fecha="fecha" name='idFechaDia1' id='idFechaDia1' class='o-form__input' onchange="concatenarFechaCompleta('idFechaDia1', 'idFechaMes1', 'idFechaAnyo1', 'fechaCompleta1')">
																						</input><span class="o-form__line"></span>
																					</div>
																					
																					<div class='col-4'>
																						<input name='idFechaMes1' id='idFechaMes1' class='o-form__input' onchange="concatenarFechaCompleta('idFechaDia1', 'idFechaMes1', 'idFechaAnyo1', 'fechaCompleta1')">
																						</input><span class="o-form__line"></span>
																					</div>
																					<div class='col-4'>
																						<input name='idFechaAnyo1' id='idFechaAnyo1' class='o-form__input' onchange="concatenarFechaCompleta('idFechaDia1', 'idFechaMes1', 'idFechaAnyo1', 'fechaCompleta1')">
																						</input><span class="o-form__line"></span>
																					</div>
																					</div><span class="o-form__message" id="year_of_birth"></span>
																					
																					<!-- Input hiden para guardar la fecha-->
																					<input type="hidden" value="" id="fechaCompleta1" fecha="fecha"/>
																				</div>
																			
																			</div>                 
																			<!--FIN  Fecha de nacimiento-->
																			
																			<div class='col-lg-4'>
																				<div class="o-form__field">
																					<label class="o-form__label">Estado Civil</label>
																					<html:select property="datos(estadoCivilDuenyo)"
																						styleClass="o-form__select"
																						styleId="datos.estadoCivilDuenyo">
																						<html:option value="">Seleccione estado civil</html:option>
																						<html:options collection="estadosCiviles" property="id"
																							labelProperty="descripcion" />
																					</html:select>
																				</div>
																			</div>
																			
																		</div>
							
																		<div class="row u-mb50">
																			<div class='col-sm-6'>
																				<div class='form-group'>
																					<label class="o-form__label">Genero</label>
																					<html:select styleClass="o-form__select"
																						property="datos(sexoDuenyo)" styleId="datos.sexoDuenyo">
																						<html:option value="">Seleccione Genero</html:option>
																						<html:option value="F">Femenino</html:option>
																						<html:option value="M">Masculino</html:option>
																					</html:select>
																				</div>
																			</div>
																			<div class="col-sm-6">
																				<div class='form-group'>
																					<label class="o-form__label">Regi�n</label>
																					<html:select property="datos(regionResidenciaDuenyo)"
																						styleClass="o-form__select" styleId="regionResidencia">
																						<html:option value="">Seleccione regi�n</html:option>
																						<html:options collection="regiones" property="id"
																							labelProperty="descripcion" />
																					</html:select>
																				</div>
																			</div>
																			<div class="col-sm-6">
																				<div class='form-group'>
																					<label class="o-form__label">Comuna</label>
																					<html:select property="datos(comunaResidenciaDuenyo)"
																						styleClass="o-form__select" styleId="comunaResidencia">
																						<html:option value="">Seleccione comuna</html:option>
																					</html:select>
																				</div>
																			</div>
																		</div>
																						
																	</div>
																							   
															</div>
															</logic:equal>
															
															<logic:equal value="6" name="idRama">
																<logic:notEqual name="idSubcategoria" value="221">
																	<logic:notEqual name="vaCaptcha" value="0">
																		<%@ include file="./includes/cotizador-captcha.jsp"%>
																	</logic:notEqual>
																</logic:notEqual>
															</logic:equal>
															
					  
							
															<!-- Validadores adicionales -->
															<div class="row">
																<div class="col-lg-12 col-xs-12">
																	<div class="alert alert-warning" id="incomplete-paso2"
																		style="display: none">
																		<p align="center" id="incomplete-text-paso2"></p>
																	</div>
																</div>
															</div>
															
															
														<!--  <div class="row top15" id="botonCotizarDiv">
																<div class="hidden-xs">
																	<div class="col-md-3">
																		<button type="button" class="btn btn-primary"
																			onclick="switchForm(false);">Volver</button>	
																	</div>
																</div>
																
																<div class="col-md-3 top15 hidden-lg">
																	<br> <br>
																	<button type="button" class="btn btn-primary center-block"
																		onclick="switchForm(false);">Volver</button>		
																</div>
				
															</div>	-->
					 
								 
																		   
													</form>
												</section>
											</div>
										</div>
								 
								
										</section>
									</div>
							   </main>	
							</div>
										   
						</div>
					</div>
	
					
				</div>
				</html:form>

				<div id="paso3">
			      <main role="main">
						<div class="remodal-bg">
							<section class="container">
								<div class="row">
									<div class="col-12">
							              <section class="o-box o-box--quotation u-mt20 u-mb40">
											<div id="grupo_valorizacion_iframe">
												<logic:notEmpty name="planesCotizados">
													<%@ include file="./includes/cotizador-valorizacion.jsp"%>
												</logic:notEmpty>
											</div>
										</section>
									</div>											
								</div>
						   </section>
						</div>	
				</main>
				</div>
			</div>
        </section>
      </div>
    </main>
	<iframe src="/cotizador/cotizacion/blank.jsp" width="100%" height="200"
		name="iframe_tabla" style="display: none;"> </iframe>
	<!-- FIN CONTENIDOS -->
	<%@ include file="./includes/cotizador-footer.jsp"%>
</body>

</html>