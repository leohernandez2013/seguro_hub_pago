<!DOCTYPE html>
<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="cl.tinet.common.struts.form.ValidableActionForm"%>
<%@page import="org.apache.struts.Globals"%>
<%@page import="cl.cencosud.acv.common.Producto"%>
<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>
<%@page import="cl.tinet.common.seguridad.servlet.AutenticacionServlet"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<html lang="es">
<head>




<%@ include file="./includes/cotizador-head.jsp"%>




<style type="text/css">
.ui-datepicker {
	color: #000000 width :       216px;
	height: auto;
	margin: 5px auto 0;
	font: 9pt Arial, sans-serif;
	-webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
	-moz-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
	box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
}

.ui-datepicker-month {
	color: #000000
}

.ui-datepicker-year {
	color: #000000
}
</style>


<script type="text/javascript">

$(document).ready(function(){

 	$("#fechaCompleta").datepicker({
		changeMonth: true,
		changeYear: true,
		showOn: "both",
		yearRange: '1936:1998',
		buttonImage: "images/calendar.gif",
		buttonImageOnly: true,
		dateFormat: 'dd-mm-yy',
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
		'Junio', 'Julio', 'Agosto', 'Septiembre',
		'Octubre', 'Noviembre', 'Diciembre'],
		monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
		'May', 'Jun', 'Jul', 'Ago',
		'Sep', 'Oct', 'Nov', 'Dic']
	}); 
});
</script>




<script type="text/javascript">	

		<!-- Identifica cliente conectado -->
		<logic:present name="cl.tinet.common.seguridad.USUARIO_CONECTADO" >
			<bean:define id="usuario" name="<%=SeguridadUtil.USUARIO_CONECTADO_KEY%>" scope="session" type="UsuarioExterno" />
		</logic:present>

		$(document).ready(function(){
			$('.datos-nombre-input').attr('placeholder','Nombre');
			$('.datos-paterno-input').attr('placeholder','Apellido Paterno');
			$('.datos-materno-input').attr('placeholder','Apellido Materno');
			$('.datos-rut-input').attr('placeholder','ej: 12345678 sin guion');
			$('.datos-email-input').attr('placeholder','ej: email@email.com');
			$('.datos-telefono-input').attr('placeholder','tel�fono');
		});

		function obtenerDescuento() {
			personalInfo();
			document.getElementById('cotizarProducto').action="/cotizador/preferente-vida.do";
			document.getElementById('cotizarProducto').method="post";
			document.getElementById('cotizarProducto').submit();
		}

		// Envio de formulario Vida
		function submitVida(){
			validateInput("paso1");
			if (validacionInput != 0) {
				return false;
			}
			bar(50,1);
			$('html, body').animate({scrollTop: 0}, 0);
			$('#waitModal').modal({backdrop: 'static', keyboard: false});
			personalInfo();
			$('#grupo_valorizacion_iframe').hide();
			var caracter="1234567890";
			caracter+="QWERTYUIOPASDFGHJKLZXCVBNM";
			caracter+="qwertyuioplkjhgfdsazxcvbnm";
			var numero_caracteres=10;

			var total=caracter.length;
			var clave="";
			for(a=0;a<numero_caracteres;a++){
				clave+=caracter.charAt(parseInt(total*Math.random(1)));
			}
			document.getElementById('claveVitrineo').value = clave;	

			document.getElementById('cotizarProducto').action="/cotizador/cotizar-producto-vida.do";
			document.getElementById('cotizarProducto').method="post";
			document.getElementById('cotizarProducto').submit();
		}	
		</script>
		
<script type="text/javascript">
$(document).ready(function(){
$('.datos-rut-input').blur(function(){
	var rut = $('.datos-rut-input').val(); 
	var cadena =rut.substr(0,1);
	if (cadena >=4 && cadena <= 9){
		if (rut != null){
			if (rut.length < 9){
				var cuerpo = rut.slice(0,-1);
				var dv = rut.slice(-1).toUpperCase(); 
				var completo = cuerpo + '-' + dv;
				
				$('.datos-rut-input').val("");
				$('.datos-rut-input').val(completo);
			}
		}
	}else if(cadena >= 1 && cadena <=3)
	if (rut != null){
		if (rut.length < 10){
		var cuerpo = rut.slice(0,-1);
		var dv = rut.slice(-1).toUpperCase(); 
		var completo = cuerpo + '-' + dv;
		
		$('.datos-rut-input').val("");
		$('.datos-rut-input').val(completo);
		}
	
	
	}else{
		$('.datos-rut-input').attr('placeholder','12345678-9');
	}
   });
});
</script>
</head>
<!--  fin head -->

<body onload="desplegarErrores();">
	<%@ include file="/google-analytics/google-analytics.jsp"%>
	<%@ include file="./includes/cotizador-header.jsp"%>

	<div class="container contenedor-sitio">
		<%@ include file="./includes/cotizador-breadcrumb.jsp"%>
		<!-- INICIO  CONTENIDOS -->
		<div class="row">
			<%@ include file="./includes/cotizador-resumen.jsp"%>
			<html:form styleId="cotizarProducto"
				action="cotizar-producto-vida.do" method="post"
				target="iframe_tabla">
				<%
					String idPlan = (String) request.getAttribute("idPlan");
						idPlan = (idPlan != null ? idPlan : "-1");
				%>
				<html:hidden property="datos(claveVitrineo)" value="1"
					styleId="claveVitrineo" />
				<html:hidden property="datos(rut)" value="" styleId="datos.rut" />
				<html:hidden property="datos(dv)" value="" styleId="datos.dv" />
				<html:hidden property="datos(idPlan)" value="<%=idPlan %>"
					styleId="datos.idPlan" />
				<html:hidden property="datos(diaFechaNacimiento)" value=""
					styleId="diasFecha" />
				<html:hidden property="datos(mesFechaNacimiento)" value=""
					styleId="mesFecha" />
				<html:hidden property="datos(anyoFechaNacimiento)" value=""
					styleId="anyosFecha" />
				<html:hidden property="datos(claveVitrineo)" value="1"
					styleId="claveVitrineo" />
				<html:hidden property="datos(hiddCaptcha)" value="1"
					styleId="hiddCaptcha" />
					
	
				
				<input type="hidden" name="idPlanValorizacion"
					id="idPlanValorizacion" value="" />

				<%
					String glosaDescuento = (String) request.getSession()
								.getAttribute("glosaDescuento");
						if (glosaDescuento != null) {
				%>
				<%
					}
				%>
				<!--  -->
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
					<%@ include file="./includes/cotizador-percent-bar.jsp"%>
					<div id="contenido-desplegado-cotizador">
					<div id="globalExceptionDivPlanes" class="">
						</div>
						<div id="paso1">

							<div class="col-lg-12">
								<h4>
									DATOS DEL CONTRATANTE <a href="#" 
										data-original-title="Datos de la persona que contrata el seguro y paga por �l."
										class="tooltip1  tooltip2 hidden-xs"> <img src="img/tooltip.jpg"></img>
									</a>
								</h4>
							</div>
							<!--  fin col-lg-12 -->

							<div class="col-lg-12">
								<div class="row">

									<!-- NOMBRE -->
									<div class="col-sm-4">
										<div class="form-group">
											<label for="nombre" class="hidden-xs">Nombre*</label>
											<html:text property="datos(nombre)"
												styleClass="form-control datos-nombre-input" size="16"
												styleId="datos.nombre" maxlength="25"
												onkeypress="return validar(event)" />
										</div>
										<!-- fin form-group -->
									</div>
									<!-- fin col-sm-4 -->

									<!-- AP PATERNO -->
									<div class="col-sm-4">
										<div class="form-group">
											<label for="nombre" class="hidden-xs">Apellido
												Paterno*</label>
											<html:text property="datos(apellidoPaterno)"
												styleClass="form-control datos-paterno-input" size="16"
												styleId="datos.apellidoPaterno" maxlength="25"
												onkeypress="return validar(event)" />
										</div>
										<!-- fin div form-group -->
									</div>
									<!-- fin col-sm-4 -->

									<!-- AP MATERNO -->
									<div class="col-sm-4">
										<div class='form-group'>
											<label for="nombre" class="hidden-xs">Apellido
												Materno*</label>
											<html:text property="datos(apellidoMaterno)"
												styleClass="form-control datos-materno-input" size="16"
												styleId="datos.apellidoMaterno" maxlength="25"
												onkeypress="return validar(event)" />
										</div>
										<!-- fin form-group -->
									</div>
									<!-- col-sm-4 -->
								</div>
								<!-- fin div ROW -->



								<div class="row">

									<!--  RUT -->
									<div class='col-sm-4'>
										<div class='form-group'>
											<label for="rut" class="hidden-xs">Rut* ej: 12345678 sin guion</label>
											<html:text styleClass="form-control datos-rut-input"
															property="datos(rutDuenyoCompleto)" maxlength="10"
															styleId="datos.rutDuenyoCompleto" onblur="obtenerDescuento();"
															/>
										</div>
										<!-- fin form-group -->
									</div>
									<!-- fin col-sm-4 -->

									<!-- FECHA NACIMIENTO -->
									<div class='col-sm-4'>
										<div class='form-group'>
											<label for="nombre" class="hidden-xs">Fecha de
												Nacimiento* </label>
												<input class="form-control input-min-width-95p datos-fecha-input"
													placeholder="30-01-1980" 
													id="fechaCompletaDuenyo"
													onBlur="autocompletarfecha(this);"
													onFocus="autocompletarfecha(this);"
													onkeypress="autocompletarfechaguion(this);"													
													maxlength="10"/>
										</div>
										<!-- fin form-group -->
									</div>
									<!-- fin col-sm-4 -->


									<!-- ESTADO CIVIL -->
									<div class='col-sm-4'>
										<div class='form-group'>
											<label for="estadoCivil" class="hidden-xs">Estado
												Civil*</label>
											<html:select property="datos(estadoCivil)"
												styleClass="form-control"
												styleId="datos.estadoCivil">
												<html:option value="">Seleccione estado civil</html:option>
												<html:options collection="estadosCiviles" property="id"
													labelProperty="descripcion" />
											</html:select>
										</div>
										<!-- fin form-group -->
									</div>
									<!-- col-sm-4 -->
								</div>
								<!-- fin div row -->

								<div class="row">
									<!-- EMAIL -->
									<div class='col-sm-4'>
										<div class='form-group'>
											<label for="nombre" class="hidden-xs">Email*</label>
											<html:text property="datos(email)"
												styleClass="form-control datos-email-input"
												styleId="datos.email" maxlength="50"
												onkeypress="return isCaracterEmail(event);"></html:text>
										</div>
									</div>

									<!-- TELEFONO -->
									<div class='col-sm-4'>
										<div class="form-group form-group-bottom">
											<label
												class="col-md-12 col-xs-12 control-label pleft hidden-xs">Tel�fono*</label>
											<div class="col-md-5 col-xs-5 pleft">
												<html:select property="datos(tipoTelefono)"
													styleClass="form-control" styleId="datos.telefono">
													<html:option value="">Seleccione</html:option>
													<html:optionsCollection name="tipoTelefono"
														label="descripcion" value="valor" />
												</html:select>
											</div>
											<div class="col-md-3 col-xs-3 pleft">
												<html:select property="datos(codigoTelefono)"
													styleClass="form-control" styleId="datos.codigoTelefono">
													<html:optionsCollection name="codigoArea"
														label="descripcion" value="valor" />
												</html:select>
											</div>
											<div class="col-md-4 col-xs-4 p0">
												<html:text property="datos(numeroTelefono)"
													styleClass="form-control datos-telefono-input"
													maxlength="8" styleId="datos.telefono3"
													onkeypress="return isNumberKey(event)"></html:text>
											</div>
										</div>
										<!-- fin form-group -->
									</div>
									<!-- col-sm-4 -->

									<div class='col-sm-4'>
										<div class='form-group'>
											<label for="user_firstname" class="hidden-xs">Sexo</label> 
											<html:select styleClass="form-control"
												property="datos(sexo)" styleId="datos.sexo">
												<html:option value="">Seleccione Sexo</html:option>
												<html:option value="F">Femenino</html:option>
												<html:option value="M">Masculino</html:option>
											</html:select>
										</div>
										<!-- fin form-group -->
										<span class="pull-left small">Todos los campos con *
											son obligatorios</span>
									</div>
									<!-- fin col-sm-4 -->
								</div>
								<!-- fin row -->


								<div class="row">
									<logic:notEqual name="vaCaptcha" value="0">
										<%@ include file="./includes/cotizador-captcha.jsp"%>
									</logic:notEqual>
								</div>
								<div class="row">
									<div class="col-lg-12 col-xs-12">
										<div class="alert alert-warning" id="incomplete-paso1"
											style="display: none">
											<p align="center" id="incomplete-text-paso1"></p>
										</div>
									</div>
								</div>
							</div>
							<!-- fin col-lg-12 -->

							<div class="col-md-12 col-xs-12">
								<div class="form-group">
									<%
										Producto[] productos = (Producto[]) request
													.getAttribute("productos");
											String cantProductos = String.valueOf(productos.length);
									%>
									<bean:define id="cantProd" value="<%=cantProductos%>"
										scope="page" />
									<logic:empty name="planesCotizados">
										<logic:greaterThan value="1" name="cantProd" scope="page">
											<label>Elige tu deducible:</label>
											<html:select property="datos(producto)"
												styleClass="form-control" styleId="producto">
												<option value="">seleccione</option>
												<html:options collection="productos" property="idProducto"
													labelProperty="nombreProducto" />
											</html:select>
										</logic:greaterThan>
									</logic:empty>
									<logic:lessEqual value="1" name="cantProd" scope="page">

										<div class="col-md-12 col-xs-12">
											<!--label>Tu Seguro:</label-->
											<logic:iterate id="producto" name="productos">
												<%
													String idProducto = ((Producto) producto)
																		.getIdProducto();
												%>
												<html:hidden property="datos(producto)" styleId="producto"
													value="<%=idProducto%>" />
												<!--label><bean:write name="producto" property="nombreProducto" /></label-->
											</logic:iterate>
										</div>

									</logic:lessEqual>
								</div>
								<!--  fin form-group -->
							</div>
							<!-- fin col-xx-00 -->

							<div class="row">
								<div class='col-md-12 col-sm-4'>
									<div class="alert alert-warning" id="incomplete-paso1"
										style="display: none">
										<p align="center" id="incomplete-text-paso1"></p>
									</div>
								</div>
							</div>
							<div class="row top15">

								<div class="hidden-xs">
									<div class="col-md-3">
										<a	href="/vseg-paris/desplegar-ficha.do?idRama=<bean:write name='idRama'/>&idSubcategoria=<bean:write name='idSubcategoria'/>" type="button" class="btn btn-primary">Volver</a>
									</div>
								</div>

								<div class="col-md-3 col-md-offset-6">
									<button type="button"
										class="btn btn-primary btn-block pull-right"
										onclick="submitVida();">COTIZAR</button>
								</div>

								<div class="hidden-lg">
									<br></br>
								</div>

								<div class="col-md-3 hidden-lg">
									<br> <br>
										<a	href="/vseg-paris/desplegar-ficha.do?idRama=<bean:write name='idRama'/>&idSubcategoria=<bean:write name='idSubcategoria'/>" type="button" class="btn btn-primary center-block">Volver</a>								
								</div>

							</div>
							<!-- fin row -->
						</div>
						<!-- fin paso1 -->
					</div>
					<!-- fin contenido-desplegado-cotizador -->
				</div>
				<!-- div col-xx-x -->




			</html:form>




			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">

				<div id="paso3">
					<div id="grupo_valorizacion_iframe">
						<logic:notEmpty name="planesCotizados">
							<%@ include file="./includes/cotizador-valorizacion.jsp"%>
						</logic:notEmpty>
					</div>
					<!-- grupo_valorizacion_iframe -->
				</div>
				<!-- fin paso 3 -->
				<!-- globalExceptionDiv -->

				<!-- Modal -->
				<div class="modal fade" id="waitModal" role="dialog">
					<div class="modal-dialog">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-body">
								<p align="center">Por favor espere...</p>
							</div>
						</div>
					</div>
				</div>
				<!-- fin de modal -->

			</div>
			<!-- fin de col-xx-00 -->
			<!-- fin de row inicial -->
		</div>
		<!-- fin container contenedor-sitio -->
	</div>
	<!-- div row -->

	<iframe src="/cotizador/cotizacion/blank.jsp" width="100%" height="200"
		name="iframe_tabla" style="display: none;"> </iframe>
	<!-- FIN CONTENIDOS -->
	<%@ include file="./includes/cotizador-footer.jsp"%>

</body>
</html>
<!-- fin de html -->