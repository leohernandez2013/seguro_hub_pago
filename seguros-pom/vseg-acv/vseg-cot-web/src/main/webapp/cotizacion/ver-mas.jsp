<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page import="cl.tinet.common.struts.form.ValidableActionForm"%>

<bean:define id="contextpath" value="<%=request.getContextPath()%>" />
<bean:define id="vsegparispath" value="/vseg-paris" />

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title></title>
		<link href="css/formulario-ver-mas.css" rel="stylesheet"
			type="text/css" />

		<script src="<bean:write name="contextpath" />/js/cotizacion.js"></script>
		<script
			src="<bean:write name="contextpath" />/js/jquery/jquery-1.4.2.js"></script>
		<script
			src="<bean:write name="contextpath" />/js/jquery/jquery.scrollTo.js"></script>
		<script src="<bean:write name="contextpath" />/js/jquery/jqModal.js"
			type="text/javascript"></script>

		<script type="text/javascript">
			//$(document).ready(function(){
			//	parent.$.colorbox.resize({
			//		width: $(document).width() 
			//		,height:  $(document).height() + 100 
			//		});
			//});
		</script>

        <!--[if lt IE 7]>
        		<script type="text/javascript" src="/vseg-paris/js/unitpngfix.js"></script>
	    <![endif]-->
	</head>

	<body>
		<div class="contver-mas">
			<div class="tus-datos-curva_top">
				<img src="images/img_light_box/fondo2/btn-cerrar.gif" width="39"
					height="39" onclick="parent.$.fn.colorbox.close();"
					style="cursor: pointer;" />
			</div>
			<div id="contenido_tus_datos_central">


				<div class="cont-text">
					<div id="tus_datos_titulos">
						<h1>
							<span class="colortitulo1">Datos para tu</span>
							<span>Cotizaci&oacute;n</span>
						</h1>
					</div>
					<div class="contdatostext">
						<div class="cont-fila1">
							<div class="contdatos1">
								<logic:notEmpty name="datosCotizacion" property="rutCliente"
									scope="session">
									<div class="titulos">
										Rut:
									</div>
									<div class="contextos">
										<bean:write name="datosCotizacion" property="rutCliente"
											scope="session" />
										-
										<bean:write name="datosCotizacion" property="dv"
											scope="session" />
									</div>
								</logic:notEmpty>
							</div>
							<div class="contdatos1">
								<logic:notEmpty name="datosCotizacion" property="nombre"
									scope="session">
									<div class="titulos">
										Nombre
									</div>
									<div class="contextos">
										<span><bean:write name="datosCotizacion"
												property="nombre" scope="session" /> </span>
									</div>
								</logic:notEmpty>
							</div>
							<div class="contdatos1">
								<logic:notEmpty name="datosCotizacion"
									property="apellidoPaterno" scope="session">
									<div class="titulos">
										Apellidos:
									</div>
									<div class="contextos">
										<bean:write name="datosCotizacion" property="apellidoPaterno"
											scope="session" />
										<bean:write name="datosCotizacion" property="apellidoMaterno"
											scope="session" />
									</div>
								</logic:notEmpty>
							</div>
							<div class="contdatos1">
								<logic:notEmpty name="datosCotizacion"
									property="numeroTelefono1" scope="session">
									<div class="titulos">
										Tel&eacute;fono:
									</div>
									<div class="contextos">
										<bean:write name="datosCotizacion" property="numeroTelefono1"
											scope="session" />
									</div>
								</logic:notEmpty>
							</div>
							<div class="contdatos1">
								<logic:notEmpty name="datosCotizacion" property="email"
									scope="session">
									<div class="titulos">
										E-mail :
									</div>
									<div class="contextos">
										<bean:write name="datosCotizacion" property="email"
											scope="session" />
									</div>
								</logic:notEmpty>
							</div>
							<div class="contdatos1">
								<logic:notEmpty name="datosCotizacion"
									property="fechaNacimiento" scope="session">
									<div class="titulos">
										Fecha de Nacimiento :
									</div>
									<div class="contextos">
										<bean:write name="datosCotizacion" property="fechaNacimiento"
											scope="session" format="dd/MM/yyyy" />
									</div>
								</logic:notEmpty>
							</div>
							<div class="contdatos1">
								<logic:notEmpty name="descEstadoCivil" scope="request">
									<div class="titulos">
										Estado Civil:
									</div>
									<div class="contextos">
										<bean:write name="descEstadoCivil" scope="request" />
									</div>
								</logic:notEmpty>
							</div>
						</div>
						<div class="cont-fila1">
							<div class="contdatos1">
								<logic:notEmpty name="descActividad" scope="request">
									<div class="titulos">
										Actividad
										<span></span>:
									</div>
									<div class="contextos">
										<bean:write name="descActividad" scope="request" />
									</div>
								</logic:notEmpty>
							</div>
							<div class="contdatos1">
								<logic:notEmpty name="descSexo" scope="request">
									<div class="titulos">
										Sexo :
									</div>
									<div class="contextos">
										<bean:write name="descSexo" scope="request" />
									</div>
								</logic:notEmpty>
							</div>
							<div class="contdatos1">
								<logic:notEmpty name="descRegion" scope="request">
									<div class="titulos">
										Region :
									</div>
									<div class="contextos">
										<bean:write name="descRegion" scope="request" />
									</div>
								</logic:notEmpty>
							</div>
							<div class="contdatos1">
								<logic:notEmpty name="descCiudad" scope="request">
									<div class="titulos">
										Ciudad :
									</div>
									<div class="contextos">
										<span><bean:write name="descCiudad" scope="request" />
										</span>
									</div>
								</logic:notEmpty>
							</div>
							<div class="contdatos1">
								<logic:notEmpty name="descComuna" scope="request">
									<div class="titulos">
										Comuna :
									</div>
									<div class="contextos">
										<bean:write name="descComuna" scope="request" />
									</div>
								</logic:notEmpty>
							</div>
							<div class="contdatos1">
								<logic:notEmpty name="datosCotizacion" property="direccion"
									scope="session">
									<div class="titulos">
										Direcci&oacute;n:
									</div>
									<div class="contextos">
										<bean:write name="datosCotizacion" property="direccion"
											scope="session" />
									</div>
								</logic:notEmpty>
							</div>
							<div class="contdatos1">
								<logic:notEmpty name="datosCotizacion"
									property="antiguedadVivienda" scope="session">
									<div class="titulos">
										AntigŁedad :
									</div>
									<div class="contextos">
										<bean:write name="datosCotizacion"
											property="antiguedadVivienda" scope="session" />
										a&ntilde;os
									</div>
								</logic:notEmpty>
							</div>
						</div>
					</div>

					<div id="tus_datos_titulos">
						<h1>
							<span class="colortitulo1">Tu</span>
							<span>Plan</span>
						</h1>
					</div>
					<div class="contdatostext">
						<div class="contplanymonto">
							<div class="slide-derecha"></div>
							<div class="cuerpo-central-plan">
								<div class="cont-plan">
									<bean:write name="datosPlan" property="nombrePlan"
										scope="session" />
								</div>
								<div class="cont-monto-peso">
									<div class="peso">
										$
										<bean:write name="datosPlan" property="primaMensualPesos"
											scope="session" />
									</div>
									<div class="con-monto-mes">
										<div class="monto">
											Monto
										</div>
										<div class="mensual">
											Mensual
										</div>
									</div>
								</div>
								<div class="cont-monto-uf">
									<div class="monto-uf">
										UF
										<bean:write name="datosPlan" property="primaMensualUF"
											scope="session" />
									</div>
									<div class="cont-mont-mensual-uf">
										<div class="monto-uf-text">
											Monto
										</div>
										<div class="mensual">
											Mensual
										</div>
									</div>
								</div>
								<div class="pormes"></div>
							</div>
							<div class="slide-izquierda"></div>
						</div>
					</div>
					<!-- 
					<div id="tus_datos_titulos">
						<h1>
							<span class="colortitulo1">Datos para tu</span>
							<span>P&oacute;liza</span>
						</h1>
					</div>
					<div class="contdatostext">
						<div class="cont-fila1">
							<div class="contdatos1">
								<logic:notEmpty name="datosCotizacion"
									property="numeroTelefono2" scope="session">
									<div class="titulos">
										Tel&eacute;fono 2 :
									</div>
									<div class="contextos">
										<bean:write name="datosCotizacion" property="numeroTelefono2"
											scope="session" />
									</div>
								</logic:notEmpty>
							</div>
							<div class="contdatos1">
								<logic:notEmpty name="datosCotizacion"
									property="apellidoPaterno" scope="session">
									<div class="titulos">
										Apellido Pateno :
									</div>
									<div class="contextos">
										<bean:write name="datosCotizacion" property="apellidoPaterno"
											scope="session" />
									</div>
								</logic:notEmpty>
							</div>
							<div class="contdatos1">
								<logic:notEmpty name="datosCotizacion"
									property="apellidoMaterno" scope="session">
									<div class="titulos">
										Apellido Mateno :
									</div>
									<div class="contextos">
										<bean:write name="datosCotizacion" property="apellidoMaterno"
											scope="session" />
									</div>
								</logic:notEmpty>
							</div>
							<div class="contdatos1">
								<logic:notEmpty name="descRegion" scope="request">
									<div class="titulos">
										Regi&oacute;n:
									</div>
									<div class="contextos">
										<bean:write name="descRegion" scope="request" />
									</div>
								</logic:notEmpty>
							</div>
						</div>
						<div class="cont-fila1">
							<div class="contdatos1">
								<logic:notEmpty name="descCiudad" scope="request">
									<div class="titulos">
										Ciudad:
									</div>
									<div class="contextos">
										<bean:write name="descCiudad" scope="request" />
									</div>
								</logic:notEmpty>
							</div>
							<div class="contdatos1">
								<logic:notEmpty name="descComuna" scope="request">
									<div class="titulos">
										Comuna :
									</div>
									<div class="contextos">
										<bean:write name="descComuna" scope="request" />
									</div>
								</logic:notEmpty>
							</div>
							<div class="contdatos1">
								<logic:notEmpty name="datosCotizacion" property="direccion"
									scope="session">
									<div class="titulos">
										Direcci&oacute;n:
									</div>
									<div class="contextos">
										<bean:write name="datosCotizacion" property="direccion"
											scope="session" />
										<bean:write name="datosCotizacion" property="numeroDireccion"
											scope="session" />
										Dpto./Casa
										<bean:write name="datosCotizacion" property="numeroDepto"
											scope="session" />
									</div>
								</logic:notEmpty>
							</div>
						</div>
					</div>
					-->

					<logic:present name="datosCotizacion" property="beneficiarios">
						<div id="tus_datos_titulos">
							<h1>
								<span class="colortitulo1">Datos </span>
								<span>Beneficiarios</span>
							</h1>
						</div>
						<div class="cont-datos">
							<div id="adicional_despliegue">
								<div id="adicional_despliegue_menu">
									<div class="adicional_menu_nombre">
										<p>
											nombre
											<br />
											beneficiario
										</p>
									</div>
									<div class="adicional_menu_nombre-mas">
										<p>
											apellido paterno
										</p>
									</div>
									<div class="adicional_menu_nombre-mas">
										<p>
											apellido materno
										</p>
									</div>
									<div class="adicional_menu_fecha">
										<p>
											fecha nacimiento
										</p>
									</div>
									<div class="adicional_menu_parentesco">
										<p>
											parentesco
										</p>
									</div>
									<div class="adicional_menu_sexo">
										<p>
											sexo
										</p>
									</div>
									<div class="adicional_menu_distribucion">
										<p>
											%
											<br />
											distribuci&oacute;n
										</p>
									</div>
								</div>
							</div>
						</div>
						<!-- -->
						<!--azul-->
						<!-- -->
						<!-- -->
						<!--gris-->
					</logic:present>

					<logic:present name="datosCotizacion" property="beneficiarios">
						<logic:iterate id="beneficiario" name="datosCotizacion"
							property="beneficiarios">
							<div class="adicional_marcar_datos-inactivo">
								<div class="adicional_marcar_datos-n">
									<bean:write name="beneficiario" property="nombre" />
								</div>
								<div class="adicional_marcar_datos-n">
									<bean:write name="beneficiario" property="apellidoPaterno" />
								</div>
								<div class="adicional_marcar_datos-n">
									<bean:write name="beneficiario" property="apellidoMaterno" />
								</div>
								<div class="adicional_marcar_datos-f">
									<bean:write name="beneficiario" property="fechaNacimiento"
										format="dd/MM/yyyy" />
								</div>
								<div class="adicional_marcar_datos-p">
									<bean:write name="beneficiario" property="descParentesco" />
								</div>
								<div class="adicional_marcar_datos-s">
									<bean:write name="beneficiario" property="sexo" />
								</div>
								<div class="adicional_marcar_datos-d">
									<bean:write name="beneficiario" property="distribucion"
										format="######.##" />
									%
								</div>
							</div>
						</logic:iterate>
					</logic:present>


					<logic:present name="datosCotizacion" property="adicionales">
						<div id="tus_datos_titulos">
							<h1>
								<span class="colortitulo1">Datos </span>
								<span>Adicionales</span>
							</h1>
						</div>
						<div class="cont-datos">
							<div id="adicional_despliegue">
								<div id="adicional_despliegue_menu">
									<div class="adicional_menu_nombre">
										<p>
											nombre
											<br />
											adicional
										</p>
									</div>
									<div class="adicional_menu_nombre-mas">
										<p>
											apellido paterno
										</p>
									</div>
									<div class="adicional_menu_nombre-mas">
										<p>
											apellido materno
										</p>
									</div>
									<div class="adicional_menu_fecha">
										<p>
											fecha nacimiento
										</p>
									</div>
									<div class="adicional_menu_parentesco">
										<p>
											parentesco
										</p>
									</div>
									<div class="adicional_menu_sexo">
										<p>
											sexo
										</p>
									</div>
								</div>
							</div>
						</div>
						<!-- -->
						<!--azul-->
						<!-- -->
						<!-- -->
						<!--gris-->
					</logic:present>

					<logic:present name="datosCotizacion" property="adicionales">
						<logic:iterate id="adicional" name="datosCotizacion"
							property="adicionales">
							<div class="adicional_marcar_datos-inactivo">
								<div class="adicional_marcar_datos-n">
									<bean:write name="adicional" property="nombre" />
								</div>
								<div class="adicional_marcar_datos-n">
									<bean:write name="adicional" property="apellidoPaterno" />
								</div>
								<div class="adicional_marcar_datos-n">
									<bean:write name="adicional" property="apellidoMaterno" />
								</div>
								<div class="adicional_marcar_datos-f">
									<bean:write name="adicional" property="fechaNacimiento"
										format="dd/MM/yyyy" />
								</div>
								<div class="adicional_marcar_datos-p">
									<bean:write name="adicional" property="descParentesco" />
								</div>
								<div class="adicional_marcar_datos-s">
									<bean:write name="adicional" property="sexo" />
								</div>
							</div>
						</logic:iterate>
					</logic:present>




				</div>

			</div>
			<div class="tus-datos-curva_botton"></div>
		</div>

	</body>
</html>
