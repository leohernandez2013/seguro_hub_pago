<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="org.apache.struts.Globals"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html:html lang="true">
<head>
<%@ include file="./includes/cotizador-head.jsp"%>
</head>
<body>
	<%@ include file="/google-analytics/google-analytics.jsp"%>
	<%@ include file="/cotizacion/includes/cotizador-header.jsp"%>
	<div class="container contenedor-sitio">
		<ol class="breadcrumb hidden-xs">
			<li><a href="/vseg-paris/index.jsp">Home</a></li>
			<li class="active">Error</li>
		</ol>
		<div class="row">
			<h4 class="titulo-cotizacion">
				<strong>ERROR</strong>
			</h4>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="caja-banner">
				<img src="img/banner-error2.jpg" width="1135" height="180"
					class="img-responsive">
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p class="centrar-texto">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
					eiusmod tempor incididunt ut labore et dolore magna aliqua.<br>
					<br> Lorem ipsum dolor sit amet, consectetur adipiscing elit,
					sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.<br>
					<br> Ante cualquier duda, ll�manos al<br> <a
						class="numero1" href="tel:600-500-5000"><img
						src="img/numero.png" alt="600 500 5000">600 500 5000</a>
				</p>
				<p class="centrar-texto">
					<button type="button" class="btn btn-primary">VOLVER A
						INTENTAR</button>
				</p>
			</div>
		</div>
	</div>
	<%@ include file="/cotizacion/includes/cotizador-footer.jsp"%>
</body>
</html:html>