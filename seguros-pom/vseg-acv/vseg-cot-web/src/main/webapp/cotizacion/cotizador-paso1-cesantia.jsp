<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="cl.tinet.common.struts.form.ValidableActionForm"%>
<%@page import="org.apache.struts.Globals"%>
<%@page import="cl.cencosud.acv.common.Producto"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ include file="./includes/cotizador-head.jsp"  %>
	
<body onload="desplegarErrores()">

<!-- MENUcotizador -derecho -->

<div id="menu-cotizador">
<div id="banner-cotizador">
	<img src="images/banner/bannercotizador-cesantia-2.jpg" alt="" width="253" height="153" border="0" />
</div>
<div class="curva_menu_cotixador"><img src="images/curva_menu_cotixador.jpg" alt="" width="255" height="18" border="0" /></div>

	<%@ include file="./includes/menu-izquierdo-cotizacion.jsp" %>
	
<div class="imgboton"></div>
</div>

<!-- MENU cotizador -derecho FIN-->

<!-- COTIZADOR CONTENIDO --> 
<div id="contenido-cotizador"> 
<div id="titulo-cotizador"><h1 style="margin-left: 0px;"><span>Contrataci�n Online / </span>Seguro de Cesant�a</h1><p><logic:present name="subcategoriaDesc"><bean:write name="subcategoriaDesc"/></logic:present></p></div> 
<div id="menu-tres-pasos"> 
<img src="images/btn-img/btn-cotizador-cotiza.tuseguro-act.jpg" alt="" width="184" height="46" border="0" /><img src="images/btn-img/btn-completa-datos.jpg" alt="" width="185" height="46" border="0" /><img src="images/btn-img/btn-contrata-online.jpg" alt="" width="184" height="46" border="0" /><img src="images/linea-gris-seguro.jpg" alt="" width="147" height="40" border="0" />
<a href="/cotizador/desplegar-cotizacion.do?idRama=<bean:write name='idRama'/>&idSubcategoria=<bean:write name='idSubcategoria'/>&volverPaso1=true" style="display: none;"></a>
</div> 
<div id="curva-cotizador"><img src="images/curva-cotizador-2.jpg" alt="" width="708" height="19" border="0" /></div> 
<div id="contenido-desplegado-cotizador">



<logic:present name="cl.tinet.common.seguridad.USUARIO_CONECTADO">
<script type="text/javascript">
<bean:define id="usuario"
		name="<%=SeguridadUtil.USUARIO_CONECTADO_KEY %>" scope="session"
		type="UsuarioExterno" />
</script>
</logic:present>

<logic:notPresent name="usuario">
<!-- INICIO REGISTRO -->

						<!-- Caja de logueo -->

						

						<div id="p7PMM03contenedorMenu" style="*margin-left: 445px; *margin-top:-196px">

					    <div id="p7PMM_1" class="p7PMMh03 p7PMMnoscript" >

					       <ul class="p7PMM">					      	

					         <li><a href="#"> <span style="font-family: Arial, verdana; text-indent: 60px; *text-indent: 0px; padding-top: 12px; font-size: 15px; color: #56a9d5; font-weight: bold; padding-top: 4px;"> Mis seguros </span> <b style="color: #666;">Online</b> </a>							

					          <div>

					            <ul>						    

					              <li><a onclick="windows.open('/vseg-paris/html/porque-registrarse.html','newWindow');">		        

								<span class="fancy iframe" 

								style="cursor: pointer; float:left; text-indent: 81px; *margin-top:0px">

								<img width="9" height="10" src="css/p7pmm/img/linkk.png" alt="" href="/vseg-paris/html/porque-registrarse.html"/>�Por qu� registrarse?</span></a>

								</li>						

					            </ul>

					          </div>

					        </li>			        

						</ul>

					      <div class="p7pmmclearfloat">&nbsp;</div>

						    </div>

						</div>		



						

					<!-- Fin caja de logueo -->	

<div id="estas-registrado">
<div id="estas-registrado-texto-iz">
<p class="textcontrata">�Est�s Registrado?</p>
<h3>Ahorra tiempo cotizando con tu clave</h3>
</div>
<div id="ingresar-datos">
<script type="text/javascript">
	$(document).ready(function (){
		//Se envia formulario al presionar enter.
		$('#passUser').keypress(function(e){
			if(e.which == 13){
				e.preventDefault();
			    submitLogin();
				return false;
			}
		});
	});
	
	function submitLogin(){
		document.loginForm.username.value = document.getElementById("rutUser").value + "-" + document.getElementById("dvUser").value;
		document.loginForm.password.value = document.getElementById("passUser").value;
		document.loginForm.submit();
	}
</script>
<form action="/cotizador/Login" method="post" name="loginForm">

<input type="hidden" name="username" value=""/>
<input type="hidden" name="password" value=""/>
<input type="hidden" name="cl.tinet.common.seguridad.servlet.URI_EXITO" value="/desplegar-cotizacion.do?idRama=<bean:write name="idRama"/>&idSubcategoria=<bean:write name="idSubcategoria"/>"/>
<input type="hidden" name="cl.tinet.common.seguridad.servlet.URI_FRACASO" value="/desplegar-cotizacion.do?idRama=<bean:write name="idRama"/>&idSubcategoria=<bean:write name="idSubcategoria"/>"/>

 
												<div id="rut-clave">
							<div class="rut-clave_con">
								<p>
									rut <input name="rutUser" id="rutUser" type="text" class="ingrasa_rut_cl" size="8" maxlength="8" onkeypress="return isNumberKey(event)"/>
									-
									<input name="dvUser" id="dvUser" type="text" class="ingrasa_rut_cl2" size="1" maxlength="1" />
									<span>clave</span>
									<input type="password" class="ingrasa_rut_cl" name="passUser" id="passUser" maxlength="6" />
								</p>
							</div>
							<div class="rut-clave-ir">
								<a onfocus="blur();" href="#"
									onclick="javascrtip:submitLogin();"><img class="dsR3"
										src="images/btn-img/ir-in-btn.jpg"
										onmouseover="this.src = 'images/btn-img/ir-in-btn_hover.jpg'"
										border="0"
										onmouseout="this.src = 'images/btn-img/ir-in-btn.jpg'"
										border="0" alt="" border="0" />
								</a>
							</div>

						</div>

						<div id="olvidaste-tu-clave">
							<!-- -->
							<div class="olvidaste-tu-clave-flecha">
								<img src="images/btn-img/btn-flacha-clave.jpg" alt="" width="10"
									height="10" border="0" />
							</div>
							<div class="olvidaste-tu-clave-texto">
								<span class="fancy iframe"
									style="text-decoration: none; cursor: pointer;"
									id="registroUsuario">Obt&eacute;n tu Clave</span>
							</div>
							<!-- -->
							<!-- -->
							<div class="olvidaste-tu-clave-flecha">
								<img src="images/btn-img/btn-flacha-clave.jpg" alt="" width="10"
									height="10" border="0" />
							</div>
							<div class="olvidaste-tu-clave-texto">
								<a id="olvidasteClave"
									style="text-decoration: none; cursor: pointer;"
									class="fancy iframe">�Olvidaste tu Clave?</a>
							</div>
							<!-- -->
						</div>
<!-- --></div>





</form>
</div>
<script type="text/javascript">

			/*$("#olvidasteClave").click(function(){
			
			    var url = "/vseg-paris/recuperar-clave.do";

			//	var params = "?rut=" + $("#rutUser").val();

			//	params += "&dv=" + $("#dvUser").val();


				var params = "?rut=";

				params += "&dv=";

				
				$(this).colorbox({
				     iframe:true, 
				     width:"450px", 
				     height:"260px", 
				     href: url + params
			    });
				
			});*/
			$(document).ready(function() {	
			$("#olvidasteClave").fancybox({ 
		    'onStart'   : function() {
					var url = "/vseg-paris/recuperar-clave.do";
					var params = "?rut=";
						params += "&dv=";

					//var params = "?rut=" + $("#rutUser").val();
					//	params += "&dv=" + $("#dvUser").val();
		     $("#olvidasteClave").attr("href",url+params);
		     
		    },

		    'width'    : 430,

		    'height'   : 130,

		    'autoScale'   : false,
		    'transitionIn'  : 'none',
		    'transitionOut'  : 'none',
		    'type'    : 'iframe',
		    'scrolling'  : 'no' 
		  });
			});
		</script>
<!-- FIN REGISTRO -->

<%@ include file="./includes/cotizador-error-login.jsp" %>
	

<!-- INICIO USUARIO LOGEADO -->
</logic:notPresent>

<script type="text/javascript">
	function createDivTable(targetDiv, finalPositionDiv){
		var divToShow = frames.iframe_tabla.document.getElementById(targetDiv);
		var divOnIframe = divToShow.cloneNode(true);
		var myDiv = document.getElementById(finalPositionDiv);
		while (myDiv.childNodes[0]) {
				myDiv.removeChild(myDiv.childNodes[0]);
		}
		if(divOnIframe.outerHTML!=undefined) {
			myDiv.innerHTML = divOnIframe.outerHTML;
		} else {
			myDiv.innerHTML = frames.iframe_tabla.$("#"+targetDiv).parent().html();
		}
		$('#loading').hide();
		$('#' + finalPositionDiv).focus();
	}
</script>

<logic:present name="usuario">
<div id="estas-registrado2">
  <div class="persnalizado-paso1">
    <div class="nombre-log-paso1">�Necesitas modificar tus datos?</div>
  </div>
  <div class="actualizar-datos-paso1">&gt; Actualizalos <span id="actualizar" name="actualizar"
					style="text-decoration: underline; cursor: pointer;">aqu�</span></div>
</div>



<div id="p7PMM03contenedorMenu2">

	    <div id="p7PMM_1" class="p7PMMh03 p7PMMnoscript">
	      <ul class="p7PMM">
	      	<!-- Vehiculo -->
	         <li><a> <span style="font-family: Arial, verdana; text-indent: 60px; *text-indent: 0px; padding-top: 12px; font-size: 15px; color: #56a9d5; font-weight: bold; padding-top: 4px;"> Mis seguros </span> <b style="color: #666;">Online</b> </a>
			
	          <div>
					<ul>
						<li> 
							<a href="/vseg-paris/secure/inicio/inicio.do" style="padding: 2px 2px 2px 57px;">
							<img width="9" height="10" border="0" onmouseout="this.src = 'css/p7pmm/img/linkk.png'" onmouseover="this.src = 'css/p7pmm/img/linkk.png'" src="css/p7pmm/img/linkk.png"> Mis Seguros
							</a>
							<a  href="/vseg-paris/secure/inicio/cotizaciones.do" style="padding: 2px 2px 2px 57px;">
							<img width="9" height="10" border="0" onmouseout="this.src = 'css/p7pmm/img/linkk.png'" onmouseover="this.src = 'css/p7pmm/img/linkk.png'" src="css/p7pmm/img/linkk.png"> Mis Cotizaciones
							</a>
							<a href="/vseg-paris/secure/inicio/inicio.do"  style="margin-top: -37px; margin-left: 143px;" >
							
								<img width="62" height="18" border="0" onmouseout="this.src = 'images/ingresar.png'" onmouseover="this.src = 'images/ingresarOver.png'" src="images/ingresar.png">
							</a>
						</li>
					</ul>
	          </div>
	        </li>
		</ul>
	      <div class="p7pmmclearfloat">&nbsp;</div>
	    </div>
	</div>
</logic:present>
<!-- FIN USUARIO LOGEADO -->




<div id="datos-del-contratante"> 
<div id="datos-del-contratante-titulo"> 
<h1 style="margin-left:0px;">Datos del Contratante<img src="images/btn-img/icono-pre.jpg" alt="" width="19" height="22" border="0" class="tooltip_show"/></h1>
<div id="nube_pregunta" class="tooltip_contenedor" style="display: none;">
    <div id="curva_nube_pregunta_top"></div>
    <div id="nube_pregunta_texto"><p>
    Datos de la persona que contrata el seguro y paga por �l.<br /> 
	En el caso de los seguros de cesant�a el contratante debe ser siempre el asegurado, es decir no puedes comprar un seguro de cesant�a para nadie m�s que para ti mismo.<br /> 
	En ese caso el contratante es tambi�n la persona protegida por el seguro.
    </p></div>
    <div id="curva_nube_pregunta_botton"></div>
</div> 
<p><span>Importante :</span> El contratante debe ser el mismo que el asegurado  / <span>Todos los campos con * son obligatorios</span></p> 
</div> 
  
<!-- bloque casilla datos --> 
<div id="casilla-datos-cotizador"> 
 
<html:form action="cotizar-producto-cesantia.do" target="iframe_tabla" method="post" styleId="cotizarProductoForm">

<%
	String idPlan = (String)request.getAttribute("idPlan");
	idPlan = (idPlan != null ? idPlan : "-1");
 %>

	<html:hidden property="datos(idPlan)" value="<%=idPlan %>" styleId="datos(idPlan)"/>
	

	<!-- --> 
	<div id="bloque-casilla"> 
	<div id="bloque-dato-usuario"><p>rut *</p></div> 
	<div id="bloque-ingresar-dato-dos"><p>
	<logic:notPresent name="usuario">
		<html:text property="datos(rut)" styleClass="textbox-d-personal" maxlength="8" styleId="datos.rut" onkeypress="return isNumberKey(event)"/> - <html:text property="datos(dv)" styleClass="textbox-rut" maxlength="1" size="3" styleId="datos.dv"/>
		</logic:notPresent>
		
		 <logic:present name="usuario">
		<html:text property="datos(rut)" styleClass="textbox-d-personal" maxlength="8" styleId="datos.rut" readonly="true"/> - <html:text property="datos(dv)" styleClass="textbox-rut" maxlength="1" size="3" styleId="datos.dv" readonly="true"/>
		</logic:present> 
	<span>ej.- 12345678</span></p>
	</div>
	<div id="error.datos.rutdv">
		<html:errors property="datos.rutdv"/>
	</div>
	</div> 
	<!-- --> 
	 
	<!-- --> 
	<div id="bloque-casilla"> 
	<div id="bloque-dato-usuario"><p>nombre *</p></div> 
	<div id="bloque-ingresar-dato-dos" class="grupo_nombre"><p>
	
	<html:text property="datos(nombre)" styleClass="textbox-d-personal" size="16"  styleId="datos.nombre" maxlength="30" onkeypress="return validar(event)"/> 
	    <html:text property="datos(apellidoPaterno)" styleClass="textbox-d-personal" size="16" styleId="datos.apellidoPaterno" maxlength="30" onkeypress="return validar(event)"></html:text>
	     <html:text property="datos(apellidoMaterno)" styleClass="textbox-d-personal" size="16" styleId="datos.apellidoMaterno"  maxlength="30" onkeypress="return validar(event)"></html:text>
	    </p></div>
	    <div id="error.datos.nombres">
		<html:errors property="datos.nombres"/>
	</div>
	</div> 
	<!-- --> 
	 
	<!-- --> 
	<div id="bloque-casilla"> 
	<div id="bloque-dato-usuario"><p>tel�fono *</p></div> 
	<div id="bloque-ingresar-dato-dos"><p> 
	
	<html:select property="datos(tipoTelefono)" styleClass="textbox" styleId="datos.tipoTelefono">
		<html:option value="">Seleccione</html:option>
		<html:optionsCollection name="tipoTelefono" label="descripcion" value="valor"/>
	</html:select> 
	 
	<html:select property="datos(codigoTelefono)" styleClass="textbox" styleId="datos.codigoTelefono" >
		<option value="">Seleccione</option>
		<html:optionsCollection name="codigoArea" label="descripcion" value="valor"/>
	</html:select>
	<script type="text/javascript">
		$(document).ready(function(){
			<logic:present property="datos(codigoTelefono)" name="cotizarProducto">
					var val_codigo_cel = "<bean:write name='cotizarProducto' property='datos(codigoTelefono)'/>";
					$("select[name='datos(codigoTelefono)']").val(val_codigo_cel);
			</logic:present>
		});
	</script>
	
	<html:text property="datos(numeroTelefono)" styleClass="textbox-fono" maxlength="8" styleId="datos.numeroTelefono" onkeypress="return isNumberKey(event)"></html:text>
	
	</p></div>
	<div id = "error.datos.telefono">
	<html:errors property="datos.telefono"/>
	</div>
	</div> 
	<!-- --> 
	 
	<!-- --> 
	<div id="bloque-casilla"> 
	<div id="bloque-dato-usuario"><p>e-mail *</p></div> 
	<div id="bloque-ingresar-dato-dos"><p>
	 
	<html:text property="datos(email)" styleClass="textbox-d-personal" maxlength="50" styleId="datos.email" onkeypress="return isCaracterEmail(event);"></html:text>
	<span>ejemplo@ejemplo.com</span></p></div>
	<div id = "error.datos.email">
	<html:errors property="datos.email"/>
	</div> 
	</div> 
	<!-- --> 
	 
	</div> 
	<!-- bloque casilla datos FIN--> 
	 
	</div> 
	<!-- datos-del-contratante fin--> 
	 
	 
	 
	 
	 
	 
	<!-- datos-del-contratante--> 
	<div id="datos-del-contratante"> 
	<!-- bloque casilla datos --> 
	<div id="casilla-datos-cotizador"> 
	 
	 
	<!-- --> 
	<div id="bloque-casilla"> 
	<div id="bloque-dato-usuario"><p>fecha de nacimiento *</p></div> 
	<div id="bloque-ingresar-dato-dos"> 
	
	 
	<html:select property="datos(diaFechaNacimiento)" size="1" styleClass="textbox" styleId="diasFecha">
	<option value="" selected="selected">d�a</option>
	</html:select>
	
	<html:select property="datos(mesFechaNacimiento)" size="1" styleClass="textbox" styleId="mesFecha">
		<option value="" selected="selected">mes</option>
		<html:optionsCollection name="meses" label="descripcion" value="valor"/>
	</html:select>
	
	<html:select property="datos(anyoFechaNacimiento)" size="1" styleClass="textbox" styleId="anyosFecha">
		<option value="" selected="selected">a�o</option>
		<html:optionsCollection name="anyos" label="descripcion" value="valor"/>
	</html:select>
	
	<script type="text/javascript">
		
			$("#actualizar").fancybox ({
				  'onStart'   : function() {
			    				  
				var url = '/vseg-paris/secure/inicio/actualizar-datos-cliente.do';
								
			   $("#actualizar").attr("href",url);
				   
			   },
			    'width'    : 670,
			    'height'   : '80%',
			    'autoScale'   : false,
			    'transitionIn'  : 'none',
			    'transitionOut'  : 'none',
			    'type'    : 'iframe',
		    	'scrolling'  : 'no' 
			   });
		$(document).ready(function(){
			var dia_fec_nac = "";
			<logic:notEmpty name="cotizarProducto" property="datos(diaFechaNacimiento)">
				dia_fec_nac += <bean:write name="cotizarProducto" property="datos(diaFechaNacimiento)" />;
				$("#diasFecha").val(dia_fec_nac);
			</logic:notEmpty>		
		})
	</script>
	
	 
	</div>
	<div id = "error.datos.fechaNacimiento">
		<html:errors property="datos.fechaNacimiento"/>
	</div>
	</div> 
	
	<div id="bloque-casilla"> 
	<div id="bloque-dato-usuario"><p>estado civil *</p></div> 
	<div id="bloque-ingresar-dato-dos"> 
	 
	<html:select property="datos(estadoCivil)" styleClass="textbox" size="1" styleId="datos.estadoCivil">
		<html:option value="">Seleccione</html:option>
		<html:options collection="estadosCiviles" property="id" labelProperty="descripcion"/>
	</html:select>
	
	</div>
	<div id = "error.datos.estadoCivil">
	<html:errors property="datos.estadoCivil"/>
	</div>
	</div> 
	<!-- --> 
	 
	<!--
	<div id="bloque-casilla"> 
	<div id="bloque-dato-usuario"><p>actividad *</p></div> 
	<div id="bloque-ingresar-dato-dos"> 
	
	<html:select property="datos(actividad)" styleClass="textbox" size="1" styleId="datos.actividad">
		<html:option value="">Seleccione</html:option>
		<html:options collection="actividades" property="id" labelProperty="descripcion"/>
	</html:select>
	
	</div>
	<div id = "error.datos.actividad">
	<html:errors property="datos.actividad"/>
	</div>
	</div> 
	--> 
	 
	<!-- --> 
	<div id="bloque-casilla"> 
	<div id="bloque-dato-usuario"><p>sexo *</p></div> 
	<div id="bloque-ingresar-dato-sex"  style="float: left; width: 385px;"> 
	<div class="bloque-ingresar-dato-radiogroup"><html:radio property="datos(sexo)" value="M" styleClass="radiog">  Masculino</html:radio>
	<html:radio property="datos(sexo)" value="F" styleClass="radiog">  Femenino</html:radio></div>
	</div>
	<div id = "error.datos.sexo">
	<html:errors property="datos.sexo"/>
	</div>
	</div> 
	<!-- --> 
	 
	</div> 
	<!-- bloque casilla datos FIN--> 
	 
	</div> 
	 <logic:notEqual name="vaCaptcha" value="0">
	 <%@ include file="./includes/cotizador-captcha.jsp" %>
	 </logic:notEqual>
	 
	 
	 
	<!-- Elige tu seguro--> 
	<!-- datos-del-contratante--> 
	<div id="datos-del-contratante" style="background: url(images/fondo-cotizador2.jpg) bottom no-repeat; background-position: 4px 16px; "> 
		<div id="casilla-datos-cotizador" style="height: 35px;" class="casilla111">
		<div id="bloque-casilla" style="width: 669px;">
			<%
				Producto[] productos = (Producto[])request.getAttribute("productos"); 
				String cantProductos = String.valueOf(productos.length);
			%>
			<bean:define id="cantProd" value="<%=cantProductos%>" scope="page"/>
			
			<!-- Si hay mas de un plan para cotizar. -->
			<logic:greaterThan value="1" name="cantProd" scope="page">
			<div id="bloque-dato-usuario" style="width: 147px; margin-left: 5px;">
				<h1 style="font-size: 16px;">
					Elige tu seguro:
				</h1>
			</div>
			<div id="bloque-ingresar-dato-dos"
				style="width: 250px; padding-left: 70px;">
				<div id = "erroros.datos.producto" >
				<html:errors property="datos.producto"/>
				</div>
				<html:select property="datos(producto)" styleClass="textbox" size="1" styleId="producto">
					<option value="">selecciona</option>
					<html:options collection="productos" property="idProducto" labelProperty="nombreProducto"/>
				</html:select>
			</div>
			</logic:greaterThan>
			<!-- Fin mas de un plan -->
			
			<!-- Si solo hay 1 plan -->
			<logic:lessEqual value="1" name="cantProd" scope="page">
			<script type="text/javascript">
				<!-- Para alinear los titulos -->
				$(".casilla111").css("paddingLeft", "37px");
			</script>
			<div id="bloque-dato-usuario" style="width: 147px;">
				<h1 style="font-size:16px;">
					Tu seguro:
				</h1>
			</div>
			<div id="bloque-ingresar-dato-dos" style="width:250px; padding-left:70px;">
				<div id = "error.datos.producto">
				<html:errors property="datos.producto"/>
				</div>
					<logic:iterate id="producto" name="productos">
						<% String idProducto = ((Producto)producto).getIdProducto(); %>
						<html:hidden property="datos(producto)" styleId="producto" value="<%=idProducto%>"/>
						<h1 style="font-size:16px;">
							<bean:write name="producto" property="nombreProducto"/>
						</h1>
					</logic:iterate>
			</div>
			</logic:lessEqual>
			<!-- Fin 1 solo plan -->
			
			<div id="btn-img-cot" style="margin-top: -3px; margin-left: 76px;">
				<a onfocus="blur();" href="javascript:submitCesantia();"><img
						src="images/btn-img/btn-cotizar.gif"
						onmouseover="this.src = 'images/btn-img/btn-cotizar-hover.gif'"
						onmouseout="this.src = 'images/btn-img/btn-cotizar.gif'"
						onclick="pageTracker._trackEvent('Boton','Paso1-BotonCotizar-cesantia');"
						alt="" width="90" height="27" border="0" />
				</a>
			</div>
		</div>
	</div>

	</div> 
	
	<script type="text/javascript">
		function submitCesantia(){
			$('.grupo_nombre :input').each(function(){
		  		if($(this).val() == "nombre" || $(this).val() == "apellido paterno" || $(this).val() == "apellido materno" ) {
		  			$(this).val("");
		  		}
		  	});
			$('html, body').animate({scrollTop: 0}, 0);
		  	$('#loading').show();
		  	$('#grupo_valorizacion_iframe').hide();
		  	var caracter="1234567890";
			caracter+="QWERTYUIOPASDFGHJKLZXCVBNM";
			caracter+="qwertyuioplkjhgfdsazxcvbnm";
			var numero_caracteres=10;
				 
			var total=caracter.length;
			var clave="";
			for(a=0;a<numero_caracteres;a++){
			 	clave+=caracter.charAt(parseInt(total*Math.random(1)));
			}
			document.getElementById('claveVitrineo').value = clave;	
			document.getElementById('hiddCaptcha').value = "0";
			$('#cotizarProductoForm').submit();
		}
	</script>
	 <html:hidden property="datos(claveVitrineo)" value="1" styleId="claveVitrineo"/>
	 <html:hidden property="datos(hiddCaptcha)" value="1" styleId="hiddCaptcha"/>
	<!-- Elige tu seguro FIN--> 
 	<input type="hidden" name="idPlanValorizacion" id="idPlanValorizacion" value=""/>
</html:form>
 
<div id="globalExceptionDiv">
<logic:present name="<%=Globals.EXCEPTION_KEY%>">
<div class="cont-alert" id="globalExceptionDivIframe">
  <div class="alert"> <img src="/cotizador/images/mensaje-alerta.jpg" width="45" height="43" class="alert_ico" />
    <div class="mensaje" align="center">
      <p><bean:write name="<%=Globals.EXCEPTION_KEY%>" property="message" ignore="true" />&nbsp;</p>
    </div>
  </div>
</div>
<script type="text/javascript">
	window.parent.createDivTable('globalExceptionDivIframe', 'globalExceptionDiv');
	window.parent.$("#captcha-container").hide();
</script>
</logic:present>
</div>

<!-- Valorizacion -->
<div id="loading" style="display:none; width:100%; text-align: center;">
	<div id="datos-del-contratante">
		<div class="loader" style="text-align:center; margin-top:15px;">
			<img src="images/loading_gif.gif" align="baseline" width="380" height="95" />
		</div>
	</div>
</div>

<div id="grupo_valorizacion_iframe">
<logic:notEmpty name="planesCotizados">

	<%@ include file="./includes/cotizador-valorizacion.jsp" %>
</logic:notEmpty>
</div>

<!-- Helper iFrame for Planes Cotizados table ajax request -->
<iframe src="/cotizador/cotizacion/blank.jsp" width="100%" height="200" name="iframe_tabla" style="display:none;">
</iframe>
<!--	Fin Helper iFrame-->

<!-- FIN Valorizacion -->

 
 
 

<!--  contenido COBERTURAS FIN --> 
 
 
<!-- sitio-confiable --> 
<div id="sitio-confiable">
<!-- 
<div class="sitio-confiable-foto"><img src="images/btn-img/ico-confiable.gif" alt="" width="75" height="49" border="0" /></div>
<div class="sitio-confiable-tex"><p>Sitio 100% Confiable <span>Si tienes alguna duda ll�manos 600 500 5000</span></p></div>
 -->
 <img alt="" border='0' src="images/footer-cencosud-tmas.jpg" width="704" height="64">

</div> 
<!-- sitio-confiable FIN --> 
 
 
 
<!-- texto-pie --> 
<div id="texto-pie">Los valores en pesos han sido calculados en base a la UF del d�a de hoy, considerar que �stos variar�n seg�n el comportamiento de la UF.<br /> Esta Cotizaci�n es v�lida s�lo para compras por internet y tiene vigencia de <span>24 horas</span></div> 
<!-- texto-pie FIN--> 
 
 
</div> 
</div> 
 
<!--curva  -->
<div class="contenido-desplegado-cotizador-curva-booton"  style="margin-right: 0px"></div>
<!--curva  -->

<div id="curva-bottoncont-secciones"></div>
</div> 
		</div> 
	</div> 

<!-- COTIZADOR CONTENIDO FIN-->

	<!--INICIO FOOTER--> 
	<%@ include file="./includes/cotizador-footer.jsp"  %>
	<!--FIN FOOTER--> 	
	
<!-- FIN INICIO CONTENIDO --> 			
</body> 
 
</html>

