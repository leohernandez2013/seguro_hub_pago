<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<meta name="generator" content="sencosud" />
		<link href="css/estilos.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/base.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
		<link href="css/jquery/jqModal.css" type="text/css" rel="stylesheet" />
			
		<link rel="stylesheet" href="js/jquery/jquery.fancybox/jquery.fancybox.css" type="text/css" />
		<link href="css/formulario-r.css" rel="stylesheet" type="text/css" />
		
		<script src="js/jquery/jquery-1.4.2.js"></script>
		
        <!--[if lt IE 7]>
        		<script type="text/javascript" src="/vseg-paris/js/unitpngfix.js"></script>
	    <![endif]-->
	</head>

	<body style="background-image: none; height:90%; width:100%;">
	<%@ include file="../google-analytics/google-analytics.jsp" %>	
	
	<!-- INICIO Responsive  -->
	<div class="col-md-12" >
		<div class="row">
			<div class="col-md-6 col-xs-6 pull-left">
				<img src="/vseg-paris/img/logo_seguros_cencosud1.png" alt="" width="150" height="60" border="0" />
			</div>
		</div>
		<br/>
		<div class="row">
			<div class="col-md-12">
				<p style="font-size:14px;">Estimado <strong><logic:notEmpty property="contratante.nombre" name="datosCotizacion"><bean:write property="contratante.nombre" name="datosCotizacion"/></logic:notEmpty></strong>, gracias por cotizar con nosotros.</br>
				Tu cotizaci�n ha sido enviada a <strong><span> <logic:notEmpty name="datosCotizacion" property="email"> <bean:write name="datosCotizacion" property="email"/> </logic:notEmpty></span></strong></p>
				<p style="font-size:14px;">Tambi�n puedes retomar tu cotizaci�n por hasta 24 horas desde la pagina de inicio <span>"Mis Seguros Online"</span> de nuestro Sitio Web.</p>
				<p style="color:#009FE3;" align="center">Si tienes alguna duda ll�manos al <strong style="color:black">600 500 5000</strong></p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<p style="font-size:10px">Para visualizar este archivo se requiere Adobe Acrobat, si no lo tienes, desc�rgalo<a href="http://www.adobe.com/products/acrobat/readstep2.html"> Aqu&iacute;</a></p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<button type="button" class="btn btn-secundary btn-block" id="descargaPDF">DESCARGAR PDF</button>
			</div>
			<p></p>
			<div class="col-md-6">
				<button type="button" class="btn btn-primary btn-block" id="continuar">CONTINUAR CONTRATACION</button>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		$("#continuar").click(function(){
			parent.location = "/cotizador/continuar-cotizacion.do";
		});
		$("#descargaPDF").click(function(){
			$("#iframePDF").attr("src", "/cotizador/descargar-pdf-cotizacion.do") ;
		});
	</script>
	<iframe frameborder="0" id="iframePDF" src="<%= request.getContextPath()%>/js/blank.gif" height="0" width="0">
	</iframe>
				
	<!-- FIN Responsive -->
	</body>
</html>
