
<%@page import="cl.cencosud.acv.common.EstadoCivilEnum"%><%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>

<%@page import="java.util.Locale"%>
<%session.setAttribute("currentLocale", new Locale("es", "CL"));%> 

<div id="menu-bienvenido-d">

<div id="bienvenido"><h4>Resumen</h4></div>

<div id="datos-personales">

<logic:present name="cl.tinet.common.seguridad.USUARIO_CONECTADO">
<script type="text/javascript">
<bean:define id="usuario"
		name="<%=SeguridadUtil.USUARIO_CONECTADO_KEY %>" scope="session"
		type="UsuarioExterno" />
</script>
</logic:present>

<logic:notPresent name="usuario">
<logic:present name="datosCotizacion" scope="session"><h3><bean:message bundle="labels-cotizador" key="labels.general.bienvenida"/> <bean:write name="datosCotizacion" property="nombre" scope="session"/></h3></logic:present>
</logic:notPresent>

<logic:present name="usuario">
<h3><bean:message bundle="labels-cotizador" key="labels.general.bienvenida"/> <bean:write name="usuario" property="nombre"/></h3>
</logic:present>

<logic:notPresent name="usuario">
<logic:present name="datosCotizacion" scope="session">
<logic:notEqual name="datosCotizacion" property="rutCliente" value="0"> <p><span><bean:message bundle="labels-cotizador" key="labels.usuario.rut"/>:</span> <bean:write name="datosCotizacion" property="rutCliente"/>-<bean:write name="datosCotizacion" property="dv"/></p></logic:notEqual>
<logic:notEmpty name="datosCotizacion" property="nombre"> <p><span><bean:message bundle="labels-cotizador" key="labels.usuario.nombre"/>:</span> <bean:write name="datosCotizacion" property="nombre"/></p></logic:notEmpty>
<logic:notEmpty name="datosCotizacion" property="apellidoPaterno"> <p><span><bean:message bundle="labels-cotizador" key="labels.usuario.apellidos"/>:</span> <bean:write name="datosCotizacion" property="apellidoPaterno"/> <logic:notEmpty name="datosCotizacion" property="apellidoMaterno"> <bean:write name="datosCotizacion" property="apellidoMaterno"/> </logic:notEmpty></p> </logic:notEmpty>

<logic:notEqual name="datosCotizacion" property="numeroTelefono1" value="0"> <p><span><bean:message bundle="labels-cotizador" key="labels.usuario.numeroTelefono"/>:</span> <bean:write name="datosCotizacion" property="numeroTelefono1"/> </p></logic:notEqual>

<logic:notEmpty name="datosCotizacion" property="email"> <p><span><bean:message bundle="labels-cotizador" key="labels.usuario.email"/>:</span> <bean:write name="datosCotizacion" property="email"/></p></logic:notEmpty>
<logic:notEmpty name="datosCotizacion" property="fechaNacimiento"> <p><span><bean:message bundle="labels-cotizador" key="labels.usuario.fechaNacimiento"/>:</span> <bean:write name="datosCotizacion" property="fechaNacimiento" format="dd/MM/yyyy"/></p> </logic:notEmpty>
<logic:notEmpty name="datosCotizacion" property="estadoCivil"> <p><span><bean:message bundle="labels-cotizador" key="labels.usuario.estadoCivil"/>:</span> <bean:write name="datosCotizacion" property="estadoCivilDesc"/></p> </logic:notEmpty>
<logic:notEmpty name="datosCotizacion" property="actividad"> <p><span><bean:message bundle="labels-cotizador" key="labels.usuario.actividad"/>:</span> <logic:present name="actividadDesc"><bean:write name="actividadDesc"/></logic:present></p></logic:notEmpty>
<logic:notEmpty name="datosCotizacion" property="sexo"> <p><span><bean:message bundle="labels-cotizador" key="labels.usuario.sexo"/>:</span> <logic:equal value="M" name="datosCotizacion" property="sexo">Masculino</logic:equal> <logic:equal value="F" name="datosCotizacion" property="sexo">Femenino</logic:equal></p></logic:notEmpty>
<logic:notEmpty name="regionDescripcion"> <p><span><bean:message bundle="labels-cotizador" key="labels.usuario.region"/>:</span> <bean:write name="regionDescripcion"/></p></logic:notEmpty>
<logic:notEmpty name="comunaDescripcion"> <p><span><bean:message bundle="labels-cotizador" key="labels.usuario.comuna"/>:</span> <bean:write name="comunaDescripcion"/></p></logic:notEmpty>
<logic:notEmpty name="ciudadDescripcion"> <p><span><bean:message bundle="labels-cotizador" key="labels.usuario.ciudad"/>:</span> <bean:write name="ciudadDescripcion"/></p></logic:notEmpty>
<logic:notEmpty name="datosCotizacion" property="direccion"> <p><span><bean:message bundle="labels-cotizador" key="labels.usuario.direccion"/>:</span> <bean:write name="datosCotizacion" property="direccion"/> <bean:write name="datosCotizacion" property="numeroDireccion"/> <logic:notEmpty name="datosCotizacion" property="numeroDepto"><bean:write name="datosCotizacion" property="numeroDepto"/></logic:notEmpty></p></logic:notEmpty>
<logic:notEmpty name="datosCotizacion" property="antiguedadVivienda"> <p><span><bean:message bundle="labels-cotizador" key="labels.usuario.antiguedadVivienda"/>:</span> <bean:write name="datosCotizacion" property="antiguedadVivienda"/></p></logic:notEmpty>
</logic:present>
</logic:notPresent>

<logic:present name="usuario">
<p><span><bean:message bundle="labels-cotizador" key="labels.usuario.rut"/>:</span> <bean:write name="usuario" property="rut_cliente"/> - <bean:write name="usuario" property="dv_cliente"/></p>
<p><span><bean:message bundle="labels-cotizador" key="labels.usuario.nombre"/>:</span> <bean:write name="usuario" property="nombre"/></p>
<p><span><bean:message bundle="labels-cotizador" key="labels.usuario.apellidos"/>:</span> <bean:write name="usuario" property="apellido_paterno"/> <bean:write name="usuario" property="apellido_materno"/></p>
<p><span><bean:message bundle="labels-cotizador" key="labels.usuario.email"/>:</span> <bean:write name="usuario" property="email"/></p>
<p><span><bean:message bundle="labels-cotizador" key="labels.usuario.fechaNacimiento"/>:</span> <bean:write name="usuario" property="fecha_nacimiento" format="dd/MM/yyyy"/></p>
<p><span><bean:message bundle="labels-cotizador" key="labels.usuario.estadoCivil"/>:</span> <%= EstadoCivilEnum.valueOf("_" + ((UsuarioExterno) request.getSession().getAttribute(SeguridadUtil.USUARIO_CONECTADO_KEY)).getEstado_civil()).getDescripcion() %> </p>
<p><span><bean:message bundle="labels-cotizador" key="labels.usuario.sexo"/>:</span> <logic:equal value="M" name="usuario" property="sexo">Masculino</logic:equal> <logic:equal value="F" name="usuario" property="sexo">Femenino</logic:equal></p>
<p><span><bean:message bundle="labels-cotizador" key="labels.usuario.direccion"/>:</span> <bean:write name="usuario" property="calle"/> <bean:write name="usuario" property="numero"/> <bean:write name="usuario" property="numero_departamento"/></p>
</logic:present>


<logic:present name="datosPlan" scope="session">
<!-- -->
<div id="nombre-delplan">
<p><bean:message bundle="labels-cotizador" key="labels.usuario.plan.nombrePlan"/></p>
<p><span> <bean:write name="datosPlan" property="nombrePlan"/> </span></p>
<div class="peso" style="line-height:14px;"><h4><span><bean:write name="datosPlan" property="primaMensualPesos" format="$ ##,##0" locale="currentLocale"/></span></h4></div>
<div class="montomensual">
	<p style="width:50px; line-height:13px;">
		<logic:notPresent name="esPrimaUnica">
			Monto Mensual
		</logic:notPresent>
		<logic:present name="esPrimaUnica">
			Prima &Uacute;nica
		</logic:present>
	</p>
</div>
<div class="peso"><h5 style="font-size:16px;text-align:right;margin-right:0px;margin-top:0px;"><span><bean:write name="datosPlan" property="primaMensualUF" format="UF #0.0000" locale="currentLocale"/></span></h5></div>
<div class="montomensual">
	<p style="width:50px; line-height:13px;">
		<logic:notPresent name="esPrimaUnica">
			Monto Mensual
		</logic:notPresent>
		<logic:present name="esPrimaUnica">
			Prima &Uacute;nica
		</logic:present>
	</p>
</div>
</div>

<!--  <div id="a1"><a href=""><h5>ver m&aacute;s</h5></a></div> -->
<script type="text/javascript">
	
	$("#a1").fancybox({ 
		    'onStart'   : function() {
					var url = "desplegar-datos-cotizacion.do";
					
		     $("#a1").attr("href",url);
		     
		    },
		    'width'    : 800,
		    'height'   : 800,
		    'autoScale'   : false,
		    'transitionIn'  : 'none',
		    'transitionOut'  : 'none',
		    'type'    : 'iframe',
		    'scrolling'  : 'no' 
		  });
</script>

</logic:present>

</div>
<logic:notPresent name="idPlanLegacyPaso2">
<logic:notPresent name="usuario">
<!--
<div id="bannerClick">
<a href="javascript: void(0);" onclick="window.open('http://arcaris.com/cliente.php?key=cb153b7242d9c04845d4db907516f9e8','popup','menubar=0,resizable=0,width=440,height=426');return false;">
<img border="0" height="121" alt="" onmouseout="this.src = 'images/banner/que-seguro-home.gif'" onmouseover="this.src = 'images/banner/que-seguro-home-hover.gif'" src="images/banner/que-seguro-home.gif" style="padding-left:12px;padding-top:6px; margin-top: 30px; margin-left: -8px;">
</a>
</div>
-->
</logic:notPresent>
<logic:present name="usuario">
<!--
<div id="bannerClick">
<a href="javascript: void(0);" onclick="window.open('http://arcaris.com/cliente.php?key=cb153b7242d9c04845d4db907516f9e8','popup','menubar=0,resizable=0,width=440,height=426');return false;">
<img border="0" height="121" alt="" onmouseout="this.src = 'images/banner/que-seguro-home.gif'" onmouseover="this.src = 'images/banner/que-seguro-home-hover.gif'" src="images/banner/que-seguro-home.gif" style="padding-left:12px;padding-top:6px; margin-left: -8px; margin-top:10px;">
</a>
</div>
-->
</logic:present>
</logic:notPresent>

<div class="divvehiculo1"></div>
</div>
