<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-54BFFF');</script>
<!-- End Google Tag Manager -->

<!--[if lte IE 9]>
<script>window.location.href='/vseg-paris/html/alerta-ie9.html'</script>
<![endif]-->
<!--[if gt IE 8]><!-->
<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ page import="cl.tinet.common.struts.form.ValidableActionForm"%>

<meta http-equiv="Content-Language" content="es" />
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="" />
<meta name="author" content="" />
<link type="image/x-icon" rel="shortcut icon"
	href="/cotizador/images/btn-img/favicon.ico" />



<!-- Bootstrap core CSS -->
<link href="/vseg-paris/css/font-awesome.min.css" rel="stylesheet">
<link href="/vseg-paris/css/bootstrap.min.css" rel="stylesheet">
<link href="/vseg-paris/css/main.css" rel="stylesheet">
<link href="/vseg-paris/css/layout.css" rel="stylesheet">
<script src="/vseg-paris/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="/vseg-paris/js/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	    
<script src="/vseg-paris/js/libs/jquery-3.2.1.min.js"></script>
<!-- <script src="js/jquery-1.11.3.min.js"></script> -->
<script src="/cotizador/js/jquery.session.js" type="text/javascript"></script>

<!-- Important Owl stylesheet -->
<!--  <link rel="stylesheet" href="css/owl.carousel.css" />-->
<!--  <link rel="stylesheet" href="css/jquery-ui.css" />-->

<!-- Default Theme -->
<!-- <link rel="stylesheet" href="css/owl.theme.css" /> -->
<style>
.tooltip2 {
	display: inline;
	position: relative;
}

.tooltip2:hover:after {
	font-size: 13px;
	background: #333;
	background: rgba(0, 0, 0, .8);
	border-radius: 5px;
	bottom: 26px;
	color: #fff;
	content: attr(data-original-title);
	left: 20%;
	padding: 5px 15px;
	position: absolute;
	z-index: 98;
	width: 220px;
}

.tooltip2:hover:before {
	border: solid;
	border-color: #333 transparent;
	border-width: 6px 6px 0 6px;
	bottom: 20px;
	content: "";
	left: 50%;
	position: absolute;
	z-index: 99;
}
</style>
<!-- Include js plugin -->
<script src="/cotizador/js/owl.carousel.js"></script>
<script type="text/javascript">



		function collapse(event){
		element = event.target;
		event.preventDefault();
		if($(element).attr('state') == 1) {
		$(element).removeClass('is-active');
		$(element).next('.o-collapse__content').removeClass('is-active');
		$(element).attr('state', 0);
		} else {
		$(element).addClass('is-active');
		$(element).next('.o-collapse__content').addClass('is-active');
		$(element).attr('state', 1);
		}
		}
		
		
				check00 = false;
				check03 = false;
				check04 = false;
				check05 = false;
				check08 = false;
				check10 = false;
				inp00 = "";
				inp03 = "";
				inp04 = "";
				inp05 = "";
				inp08 = "";
				inp10 = "";
				 $('html, body').animate({scrollTop: 0}, 0);
				
				function bar(val,paso) {
					if(val!== 'undefined' && val >= 0 && val <= 100) {
						if(paso && paso >0) {
							val = (val * 33.35) / 100;
							if(paso == 2) {
								val = val + 33.35
							} else if(paso == 3) {
								val = val + 66.7;
							}
						}
						if(val >= 100) {
							val = 99.95;
						}
						$("[data-percentage]").css("width",val+"%");
					}
				}
				
			    function cargar00(){
					if ($('#input00').is (':checked')){
						document.getElementById("hiddProducto2").value = "188";
						$('.visible00').show();
						$('.visible00').attr("style", "display: table;");
					} else {
						document.getElementById("hiddProducto2").value = "1";
						$('.visible00').hide();
						$('.visible00').attr("style", "display: none;");
						$('#deducible00').hide();
						$('#check00sin').removeAttr('checked');
						$('#check00con').removeAttr('checked');
						habilitarComparador();
					}
						//if (document.getElementById("hiddProducto2").value != "188" && document.getElementById("producto").value != "188"){
							//document.getElementById("hiddProducto2").value = "188";
							//$('#waitModal').modal({backdrop: 'static', keyboard: false});
							//$("#formularioVehiculo").submit();
							////$('.visible00').show();
						/*} else {
							$('.visible00').show();
							$('.visible00').attr("style", "display: inline;") 
						}*/
					/*
					} else {
						document.getElementById("hiddProducto2").value = "1";
						$('.visible00').hide();
						$('.visible00').attr("style", "display: none;")
						$('#deducible00').hide();
						$('#check00sin').removeAttr('checked');
						$('#check00con').removeAttr('checked');
						habilitarComparador();
					}*/
				};
				function cargar03(){
					if ($('#input03').is (':checked')){
						document.getElementById("hiddProducto3").value = "189";
						$('.visible03').show();
						$('.visible03').attr("style", "display: table;");
						cargar00();
						cargar05();
						cargar10();
					} else {
						document.getElementById("hiddProducto3").value = "1";
						$('.visible03').hide();
						$('.visible03').attr("style", "display: none;");
						$('#deducible03').hide();
						$('#check03sin').removeAttr('checked');
						$('#check03con').removeAttr('checked');
						habilitarComparador();
					}
					//if ($('#input03').is (':checked')){
						
						//if (document.getElementById("hiddProducto3").value != "189" && document.getElementById("producto").value != "189"){
							//document.getElementById("hiddProducto3").value = "189";
							//$('#waitModal').modal({backdrop: 'static', keyboard: false});
							//$("#formularioVehiculo").submit();
							////$('.visible03').show();
						/*} else {
							$('.visible03').show();
							$('.visible03').attr("style", "display: inline;") 
						}*/
					/*
					} else {
						document.getElementById("hiddProducto3").value = "1";
						$('.visible03').hide();
						$('.visible03').attr("style", "display: none;")
						$('#check03sin').removeAttr('checked');
						$('#check03con').removeAttr('checked');
						habilitarComparador();
					}*/
				};
				function cargar04(){
					if ($('#input04').is (':checked')){
						document.getElementById("hiddProducto4").value = "190";
						$('.visible04').show();
						$('.visible04').attr("style", "display: table;");
					} else {
						document.getElementById("hiddProducto4").value = "1";
						$('.visible04').hide();
						$('.visible04').attr("style", "display: none;");
						$('#deducible04').hide();
						$('#check04sin').removeAttr('checked');
						$('#check04con').removeAttr('checked');
						habilitarComparador();
					}
					//if ($('#input04').is (':checked')){
						
						//if (document.getElementById("hiddProducto4").value != "190" && document.getElementById("producto").value != "190"){
							//document.getElementById("hiddProducto4").value = "190";
							//$('#waitModal').modal({backdrop: 'static', keyboard: false});
							//$("#formularioVehiculo").submit();
							////$('.visible04').show();
						/*} else {
							$('.visible04').show();
							$('.visible04').attr("style", "display: inline;") 
						}*/
					/*
					} else {
						document.getElementById("hiddProducto4").value = "1";
						$('.visible04').hide();
						$('.visible04').attr("style", "display: none;")
						$('#check04sin').removeAttr('checked');
						$('#check04con').removeAttr('checked');
						habilitarComparador();
					}*/
				};
				function cargar05(){
					if ($('#input05').is (':checked')){
						document.getElementById("hiddProducto5").value = "191";
						$('.visible05').show();
						$('.visible05').attr("style", "display: table;");
					} else {
						document.getElementById("hiddProducto5").value = "1";
						$('.visible05').hide();
						$('.visible05').attr("style", "display: none;");
						$('#deducible05').hide();
						$('#check05sin').removeAttr('checked');
						$('#check05con').removeAttr('checked');
						habilitarComparador();
					}

					//if ($('#input05').is (':checked')){
						
						//if (document.getElementById("hiddProducto5").value != "191" && document.getElementById("producto").value != "191"){
							//document.getElementById("hiddProducto5").value = "191";
							//$('#waitModal').modal({backdrop: 'static', keyboard: false});
							//$("#formularioVehiculo").submit();
							////$('.visible05').show();
						/*} else {
							$('.visible05').show();
							$('.visible05').attr("style", "display: inline;") 
						}*/
					/*
					} else {
						document.getElementById("hiddProducto5").value = "1";
						$('.visible05').hide();
						$('.visible05').attr("style", "display: none;")
						$('#check05sin').removeAttr('checked');
						$('#check05con').removeAttr('checked');
						habilitarComparador();
					}*/
				};
				function cargar08(){
					if ($('#input08').is (':checked')){
						document.getElementById("hiddProducto8").value = "192";
						$('.visible08').show();
						$('.visible08').attr("style", "display: table;")
					} else {
						document.getElementById("hiddProducto8").value = "1";
						$('.visible08').hide();
						$('.visible08').attr("style", "display: none;")
						$('#deducible08').hide();
						$('#check08sin').removeAttr('checked');
						$('#check08con').removeAttr('checked');
						habilitarComparador();
					}
					//if ($('#input08').is (':checked')){
						
						//if (document.getElementById("hiddProducto6").value != "192" && document.getElementById("producto").value != "192"){
							//document.getElementById("hiddProducto6").value = "192";
							//$('#waitModal').modal({backdrop: 'static', keyboard: false});
							//$("#formularioVehiculo").submit();
							////$('.visible08').show();
						/*} else {
							$('.visible08').show();
							$('.visible08').attr("style", "display: inline;") 
						}*/
					/*
					} else {
						document.getElementById("hiddProducto6").value = "1";
						$('.visible08').hide();
						$('.visible08').attr("style", "display: none;")
						$('#check08sin').removeAttr('checked');
						$('#check08con').removeAttr('checked');
						habilitarComparador();
					}*/
				};
				function cargar10(){
					if ($('#input10').is (':checked')){
						/*
						if (document.getElementById("hiddProducto7").value != "88" && document.getElementById("producto").value != "88"){
							document.getElementById("hiddProducto7").value = "88";
							$('#waitModal').modal({backdrop: 'static', keyboard: false});
							$("#formularioVehiculo").submit();
							//$('.visible10').show();
						} else {
							$('.visible10').show();
							$('.visible10').attr("style", "display: inline;") 
						}*/
						document.getElementById("hiddProducto7").value = "88";
						$('.visible10').show();
						$('.visible10').attr("style", "display: table;");
					} else {
						document.getElementById("hiddProducto7").value = "1";
						$('.visible10').hide();
						$('.visible10').attr("style", "display: none;")
						$('#check10sin').removeAttr('checked');
						$('#check10con').removeAttr('checked');
						habilitarComparador();
					}
				};
				
				function cargarDeducible(){
					if($('#input00').is (':checked') 
						|| $('#input03').is (':checked')
						|| $('#input05').is (':checked')
						|| $('#input10').is (':checked')){
						cargar00();
						cargar03();
						cargar05();
						cargar10();
					}else{
						$('.visible00').show();
						$('.visible00').attr("style", "display: table;");
						$('.visible03').show();
						$('.visible03').attr("style", "display: table;");
						$('.visible05').show();
						$('.visible05').attr("style", "display: table;");
						$('.visible10').show();
						$('.visible10').attr("style", "display: table;");
					}

				}
				function cargarCheck(producto){
					//alert(producto);
					//var plop = document.getElementById("hiddProducto2").value;
					//if(document.getElementById("hiddProducto2") != null){
						//alert(plop);
					//} else {
						//alert('es nulo');
					//}
					//if (producto == 188){
					//if (producto == 0){
						check00 = false;
					//}
					//if (producto == 0){ //189
						check03 = false;
					//}
					//if (producto == 0){ //190
						check04 = false;
					//}
					//if (producto == 0){ //191
						check05 = false;
					//}
					//if (producto == 0){ //192
						check08 = false;
					//}
					//if (producto == 0){ //88
						check10 = false;
					//}
				};

       $(document).ready(function() {

            $(".signin").click(function(e) {         
				e.preventDefault();
                $("fieldset#signin_menu").toggle();
				$(".signin").toggleClass("menu-open");
            });
			
				
			
        });
   </script>
<script type="text/javascript">


		$(document).ready(function() {
		
		$('html, body').animate({scrollTop: 0}, 0);
			
			$("#menu-bienvenido-d").height($("#contenido-cotizador").height() - 250);
			$(".divvehiculo1").height($("#contenido-cotizador").height() - 400);

			//Mostrar captcha si no hay valorizaciones.			
			if($("#grupo_valorizacion").length == 0){
				try {
					window.parent.recargar();
				} catch(e){
					//
				}
				window.parent.$("#captcha-container").show();
			}

			//Generar objeto para celulares
			var codigo_celular = "<option value=''>Seleccione</option>";
				codigo_celular += "<option value=\"09\">09</option>";
				codigo_celular += "<option value=\"08\">08</option>";
				codigo_celular += "<option value=\"07\">07</option>";
				codigo_celular += "<option value=\"06\">06</option>";
				codigo_celular += "<option value=\"05\">05</option>";
				
				
			var codigo_fijo = $("#contenido-desplegado-cotizador select[name='datos(codigoTelefono)']").html();
			var codigo_fijo_1 = $("#contenido-desplegado-cotizador select[name='datos(cteCodigoTelefono1)']").html();
			var codigo_fijo_2 = $("#contenido-desplegado-cotizador select[name='datos(cteCodigoTelefono2)']").html();
			var codigo_fijo_3 = $("#contenido-desplegado-cotizador select[name='datos(dueCodigoTelefono1)']").html();
			var codigo_fijo_4 = $("#contenido-desplegado-cotizador select[name='datos(dueCodigoTelefono2)']").html();
			var codigo_fijo_5 = $("#contenido-desplegado-cotizador select[name='datos(contactoCodigoTelefono1)']").html();
			var codigo_fijo_6 = $("#contenido-desplegado-cotizador select[name='datos(contactoCodigoTelefono2)']").html();
			var codigo_fijo_7 = $("#contenido-desplegado-cotizador select[name='datos(asegCodigoTelefono1)']").html();
			var codigo_fijo_8 = $("#contenido-desplegado-cotizador select[name='datos(asegCodigoTelefono2)']").html();
			var codigo_fijo_9 = $("#contenido-desplegado-cotizador select[name='datos(codigoTelefono1)']").html();
			var codigo_fijo_10 = $("#contenido-desplegado-cotizador select[name='datos(codigoTelefono2)']").html();
		
				
			if($("#contenido-desplegado-cotizador select[name='datos(tipoTelefono)'] :selected").text() == 'Celular') {
				$("#contenido-desplegado-cotizador select[name='datos(codigoTelefono)']").html(codigo_celular);
				
			}
			$("#contenido-desplegado-cotizador select[name='datos(tipoTelefono)']").change(function(){
				var sel = $("#contenido-desplegado-cotizador select[name='datos(tipoTelefono)'] :selected").text();

				if(sel == 'Celular'){
					$("#contenido-desplegado-cotizador select[name='datos(codigoTelefono)']").html(codigo_celular);
				} else {
					$("#contenido-desplegado-cotizador select[name='datos(codigoTelefono)']").html(codigo_fijo);
				}
			});
			
			
			// cteCodigoTelefono1 cteTipoTelefono1
			if($("#contenido-desplegado-cotizador select[name='datos(cteTipoTelefono1)'] :selected").text() == 'Celular') {
				$("#contenido-desplegado-cotizador select[name='datos(cteCodigoTelefono1)']").html(codigo_celular);
				
				
			}
			$("#contenido-desplegado-cotizador select[name='datos(cteTipoTelefono1)']").change(function(){
				var sel = $("#contenido-desplegado-cotizador select[name='datos(cteTipoTelefono1)'] :selected").text();

				if(sel == 'Celular'){
					$("#contenido-desplegado-cotizador select[name='datos(cteCodigoTelefono1)']").html(codigo_celular);
				} else {
					$("#contenido-desplegado-cotizador select[name='datos(cteCodigoTelefono1)']").html(codigo_fijo_1);
				}
			});
			
			// cteCodigoTelefono2 cteTipoTelefono2
			if($("#contenido-desplegado-cotizador select[name='datos(cteTipoTelefono2)'] :selected").text() == 'Celular') {
				$("#contenido-desplegado-cotizador select[name='datos(cteCodigoTelefono2)']").html(codigo_celular);
				
				
			}
			$("#contenido-desplegado-cotizador select[name='datos(cteTipoTelefono2)']").change(function(){
				var sel = $("#contenido-desplegado-cotizador select[name='datos(cteTipoTelefono2)'] :selected").text();

				if(sel == 'Celular'){
					$("#contenido-desplegado-cotizador select[name='datos(cteCodigoTelefono2)']").html(codigo_celular);
				} else {
					$("#contenido-desplegado-cotizador select[name='datos(cteCodigoTelefono2)']").html(codigo_fijo_2);
				}
			});
			
			// dueCodigoTelefono1 dueTipoTelefono1
			if($("#contenido-desplegado-cotizador select[name='datos(dueTipoTelefono1)'] :selected").text() == 'Celular') {
				$("#contenido-desplegado-cotizador select[name='datos(dueCodigoTelefono1)']").html(codigo_celular);
				
			}
			$("#contenido-desplegado-cotizador select[name='datos(dueTipoTelefono1)']").change(function(){
				var sel = $("#contenido-desplegado-cotizador select[name='datos(dueTipoTelefono1)'] :selected").text();

				if(sel == 'Celular'){
					$("#contenido-desplegado-cotizador select[name='datos(dueCodigoTelefono1)']").html(codigo_celular);
				} else {
					$("#contenido-desplegado-cotizador select[name='datos(dueCodigoTelefono1)']").html(codigo_fijo_3);
				}
			});
			
			// dueCodigoTelefono2 dueTipoTelefono2
			if($("#contenido-desplegado-cotizador select[name='datos(dueTipoTelefono2)'] :selected").text() == 'Celular') {
				$("#contenido-desplegado-cotizador select[name='datos(dueCodigoTelefono2)']").html(codigo_celular);
				
				
			}
			$("#contenido-desplegado-cotizador select[name='datos(dueTipoTelefono2)']").change(function(){
				var sel = $("#contenido-desplegado-cotizador select[name='datos(dueTipoTelefono2)'] :selected").text();

				if(sel == 'Celular'){
					$("#contenido-desplegado-cotizador select[name='datos(dueCodigoTelefono2)']").html(codigo_celular);
				} else {
					$("#contenido-desplegado-cotizador select[name='datos(dueCodigoTelefono2)']").html(codigo_fijo_4);
				}
			});
			
			// contactoCodigoTelefono1 contactoTipoTelefono1
			if($("#contenido-desplegado-cotizador select[name='datos(contactoTipoTelefono1)'] :selected").text() == 'Celular') {
				$("#contenido-desplegado-cotizador select[name='datos(contactoCodigoTelefono1)']").html(codigo_celular);
				
				
			}
			$("#contenido-desplegado-cotizador select[name='datos(contactoTipoTelefono1)']").change(function(){
				var sel = $("#contenido-desplegado-cotizador select[name='datos(contactoTipoTelefono1)'] :selected").text();

				if(sel == 'Celular'){
					$("#contenido-desplegado-cotizador select[name='datos(contactoCodigoTelefono1)']").html(codigo_celular);
				} else {
					$("#contenido-desplegado-cotizador select[name='datos(contactoCodigoTelefono1)']").html(codigo_fijo_5);
				}
			});
			// contactoCodigoTelefono2 contactoTipoTelefono2
			if($("#contenido-desplegado-cotizador select[name='datos(contactoTipoTelefono2)'] :selected").text() == 'Celular') {
				$("#contenido-desplegado-cotizador select[name='datos(contactoCodigoTelefono2)']").html(codigo_celular);
			}
			$("#contenido-desplegado-cotizador select[name='datos(contactoTipoTelefono2)']").change(function(){
				var sel = $("#contenido-desplegado-cotizador select[name='datos(contactoTipoTelefono2)'] :selected").text();

				if(sel == 'Celular'){
					$("#contenido-desplegado-cotizador select[name='datos(contactoCodigoTelefono2)']").html(codigo_celular);
				} else {
					$("#contenido-desplegado-cotizador select[name='datos(contactoCodigoTelefono2)']").html(codigo_fijo_6);
				}
			});
			// asegCodigoTelefono1 asegTipoTelefono1
			if($("#contenido-desplegado-cotizador select[name='datos(asegTipoTelefono1)'] :selected").text() == 'Celular') {
				$("#contenido-desplegado-cotizador select[name='datos(asegCodigoTelefono1)']").html(codigo_celular);
			}
			$("#contenido-desplegado-cotizador select[name='datos(asegTipoTelefono1)']").change(function(){
				var sel = $("#contenido-desplegado-cotizador select[name='datos(asegTipoTelefono1)'] :selected").text();

				if(sel == 'Celular'){
					$("#contenido-desplegado-cotizador select[name='datos(asegCodigoTelefono1)']").html(codigo_celular);
				} else {
					$("#contenido-desplegado-cotizador select[name='datos(asegCodigoTelefono1)']").html(codigo_fijo_7);
				}
			});
			// asegCodigoTelefono2 asegTipoTelefono2
			if($("#contenido-desplegado-cotizador select[name='datos(asegTipoTelefono2)'] :selected").text() == 'Celular') {
				$("#contenido-desplegado-cotizador select[name='datos(asegCodigoTelefono2)']").html(codigo_celular);
			}
			$("#contenido-desplegado-cotizador select[name='datos(asegTipoTelefono2)']").change(function(){
				var sel = $("#contenido-desplegado-cotizador select[name='datos(asegTipoTelefono2)'] :selected").text();

				if(sel == 'Celular'){
					$("#contenido-desplegado-cotizador select[name='datos(asegCodigoTelefono2)']").html(codigo_celular);
				} else {
					$("#contenido-desplegado-cotizador select[name='datos(asegCodigoTelefono2)']").html(codigo_fijo_8);
				}
			});
			// codigoTelefono1 tipoTelefono1
			if($("#contenido-desplegado-cotizador select[name='datos(tipoTelefono1)'] :selected").text() == 'Celular') {
				$("#contenido-desplegado-cotizador select[name='datos(codigoTelefono1)']").html(codigo_celular);
			}
			$("#contenido-desplegado-cotizador select[name='datos(tipoTelefono1)']").change(function(){
				var sel = $("#contenido-desplegado-cotizador select[name='datos(tipoTelefono1)'] :selected").text();

				if(sel == 'Celular'){
					$("#contenido-desplegado-cotizador select[name='datos(codigoTelefono1)']").html(codigo_celular);
				} else {
					$("#contenido-desplegado-cotizador select[name='datos(codigoTelefono1)']").html(codigo_fijo_9);
				}
			});
			// codigoTelefono2 tipoTelefono2
			if($("#contenido-desplegado-cotizador select[name='datos(tipoTelefono2)'] :selected").text() == 'Celular') {
				$("#contenido-desplegado-cotizador select[name='datos(codigoTelefono2)']").html(codigo_celular);
			}
			$("#contenido-desplegado-cotizador select[name='datos(tipoTelefono2)']").change(function(){
				var sel = $("#contenido-desplegado-cotizador select[name='datos(tipoTelefono2)'] :selected").text();

				if(sel == 'Celular'){
					$("#contenido-desplegado-cotizador select[name='datos(codigoTelefono2)']").html(codigo_celular);
				} else {
					$("#contenido-desplegado-cotizador select[name='datos(codigoTelefono2)']").html(codigo_fijo_10);
				}
			});
			
			
			
			$("#contenido-desplegado-cotizador :input").change(function(){
			
					if($("#grupo_valorizacion").length > 0) {
						//Mostrar captcha para poder valorizar nuevamente.
						try {
							recargar();
						} catch(e){
							//
						}
						$("#captcha-container").show();
						
						<logic:equal name="idRama" value="1">
							alert("Se ha modificado al menos un dato de la cotizaci�n, para continuar debes volver a cotizar");
							window.parent.document.getElementById('botonCotizarDiv').style.display='inline';
							$("#grupo_valorizacion").remove();	
						</logic:equal>
						
						<logic:notEqual name="idRama" value="1">
							alert("Se ha modificado al menos un dato de la cotizaci�n, para continuar debes volver a cotizar");
							$("#grupo_valorizacion").remove();	
						</logic:notEqual>						
					}
			});
					
			//var rut_cliente = "";
			//var dv_cliente = "";
			  
			//var url = '/vseg-paris/registrocliente/registroCliente.do';
			//var params = "?rut_cliente=" + rut_cliente;
			//params += "&dv_cliente=" + dv_cliente;
			   
			//$("#registroUsuario").colorbox({
			     //iframe:true, 
			     //width:"730px", 
			     //height:"100%",
			     //href: url + params 
			   // });
			
			// VALOR DEFAULT PARA DIRECCION
// 			$('.grupo_direccion :input').each(function(index) {
	    	 	
// 	    	 	if($(this).val() == "") {
	    	 		
// 	    	 		if(index == 0 || (index % 3 == 0)) {
// 	    	 			$(this).val("calle");
// 	    	 		}
// 	    	 		else if(index == 1 || ((index-1) % 3 == 0)){
// 	    	 			$(this).val("n�");
// 	    	 		}
// 	    	 		else if(index == 2 || ((index-2) % 3 == 0)){
// 	    	 			$(this).val("n� depto/casa");
// 	    	 		}
	    	 		
// 	    	 	}
// 		  	});
		  	
		  	$('.grupo_direccion :input').focus(function(){
		  		if($(this).val() == "calle" || $(this).val() == "n�" || $(this).val() == "n� depto/casa" ) {
		  			$(this).val("");
		  		}
		  	});
		  	
// 		  	$('.grupo_direccion :input').focusout(function(){
// 		  		$('.grupo_direccion :input').each(function(index) {
// 	    	 	if($(this).val() == "") {
	    	 		
// 	    	 		if(index == 0 || (index % 3 == 0)) {
// 	    	 			$(this).val("calle");
// 	    	 		}
// 	    	 		else if(index == 1 || ((index-1) % 3 == 0)){
// 	    	 			$(this).val("n�");
// 	    	 		}
// 	    	 		else if(index == 2 || ((index-2) % 3 == 0)){
// 	    	 			$(this).val("n� depto/casa");
// 	    	 		}
	    	 		
// 	    	 	}
// 		  		});
// 		  	});
			// FIN VALOR DEFAULT PARA DIRECCION
			
			
			
		});

	    var clear="images/clear.gif"; //path to clear.gif
	    
	    
	    
	    
	    
        </script>
<script type="text/javascript">
		 
		    $(document).ready(function() {
		
		      var owl = $("#owl-demo");
		
		      owl.owlCarousel({
		
		      items : 6, //10 items above 1000px browser width
		      itemsDesktop : [1200,8], //5 items between 1000px and 901px
		      itemsDesktopSmall : [992,3], // 3 items betweem 900px and 601px
		      itemsTablet: [768,2], //2 items between 600 and 0;
		      itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
		      
		      });
		      
		      $('.session-expirada').hide();
		      $('.datos-email-input').attr('mail','mail');
		      $('.datos-rut-input').attr('rut','rut');
              $('.datos-fecha-input').attr('fecha','fecha');
		      $('.datos-fecha2-input').attr('fecha2','fecha2');
		      $('.datos-viaje-input').attr('viaje','viaje');
			  
		      $("#contenido-desplegado-cotizador :input").change(function(){
					
					if($("#grupo_valorizacion").length > 0) {
						//Mostrar captcha para poder valorizar nuevamente.
						try {
							recargar();
						} catch(e){
							//
						}
						$("#captcha-container").show();
						
						<logic:equal name="idRama" value="1">
							alert("Se ha modificado al menos un dato de la cotizaci�n, para continuar debes volver a cotizar");
							window.parent.document.getElementById('botonCotizarDiv').style.display='inline';
							$("#grupo_valorizacion").remove();	
						</logic:equal>
						
						<logic:notEqual name="idRama" value="1">
							alert("Se ha modificado al menos un dato de la cotizaci�n, para continuar debes volver a cotizar");
							$("#grupo_valorizacion").remove();	
						</logic:notEqual>						
					}
			});
		    });
		    
    
    		function isRutKey(e){
			    var unicode=e.charCode? e.charCode : e.keyCode;
			    if (unicode!==8&&unicode!==107&&unicode!==75){ 
			        if (unicode<48||unicode>57) 
			            return false;
			    }
			}
    		
			
			function replaceAll( text, busca, reemplaza ){

				while (text.toString().indexOf(busca) != -1)
				text = text.toString().replace(busca,reemplaza);
				return text;

			}

				
    		function autocompletarfecha(input) {
			
				if(input && input.value && input.value.length && input.value.length >= 8) {
								
										var value = replaceAll(input.value, "-", "" );
										var dia = value.substring(0,2);
										var mes = value.substring(2,4);
										var anio = value.substring(4,8);
    				    				input.value = dia + "-" + mes + "-" + anio;
				}
			  
    		}
			
			
    		function autocompletarrut(input) {
    			if(input && input.value && input.value.length && input.value.length > 1) {
    				input.value = input.value.replace("-","");
    				    				var value = input.value.substring(0, input.value.length -1);
    				    				var digit = input.value.substring(input.value.length -1, input.value.length);
    				    				input.value = value + "-" + digit;
    				    			}
    		}
			
			function autocompletarfechaguion(input) {
				if(input.value.length == 2) {
					var fecha = input.value;
					input.value = fecha + "-";
				}
				else{ 
					if(input.value.length == 5) {
						var diaMes = input.value;
						input.value = diaMes +"-";
					}
				}
    		}
    		
			function validarFecha(campo)
			{	
				var dia = campo.substring(0,2);
				var mes = campo.substring(3,5);
												
				if(dia > 31  ||  mes > 12 ){
					return false;
				}else{
					return true;
				}
			}
			
			var rutResult = 0;
    		function validaRut(campo){
    			rutResult = 0;
    			if ( campo.length == 0 ){ rutResult = 1;return false; }
    			if ( campo.length < 8 ){ rutResult = 1;return false; }

    			campo = campo.replace('-','')
    			campo = campo.replace(/\./g,'')

    			var suma = 0;
    			var caracteres = "1234567890kK";
    			var contador = 0;    
    			for (var i=0; i < campo.length; i++){
    				u = campo.substring(i, i + 1);
    				if (caracteres.indexOf(u) != -1)
    				contador ++;
    			}
    			if ( contador==0 ) { rutResult = 1;return false }
    			
    			var rut = campo.substring(0,campo.length-1)
    			var drut = campo.substring( campo.length-1 )
    			var dvr = '0';
    			var mul = 2;
				var rut2 = parseInt(rut) ;
			
    			
    			for (i= rut.length -1 ; i >= 0; i--) {
    				suma = suma + rut.charAt(i) * mul
    		                if (mul == 7) 	mul = 2
    				        else	mul++
    			}
				
				if(suma == 0){

				return false;
				
				}
				
    			res = suma % 11
    			if (res==1)		dvr = 'k'
    		                else if (res==0) dvr = '0'
    			else {
    				dvi = 11-res
    				dvr = dvi + ""
    			}
    			if ( dvr != drut.toLowerCase() ) { rutResult = 1;return false; }
    			else { return true; }
    		}
    		// INICIO validacio secuencial de campos
    		// GC 13-12-16		
    		$(document).ready(function() {
    			var i = 1;
				$("#paso1").find(":input:not(:hidden,:button,:submit)").each(function(){
					var opcional = $(this).hasClass("optional");
					if(!opcional){
						$(this).addClass(" 1"+i);
						if($('.1'+i).attr('onblur') != undefined){
							var attr = "javascript: "+$('.1'+i).attr('onblur');
							attr +=" validarOnblur(1"+i+");";
							$('.1'+i).attr('onblur',attr);
						} else {
							var attr = "javascript: validarOnblur(1"+i+");";
							$('.1'+i).attr('onblur',attr);
						}
						i++;
					}
				});
				i = 1;
				$("#paso2").find(":input:not(:hidden,:button,:submit)").each(function(){
					var opcional = $(this).hasClass("optional");
					if(!opcional){
						$(this).addClass(" 2"+i);
						if($('.2'+i).attr('onblur') != undefined){
							var attr = "javascript: "+$('.2'+i).attr('onblur');
							attr +=" validarOnblur(2"+i+");";
							$('.2'+i).attr('onblur',attr);
						} else {
							var attr = "javascript: validarOnblur(2"+i+");";
							$('.2'+i).attr('onblur',attr);
						}
						i++;
					}
				});
    		});
    		
    		function validarOnblur(input) {
    			if($("."+input).val().trim() != "" && $("."+input).val() != undefined){
    				$("."+input).css("border-color","#cecece");
    				$("."+input).val($("."+input).val().trim());
    				//Validador de campos nueva web seguros
    				if(!$("."+input).hasClass('is-children')) {
				   		$("."+input).parents('.o-form__field').addClass('is-ok');
				   	}
				    $("."+input).parents('.o-form__field').removeClass('is-error');
				   	$("."+input).parents('.o-form__field').find('.o-form__message').html('');        

					if($("."+input).attr("mail")) {
						var result = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test($("."+input).val());
		    			if(result == false) {
							$("."+input).css("border-color","red");
							$("."+input).parents('.o-form__field').removeClass('is-ok');
				    		$("."+input).parents('.o-form__field').addClass('is-error');
				   			$("."+input).parents('.o-form__field').find('.o-form__message').html('El email tiene el formato incorrecto');   
	 					} 
					} else if($("."+input).attr("rut")) {
						var result = validaRut($("."+input).val());
						if(result==false){
							$("."+input).css("border-color","red");
							$("."+input).parents('.o-form__field').removeClass('is-ok');
							$("."+input).parents('.o-form__field').addClass('is-error');
   							$("."+input).parents('.o-form__field').find('.o-form__message').html('El rut tiene el formato incorrecto');
						} 
					} else if($("."+input).attr("fecha")) {
	    					var result = validarFecha($("."+input).val());
	    					if(result == false) {
	    						$("."+input).css("border-color","red");
	    						$("."+input).parents('.o-form__field').addClass('is-error');
	    						$("."+input).parents('.o-form__field').removeClass('is-ok');
   								$("."+input).parents('.o-form__field').find('.o-form__message').html('Este campo es requerido');
	    					} 
    				} else if($("."+input).attr("fecha2")) {
	    					var result = validarFecha($("."+input).val());
	    					if(result == false) {
	    						$("."+input).css("border-color","red");
	    						$("."+input).parents('.o-form__field').addClass('is-error');
	    						$("."+input).parents('.o-form__field').removeClass('is-ok');
   								$("."+input).parents('.o-form__field').find('.o-form__message').html('Este campo es requerido');
	    					}
    				}
	    				
				}else {
					$("."+input).css("border-color","red");
					$("."+input).val("");
					//Validador de campos nueva web seguros
					$("."+input).parents('.o-form__field').removeClass('is-ok');
					$("."+input).parents('.o-form__field').addClass('is-error');
   					$("."+input).parents('.o-form__field').find('.o-form__message').html('Este campo es requerido');
				}
				
				var once = new Boolean(false);
				var veintiuno = new Boolean(false);
				var logica = false;
				
				once = (input == "11" ? Boolean(false): Boolean(true));
				veintiuno = (input == "21" ? Boolean(false): Boolean(true));
				
				if (!once){
					logica = false;
				} else if(!veintiuno){
					logica = false;
				} else{
					logica = true;
				}
				
				if (logica){
					var inputResp = input;
					largo = input.toString().length;
					if(largo == 3){
						var aux = input.toString().substring(1, 3);
						aux--;
						if(aux.toString().length == 1){
						input = input.toString().substring(0, 1)+aux;
						} else{
							input = input-1;
						}
					}else{
						input = input-1;
					}
					

					if($("."+input).val().trim() != "" && $("."+input).val() != undefined){
						$("."+input).css("border-color","#cecece");
	    				$("."+input).val($("."+input).val().trim());
						if($("."+input).attr("mail")) {
							var result = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test($("."+input).val());
			    			if(result == false) {
								$("."+input).css("border-color","red");
		 					}
						} else if($("."+input).attr("rut")) {
							var result = validaRut($("."+input).val());
							if(result==false){
								$("."+input).css("border-color","red");
							}
						} else if($("."+input).attr("fecha")) {
		    					var result = validarFecha($("."+input).val());
		    					if(result == false) {
		    						$("."+input).css("border-color","red");
		    					}
	    				} else if($("."+input).attr("fecha2")) {
		    					var result = validarFecha($("."+input).val());
		    					if(result == false) {
		    						$("."+input).css("border-color","red");
		    					}
	    				}
		    				
					} else {
						$("."+input).css("border-color","red");
						$("."+input).val("");
					}
				}
			}
    		// FIN validacio secuencial de campos
    		
    		// INI cambio calendario
    		// GC:24/01/17
    		
    		//carga los dias en el select en la fecha de naciemiento
    		function cargaDia(idCombo){
				var options = "";
						options += '<option value="">D�a</option>';
						for (var i=1;i<=31;i++){
							var iconcero = "";
							if (i<=9){
								iconcero = "0"+i;
							} else {
								iconcero = i;
							}
							options += '<option value="'+iconcero+'">'+i+'</option>';
						}
						$("#"+idCombo).html(options);
			}
			
			//carga los meses en el select en la fecha de naciemiento
			function cargaMes(idCombo){
				var options = "";
						options += '<option value="">Mes</option>';
						for (var i=1;i<=12;i++){
							var iconcero = "";
							if (i<=9){
								iconcero = "0"+i;
							} else {
								iconcero = i;
							}
							options += '<option value="'+iconcero+'">'+i+'</option>';
						}
						$("#"+idCombo).html(options);
			}
			
			//carga los a�os en el select en la fecha de naciemiento
			function cargaAnyo(idCombo,viajeGrupal){
				var options = "";
				var dt = new Date();
				var year = dt.getFullYear()-18;
				if(viajeGrupal){
					year = dt.getFullYear();
				}
				options += '<option value="">A�o</option>';
					for (var i=year;i>=1930;i--){
						options += '<option value="'+i+'">'+i+'</option>';
					}
				$("#"+idCombo).html(options);
			}
			
			// INI cambio calendario
    		
    		//carga los dias en el select en la fecha de naciemiento (dia actual)
    		function cargaDiaNoAseg(idCombo){
				var options = "";
				var dt = new Date();
				var dia = dt.getUTCDate();
						options += '<option value="">D�a</option>';
						for (var i=dia;i>=1;i--){
								options += '<option value="'+i+'">'+i+'</option>';
							}
						$("#"+idCombo).html(options);
			}
			
			//carga los meses en el select en la fecha de naciemiento (hasta mes actual)
			function cargaMesNoAseg(idCombo){
				var options = "";
				var dt = new Date();
				var mes = dt.getMonth()+1;
						options += '<option value="">Mes</option>';
					for (var i=mes;i>=1;i--){
						options += '<option value="'+i+'">'+i+'</option>';
					}
						$("#"+idCombo).html(options);
			}
			
			//carga los a�os en el select en la fecha de naciemiento (carga el a�o actual)
			function cargaAnyoNoAseg(idCombo,viajeGrupal){
				var options = "";
				var dt = new Date();
				var year = dt.getFullYear();
				if(viajeGrupal){
					year = dt.getFullYear();
				}
				options += '<option value="">A�o</option>';
					for (var i=year;i>=1930;i--){
						options += '<option value="'+i+'">'+i+'</option>';
					}
				$("#"+idCombo).html(options);
			}
			
			
			//funcion para contatenar los nuevos select de fecha en un input hidden
			//param idFechaDia ID del select dia de la fecha de nacimiento
			//param idFechaMes ID del select mes de la fecha de nacimiento
			//param idFechaAnyo ID del select a�o de la fecha de nacimiento
			//param fechaCompleta ID input hidden que guarda la fecha con el formato correcto y se utiliza para realizar el submit
			function concatenarFechaCompleta(idFechaDia, idFechaMes, idFechaAnyo, fechaCompleta){
				var dia=$("#"+idFechaDia).val();
				var mes=$("#"+idFechaMes).val();
				var anyo=$("#"+idFechaAnyo).val();
				var fecha = dia+"-"+mes+"-"+anyo;
				$("#"+fechaCompleta).val(fecha);
			}
			
			//funcion para validar el formato de la fecha de nacimiento dd-MM-yyyy 01-02-2017
			function validarFormatoFecha(campo) {
				var RegExPattern = /^\d{2}\-\d{2}\-\d{4}$/;
			    if ((campo.match(RegExPattern)) && (campo!='')) {
					if(existeFecha(campo)){	
						return true;
					}else{
						return false;
					}
				} else {
					return false;
				}
			}
			
			//funcion para validar si la fecha es valida dd-MM-yyyy 01-02-2017
			function existeFecha(fecha){
				var fechaf = fecha.split("-");
				var day = fechaf[0];
				var month = fechaf[1];
				var year = fechaf[2];
				var date = new Date(year,month,'0');
				if((day-0)>(date.getDate()-0)){
					return false;
				}
				return true;
			}
			
			/*	funcion para cargar la fecha con la sesion activa
				param fecha id input hidden que se carga con la fecha de nacimiento desde sesion
				param idFechaDia id select dia fecha nacimiento
				param idFechaMes id select mes fecha naciemiento
				param idFechaAnyo is select a�o fecha nacimiento
			**/
			function cargarSelectFechaNacimiento(fecha, idFechaDia, idFechaMes, idFechaAnyo){
				var fechaArray = $("#"+fecha).val();
				if(fechaArray != ""){
					fechaArray = fechaArray.split("-");
					$("#"+idFechaDia).val(fechaArray[0]);
					$("#"+idFechaMes).val(fechaArray[1]);
					$("#"+idFechaAnyo).val(fechaArray[2]);
					return true;
				}else{
					return false
				}
			}
			
    		//FIN cambio calendario
    		
    		var validacionInput = 0;
    		function validateInput(id) {
    			validacionInput = 0;
    			$('#'+id+'').find(":input:not(:hidden,:button,:submit)").each(function(){
    				var optional = $(this).hasClass("optional");
    				    if(optional==false) {
							   	$(this).css("border-color","#cecece");
							   	if(!this.value) {
							   	$("#incomplete-text-"+id).html("Por favor complete el formulario correctamente");
							   	$("#incomplete-"+id).show("fast");
							   	$(this).css("border-color","red");
							   	// validaci?n 
							   	$(this).parents('.o-form__field').addClass('is-error');
							   	$(this).parents('.o-form__field').find('.o-form__message').html('Este campo es requerido');
							   	$(this).focus();
							   	validacionInput = 1;
							   	return false;
						} 
	    				if($(this).attr("mail")) {
	    					var result = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(this.value);
	    					if(result == false) {
	    						//$("#incomplete-text-"+id).html("El correo tiene el formato incorrecto");
	    						//$("#incomplete-"+id).show("fast");
	    						$(this).parents('.o-form__field').addClass('is-error');
   								$(this).parents('.o-form__field').find('.o-form__message').html('El correo tiene el formato incorrecto');
	    						$(this).css("border-color","red");
	    						$(this).focus();
	    						validacionInput = 1;
	    						return false;
	    					} 
	    				}
	    				if($(this).attr("rut")) {
						
							var rutDenegado1 = "14182714-6";
							var rutDenegado2 = "14479113-4";
						
							if( (this.value == rutDenegado1) || (this.value == rutDenegado2)){
									$("#incomplete-text-"+id).html("El rut ingresado ha superado la cantidad de cotizaciones permitidas");
									$("#incomplete-"+id).show("fast");
									$(this).css("border-color","red");
									$(this).focus();
									validacionInput = 1;
									return false;
							}else{
								var result = validaRut(this.value);
								if(result==false){
									//$("#incomplete-text-"+id).html("El rut tiene el formato incorrecto");
									//$("#incomplete-"+id).show("fast");
									$(this).parents('.o-form__field').addClass('is-error');
   									$(this).parents('.o-form__field').find('.o-form__message').html('El rut tiene el formato incorrecto');
									$(this).css("border-color","red");
									$(this).focus();
									validacionInput = 1;
									return false;
								} 
							}
	    				}
						if($(this).attr("fecha")) {
						var $fechaHidden = $(this);						
							if($(this).is("select")){
								//var $fechaHidden = $(this).parent().parent().children(":hidden");
		    					//var result = validarFormatoFecha($fechaHidden.val());
								var result = validarFormatoFecha($("#fechaCompleta").val());
		    					if(result == false) {
		    						//$("#incomplete-text-"+id).html("La fecha tiene formato incorrecto");
		    						//$("#incomplete-"+id).show("fast");
		    						$(this).parents('.o-form__field').addClass('is-error');
   									$(this).parents('.o-form__field').find('.o-form__message').html('La fecha tiene formato incorrecto');
		    						$(this).parent().parent().children("div").each(function(){
										$(this).children().css("border-color","red");
									});
		    						validacionInput = 1;
		    						return false;
		    					}
		    				}else{
		    					var result = validarFormatoFecha(this.value);
		    					if(result == false) {
		    						//$("#incomplete-text-"+id).html("La fecha tiene formato incorrecto");
		    						//$("#incomplete-"+id).show("fast");
		    						$(this).parents('.o-form__field').addClass('is-error');
   									$(this).parents('.o-form__field').find('.o-form__message').html('La fecha tiene formato incorrecto');
		    						$(this).css("border-color","red");
		    						$(this).focus();
		    						validacionInput = 1;
		    						return false;
	    						} 
		    				}
		    				
		    				// se agrega flag para validar la fecha de nacimiento en los flujos de moto y vehiculo.
		    				var validarFechaNacimiento = false;
		    				//Moto
		    				<logic:equal name="idSubcategoria" value="239">
	    						validarFechaNacimiento = true;
	    					</logic:equal>	
		    				
		    				//Vehiculo
		    				<logic:equal name="idRama" value="1">
		    					validarFechaNacimiento = true;
		    				</logic:equal>

		    				if(validarFechaNacimiento == true){
		    					if($(this).is("select")){
		    						//var $fechaHidden = $(this).parent().parent().children(":hidden");
		    						if (!calcularEdadFecha($("#fechaCompleta").val(),'18','90')) {
			    						$("#incomplete-text-"+id).html("El contratante debe ser mayor a 18 a?os");
			    						$("#incomplete-"+id).show("fast");
			    						$(this).parent().parent().children("div").each(function(){
											$(this).children().css("border-color","red");
										});
										validacionInput = 1;
										return false;
									}           
		    					}else{
		    						if (!calcularEdadFecha(this.value,'18','90')) {
			    						$("#incomplete-text-"+id).html("El contratante debe ser mayor a 18 a?os");
			    						$("#incomplete-"+id).show("fast");
			    						$(this).css("border-color","red");
			    						$(this).focus();
			    						validacionInput = 1;
			    						return false;
		    						}
		    					}
		    				}
	    				}
						if($(this).attr("fecha2")) {
	    					var result = validarFormatoFecha(this.value);
	    					if(result == false) {
	    						$("#incomplete-text-"+id).html("La fecha tiene formato incorrecto");
	    						$("#incomplete-"+id).show("fast");
	    						$(this).css("border-color","red");
	    						$(this).focus();
	    						validacionInput = 1;
	    						return false;
	    					} 
	    				}
					}
					
	    		});
    			if(validacionInput == 0) {
    			$("#incomplete-text-"+id).html("");
				$("#incomplete-"+id).hide("fast");
    			}
    			return true;
    		}
    		
    		function switchForm(bool) {
	    		if(bool===false) {
	    			$('html, body').animate({scrollTop: 0}, 0);
	    			$("#paso1").show();
	    			$("#paso2").hide();
	    		} else {
	    			console.log("pasa la validacion");
	    			var result = validateInput("paso1");
	    			if(validacionInput == 0) {
	    			
	    				personalInfo();
						var edadCorrecta = true;
						<logic:equal name="idSubcategoria" value="119">
							edadCorrecta =	calcularEdad();
						</logic:equal>
						
						if ( !edadCorrecta ){
							$("#incomplete-text-paso1").html("La edad requerida es mayor de 18 y menor de 75 a�os");
	    					$("#incomplete-paso1").show("fast");
	    					$("#fechaCompleta").css("border-color","red");
	    					$("#fechaCompleta").focus();
	    					console.log(this.paso1 + ' err�neo');
	    					validacionInput = 1;
						
						}else{						
	    				try {
		    				if(paso2bar && paso2bar !== 'undefined' && paso2bar >= 0 && paso2bar <= 100) {
		    					bar(paso2bar,2);
		    				} else {
		    					bar(33.35,1);
		    				}
	    				} catch(err) {
	    					bar(33.35,1);
	    				}
	    				$('html, body').animate({scrollTop: 0}, 0);
	    				$("#paso2").show();
	    				$("#paso1").hide();
						}
	    			}
	    			
	    		}
	    	}
			
			function switchForm2(bool) {
	    		if(bool===false) {
	    			$('html, body').animate({scrollTop: 0}, 0);
	    			$("#paso1").show();
	    			$("#paso2").hide();
	    		} else {
	    			$('html, body').animate({scrollTop: 0}, 0);
	    			$("#paso2").show();
	    			$("#paso1").hide();
	    		}
	    	}
			
			function switchForm3(bool) {
	    		var result = validateInput("paso1");
				
				if(validacionInput == 0) {
					return true;
				}else{
					return false;
				}
			}
	    	
	    	function personalInfo() {
	    		if(document.getElementById('datos.rutcompleto')) {
	    	  		var rutCompleto = document.getElementById('datos.rutcompleto').value.split('-');
	    	  		if(rutCompleto.length > 0) {
		    			document.getElementById('datos.rut').value = rutCompleto[0];
		    			if(rutCompleto.length > 1) {
		    				document.getElementById('datos.dv').value = rutCompleto[1];
		    			}
	    			}
	    	  	}
	    		if(document.getElementById('fechaCompleta')) {
	    			var fechaCompleta = document.getElementById('fechaCompleta').value.split('-');
	    			if(fechaCompleta.length === 3) {
	    				document.getElementById('diasFecha').value = fechaCompleta[0];
	    				document.getElementById('mesFecha').value = fechaCompleta[1];
	    				document.getElementById('anyosFecha').value = fechaCompleta[2];
	    			}
	    		}
	    	}
    
    		function desplegarErrores(){
				erroresOnPage = false;
				window.parent.cleanErroresParentPage();
				var campos = "";
				<logic:messagesPresent>
					<logic:iterate id="error" name="<%=ValidableActionForm.PROPERTY_NAMES%>">
						campos += "<bean:write name="error"/>" + ";";
					</logic:iterate>
				</logic:messagesPresent>
				if(campos != "" && campos.length > 0) {
					errores = campos.split(";");
					console.log("errores: " + errores);
					window.parent.setParentErrores(errores);
	
					for (var i = 0; i < errores.length; i++) {
						if(document.getElementById(errores[i]) != null) {
							window.parent.brandErrors(errores[i], i);
							window.parent.markErrorToFocus(errores[i]);
						}
						if(document.getElementById('error.' + errores[i]) != null){
							window.parent.printErrors('error.' + errores[i]);
						}
					}
					window.parent.setFocusOnFirstError();
					erroresOnPage = true;
				}
				if(erroresOnPage) {
					window.parent.$('#waitModal').modal('hide');
					
				}
			}
    		
    		function cleanParentClonedDivs(){
    			if(document.getElementById('globalExceptionDiv') != null){
    				globExceptionDiv = document.getElementById('globalExceptionDiv');
    				while (globExceptionDiv.childNodes[0]) {
    					globExceptionDiv.removeChild(globExceptionDiv.childNodes[0]);
    				}
    			}
				if(document.getElementById('globalExceptionDiv1') != null){
    				globExceptionDiv1 = document.getElementById('globalExceptionDiv1');
    				while (globExceptionDiv1.childNodes[0]) {
    					globExceptionDiv1.removeChild(globExceptionDiv1.childNodes[0]);
    				}
    			}
    			
    			if(document.getElementById('grupo_valorizacion_iframe') != null){
    				grupoValDiv = document.getElementById('grupo_valorizacion_iframe');
    				while (grupoValDiv.childNodes[0]) {
    					grupoValDiv.removeChild(grupoValDiv.childNodes[0]);
    				}
    			}
    		}
    		
    		function brandErrors(id, i){
    			divToBrand = document.getElementById(id);
    			classArr[i] = divToBrand.className;
    		}
    		
    		function printErrors(id){
    			myDiv = document.getElementById(id);
    			var errorDivCopy = frames.iframe_tabla.document.getElementById(id);
    			var errorDiv = errorDivCopy.cloneNode(true);
    			while (myDiv.childNodes[0]) {
    				myDiv.removeChild(myDiv.childNodes[0]);
    			}
    			if(errorDiv.outerHTML!=undefined) {
    				myDiv.innerHTML = errorDiv.outerHTML;
    			} else {
    				myDiv.appendChild(errorDiv);
    			}
    		}
    		
    		function markErrorToFocus(errorId){
    			erroresArr[errorId] = 1;
    		}
    		
    		function setFocusOnFirstError(){
    			for(key in erroresArr){
    				if(erroresArr[key]){
    					//alert('Key: ' + key + ' - Valor: ' + erroresArr[key]);
    					document.getElementById(key).focus();
    					break;
    				}
    			}
    			resetErroresOrdenValue();
    		}
    		
    		window.parent.cleanParentClonedDivs();
    		
    		var errores = new Array();
			var classArr = new Array();
			
			var erroresArr = new Array();
			
			setErroresOrden();
			
			
			function setErroresOrden(){
				erroresArr['datos.rut'] = 0;
				erroresArr['datos.dv'] = 0;
				erroresArr['datos.nombre'] = 0;
				erroresArr['datos.apellidoPaterno'] = 0;
				erroresArr['datos.apellidoMaterno'] = 0;
				erroresArr['datos.telefono1'] = 0;
				erroresArr['datos.telefono2'] = 0;
				erroresArr['datos.telefono3'] = 0;
				erroresArr['datos.tipoTelefono'] = 0;
				erroresArr['datos.codigoTelefono'] = 0;
				erroresArr['datos.numeroTelefono'] = 0;
				erroresArr['datos.email'] = 0;
				erroresArr['regionResidenciaCte'] = 0;
				erroresArr['comunaResidenciaCte'] = 0;
				erroresArr['diasFecha'] = 0;
				erroresArr['mesFecha'] = 0;
				erroresArr['anyosFecha'] = 0;
				erroresArr['datos.estadoCivil'] = 0;
				erroresArr['datos.rutDuenyo'] = 0;
				erroresArr['datos.dvDuenyo'] = 0;
				erroresArr['datos.dvRutDuenyo'] = 0;
				erroresArr['datos.nombreDuenyo'] = 0;
				erroresArr['datos.apellidoPaternoDuenyo'] = 0;
				erroresArr['datos.apellidoMaternoDuenyo'] = 0;
				erroresArr['regionResidencia'] = 0;
				erroresArr['comunaResidencia'] = 0;
				erroresArr['ciudadResidencia'] = 0;
				erroresArr['diasFechaDuenyo'] = 0;
				erroresArr['mesFechaDuenyo'] = 0;
				erroresArr['anyosFechaDuenyo'] = 0;
				erroresArr['datos.estadoCivilDuenyo'] = 0;
				erroresArr['datos.sexoDuenyo'] = 0;
				erroresArr['datos.direccion'] = 0;
				erroresArr['datos.direccionNumero'] = 0;
				erroresArr['datos.direccionNroDepto'] = 0;
				erroresArr['antiguedadVivienda'] = 0;
				erroresArr['tipoVehiculo'] = 0;
				erroresArr['marcaVehiculo'] = 0;
				erroresArr['modeloVehiculo'] = 0;
				erroresArr['anyoVehiculo'] = 0;
				erroresArr['valorComercial'] = 0;
				erroresArr['numeroPuertas'] = 0;
				erroresArr['producto'] = 0;
			}
			
			function resetErroresOrdenValue(){
				for(key in erroresArr){
					erroresArr[key] = 0;
				}
			}
			
			function setParentErrores(err){
				errores = err;
			}
			
			function loadCotizadorValoracionScript(){
				var script = document.createElement('script'); 
				script.type = 'text/javascript';
				script.src = 'js/Cotizador_Valorizacion.js'; 
				document.getElementsByTagName('head')[0].appendChild(script); 
			}
			
			function cleanErroresParentPage(){
				for (var i = 0; i < errores.length; i++) {
					if(document.getElementById(errores[i]) != null) {
						document.getElementById(errores[i]).className = classArr[i];
					}
					if(document.getElementById('error.' + errores[i]) != null){
						myDiv = document.getElementById('error.' + errores[i]);
						while (myDiv.childNodes[0]) {
							myDiv.removeChild(myDiv.childNodes[0]);
						}
					}
				}
		  		window.parent.$('#grupo_valorizacion_iframe').fadeIn(500);
			}
			
			function createDivTable(targetDiv, finalPositionDiv){
				var divToShow = frames.iframe_tabla.document.getElementById(targetDiv);
				var divOnIframe = divToShow.cloneNode(true);
				var myDiv = document.getElementById(finalPositionDiv);
				while (myDiv.childNodes[0]) {
					myDiv.removeChild(myDiv.childNodes[0]);
				}
				if(divOnIframe.outerHTML!=undefined) {
					myDiv.innerHTML = divOnIframe.outerHTML;
				} else {
					myDiv.innerHTML = frames.iframe_tabla.$("#"+targetDiv).parent().html();
				}
				$('#waitModal').modal('hide');
				$('#' + finalPositionDiv).focus();
				
			}
			
			function redefineVerCoberturas(finalPositionDiv){
				$('#' + finalPositionDiv).find('a').each(function(){
					redefineClick($(this));
				});
			}
			
			//calcular EDAD
			
			function calcularEdad(asegurado){
				
				if(asegurado){
				
					var dia = $("#diasFechaDuenyo").val();
					var mes = $("#mesFechaDuenyo").val();
					var ano = $("#anyosFechaDuenyo").val();
					
				}else{
				
					var dia = $("#diasFecha").val();
					var mes = $("#mesFecha").val();
					var ano = $("#anyosFecha").val();

				}
				
					// rescatamos los valores actuales
					var fecha_hoy = new Date();
					var ahora_ano = fecha_hoy.getYear();
					var ahora_mes = fecha_hoy.getMonth()+1;
					var ahora_dia = fecha_hoy.getDate();

					// realizamos el calculo
					var edad = (ahora_ano + 1900) - ano;
					if ( ahora_mes < mes ){
						edad--;
					}
					if ((mes == ahora_mes) && (ahora_dia < dia)){
						edad--;
					}
					if (edad > 1900){
						edad -= 1900;
					}

					// calculamos los meses
					var meses=0;
					if(ahora_mes>mes)
						meses=ahora_mes-mes;
					if(ahora_mes<mes)
						meses=12-(mes-ahora_mes);
					if(ahora_mes==mes && dia>ahora_dia)
						meses=11;

					// calculamos los dias
					var dias=0;
					if(ahora_dia>dia)
						dias=ahora_dia-dia;
					if(ahora_dia<dia){
						var ultimoDiaMes=new Date(ahora_ano, ahora_mes, 0);
						dias=ultimoDiaMes.getDate()-(dia-ahora_dia);
					}
					
					if(edad>18 && edad<75){
						return true;
					}else{
						return false;
					}	
			}
			
			
			
			$(document).ready(function(){
			
			// Tooltip.
			if(navigator.appName == "Microsoft Internet Explorer") {
				$(".tooltip_show").tooltip({position: "top right", offset: [30, 3]});
			}
			else{
				$(".tooltip_show").tooltip({position: "top right", offset: [20,3]});
			}
			
				
			// VALOR DEFAULT PARA NOMBRE
// 			$('.grupo_nombre :input').each(function(index) {
	    	 	
// 	    	 	if($(this).val() == "") {
	    	 		
// 	    	 		if(index == 0 || (index % 3 == 0)) {
// 	    	 			$(this).val("nombre");
// 	    	 		}
// 	    	 		else if(index == 1 || ((index-1) % 3 == 0)){
// 	    	 			$(this).val("apellido paterno");
// 	    	 		}
// 	    	 		else if(index == 2 || ((index-2) % 3 == 0)){
// 	    	 			$(this).val("apellido materno");
// 	    	 		}
	    	 		
// 	    	 	}
// 		  	});
		  	
		  	$('.grupo_nombre :input').focus(function(){
		  		if($(this).val() == "nombre" || $(this).val() == "apellido paterno" || $(this).val() == "apellido materno" ) {
		  			$(this).val("");
		  		}
		  	});
		  	
// 		  	$('.grupo_nombre :input').focusout(function(){
// 		  		$('.grupo_nombre :input').each(function(index) {
// 	    	 	if($(this).val() == "") {
	    	 		
// 	    	 		if(index == 0 || (index % 3 == 0)) {
// 	    	 			$(this).val("nombre");
// 	    	 		}
// 	    	 		else if(index == 1 || ((index-1) % 3 == 0)){
// 	    	 			$(this).val("apellido paterno");
// 	    	 		}
// 	    	 		else if(index == 2 || ((index-2) % 3 == 0)){
// 	    	 			$(this).val("apellido materno");
// 	    	 		}
	    	 		
// 	    	 	}
// 		  		});
// 		  	});
			// FIN VALOR DEFAULT PARA NOMBRE
			
			// VALOR DEFAULT PARA NOMBRE
// 			$('.grupo_nombre1 :input').each(function(index) {
	    	 	
// 	    	 	if($(this).val() == "") {
	    	 		
// 	    	 		if(index == 0 || (index % 3 == 0)) {
// 	    	 			$(this).val("nombre");
// 	    	 		}
// 	    	 		else if(index == 1 || ((index-1) % 3 == 0)){
// 	    	 			$(this).val("apellido paterno");
// 	    	 		}
// 	    	 		else if(index == 2 || ((index-2) % 3 == 0)){
// 	    	 			$(this).val("apellido materno");
// 	    	 		}
	    	 		
// 	    	 	}
// 		  	});
		  	
		  	$('.grupo_nombre1 :input').focus(function(){
		  		if($(this).val() == "nombre" || $(this).val() == "apellido paterno" || $(this).val() == "apellido materno" ) {
		  			$(this).val("");
		  		}
		  	});
		  	
// 		  	$('.grupo_nombre1 :input').focusout(function(){
// 		  		$('.grupo_nombre1 :input').each(function(index) {
// 	    	 	if($(this).val() == "") {
	    	 		
// 	    	 		if(index == 0 || (index % 3 == 0)) {
// 	    	 			$(this).val("nombre");
// 	    	 		}
// 	    	 		else if(index == 1 || ((index-1) % 3 == 0)){
// 	    	 			$(this).val("apellido paterno");
// 	    	 		}
// 	    	 		else if(index == 2 || ((index-2) % 3 == 0)){
// 	    	 			$(this).val("apellido materno");
// 	    	 		}
	    	 		
// 	    	 	}
// 		  		});
// 		  	});
			// FIN VALOR DEFAULT PARA NOMBRE
			
			});

		</script>
<script type="text/javascript">
		      //<![CDATA[
		      $(document).ready(function(){
		        $('.toggle-menu').jPushMenu({closeOnClickLink: false});
		        $('.dropdown-toggle').dropdown();
		        $('html, body').animate({scrollTop: 0}, 0);
		        $('[data-toggle="tooltip"]').tooltip();
		      });
		      //]]>
		    </script>
<script src="/cotizador/js/vendor/bootstrap.min.js"></script>
<script src="/cotizador/js/jPushMenu.js"></script>
<script src="/cotizador/js/v2p.js"></script>
<link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,700|Varela+Round" rel="stylesheet">
<!-- <link href="css/main.min.css" rel="stylesheet"> -->


<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<meta name="generator" content="Cencosud" />
<logic:equal name="idRama" value="1">
	<logic:present name="isPromocion">
		<logic:notPresent name="paso2">
			<title>Cotizar Paso 1 Promoci�n Veh�culos | Seguros Cencosud
			</title>
		</logic:notPresent>
		<logic:present name="paso2">
			<title>Cotizar Paso 2 Promoci�n Veh�culos | Seguros Cencosud
			</title>
		</logic:present>
	</logic:present>
	<logic:notPresent name="isPromocion">
		<logic:notPresent name="paso2">
			<title>Cotizar Paso 1 Veh�culos | Seguros Cencosud</title>
		</logic:notPresent>
		<logic:present name="paso2">
			<title>Cotizar Paso 2 Veh�culos | Seguros Cencosud</title>
		</logic:present>
	</logic:notPresent>
</logic:equal>
<logic:equal name="idRama" value="2">
	<logic:notPresent name="paso2">
		<title>Cotizar Paso 1 Hogar | Seguros Cencosud</title>
	</logic:notPresent>
	<logic:present name="paso2">
		<title>Cotizar Paso 2 Hogar | Seguros Cencosud</title>
	</logic:present>
</logic:equal>
<logic:equal name="idRama" value="3">
	<logic:notPresent name="paso2">
		<title>Cotizar Paso 1 Vida | Seguros Cencosud</title>
	</logic:notPresent>
	<logic:present name="paso2">
		<title>Cotizar Paso 2 Vida | Seguros Cencosud</title>
	</logic:present>
</logic:equal>
<logic:equal name="idRama" value="4">
	<logic:notPresent name="paso2">
		<title>Cotizar Paso 1 Salud | Seguros Cencosud</title>
	</logic:notPresent>
	<logic:present name="paso2">
		<title>Cotizar Paso 2 Salud | Seguros Cencosud</title>
	</logic:present>
</logic:equal>
<logic:equal name="idRama" value="5">
	<logic:notPresent name="paso2">
		<title>Cotizar Paso 1 Fraude | Seguros Cencosud</title>
	</logic:notPresent>
	<logic:present name="paso2">
		<title>Cotizar Paso 2 Fraude | Seguros Cencosud</title>
	</logic:present>
</logic:equal>
<logic:equal name="idRama" value="6">
	<logic:equal name="idSubcategoria" value="119">
		<logic:notPresent name="paso2">
			<title>Cotizar Paso 1 Viaje Protegido | Seguros Cencosud</title>
		</logic:notPresent>
		<logic:present name="paso2">
			<title>Cotizar Paso 2 Viaje Protegido | Seguros Cencosud</title>
		</logic:present>
	</logic:equal>
	<logic:equal name="idSubcategoria" value="46">
		<logic:notPresent name="paso2">
			<title>Cotizar Paso 1 Bolso Protegido | Seguros Cencosud</title>
		</logic:notPresent>
		<logic:present name="paso2">
			<title>Cotizar Paso 2 Bolso Protegido | Seguros Cencosud</title>
		</logic:present>
	</logic:equal>
</logic:equal>


<script src="/cotizador/js/cotizacion.js"></script>    
<script src="/cotizador/js/jquery/jquery.formatCurrency-1.4.0.js"></script>
<!-- 		<script src="js/jquery/jquery-ui-1.8.4.custom.min.js" type="text/javascript"></script>  -->
<script src="/cotizador/js/jquery-ui.js"></script>
