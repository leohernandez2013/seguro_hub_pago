<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>
<%@page import="cl.tinet.common.seguridad.servlet.AutenticacionServlet"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>


<logic:present name="<%=AutenticacionServlet.ERROR_AUTENTICACION_KEY%>">
<!--BOX PROCESO -->

<script type="text/javascript">
			$(document).ready(function(){
				html = "";
				campo = "<bean:write name="<%=AutenticacionServlet.ERROR_AUTENTICACION_KEY.trim()%>" ignore="true" />";
				cadena = "bloqueada";
				numIntentos = "3";
				numIntentos2 = "2";
				numIntentos3 = "1";
				numIntentos4 = "0";
				numIntentos5 = "rut";
				numIntentos6 = "usuario";
				if ($('#iniciarSesionDesktop').is(':visible')){
					if (campo.indexOf(numIntentos) > 0){
						$('#intento0').show();
					}else if (campo.indexOf(numIntentos2) > 0){
						$('#intento1').show();
					} else if (campo.indexOf(numIntentos3) > 0){
						$('#intento2').show();
					} else if (campo.indexOf(numIntentos4) > 0){
						$('#intento3').show();
					} else if (campo.indexOf(cadena) > 0){
						$('#intento4').show();
					} else if (campo.indexOf(numIntentos4) > 0){
						$('#intento6').show();
					} else if (campo.indexOf(numIntentos6) > 0){
						$('#intento5').show();
					}else if (campo.indexOf(numIntentos5) > 0){
						$('#intento6').show();
					}
					else{
						$('#errorLoginTxt').html(campo);
						$('#errorLoginTxt').show();
					}
					$('#loginDesktop').trigger('click.bs.dropdown');
					
				}else if($('#iniciarSesionMobile').is(':visible')){
					if (campo.indexOf(numIntentos) > 0){
						$('#mintento0').show();
					}else if (campo.indexOf(numIntentos2) > 0){
						$('#mintento1').show();
					} else if (campo.indexOf(numIntentos3) > 0){
						$('#mintento2').show();
					} else if (campo.indexOf(numIntentos4) > 0){
						$('#mintento3').show();
					} else if (campo.indexOf(cadena) > 0){
						$('#mintento4').show();
					} else if (campo.indexOf(numIntentos4) > 0){
						$('#mintento6').show();
					} else if (campo.indexOf(numIntentos6) > 0){
						$('#mintento5').show();
					}else if (campo.indexOf(numIntentos5) > 0){
						$('#mintento6').show();
					}
					else{
						$('#errorLoginTxt').html(campo);
						$('#errorLoginTxt').show();
					}
					$("#mostrarLogin").slideToggle();
				}
			});
</script>




</logic:present>
