<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!-- INICIO  BREADCRUMB -->

<section class="o-breadcrumb">
          <div class="container">
            <div class="row">
              <div class="col-12"><a class="o-breadcrumb__link" href=""/vseg-paris/index.jsp"">Inicio</a>
              
			            <logic:equal name="idRama" value="1"><a class="o-breadcrumb__link" href="/vseg-paris/desplegar-pagina-intermedia-vehiculos.do?idRama=1">SEGURO DE VEHÍCULO</a></logic:equal>
						<logic:equal name="idRama" value="2"><a class="o-breadcrumb__link" href="/vseg-paris/desplegar-pagina-intermedia-hogar.do?idRama=2">SEGURO DE HOGAR</a></logic:equal>
						<logic:equal name="idRama" value="3"><a class="o-breadcrumb__link" href="/vseg-paris/desplegar-pagina-intermedia-vida.do?idRama=3">SEGURO DE VIDA Y SALUD</a></logic:equal>
						<logic:equal name="idRama" value="5"><a class="o-breadcrumb__link" href="/vseg-paris/desplegar-pagina-intermedia-fraude.do?idRama=5">SEGURO DE FRAUDE</a></logic:equal>
						<logic:equal name="idRama" value="6"><a class="o-breadcrumb__link" href="/vseg-paris/desplegar-pagina-intermedia-fraude.do?idRama=6">SEGURO DE VIAJES</a></logic:equal>
						<logic:equal name="idRama" value="7"><a class="o-breadcrumb__link" href="#">SEGURO DE VIDA Y SALUD</a></logic:equal>
				      	<logic:equal name="idRama" value="8"><a class="o-breadcrumb__link" href="/vseg-paris/desplegar-pagina-intermedia-fraude.do?idRama=8">SEGURO DE MOTO</a></logic:equal>
             
        <logic:equal name="idRama" value="1">
	      	<logic:equal name="idSubcategoria" value="22"><a class="o-breadcrumb__link" href="/cotizador/desplegar-cotizacion.do?idRama=1&idSubcategoria=22">FULL COBERTURA</a></logic:equal>
	      	<logic:equal name="idSubcategoria" value="21"><a class="o-breadcrumb__link" href="/cotizador/desplegar-cotizacion.do?idRama=1&idSubcategoria=21&idTipo=1">PÉRDIDA TOTAL</a></logic:equal>
	      	<logic:equal name="idSubcategoria" value="36"><a class="o-breadcrumb__link" href="/cotizador/desplegar-cotizacion.do?idRama=1&idSubcategoria=36&idTipo=1">SEGURO OBLIGATORIO MERCOSUR</a></logic:equal>
	      	<logic:equal name="idSubcategoria" value="79"><a class="o-breadcrumb__link" href="#">ROBO CONTENIDO</a></logic:equal>
      	</logic:equal>
		<logic:equal name="idRama" value="2">
	      	<logic:equal name="idSubcategoria" value="24"><a class="o-breadcrumb__link" href="/cotizador/desplegar-cotizacion.do?idRama=2&idSubcategoria=24">INCENDIO Y SISMO / ESTRUCTURA</a></logic:equal>
	      	<logic:equal name="idSubcategoria" value="43"><a class="o-breadcrumb__link" href="/cotizador/desplegar-cotizacion.do?idRama=2&idSubcategoria=43">INCENDIO Y SISMO / CONTENIDO</a></logic:equal>
	      	<logic:equal name="idSubcategoria" value="44"><a class="o-breadcrumb__link" href="/cotizador/desplegar-cotizacion.do?idRama=2&idSubcategoria=44">INCENDIO + ROBO</a></logic:equal>
	      	<logic:equal name="idSubcategoria" value="25"><a class="o-breadcrumb__link" href="/cotizador/desplegar-cotizacion.do?idRama=2&idSubcategoria=25">INCENDIO + ROBO + SISMO</a></logic:equal>
			<logic:equal name="idSubcategoria" value="26"><a class="o-breadcrumb__link" href="/cotizador/desplegar-cotizacion.do?idRama=2&idSubcategoria=26">HOGAR PREMIO A LA PERMANENCIA</a></logic:equal>
			<logic:equal name="idSubcategoria" value="201"><a class="o-breadcrumb__link" href="/cotizador/desplegar-cotizacion.do?idRama=2&idSubcategoria=201">HOGAR FULL ASISTENCIA</a></logic:equal>
			<logic:equal name="idSubcategoria" value="220"><a class="o-breadcrumb__link" href="/cotizador/desplegar-cotizacion.do?idRama=2&idSubcategoria=220">HOGAR VACACIONES</a></logic:equal>
      	</logic:equal>
		<logic:equal name="idRama" value="3">
	      	<logic:equal name="idSubcategoria" value="29"><a class="o-breadcrumb__link" href="#">SEGURO DE ACCIDENTES</a></logic:equal>
      	</logic:equal>
		<logic:equal name="idRama" value="5">
	      	<logic:equal name="idSubcategoria" value="139"><a class="o-breadcrumb__link" href="/cotizador/desplegar-cotizacion.do?idRama=5&idSubcategoria=139">SEGURO FRAUDE</a></logic:equal>
			<logic:equal name="idSubcategoria" value="159"><a class="o-breadcrumb__link" href="#">FRAUDE CON DEVOLUCIÓN</a></logic:equal>
      	</logic:equal>
		<logic:equal name="idRama" value="6">
	      	<logic:equal name="idSubcategoria" value="119"><a class="o-breadcrumb__link" href="#">ASISTENCIA EN VIAJE</a></logic:equal>
			<logic:equal name="idSubcategoria" value="140"><a class="o-breadcrumb__link" href="/cotizador/desplegar-cotizacion.do?idRama=6&idSubcategoria=140">MASCOTAS</a></logic:equal>
			<logic:equal name="idSubcategoria" value="221"><a class="o-breadcrumb__link" href="/cotizador/desplegar-cotizacion.do?idRama=6&idSubcategoria=221">VIAJE GRUPAL</a></logic:equal>
      	</logic:equal>
		<logic:equal name="idRama" value="7">
			<logic:equal name="idSubcategoria" value="161"><a class="o-breadcrumb__link" href="#">HOSPITALIZACIÓN CON DEVOLUCIÓN</a></logic:equal>
	      	<logic:equal name="idSubcategoria" value="162"><a class="o-breadcrumb__link" href="#">ACCIDENTES PERSONALES CON DEVOLUCIÓN</a></logic:equal>
			<logic:equal name="idSubcategoria" value="164"><a class="o-breadcrumb__link" href="#">ONCOLÓGICO CON DEVOLUCIÓN</a></logic:equal>
      	</logic:equal>
      	<logic:equal name="idRama" value="8">
	      	<logic:equal name="idSubcategoria" value="239"><a class="o-breadcrumb__link" href="/cotizador/desplegar-cotizacion.do?idRama=8&idSubcategoria=239">SEGURO OBLIGATORIO MERCUSOR</a></logic:equal>
      	</logic:equal>
             
              <a class="o-breadcrumb__link o-breadcrumb__link--current" href="#">
              Cotización
              </a>
              </div>
            </div>
          </div>
 </section>
  
  <!-- FIN  BREADCRUMB --> 