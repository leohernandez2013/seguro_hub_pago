<!-- INICIO COMPA�IAS QUE TRABAJN CON NOSOTROS -->
<!-- INICIO FOOTER-->
<div class="footer footer-seguros" id="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<div class="modal fade" id="waitModal" role="dialog">
					<div class="modal-dialog">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-body">
								<p align="center">Por favor espere...</p>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!--/.row-->
		
	</div>
	<!--/.container-->
</div>
<div class="u-bgwhite u-ptb40">
          <div class="container">
            <div class="row">
              <div class="col-sm-12">
                <h2 class="o-title o-title--secundary u-text-center u-mb40">Compa��as que trabajan con nosotros</h2>
              </div>
            </div>
			<div class="row footer_logotipos">
              <div class="col-6 col-md-4 col-lg-2"><img class="u-image" src="/vseg-paris/img/brand-bancochile.jpg" alt=""></div>
              <div class="col-6 col-md-4 col-lg-2"><img class="u-image" src="/vseg-paris/img/brand-bicevida.jpg" alt=""></div>
              <div class="col-6 col-md-4 col-lg-2"><img class="u-image" src="/vseg-paris/img/brand-consorcio.jpg" alt=""></div>
              <div class="col-6 col-md-4 col-lg-2"><img class="u-image" src="/vseg-paris/img/brand-libertyseguros.jpg" alt=""></div>
              <div class="col-6 col-md-4 col-lg-2"><img class="u-image" src="/vseg-paris/img/brand-segurossura.jpg" alt=""></div>
              <div class="col-6 col-md-4 col-lg-2 "><img class="u-image" src="/vseg-paris/img/brand-mapfreseguros.jpg" alt=""></div>
              <div class="col-6 col-md-4 col-lg-2"><img class="u-image" src="/vseg-paris/img/brand-metlife.jpg" alt=""></div>
              <div class="col-6 col-md-4 col-lg-2"><img class="u-image" src="/vseg-paris/img/brand-zenitseguros.jpg" alt=""></div>
			 <div class="col-6 col-md-4 col-lg-2"><img class="u-image" src="/vseg-paris/img/hdi_seguros.jpg" alt=""></div>
            </div>
          </div>
</div>
<div class="c-socialbar u-ptb20">
      <div class="c-socialbar__social"><span class="c-socialbar__text">S�guenos en</span><a class="c-socialbar__link c-socialbar__link--twitter" href="http://bit.ly/29g8lbN" target="_blank"><i class="c-socialbar__icon mx-twitter"></i></a><a class="c-socialbar__link c-socialbar__link--youtube" href="https://www.youtube.com/user/Seguroscencosud" target="_blank"><i class="c-socialbar__icon mx-youtube"></i></a><a class="c-socialbar__link c-socialbar__link--facebook" href="http://bit.ly/29h8n3W" target="_blank"><i class="c-socialbar__icon mx-facebook"></i></a></div>
      <div class="c-socialbar__phone"><span>Servicio al cliente</span><br><a href="tel:+5626005005000">600 500 5000</a></div>
</div>
<footer class="c-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <ul class="o-list o-list--links">
              <li class="o-list__item o-list__item--parent"><a class="o-list__link js-toogle" href="#" state="0"><strong>Te ayudamos</strong></a></li>
              <li class="o-list__item"><a class="o-list__link" href="/vseg-paris/html/preguntas_frecuentes.html">Preguntas Frecuentes</a></li>
              <li class="o-list__item"><a class="o-list__link" href="/vseg-paris/html/en_caso_siniestro.html">En caso de siniestro</a></li>
              <li class="o-list__item"><a class="o-list__link" href="/vseg-paris/html/nuestras_sucursales.html">Sucursales</a></li>
              <li class="o-list__item"><a class="o-list__link" href="/vseg-paris/html/polizas.html">P�lizas</a></li>
              <li class="o-list__item"><a class="o-list__link" href="/vseg-paris/html/glosario.html">Glosario</a></li>
              <li class="o-list__item"><a class="o-list__link" href="/vseg-paris/html/puntos_cencosud.html">Acumulas Puntos Cencosud</a></li>
              <li class="o-list__item"><a class="o-list__link" href="/vseg-paris/html/contacto.html">Cont�ctanos</a></li>
            </ul>
          </div>
          <div class="col-md-3">
            <ul class="o-list o-list--links">
              <li class="o-list__item o-list__item--parent"><a class="o-list__link js-toogle" href="#" state="0"><strong>Pol�ticas de sitio</strong></a></li>
              <li class="o-list__item"><a class="o-list__link" href="/vseg-paris/html/nuestra_empresa.html">Nuestra Empresa</a></li>
              <li class="o-list__item"><a class="o-list__link" href="/vseg-paris/html/politica_seguridad.html">Pol�ticas de Seguridad</a></li>
              <li class="o-list__item"><a class="o-list__link" href="/vseg-paris/html/terminos_condiciones.html">T�rminos y condiciones</a></li>
            </ul>
          </div>
          <div class="col-md-3">
            <ul class="o-list o-list--links">
              <li class="o-list__item o-list__item--parent"><a class="o-list__link js-toogle" href="#" state="0"><strong>Informaci�n</strong></a></li>
              <li class="o-list__item"><a class="o-list__link" href="http://www.svs.cl/portal/principal/605/w3-channel.html" target="_blank">Superintendencia de Valores y Seguros</a></li>
              <li class="o-list__item"><a class="o-list__link" href="/vseg-paris/html/bases_Promocion.html">Bases de promociones</a></li>
              <li class="o-list__item"><a class="o-list__link" href="/vseg-paris/html/credito_consumo.html">Seguro de Cr�dito de Consumo</a></li>
              <li class="o-list__item"><a class="o-list__link" href="/vseg-paris/html/Contratacion_Voluntaria.html">Contrataci�n Voluntaria</a></li>
              <li class="o-list__item"><a class="o-list__link" href="/vseg-paris/contactoDenuncias.do?donde=inicio">Denuncias Ley N�20.393</a></li>
			  <li class="o-list__item"><a class="o-list__link" href="/vseg-paris/html/ganadores.html">Ganadores</a></li>
            </ul>
          </div>
          <div class="col-md-3">
            <ul class="o-list o-list--links">
              <li class="o-list__item o-list__item--parent"><a class="o-list__link js-toogle" href="#" state="0"><strong>Cencosud</strong></a></li>
              <li class="o-list__item"><a class="o-list__link" href="https://www.tarjetacencosud.cl/TarjetaMasWEB/home.html">Tarjeta Scotiabank Cencosud</a></li>
              <li class="o-list__item"><a class="o-list__link" href="http://www.puntoscencosud.cl/">Puntos Cencosud</a></li>
              <li class="o-list__item"><a class="o-list__link" href="http://www.paris.cl/">Paris</a></li>
              <li class="o-list__item"><a class="o-list__link" href="http://www.jumbo.cl/">Jumbo</a></li>
              <li class="o-list__item"><a class="o-list__link" href="http://www.easy.cl/">Easy</a></li>
              <li class="o-list__item"><a class="o-list__link" href="http://www.johnson.cl/">Johnson</a></li>
              <li class="o-list__item"><a class="o-list__link" href="http://www.santaisabel.cl/">Santa Isabel</a></li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
<div class="c-payments">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 u-text-center">
            <p class="c-payments__text">Medios de Pago:</p><a class="c-payments__link" href=""><img class="c-payments__image" src="/vseg-paris/img/payments-cards.png" alt=""></a><a class="c-payments__link" href=""><img class="c-payments__image" src="/vseg-paris/img/payments-webpay.png" alt=""></a>
          </div>
        </div>
      </div>
    </div>
	<div class="col-12"> 
		<div class="txt_legal c-summary__subtitle" style="padding-bottom: 0;"> 
			<p>Copyright 2017. Todos los derechos reservados. Sitio optimizado para los navegadores Mozilla Firefox, Google Chrome e Explorer 10 � superior.</p>        
		</div> 
	</div>
<!-- FIN FOOTER--> 
<script src="/cotizador/js/libs/slick.min.js"></script>
<script src="/cotizador/js/jquery.validate.js"></script>    
<script src="/cotizador/js/libs/remodal.js"></script>
<script src="/cotizador/js/main.js"></script>
<script src="/vseg-paris/js/layout.js"></script>
