<logic:present name="cl.tinet.common.seguridad.USUARIO_CONECTADO">
<script type="text/javascript">
<bean:define id="usuario"
		name="<%=SeguridadUtil.USUARIO_CONECTADO_KEY %>" scope="session"
		type="UsuarioExterno" />
</script>
</logic:present>

<logic:notPresent name="usuario">
<!-- INICIO REGISTRO -->
<div id="estas-registrado">
<div id="estas-registrado-texto-iz">
<p class="textcontrata">�Est�s Registrado?</p>
<h3>Ahorra tiempo cotizando con tu clave</h3>
</div>
<div id="ingresar-datos">




<script type="text/javascript">
	$(document).ready(function (){
		//Se envia formulario al presionar enter.
		$('#passUser').keypress(function(e){
			if(e.which == 13){
				e.preventDefault();
			    submitLogin()
				return false;
			}
		});
	}

	function submitLogin(){
		document.loginForm.username.value = document.getElementById("rutUser").value + "-" + document.getElementById("dvUser").value;
		document.loginForm.password.value = document.getElementById("passUser").value;
		document.loginForm.submit();
	});
</script>
<form action="/cotizador/Login" method="post" name="loginForm">

<input type="hidden" name="username" value=""/>
<input type="hidden" name="password" value=""/>
<input type="hidden" name="cl.tinet.common.seguridad.servlet.URI_EXITO" value="/desplegar-cotizacion.do?idRama=<bean:write name="idRama"/>&idSubcategoria=<bean:write name="idSubcategoria"/>"/>
<input type="hidden" name="cl.tinet.common.seguridad.servlet.URI_FRACASO" value="/desplegar-cotizacion.do?idRama=<bean:write name="idRama"/>&idSubcategoria=<bean:write name="idSubcategoria"/>"/>

<div id="rut-clave">
<div class="rut-clave_con">
<p>rut
<input name="rutUser" id="rutUser" type="text" class="ingrasa_rut_cl" size="8"  maxlength="8" />
-
<input name="dvUser" id="dvUser" type="text" class="ingrasa_rut_cl2" size="1"  maxlength="1"/>
<span>clave</span>
<input type="password" class="ingrasa_rut_cl" name="passUser" id="passUser" maxlength="6" /></p>
</div>
<div class="rut-clave-ir">
<a onfocus="blur();" href="#" onclick="javacrtip:submitLogin();"><img class="dsR3"  src="images/btn-img/ir-in-btn.jpg"onmouseover="this.src = 'images/btn-img/ir-in-btn_hover.jpg'" border="0" onmouseout="this.src = 'images/btn-img/ir-in-btn.jpg'" border="0" alt="" border="0" /></a></div>
</div>
</form>
<div id="olvidaste-tu-clave">
<!-- -->
<div class="olvidaste-tu-clave-flecha">
<img  src="images/btn-img/btn-flacha-clave.jpg" alt="" width="10" height="10" border="0" /></div>
<div class="olvidaste-tu-clave-texto">
<span class="fancy iframe"  style="text-decoration: underline; cursor: pointer;" id="registroUsuario">Obt&eacute;n tu Clave</span>
</div>
<!-- --><!-- -->
<div class="olvidaste-tu-clave-flecha">
<img src="images/btn-img/btn-flacha-clave.jpg" alt="" width="10" height="10" border="0" /></div>
<div class="olvidaste-tu-clave-texto">
<a id="olvidasteClave" style="text-decoration: underline; cursor: pointer;" class="fancy iframe">�Olvidaste tu Clave?</a></div>
<!-- --></div>
</div>
</div>
<script type="text/javascript">

			//$("#olvidasteClave").click(function(){
			
			//	var url = "/vseg-paris/cambio-clave/recuperar-clave.do";
			//	var params = "?rut=" + $("#rutUser").val();
			//	params += "&dv=" + $("#dvUser").val();
				
			//	$(this).colorbox({
			//	     iframe:true, 
			//	     width:"100%", 
			//	     height:"100%", 
			//	     href: url + params
			//    });
				
			//});
			
			$(document).ready(function() {	
			$("#olvidasteClave").fancybox({ 
		    'onStart'   : function() {
					var url = "/vseg-paris/cambio-clave/recuperar-clave.do";
					var params = "?rut=" + $("#rutUser").val();
						params += "&dv=" + $("#dvUser").val();
		     $("#olvidasteClave").attr("href",url+params);
		     
		    },
		    'width'    : 440,
		    'height'   : 240,
		    'autoScale'   : false,
		    'transitionIn'  : 'none',
		    'transitionOut'  : 'none',
		    'type'    : 'iframe',
		    'scrolling'  : 'no' 
		  });
			});
		</script>
<!-- FIN REGISTRO -->

<%@ include file="cotizador-error-login.jsp" %>
	

<!-- INICIO USUARIO LOGEADO -->
</logic:notPresent>
<logic:present name="usuario">
<div id="estas-registrado2">
  <div class="persnalizado-paso1">
    <div class="nombre-log-paso1">�Necesitas modificar tus datos?</div>
  </div>
  <div class="actualizar-datos-paso1">&gt; Actualizalos <span id="actualizar" name="actualizar"
					style="text-decoration: underline; cursor: pointer;">aqu�</span></div>
</div>
</logic:present>
<!-- FIN USUARIO LOGEADO -->