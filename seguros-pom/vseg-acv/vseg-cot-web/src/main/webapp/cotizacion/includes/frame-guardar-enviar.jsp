<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="../../js/jquery/jquery-1.4.2.js"></script>
<script type="text/javascript">

$(document).ready(function(){

	$("#enviar").val(parent.$("#enviar").val());
	$("#idProducto").val(parent.$("#idProducto").val());
	$("#idPlan").val(parent.$("#idPlan").val());
	$("#nombrePlan").val(parent.$("#nombrePlan").val());
	$("#primaPlanPesos").val(parent.$("#primaPlanPesos").val());
	$("#primaPlanUF").val(parent.$("#primaPlanUF").val());
	$("#primaAnualPesos").val(parent.$("#primaAnualPesos").val());
	$("#primaAnualUF").val(parent.$("#primaAnualUF").val());
	
	$("#guardarCotizacion").submit();
	
});	
	
</script>

</head>
<body>

<html:form styleId="guardarCotizacion" action="/guardar-cotizacion" method="post" target="_self">

<html:hidden property="datos(enviar)" styleId="enviar"/>

<html:hidden property="datos(idProducto)" styleId="idProducto"/>

<html:hidden property="datos(idPlan)" styleId="idPlan"/>
<html:hidden property="datos(nombrePlan)" styleId="nombrePlan"/>

<html:hidden property="datos(primaPlanPesos)" styleId="primaPlanPesos"/>
<html:hidden property="datos(primaPlanUF)" styleId="primaPlanUF"/>

<html:hidden property="datos(primaAnualPesos)" styleId="primaAnualPesos"/>
<html:hidden property="datos(primaAnualUF)" styleId="primaAnualUF"/>
<html:hidden property="datos(enviar)" styleId="enviar"/>

</html:form>
</body>
</html>

<bean:define id="googleAnalytics" value="true"></bean:define>