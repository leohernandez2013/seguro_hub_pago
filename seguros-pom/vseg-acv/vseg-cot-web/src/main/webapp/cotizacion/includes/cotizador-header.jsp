<!-- INICIO HEADER -->
<!-- Static navbar -->
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>
<%@page import="cl.tinet.common.seguridad.servlet.AutenticacionServlet"%>

<%@include file="index-error-login.jsp" %>

<logic:present name="cl.tinet.common.seguridad.USUARIO_CONECTADO">
	<script type="text/javascript">
		console.log('USUARIO_CONECTADO');
		<bean:define id="usuario" name="<%=SeguridadUtil.USUARIO_CONECTADO_KEY %>" scope="session" type="UsuarioExterno" />
	</script>
</logic:present>
	
<script type="text/javascript">	
	function isRutKey(e){
		var unicode=e.charCode? e.charCode : e.keyCode;
		if (unicode!==8&&unicode!==107&&unicode!==75){ 
			if (unicode<48||unicode>57) 
				return false;
		}
	}
	function autocompletarrut(input) {
		if(input && input.value && input.value.length && input.value.length > 1) {
			input.value = input.value.replace("-","");
			var value = input.value.substring(0, input.value.length -1);
			var digit = input.value.substring(input.value.length -1, input.value.length);
			input.value = value + "-" + digit;
		}
	}
	
	function mostrarLogin(){
        $("#mostrarLogin").slideToggle();
	}
	
	function LogInCliente(idValidate, idForm) {
		//window.parent.$("html, body").animate({scrollTop:0}, 'fast');
		var form = document.getElementById(idForm);
		
		console.log(form.username.value);
		console.log(form.password.value);

		if (!validateInputLogIn(idValidate)) {
			return false;
		}else{			
			var url = ".."+window.location.pathname;
			var variables = window.location.search;
			var urlCompleta = url+variables;			
			$('input:hidden[name="cl.tinet.common.seguridad.servlet.URI_EXITO"]').val(urlCompleta);
			$('input:hidden[name="cl.tinet.common.seguridad.servlet.URI_FRACASO"]').val(urlCompleta);
		
			form.username.value = form.rutlogin.value ;
			form.password.value = form.clavelogin.value;
			$(form).submit();
			
		}
	}
	function validateInputLogIn(id) {
		validacionInput = 0;
		console.log('in validate input: ' + id);
		$('#'+id+'').find(":input:not(:hidden,:button,:submit)").each(function(){
			console.log('testing ' + this.id);
			var optional = $(this).hasClass("optional");
			if(optional==false) {
				console.log("Entro Optional");
				$(this).css("border-color","#cecece");
				if(!this.value) {
					$("#incomplete-text-"+id).html("Por favor complete el formulario correctamente");
					$("#incomplete-"+id).show("fast");
					$(this).css("border-color","red");
					$(this).focus();
					console.log(this.id + ' est� vac�o'); 
					validacionInput = 1;
					return false;
				} 
			}	
		});
		if(validacionInput == 0) {
			$("#incomplete-text-"+id).html("");
			$("#incomplete-"+id).hide("fast");
			return true;
		}else{
			return false;
		}
	}
	function actionMenu(event,link) {
		winWidth = $(window).width()
		element = event.target;
		if (winWidth <= 992) {
		event.preventDefault();
		$(element).toggleClass('is-active');
		$(element).parent().find('.c-nav__sub').toggle();
		}else{
		window.location.href=link;
		}
	}
</script>

<script src="/cotizador/js/Revisa_Sesion_Expirada.js" type="text/javascript"></script>

<!-- INICIO ModalSesionExpirada -->
	  <div class="modal fade" id="ModalSesionExpirada" role="dialog">
	  </div> 
<!-- fin ModalSesionExpirada -->	
<header class="c-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <div class="c-header__link"><a href="/vseg-paris/index.jsp"><img class="c-header__brand" src="/vseg-paris/img/seguros_cencosud-brand.png"></a></div>
            <logic:notEmpty name="usuario">
					<li>
						<a href="/vseg-paris/secure/inicio/cotizaciones.do" style="font-weight: bold;">
							<!--img src="/vseg-paris/img/login.png" alt="login" /-->
							<!--bean:write name="usuario" property="usuarioExterno.nombre" /-->
							Mi cuenta
						</a>
					</li>
					<li>
						<a href="/vseg-paris/autenticacion/cerrarSession">
							<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Cerrar
						</a>
					</li>
				</logic:notEmpty>
			<logic:notPresent name="usuario">
            <div class="c-header__sesion"><!--<a class="c-header__btn c-header__btn--login" href="#loginModal">Iniciar Sesi�n</a>-->
            <a class="c-header__btn c-header__btn--help" href="/vseg-paris/html/preguntas_frecuentes.html"">Te ayudamos</a></div>
            </logic:notPresent>
            
            <div class="c-header__service"><span>Servicio al cliente</span><a href="tel:+5626005005000">600 500 5000</a></div>
            <div class="c-header__toogle"><a class="c-header__menu" id="menu-mobile" href="#" state="0"></a></div>
        	<div class="c-header__mobile-buttons"<a class="c-header__btn-mobile c-header__btn-mobile--call" href="tel:6005005000"><i></i></a>
            <logic:notPresent name="usuario"><!--<a class="c-header__btn-mobile c-header__btn-mobile--user" href="#loginModal"><i></i></a>--></logic:notPresent>
            </div>
          </div>
        </div>
      </div>
    </header>
    <nav class="c-nav">
      <ul class="c-nav__items">
        <li class="c-nav__item--closeMenu u-hidemobile"><a id="close_menu" href="#"><i class="mx-cancel"></i></a></li>
        <li class="c-nav__item"><a class="c-nav__link" href="/vseg-paris/index.jsp">Inicio</a></li>
        <li class="c-nav__item"><a class="c-nav__link" href="#" onclick="actionMenu(event,'/vseg-paris/desplegar-pagina-intermedia-vehiculos.do?idRama=1');">Veh�culo<i class="mx-chevron-down"></i></a>
          <ul class="c-nav__sub">
            <li class="c-nav__subitem"><a class="c-nav__link" href="/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=22&idTipo=1">Seguro Auto Full Cobertura</a></li>
            <li class="c-nav__subitem"><a class="c-nav__link" href="/vseg-paris/html/AutoPlay.html">Full Cobertura Auto Play</a></li>
            <li class="c-nav__subitem"><a class="c-nav__link" href="/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=21&idTipo=1">P�rdida Total</a></li>
            <li class="c-nav__subitem"><a class="c-nav__link" href="/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=36">Seguro Obligatorio Mercosur</a></li>
            <li class="c-nav__subitem"><a class="c-nav__link" href="https://www.seguroscencosud.cl/soap/home.do">SOAP</a></li>
          </ul>
        </li>
        <li class="c-nav__item"><a class="c-nav__link " href="#" onclick="window.location.href='https://www.seguroscencosud.cl/soap/home.do'">Soap</a></li>
        <li class="c-nav__item"><a class="c-nav__link " href="#" onclick="actionMenu(event,'/vseg-paris/desplegar-pagina-intermedia-fraude.do?idRama=8');">Moto<i class="mx-chevron-down"></i></a>
        <ul class="c-nav__sub">
            <li class="c-nav__subitem"><a class="c-nav__link" href="/vseg-paris/desplegar-ficha.do?idRama=8&idSubcategoria=239">Seguro Obligatorio Mercosur</a></li>
        </ul>
        </li>
        <li class="c-nav__item"><a class="c-nav__link " href="#" onclick="actionMenu(event,'/vseg-paris/desplegar-pagina-intermedia-fraude.do?idRama=6');">Viajes<i class="mx-chevron-down"></i></a>
          <ul class="c-nav__sub">
            <li class="c-nav__subitem"><a class="c-nav__link" href="/vseg-paris/desplegar-ficha.do?idRama=6&idSubcategoria=221">Seguro de viajes</a></li>
            <li class="c-nav__subitem"><a class="c-nav__link" href="/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=36">Seguro Obligatorio Mercosur  </a></li>
          </ul>
        </li>
        <li class="c-nav__item"><a class="c-nav__link" href="#" onclick="actionMenu(event,'/vseg-paris/desplegar-pagina-intermedia-vida.do?idRama=3');">Vida y Salud<i class="mx-chevron-down"></i></a>
        	<ul class="c-nav__sub">
                  <!--li><a href="/vseg-paris/desplegar-pagina-intermedia-vida.do?idRama=3">Vida y Salud Home</a></li-->
				  <!--li><a href="/vseg-paris/desplegar-ficha.do?idRama=3&idSubcategoria=29">Seguro de Accidentes</a></li--> 
                  <!--li><a href="/vseg-paris/desplegar-ficha.do?idRama=7&idSubcategoria=162">AP con devoluci�n</a></li>
                  <li><a href="/vseg-paris/desplegar-ficha.do?idRama=7&idSubcategoria=161">Hospitalizaci�n con devoluci�n</a></li>
                  <li><a href="/vseg-paris/desplegar-ficha.do?idRama=7&idSubcategoria=164">Oncologico con devoluci�n</a></li-->
                  <!--li><a href="/vseg-paris/desplegar-ficha.do?idRama=7&idSubcategoria=165&VP=1">Catastr�fico con devoluci�n</a></li-->
                  <!--<li><a href="/vseg-paris/desplegar-ficha.do?idRama=6&idSubcategoria=140">Seguro Mascota</a></li>-->
                  <li class="c-nav__subitem"><a class="c-nav__link" href="/vseg-paris/desplegar-ficha.do?idRama=6&idSubcategoria=140">Seguro Mascota</a></li>
                  <!--<li class="c-nav__subitem"><a class="c-nav__link" href="/vseg-paris/desplegar-ficha.do?idRama=3&idSubcategoria=300">Seguro Vida con Ahorro</a></li>-->
            </ul>
        </li>
        <li class="c-nav__item"><a class="c-nav__link" href="#" onclick="actionMenu(event,'/vseg-paris/desplegar-pagina-intermedia-hogar.do?idRama=2');">Hogar<i class="mx-chevron-down"></i></a>
          <ul class="c-nav__sub">
            <li class="c-nav__subitem"><a class="c-nav__link" href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=220">Hogar vacaciones</a></li>
            <li class="c-nav__subitem"><a class="c-nav__link" href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=24">Incendio + Sismo/Estructura</a></li>
            <li class="c-nav__subitem"><a class="c-nav__link" href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=43">Incendio + Sismo/Contenido</a></li>
            <li class="c-nav__subitem"><a class="c-nav__link" href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=44">Incendio + Robo</a></li>
            <li class="c-nav__subitem"><a class="c-nav__link" href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=25">Incendio + Robo + Sismo</a></li>
            <li class="c-nav__subitem"><a class="c-nav__link" href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=26">Hogar Premio a la permanencia</a></li>
            <li class="c-nav__subitem"><a class="c-nav__link" href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=201">Hogar Full Asistencia</a></li>
            <li class="c-nav__subitem"><a class="c-nav__link" href="/vseg-paris/desplegar-ficha.do?idRama=6&idSubcategoria=140">Seguro Mascota</a></li>
          </ul>
        </li>
        <li class="c-nav__item"><a class="c-nav__link" href="#" onclick="actionMenu(event,'/vseg-paris/desplegar-pagina-intermedia-fraude.do?idRama=5');">Fraude<i class="mx-chevron-down"></i></a>
          <ul class="c-nav__sub">
            <li class="c-nav__subitem"><a class="c-nav__link" href="/vseg-paris/desplegar-ficha.do?idRama=5&idSubcategoria=139">Seguro fraude</a></li>
            <li class="c-nav__subitem"><a class="c-nav__link" href="/vseg-paris/desplegar-ficha.do?idRama=5&idSubcategoria=159">Fraude con Premio a la permanencia</a></li>
          </ul>
        </li>
      </ul>
    </nav>
   <div class="remodal c-modal" data-remodal-id="loginModal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">   
        <button class="remodal-close" data-remodal-action="close" aria-label="Close">  </button>
        <div class="c-modal__wrap">
          <div class="c-modal__modalHead">
            <div class="c-modal__modalHead-inner"><i class="o-icon">
                <svg width="20px" height="23px" viewbox="0 0 20 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="iPhone-7-Copy" transform="translate(-115.000000, -119.000000)" fill="#E8772A">
                      <g id="user" transform="translate(115.000000, 119.000000)">
                        <g id="Group-4">
                          <path id="Shape" d="M19.8691499,18.2666667 L19.8691499,18.252381 C19.8691499,18.2142857 19.8643123,18.1761905 19.8643123,18.1333333 C19.835287,17.1904762 19.7723988,14.9857143 17.6729005,14.2809524 C17.6583878,14.2761905 17.6390376,14.2714286 17.6245249,14.2666667 C15.4427882,13.7190476 13.6287055,12.4809524 13.6093553,12.4666667 C13.3142645,12.2619048 12.90791,12.3333333 12.6998952,12.6238095 C12.4918804,12.9142857 12.5644437,13.3142857 12.8595345,13.5190476 C12.9417729,13.5761905 14.8671193,14.8952381 17.2762211,15.5047619 C18.4033711,15.9 18.5291475,17.0857143 18.5630104,18.1714286 C18.5630104,18.2142857 18.5630104,18.252381 18.5678479,18.2904762 C18.5726855,18.7190476 18.5436601,19.3809524 18.4662593,19.7619048 C17.6825756,20.2 14.6107289,21.7142857 9.93765202,21.7142857 C5.28392531,21.7142857 2.19272846,20.1952381 1.4042072,19.7571429 C1.32680634,19.3761905 1.29294347,18.7142857 1.30261857,18.2857143 C1.30261857,18.247619 1.30745613,18.2095238 1.30745613,18.1666667 C1.341319,17.0809524 1.4670954,15.8952381 2.59424543,15.5 C5.00334719,14.8904762 6.92869359,13.5666667 7.010932,13.5142857 C7.30602278,13.3095238 7.37858609,12.9095238 7.17057127,12.6190476 C6.96255646,12.3285714 6.55620195,12.2571429 6.26111117,12.4619048 C6.24176095,12.4761905 4.4373534,13.7142857 2.24594156,14.2619048 C2.22659134,14.2666667 2.21207868,14.2714286 2.19756602,14.2761905 C0.0980676904,14.9857143 0.0351794916,17.1904762 0.00615416912,18.1285714 C0.00615416912,18.1714286 0.00615416912,18.2095238 0.00131661537,18.247619 L0.00131661537,18.2619048 C-0.00352093838,18.5095238 -0.00835849214,19.7809524 0.248031857,20.4190476 C0.296407394,20.5428571 0.383483362,20.647619 0.499584652,20.7190476 C0.644711264,20.8142857 4.12291241,22.9952381 9.94248957,22.9952381 C15.7620667,22.9952381 19.2402679,20.8095238 19.3853945,20.7190476 C19.4966582,20.647619 19.5885718,20.5428571 19.6369473,20.4190476 C19.878825,19.7857143 19.8739874,18.5142857 19.8691499,18.2666667 Z" fill-rule="nonzero"></path>
                          <path id="Shape" d="M9.85057605,12.3904762 L9.92797691,12.3904762 L9.95700223,12.3904762 L10.0053778,12.3904762 C11.422781,12.3666667 12.5692813,11.8761905 13.4158532,10.9380952 C15.2783114,8.87142857 14.9687079,5.32857143 14.934845,4.99047619 C14.8139062,2.45238095 13.5948427,1.23809524 12.5886315,0.671428571 C11.8388106,0.247619048 10.9632134,0.019047619 9.98602756,0 L9.95216468,0 L9.93281447,0 L9.90378914,0 C9.36682068,0 8.31223396,0.0857142857 7.30118523,0.652380952 C6.28529894,1.21904762 5.04688518,2.43333333 4.92594633,4.99047619 C4.89208346,5.32857143 4.58248002,8.87142857 6.44493821,10.9380952 C7.28667256,11.8761905 8.4331728,12.3666667 9.85057605,12.3904762 Z M6.21757318,5.10952381 C6.21757318,5.0952381 6.22241074,5.08095238 6.22241074,5.07142857 C6.38205001,1.65714286 8.84436487,1.29047619 9.89895159,1.29047619 L9.9183018,1.29047619 L9.95700223,1.29047619 C11.2631417,1.31904762 13.4835789,1.84285714 13.6335431,5.07142857 C13.6335431,5.08571429 13.6335431,5.1 13.6383806,5.10952381 C13.6432182,5.14285714 13.981847,8.38095238 12.4435049,10.0857143 C11.8339731,10.7619048 11.0212641,11.0952381 9.95216468,11.1047619 L9.92797691,11.1047619 L9.92797691,11.1047619 L9.90378914,11.1047619 C8.83952732,11.0952381 8.02198073,10.7619048 7.41728652,10.0857143 C5.88378198,8.39047619 6.21273563,5.13809524 6.21757318,5.10952381 Z" fill-rule="nonzero"></path>
                        </g>
                      </g>
                    </g>
                  </g>
                </svg></i>
              <h1>INICIO SESI�N</h1>
            </div>
          </div>
          <div class="c-modal__modalBody">
          <div id="formularioLogin">
            <form class="o-form--loginForm" id="login" action="/vseg-paris/Login" method="post">
            <input type="hidden" value="/index.jsp" name="cl.tinet.common.seguridad.servlet.URI_EXITO">
			<input type="hidden" value="/index.jsp" name="cl.tinet.common.seguridad.servlet.URI_FRACASO">
			<input type="hidden" value="" name="username">
			<input type="hidden" value="" name="password">
              <div class="row">
                <div class="col-lg-12">
                  <div class="o-form__field">
                    <label class="o-form__label">RUT</label>
                    <input class="o-form__input" type="text" placeholder="Ej: 1234567-9" required="" name="rutlogin"  id="rutloginMobile" maxlength="12"><span class="o-form__message"></span>
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="o-form__field">
                    <label class="o-form__label">Contrase�a</label>
                    <input class="o-form__input" type="password" placeholder="*******" required="" id="clavelogin" name="clavelogin"><span class="o-form__message"></span>
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="o-form__field">
                    <button class="o-btn o-btn--primary" id="enter" type="button" onclick="javascript:LogInCliente('formularioLogIn','login');">Ingresar</button>
                  </div>
                </div>
              </div>
            </form>
            </div>
          </div>
          <div class="c-modal__modalLinks">
            <div class="row">
             <div class="col-lg-12"><a class="lostPass" href="#modalOlvidasteClave">�OLVIDASTE TU CONTRASE�A?</a><a class="lostPass" href="/vseg-paris/html/porque-registrarse.html">�Para que sirve tu clave? </a></div>
              <div class="col-lg-12"><a class="registration" href="/vseg-paris/registrocliente/registroCliente.do?rut_cliente=&dv_cliente="><i class="o-icon">
                    <svg width="18px" height="18px" viewbox="0 0 18 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="iPhone-7-Copy" transform="translate(-133.000000, -443.000000)" fill="#E8772A">
                          <g id="key" transform="translate(133.000000, 443.000000)">
                            <g id="Group-14">
                              <path id="Shape" d="M16.3993732,1.59392593 C15.3694392,0.566074074 13.9999918,0 12.5434021,0 C11.0867753,0 9.71736495,0.566074074 8.68743093,1.59392593 C7.12028041,3.15785185 6.66508454,5.48944444 7.49738969,7.50866667 L0.00612371134,14.9845926 L0,17.9574815 L5.36518763,17.9635926 L5.36518763,15.8998148 L7.43258969,15.8998148 L7.43258969,13.8366667 L9.49999175,13.8366667 L9.49999175,11.4480741 L10.4728082,10.4772593 C11.1265608,10.744037 11.8347959,10.8837778 12.543699,10.8837778 C14.0001773,10.8837778 15.3695134,10.3177778 16.3993732,9.29 C18.5255629,7.16818519 18.5255629,3.71574074 16.3993732,1.59392593 Z M15.6120866,8.50433333 C14.7925113,9.32222222 13.7027876,9.77266667 12.543699,9.77266667 C11.8707216,9.77266667 11.1988206,9.6147037 10.600701,9.31588889 L10.2417402,9.13655556 L8.38658969,10.9878519 L8.38658969,12.7255556 L6.31918763,12.7255556 L6.31918763,14.7887037 L4.25178557,14.7887037 L4.25178557,16.8512222 L1.11570309,16.8476667 L1.11859794,15.4458148 L8.84122887,7.73888889 L8.66152577,7.38066667 C7.82380206,5.71077778 8.15058557,3.701 9.47471753,2.37959259 C10.2944041,1.56159259 11.3842021,1.11111111 12.5434021,1.11111111 C13.7026021,1.11111111 14.7924,1.56159259 15.6120866,2.37959259 C17.3041608,4.06818519 17.3041608,6.81574074 15.6120866,8.50433333 Z" fill-rule="nonzero"></path>
                              <path id="Shape" d="M13.0363196,3 C12.4921258,3 11.9805546,3.21148148 11.5957258,3.59548148 C10.8014247,4.38818519 10.8014247,5.678 11.5957258,6.4707037 C11.9805175,6.8547037 12.4921258,7.06618519 13.0362825,7.06618519 C13.5804392,7.06618519 14.0920474,6.8547037 14.4768763,6.4707037 C15.2711773,5.678 15.2711773,4.38818519 14.4768763,3.59548148 C14.0920845,3.21148148 13.5804763,3 13.0363196,3 Z M13.6895897,5.685 C13.5150825,5.85914815 13.2830866,5.95507407 13.0362825,5.95507407 C12.7894784,5.95507407 12.5574825,5.85914815 12.3829753,5.68503704 C12.0227526,5.32555556 12.0227526,4.74062963 12.3829753,4.38114815 C12.5574825,4.207 12.7894784,4.11111111 13.0362825,4.11111111 C13.2830866,4.11111111 13.5150454,4.207 13.6895897,4.38114815 C14.0498124,4.74062963 14.0498124,5.32551852 13.6895897,5.685 Z" fill-rule="nonzero"></path>
                            </g>
                          </g>
                        </g>
                      </g>
                    </svg></i>REG�STRATE</a></div>
            </div>
          </div>
        </div>
      </div>
      
      <!-- INICIO ModalRecuperarClave -->

	 <div class="remodal c-modal" data-remodal-id="modalOlvidasteClave" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">   
        <button class="remodal-close" data-remodal-action="close" aria-label="Close">  </button>
        <div class="c-modal__wrap">
          <div class="c-modal__modalHead">
            <div class="c-modal__modalHead-inner" style="max-width: 200px">
              <h1>�Olvidaste tu Clave?</h1>
            </div>
          </div>
          <div class="c-modal__modalBody">
            	<iframe class="" frameborder="0" src="/vseg-paris/recuperar-clave.do?rut=&dv=" id="cargaOlvidasteClave" width="100%" height="250px">
				</iframe>
          </div>
        </div>
      </div>
	  

<!-- INICIO ModalSesionExpirada -->
	  <div class="modal fade" id="ModalSesionExpirada" role="dialog">
	  </div> 
<!-- fin ModalSesionExpirada -->	  
		<div class="modal fade" id="modalCatastrofico" role="dialog">
			<div class="modal-dialog modal-md">
			  <div class="modal-content">
				<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
					<iframe class="" frameborder="0" id="catastrofico" width="100%" height="500px">
					</iframe>	
				</div>
			  </div>
			</div>
		</div>
<!-- FIN HEADER --> 
<!-- INICIO Login Mobile-->
<div id="mostrarLogin" style="padding-left: 30px; padding-right: 30px; padding-top: 100px; display: none;" class="row">
	<div class="visible-xs">
		<div id="formularioLogInMobile">
			<form method="post" id="formMobile" class="loginForm" action="/vseg-paris/Login" style="padding-left:10px; padding-right:10px;">
			
				<input type="hidden" value="" name="cl.tinet.common.seguridad.servlet.URI_EXITO">
				<input type="hidden" value="" name="cl.tinet.common.seguridad.servlet.URI_FRACASO">
				<input type="hidden" value="" name="username">
				<input type="hidden" value="" name="password">
				<div>
					<label for="username">Rut</label>
					<input id="rutlogin" name="rutlogin" onkeypress="return isRutKey(event)" onkeyup="autocompletarrut(this)" style="font-size:15px;" 
						class="form-control" value=""  maxlength="10" title="username" tabindex="4" type="text" placeholder="12345678-9">
				</div>
				<div>
					<label for="password">Clave</label>
					<input id="clavelogin" name="clavelogin" class="form-control" maxlength="6" value="" title="password" 
						tabindex="5" type="password" placeholder="******" style="font-size:15px;" >
				</div>
				<div>
					<button type="button" class="btn btn-primary top15" onclick="javascript:LogInCliente('formularioLogInMobile','formMobile');">Ingresar</button>
				</div>
				<div class="campobligatorio_body">
					<div style="display:none" id="mintento0" class="rc_campobligatorio" ><p style="color:#f07d00;">La Clave Es Incorrecta.</p><p style="color:#f07d00;">Le Quedan 3 Intentos.</p></div>
					<div style="display:none" id="mintento1" class="rc_campobligatorio" ><p style="color:#f07d00;">La Clave Es Incorrecta.</p><p style="color:#f07d00;">Le Quedan 2 Intentos.</p></div>
					<div style="display:none" id="mintento2" class="rc_campobligatorio" ><p style="color:#f07d00;">La Clave Es Incorrecta.</p><p style="color:#f07d00;">Le Queda 1 Intento.</p></div>
					<div style="display:none" id="mintento3" class="rc_campobligatorio" ><p style="color:#f07d00;">La Clave Ingresada No Es Correcta.</p> <p style="color:#f07d00;"> <br /> Su Clave Ha Sido Bloqueada.</p></div>
					<div style="display:none" id="mintento4" class="rc_campobligatorio" ><p style="color:#f07d00;">Su Clave Ha Sido Bloqueada.</p>  </div> 
					<div style="display:none" id="mintento5" class="rc_campobligatorio" ><p style="color:#f07d00;">No Tenemos Tus Datos Registrados.</p> <p style="color:#f07d00;"><br> Te Invitamos A Registrate.</p></div>
					<div style="display:none" id="mintento6" class="rc_campobligatorio" ><p style="color:#f07d00;">El Rut Es Incorrecto. </p></div>
				</div>
				<p class="remember">
					<div id="signin_submit"><a href="#"></a></div>
				</p>
				<p class="forgot"> <a href="#" onclick="javascript:olvidasteClave();">&iquest;Olvidaste tu Clave?</a> </p>
				<p class="forgot"> <a href="/vseg-paris/html/porque-registrarse.html" id="resend_password_link">&iquest;Para que sirve tu clave?</a> </p>
				<logic:notPresent name="usuario">
					<p class="forgot">
						<a id="registroUsuario2" href="/vseg-paris/registrocliente/registroCliente.do?rut_cliente=&dv_cliente=" id="loginMobile">Registrarse</a>
					</p>
				</logic:notPresent>
			</form>
		</div>
	</div>
</div>
<!-- FIN Login Mobile-->