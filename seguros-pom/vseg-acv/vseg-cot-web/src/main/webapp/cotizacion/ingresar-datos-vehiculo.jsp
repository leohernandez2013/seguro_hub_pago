<!DOCTYPE html>
<%@ page language="java" pageEncoding="ISO-8859-1"%>
<%@page import="cl.tinet.common.struts.form.ValidableActionForm"%>
<%@page import="org.apache.struts.Globals"%>
<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>
<%@page import="cl.tinet.common.seguridad.servlet.AutenticacionServlet"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<html lang="en">
<head>

<%@ include file="./includes/cotizador-head.jsp"  %>

<script type="text/javascript">
$(function() {
   $('.datos-calle-input').attr('placeholder','Direcci�n');
   $('.datos-direccion-input').attr('placeholder','N�mero');
   $('.datos-depto-input').attr('placeholder','Dpto.');
});

function cargaFiltro(){
		    var x = document.getElementById("filtro");

			if (x.classList.contains("is-active")) {
				x.classList.remove("is-active");
			} else {
				x.classList.add("is-active");
			}
		} 

function registroRemoto(){
	var rut_cliente =  "<bean:write name='datosCotizacion' property='rutCliente'/>";
	var dv_cliente = "<bean:write name='datosCotizacion' property='dv'/>";
	var diaFechaNacimiento = "<bean:write name='datosCotizacion' property='fechaNacimiento' format='dd'/>";
	var mesFechaNacimiento = "<bean:write name='datosCotizacion' property='fechaNacimiento' format='MM'/>";
	var anoFechaNacimiento = "<bean:write name='datosCotizacion' property='fechaNacimiento' format='yyyy'/>";
	var val_sexo = "<bean:write name='datosCotizacion' property='sexo'/>";
	var val_estadoCivil= "<bean:write name='datosCotizacion' property='estadoCivil'/>";
	var val_nombre = "<bean:write name='datosCotizacion' property='nombre'/>";
	var val_apellidoP = "";
	var val_apellidoM = "";
	var val_email = "";
	var val_codigo_cel = "";
	var val_tipo_cel = "";
	var val_numero_cel = "";

	<logic:notEmpty name="datosCotizacion" property="apellidoPaterno">
		val_apellidoP = "<bean:write name='datosCotizacion' property='apellidoPaterno'/>";
	</logic:notEmpty>
	
	<logic:notEmpty name="datosCotizacion" property="apellidoMaterno">
		val_apellidoM = "<bean:write name='datosCotizacion' property='apellidoMaterno'/>";
	</logic:notEmpty>
	
	<logic:present property="email" name="datosCotizacion">
		val_email = "<bean:write name='datosCotizacion' property='email'/>";
	</logic:present>			

	<logic:present property="tipoTelefono1" name="datosCotizacion">
		val_tipo_cel = "<bean:write name='datosCotizacion' property='tipoTelefono1'/>";
	</logic:present>		
	
	<logic:present property="datos(cteCodigoTelefono1)" name="ingresarDatosVehiculo">
		val_codigo_cel = "<bean:write name='ingresarDatosVehiculo' property='datos(cteCodigoTelefono1)'/>";
	</logic:present>				
	
	<logic:present property="datos(numeroTelefono1)" name="ingresarDatosVehiculo">
		val_numero_cel = "<bean:write name='ingresarDatosVehiculo' property='datos(numeroTelefono1)'/>";
	</logic:present>				
					
	var url = '/cotizador/registroClienteRemote.do';
	var params = "?rut_cliente=" + rut_cliente;
	params += "&dv_cliente=" + dv_cliente;
	params += "&diaFechaNacimiento=" + diaFechaNacimiento;
	params += "&mesFechaNacimiento=" + mesFechaNacimiento;
	params += "&anoFechaNacimiento=" + anoFechaNacimiento;
	params += "&val_sexo=" + val_sexo;
	params += "&val_estadoCivil=" + val_estadoCivil;
	params += "&val_nombre=" + val_nombre;
	params += "&val_apellidoP=" + val_apellidoP;
	params += "&val_apellidoM=" + val_apellidoM;
	params += "&val_email=" + val_email;
	params += "&val_codigo_cel=" + val_codigo_cel;
	params += "&val_tipo_cel=" + val_tipo_cel;
	params += "&val_numero_cel=" + val_numero_cel;		

	$("#cargaRegistrate").attr('src',url+params);
	$("#modalRegistrate").modal({
	backdrop: 'static',
	show: true
	});
}
</script>
<script type="text/javascript">
function autocompletarfechaVehiculo(input) {		
	if(input && input.value && input.value.length && input.value.length >= 8) {
		var value = replaceAll(input.value, "/", "" );
		var dia = value.substring(0,2);
		var mes = value.substring(2,4);
		var anio = value.substring(4,8);
		input.value = dia + "/" + mes + "/" + anio;
	}
}

$(document).ready(function(){
	bar(0,2);
	paso2bar = 50;
	$("#inputIniVig").datepicker({
		minDate: 0,
		dateFormat: 'dd/mm/yy',
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
		'Junio', 'Julio', 'Agosto', 'Septiembre',
		'Octubre', 'Noviembre', 'Diciembre'],
		monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
		'May', 'Jun', 'Jul', 'Ago',
		'Sep', 'Oct', 'Nov', 'Dic']
	}); 
 	$("#datos.fechaNacimiento").datepicker({
		changeMonth: true,
		changeYear: true,
		showOn: "both",
		yearRange: '1936:1996',
		buttonImage: "images/calendar.gif",
		buttonImageOnly: true,
		dateFormat: 'dd-mm-yy',
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
		'Junio', 'Julio', 'Agosto', 'Septiembre',
		'Octubre', 'Noviembre', 'Diciembre'],
		monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr',
		'May', 'Jun', 'Jul', 'Ago',
		'Sep', 'Oct', 'Nov', 'Dic']
	}); 
});
</script>


<script type="text/javascript">
	$(document).ready(function(){		
		$('#resumen-content').load('/vseg-paris/html/preguntas-frecuentes-resumen-cotizacion-vehiculo.html');
		$('#resumen-banner').hide();
		$('#paso2').hide();
		if($(".divtexnovalido").length) {
			$("#fecha-invalida").show("fast");
		}
		
		<logic:present name="error-login" scope="request">
			$("#rutpaso2").addClass("novalidovida");
			$("#dvpaso2").addClass("novalidovida");
			$("#clavepaso2").addClass("novalidovida");
		</logic:present>		
		
		$("select#cteRegion").change(function() {
			$.getJSON('/cotizador/buscar-comunas.do?idRegion=' + this.value, function(data){
				$('select#comunaResidencia option').remove();
				var options = '';
				options += '<option value="">Seleccione</option>';				
				for(var i = 0; i < data.length; i++) {
					options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
				}
				$('select#cteComuna').html(options);
				$("select#cteCiudad").html('<option value="">Seleccione</option>');
			});
		});

		$("select#cteComuna").change(function() {
			$.getJSON('/cotizador/buscar-ciudades.do?idComuna=' + this.value, function(data){
				$('select#ciudadResidencia option').remove();
				var options = '';
				options += '<option value="">Seleccione</option>';
				for(var i = 0; i < data.length; i++) {
					options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
				}
				$('select#cteCiudad').html(options);
			});

		});

		$("select#dueRegion").change(function() {
			$.getJSON('/cotizador/buscar-comunas.do?idRegion=' + this.value, function(data){
				$('select#comunaResidencia option').remove();
				var options = '';
				options += '<option value="">Seleccione</option>';
				for(var i = 0; i < data.length; i++) {
					options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
				}
				$('select#dueComuna').html(options);
				$("select#dueCiudad").html('<option value="">Seleccione</option>');
			});
		});

		$("select#dueComuna").change(function() {
			$.getJSON('/cotizador/buscar-ciudades.do?idComuna=' + this.value, function(data){
				$('select#ciudadResidencia option').remove();
				var options = '';
				options += '<option value="">Seleccione</option>';
				for(var i = 0; i < data.length; i++) {
					options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
				}
				$('select#dueCiudad').html(options);
			});
		});

		$("input[name='datos(esNuevo)']").click(function(){
			try{
				if($(this).val() == 1){
					$(".texto_factura").show();
					$(".texto_inspeccion").hide();				
				}else{
					$(".texto_factura").hide();
					$(".texto_inspeccion").show();
				}
			}
			catch(ex) {
				// No existe texto provisorio.
			}
		});

		$("input[name='datos(tipoInspeccion)']").click(function(){
			try{
				if($(this).val() == 1){
					$(".domicilio").show();
				}else{
					$(".domicilio").hide();
				}
			}
			catch(ex) {
				// No existe texto provisorio.
			}
		});	
	});
</script>
<script type="text/javascript">
									var solicitaFactura = '<bean:write name="solicitaFactura"/>';
									function esNuevoCheck(boton){
										if(solicitaFactura == 1) {
											if(boton.value == 1) {
												$("#inputFactura").show("fast");
											//	$("#inpeccionVehi").hide("fast");
											}else{
											//	$("#inpeccionVehi").show("fast");
												$("#inputFactura").hide("fast");
											}
										}
									}
</script>
<script type="text/javascript">
								$(document).ready(function(){
									var $radios = $("input[name='datos(esNuevo)']");
									if($radios.is(':checked') === false) {
										$radios.filter('[value=0]').attr('checked', true);
									}
									try{
										$(".texto_factura").hide();
										$(".texto_inspeccion").show();
									}catch(ex) {
										// no existe texto.
									}
								});
</script>
</head>
<body onload="desplegarErrores()">
	<%@ include file="/google-analytics/google-analytics.jsp"%>
	<%@ include file="./includes/cotizador-header.jsp"%>
	
	<logic:present name="paso2" scope="session">
		<script type="text/javascript">
		   <bean:define id="paso2" name="paso2" scope="session"/>
		</script>
	</logic:present>
	
	<div class="container contenedor-sitio">
		<%@ include file="./includes/cotizador-breadcrumb.jsp"%>
		<!-- INICIO  CONTENIDOS -->
		<div class="row">
				<html:form action="guardar-datos-adicionales-vehiculo" method="post"
					styleId="paso2VehiculoForm" enctype="multipart/form-data">
					<div id="contenido-desplegado-cotizador">
						<html:hidden property="datos(rut)" value="" styleId="datos.rut" />
						<html:hidden property="datos(dv)" value="" styleId="datos.dv" />
						<div id="paso1">
						<main role="main">
      <div class="remodal-bg">
        <section class="container">
          <div class="row o-form o-form--standard o-form--linear o-form--small">
            <div class="col-12">
				<logic:equal name="idRama" value="1">
				      	<logic:equal name="idSubcategoria" value="22"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Full Cobertura</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="21"><h1 class="o-title o-title--primary">Cotizaci�n Seguro P�rdida Total</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="36"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Obligatorio Mercosur</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="79"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Robo Contenido</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="2">
				      	<logic:equal name="idSubcategoria" value="24"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Sismo / Estructura</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="43"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Sismo / Contenido</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="44"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Robo</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="25"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio, Robo y Sismo</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="26"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Premio a la Permanencia</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="201"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Full Asistencia</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="220"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Vacaciones</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="3">
				      	<logic:equal name="idSubcategoria" value="29"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Viaje Grupal</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="5">
				      	<logic:equal name="idSubcategoria" value="139"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Fraude</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="159"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Fraude con devoluci�n</h1></logic:equal>
			      	</logic:equal>
            		<logic:equal name="idRama" value="6">
				      	<logic:equal name="idSubcategoria" value="119"><h1 class="o-title o-title--primary">Asistencia Viaje</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="140"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Mascota</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="221"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Viaje Grupal</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="7">
						<logic:equal name="idSubcategoria" value="161"><h1 class="o-title o-title--primary">Hospitalizaci�n con devoluci�n</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="162"><h1 class="o-title o-title--primary">Accidentes Personales con devoluci�n</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="164"><h1 class="o-title o-title--primary">Oncol�gico con devoluci�n</h1></logic:equal>
			      	</logic:equal>
			      	<logic:equal name="idRama" value="8">
				      	<logic:equal name="idSubcategoria" value="239"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Obligatorio Mercosur</h1></logic:equal>
			      	</logic:equal>
			</div>
            <div class="col-12">
              <section class="o-box o-box--recruiting u-mtb50">
                <div class="o-steps">
                  <div class="o-steps__item o-steps__item--current"><span class="o-steps__milestone"><span class="o-steps__value">1</span></span>
                    <h4 class="o-steps__title">Informaci�n contratante</h4>
                  </div>
                  <div class="o-steps__item"><span class="o-steps__milestone"><span class="o-steps__value">2</span></span>
                    <h4 class="o-steps__title">Datos del seguro</h4>
                  </div>
                  <div class="o-steps__item"><span class="o-steps__milestone"><span class="o-steps__value">3</span></span>
                    <h4 class="o-steps__title">Confirmaci�n y pago</h4>
                  </div>
                </div>
                <div class="u-pad20">
                  <div class="row">
                    <div class="col-lg-8 u-mobile_second">
                      <h2 class="o-title o-title--subtitle u-mb10">Informaci�n contratante</h2>
                      <p class="o text"></p>
                      <form class="o-form o-form--standard o-form--linear o-form--small" id="form_recruiting_step1" action="">
                        <div class="row u-mt40">
                          <div class="col-lg-6">
                            <div class="o-form__field">
                              <label class="o-form__label">Nombre</label>
                              <input class="o-form__input" type="text" placeholder="Ingrese su nombre" required="" name="datos(nombre)" id="datos.nombre"><span class="o-form__line"></span><span class="o-form__message"></span>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="o-form__field">
                              <label class="o-form__label">Apellido paterno</label>
                              <input class="o-form__input" type="text" placeholder="Ingrese su apellido paterno" required="" name="datos(apellidoPaterno)" maxlength="30" size="16"
													id="datos.apellidoPaterno"><span class="o-form__line"></span><span class="o-form__message"></span>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="o-form__field">
                              <label class="o-form__label">Apellido materno</label>
                              <input class="o-form__input" type="text" placeholder="Ingrese su apellido materno" required="" name="datos(apellidoMaterno)" maxlength="30" size="16"
													id="datos.apellidoMaterno"><span class="o-form__line"></span><span class="o-form__message"></span>
                            </div>
                            <script type="text/javascript">
								$(document).ready(function(){
									<logic:present property="nombre" name="datosCotizacion">
										var val_nombre = "<bean:write name='datosCotizacion' property='nombre'/>";
										$("input[name='datos(nombre)']").val(val_nombre);							
									</logic:present>
									
									<logic:present property="apellido_paterno" name="usuario">
										var val_apellidoP = "<bean:write name='usuario' property='apellido_paterno'/>";
										$("input[name='datos(apellidoPaterno)']").val(val_apellidoP);
									</logic:present>
									
									<logic:present property="apellido_materno" name="usuario">
										var val_apellidoM = "<bean:write name='usuario' property='apellido_materno'/>";
										$("input[name='datos(apellidoMaterno)']").val(val_apellidoM);
									</logic:present>
									
									<logic:present name='patente'>
										var val_apellidoM = "<bean:write name='patente'/>";
										$("input[name='datos(patente)']").val(val_apellidoM);
									</logic:present>
								});
							</script>
                          </div>
                          <div class="col-lg-6">
                            <div class="o-form__field">
                              <label class="o-form__label">RUT</label>
                              <input class="o-form__input" type="text" placeholder="Ej: 1234567-9" required="" name="datos(rutCompleto)" maxlength="9"
												id="datos.rutcompleto" onkeypress="return isRutKey(event)"
												onkeyup="autocompletarrut(this)"><span class="o-form__line"></span><span class="o-form__message"></span>
							
                            </div>
                            <script type="text/javascript">
										$(document).ready(function(){
											<logic:present property="rutCliente" name="datosCotizacion">
												var val_rut = "<bean:write name='datosCotizacion' property='rutCliente'/>";
												var val_dv = "<bean:write name='datosCotizacion' property='dv'/>";
												$("input[name='datos(rut)']").val(val_rut);
												$("input[name='datos(dv)']").val(val_dv);
												$("input[name='datos(rutCompleto)']").val(val_rut+'-'+val_dv);
												$("input[name='datos(rutCompleto)']").attr('disabled', 'disabled');
											</logic:present>
										});
							</script>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="o-form__field">
                              <label class="o-form__label">Fecha de Nacimiento</label>
                              <input class="o-form__input datos-fecha-input" type="text" placeholder="" required="" name="datos(fechaNacimiento)" size="16"
												id="datos.fechaNacimiento" onBlur="autocompletarfecha(this);"
												onFocus="autocompletarfecha(this);"
												onkeypress="autocompletarfechaguion(this);"	
												maxlength="10"><span class="o-form__line"></span><span class="o-form__message"></span>
												<script type="text/javascript">
											$(document).ready(function(){
												<logic:present property="fechaNacimiento" name="datosCotizacion">
													var val_fechaNacimiento = "<bean:write name='datosCotizacion' property='fechaNacimiento' format="dd-MM-yyyy"/>";
													$("input[name='datos(fechaNacimiento)']").val(val_fechaNacimiento);
													$("input[name='datos(fechaNacimiento)']").attr('disabled', 'disabled');
												</logic:present>
											});
										</script>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="o-form__field o-form__field--select">
                              <label class="o-form__label">Estado Civil</label>
                              <html:select property="datos(estadoCivil)"
												styleClass="o-form__select"
												styleId="datos.estadoCivil">
												<html:option value="">Seleccione</html:option>
												<html:options collection="estadosCiviles" property="id"
													labelProperty="descripcion" />
											</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
							<script type="text/javascript">
										$(document).ready(function(){
											<logic:present property="estadoCivil" name="datosCotizacion">
												var val_estadoCivil= "<bean:write name='datosCotizacion' property='estadoCivil'/>";
												$("select[name='datos(estadoCivil)']").val(val_estadoCivil);
												$("select[name='datos(estadoCivil)']").attr('disabled', 'disabled');
											</logic:present>
										});
							</script>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="o-form__field o-form__field--select">
                              <label class="o-form__label">Genero</label>
                              <select class="o-form__select" name="datos(sexo)">
												<option value="">Seleccione</option>
												<option value="F">Femenino</option>
												<option value="M">Masculino</option>
							  </select><span class="o-form__line"></span><span class="o-form__message"></span>
								<script type="text/javascript">
										$(document).ready(function(){
											<logic:present property="sexo" name="datosCotizacion">
												var val_sexo = "<bean:write name='datosCotizacion' property='sexo'/>";
												$("select[name='datos(sexo)']").attr('disabled', 'disabled');
												$("select[name='datos(sexo)']").val(val_sexo);
											</logic:present>
										});
								</script>
								
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="o-form__field">
                            <script type="text/javascript">
										$(document).ready(function(){
											<logic:present property="email" name="datosCotizacion">
												var val_email = "<bean:write name='datosCotizacion' property='email'/>";
												$("input[name='datos(email)']").val(val_email);
											</logic:present>
										});
							</script>
                              <label class="o-form__label">Email</label>
                              <input type="email" required class="o-form__input datos-email-input"
												placeholder="Email" name="datos(email)"
												maxlength="50" id="datos.email" /><span class="o-form__line"></span><span class="o-form__message"></span>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="o-form__field o-form__field--select">
                              <label class="o-form__label">Tel�fono</label>
                              <div class="row">
                                <div class="col-4">
                                  <html:select property="datos(cteTipoTelefono1)"
													styleClass="o-form__select" styleId="datos.telefono1_1">
													<html:option value="">Seleccione</html:option>
													<html:options collection="tipoTelefono" property="valor"
														labelProperty="descripcion" />
								  </html:select><span class="o-form__line"></span>
                                </div>
                                <div class="col-3">
								<html:select property="datos(cteCodigoTelefono1)"
													styleClass="o-form__select" styleId="datos.telefono1_2">
													<html:option value="">Seleccione</html:option>
													<html:options collection="codigoArea" property="valor"
														labelProperty="descripcion" />
								</html:select><span class="o-form__line"></span>
								<script type="text/javascript">
											$(document).ready(function(){
												<logic:present property="datos(cteCodigoTelefono1)" name="ingresarDatosVehiculo">
													var val_codigo_cel = "<bean:write name='ingresarDatosVehiculo' property='datos(cteCodigoTelefono1)'/>";
													$("select[name='datos(cteCodigoTelefono1)']").val(val_codigo_cel);
												</logic:present>
											});
								</script>
                                </div>
                                <div class="col-5">
								<input class="o-form__input" placeholder="Tel�fono"
													pattern="[0-9]*" name="datos(cteNumeroTelefono1)"
													maxlength="8" size="8" id="datos.telefono1_3"
													onkeypress="return isNumberKey(event)"><span class="o-form__line"></span>
                                </div>
                                <script type="text/javascript">
													$(document).ready(function(){
														<logic:present property="numeroTelefono1" name="datosCotizacion">
															var val_num = "<bean:write name='datosCotizacion' property='numeroTelefono1'/>";
															if(val_num.length>0){
																val_num = val_num.substring(2);
															}
															$("input[name='datos(cteNumeroTelefono1)']").val(val_num);
														</logic:present>	
													});
								</script>
                              </div><span class="o-form__message" id="phone--recruiting"></span>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="o-form__field o-form__field--select">
                              <label class="o-form__label">Regi�n</label>
                              <html:select property="datos(cteRegion)"
												styleClass="o-form__select" styleId="cteRegion">
												<html:option value="">Seleccione</html:option>
												<html:options collection="regiones" property="id"
													labelProperty="descripcion" />
							  </html:select><span class="o-form__line"></span><span class="o-form__message"></span>
							  <script type="text/javascript">
								regionToRoman('cteRegion');
							</script>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="o-form__field o-form__field--select">
                              <label class="o-form__label">Comuna</label>
                              <html:select property="datos(cteComuna)"
												styleClass="o-form__select" styleId="cteComuna">
												<html:option value="">Seleccione</html:option>
							  </html:select><span class="o-form__line"></span><span class="o-form__message"></span>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="o-form__field o-form__field--select">
                              <label class="o-form__label">Ciudad</label>
                              <html:select property="datos(cteCiudad)"
												styleClass="o-form__select" styleId="cteCiudad">
												<html:option value="">Seleccione</html:option>
							  </html:select><span class="o-form__line"></span><span class="o-form__message"></span>
                            </div>
                          </div>
                          <script type="text/javascript">
								$(document).ready(function(){
									var valor = "";
									if($("#cteRegion").val() != null && $("#cteRegion").val() != "" ) {
										$.getJSON('/cotizador/buscar-comunas.do?idRegion=' + $("#cteRegion").val(), function(data){
											$('select#cteComuna option').remove();
											var options = '';
											options += '<option value="">Seleccione</option>';
											<logic:present property="datos(cteComuna)" name="ingresarDatosVehiculo">
												valor = "<bean:write name='ingresarDatosVehiculo' property='datos(cteComuna)'/>";
											</logic:present>
											for(var i = 0; i < data.length; i++) {
												if(valor != '' && valor == data[i].id) {
													options += '<option value="' + data[i].id + '" selected="selected">' + data[i].descripcion + '</option>';						
												} else {
													options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
												}
											}
											$('select#cteComuna').html(options);
											recargarCiudadesPaso2Hogar(valor);
										});
									}
			
									//Se envia formulario al presionar enter.
									$('#clavepaso2').keypress(function(e){
										if(e.which == 13){
											e.preventDefault();
											submitPaso2Vehiculo();
											return false;
										}
									});
								});
			
								function recargarCiudadesPaso2Hogar(valor) {
									$.getJSON('/cotizador/buscar-ciudades.do?idComuna=' + valor, function(data){
										var valorciudad = '';
										$('select#cteCiudad option').remove();
										var options = '';
										options += '<option value="">Seleccione</option>';
										<logic:present property="datos(cteCiudad)" name="ingresarDatosVehiculo">
											var valorciudad = '<bean:write name='ingresarDatosVehiculo' property='datos(cteCiudad)'/>';
										</logic:present>
										for(var i = 0; i < data.length; i++) {
											if(valorciudad!='' && valorciudad == data[i].id) {
												options += '<option value="' + data[i].id + '" selected="selected">' + data[i].descripcion + '</option>';
											} else {
												options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
											}
										}
			
										$('select#cteCiudad').html(options);
			
									});
								}
							</script>
                        </div>
                        <div class="row">
						<div class="col-lg-6">
                            <div class="o-form__field">
                              <label class="o-form__label">Direcci�n</label>
                              <div class="row">
                                <div class="col-6">
                                  <logic:present name="usuario">
													<html:text property="datos(cteCalle)" 
														styleClass="o-form__input datos-calle-input" size="25"
														styleId="datos.calle1" maxlength="20" onkeypress="return validar(event)"> 
													</html:text><span class="o-form__line"></span>
								</logic:present>
								<logic:notPresent name="usuario">
													<input class="o-form__input" placeholder="Direcci�n"
													name="datos(cteCalle)" maxlength="25" size="20"
													id="datos.calle1" onkeypress="return validar(event)"><span class="o-form__line"></span>
								</logic:notPresent>
                                </div>
                                <div class="col-3">
                                  <logic:notPresent name="usuario">
													<input class="o-form__input" placeholder="N�mero"
														name="datos(cteNumeroCalle)" maxlength="5" size="5"
														id="datos.nro1" onkeypress="return isNumberKey(event)"><span class="o-form__line"></span>
								</logic:notPresent>
								<logic:present name="usuario">
													<html:text property="datos(cteNumeroCalle)" title="N�mero" 
													styleClass="o-form__input datos-direccion-input" size="5"
													styleId="datos.nro1" maxlength="5"
													onkeypress="return isNumberKey(event)">
													</html:text><span class="o-form__line"></span>
								</logic:present>
                                </div>
                                <div class="col-3">
                                  <logic:notPresent name="usuario">
													<input class="o-form__input optional" placeholder="Dpto."
														name="datos(cteNumeroDepto)" maxlength="5" size="10"
														id="datos.depto1" onkeypress="return isNumberKey(event)"><span class="o-form__line"></span>
								</logic:notPresent>
								<logic:present name="usuario">
													<html:text property="datos(cteNumeroDepto)"
													styleClass="o-form__input datos-depto-input optional"
													size="10" styleId="datos.depto1" maxlength="5"
													onkeypress="return isNumberKey(event)">
													</html:text><span class="o-form__line"></span>
								</logic:present>
                                </div>
                              </div><span class="o-form__message" id="direction--recruiting"></span>
                            </div>
                          </div>
						</div>
						<logic:equal value="false" name="datosCotizacion"
								property="clienteEsAsegurado">
						<h2 class="o-title o-title--subtitle u-mb10">DATOS DE CONTACTO DUE�O DEL VEH�CULO</h2>
						<div class="row">
                          <div class="col-lg-6">
                            <div class="o-form__field">
                              <label class="o-form__label">Tel�fono</label>
                              <div class="row">
                                <div class="col-4">
                                 <div class="o-form__field o-form__field--select">
								  <html:select property="datos(dueTipoTelefono1)"
														styleClass="o-form__select" styleId="dueTipoTelefono1">
														<html:option value="">Seleccione</html:option>
														<html:options collection="tipoTelefono" property="valor"
															labelProperty="descripcion" />
								</html:select><span class="o-form__line"></span>
								</div>
                                </div>
                                <div class="col-3">
                                <div class="o-form__field o-form__field--select">
								<html:select property="datos(dueCodigoTelefono1)"
														styleClass="o-form__select" styleId="dueCodigoTelefono1">
														<html:option value="">Seleccione</html:option>
														<html:options collection="codigoArea" property="valor"
															labelProperty="descripcion" />
								</html:select><span class="o-form__line"></span>
								</div>
								<script type="text/javascript">
													$(document).ready(function(){
														<logic:present property="datos(dueCodigoTelefono1)" name="ingresarDatosVehiculo">
															var val_codigo_cel = "<bean:write name='ingresarDatosVehiculo' property='datos(dueCodigoTelefono1)'/>";
															$("select[name='datos(dueCodigoTelefono1)']").val(val_codigo_cel);
														</logic:present>
													});
								</script>
                                </div>
                                <div class="col-5">
								<input class="o-form__input" placeholder="Tel�fono"
														pattern="[0-9]*" name="datos(dueNumeroTelefono1)"
														maxlength="8" size="8" id="dueNumeroTelefono1"
														onkeypress="return isNumberKey(event)"><span class="o-form__line"></span>
                                </div>
                              </div><span class="o-form__message" id="phone--recruiting"></span>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="o-form__field o-form__field--select">
                              <label class="o-form__label">Regi�n</label>
                              <html:select property="datos(dueRegion)"
													styleClass="o-form__select" styleId="dueRegion">
													<html:option value="">Seleccione</html:option>
													<html:options collection="regiones" property="id"
														labelProperty="descripcion" />
							</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
                            </div>
                            <script type="text/javascript">
											regionToRoman('dueRegion');
							</script>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="o-form__field o-form__field--select">
                              <label class="o-form__label">Comuna</label>
                              <html:select property="datos(dueComuna)"
													styleClass="o-form__select" styleId="dueComuna">
													<html:option value="">Seleccione</html:option>
							</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="o-form__field o-form__field--select">
                              <label class="o-form__label">Ciudad</label>
                              <html:select property="datos(dueCiudad)"
													styleClass="o-form__select" styleId="dueCiudad">
													<html:option value="">Seleccione</html:option>
							</html:select><span class="o-form__line"></span><span class="o-form__message"></span>
                            </div>
                            <script type="text/javascript">
										$(document).ready(function(){
											var valor = "";
											if($("#dueRegion").val() != null && $("#dueRegion").val() != "" ) {
												$.getJSON('/cotizador/buscar-comunas.do?idRegion=' + $("#dueRegion").val(), function(data){
													$('select#dueComuna option').remove();
													var options = '';
													options += '<option value="">Seleccione</option>';
													<logic:present property="datos(dueComuna)" name="ingresarDatosVehiculo">
														valor = "<bean:write name='ingresarDatosVehiculo' property='datos(dueComuna)'/>";
													</logic:present>
													for(var i = 0; i < data.length; i++) {
														if(valor!='' && valor==data[i].id){
															options += '<option value="' + data[i].id + '" selected="selected">' + data[i].descripcion + '</option>';
														} else {
															options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
														}
													}
													$('select#dueComuna').html(options);
													recargarCiudadesPaso2HogarDue(valor);
												});
											}
										});
				
										function recargarCiudadesPaso2HogarDue(valor) {
											$.getJSON('/cotizador/buscar-ciudades.do?idComuna=' + valor, function(data){
												var valorciudad = '';
												$('select#dueCiudad option').remove();
												var options = '';
												options += '<option value="">Seleccione</option>';
												<logic:present property="datos(dueCiudad)" name="ingresarDatosVehiculo">
													valorciudad = '<bean:write name='ingresarDatosVehiculo' property='datos(dueCiudad)'/>';
												</logic:present>
												for(var i = 0; i < data.length; i++) {
													if(valorciudad!='' && valorciudad==data[i].id) {
														options += '<option value="' + data[i].id + '" selected="selected">' + data[i].descripcion + '</option>';
													} else {
														options += '<option value="' + data[i].id + '">' + data[i].descripcion + '</option>';
													}
												}
												$('select#dueCiudad').html(options);
											});
										}
									</script>
                          </div>
                        </div>
						<div class="row">
						<div class="col-lg-6">
                            <div class="o-form__field">
                              <label class="o-form__label">Direcci�n</label>
                              <div class="row">
                                <div class="col-6">
                                  <input class="o-form__input" placeholder="Direcci�n"
														name="datos(dueCalle)" maxlength="25" size="20"
														id="dueCalle" onkeypress="return validar(event)"><span class="o-form__line"></span>
                                </div>
                                <div class="col-3">
                                  <input class="o-form__input" placeholder="N�mero"
														name="datos(dueNumeroCalle)" maxlength="5" size="5"
														id="dueNumeroCalle" onkeypress="return isNumberKey(event)"><span class="o-form__line"></span>
                                </div>
                                <div class="col-3">
                                  <input class="o-form__input optional" placeholder="Dpto."
														name="datos(dueNumeroDepto)" maxlength="5" size="10"
														id="dueNumeroDepto" onkeypress="return isNumberKey(event)"><span class="o-form__line"></span>
                                </div>
                              </div><span class="o-form__message" id="direction--recruiting"></span>
                            </div>
                          </div>
						</div>
						</logic:equal>
                        <div class="row o-actions o-actions--flex">
                          <div class="col-lg-3">
                            <div class="o-form__field is-last"><a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="/cotizador/desplegar-cotizacion.do?idRama=<bean:write name='idRama'/>&idSubcategoria=<bean:write name='idSubcategoria'/>&volverPaso1=true"><i class="mx-arrow-left"></i> Volver</a></div>
                          </div>
                          <div class="col-lg-3 offset-lg-6">
                            <div class="o-form__field is-last">
                              <button class="o-btn o-btn--primary o-btn o-btn--icon" id="send_form_recruiting_step1" type="button" onclick="switchForm(true)">Siguiente<i class="o-icon">
                                  <svg width="13px" height="14px" viewbox="0 0 13 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                      <g id="botones/orangeButton+rightArrow" transform="translate(-113.000000, -19.000000)" fill-rule="nonzero" fill="#FFFFFF">
                                        <g id="orangeButton+rightArrow">
                                          <g id="arrows" transform="translate(113.000000, 19.000000)">
                                            <path id="Shape" d="M12.6595914,6.18869213 L6.92029216,0.341285393 C6.69713064,0.113730337 6.42956532,0 6.11805938,0 C5.81235867,0 5.54791211,0.113730337 5.32456532,0.341285393 L4.66341805,1.01492135 C4.44004038,1.23052135 4.32838242,1.49998202 4.32838242,1.82336629 C4.32838242,2.14668764 4.44004038,2.41614831 4.66341805,2.63181124 L7.24652732,5.27252584 L1.03990736,5.27252584 C0.734361045,5.27252584 0.486033254,5.38477753 0.295016627,5.60937528 C0.104,5.83397303 0.00849168646,6.10494382 0.00849168646,6.42222472 L0.00849168646,7.57198652 C0.00849168646,7.8893618 0.104030879,8.1602382 0.295016627,8.38483596 C0.486002375,8.60943371 0.734361045,8.72159101 1.03990736,8.72159101 L7.2464038,8.72159101 L4.66332542,11.3535281 C4.43994774,11.5809258 4.32828979,11.8534382 4.32828979,12.1709079 C4.32828979,12.4883146 4.43994774,12.7607955 4.66332542,12.9881933 L5.32447268,13.6618292 C5.5537791,13.8834067 5.81822565,13.9941798 6.11796675,13.9941798 C6.42363658,13.9941798 6.69114014,13.8834067 6.9202304,13.6618292 L12.6594988,7.81442247 C12.8769786,7.59284494 12.9857957,7.32045843 12.9857957,6.9970427 C12.9857957,6.66774382 12.8769786,6.3981573 12.6595914,6.18869213 Z"></path>
                                          </g>
                                        </g>
                                      </g>
                                    </g>
                                  </svg></i></button>
                            </div>
                          </div>
                        </div>
                        <div class="row u-hidden_desktop">
                          <div class="col-12">
                            <!--<div class="c-assistance u-mb30">
                              <h4 class="c-assistance__title">�Necesitas asistencia?</h4><a class="c-assistance__button o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="#">Te llamamos gratis</a>
                              <p class="c-assistance__text">O ll�manos gratis al<strong class="c-assistance__outstanding"> 600 500 5000</strong></p>
                            </div>-->
                          </div>
                        </div>
                      </form>
                    </div>
                    <div class="col-lg-4">
                      <section class="c-summary" id="summary_sure">
                        <h2 class="c-summary__title">Resumen de tu seguro</h2>
                        <div class="c-summary__company"></div>
                        <h3 class="c-summary__subtitle"><bean:write name="datosPlan" property="nombrePlan" /></h3>
                        <div class="row">
                          <div class="col-12">
                            <div class="c-summary__cost">
                              <div class="c-summary__label">Monto Total:</div>
                              <div class="c-summary__price"><sup>$</sup><bean:write name="datosPlan" property="primaMensualPesos"
								format="##,##0" locale="currentLocale" /><sub>/Mes*</sub></div>
                            </div>
                            <p class="c-summary__legal"></p>
                          </div>
                          <div class="col-12 u-mobile_second">
                            <div class="o-collapse"><a class="o-collapse__button o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="#" state="0" onclick="collapse(event);"> Ver detalles contrataci�n<i class="o-icon o-icon--triangle"><span class="u-triangle_down"></span></i></a>
                              <div class="o-collapse__content">
                                <div class="c-summary__detail">
                                  <table class="o-table o-table--basic u-mb20">
                                    <thead class="o-table__head">
                                      <tr class="o-table__row">
                                        <th class="o-table__col">Tipo</th>
                                        <th class="o-table__col">Modelo</th>
                                      </tr>
                                    </thead>
                                    <tbody class="o-table__body">
                                      <tr class="o-table__row">
                                        <td class="o-table__col"><bean:write name="tipoDesc" scope="request"/></td>
                                        <td class="o-table__col"><bean:write name="modeloDes" scope="request"/></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <table class="o-table o-table--basic">
                                    <thead class="o-table__head">
                                      <tr class="o-table__row">
                                        <th class="o-table__col">Marca</th>
                                        <th class="o-table__col">A�o</th>
                                      </tr>
                                    </thead>
                                    <tbody class="o-table__body">
                                      <tr class="o-table__row">
                                        <td class="o-table__col"><bean:write name="marcaDesc" scope="request"/></td>
                                        <td class="o-table__col"><bean:write name="datosCotizacion" property="anyoVehiculo"/></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!--<div class="c-assistance u-mb30 u-hidden_mobile">
                          <h4 class="c-assistance__title">�Necesitas asistencia?</h4><a class="c-assistance__button o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="#">Te llamamos gratis</a>
                          <p class="c-assistance__text">O ll�manos gratis al<strong class="c-assistance__outstanding"> 600 500 5000</strong></p>
                        </div>-->
                      </section>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </section>
        
      </div>
    </main>
							
							
							
							
						</div>
						<div id="paso2">
						
						<div id="validaForm">
							
							<main role="main">
      <div class="remodal-bg">
        <section class="container">
          <div class="row">
            <div class="col-12">
				<logic:equal name="idRama" value="1">
				      	<logic:equal name="idSubcategoria" value="22"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Full Cobertura</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="21"><h1 class="o-title o-title--primary">Cotizaci�n Seguro P�rdida Total</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="36"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Obligatorio Mercosur</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="79"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Robo Contenido</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="2">
				      	<logic:equal name="idSubcategoria" value="24"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Sismo / Estructura</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="43"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Sismo / Contenido</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="44"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Robo</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="25"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio, Robo y Sismo</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="26"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Premio a la Permanencia</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="201"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Full Asistencia</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="220"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Vacaciones</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="3">
				      	<logic:equal name="idSubcategoria" value="29"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Viaje Grupal</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="5">
				      	<logic:equal name="idSubcategoria" value="139"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Fraude</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="159"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Fraude con devoluci�n</h1></logic:equal>
			      	</logic:equal>
            		<logic:equal name="idRama" value="6">
				      	<logic:equal name="idSubcategoria" value="119"><h1 class="o-title o-title--primary">Asistencia Viaje</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="140"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Mascota</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="221"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Viaje Grupal</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="7">
						<logic:equal name="idSubcategoria" value="161"><h1 class="o-title o-title--primary">Hospitalizaci�n con devoluci�n</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="162"><h1 class="o-title o-title--primary">Accidentes Personales con devoluci�n</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="164"><h1 class="o-title o-title--primary">Oncol�gico con devoluci�n</h1></logic:equal>
			      	</logic:equal>
			      	<logic:equal name="idRama" value="8">
				      	<logic:equal name="idSubcategoria" value="239"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Obligatorio Mercosur</h1></logic:equal>
			      	</logic:equal>
			</div>
            <div class="col-12">
              <section class="o-box o-box--recruiting u-mtb50">
                <div class="o-steps">
                  <div class="o-steps__item o-steps__item--Completed"><span class="o-steps__milestone"><span class="o-steps__value">1</span></span>
                    <h4 class="o-steps__title">Informaci�n contratante</h4>
                  </div>
                  <div class="o-steps__item o-steps__item--current"><span class="o-steps__milestone"><span class="o-steps__value">2</span></span>
                    <h4 class="o-steps__title">Datos del seguro</h4>
                  </div>
                  <div class="o-steps__item"><span class="o-steps__milestone"><span class="o-steps__value">3</span></span>
                    <h4 class="o-steps__title">Confirmaci�n y pago</h4>
                  </div>
                </div>
                <div class="u-pad20">
                  <div class="row o-form o-form--standard o-form--linear o-form--small">
                    <div class="col-lg-8 u-mobile_second">
                      <h2 class="o-title o-title--subtitle u-mb10">Datos del Seguro</h2>
                      <p class="o text"></p>
                        <div class="row u-mt40">
                          <div class="col-lg-6">
                            <div class="o-form__field">
                              <label class="o-form__label">Tipo</label>
                              <html:text property="datos(tipoVehiculo)"
													styleClass="o-form__input" styleId="datos.tipoVehiculo"></html:text></span><span class="o-form__message"></span>
                            <script type="text/javascript">
												$(document).ready(function(){
													<logic:present name="datosCotizacion" scope="session">
														var val_tipoVehiculo = "<bean:write name="tipoDesc" scope="request"/>";
														$("input[name='datos(tipoVehiculo)']").val(val_tipoVehiculo);
														$("input[name='datos(tipoVehiculo)']").attr('disabled', 'disabled');
													</logic:present>
												});
							</script>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="o-form__field">
                              <label class="o-form__label">Marca</label>
                              <html:text property="datos(marcaVehiculo)"
													styleClass="o-form__input" styleId="datos.marcaVehiculo"></html:text><span class="o-form__line"></span><span class="o-form__message"></span>
												<script type="text/javascript">
													$(document).ready(function(){
														<logic:present name="datosCotizacion" scope="session">
															var val_marcaVehiculo = "<bean:write name="marcaDesc" scope="request"/>";
															$("input[name='datos(marcaVehiculo)']").val(val_marcaVehiculo);
															$("input[name='datos(marcaVehiculo)']").attr('disabled', 'disabled');
														</logic:present>
													});
												</script>
                            </div>
                          </div>
                        </div>
                          <div class="row">
                          <div class="col-lg-6">
                            <div class="o-form__field">
                              <label class="o-form__label">Modelo</label>
                             <html:text property="datos(modeloVehiculo)"
													styleClass="o-form__input" styleId="datos.modeloVehiculo"></html:text><span class="o-form__line"></span><span class="o-form__message"></span>
												<script type="text/javascript">
													$(document).ready(function(){
														<logic:present name="datosCotizacion" scope="session">
															var val_modeloVehiculo = "<bean:write name="modeloDes" scope="request"/>";
															$("input[name='datos(modeloVehiculo)']").val(val_modeloVehiculo);
															$("input[name='datos(modeloVehiculo)']").attr('disabled', 'disabled');
														</logic:present>
													});
												</script>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="o-form__field">
                              <label class="o-form__label">A�o</label>
                              <html:text property="datos(anoVehiculo)"
													styleClass="o-form__input" size="18" maxlength="4"
													styleId="datos.anoVehiculo"></html:text><span class="o-form__line"></span><span class="o-form__message"></span>
												<script type="text/javascript">
													$(document).ready(function(){
														<logic:present name="datosCotizacion" scope="session">
															var val_anoVehiculo = "<bean:write name="datosCotizacion" property="anyoVehiculo"/>";
															$("input[name='datos(anoVehiculo)']").val(val_anoVehiculo);
															$("input[name='datos(anoVehiculo)']").attr('disabled', 'disabled');
														</logic:present>
													});
												</script>
                            </div>
                          </div>
                          </div>
                          
                          <div class="row">
                          <div class="col-lg-6">
                            <div class="o-form__field">
                              <label class="o-form__label">Patente</label>
                              <logic:equal name="idRama" value="1">
													<input class="o-form__input" placeholder="Patente"
														name="datos(patente)" maxlength="6" size="18" id="patente"><span class="o-form__line"></span><span class="o-form__message"></span>
							  </logic:equal>
				 			  <logic:equal name="idRama" value="8">
													<input class="o-form__input" placeholder="Patente"
														name="datos(patente)" maxlength="6" size="18" id="patente"><span class="o-form__line"></span><span class="o-form__message"></span>
							  </logic:equal>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="o-form__field">
                              <label class="o-form__label">N� Motor</label>
                              <input class="o-form__input" placeholder="N� Motor"
													name="datos(numeroMotor)" maxlength="20" size="18"
													id="numeroMotor"><span class="o-form__line"></span><span class="o-form__message"></span>
                            </div>
                          </div>
                          </div>
                          <div class="row">
                          <div class="col-lg-6">
                            <div class="o-form__field o-form__field--select">
                              <label class="o-form__label">Color</label>
                              <html:select property="datos(colorVehiculo)"
													styleClass="o-form__select" styleId="color">
													<html:option value="65">Seleccione</html:option>
													<html:options collection="colorVehiculo" property="idColor"
														labelProperty="descripcion" />
							  </html:select><span class="o-form__line"></span><span class="o-form__message"></span>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="o-form__field">
                              <label class="o-form__label">N� Chasis</label>
                              <input class="o-form__input" placeholder="N� Chasis"
													name="datos(numeroChasis)" maxlength="20" size="18"
													id="numeroChasis"><span class="o-form__line"></span><span class="o-form__message"></span>
                            </div>
                          </div>
                          </div>
                          <logic:equal name="solicitaAutoNuevo" value="1">
							  <div class="row">
							  <div class="col-lg-6">
							  
	                            <div class="o-form__field o-form__field--select">
	                              <label class="o-form__label">Antig�edad veh�culo</label>
	                              <select class="o-form__select" required="" name="datos(esNuevo)"
															id="esNuevo" onchange="javascript:esNuevoCheck(this)">
	                                <option value="">Seleccione antig�edad</option>
	                                <option value="1">Auto nuevo (menos de 48 horas despues de la compra)</option>
	                                <option value="0">Auto usado</option>
	                              </select><span class="o-form__line"></span><span class="o-form__message"></span>
	                            </div>
	                          </div>
							  </div>
							</logic:equal>
                          <logic:notEqual name="solicitaAutoNuevo" value="1">
								<div class="row">
								<div class="col-lg-6" style="display: none;">
	                            <div class="o-form__field o-form__field--select">
	                              
									<html:radio property="datos(esNuevo)" styleId="esNuevo"
										value="0"></html:radio>
									<span class="o-form__line"></span><span class="o-form__message"></span>
	                            </div>
	                          </div>
								</div>
							</logic:notEqual>
							<div class="row">
							<div id="inputFactura">
						 <div class="col-lg-6">
		                      <div class="o-form__field">
		                        <label class="o-form__label">Fecha de la factura:*</label>
		                        <div class="row">
		                          <div class="col-4">
		                          <div class="o-form__field o-form__field--select">
									<html:select property="datos(diaFactura)"
																		styleClass="o-form__select" styleId="dias">
																		<html:option value="">d�a</html:option>
																		<html:options collection="diasVehiculo" property="key"
																			labelProperty="value" />
									</html:select><span class="o-form__line"></span>
									</div>
		                          </div>
		                          <div class="col-4">
		                          <div class="o-form__field o-form__field--select">
		                            <html:select property="datos(mesFactura)"
																		styleClass="o-form__select" styleId="mesFecha">
																		<html:option value="">mes</html:option>
																		<html:options collection="mesesVehiculo" property="key"
																			labelProperty="value" />
									</html:select><span class="o-form__line"></span>
									</div>
		                          </div>
		                          <div class="col-4">
		                          <div class="o-form__field o-form__field--select">
									<html:select property="datos(anyoFactura)"
																		styleClass="o-form__select" styleId="anyoFecha">
																		<html:option value="">a�o</html:option>
																		<html:options collection="anyosVehiculo" property="key"
																			labelProperty="value" />
									</html:select><span class="o-form__line"></span>
									</div>
		                          </div>
		                        </div><span class="o-form__message" id="year_of_birth"></span>
		                      </div>
                       </div>
                       <div class='col-lg-6'>
												<div class="form-group">
												<label class="o-form__label">Adjuntar archivo:</label>
													 <input type="file"
														name="archivoFactura" id="factura" class="o-form_input" />
													<p class="texto-chico-informativo">* Formatos
														permitidos: JPG, TIFF, BMP Y PDF. La imagen no debe
														exceder los 5 MB.</p>
												</div>
						</div>
											<logic:equal name="ingresarDatosVehiculo"
												property="datos(esNuevo)" value="0">
												<script type="text/javascript">
													$("#inputFactura").hide("fast");
												</script>
											</logic:equal>
										</div>
							</div>
                          <logic:present name="datosPlan" scope="session">
                          	<div class="row">
                          	<div class="col-lg-6">
											<logic:notEqual name="datosPlan" property="maxDiaIniVig"
												value="0">
												<div class='col-sm-12'>
													<h4>Datos de Viaje</h4>
												</div>
												<div class="col-sm-6">
													<div class="form-group">
														<label>Fecha de Viaje*</label> <input class="form-control"
															placeholder="30/01/1980" name="datos(fechaInicio)"
															id="inputIniVig" onBlur="autocompletarfechaVehiculo(this);"
															onFocus="autocompletarfechaVehiculo(this);"
															maxlength="10"/>
													</div>
												</div>
												<div class="col-sm-12" id="div-mensaje-error"
													style="display: none">
													<div class="form-group">
														<div class="alert alert-warning">
															<p id="error-fecha-viaje" align="center">
																<html:errors property='diaIniVig' />
															</p>
														</div>
													</div>
												</div>
												<script>
													$().ready(function() {
														if($(".divtexnovalido").length) {
															$("#div-mensaje-error").show("fast");
														}
													});
												</script>
												<logic:notPresent name="sinTextoProvisorio">
													<div class="separacion-hor-cont-lateral"></div>
												</logic:notPresent>
											</logic:notEqual>
							</div>
                          	</div>
						 </logic:present>
                          <div class="col-12 u-mb30">
                            <hr class="u-line u-line--gray">
                          </div>
                        <logic:equal name="subcategoriaDesc"
						 value="Cobertura Argentina">
							<div class="row">
	                          <div class="col-lg-9"><strong class="o-text">�El veh�culo es de uso particular?</strong></div>
	                          <div class="col-lg-3">
	                            <div class="o-form__field--inline u-mmt10">
	                               <select class="o-form__select" id="pregunta1" name="pregunta1"
														onchange="respuestaExcluyente(this.value==='true', 1)">
														<option selected value="true">S�</option>
														<option value="false">No</option>
								   </select>
	                              <div class="u-clearfix"></div><span class="o-form__message"></span>
	                            </div>
	                          </div>
	                        </div>
						</logic:equal>
						<logic:equal name="datosPlan" property="maxDiaIniVig"
											value="0">
						<div class="row">
                          <div class="col-lg-9"><strong class="o-text">�El veh�culo es de uso particular?</strong></div>
                          <div class="col-lg-3">
                            <div class="o-form__field--inline u-mmt10">
                               <select class="o-form__select" id="pregunta1" name="pregunta1"
													onchange="respuestaExcluyente(this.value==='true', 1)">
													<option selected value="true">S�</option>
													<option value="false">No</option>
							   </select>
                              <div class="u-clearfix"></div><span class="o-form__message"></span>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-9"><strong class="o-text">�El veh�culo presenta da�os que impiden el normal funcionamiento de �ste?</strong></div>
                          <div class="col-lg-3">
                            <div class="o-form__field--inline u-mmt10">
                               <select class="o-form__select" id="pregunta2" name="pregunta2"
													onchange="respuestaExcluyente(this.value==='true', 2)">
													<option value="false">S�</option>
													<option selected value="true">No</option>
								</select>
                              <div class="u-clearfix"></div><span class="o-form__message"></span>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-9"><strong class="o-text">�El veh�culo fue internado a Chile con franquicia aduanera?*</strong></div>
                          <div class="col-lg-3">
                            <div class="o-form__field--inline u-mmt10">
                            <select class="o-form__select" id="pregunta3" name="pregunta3"
													onchange="respuestaExcluyente(this.value==='true', 3)">
													<option value="false">S�</option>
													<option selected value="true">No</option>
												</select>
                              <div class="u-clearfix"></div><span class="o-form__message"></span>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-9"><strong class="o-text">�El veh�culo presenta da�os en alguna pieza que afecte su seguridad?</strong></div>
                          <div class="col-lg-3">
                            <div class="o-form__field--inline u-mmt10">
                              <select class="o-form__select" id="pregunta4" name="pregunta4"
													onchange="respuestaExcluyente(this.value==='true', 4)">
													<option value="false">S�</option>
													<option selected value="true">No</option>
							 </select>
                              <div class="u-clearfix"></div><span class="o-form__message"></span>
                            </div>
                          </div>
                        </div>					
						</logic:equal>
						<div class="form-group">
												<html:hidden property="datos(esParticular)"
													styleClass="form-control" value="1" styleId="esParticular"></html:hidden>
						</div>
                        <script type="text/javascript">
										function respuestaExcluyente(valor, pregunta){
											if($("select[name='pregunta1']").val()=='false'||
													$("select[name='pregunta2']").val()=='false'||
													$("select[name='pregunta3']").val()=='false'||
													$("select[name='pregunta4']").val()=='false') {
												$('#divAviso').show();
												<logic:notPresent name="usuario">					
													$('#opciones-compra').hide();
												</logic:notPresent>
												<logic:present name="usuario">
													$('#continuar-compra').hide();
												</logic:present>
											}else{
												$('#divAviso').hide();
												<logic:notPresent name="usuario">					
													$('#opciones-compra').show();
												</logic:notPresent>
												<logic:present name="usuario">
													$('#continuar-compra').show();
												</logic:present>
											}
										};
											</script>
											<div id="divAviso" class="row" style="display: none;">
											<div class="col-sm-12">
												<div class="alert alert-warning top10">
													<p align="center">Seg�n las respuestas entregadas no es
														posible continuar con la cotizaci�n</p>
												</div>
											</div>
							</div>
						<logic:equal value="1" name="solicitaInspeccion">			
						<!-- INSPECCION OBLIGATORIA -->
                        <div class="row">
                          <div class="col-12">
                            <hr class="u-line u-line--gray">
                            <h2 class="o-title o-title--subtitle o-title--primary u-mt30">Inspecci�n obligatoria</h2>
                            <p class="o-text"><bean:write name="datosCotizacion" property="nombre" />, es fundamental que realices la inspecci�n de tu veh�culo para que tengas cobertura en caso de siniestro. Nos contactaremos contigo al tel�fono <bean:write name="datosCotizacion" property="numeroTelefono1" /> en un m�ximo de 48 horas h�biles, en horario laboral (08:30 a 18:30) donde coordinaremos una inspecci�n a domicilio.</p>
                          </div>
                          <div class="cotizador_cn_fono">
                          
                            <div class="o-form__field--select"><span class="o-checkbox u-mb10">
                                <input class="" type="checkbox" id="conf_num"
														onclick="javascript:cambiarNumero();">
                                <label class="" for="mandatory_inspection_check">Confirmar numero</label></span>
                              <input class="o-form__input" name="firstname" id="firstname" maxlength="11"
														value="<bean:write name='datosCotizacion' property='numeroTelefono1'/>"
														placeholder="<bean:write name='datosCotizacion' property='numeroTelefono1'/>"
														pattern="[0-9]*"><span class="o-form__line"></span>
                            </div>
                          
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-12">
                            <div class="o-legal">
                              <p class="o-legal__text">El asegurado asume, entre otras cosas, la obligaci�n de realizar la inspecci�n del veh�culo en el plazo de 48 horas h�biles desde la contrataci�n del seguro. Durante el tiempo que medie entre la contrataci�n del seguro y la inspecci�n, el veh�culo gozar� de la cobertura contratada sin perjuicio de los l�mites y exclusiones se�alados en la p�liza respectiva, con las siguientes restricciones:</p>
                              <p class="o-legal__text">a) Para la cobertura de da�os al veh�culo asegurado, se aplicar� un deducible equivalente al 10% de la p�rdida con un m�nimo de 30 UF.</p>
                              <p class="o-legal__text">b) Para las coberturas de robo, hurto y uso no autorizado, se aplicar� un deducible equivalente al 90% de la p�rdida con un m�nimo de 100 UF.</p>
                              <p class="o-legal__text">Una vez realizada y aprobada la inspecci�n del veh�culo asegurado, se aplicar� inmediatamente la cobertura contratada sin las restricciones se�aladas en las letras a) y b) precedentes. Mientras no se efect�e la inspecci�n del veh�culo, se mantendr�n los deducibles y restricciones mencionadas en las letras a) y b) precedentes.</p>
                              <p class="o-legal__text">En caso de que el asegurado no realice la inspecci�n del veh�culo, la compa��a pondr� t�rmino anticipado al contrato de seguro vigente, conforme a lo establecido en el Art. 27: T�rmino anticipado del seguro. La compa��a informar� al asegurado el t�rmino anticipado mediante comunicaci�n escrita enviada al domicilio.</p>
                            </div>
                          </div>
                        </div>
                        <!-- FIN INSPECCION OBLIGATORIA -->
                        </logic:equal>
                        <div class="row o-actions o-actions--flex">
                        <logic:notPresent name="usuario">
                        <div class="col-lg-3"> 
                            <div class="o-form__field is-last"><a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" onclick="switchForm(false)"><i class="mx-arrow-left"></i> Volver </a></div>
                          </div>
                          <logic:equal value="1" name="solicitaInspeccion">
                          <div id="opciones-compra" class="col-lg-3 offset-lg-6">
                            <div class="o-form__field is-last">
                              <button class="o-btn o-btn--primary o-btn o-btn--icon" id="send_form_recruiting_step2" type="button" onclick="showModal()">Siguiente<i class="o-icon">
                                  <svg width="13px" height="14px" viewbox="0 0 13 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                      <g id="botones/orangeButton+rightArrow" transform="translate(-113.000000, -19.000000)" fill-rule="nonzero" fill="#FFFFFF">
                                        <g id="orangeButton+rightArrow">
                                          <g id="arrows" transform="translate(113.000000, 19.000000)">
                                            <path id="Shape" d="M12.6595914,6.18869213 L6.92029216,0.341285393 C6.69713064,0.113730337 6.42956532,0 6.11805938,0 C5.81235867,0 5.54791211,0.113730337 5.32456532,0.341285393 L4.66341805,1.01492135 C4.44004038,1.23052135 4.32838242,1.49998202 4.32838242,1.82336629 C4.32838242,2.14668764 4.44004038,2.41614831 4.66341805,2.63181124 L7.24652732,5.27252584 L1.03990736,5.27252584 C0.734361045,5.27252584 0.486033254,5.38477753 0.295016627,5.60937528 C0.104,5.83397303 0.00849168646,6.10494382 0.00849168646,6.42222472 L0.00849168646,7.57198652 C0.00849168646,7.8893618 0.104030879,8.1602382 0.295016627,8.38483596 C0.486002375,8.60943371 0.734361045,8.72159101 1.03990736,8.72159101 L7.2464038,8.72159101 L4.66332542,11.3535281 C4.43994774,11.5809258 4.32828979,11.8534382 4.32828979,12.1709079 C4.32828979,12.4883146 4.43994774,12.7607955 4.66332542,12.9881933 L5.32447268,13.6618292 C5.5537791,13.8834067 5.81822565,13.9941798 6.11796675,13.9941798 C6.42363658,13.9941798 6.69114014,13.8834067 6.9202304,13.6618292 L12.6594988,7.81442247 C12.8769786,7.59284494 12.9857957,7.32045843 12.9857957,6.9970427 C12.9857957,6.66774382 12.8769786,6.3981573 12.6595914,6.18869213 Z"></path>
                                          </g>
                                        </g>
                                      </g>
                                    </g>
                                  </svg></i></button>
                            </div>
                          </div>
                        </logic:equal>
                        <logic:notEqual value="1" name="solicitaInspeccion">
                          <div id="opciones-compra" class="col-lg-3 offset-lg-6">
                             <div class="o-form__field is-last">
                              <button class="o-btn o-btn--primary o-btn o-btn--icon" id="send_form_recruiting_step2" type="button" onclick="showModalLight()">Siguiente<i class="o-icon">
                                  <svg width="13px" height="14px" viewbox="0 0 13 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                      <g id="botones/orangeButton+rightArrow" transform="translate(-113.000000, -19.000000)" fill-rule="nonzero" fill="#FFFFFF">
                                        <g id="orangeButton+rightArrow">
                                          <g id="arrows" transform="translate(113.000000, 19.000000)">
                                            <path id="Shape" d="M12.6595914,6.18869213 L6.92029216,0.341285393 C6.69713064,0.113730337 6.42956532,0 6.11805938,0 C5.81235867,0 5.54791211,0.113730337 5.32456532,0.341285393 L4.66341805,1.01492135 C4.44004038,1.23052135 4.32838242,1.49998202 4.32838242,1.82336629 C4.32838242,2.14668764 4.44004038,2.41614831 4.66341805,2.63181124 L7.24652732,5.27252584 L1.03990736,5.27252584 C0.734361045,5.27252584 0.486033254,5.38477753 0.295016627,5.60937528 C0.104,5.83397303 0.00849168646,6.10494382 0.00849168646,6.42222472 L0.00849168646,7.57198652 C0.00849168646,7.8893618 0.104030879,8.1602382 0.295016627,8.38483596 C0.486002375,8.60943371 0.734361045,8.72159101 1.03990736,8.72159101 L7.2464038,8.72159101 L4.66332542,11.3535281 C4.43994774,11.5809258 4.32828979,11.8534382 4.32828979,12.1709079 C4.32828979,12.4883146 4.43994774,12.7607955 4.66332542,12.9881933 L5.32447268,13.6618292 C5.5537791,13.8834067 5.81822565,13.9941798 6.11796675,13.9941798 C6.42363658,13.9941798 6.69114014,13.8834067 6.9202304,13.6618292 L12.6594988,7.81442247 C12.8769786,7.59284494 12.9857957,7.32045843 12.9857957,6.9970427 C12.9857957,6.66774382 12.8769786,6.3981573 12.6595914,6.18869213 Z"></path>
                                          </g>
                                        </g>
                                      </g>
                                    </g>
                                  </svg></i></button>
                             </div>
                           </div>
                        </logic:notEqual>
                        </logic:notPresent>
                          
                        
                          
                          <div class="col-lg-12">
								<div class='form-group'>
									<div class="row">
										
										
										
										
										
										<div class="col-sm-3">
											<div class="form-group">
												<html:hidden property="datos(esParticular)"
													styleClass="form-control" value="1" styleId="esParticular"></html:hidden>
											</div>
										</div>
										
										<div class="separacion-hor-cont-lateral"></div>
										
										<div class="row">
											<div class='col-md-12 col-sm-4'>
												<div class="alert alert-warning" id="incomplete-paso2"
													style="display: none">
													<p align="center" id="incomplete-text-paso2"></p>
												</div>
											</div>
											
										</div>
									</div>
								</div>
								<div class="form-group">
									
										<logic:notPresent name="sinTextoProvisorio">
										<div class="row">
											<div class="texto_factura">
												<div class="col-sm-12">
													<p>Es fundamental que adjuntes la factura de tu
														veh�culo para validar tus datos.</p>
													<p>Nos contactaremos contigo dentro de las pr�ximas 48
														horas en horario laboral (08:30 a 18:30 hrs.)</p>
													<html:hidden property="datos(tipoInspeccion)" value="1"
														name="cotizarProducto" />
												</div>
											</div>
											</div>
											<logic:equal value="1" name="solicitaInspeccion">
												<html:hidden property="datos(tipoInspeccion)" value="1"
													name="cotizarProducto" />
												<html:hidden property="datos(hiddNumeroFono)" value="1"
													styleId="hiddNumeroFono" />
										
									<div class="row top10" id="divFormIncompleto"
										style="display: none;">
										<div class="col-sm-12">
											<div class="alert alert-warning">
												<p align="center">Por favor complete el formulario
													correctamente</p>
											</div>
										</div>
									</div>
									
									<!--acordeones-->
									
									<!-- fin acordeones-->
									</logic:equal>
										<script type="text/javascript">			
											function cambiarNumero(){
												var numero = document.getElementById('firstname').value;
												var er_tlfono = /([0-9])/
												if(!er_tlfono.test(numero)) {
													alert('Campo TELEFONO no v�lido.');
												return false;
												}
												$('span#numeroFono').empty();
												$('span#numeroFono').html(numero);
												document.getElementById('hiddNumeroFono').value = numero;
												$(".burbuja-telefono-contacto").hide();
												$("#divErrorNumero").hide();
											};
											
											function submitPaso2Vehiculo(){
												$("#username").val($("#rutpaso2").val() + '-' + $("#dvpaso2").val());
												$("#password").val($("#clavepaso2").val());
												var existeVacio = "0";
																								
												var numero = $("#hiddNumeroFono").val();
												var html = "";
												if (numero == "1"){
													$("#divErrorNumero").show();
													existeVacio = "1";
												} else {
													$("#waitModal").modal({backdrop: 'static', keyboard: false});															
													$("#divErrorNumero").hide();
												}
												
												//Inicio validar respuestas codigo de comercio
												<logic:equal name="idRama" value="1">
													<logic:equal name="subcategoriaDesc" value="Cobertura Argentina">
														var valor;
														var pregunta;
														if(document.getElementsByName('pregunta1').value==='false'){
															valor = false;
														pregunta = 1;
														respuestaExcluyente(valor, pregunta);
													}else{
													</logic:equal>
													<logic:notEqual name="subcategoriaDesc" value="Cobertura Argentina">
													var valor;
													var pregunta;
													if(document.getElementsByName('pregunta1').value==='false'){
														valor = false;
														pregunta = 1;
														respuestaExcluyente(valor, pregunta);
													}else if(document.getElementsByName('pregunta2').value==='true'){
														valor = false;
														pregunta = 2;
														respuestaExcluyente(valor, pregunta);
													}else if(document.getElementsByName('pregunta3').value==='true'){
														valor = false;
														pregunta = 3;
														respuestaExcluyente(valor, pregunta);
													}else if(document.getElementsByName('pregunta4').value==='true'){
														valor = false;
														pregunta = 4;
														respuestaExcluyente(valor, pregunta);
													}else{
													</logic:notEqual>
												</logic:equal>
													//Si las respuestas de codigo comercio son correctas se envia el formulario
													if (existeVacio == 0){
														$("#paso2VehiculoForm").submit();
													} else {
														$("#waitModal").modal('hide');
														$("#divFormIncompleto").show();
													}
												<logic:equal name="idRama" value="1">}</logic:equal>
											};
										</script>
									</logic:notPresent>
									<script type="text/javascript">							
										function submitPaso2Vehiculo(){
											$("#username").val($("#rutpaso2").val() + '-' + $("#dvpaso2").val());
											$("#password").val($("#clavepaso2").val());
											var existeVacio = "0";
											
											
											var numero = $("#hiddNumeroFono").val();
											var patente = $("#patente").val();
											var NumMotor = $("#numeroMotor").val();
											var NumChasis = $("#numeroChasis").val();
											var color = $("#color").val();
											var html = "";
						
										if (numero == "1"){
											console.log("se cae x el numero");
											$("#divErrorNumero").show();
											existeVacio = "1";
										} else {
											$("#waitModal").modal({backdrop: 'static', keyboard: false});	
											$("#divErrorNumero").hide();
										}
																
										//Inicio validar respuestas codigo de comercio
										<logic:equal name="idRama" value="1">
											<logic:equal name="subcategoriaDesc" value="Cobertura Argentina">
												var valor;
												var pregunta;
												if(document.getElementsByName('pregunta1').value==='false'){
													valor = false;
													pregunta = 1;
													respuestaExcluyente(valor, pregunta);
												}else{
											</logic:equal>

											<logic:notEqual name="subcategoriaDesc" value="Cobertura Argentina">
												var valor;
												var pregunta;
												if(document.getElementsByName('pregunta1').value==='false'){
													valor = false;
													pregunta = 1;
													respuestaExcluyente(valor, pregunta);
												}else if(document.getElementsByName('pregunta2').value==='true'){
													valor = false;
													pregunta = 2;
													respuestaExcluyente(valor, pregunta);
												}else if(document.getElementsByName('pregunta3').value==='true'){
													valor = false;
													pregunta = 3;
													respuestaExcluyente(valor, pregunta);
												}else if(document.getElementsByName('pregunta4').value==='true'){
													valor = false;
													pregunta = 4;
													respuestaExcluyente(valor, pregunta);
												}else{
											</logic:notEqual>
										</logic:equal>
											if (patente == "" || NumMotor == "" || NumChasis == "" || color == "65"){
												existeVacio= 1;
											}
													//Si las respuestas de codigo comercio son correctas se envia el formulario
													if (existeVacio == 0){
														$("#paso2VehiculoForm").submit();
													} else {	
														$("#patente").focus();
														if(patente == ""){
															$("#patente").css("border-color","red");
														}else if (NumMotor == ""){
															$("#numeroMotor").focus();
															$("#numeroMotor").css("border-color","red");
														}else if (NumChasis == ""){
															$("#numeroChasis").focus();
															$("#numeroChasis").css("border-color","red");
														}	
														$("#waitModal").modal('hide');
														$("#divFormIncompleto").show();
													}
										<logic:equal name="idRama" value="1">}</logic:equal>
										};
									</script>
								
				<script type="text/javascript">	
					function showModal() {
						var result = validateInput("paso2");
	    				if(validacionInput == 0){
	    				elemento = document.getElementById("conf_num");
	    					if(elemento.checked){
		    					var inst = $('[data-remodal-id=continueModal]').remodal();
								inst.open();
	    					}else {
	    						$("#divErrorNumero").show();
	    						$("#conf_num").focus();
	    					}	    					
	    				}
					}
				
					function showModalLight(){
						var result = validateInput("paso2");
	    				if(validacionInput == 0){
		    					var inst = $('[data-remodal-id=continueModal]').remodal();
								inst.open();
	    				}
					}
					
					function closeModal(){
						$('[data-remodal-id=continueModal]').remodal().close();
					}
								
					function sinRegistro(){
						validateInput("validaForm");
						closeModal();
						if(validacionInput == 0) {
							var rutSinRegistro = $("input[name='datos(rut)']").val();
							document.getElementById('paso2VehiculoForm').action="/cotizador/guardar-datos-adicionales-vehiculo.do?sinRegistro=1&rutSinRegistro="+rutSinRegistro;
							submitPaso2Vehiculo();
						}
					}
					
					function conRegistro(){
						validateInput("paso2");
						if(validacionInput == 0) {
							submitPaso2Vehiculo();
						}
					}
				</script>
				
				<!-- INICIO ModalRegistroRemoto -->
				<div class="modal fade" id="modalRegistrate" role="dialog">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4>Registrate</h4>
							</div>
							<div class="modal-body">
								<iframe class="" frameborder="0" id="cargaRegistrate" width="100%" height="500px">
								</iframe>	
							</div>
						</div>
					</div>
				</div> 
				<!-- FIN ModalRegistroRemoto -->	
				</div>
				</div>
				<div class="col-lg-12">
				<logic:notPresent name="usuario">					
				<!-- BOTONES COLAPSABLES -->
					<div class="form-group">
						<div class="row">
						
						  <div class="panel-group" id="accordion-botones" role="tablist" aria-multiselectable="true">
							<div class="panel" style="margin-top:0">
							  <!--<div class="panel-heading" role="tab" id="1">
								<div class="panel-title2"> <a class="btn btn-primary collapsed" role="button" data-toggle="collapse" 
								data-parent="#accordion-botones" aria-expanded="true" href="#collapsebot1"> 
								COMPRA SIN REGISTRARTE </a> </div>
							  </div>
							  <div id="collapsebot1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="1">
								<div class="panel-body">
								  <p class="text-center">Puedes realizar tus compras sin necesidad de estar registrado.</p>
								  <a class="btn btn-primary btn-block" href="javascript:sinRegistro();" role="button">CONTINUAR COMPRA</a> </div>
							  </div>-->
							</div>
							 <!--  <div class="top15 hidden-lg"></div>-->
							<div class="panel">
							  <div class="panel-heading" role="tab" id="2">
								<!--<div class="panel-title2"> <a class="btn btn-primary collapsed" role="button" data-toggle="collapse" 
								data-parent="#accordion-botones" aria-expanded="false" href="#collapsebot2"> 
								USUARIO REGISTRADO </a> </div>-->
								<input type="hidden" name="username" id="username" value="" /> 
								<input type="hidden" name="password" id="password" value="" /> 
								<input type="hidden" name="rutpaso2" id="rutpaso2" value="" /> 
								<input type="hidden" name="dvpaso2" id="dvpaso2" value="" />
							  </div>
							  <div id="collapsebot2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="2">
								<div class="panel-body">
								<!-- inicio modal nuevo  --> 
						  <div class="remodal c-modal c-modal--modal-options" data-remodal-id="continueModal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">   
        <button class="remodal-close" data-remodal-action="close" aria-label="Close">  </button>
        <div class="c-modal__wrap">
          <div class="c-modal__modalHead">
            <div class="c-modal__modalHead-inner"> 
              <h2>Selecciona como deseas continuar tu compra</h2>
            </div>
          </div>
          <div class="c-modal__modalBody">
            <div class="row">
              <div class="col-md-12"> 
                <div class="item guest">
                  <h3>Continuar como invitado</h3>
                  <button class="o-btn o-btn--primary o-btn--medium" id="enter" type="button" onclick="sinRegistro()">Comprar</button>
                  <p>Podr�s realizar tus compras sin estar registrado y crear tu contrase�a despu�s. </p>
                </div>
              </div>
              <!--<div class="col-md-6"> 
                <div class="item login">
                  <h3>O como usuario registrado</h3>
                    <div class="row o-form--loginForm">
                      <div class="col-lg-12">
                        <div class="o-form__field">
                        
                          <label class="o-form__label">RUT</label>
                          <input class="o-form__input" id="rutpaso2completo" maxlength="10" required size="30" type="text"><span class="o-form__message"></span>
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <div class="o-form__field">
                          <label class="o-form__label">Contrase�a</label>
                          <html:password property="datos(clavepaso2)" styleClass="o-form__input" maxlength="6" styleId="clavepaso2"/><span class="o-form__message"></span>
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <div class="o-form__field">
                        <button type="button" class="o-btn o-btn--primary o-btn--medium" id="submitPaso2Vehiculo" name="submitPaso2Vehiculo" 
								  onclick="javascript:conRegistro();">Ingresar y continuar</button>
                        </div>
                      </div>
                      <script type="text/javascript">
									<logic:present name="datosCotizacion">
										$("#rutpaso2").val("<bean:write name='datosCotizacion' property='rutCliente'/>");	
										$("#dvpaso2").val("<bean:write name='datosCotizacion' property='dv'/>");
										$("#rutpaso2completo").val($("#rutpaso2").val()+'-'+$("#dvpaso2").val());
										$("#rutpaso2completo").attr("readonly", "true");
									</logic:present>
								  </script>
                    </div>
                </div>
              </div>-->
            </div>
          </div>
        </div>
      </div>
						  <!-- Fin modal nuevo -->
								  <!--<p>
									<input class="form-control" id="rutpaso2completo" maxlength="10" required size="30" type="text">
									<br>
									<html:password property="datos(clavepaso2)" styleClass="form-control" maxlength="6" styleId="clavepaso2"/>
									<br>
								  </p>
								  <button type="button" class="btn btn-primary btn-block" id="submitPaso2Vehiculo" name="submitPaso2Vehiculo" 
								  onclick="javascript:conRegistro();">INGRESAR</button>
								  
								  <a class="small" href="javascript:olvidasteClave();">�Olvidaste tu clave?</a> 
								  <script type="text/javascript">
									<logic:present name="datosCotizacion">
										$("#rutpaso2").val("<bean:write name='datosCotizacion' property='rutCliente'/>");	
										$("#dvpaso2").val("<bean:write name='datosCotizacion' property='dv'/>");
										$("#rutpaso2completo").val($("#rutpaso2").val()+'-'+$("#dvpaso2").val());
										$("#rutpaso2completo").attr("readonly", "true");
									</logic:present>
								  </script>-->
								</div>
							  </div>
							</div>
							  <div class="top15 hidden-lg"></div>
							<!--<div class="panel panel-default col-md-4" style="margin-top:0">
							  <div class="panel-heading" role="tab" id="3">
								<div class="panel-title2"> 
									<a class="btn btn-primary collapsed" role="button" data-toggle="collapse" 
									data-parent="#accordion-botones" aria-expanded="false" href="#collapsebot3"> �ERES NUEVO? </a> 
								</div>
							  </div>
							  <div id="collapsebot3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="3">
								<div class="panel-body">
								  <p class="text-center">
									Reg�strate aqu� y podr�s revisar tus p�lizas contratadas, cotizaciones y cotizar de manera m�s r�pida
								  </p>
								  <a class="btn btn-primary btn-block" onclick="javascript:registroRemoto();" role="button">AQU�</a> </div>
							  </div>
							</div>-->
						  </div>
						</div>
					</div>
					<!--<div class="col-md-3 hidden-xs">
						<button type="button" class="btn btn-primary"
							onclick="switchForm(false)">Volver</button>
					</div>
					<div class="col-md-12 hidden-lg text-center">
						<button type="button" class="btn btn-primary top15"
							onclick="switchForm(false)">Volver</button>
					</div>-->
				<!-- FIN BOTONES COLAPSABLES --> 
				</logic:notPresent>

				<logic:present name="usuario">
				<!-- NUEVOS BOTONES CON LA SESION INICIADA -->
				<div class="col-lg-3"> 
                            <div class="o-form__field is-last"><a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" onclick="switchForm(false)"><i class="mx-arrow-left"></i> Volver </a></div>
                          </div>
                          <div class="col-lg-3 offset-lg-6">
                            <div class="o-form__field is-last">
                              <button class="o-btn o-btn--primary o-btn o-btn--icon" id="send_form_recruiting_step2" type="button" onclick="javascript:submitPaso2Vehiculo();">Siguiente<i class="o-icon">
                                  <svg width="13px" height="14px" viewbox="0 0 13 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                      <g id="botones/orangeButton+rightArrow" transform="translate(-113.000000, -19.000000)" fill-rule="nonzero" fill="#FFFFFF">
                                        <g id="orangeButton+rightArrow">
                                          <g id="arrows" transform="translate(113.000000, 19.000000)">
                                            <path id="Shape" d="M12.6595914,6.18869213 L6.92029216,0.341285393 C6.69713064,0.113730337 6.42956532,0 6.11805938,0 C5.81235867,0 5.54791211,0.113730337 5.32456532,0.341285393 L4.66341805,1.01492135 C4.44004038,1.23052135 4.32838242,1.49998202 4.32838242,1.82336629 C4.32838242,2.14668764 4.44004038,2.41614831 4.66341805,2.63181124 L7.24652732,5.27252584 L1.03990736,5.27252584 C0.734361045,5.27252584 0.486033254,5.38477753 0.295016627,5.60937528 C0.104,5.83397303 0.00849168646,6.10494382 0.00849168646,6.42222472 L0.00849168646,7.57198652 C0.00849168646,7.8893618 0.104030879,8.1602382 0.295016627,8.38483596 C0.486002375,8.60943371 0.734361045,8.72159101 1.03990736,8.72159101 L7.2464038,8.72159101 L4.66332542,11.3535281 C4.43994774,11.5809258 4.32828979,11.8534382 4.32828979,12.1709079 C4.32828979,12.4883146 4.43994774,12.7607955 4.66332542,12.9881933 L5.32447268,13.6618292 C5.5537791,13.8834067 5.81822565,13.9941798 6.11796675,13.9941798 C6.42363658,13.9941798 6.69114014,13.8834067 6.9202304,13.6618292 L12.6594988,7.81442247 C12.8769786,7.59284494 12.9857957,7.32045843 12.9857957,6.9970427 C12.9857957,6.66774382 12.8769786,6.3981573 12.6595914,6.18869213 Z"></path>
                                          </g>
                                        </g>
                                      </g>
                                    </g>
                                  </svg></i></button>
                            </div>
                          </div>
				
				<!-- FIN BOTONES NUEVOS -->
					<!--<div class="row top15">
						<div class="col-md-12" id="continuar-compra">
							<div class="col-md-4 hidden-xs pull-right">
								<button type="button" class="btn btn-primary btn-block"
									onclick="javascript:submitPaso2Vehiculo();">CONTINUAR</button>
							</div>
							<div class="col-md-4 hidden-lg bot15">
								<button type="button" class="btn btn-primary btn-block"
									onclick="javascript:submitPaso2Vehiculo();">CONTINUAR</button>
							</div>
						</div>
						<div class="col-md-3 hidden-xs">
							<button type="button" class="btn btn-primary"
								onclick="switchForm(false)">Volver</button>
						</div>
						<div class="col-md-12 hidden-lg text-center">
							<button type="button" class="btn btn-primary top15"
								onclick="switchForm(false)">Volver</button>
						</div>
					</div>-->
				</logic:present>
				</div>
                        </div>
                        	
							<div class="row top10" id="divErrorNumero"
										style="display: none;">
										<div class="col-sm-12">
											<div class="alert alert-warning">
												<p align="center">Debes confirmar un n�mero</p>
											</div>
										</div>
							</div>
                        <div class="row u-hidden_desktop">
                          <div class="col-12">
                            <!--<div class="c-assistance u-mb30">
                              <h4 class="c-assistance__title">�Necesitas asistencia?</h4><a class="c-assistance__button o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="#">Te llamamos gratis</a>
                              <p class="c-assistance__text">O ll�manos gratis al<strong class="c-assistance__outstanding"> 600 500 5000</strong></p>
                            </div>-->
                          </div>
                        </div>
                    </div>
                    <div class="col-lg-4 u-mobile_first">
                      <section class="c-summary" id="summary_sure">
                        <h2 class="c-summary__title">Resumen de tu seguro</h2>
                        <div class="c-summary__company"></div>
                        <h3 class="c-summary__subtitle"><bean:write name="datosPlan" property="nombrePlan" /></h3>
                        <div class="row">
                          <div class="col-12">
                            <div class="c-summary__cost">
                              <div class="c-summary__label">Monto Total:</div>
                              <div class="c-summary__price"><sup>$</sup><bean:write name="datosPlan" property="primaMensualPesos"
								format="##,##0" locale="currentLocale" /><sub>/Mes*</sub></div>
                            </div>
                            <p class="c-summary__legal"></p>
                          </div>
                          <div class="col-12 u-mobile_second">
                            <div class="o-collapse"><a class="o-collapse__button o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="#" state="0" onclick="collapse(event);"> Ver detalles contrataci�n<i class="o-icon o-icon--triangle"><span class="u-triangle_down"></span></i></a>
                              <div class="o-collapse__content">
                                <div class="c-summary__detail">
                                  <table class="o-table o-table--basic u-mb20">
                                    <thead class="o-table__head">
                                      <tr class="o-table__row">
                                        <th class="o-table__col">Tipo</th>
                                        <th class="o-table__col">Modelo</th>
                                      </tr>
                                    </thead>
                                    <tbody class="o-table__body">
                                      <tr class="o-table__row">
                                        <td class="o-table__col"><bean:write name="tipoDesc" scope="request"/></td>
                                        <td class="o-table__col"><bean:write name="modeloDes" scope="request"/></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <table class="o-table o-table--basic">
                                    <thead class="o-table__head">
                                      <tr class="o-table__row">
                                        <th class="o-table__col">Marca</th>
                                        <th class="o-table__col">A�o</th>
                                      </tr>
                                    </thead>
                                    <tbody class="o-table__body">
                                      <tr class="o-table__row">
                                        <td class="o-table__col"><bean:write name="marcaDesc" scope="request"/></td>
                                        <td class="o-table__col"><bean:write name="datosCotizacion" property="anyoVehiculo"/></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!--<div class="c-assistance u-mb30 u-hidden_mobile">
                          <h4 class="c-assistance__title">�Necesitas asistencia?</h4><a class="c-assistance__button o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="#">Te llamamos gratis</a>
                          <p class="c-assistance__text">O ll�manos gratis al<strong class="c-assistance__outstanding"> 600 500 5000</strong></p>
                        </div>-->
                      </section>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </section>
      </div>
    </main>
							
							
							
							
				</div>
							
				
				</div>
				</html:form>
		</div>
	</div>
	</div>
	<%@ include file="./includes/cotizador-footer.jsp"%>
</body>
</html>