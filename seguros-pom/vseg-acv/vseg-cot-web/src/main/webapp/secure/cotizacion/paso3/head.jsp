<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<html lang="es" xml:lang="es">
<meta http-equiv="Content-Language" content="es" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="" />
<meta name="author" content="" />
<link type="image/x-icon" rel="shortcut icon"
	href="/cotizador/images/btn-img/favicon.ico" />
<!-- Bootstrap core CSS -->
<link href="/vseg-paris/css/bootstrap.min.css" rel="stylesheet" />
<link href="/vseg-paris/css/bootstrap-theme.min.css" rel="stylesheet" />
<!--<link href="/vseg-paris/css/estilos.css" rel="stylesheet" />-->
<link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,700|Varela+Round" rel="stylesheet">
<script src="/vseg-paris/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="/vseg-paris/js/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		  <script>window.location.href='/vseg-paris/html/alerta-ie9.html'</script>
	    <![endif]-->

<script src="/cotizador/js/libs/jquery-3.2.1.min.js"></script>
<script src="/cotizador/js/jquery.session.js" type="text/javascript"></script>

<!-- Important Owl stylesheet -->
<link rel="stylesheet" href="/vseg-paris/css/owl.carousel.css" />

<!-- Default Theme -->
<link rel="stylesheet" href="/vseg-paris/css/owl.theme.css" />
<!-- Include js plugin -->
<script src="/cotizador/js/owl.carousel.js"></script>
<script src="/cotizador/js/Revisa_Sesion_Expirada.js"></script>
<script type="text/javascript">
	$(document).ready(function() {

		var owl = $("#owl-demo");

		owl.owlCarousel({

			items : 6, //10 items above 1000px browser width
			itemsDesktop : [ 1200, 8 ], //5 items between 1000px and 901px
			itemsDesktopSmall : [ 992, 3 ], // 3 items betweem 900px and 601px
			itemsTablet : [ 768, 2 ], //2 items between 600 and 0;
			itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option

		});

		$('.session-expirada').hide();
	});
</script>
<script type="text/javascript">
	//<![CDATA[
	$(document).ready(function() {
		$('.toggle-menu').jPushMenu({
			closeOnClickLink : false
		});
		$('.dropdown-toggle').dropdown();
		$('html, body').animate({
			scrollTop : 0
		}, 0);
		$('[data-toggle="tooltip"]').tooltip();
	});
	//]]>
</script>

<script src="/vseg-paris/js/vendor/bootstrap.min.js"></script>
<script src="/vseg-paris/js/jPushMenu.js"></script>
