<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ page import="java.util.Locale"%>
<%
	session.setAttribute("currentLocale", new Locale("es", "CL"));
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<bean:define id="contextpath" value="<%=request.getContextPath()%>" />
<bean:define id="vsegparispath" value="/vseg-paris" />
<html lang="en">
<head>
<meta http-equiv="Expires" content="0">
<meta http-equiv="Last-Modified" content="0">
<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
<meta http-equiv="Pragma" content="no-cache">
<%@ include file="/cotizacion/includes/cotizador-head.jsp"%>
<%@ include file="./head.jsp"%>
<script src="<bean:write name="contextpath" />/js/Valida_TarParisMas.js"></script>
<script src="<bean:write name="contextpath" />/js/Valida_TarMasEasy.js"></script>
<script src="<bean:write name="contextpath" />/js/Valida_TarJumbo.js"></script>
<script src="<bean:write name="contextpath" />/js/Valida_TarMasCenco.js"></script>
<script
	src="<bean:write name="contextpath" />/js/Valida_TarjetaBanco.js"></script>
	<script src="<bean:write name="contextpath" />/js/cotizacion.js"></script>
<script type="text/javascript">
	var existePrimaUnica = false;
	<logic:present name="existePrimaUnica">
	existePrimaUnica = true;
	</logic:present>

	function bar(val,paso) {
		if(val!== 'undefined' && val >= 0 && val <= 100) {
			if(paso && paso >0) {
				val = (val * 33.35) / 100;
				if(paso == 2) {
					val = val + 33.35
				} else if(paso == 3) {
					val = val + 66.7;
				}
			}
			if(val >= 100) {
				val = 99.95;
			}
			$("[data-percentage]").css("width",val+"%");
		}
	}
	
	function validarCondComerciales() {
		return $('input[name=condcomerciales]').attr('checked');
	}
	
	function infoPersona(){
		
	}
	
	

	function validarCargoRecurrente() {
		var tjta = $("#tjtaRecurrente").val();
		if ("WEBPAY" == tjta) {
			var recurr = $('#cargoR2').attr('checked');
		} else {
			var recurr = $('#cargoR').attr('checked');
		}
		if (recurr != undefined && recurr == false) {
			return false;
		} else {
			return true;
		}
	}

	function resetCargoRecurrente(obj) {
		$("input[type='checkbox'][name='cargoRecurrente']").parent().parent()
				.removeAttr('style').css({
					'width' : '100%'
				}).children('#aceptar-pago').remove();
		$("input[type='checkbox'][name='cargoRecurrente']").parent().parent()
				.children('.fraude-acepto-conte-tex').removeAttr('style').find(
						'p').removeAttr('style').css({
					'font-weight' : 'bolder',
					'text-decoration' : 'underline'
				}).html('Acepto el Pago Autom�tico Mensual de mi Tarjeta');
	}

	function alertaCondComerciales() {
		$('#condcomerciales')
				.parent()
				.parent()
				.children('.fraude-acepto-conte-tex')
				.html(
						'<div class="fraude-acepto-conte-tex" style="background-color:#FFCC99; border: 1px solid #FF9900;">'
								+ '<p style="font-weight:bolder; font-size:11px; margin:5px;">'
								+ '<a href="#">Acepto las Condiciones Comerciales</a>'
								+ '</p>' + '</div>')
				.parent()
				.parent()
				.append(
						'<div id="error" style="float:right; width:auto; margin:10px; margin-top:0px; margin-left:-30px;">&nbsp;<p style="color:#FF8040; margin-left:5px; margin-right:8px;">* Debes aceptar las Condiciones Comerciales.</p></div>');
	}

	function alertaCargoRecurrente() {
		var cargo = $("input[type='checkbox'][name='cargoRecurrente']");

		cargo
				.parent()
				.parent()
				.css({
					'width' : '100%',
					'margin-top' : '8px',
					'margin-bottom' : '8px'
				})
				.prepend(
						'<div id="aceptar-pago" style="float: right; margin-top:10px;"><p style="color:#FF8000">* Debes aceptar el Pago Autom&aacute;tico Mensual.</p></div>');

		cargo
				.parent()
				.parent()
				.children('.fraude-acepto-conte-tex')
				.css({
					'background-color' : '#FFCC99',
					'border' : '1px solid #FF9900'
				})
				.find('p')
				.css({
					'font-weight' : 'bolder',
					'font-size' : '11px',
					'margin' : '5px',
					'text-decoration' : 'underline'
				})
				.html(
						'<a href="#">Acepto el Pago Autom�tico Mensual de mi Tarjeta</a>');
	}

	$(document)
			.ready(
					function() {

						$("#condcomerciales").removeAttr('checked');

												bar(0,3);
						$("#pdf-link")
								.attr("href",
										"/vseg-paris/secure/inicio/obtener-poliza-html.do#cargo_recurrente");
						var condOrig = $('#condcomerciales').parent().parent()
								.children('.fraude-acepto-conte-tex').html();
						function resetCondComerciales(obj) {

							var condCom = $('#condcomerciales').parent()
									.parent().children(
											'.fraude-acepto-conte-tex');

							condCom.html(condOrig).parent().parent().children(
									'#error').remove();

							// 							if (navigator.appName == "Microsoft Internet Explorer") {
							// 								condCom.find(".tooltip_show").tooltip({
							// 									position : "top right",
							// 									offset : [ 30, 3 ]
							// 								});
							// 							} else {
							// 								condCom.find(".tooltip_show").tooltip({
							// 									position : "top right",
							// 									offset : [ 20, 3 ]
							// 								});
							// 							}
						}

						$(".ccnum").keyup(function(e) {
							var name = $(this).attr('name');
							var prefijo = name.substr(0, (name.length - 1));

							var pos = name.replace(prefijo, '') * 1;

							//Valida aceptacion de condiciones comerciales.
							/*if (!validarCondComerciales()) {
								$(this).val('');
								resetCondComerciales($('#condcomerciales'));
								alertaCondComerciales();
								return false;
							}*/

							var code = e.keyCode || e.which;

							if (code == 8) {
								if ($(this).val().length == 0) {
									if ($("#" + prefijo + (pos - 1)) != null) {
										$("#" + prefijo + (pos - 1)).focus();
									}
								}
								return false;
							} else {
								if ($(this).val().length >= 4) {
									if ($("#" + prefijo + (pos + 1)) != null) {
										$("#" + prefijo + (pos + 1)).focus();
									}
								}
							}
						});

						$("#condcomerciales").click(function() {
						
								$("#checkCondComerciales").css("background-color","#f6f6f6");
								$("#checkCondComerciales").css("border-radius","0px")
							if ($("#condcomerciales").attr("checked")) {
								$("#condcomerciales").removeAttr("checked");
							} else {
								$("#condcomerciales").attr("checked", "checked");
							}
							// 												$('#pdf-link')
							// 														.attr("href",
							// 																"/vseg-paris/secure/inicio/obtener-poliza-html.do#condiciones_comerciales");
							// 												$('#iframe_propuesta_seguro')
							// 														.attr("src",
							// 																"/vseg-paris/secure/inicio/obtener-poliza-html.do#condiciones_comerciales");
							resetCondComerciales($(this));
							
						});

						$("input[name='cargoRecurrente']").click(function() {
							if ($("input[name='cargoRecurrente']").attr("checked")) {
								$("input[name='cargoRecurrente']").removeAttr("checked");
							} else {
								$("input[name='cargoRecurrente']").attr("checked", "checked");
							}
							// 												$('#pdf-link')
							// 														.attr("href",
							// 																"/vseg-paris/secure/inicio/obtener-poliza-html.do#cargo_recurrente");
							// 												$('#iframe_propuesta_seguro')
							// 														.attr("src",
							// 																"/vseg-paris/secure/inicio/obtener-poliza-html.do#cargo_recurrente");
							resetCargoRecurrente($(this));
						});

						// Tooltip.
						// 						if (navigator.appName == "Microsoft Internet Explorer") {
						// 							$(".tooltip_show").tooltip({
						// 								position : "top right",
						// 								offset : [ 30, 3 ]
						// 							});
						// 						} else {
						// 							$(".tooltip_show").tooltip({
						// 								position : "top right",
						// 								offset : [ 20, 3 ]
						// 							});
						// 						}

						// 						$("#tool1").tooltip({
						// 							position : "top center"
						// 						}); // Tooltip tarjeta bancaria y tmas.
						// 						$("#tool2").tooltip({
						// 							position : "top center"
						// 						}); // Tooltip tarjeta bancaria y tmas.

						// 						$('#content-formulario2').jqm({
						// 							modal : 'true',
						// 							overlay : 55,
						// 							trigger : 'a#pruebaFancyBox'
						// 						});

						$(".tarjeta-mas-container").hide();
						$(".tarjeta-credito-container").hide();

						//Deshabilitar pago.
						deshabilitaMedio("visaCenco");
						deshabilitaMedio("masterCenco");
						deshabilitaMedio("visa");
						deshabilitaMedio("mastercard");
						deshabilitaMedio("master");
						deshabilitaMedio("american");
						deshabilitaMedio("REDCOMPRA");
						deshabilitaMedio("paris");
						deshabilitaMedio("easy");
						deshabilitaMedio("jumbo");

						deshabilitaMedio("tarjetacenco");
						var idRama = <bean:write name="idRama" scope="request"/>;
						var idPlan = <bean:write name="idPlan" scope="request"/>;

						if (idPlan == '2105') {
							$("#tool1").hide();
						}

						//Habilitar medios disponibles.
						<logic:iterate id="formaPago" name="hFormaPago" type="java.util.Map.Entry">
						<bean:define id="formaPagoKey" name="formaPago" property="key" />
						<bean:define id="formaPagoValue" name="formaPago" property="value" />

						//<bean:write name="formaPagoKey" /> : <bean:write name="formaPagoValue" />
						<logic:equal value="WEBPAY" name="formaPagoKey">
						<logic:equal value="1" name="formaPagoValue">
						//habilitar webpay.
						habilitaMedio("visaCenco");
						habilitaMedio("masterCenco");
						habilitaMedio("visa");
						habilitaMedio("mastercard");
						habilitaMedio("master");
						habilitaMedio("american");
						habilitaMedio("REDCOMPRA");
						</logic:equal>
						</logic:equal>

						<logic:equal value="PARISMAS" name="formaPagoKey">
						<logic:equal value="1" name="formaPagoValue">
						//habilitar paris.
						habilitaMedio("paris");
						</logic:equal>
						</logic:equal>

						<logic:equal value="EASY" name="formaPagoKey">
						<logic:equal value="1" name="formaPagoValue">
						//habilitar paris.
						habilitaMedio("easy");
						</logic:equal>
						</logic:equal>

						<logic:equal value="JUMBO" name="formaPagoKey">
						<logic:equal value="1" name="formaPagoValue">
						//habilitar paris.
						habilitaMedio("jumbo");
						</logic:equal>
						</logic:equal>

						<logic:equal value="CENCOSUD" name="formaPagoKey">

						<logic:equal value="1" name="formaPagoValue">

						//habilitar paris.

						habilitaMedio("tarjetacenco");

						</logic:equal>

						</logic:equal>
						</logic:iterate>

						$("#elige_medio_pago").show();

						function habilitaMedio(id) {
							$("#" + id).show();
						}

						function deshabilitaMedio(id) {
							$("#" + id).hide();
						}

						$('.ocultar').hide();

						//Setear ambos cargos recurrentes con el mismo valor.
						navegadorNom = navigator.appName;
						navegadorVer = navigator.appVersion;
						if (navegadorNom.indexOf("Explorer") != -1
								&& navegadorVer.indexOf("MSIE 8.0") != -1) {
							$("#cargoR").click(function() {
								$("#tjtaRecurrente").val("MAS");
								var newval = $("#cargoR").attr('checked');
							});
							$("#cargoR2").click(function() {
								$("#tjtaRecurrente").val("WEBPAY");
								var newval = $("#cargoR2").attr('checked');
							});
							var newval = $(this).attr('checked');
							$("input[type='checkbox'][name='cargoRecurrente']")
									.each(function() {
										$(this).attr("checked", newval);
									});
						} else {
							$("input[type='checkbox'][name='cargoRecurrente']")
									.click(
											function() {
												var newval = $(this).attr(
														'checked');
												$(
														"input[type='checkbox'][name='cargoRecurrente']")
														.each(
																function() {
																	$(this)
																			.attr(
																					"checked",
																					newval);
																});
											});
						}

						$("input[type='radio'][name='mediopago']")
								.click(
										function() {
											$('.ocultar').show();
											tipoTarjeta = $(this).val();
											if (tipoTarjeta == 'PARISMAS'
													|| tipoTarjeta == 'EASY'
													|| tipoTarjeta == 'JUMBO'
													|| tipoTarjeta == 'CENCOSUD') { //AGREGAR TARJETA CENCOSUD
												$(".tarjeta-mas-container")
														.show();
												$(".tarjeta-credito-container")
														.hide();

												$("#texto_tooltop_medio_tbanco")
														.hide();
												$("#texto_tooltop_medio_tmas")
														.show();

											} else if(tipoTarjeta == 'REDCOMPRA'){
												$(".tarjeta-mas-container")
														.hide();
												$(".tarjeta-credito-container")
														.show();

												$("#texto_tooltop_medio_tmas")
														.hide();
												$("#texto_tooltop_medio_tbanco")
														.show();

												tipoTarjeta = 'REDCOMPRA';
											}else{
												$(".tarjeta-mas-container")
														.hide();
												$(".tarjeta-credito-container")
														.show();

												$("#texto_tooltop_medio_tmas")
														.hide();
												$("#texto_tooltop_medio_tbanco")
														.show();

												tipoTarjeta = 'WEBPAY';
											}
											$("#mensaje_selecciona_medio_pago")
													.hide();

											//Forma de pago.
											setFormaDePago(tipoTarjeta);
										});
										
						$('body').on('click', '.js-collapse', function(event) {
							 $(this).toggleClass('is_active');
							 //$(this).next().slideToggle();
						});

						function setFormaDePago(tipoTarjeta) {
							$("#tjtaRecurrente").val(tipoTarjeta);
							$
									.getJSON(
											"/vseg-paris/secure/inicio/actualizar-forma-pago.do",
											{
												tipoTarjeta : tipoTarjeta,
												ajax : 'true'
											},
											function(j) {
												$("#iframe_propuesta_seguro")
														.attr(
																"src",
																"/vseg-paris/secure/inicio/obtener-poliza-html.do?"
																		+ Math
																				.round(new Date()
																						.getTime() / 1000));
											});
						}

						$("#contratar").click(function() {
							<logic:present name="existePrimaUnica">
								if ($('input[name="mediopago"]:checked') && $("#condcomerciales").attr("checked")) {
												$("#mediopago").css("border-color","#cecece");
												$("#checkCondComerciales").css("background-color","#f6f6f6");
												$("#checkCondComerciales").css("border-radius","0px")
							</logic:present>
							<logic:notPresent name="existePrimaUnica">
								if ($('input[name="mediopago"]:checked') && $("#condcomerciales").attr("checked")&& $("input[name='cargoRecurrente']").attr("checked")) {
												$("#mediopago").css("border-color","#cecece");
												$("#checkCondComerciales").css("background-color","#f6f6f6");
												$("#checkCondComerciales").css("border-radius","0px")
												$("#checkCargoRecurrente").css("background-color","#f6f6f6");
												$("#checkCargoRecurrente").css("border-radius","0px")
												$("#checkCargoRecurrente2").css("background-color","#f6f6f6");
												$("#checkCargoRecurrente2").css("border-radius","0px")
												
												
							</logic:notPresent>
							
												$("#warning-chackbox").hide("fast");
												var _error = false;
												$("#pagoBloqueado").show();

												$("#pagoNoBloqueado").hide();

												if (!validarCondComerciales()) {
													$("#pagoBloqueado").hide();
													$("#pagoNoBloqueado")
															.show();
													alertaCondComerciales();
													_error = true;
												}
												if (!validarCargoRecurrente()) {
													$("#pagoBloqueado").hide();
													$("#pagoNoBloqueado")
															.show();
													alertaCargoRecurrente();
													_error = true;
													return false;
												}

												resetCondComerciales($('#condcomerciales'));

												resetCargoRecurrente($("input[type='checkbox'][name='cargoRecurrente']"));
												var nroTarjeta = "";
												var cargoRecurrente = $(
														"input[type='checkbox'][name='cargoRecurrente']")
														.attr("value");
														
												// Nueva forma de pago
												var fopId = $("#fopId").val();

												//Compra sin registro
												var aceptoSinRegistro = $(
														"input[type='checkbox'][name='aceptoSinRegistro']:checked")
														.attr("value");

												//Validar tarjeta.
												var esValida = false;
												var tipoTarjeta = $(
														"input[name='mediopago']:checked")
														.val();
												form = document.tarjeta;
												if (tipoTarjeta == 'PARISMAS') {
													esValida = validaBancnumMasParis(form);
													nroTarjeta = form.ccnum_mas1.value
															+ form.ccnum_mas2.value
															+ form.ccnum_mas3.value
															+ form.ccnum_mas4.value;
												} else if (tipoTarjeta == 'EASY') {
													esValida = validaBancnumMasEasy(form);
													nroTarjeta = form.ccnum_mas1.value
															+ form.ccnum_mas2.value
															+ form.ccnum_mas3.value
															+ form.ccnum_mas4.value;
												} else if (tipoTarjeta == 'JUMBO') {
													esValida = validaBancnumMasJumbo(form);
													nroTarjeta = form.ccnum_mas1.value
															+ form.ccnum_mas2.value
															+ form.ccnum_mas3.value
															+ form.ccnum_mas4.value;
												} else if (tipoTarjeta == 'CENCOSUD') {
													esValida = validaBancnumMasCenco(form);
													nroTarjeta = form.ccnum_mas1.value
															+ form.ccnum_mas2.value
															+ form.ccnum_mas3.value
															+ form.ccnum_mas4.value;
												} else if (tipoTarjeta == 'Visa'
														|| tipoTarjeta == 'MasterCard'
														|| tipoTarjeta == 'Diners Club'
														|| tipoTarjeta == 'VisaCenco'
														|| tipoTarjeta == 'MasterCenco'
														|| tipoTarjeta == 'REDCOMPRA'
														|| tipoTarjeta == 'American Express') {

													if (!existePrimaUnica) {
														//if (form.cardNumber.value == '') {
															form.cardNumber.value = form.ccnum1.value
																	+ form.ccnum2.value
																	+ form.ccnum3.value
																	+ form.ccnum4.value;
														//}
													}
													
													nroTarjeta = form.cardNumber.value;
													if (!existePrimaUnica) {
														esValida = validaBancnum(form);
													} else {
														esValida = existePrimaUnica;
														
													//	VALIDA RED COMPRA
														if (tipoTarjeta == 'REDCOMPRA') {
															nroTarjeta = "RC";
														}  
														
														if (tipoTarjeta == 'Visa'
																|| tipoTarjeta == 'MasterCard'
																|| tipoTarjeta == 'Diners Club'
																|| tipoTarjeta == 'VisaCenco'
																|| tipoTarjeta == 'MasterCenco'
																|| tipoTarjeta == 'American Express') {
															nroTarjeta = "WPAY";
														}
													}
												} else if (tipoTarjeta == 'American Express') {
													if (form.cardNumber.value == '') {
														form.cardNumber.value = form.ccnum1.value
																+ form.ccnum2.value
																+ form.ccnum3.value
																+ form.ccnum4.value;
													}
													nroTarjeta = form.cardNumber.value;
													if (!existePrimaUnica) {
														esValida = isValidCreditCard(
																tipoTarjeta,
																form.cardNumber.value);
														if (!esValida) {
															alert('N� Tarjeta Inv�lida, Favor Reintente el ingreso.');
															$("#ccnum1").focus();
														}
													} else {
														esValida = existePrimaUnica;
													}
												} else {
													$("#pagoBloqueado").hide();
													$("#pagoNoBloqueado")
															.show();
													alert('Debe seleccionar un tipo de tarjeta')
												}

												if(tipoTarjeta == 'REDCOMPRA'){
													tipoTarjeta = 'REDCOMPRA';
												}

												if (tipoTarjeta == 'Visa'
																|| tipoTarjeta == 'MasterCard'
																|| tipoTarjeta == 'Diners Club'
																|| tipoTarjeta == 'VisaCenco'
																|| tipoTarjeta == 'MasterCenco'
																|| tipoTarjeta == 'American Express') {
															tipoTarjeta = 'WEBPAY';
														}
											
												
												if (esValida) {
													$
															.getJSON(
																	"<html:rewrite action="/grabar-datos-pago" module="/secure"/>",
																	{
																		nroTarjeta : nroTarjeta,
																		tipoTarjeta : tipoTarjeta,
																		cargoRecurrente : cargoRecurrente,
																		aceptoSinRegistro : aceptoSinRegistro,
																		fopId : fopId,
																		ajax : 'true'
																	},
																	function(j) {
																		if (j != null
																				&& j.status == 'ok') {

																			if (tipoTarjeta == 'WEBPAY' ) {
																				
																				// se apenda action para pago
																				if(j.urlWebpay!= null && j.urlWebpay != ''){
																				  $("#pago").attr('action', j.urlWebpay);
																				  $("#paymentId").val(j.paymentId);
																				 }
																		
																				<logic:present name="existePrimaUnica">
																				$(
																						"#pago")
																						.submit();
																				</logic:present>
																				<logic:notPresent name="existePrimaUnica">
																				var monto = $(
																						"#pago :input[name='montoProporcional']")
																						.val();
																				$(
																						"#pago :input[name='TBK_MONTO']")
																						.attr(
																								'value',
																								monto);
																				$(
																						"#pago")
																						.submit();
																				</logic:notPresent>
																			} else if(tipoTarjeta == 'REDCOMPRA' ){
																				console.log(j.urlWebpay);
																				if(j.urlWebpay!= null && j.urlWebpay != ''){
																				  $("#pago").attr('action', j.urlWebpay);
																				  $("#paymentId").val(j.paymentId);
																				 }
																			
																				<logic:present name="existePrimaUnica">
																				$(
																						"#pago")
																						.submit();
																				</logic:present>
																				<logic:notPresent name="existePrimaUnica">
																				var monto = $(
																						"#pago :input[name='montoProporcional']")
																						.val();
																				$(
																						"#pago :input[name='TBK_MONTO']")
																						.attr(
																								'value',
																								monto);
																				$(
																						"#pago")
																						.submit();
																				</logic:notPresent>
																			}else{
																				var monto = $(
																						"#pago :input[name='TBK_MONTO']")
																						.val();
																				$
																						.getJSON(
																								"<html:rewrite action="/pagar-cencosud" module="/secure"/>",
																								{
																									tipoTarjeta : tipoTarjeta,
																									cargoRecurrente : cargoRecurrente,
																									nroTarjeta : nroTarjeta,
																									monto : monto,
																									ajax : 'true'
																								},
																								function(
																										i) {

																									if (i != null
																											&& i.status == 'ok') {
																										location.href = "<html:rewrite action="/exito-cencosud" module="/secure" />";
																									} else if (i.status == '1') {
																										$(
																												"#pagoBloqueado")
																												.hide();
																										$(
																												"#pagoNoBloqueado")
																												.show();
																										alert('No fue posible realizar el cargo en su tarjeta, favor comunicarse con el call center');
																									} else if (i.status == '2') {
																										$(
																												"#pagoBloqueado")
																												.hide();
																										$(
																												"#pagoNoBloqueado")
																												.show();
																										alert('No fue posible realizar el cargo en su tarjeta, favor comunicarse con el call center');
																									} else if (i.status == '3') {
																										$(
																												"#pagoBloqueado")
																												.hide();
																										$(
																												"#pagoNoBloqueado")
																												.show();
																										alert('El rut asociado a la tarjeta no es igual al rut del contratante');
																									} else if (i.status == '4') {
																										$(
																												"#pagoBloqueado")
																												.hide();
																										$(
																												"#pagoNoBloqueado")
																												.show();
																										alert('La tarjeta se encuentra bloqueada');
																									} else if (i.status == '5') {
																										$(
																												"#pagoBloqueado")
																												.hide();
																										$(
																												"#pagoNoBloqueado")
																												.show();
																										alert('No fue posible realizar el cargo en su tarjeta, favor comunicarse con el call center');
																									} else {
																										$(
																												"#pagoBloqueado")
																												.hide();
																										$(
																												"#pagoNoBloqueado")
																												.show();
																										alert('Error al procesar la solicitud, intente nuevamente 2');
																									}
																								});
																			}
																		} else {
																			$(
																					"#pagoBloqueado")
																					.hide();

																			$(
																					"#pagoNoBloqueado")
																					.show();
																			alert('Error al procesar la solicitud, intente nuevamente');
																		}
																	});
												} else {
													$("#ccnum1").focus();
													$("#pagoBloqueado").hide();
													$("#pagoNoBloqueado")
															.show();
												}
												return false;
											} else {
											
												$("#mediopago").css("border-color","#cecece");
												$("#checkCondComerciales").css("background-color","#f6f6f6");
												$("#checkCondComerciales").css("border-radius","0px")
												$("#checkCargoRecurrente").css("background-color","#f6f6f6");
												$("#checkCargoRecurrente").css("border-radius","0px")
											
												$("#warning-chackbox").show(
														"fast");
														
											if (!$('input[name="mediopago"]:checked')){
												
												$(".chekk").focus();
											}
											if (!$("#condcomerciales").attr("checked")){
												$("#checkCondComerciales").css("background-color","rgba(217, 83, 79, 0.45)");
												$("#checkCondComerciales").css("border-radius","5px")
												$("#condcomerciales").focus();
											}	
											
											
											
											<logic:notPresent name="existePrimaUnica">
											var tipoTarjeta1 = $(
														"input[name='mediopago']:checked")
														.val();
											if (tipoTarjeta1 == 'PARISMAS' || tipoTarjeta1 == 'EASY' || tipoTarjeta1 == 'JUMBO' ||tipoTarjeta1 == 'CENCOSUD') {
											
												if(!$("#cargoRecurrente2").attr("checked")){
													$("#checkCargoRecurrente2").css("background-color","rgba(217, 83, 79, 0.45)");
													$("#checkCargoRecurrente2").css("border-radius","5px")
													$("#cargoRecurrente2").focus();
												}
											
											}
											else {
											
												if(!$("#cargoRecurrente").attr("checked")){
													$("#checkCargoRecurrente").css("background-color","rgba(217, 83, 79, 0.45)");
													$("#checkCargoRecurrente").css("border-radius","5px")
													$("#cargoRecurrente").focus();
												}
											}
											</logic:notPresent>
											}
										});
					});


</script>
</head>
<body>
<script type="text/javascript">
	<% if(request.getSession().getAttribute("fechaMaxima") != null && request.getSession().getAttribute("fechaMaxima").equals("true")){%>
		window.location.href = "/cotizador/ingresar-datos-fraude.do";
	<%}%>
</script>
	<%@ include file="/google-analytics/google-analytics.jsp"%>
	<%@ include file="/cotizacion/includes/cotizador-header.jsp"%>
	<div class="container contenedor-sitio">
		<%@ include file="/cotizacion/includes/cotizador-breadcrumb.jsp"%>
		<!-- INICIO  CONTENIDOS -->
		<main role="main">
		<logic:equal value="1" name="idRama">
							<bean:define id="volverPaso2" value="/cotizador/ingresar-datos-vehiculo.do"></bean:define>
						</logic:equal>
						<logic:equal value="2" name="idRama">
							<bean:define id="volverPaso2" value="/cotizador/ingresar-datos-hogar.do"></bean:define>
						</logic:equal>
						<logic:equal value="3" name="idRama">
							<bean:define id="volverPaso2" value="/cotizador/ingresar-datos-vida.do"></bean:define>
						</logic:equal>
						<logic:equal value="4" name="idRama">
							<bean:define id="volverPaso2" value="/cotizador/ingresar-datos-salud.do"></bean:define>
						</logic:equal>
						<logic:equal value="5" name="idRama">
							<bean:define id="volverPaso2" value="/cotizador/ingresar-datos-cesantia.do"></bean:define>
						</logic:equal>
						<logic:equal value="6" name="idRama">
							<bean:define id="volverPaso2" value="/cotizador/ingresar-datos-fraude.do"></bean:define>
						</logic:equal>
						<logic:equal value="7" name="idRama">
							<bean:define id="volverPaso2" value="/cotizador/ingresar-datos-fraude.do"></bean:define>
						</logic:equal>
						<logic:equal value="8" name="idRama">
							<bean:define id="volverPaso2" value="/cotizador/ingresar-datos-vehiculo.do"></bean:define>
						</logic:equal>
      <div class="remodal-bg">
        <section class="container">
          <div class="row">
            <div class="col-12">
				<logic:equal name="idRama" value="1">
				      	<logic:equal name="idSubcategoria" value="22"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Full Cobertura</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="21"><h1 class="o-title o-title--primary">Cotizaci�n Seguro P�rdida Total</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="36"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Obligatorio Mercosur</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="79"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Robo Contenido</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="2">
				      	<logic:equal name="idSubcategoria" value="24"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Sismo / Estructura</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="43"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Sismo / Contenido</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="44"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio y Robo</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="25"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Incendio, Robo y Sismo</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="26"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Premio a la Permanencia</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="201"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Full Asistencia</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="220"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Hogar Vacaciones</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="3">
				      	<logic:equal name="idSubcategoria" value="29"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Viaje Grupal</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="5">
				      	<logic:equal name="idSubcategoria" value="139"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Fraude</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="159"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Fraude con devoluci�n</h1></logic:equal>
			      	</logic:equal>
            		<logic:equal name="idRama" value="6">
				      	<logic:equal name="idSubcategoria" value="119"><h1 class="o-title o-title--primary">Asistencia Viaje</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="140"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Mascota</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="221"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Viaje Grupal</h1></logic:equal>
			      	</logic:equal>
					<logic:equal name="idRama" value="7">
						<logic:equal name="idSubcategoria" value="161"><h1 class="o-title o-title--primary">Hospitalizaci�n con devoluci�n</h1></logic:equal>
				      	<logic:equal name="idSubcategoria" value="162"><h1 class="o-title o-title--primary">Accidentes Personales con devoluci�n</h1></logic:equal>
						<logic:equal name="idSubcategoria" value="164"><h1 class="o-title o-title--primary">Oncol�gico con devoluci�n</h1></logic:equal>
			      	</logic:equal>
			      	<logic:equal name="idRama" value="8">
				      	<logic:equal name="idSubcategoria" value="239"><h1 class="o-title o-title--primary">Cotizaci�n Seguro Obligatorio Mercosur</h1></logic:equal>
			      	</logic:equal>
			</div>
            <div class="col-12">
              <section class="o-box o-box--recruiting u-mtb50">
                <div class="o-steps">
                  <div class="o-steps__item o-steps__item--Completed"><span class="o-steps__milestone"><span class="o-steps__value" style="background-image: url(/vseg-paris/img/step_complete.png);">1</span></span>
                    <h4 class="o-steps__title">Informaci�n contratante</h4>
                  </div>
                  <div class="o-steps__item o-steps__item--Completed"><span class="o-steps__milestone"><span class="o-steps__value" style="background-image: url(/vseg-paris/img/step_complete.png);">2</span></span>
                    <h4 class="o-steps__title">Datos del seguro</h4>
                  </div>
                  <div class="o-steps__item o-steps__item--current"><span class="o-steps__milestone"><span class="o-steps__value">3</span></span>
                    <h4 class="o-steps__title">Confirmaci�n y pago</h4>
                  </div>
                </div>
                <div class="u-pad20">
                  <div class="row o-form o-form--standard o-form--linear">
                    <div class="col-lg-8 u-mobile_second"> 
                        <h2 class="o-title o-title--subtitle u-mb10 js-collapse">Resumen datos personales contratante<i class="mx-chevron-down"></i></h2>
                        <div class="o-collapse--big">
                          <p class="o text">Descarga tu propuesta de seguro y selecciona medio de pago</p>
                          <div class="row">
                            <div class="col-12">
                              <div class="c-summary c-summary--data"> 
                                <div class="row">
                                  <div class="col-4">
                                    <dl class="o-description">
                                      <dt class="o-description__term">RUT</dt>
                                      <dd class="o-description__def"><bean:write name="datosCotizacion" property="rutCliente"
										format="#,###" locale="currentLocale" /></dd>
                                    </dl>
                                  </div>
                                  <div class="col-4">
                                    <dl class="o-description">
                                      <dt class="o-description__term">Nombre</dt>
                                      <dd class="o-description__def"><bean:write name="datosCotizacion" property="nombre" /></dd>
                                    </dl>
                                  </div>
                                  <div class="col-4">
                                    <dl class="o-description">
                                      <dt class="o-description__term">Apellidos</dt>
                                      <dd class="o-description__def"><bean:write name="datosCotizacion" property="apellidoPaterno" /> <bean:write name="datosCotizacion" property="apellidoMaterno" /></dd>
                                    </dl>
                                  </div>
                                  <div class="col-4">
                                    <dl class="o-description">
                                      <dt class="o-description__term">Tel�fono</dt>
                                      <dd class="o-description__def"><bean:write name="datosCotizacion" property="numeroTelefono1" /></dd>
                                    </dl>
                                  </div>
                                  <div class="col-4">
                                    <dl class="o-description">
                                      <dt class="o-description__term">Email</dt>
                                      <dd class="o-description__def"><bean:write name="datosCotizacion" property="email" /></dd>
                                    </dl>
                                  </div>
                                  <div class="col-4">
                                    <dl class="o-description">
                                      <dt class="o-description__term">Fecha nacimiento</dt>
                                      <dd class="o-description__def"><bean:write name="datosCotizacion" property="fechaNacimiento" format="dd/MM/yyyy" /></dd>
                                    </dl>
                                  </div>
                                  <div class="col-4">
                                    <dl class="o-description">
                                      <dt class="o-description__term">Estado civil</dt>
                                      <dd class="o-description__def"><bean:write name="datosCotizacion" property="estadoCivilDesc" /></dd>
                                    </dl>
                                  </div>
                                  <div class="col-4">
                                    <dl class="o-description">
                                      <dt class="o-description__term">Sexo</dt>
                                      <dd class="o-description__def">
                                      <logic:equal value="M" name="datosCotizacion" property="sexo">Masculino</logic:equal>
									  <logic:equal value="F" name="datosCotizacion" property="sexo">Femenino</logic:equal>
                                      </dd>
                                    </dl>
                                  </div>
                                  <div class="col-4">
                                    <dl class="o-description">
                                      <dt class="o-description__term">Regi�n</dt>
                                      <dd class="o-description__def"><bean:write name="regionDescripcion" /></dd>
                                    </dl>
                                  </div>
                                  <div class="col-4">
                                    <dl class="o-description">
                                      <dt class="o-description__term">Ciudad</dt>
                                      <dd class="o-description__def"><bean:write name="ciudadDescripcion" /> </dd>
                                    </dl>
                                  </div>
                                  <div class="col-4">
                                    <dl class="o-description">
                                      <dt class="o-description__term">Comuna</dt>
                                      <dd class="o-description__def"><bean:write name="comunaDescripcion" /></dd>
                                    </dl>
                                  </div>
                                  <div class="col-4">
                                    <dl class="o-description">
                                      <dt class="o-description__term">Direcci�n</dt>
                                      <dd class="o-description__def">
                                      <bean:write name="datosCotizacion" property="direccion" />
									  <bean:write name="datosCotizacion" property="numeroDireccion" />
									  <bean:write name="datosCotizacion" property="numeroDepto" />
                                      </dd>
                                    </dl>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row u-mtb20">
                          <div class="col-lg-8">
                            <h2 class="o-title o-title--subtitle u-mb10">Propuesta de Seguro</h2>
                            <p class="o text">Mant�nte informado y descarga tu p�liza, en ella encontrar�s el detalle del seguro que has contratado con Seguros Cencosud.</p>
                          </div>
                          <div class="col-lg-4"><a class="o-btn o-btn--primary o-btn o-btn--icon" id="pdf-link" href="#" target="_blank"><i class="o-icon o-icon--left">
                                <svg width="18px" height="19px" viewbox="0 0 18 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                  <g id="icon_download" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g id="Desktop" transform="translate(-627.000000, -920.000000)" fill-rule="nonzero" fill="#FFFFFF">
                                      <g id="orangeButton+rightArrow" transform="translate(604.000000, 904.502137)">
                                        <g id="download-icon" transform="translate(23.000000, 16.000000)">
                                          <path id="Shape" d="M16.514832,9.960384 C15.719568,9.960384 15.074832,10.60512 15.074832,11.400384 L15.074832,15.077616 L2.882832,15.077616 L2.882832,11.400384 C2.882832,10.60512 2.238096,9.960384 1.442832,9.960384 C0.647568,9.960384 0.002832,10.60512 0.002832,11.400384 L0.002832,16.517616 C0.002832,17.31288 0.647568,17.957616 1.442832,17.957616 L16.514832,17.957616 C17.310096,17.957616 17.954832,17.31288 17.954832,16.517616 L17.954832,11.400384 C17.954832,10.605072 17.310096,9.960384 16.514832,9.960384 Z"></path>
                                          <path id="Shape" d="M12.026256,5.197776 L10.418832,6.8052 L10.418832,1.44 C10.418832,0.644736 9.774096,0 8.978832,0 C8.183568,0 7.538832,0.644736 7.538832,1.44 L7.538832,6.8052 L5.931408,5.197776 C5.650272,4.91664 5.281728,4.776 4.913184,4.776 C4.54464,4.776 4.176144,4.91664 3.89496,5.197776 C3.33264,5.760192 3.33264,6.671904 3.89496,7.234224 L7.960608,11.299872 C8.522928,11.862288 9.434736,11.862288 9.997056,11.299872 L14.062704,7.234224 C14.62512,6.671904 14.62512,5.760192 14.062704,5.197776 C13.500384,4.63536 12.588576,4.63536 12.026256,5.197776 Z"></path>
                                        </g>
                                      </g>
                                    </g>
                                  </g>
                                </svg></i>Descargar P�liza</a></div>
                        </div>
                        <div class="row u-mb20">
                          <div class="col-12">
                            <h2 class="o-title o-title--subtitle u-mb15">Confirmaci�n y pago</h2>
                          </div>
                          <div class="col-12">
                            <div class="o-form__field o-form__field--inline">
                              <label class="o-form__label">Tarjeta de cr�dito CENCOSUD</label>
                              <div class="o-form__sub_field"><span class="o-radio o-radio--inline"> 
                                  <input class="" type="radio" required="" name="mediopago" id="mediopago" value="PARISMAS">
                                  <img src="/cotizador/images/pagos/tarjeta-paris.jpg" alt=""></span></div>
                              <div class="o-form__sub_field"><span class="o-radio o-radio--inline"> 
                                  <input class="" type="radio" required="" name="mediopago" id="mediopago" value="EASY">
                                  <img src="/cotizador/images/pagos/tarjetaeasy.jpg" alt=""></span></div>
                              <div class="o-form__sub_field"><span class="o-radio o-radio--inline"> 
                                  <input class="" type="radio" required="" name="mediopago" id="mediopago" value="JUMBO">
                                  <img src="/cotizador/images/pagos/tarjeta-jumbo.jpg" alt=""></span></div>
                              <div class="o-form__sub_field"><span class="o-radio o-radio--inline"> 
                                  <input class="" type="radio" required="" name="mediopago" id="mediopago" value="CENCOSUD">
                                  <img src="/cotizador/images/pagos/tarjetaCencosud.jpg" alt=""></span></div>
                            </div>
                          </div>
                          <div class="col-12">
                            <div class="o-form__field o-form__field--inline">
                              <label class="o-form__label">Otras tarjetas de cr�dito</label>
                              
                              <!-- KCC -->
                              <input type="hidden" id="fopId" value="<bean:write name="fopId" />"></input>
                              <input type="hidden" name="TBK_ID_SESION" value="<bean:write name="idSesion"/>" /> 
                              <!-- FIN KCC -->
                              
                              <div class="o-form__sub_field"><span class="o-radio o-radio--inline"> 
                                  <input class="" type="radio" required="" name="mediopago" id="mediopago" value="VisaCenco">
                                  <img src="/cotizador/images/pagos/tarjeta_visa_cenco.jpg" alt=""></span></div>
                              <div class="o-form__sub_field"><span class="o-radio o-radio--inline"> 
                                  <input class="" type="radio" required="" name="mediopago" id="mediopago" value="MasterCenco">
                                  <img src="/cotizador/images/pagos/tarjeta_master_cenco.jpg" alt=""></span></div>
                              <div class="o-form__sub_field"><span class="o-radio o-radio--inline"> 
                                  <input class="" type="radio" required="" name="mediopago" id="mediopago" value="Visa">
                                  <img src="/cotizador/images/pagos/tarjeta_visa.jpg" alt=""></span></div>
                              <div class="o-form__sub_field"><span class="o-radio o-radio--inline"> 
                                  <input class="" type="radio" required="" name="mediopago" id="mediopago" value="MasterCard">
                                  <img src="/cotizador/images/pagos/mastercard.jpg" alt=""></span></div>
                              <div class="o-form__sub_field"><span class="o-radio o-radio--inline"> 
                                  <input class="" type="radio" required="" name="mediopago" id="mediopago" value="American Express">
                                  <img src="/cotizador/images/pagos/tarjeta-master.jpg" alt=""></span></div>
                              <div class="o-form__sub_field"><span class="o-radio o-radio--inline"> 
                                  <input class="" type="radio" required="" name="mediopago" id="mediopago" value="Diners Club">
                                  <img src="/cotizador/images/pagos/tarjeta-amer.jpg" alt=""></span></div>
                            </div>
                          </div>
                          <logic:present name="existePrimaUnica">
                          <div class="col-12">
                            <div class="o-form__field o-form__field--inline is-last">
                              <label class="o-form__label">Otras medios de pago</label>
                                  
										<div class="o-form__sub_field"><span class="o-radio o-radio--inline">
		                                  <input class="" type="radio" required="" name="mediopago" id="mediopago" value="REDCOMPRA">
		                                  <img src="/cotizador/images/pagos/redcompra.jpg" alt=""></span></div>
									
                              <div class="u-clearfix"></div><span class="o-form__message" id="card_field"></span>
                            </div>
                          </div>
                          </logic:present>
                        </div>
                        <form action="" name="tarjeta" id="tarjeta">
                        <div class="row tarjeta-mas-container">
                          <div class="col-12"> </div>
                          <div class="col-lg-4">
                            <dl class="o-description o-description--highlight_def">
                              <dt class="o-description__term">Nombre Titular Tarjeta</dt>
                              <dd class="o-description__def">
                              <bean:write name="datosCotizacion" property="contratante.nombre" /> <bean:write name="datosCotizacion" property="contratante.apellidoPaterno" /> <bean:write name="datosCotizacion" property="contratante.apellidoMaterno" />
                              </dd>
                            </dl>
                          </div>
                          <div class="col-lg-3">
                            <dl class="o-description o-description--highlight_def">
                              <dt class="o-description__term">RUT Titular Tarjeta</dt>
                              <dd class="o-description__def">
                              <bean:write name="datosCotizacion" property="contratante.rut" format="##,##0" locale="currentLocale" /> - <bean:write name="datosCotizacion" property="contratante.dv" />
                              </dd>
                            </dl>
                          </div>
                          <div class="col-lg-5">
                            <div class="o-form__field">
                              <label class="o-form__label">Ingrese el n�mero de su tarjeta</label>
                              <div class="row">
                              <div class="col-3">
                              <input type="hidden" class="textbox" name="ccnum_mas"
									id="ccnum_mas" />
                               	<input class="o-form__input ccnum" name="ccnum_mas1"
										size="4" maxlength="4" id="ccnum_mas1" required size="10"
										type="text" pattern="[0-9]*" onkeypress="return isNumberKey(event)"placeholder="">
                              </div>
                              <div class="col-3">
                               	<input class="o-form__input ccnum" name="ccnum_mas2"
										size="4" maxlength="4" id="ccnum_mas2" required size="10"
										type="text" pattern="[0-9]*" onkeypress="return isNumberKey(event)" placeholder="" >
                              </div>
                              <div class="col-3">
                               	<input class="o-form__input ccnum" name="ccnum_mas3"
										size="4" maxlength="4" id="ccnum_mas3" required size="10"
										type="text" pattern="[0-9]*"  onkeypress="return isNumberKey(event)" placeholder="">
                              </div>
                              <div class="col-3">
                               	<input class="o-form__input ccnum" name="ccnum_mas4"
										size="4" maxlength="4" id="ccnum_mas4" required size="10"
										type="text" pattern="[0-9]*" onkeypress="return isNumberKey(event)" placeholder="">
                              </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- input tarjeta credito -->
                        <div class="row tarjeta-credito-container">
                          <div class="col-12"> </div>
                          <div class="col-lg-4">
                            <dl class="o-description o-description--highlight_def">
                              <dt class="o-description__term">Nombre Titular Tarjeta</dt>
                              <dd class="o-description__def">
                              <bean:write name="datosCotizacion" property="contratante.nombre" /> <bean:write name="datosCotizacion" property="contratante.apellidoPaterno" /> <bean:write name="datosCotizacion" property="contratante.apellidoMaterno" />
                              </dd>
                            </dl>
                          </div>
                          <div class="col-lg-3">
                            <dl class="o-description o-description--highlight_def">
                              <dt class="o-description__term">RUT Titular Tarjeta</dt>
                              <dd class="o-description__def">
                              <bean:write name="datosCotizacion" property="contratante.rut" format="##,##0" locale="currentLocale" /> - <bean:write name="datosCotizacion" property="contratante.dv" />
                              </dd>
                            </dl>
                          </div>
                          <logic:notPresent name="existePrimaUnica">
                          <div class="col-lg-5">
                            <div class="o-form__field">
                              <label class="o-form__label">Ingrese el n�mero de su tarjeta</label>
                              <div class="row">
                              <div class="col-3">
                              <input type="hidden" class="textbox" name="cardNumber"
										id="cardNumber" value="" />
								<input type="hidden"
										class="textbox" name="ccnum" id="ccnum" />
								<input class="o-form__input input-dest ccnum" name="ccnum1" size="4"
											maxlength="4" id="ccnum1" required size="10" type="text"
											pattern="[0-9]*" onkeypress="return isNumberKey(event)" placeholder="" />
                              </div>
                              <div class="col-3">
										<input class="o-form__input input-dest ccnum" name="ccnum2" size="4"
											maxlength="4" id="ccnum2" required size="10" type="text"
											pattern="[0-9]*" onkeypress="return isNumberKey(event)" placeholder="" />
                              </div>
                              <div class="col-3">										
								<input class="o-form__input input-dest ccnum" name="ccnum3" size="4"
											maxlength="4" id="ccnum3" required size="10" type="text"
											pattern="[0-9]*" onkeypress="return isNumberKey(event)" placeholder="" />
                              </div>
                              <div class="col-3">
								<input class="o-form__input input-dest ccnum" name="ccnum4" size="4"
											maxlength="4" id="ccnum4" required size="10" type="text"
											pattern="[0-9]*" onkeypress="return isNumberKey(event)" placeholder="" />
                              </div>
                              </div>
                            </div>
                          </div>
                          </logic:notPresent>
                        </div>
                        <logic:present name="existePrimaUnica">
                        <input type="hidden" class="textbox" name="cardNumber"
										id="cardNumber" value="" />
                        </logic:present>
                        
                        <logic:notPresent name="existePrimaUnica">
                        <div class="row">
                          <div class="col-12"> 
                            <h2 class="o-title o-title--subtitle u-mt20 u-mb10">Condiciones de pago  </h2>
                            <p class="o text">La forma de pago de tu seguro se realizar� en 2 pasos:</p>
                            <div class="c-payment">
                              <div class="c-payment__steps c-payment__steps--path"><span class="c-payment__milestone"><span class="c-payment__number">1</span></span></div>
                              <div class="c-payment__content">
                                <h2 class="c-payment__title">Suscripci�n a Pago Autom�tico con Tarjeta Bancaria (PAT)<span class="o-help"><i class="o-help__icon">
                                      <svg width="11px" height="17px" viewbox="0 0 11 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <g id="Cotizaci�n-Paso-3" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                          <g id="Desktop" transform="translate(-355.000000, -434.000000)" fill="#FFFFFF">
                                            <g id="Valor-cuota-mensual" transform="translate(201.000000, 432.000000)">
                                              <g id="Group-2" transform="translate(154.000000, 2.000000)">
                                                <path id="Path" d="M5.49009823,16.4153497 C4.744,16.4153497 4.13949705,15.8108468 4.13949705,15.0647485 C4.13949705,14.3199705 4.744,13.7154676 5.49009823,13.7154676 C6.23487623,13.7154676 6.83937917,14.3199705 6.83937917,15.0647485 C6.83937917,15.8108468 6.23487623,16.4153497 5.49009823,16.4153497 Z"></path>
                                                <path id="Path" d="M7.87134971,9.96657564 C7.32060511,10.2694872 6.80133988,10.8687092 6.80133988,11.1126228 C6.80133988,11.836442 6.21387623,12.4239057 5.49009823,12.4239057 C4.76632024,12.4239057 4.17885658,11.836442 4.17885658,11.1126228 C4.17885658,9.44731041 5.70517289,8.16490766 6.60730648,7.66924165 C7.93430845,6.93754224 8.11262279,6.14032613 8.11262279,5.45846562 C8.11262279,3.53224165 6.46963065,3.24504912 5.49009823,3.24504912 C4.22733399,3.24504912 2.86757367,4.07378585 2.86757367,5.89509234 C2.86757367,6.61891159 2.28011002,7.20633399 1.55633202,7.20633399 C0.832554028,7.20633399 0.245090373,6.61887033 0.245090373,5.89509234 C0.245090373,2.47007073 2.94761297,0.622524558 5.49013949,0.622524558 C8.10086444,0.622524558 10.7351886,2.11736149 10.7351886,5.45846562 C10.7351473,7.40304912 9.77265422,8.92148527 7.87134971,9.96657564 Z"></path>
                                              </g>
                                            </g>
                                          </g>
                                        </g>
                                      </svg></i></span></h2>
                                <p class="o-text">Se realizar� una suscripci�n a Pago Autom�tico con Tarjeta Bancaria (PAT), que se cobrar� por mes vencido y ser� renovado autom�ticamente una vez al a�o.</p>
                              </div>
                            </div>
                            <div class="c-payment">
                              <div class="c-payment__steps"><span class="c-payment__milestone"><span class="c-payment__number">2</span></span></div>
                              <div class="c-payment__content">
                                <h2 class="c-payment__title">Primer Pago proporcional del seguro</h2>
                                <p class="o-text">Corresponde al pago proporcional del mes de compra, que se realizar� v�a WebPay (Ej: Si contrataste el 17 de noviembre, se cobrar� desde ese d�a al 30 de noviembre).</p>
                                <p class="o-text">Al seleccionar<strong class="o-text o-text--highlight"> "Pagar"</strong> ,ingresar�s a la p�gina de Web Pay para realizar el pago proporcional en tu tarjeta de cr�dito.</p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-12">
                            <div class="o-alert o-alert--info u-mtb20"><span class="o-alert__icon"><img src="/vseg-paris/img/icon-info.svg" alt=""></span>
                              <div class="o-alert__content">
                                <p class="o-alert__text">Un vez direccionado a Web Pay recuerde que debe pagar con la misma tarjeta ingresada anteriormente, de lo contrario el pago se rechazara.</p>
                              </div>
                            </div>
                            <hr class="u-line u-line--gray">
                          </div>
                        </div>
                        </logic:notPresent>
                        <div class="row u-mb10">
                          <div class="col-12">
                            <div class="o-form__field o-form__checkbox u-mb0"><span class="o-checkbox">
                                <input type="checkbox" name="condcomerciales"
								value="acepto" id="condcomerciales" />
                                <label class="" for="comercial"><strong>He le�do y acepto las condiciones comerciales de mi p�liza.</strong></label></span><span class="o-form__message"></span></div>
                          </div>
                          <logic:notPresent name="existePrimaUnica">
                          <div class="col-12">
                            <div class="o-form__field u-mb0"><span class="o-checkbox">
                                <input type="checkbox" 
											name="cargoRecurrente" id="cargoRecurrente"value="1" id="cargoR" />
                                <label class="" for="eferts"><strong>Acepto el Pago Autom�tico Mensual de mi Tarjeta.</strong></label></span></div>
                          </div>
                          </logic:notPresent>
                          <div class="col-12">
                            <div class="o-form__field u-mb0"><span class="o-checkbox">
                                <input type="checkbox"
											checked="checked" id="inlineCheckbox2" value="option2" />
                                <label for="eferts"><strong>Acepto recibir ofertas y promociones en mi correo electr�nico</strong></label></span></div>
                          </div>
                        </div>
                        </form>
                        <div class="row o-actions o-actions--flex">
			<!-- INICIO  COTIZACION -->
				<div class="col-lg-12">
					<p class="small">Todos los campos con * son obligatorios</p>
				</div>
				<div class="col-lg-12" style="display: none" id="warning-chackbox">
					<div class="alert alert-warning">
						<p align="center">Debes elegir una tarjeta y aceptar las condiciones comerciales <logic:notPresent name="existePrimaUnica">, adem�s aceptar el cargo autom�tico mensual</logic:notPresent></p>
					</div>
				</div>
				<form action="" method="post" name="pago" id="pago" style="width: 100%;">
					<logic:notPresent name="existePrimaUnica">
						<input type="hidden" name="montoProporcional" value="<bean:write name="montoProporcional"/>" />
					</logic:notPresent>
					<input type="hidden" id="paymentId" name="paymentId" value="" />
					<input type="hidden" name="TBK_TIPO_TRANSACCION" value="TR_NORMAL" />
					<input type="hidden" name="TBK_ORDEN_COMPRA" value="<bean:write name="ordenCompra"/>" /> 
					<input type="hidden" name="TBK_MONTO" value="<bean:write name="monto"/>" />
						<div class="col-lg-3">
                            <div class="o-form__field is-last"><a class="o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="<bean:write name='volverPaso2'/>?volverPaso2=true" id="volverPaso2"><i class="mx-arrow-left"></i> Volver</a></div>
                          </div>
                          <div class="col-lg-3 offset-lg-6">
                            <div class="o-form__field is-last">
                              <button class="o-btn o-btn--primary o-btn o-btn--icon" type="button" id="contratar">Siguiente<i class="o-icon">
                                  <svg width="13px" height="14px" viewbox="0 0 13 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <g id="Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                      <g id="botones/orangeButton+rightArrow" transform="translate(-113.000000, -19.000000)" fill-rule="nonzero" fill="#FFFFFF">
                                        <g id="orangeButton+rightArrow">
                                          <g id="arrows" transform="translate(113.000000, 19.000000)">
                                            <path id="Shape" d="M12.6595914,6.18869213 L6.92029216,0.341285393 C6.69713064,0.113730337 6.42956532,0 6.11805938,0 C5.81235867,0 5.54791211,0.113730337 5.32456532,0.341285393 L4.66341805,1.01492135 C4.44004038,1.23052135 4.32838242,1.49998202 4.32838242,1.82336629 C4.32838242,2.14668764 4.44004038,2.41614831 4.66341805,2.63181124 L7.24652732,5.27252584 L1.03990736,5.27252584 C0.734361045,5.27252584 0.486033254,5.38477753 0.295016627,5.60937528 C0.104,5.83397303 0.00849168646,6.10494382 0.00849168646,6.42222472 L0.00849168646,7.57198652 C0.00849168646,7.8893618 0.104030879,8.1602382 0.295016627,8.38483596 C0.486002375,8.60943371 0.734361045,8.72159101 1.03990736,8.72159101 L7.2464038,8.72159101 L4.66332542,11.3535281 C4.43994774,11.5809258 4.32828979,11.8534382 4.32828979,12.1709079 C4.32828979,12.4883146 4.43994774,12.7607955 4.66332542,12.9881933 L5.32447268,13.6618292 C5.5537791,13.8834067 5.81822565,13.9941798 6.11796675,13.9941798 C6.42363658,13.9941798 6.69114014,13.8834067 6.9202304,13.6618292 L12.6594988,7.81442247 C12.8769786,7.59284494 12.9857957,7.32045843 12.9857957,6.9970427 C12.9857957,6.66774382 12.8769786,6.3981573 12.6595914,6.18869213 Z"></path>
                                          </g>
                                        </g>
                                      </g>
                                    </g>
                                  </svg></i></button>
                            </div>
                          </div>
				</form>
			
                        </div>
                        <!--
                        <div class="row u-hidden_desktop">
                          <div class="col-12">
                            <div class="c-assistance u-mb30">
                              <h4 class="c-assistance__title">�Necesitas asistencia?</h4><a class="c-assistance__button o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="#">Te llamamos gratis</a>
                              <p class="c-assistance__text">O ll�manos gratis al<strong class="c-assistance__outstanding"> 600 500 5000</strong></p>
                            </div>
                          </div>
                        </div>-->
                    </div>
                    <div class="col-lg-4 u-mobile_first">
                      <section class="c-summary" id="summary_sure">
                        <h2 class="c-summary__title">Resumen de tu seguro</h2>
                        <div class="c-summary__company"></div>
                        <h3 class="c-summary__subtitle"><bean:write name="datosPlan" property="nombrePlan" /></h3>
                        <div class="row">
                          <div class="col-12">
                            <div class="c-summary__cost">
                              <div class="c-summary__label">Monto Total:</div>
                              <div class="c-summary__price"><sup>$</sup><bean:write name="datosPlan" property="primaMensualPesos"
								format="##,##0" locale="currentLocale" /><sub>/Mes*</sub></div>
                            </div>
                            <p class="c-summary__legal"></p>
                          </div>
                          <div class="col-12 u-mobile_second"> 
                          </div>
                        </div>
                        <!--
                        <div class="c-assistance u-mb30 u-hidden_mobile">
                          <h4 class="c-assistance__title">�Necesitas asistencia?</h4><a class="c-assistance__button o-btn o-btn--outline o-btn--secundary o-btn o-btn--icon" href="#">Te llamamos gratis</a>
                          <p class="c-assistance__text">O ll�manos gratis al<strong class="c-assistance__outstanding"> 600 500 5000</strong></p>
                        </div>-->
                      </section>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </section>
      </div>
    </main>
		
	</div>
	</div>
	<div class="clearfix"></div>
	<!-- FIN CONTENIDOS -->

	<%@ include file="/cotizacion/includes/cotizador-footer.jsp"%>
	
</body>
</html>
