<%@page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<div id="header">
	<link type='text/css' rel='stylesheet' href='/vseg-paris/style/estilo_header.css' />
	<link type='text/css' rel='stylesheet' href='/vseg-paris/style/login_header.css' />
	<link type='text/css' rel='stylesheet' href='/vseg-paris/style/estilo_footer.css' />
	<link type='text/css' rel='stylesheet' href='/vseg-paris/style/estilo_calugas.css' />
	<link type='text/css' rel='stylesheet' href='/vseg-paris/style/estilo_home.css' />
  <link rel="stylesheet" type="text/css" href="/vseg-paris/style/jquery.fancybox-1.3.4.css" media="screen" />


	<logic:present name="cl.tinet.common.seguridad.USUARIO_CONECTADO">
	<script type="text/javascript">
	<bean:define id="usuario"
	name="<%=SeguridadUtil.USUARIO_CONECTADO_KEY %>" scope="session"
	type="UsuarioExterno" />
	</script>
	</logic:present>
	<!-- HEADER -->
<div class="body_header">
	<div class="contenido_header">
		<div class="multisitio_header">
			<div id="visita"></div>
			<div id="paris"><a href="http://www.paris.cl/" target="_blank"></a></div>
			<div id="jumbo"><a href="http://www.jumbo.cl/FO/LogonForm?utm_source=Seguros&utm_medium=Header_Multisitio&utm_campaign=Header_Multisitio_Jumbo" target="_blank"></a></div>
			<div id="easy"><a href="http://www.easy.cl/easy/CargaInicio?mundo=1&tpCa=0&caN0=0&caN1=0&cab=?utm_source=Seguros&utm_medium=Header_Multisitio&utm_campaign=Header_Multisitio_Easy" target="_blank"></a></div>
			<div id="tarjetas"><a href="http://www.tarjetamas.cl/TarjetaMasWEB/inicio.do?utm_source=Seguros&utm_medium=Header_Multisitio&utm_campaign=Header_Multisitio_TarjetaMas" target="_blank"></a></div>
			<div id="seguros"><a class="active"></a></div>
			<div id="banco"><a href="http://www.bancoparis.cl/?utm_source=headerSeguro&utm_medium=header&utm_campaign=Header+Seguro" target="_blank"></a></div>
			<div id="puntos"><a href="http://www.puntoscencosud.cl/NectarHome.nectar?utm_source=Seguros&utm_medium=header_multisitio&utm_campaign=Header_Multisitio_Puntos" target="_blank" class="multisitio_header"></a></div>
			<div id="venta_empresas"><a href="http://www.ventaempresascencosud.cl" target="_blank"></a></div>
		</div>
		<div class="separacion_1_header"></div>
		<div class="bienvenido_body">
			<logic:notEmpty name="usuario">
			<div class="bienvenido_contenido" >
				<div class="bienvenido_header">	
					Bienvenido
					<span><bean:write name="usuario" property="usuarioExterno.nombre" /> 
						<bean:write name="usuario" property="usuarioExterno.apellido_paterno" /> 
						<bean:write name="usuario" property="usuarioExterno.apellido_materno" /> 
					</span>
				</div>
				<div class="cerrar_sesion"><a href="<%=request.getContextPath()%>/autenticacion/cerrarSession" target="_self">Cerrar sesi�n</a></div>
			</div>
			</logic:notEmpty>
		</div>
	<!-- FIN SESI�N USUARIO -->

		<div class="logo_header"></div>

	<!-- INFO -->
		<div class="sac_header" style="width:85px;">
			<a href="/vseg-paris/Sucursales" target="_self"><span>Nuestras</span><br /> 
			Sucursales
			</a>
		</div>

		<div class="sac_header" style="width:85px;">
			<a href="/vseg-paris/Sucursales" target="_self"><span>Te</span><br />
			Ayudamos</a>
		</div>
	<logic:notEmpty name="usuario">
		<div class="sac_header" style="width: auto; padding-right:5px">
			<a href="/vseg-paris/secure/inicio/inicio.do" target="_self" class="signin"><span>Mis</span><br />
			Seguros</a>
		</div>
	</logic:notEmpty>

	<logic:notPresent name="usuario">

		<div class="sac_header topnav" id="topnav" style="width: 90px;">
			<a  href="#" target="_self" class="signin"><span>Ingresa</span><br />
		a tu Cuenta</a>

			<fieldset id="signin_menu">
				<div>
					<img border="0" display="block" src="/vseg-paris/img/flecha_inicio.png" width="240" height="20" />
				</div>
					<form method="post" id="loginForm"class="loginForm" action="/vseg-paris/Login" style="background-color:#FFF; padding-left:10px; padding-right:10px; padding-bottom:10px;">
						<input type="hidden" value="/index.jsp" name="cl.tinet.common.seguridad.servlet.URI_EXITO">
						<input type="hidden" value="/index.jsp" name="cl.tinet.common.seguridad.servlet.URI_FRACASO">
						<input type="hidden" value="" name="username">
						<input type="hidden" value="" name="password">
						<p>
							<label for="username">Rut</label>
							<input id="rutlogin" name="rutlogin" value="" title="username" tabindex="4" type="text">
						</p>
						<p>
							<label for="password">Clave</label>
							<input id="clavelogin" name="clavelogin" value="" title="password" tabindex="5" type="password">
						</p>
						<p class="remember">
							<div id="signin_submit"><a href="#"></a></div>
						</p>
						<p class="forgot"> <a href="#" class="olvidasteClave">�Olvidaste tu Clave?</a> </p>
						<p class="forgot"> <a href="/vseg-paris/html/porque-registrarse.html" id="resend_password_link">�Para que sirve tu clave?</a> </p>
					<!--	<p class="forgot"> <a class="iframe"  id="registroUsuario" class="registro" href="/vseg-paris/registrocliente/registroCliente.do?rut_cliente=&dv_cliente=">Resgistrate aqu�</a> </p> -->
					</form>

			</fieldset>
		</div>
	</logic:notPresent>
	<logic:notEmpty name="usuario">

		<div class="sac_header" style="width:auto">
			<a href="/vseg-paris/secure/inicio/cotizaciones.do"><span>Mis</span><br />
			Cotizaciones</a>
		</div>
	</logic:notEmpty>
	<logic:notPresent name="usuario">
		<div class="sac_header" style="width:70px;">
			<a id="registroUsuario2" class="registro" href="/vseg-paris/registrocliente/registroCliente.do?rut_cliente=&dv_cliente="><span>Registrate</span><br />
			Aqu�</a>
		</div>
	</logic:notPresent>

		<div class="sac_header" style="width:110px;">
			<span>Servicio al Cliente</span><br />
			600 500 5000
		</div>
	<!-- FIN INFO -->

	<div class="separacion_2_header"></div>

	</div>

	</div>
	<!-- FIN HEADER -->

	<!-- HEADER -->
	<div class="body_menu">

	<div class="menu_contenido_header">

	<!-- MENU HEADER -->
	<div class="menu_header">
	<div id="menu">
	<ul>

	<li class="nivel1"><a href="/vseg-paris/index.jsp" class="nivel1" style="text-align:center;">Inicio</a></li>

	<li class="nivel1">
		<a href="/vseg-paris/desplegar-pagina-intermedia-vehiculos.do?idRama=1" style="text-align:center;" class="nivel1">Veh&iacute;culos</a>
		<ul style="width: 180px;">
			<li><a href="/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=22&idTipo=1">Full Servicio</a></li>
			<li><a href=" https://www.seguroscencosud.cl/vseg-paris/html/AutoPlay.html">Full Cobertura Auto Play</a></li>
			<li><a href="/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=21&idTipo=1">P&eacute;rdida Total</a></li>
			<!--li><a href="/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=23">Da&ntilde;os a Terceros</a></li-->
			<li><a href="/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=36">Seguro Obligatorio Mercosur</a></li>
			<li><a href="/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=79">Robo Contenido</a></li>
			<li><a target="_blank" href="/soap">SOAP</a></li>
			<!--li><a href="/vseg-paris/desplegar-promociones.do?idRama=1">Promociones</a></li-->
		</ul>
	</li>
	
	<li class="nivel1">
		<a href="/vseg-paris/desplegar-pagina-intermedia-fraude.do?idRama=8" style="text-align:center;" class="nivel1">Moto</a>
		<ul style="width: 180px;">
			<li><a href="/vseg-paris/desplegar-ficha.do?idRama=8&idSubcategoria=239">Seguro Obligatorio Mercosur</a></li>
		</ul>
	</li>
	
	<li class="nivel1">
		<a href="/vseg-paris/desplegar-pagina-intermedia-fraude.do?idRama=6" class="nivel1" style="text-align:center;">Viajes</a>
		<ul style="width: 180px;">
			<li><a href="/vseg-paris/desplegar-ficha.do?idRama=6&idSubcategoria=221">Seguro de Viajes</a></li>
			<li><a href="/vseg-paris/desplegar-ficha.do?idRama=1&idSubcategoria=36">Seguro Obligatorio Mercosur</a></li>
		</ul>
	</li>

	<li class="nivel1">
		<a href="/vseg-paris/desplegar-pagina-intermedia-vida.do?idRama=3" class="nivel1" style="text-align:center;">Vida y Salud</a>
		<ul style="width: 190px;">
			<li><a href="/vseg-paris/desplegar-ficha.do?idRama=3&idSubcategoria=29">Seguro de Accidentes</a></li>
			<!--li><a href="/vseg-paris/desplegar-ficha.do?idRama=7&idSubcategoria=163">Vida con Devoluci&oacute;n</a></li-->
			<li><a href="/vseg-paris/desplegar-ficha.do?idRama=7&idSubcategoria=162">AP con Devoluci&oacute;n</a></li>
			<li><a href="/vseg-paris/desplegar-ficha.do?idRama=7&idSubcategoria=161">Hospitalizaci&oacute;n con Devoluci&oacute;n</a></li>
			<li><a href="/vseg-paris/desplegar-ficha.do?idRama=7&idSubcategoria=164">Oncol&oacute;gico con Devoluci&oacute;n </a></li>
			<li><a href="/vseg-paris/desplegar-ficha.do?idRama=7&idSubcategoria=165&VP=1">Catastr&oacute;fico con Devoluci&oacute;n</a></li>
			<li><a href="/vseg-paris/desplegar-ficha.do?idRama=6&idSubcategoria=140">Seguro Mascota</a></li>
		</ul>
	</li>
	
	<li class="nivel1">
		<a href="/vseg-paris/desplegar-pagina-intermedia-hogar.do?idRama=2" class="nivel1" style="text-align:center;">Hogar</a>
		<ul style="width: 210px;">
		<li><a href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=220">Hogar Vacaciones</a></li>
		<li><a href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=24">Incendio + Sismo / Estructura</a></li>
		<li><a href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=43">Incendio + Sismo / Contenido</a></li>
		<li><a href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=44">Incendio + Robo</a></li>
		<li><a href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=25">Incendio + Robo + Sismo</a></li>
		<li><a href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=26">Hogar con Devoluci&oacute;n Dinero</a></li>
		<li><a href="/vseg-paris/desplegar-ficha.do?idRama=2&idSubcategoria=201">Hogar Full Asistencia</a></li>
		<li><a href="/vseg-paris/desplegar-ficha.do?idRama=6&idSubcategoria=140">Seguro Mascota</a></li>
		<!--li><a href="/vseg-paris/desplegar-promociones.do?idRama=2">Promociones</a></li-->
		</ul>
	</li>

	<li class="nivel1">
		<a href="/vseg-paris/desplegar-pagina-intermedia-fraude.do?idRama=5" class="nivel1" style="text-align:center;">Fraude</a>
		<ul style="width: 180px;">
			<li><a href="/vseg-paris/desplegar-ficha.do?idRama=5&idSubcategoria=139">Seguro Fraude</a></li>
			<li><a href="/vseg-paris/desplegar-ficha.do?idRama=5&idSubcategoria=159">Fraude con Premio a la Permanencia</a></li>
		</ul>
	</li>
	
	<li class="nivel1">
		<a href="/vseg-paris/desplegar-pagina-intermedia-fraude.do?idRama=7" class="nivel1" style="text-align:center;">Con Devoluci&oacute;n</a>
		<ul style="width: 192px;">
			<li><a href="/vseg-paris/desplegar-ficha.do?idRama=7&idSubcategoria=162">AP con Devoluci&oacute;n</a></li>
			<li><a href="/vseg-paris/desplegar-ficha.do?idRama=7&idSubcategoria=161">Hospitalizaci&oacute;n con Devoluci&oacute;n</a></li>
			<!--li><a href="/vseg-paris/desplegar-ficha.do?idRama=7&idSubcategoria=163">Vida con Devoluci&oacute;n</a></li-->
			<li><a href="/vseg-paris/desplegar-ficha.do?idRama=7&idSubcategoria=165&VP=1">Catastr&oacute;fico con Devoluci&oacute;n</a></li>
			<li><a href="/vseg-paris/desplegar-ficha.do?idRama=7&idSubcategoria=164">Oncol&oacute;gico con Devoluci&oacute;n </a></li>
			<li><a href="/vseg-paris/desplegar-ficha.do?idRama=5&idSubcategoria=159">Fraude con Premio a la Permanencia</a></li>
			<li><a href="/vseg-paris/desplegar-ficha.do?idRama=7&idSubcategoria=179&VP=1">Mas Seguro Integral</a></li>
		</ul>
	</li>
	
	<!--
	<li class="nivel1"><a href="#" class="nivel1" style="text-align:center;">Promociones</a></li>
	-->
	</ul>
	</div>
	</div>
	<!-- MENU HEADER -->

	</div>

	</div>
	<!-- FIN HEADER -->

	<!-- HOME -->
	<div id="sombra_header"></div>
	</div>
