<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<bean:define id="vsegparispath" value="/vseg-paris" />
<bean:define id="contextpath" value="<%=request.getContextPath()%>" />

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ include file="/cotizacion/includes/cotizador-head.jsp"%>
<%@ include file="../head.jsp"%>
<title>Seguros Cencosud1 - Fracaso</title>

</head>

<body>
<%@ include file="../../../../google-analytics/google-analytics.jsp" %>
<%@ include file="/cotizacion/includes/cotizador-header.jsp"%>

<!-- NUEVA VISTA -->
<main role="main">
      <div class="remodal-bg">
        <section class="container">
          <div class="row">
            <div class="col-12">
              <h1 class="o-title o-title--primary"></h1>
            </div>
            <div class="col-12">
              <section class="o-box o-box--message u-text-center u-mtb50"><img class="u-image" src="img/cross_error_big.png" alt="">
                <h1 class="o-title o-title--secundary">�El pago efectuado ha sido rechazado!</h1>
                <p class="o-text u-mb20">Su transacci�n no ha podido ser procesada</p>
                <p class="o-text u-mb20">Porfavor vuelva a intentarlo y si el problema persiste comun�quese con su banco emisor</p>
             
               
                <p class="o-text u-mb20">Ante cualquier duda, ll�manos al</p>
              <h2 class="o-title o-title">   <i class="o-icon o-icon--left">
                    <!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In  -->
<svg version="1.1"
	 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
	 x="0px" y="0px" width="18px" height="18px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;"
	 xml:space="preserve">
<style type="text/css">
	.st0{fill:#666666;}
</style>
<defs>
</defs>
<path class="st0" d="M256,0C114.6,0,0,114.6,0,256c0,141.4,114.6,256,256,256s256-114.6,256-256C512,114.6,397.4,0,256,0z
	 M391.2,373.5L370.7,394c-3.7,3.7-14.4,5.9-14.7,5.9c-64.8,0.6-127.3-24.9-173.2-70.8c-46-46-71.5-108.7-70.8-173.7
	c0,0,2.3-10.4,6-14.1l20.5-20.5c7.5-7.5,21.9-10.9,32-7.6l4.3,1.4c10.1,3.4,20.6,14.5,23.4,24.8l10.3,37.8c2.8,10.3-1,24.9-8.5,32.4
	l-13.7,13.7c13.4,49.7,52.5,88.8,102.2,102.3l13.7-13.7c7.5-7.5,22.2-11.3,32.4-8.5l37.8,10.3c10.3,2.8,21.4,13.3,24.8,23.4l1.4,4.4
	C402.1,351.7,398.7,366.1,391.2,373.5z"/>
</svg>

                     
                    
                </i>  600 500 5000</h2>
                <div class="o-alert info u-mtb20">
                 
                </div>
                <a class="o-btn o-btn--primary" href="<bean:write name="contextpath" />/secure/cotizador-forma-de-pago.do">Volver a intentar</a>
              </section>
            </div>
          </div>
        </section>
        
      </div>
    </main>
<!-- FIN NUEVA VISTA -->
<%@ include file="../../../../cotizacion/includes/cotizador-footer.jsp"%>
</body>
</html>
