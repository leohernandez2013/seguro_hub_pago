<%@ page import="cl.tinet.common.seguridad.util.SeguridadUtil"%>
<%@page import="cl.tinet.common.seguridad.model.UsuarioExterno"%>
<%@ page language="java" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<bean:define id="contextpath" value="<%=request.getContextPath()%>" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	
<html>
	<head>
		<%@ include file="/cotizacion/includes/cotizador-head.jsp"%>
		<%@ include file="../head.jsp"%>
		<title>Seguro Cencosud</title>
		<script type="text/javascript" src="../css/p7pmm/p7PMMscripts.js"></script>		
	</head>
	
	<!-- INICIO TAG SEGUIMIENTO -->
   <script type="text/javascript">
		var dataLayer=[];
		dataLayer.push({
			'transactionId': '<bean:write name="resultNroBigsa"/>',
			'transactionAffiliation': 'Tarjeta Cencosud',
			'transactionTotal': <logic:notEqual name="datosPlan" property="maxDiaIniVig" value="0"><bean:write name="datosPlan" property="primaMensualPesos" locale="currentLocale"/></logic:notEqual><logic:equal name="datosPlan" property="maxDiaIniVig" value="0"><bean:write name="datosPlan" property="primaAnualPesos" locale="currentLocale"/></logic:equal>,
			'transactionProducts': [
			{
			'sku': '<bean:write name="idSubcategoria"/>',
			'name': '<bean:write name="subcategoriaDesc"/>',
			'category': '<logic:equal name="idRama" value="1">Vehiculos</logic:equal><logic:equal name="idRama" value="2">Hogar</logic:equal><logic:equal name="idRama" value="3">Vida y Salud</logic:equal><logic:equal name="idRama" value="5">Fraude</logic:equal><logic:equal name="idRama" value="6">Viajes</logic:equal><logic:equal name="idRama" value="7">Con Devolucion</logic:equal>_<bean:write name="datosPlan" property="nombrePlan" />',
			'price': <bean:write name="datosPlan" property="primaMensualPesos" locale="currentLocale"/>,
			'quantity': <logic:notEqual name="datosPlan" property="maxDiaIniVig" value="0">1</logic:notEqual><logic:equal name="datosPlan" property="maxDiaIniVig" value="0">12</logic:equal>
			}
			],
			'event':'trackTrans'
		});
	</script>
	<!-- FIN TAG SEGUIMIENTO -->

	<body>
		<%@ include file="../../../../google-analytics/google-analytics.jsp" %>
		<%@ include file="/cotizacion/includes/cotizador-header.jsp"%>
		
	
<!-- FIN  BREADCRUMB --> 
<!-- INICIO  CONTENIDOS -->
<section class="container">
          <div class="row">
            <div class="col-12">
              <section class="o-box o-box--message u-text-center u-mtb50"><img class="u-image" src="img/check_success_big.png" alt="">
                <h1 class="o-title o-title--secundary">�Felicitaciones <bean:write name="datosCotizacion" property="contratante.nombre" /> <bean:write name="datosCotizacion" property="contratante.apellidoPaterno" /> <bean:write name="datosCotizacion" property="contratante.apellidoMaterno" />!</h1>
                <p class="o-text u-mb20">Tu pago se ha realizado con �xito</p><small class="o-text o-text--small">Desde este momento ya cuentas con tu</small>
                <h2 class="o-title o-title--subtitle o-title--primary">Seguro </h2>
                <p class="o-text u-mb20">Tu p�liza ser� enviada a &nbsp;<strong class="o-text o-text--highlight"><bean:write name="datosCotizacion" property="email" /></strong> o puedes descargarla a continuaci�n.</p><a class="o-btn o-btn--link o-btn--primary o-btn o-btn--icon" href="/vseg-paris/secure/inicio/obtener-poliza.do?x.pdf"><i class="o-icon o-icon--left">
                    <svg width="18px" height="19px" viewbox="0 0 18 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <g id="icon_download" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="Desktop" transform="translate(-627.000000, -920.000000)" fill-rule="nonzero" fill="#FFFFFF">
                          <g id="orangeButton+rightArrow" transform="translate(604.000000, 904.502137)">
                            <g id="download-icon" transform="translate(23.000000, 16.000000)">
                              <path id="Shape" d="M16.514832,9.960384 C15.719568,9.960384 15.074832,10.60512 15.074832,11.400384 L15.074832,15.077616 L2.882832,15.077616 L2.882832,11.400384 C2.882832,10.60512 2.238096,9.960384 1.442832,9.960384 C0.647568,9.960384 0.002832,10.60512 0.002832,11.400384 L0.002832,16.517616 C0.002832,17.31288 0.647568,17.957616 1.442832,17.957616 L16.514832,17.957616 C17.310096,17.957616 17.954832,17.31288 17.954832,16.517616 L17.954832,11.400384 C17.954832,10.605072 17.310096,9.960384 16.514832,9.960384 Z"></path>
                              <path id="Shape" d="M12.026256,5.197776 L10.418832,6.8052 L10.418832,1.44 C10.418832,0.644736 9.774096,0 8.978832,0 C8.183568,0 7.538832,0.644736 7.538832,1.44 L7.538832,6.8052 L5.931408,5.197776 C5.650272,4.91664 5.281728,4.776 4.913184,4.776 C4.54464,4.776 4.176144,4.91664 3.89496,5.197776 C3.33264,5.760192 3.33264,6.671904 3.89496,7.234224 L7.960608,11.299872 C8.522928,11.862288 9.434736,11.862288 9.997056,11.299872 L14.062704,7.234224 C14.62512,6.671904 14.62512,5.760192 14.062704,5.197776 C13.500384,4.63536 12.588576,4.63536 12.026256,5.197776 Z"></path>
                            </g>
                          </g>
                        </g>
                      </g>
                    </svg></i>Descargar P�liza</a>
                <div class="o-alert o-alert--info u-mtb20">
                  <div class="o-alert__content">
                    <p class="o-alert__text u-text-center">Recuerda que puedes acceder a la informaci�n de tus p�lizas y beneficios exclusivos para clientes, solo debes iniciar sesi�n e ir a la secci�n<strong> "Mis seguros online".</strong></p>
                  </div>
                </div><a class="o-btn o-btn--primary" href="/vseg-paris/index.jsp">Volver al Inicio</a>
              </section>
            </div>
          </div>
        </section>
<!-- FIN CONTENIDOS --> 

<%@ include file="../../../../cotizacion/includes/cotizador-footer.jsp"%>

	</body>
</html>