package cl.cencosud.acv.common.exception;

public class ProcesoCotizacionException extends ACVException {

    /**
     * Identificador de la clase para serialización.
     */
    private static final long serialVersionUID = -9002895352666219806L;

    public static final String PRODUCTO_NO_ENCONTRADO =
        "cl.cencosud.ventaseguros.cotizacion.exception.PRODUCTO_NO_ENCONTRADO";

    public static final String PLAN_NO_ENCONTRADO =
        "cl.cencosud.ventaseguros.cotizacion.exception.PLAN_NO_ENCONTRADO";

    public static final String CAPTCHA_INVALIDO =
        "cl.cencosud.ventaseguros.cotizacion.exception.CAPTCHA_INVALIDO";

    public static final String VEHICULO_NO_ENCONTRADO =
        "cl.cencosud.ventaseguros.cotizacion.exception.VEHICULO_NO_ENCONTRADO";

    public static final String ERROR_VALORIZACION =
        "cl.cencosud.ventaseguros.cotizacion.exception.ERROR_VALORIZACION";

    public static final String NO_SOLICITUD =
        "cl.cencosud.ventaseguros.cotizacion.exception.NO_SOLICITUD";

    public ProcesoCotizacionException(String messageKey, Object[] arguments) {
        super(messageKey, arguments);
    }

    public ProcesoCotizacionException(String messageKey, int arguments) {
        super(messageKey, new Object[] { arguments });
    }
}
