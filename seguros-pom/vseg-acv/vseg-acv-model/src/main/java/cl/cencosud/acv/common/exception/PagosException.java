package cl.cencosud.acv.common.exception;

public class PagosException extends ACVException {

    /**
     * Identificador de la clase para serialización.
     */
    private static final long serialVersionUID = 3318534913101210088L;

    public static final String ERROR_DE_VALIDACION =
        "cl.cencosud.asesorcotizador.ejb.pagos.exception.ERROR_DE_VALIDACION";

    public static final String ERROR_RUT_VALIDACION =
        "cl.cencosud.asesorcotizador.ejb.pagos.exception.ERROR_RUT_VALIDACION";

    public static final String ERROR_OBTENER_SALDO =
        "cl.cencosud.asesorcotizador.ejb.pagos.exception.ERROR_OBTENER_SALDO";

    public static final String ERROR_TARJETA_BLOQUEADA =
        "cl.cencosud.asesorcotizador.ejb.pagos.exception.ERROR_TARJETA_BLOQUEADA";

    public static final String ERROR_SALDO_NO_DISPONIBLE =
        "cl.cencosud.asesorcotizador.ejb.pagos.exception.ERROR_SALDO_NO_DISPONIBLE";

    public PagosException(String messageKey, Object[] arguments) {
        super(messageKey, arguments);
    }

    public PagosException(String messageKey, int arguments) {
        super(messageKey, new Object[] { arguments });
    }

    public PagosException(String messageKey) {
        super(messageKey, new Object[] {});
    }
}
