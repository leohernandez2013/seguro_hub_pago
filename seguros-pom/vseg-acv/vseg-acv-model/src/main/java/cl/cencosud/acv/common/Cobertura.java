package cl.cencosud.acv.common;

import java.io.Serializable;

public class Cobertura implements Serializable {

	/**
	 * Identificador de la clase para serialización.
	 */
	private static final long serialVersionUID = 637263592666476627L;
	/**
	 * Identificador de la cobertura.
	 */
	private String idCobertura;
	/**
	 * Descripción de la cobertura.
	 */
	private String descripcion;
	/**
	 * Monto asociado a la cobertura.
	 */
	private String monto;

	/**
	 * @return the idCobertura
	 */
	public String getIdCobertura() {
		return idCobertura;
	}

	/**
	 * @param idCobertura
	 *            the idCobertura to set
	 */
	public void setIdCobertura(String idCobertura) {
		this.idCobertura = idCobertura;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the monto
	 */
	public String getMonto() {
		return monto;
	}

	/**
	 * @param monto
	 *            the monto to set
	 */
	public void setMonto(String monto) {
		this.monto = monto;
	}
}
