package cl.cencosud.acv.common;

public enum EstadoCivilEnum {

    /**
     * Estado soltero.
     */
    _1("S", "Soltero"),
    /**
     * Estado Casado.
     */
    _2("C", "Casado"),
    /**
     * Estado viudo.
     */
    _3("V", "Viudo"),
    /**
     * Estado Anulado.
     */
    _4("A", "Anulado"),
    /**
     * Estado Divorciado.
     */
    _5("D", "Divorciado");

    /**
     * Codigo de estado civil.
     */
    private String codigo;
    /**
     * Descripcion del estado civil.
     */
    private String descripcion;

    /**
     * Enum.
     * @param codigo
     * @param descripcion
     */
    EstadoCivilEnum(String codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    /**
     * @return retorna el valor del atributo codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo a establecer en el atributo codigo.
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return retorna el valor del atributo descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion a establecer en el atributo descripcion.
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
