package cl.cencosud.acv.common;

import java.io.Serializable;

public class Producto implements Serializable {

	/**
	 * Identificador de la clase para serialización.
	 */
	private static final long serialVersionUID = 4070491959715056593L;
	/**
	 * Identificador del producto.
	 */
	private String idProducto;
	/**
	 * Nombre del producto.
	 */
	private String nombreProducto;

	/**
	 * @return the idProducto
	 */
	public String getIdProducto() {
		return idProducto;
	}

	/**
	 * @param idProducto
	 *            the idProducto to set
	 */
	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}

	/**
	 * @return the nombreProducto
	 */
	public String getNombreProducto() {
		return nombreProducto;
	}

	/**
	 * @param nombreProducto
	 *            the nombreProducto to set
	 */
	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

}
