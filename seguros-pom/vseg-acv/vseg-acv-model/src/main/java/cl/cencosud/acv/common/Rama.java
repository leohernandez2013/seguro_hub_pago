package cl.cencosud.acv.common;

import java.io.Serializable;
import java.util.Date;

/**
 * Ramas de productos.
 * <br/>
 * @author ghost23
 * @version 1.0
 * @created 24/08/2010
 */
public class Rama implements Serializable {

    /**
     * Serial de la clase.
     */
    private static final long serialVersionUID = -6765145157593725003L;

    /**
     * Identificador de la rama.
     */
    private int id_rama;
    /**
     * Identificador del tipo de producto.
     */
    private int id_tipo_producto;
    /**
     * Titulo de la rama.
     */
    private String titulo_rama;
    /**
     * Fecha de creacion de la rama.
     */
    private Date fecha_creacion;
    /**
     * Fecha de ultima actualizacion.
     */
    private Date fecha_modificacion;
    /**
     * Identificador del pais.
     */
    private int id_pais;

    /**
     * Descripcion de la rama
     */
    private String descripcion_rama;

    /**
     * @return retorna el valor del atributo id_rama
     */
    public int getId_rama() {
        return id_rama;
    }

    /**
     * @param id_rama a establecer en el atributo id_rama.
     */
    public void setId_rama(int id_rama) {
        this.id_rama = id_rama;
    }

    /**
     * @return retorna el valor del atributo id_tipo_producto
     */
    public int getId_tipo_producto() {
        return id_tipo_producto;
    }

    /**
     * @param id_tipo_producto a establecer en el atributo id_tipo_producto.
     */
    public void setId_tipo_producto(int id_tipo_producto) {
        this.id_tipo_producto = id_tipo_producto;
    }

    /**
     * @return retorna el valor del atributo titulo_rama
     */
    public String getTitulo_rama() {
        return titulo_rama;
    }

    /**
     * @param titulo_rama a establecer en el atributo titulo_rama.
     */
    public void setTitulo_rama(String titulo_rama) {
        this.titulo_rama = titulo_rama;
    }

    /**
     * @return retorna el valor del atributo fecha_creacion
     */
    public Date getFecha_creacion() {
        return fecha_creacion;
    }

    /**
     * @param fecha_creacion a establecer en el atributo fecha_creacion.
     */
    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    /**
     * @return retorna el valor del atributo fecha_modificacion
     */
    public Date getFecha_modificacion() {
        return fecha_modificacion;
    }

    /**
     * @param fecha_modificacion a establecer en el atributo fecha_modificacion.
     */
    public void setFecha_modificacion(Date fecha_modificacion) {
        this.fecha_modificacion = fecha_modificacion;
    }

    /**
     * @return retorna el valor del atributo id_pais
     */
    public int getId_pais() {
        return id_pais;
    }

    /**
     * @param id_pais a establecer en el atributo id_pais.
     */
    public void setId_pais(int id_pais) {
        this.id_pais = id_pais;
    }

    /**
     * @return retorna el valor del atributo descripcion_rama
     */
    public String getDescripcion_rama() {
        return descripcion_rama;
    }

    /**
     * @param descripcion_rama a establecer en el atributo descripcion_rama.
     */
    public void setDescripcion_rama(String descripcion_rama) {
        this.descripcion_rama = descripcion_rama;
    }

}
