package cl.cencosud.acv.common.valorizacion;

import java.io.Serializable;

public class ValorizacionSeguro implements Serializable {

	/**
	 * Identificador de la clase para Serializcion.
	 */
	private static final long serialVersionUID = 8847541414023119622L;
	/**
	 * Prima anual.
	 */
	private float primaAnual;
	/**
	 * Prima mensual.
	 */
	private float primaMensual;
	/**
	 * Prima anual en pesos.
	 */
	private long primaAnualPesos;
	/**
	 * Prima mensual en pesos.
	 */
	private long primaMensualPesos;

	private long idProducto;
	/**
	 * @return the primaAnual
	 */
	public float getPrimaAnual() {
		return primaAnual;
	}

	/**
	 * @param primaAnual
	 *            the primaAnual to set
	 */
	public void setPrimaAnual(float primaAnual) {
		this.primaAnual = primaAnual;
	}

	/**
	 * @return the primaMensual
	 */
	public float getPrimaMensual() {
		return primaMensual;
	}

	/**
	 * @param primaMensual
	 *            the primaMensual to set
	 */
	public void setPrimaMensual(float primaMensual) {
		this.primaMensual = primaMensual;
	}

	/**
	 * @return the primaAnualPesos
	 */
	public long getPrimaAnualPesos() {
		return primaAnualPesos;
	}

	/**
	 * @param primaAnualPesos
	 *            the primaAnualPesos to set
	 */
	public void setPrimaAnualPesos(long primaAnualPesos) {
		this.primaAnualPesos = primaAnualPesos;
	}

	/**
	 * @return the primaMensualPesos
	 */
	public long getPrimaMensualPesos() {
		return primaMensualPesos;
	}

	/**
	 * @param primaMensualPesos
	 *            the primaMensualPesos to set
	 */
	public void setPrimaMensualPesos(long primaMensualPesos) {
		this.primaMensualPesos = primaMensualPesos;
	}

	public void setIdProducto(long idProducto) {
		this.idProducto = idProducto;
	}

	public long getIdProducto() {
		return idProducto;
	}


}
