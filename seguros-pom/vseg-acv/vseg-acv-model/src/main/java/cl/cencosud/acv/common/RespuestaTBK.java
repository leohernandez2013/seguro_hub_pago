package cl.cencosud.acv.common;

import java.io.Serializable;
import java.util.Date;

public class RespuestaTBK implements Serializable {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 3570428959727650139L;

    private String TBK_ORDEN_COMPRA;
    private String TBK_TIPO_TRANSACCION;
    private int TBK_RESPUESTA;
    private int TBK_MONTO;
    private String TBK_CODIGO_AUTORIZACION;
    private int TBK_FINAL_NUMERO_TARJETA;
    private int TBK_FECHA_CONTABLE;
    private int TBK_FECHA_TRANSACCION;
    private int TBK_HORA_TRANSACCION;
    private String TBK_ID_SESION;
    private long TBK_ID_TRANSACCION;
    private String TBK_TIPO_PAGO;
    private int TBK_NUMERO_CUOTAS;
    private int TBK_TASA_INTERES_MAX;
    private String TBK_VCI;
    private String TBK_MAC;
    private Date FECHA;

    /**
     * @return retorna el valor del atributo tBK_ORDEN_COMPRA
     */
    public String getTBK_ORDEN_COMPRA() {
        return TBK_ORDEN_COMPRA;
    }

    /**
     * @param tbk_orden_compra a establecer en el atributo tBK_ORDEN_COMPRA.
     */
    public void setTBK_ORDEN_COMPRA(String tbk_orden_compra) {
        TBK_ORDEN_COMPRA = tbk_orden_compra;
    }

    /**
     * @return retorna el valor del atributo tBK_TIPO_TRANSACCION
     */
    public String getTBK_TIPO_TRANSACCION() {
        return TBK_TIPO_TRANSACCION;
    }

    /**
     * @param tbk_tipo_transaccion a establecer en el atributo tBK_TIPO_TRANSACCION.
     */
    public void setTBK_TIPO_TRANSACCION(String tbk_tipo_transaccion) {
        TBK_TIPO_TRANSACCION = tbk_tipo_transaccion;
    }

    /**
     * @return retorna el valor del atributo tBK_RESPUESTA
     */
    public int getTBK_RESPUESTA() {
        return TBK_RESPUESTA;
    }

    /**
     * @param tbk_respuesta a establecer en el atributo tBK_RESPUESTA.
     */
    public void setTBK_RESPUESTA(int tbk_respuesta) {
        TBK_RESPUESTA = tbk_respuesta;
    }

    /**
     * @return retorna el valor del atributo tBK_MONTO
     */
    public int getTBK_MONTO() {
        return TBK_MONTO;
    }

    /**
     * @param tbk_monto a establecer en el atributo tBK_MONTO.
     */
    public void setTBK_MONTO(int tbk_monto) {
        TBK_MONTO = tbk_monto;
    }

    /**
     * @return retorna el valor del atributo tBK_CODIGO_AUTORIZACION
     */
    public String getTBK_CODIGO_AUTORIZACION() {
        return TBK_CODIGO_AUTORIZACION;
    }

    /**
     * @param tbk_codigo_autorizacion a establecer en el atributo tBK_CODIGO_AUTORIZACION.
     */
    public void setTBK_CODIGO_AUTORIZACION(String tbk_codigo_autorizacion) {
        TBK_CODIGO_AUTORIZACION = tbk_codigo_autorizacion;
    }

    /**
     * @return retorna el valor del atributo tBK_FINAL_NUMERO_TARJETA
     */
    public int getTBK_FINAL_NUMERO_TARJETA() {
        return TBK_FINAL_NUMERO_TARJETA;
    }

    /**
     * @param tbk_final_numero_tarjeta a establecer en el atributo tBK_FINAL_NUMERO_TARJETA.
     */
    public void setTBK_FINAL_NUMERO_TARJETA(int tbk_final_numero_tarjeta) {
        TBK_FINAL_NUMERO_TARJETA = tbk_final_numero_tarjeta;
    }

    /**
     * @return retorna el valor del atributo tBK_FECHA_CONTABLE
     */
    public int getTBK_FECHA_CONTABLE() {
        return TBK_FECHA_CONTABLE;
    }

    /**
     * @param tbk_fecha_contable a establecer en el atributo tBK_FECHA_CONTABLE.
     */
    public void setTBK_FECHA_CONTABLE(int tbk_fecha_contable) {
        TBK_FECHA_CONTABLE = tbk_fecha_contable;
    }

    /**
     * @return retorna el valor del atributo tBK_FECHA_TRANSACCION
     */
    public int getTBK_FECHA_TRANSACCION() {
        return TBK_FECHA_TRANSACCION;
    }

    /**
     * @param tbk_fecha_transaccion a establecer en el atributo tBK_FECHA_TRANSACCION.
     */
    public void setTBK_FECHA_TRANSACCION(int tbk_fecha_transaccion) {
        TBK_FECHA_TRANSACCION = tbk_fecha_transaccion;
    }

    /**
     * @return retorna el valor del atributo tBK_HORA_TRANSACCION
     */
    public int getTBK_HORA_TRANSACCION() {
        return TBK_HORA_TRANSACCION;
    }

    /**
     * @param tbk_hora_transaccion a establecer en el atributo tBK_HORA_TRANSACCION.
     */
    public void setTBK_HORA_TRANSACCION(int tbk_hora_transaccion) {
        TBK_HORA_TRANSACCION = tbk_hora_transaccion;
    }

    /**
     * @return retorna el valor del atributo tBK_ID_SESION
     */
    public String getTBK_ID_SESION() {
        return TBK_ID_SESION;
    }

    /**
     * @param tbk_id_sesion a establecer en el atributo tBK_ID_SESION.
     */
    public void setTBK_ID_SESION(String tbk_id_sesion) {
        TBK_ID_SESION = tbk_id_sesion;
    }

    /**
     * @return retorna el valor del atributo tBK_ID_TRANSACCION
     */
    public long getTBK_ID_TRANSACCION() {
        return TBK_ID_TRANSACCION;
    }

    /**
     * @param tbk_id_transaccion a establecer en el atributo tBK_ID_TRANSACCION.
     */
    public void setTBK_ID_TRANSACCION(long tbk_id_transaccion) {
        TBK_ID_TRANSACCION = tbk_id_transaccion;
    }

    /**
     * @return retorna el valor del atributo tBK_TIPO_PAGO
     */
    public String getTBK_TIPO_PAGO() {
        return TBK_TIPO_PAGO;
    }

    /**
     * @param tbk_tipo_pago a establecer en el atributo tBK_TIPO_PAGO.
     */
    public void setTBK_TIPO_PAGO(String tbk_tipo_pago) {
        TBK_TIPO_PAGO = tbk_tipo_pago;
    }

    /**
     * @return retorna el valor del atributo tBK_NUMERO_CUOTAS
     */
    public int getTBK_NUMERO_CUOTAS() {
        return TBK_NUMERO_CUOTAS;
    }

    /**
     * @param tbk_numero_cuotas a establecer en el atributo tBK_NUMERO_CUOTAS.
     */
    public void setTBK_NUMERO_CUOTAS(int tbk_numero_cuotas) {
        TBK_NUMERO_CUOTAS = tbk_numero_cuotas;
    }

    /**
     * @return retorna el valor del atributo tBK_TASA_INTERES_MAX
     */
    public int getTBK_TASA_INTERES_MAX() {
        return TBK_TASA_INTERES_MAX;
    }

    /**
     * @param tbk_tasa_interes_max a establecer en el atributo tBK_TASA_INTERES_MAX.
     */
    public void setTBK_TASA_INTERES_MAX(int tbk_tasa_interes_max) {
        TBK_TASA_INTERES_MAX = tbk_tasa_interes_max;
    }

    /**
     * @return retorna el valor del atributo tBK_VCI
     */
    public String getTBK_VCI() {
        return TBK_VCI;
    }

    /**
     * @param tbk_vci a establecer en el atributo tBK_VCI.
     */
    public void setTBK_VCI(String tbk_vci) {
        TBK_VCI = tbk_vci;
    }

    /**
     * @return retorna el valor del atributo tBK_MAC
     */
    public String getTBK_MAC() {
        return TBK_MAC;
    }

    /**
     * @param tbk_mac a establecer en el atributo tBK_MAC.
     */
    public void setTBK_MAC(String tbk_mac) {
        TBK_MAC = tbk_mac;
    }

    /**
     * @return retorna el valor del atributo fECHA
     */
    public Date getFECHA() {
        return FECHA;
    }

    /**
     * @param fecha a establecer en el atributo fECHA.
     */
    public void setFECHA(Date fecha) {
        FECHA = fecha;
    }

}
