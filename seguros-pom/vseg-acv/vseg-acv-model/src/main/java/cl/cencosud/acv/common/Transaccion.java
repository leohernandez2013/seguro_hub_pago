package cl.cencosud.acv.common;

import java.io.Serializable;
import java.util.Date;

public class Transaccion implements Serializable {

    /**
     * TODO Describir atributo serialVersionUID.
     */
    private static final long serialVersionUID = -3467098385046387217L;

    private long id_transaccion;
    private long id_solicitud;
    private String numero_orden_compra;
    private int tipo_medio_pago;
    private int tipo_cargo;
    private long monto;
    private String estado_transaccion;
    private String nro_tarjeta_entrada;
    private String nro_tarjeta_salida;
    private String respuesta_medio;
    private Date fecha_creacion;
    private Date fecha_modificacion;
    private int id_pais;
    private long rut_cliente;

    /**
     * @return retorna el valor del atributo id_transaccion
     */
    public long getId_transaccion() {
        return id_transaccion;
    }

    /**
     * @param id_transaccion a establecer en el atributo id_transaccion.
     */
    public void setId_transaccion(long id_transaccion) {
        this.id_transaccion = id_transaccion;
    }

    /**
     * @return retorna el valor del atributo id_solicitud
     */
    public long getId_solicitud() {
        return id_solicitud;
    }

    /**
     * @param id_solicitud a establecer en el atributo id_solicitud.
     */
    public void setId_solicitud(long id_solicitud) {
        this.id_solicitud = id_solicitud;
    }

    /**
     * @return retorna el valor del atributo numero_orden_compra
     */
    public String getNumero_orden_compra() {
        return numero_orden_compra;
    }

    /**
     * @param numero_orden_compra a establecer en el atributo numero_orden_compra.
     */
    public void setNumero_orden_compra(String numero_orden_compra) {
        this.numero_orden_compra = numero_orden_compra;
    }

    /**
     * @return retorna el valor del atributo tipo_medio_pago
     */
    public int getTipo_medio_pago() {
        return tipo_medio_pago;
    }

    /**
     * @param tipo_medio_pago a establecer en el atributo tipo_medio_pago.
     */
    public void setTipo_medio_pago(int tipo_medio_pago) {
        this.tipo_medio_pago = tipo_medio_pago;
    }

    /**
     * @return retorna el valor del atributo tipo_cargo
     */
    public int getTipo_cargo() {
        return tipo_cargo;
    }

    /**
     * @param tipo_cargo a establecer en el atributo tipo_cargo.
     */
    public void setTipo_cargo(int tipo_cargo) {
        this.tipo_cargo = tipo_cargo;
    }

    /**
     * @return retorna el valor del atributo monto
     */
    public long getMonto() {
        return monto;
    }

    /**
     * @param monto a establecer en el atributo monto.
     */
    public void setMonto(long monto) {
        this.monto = monto;
    }

    /**
     * @return retorna el valor del atributo estado_transaccion
     */
    public String getEstado_transaccion() {
        return estado_transaccion;
    }

    /**
     * @param estado_transaccion a establecer en el atributo estado_transaccion.
     */
    public void setEstado_transaccion(String estado_transaccion) {
        this.estado_transaccion = estado_transaccion;
    }

    /**
     * @return retorna el valor del atributo nro_tarjeta_entrada
     */
    public String getNro_tarjeta_entrada() {
        return nro_tarjeta_entrada;
    }

    /**
     * @param nro_tarjeta_entrada a establecer en el atributo nro_tarjeta_entrada.
     */
    public void setNro_tarjeta_entrada(String nro_tarjeta_entrada) {
        this.nro_tarjeta_entrada = nro_tarjeta_entrada;
    }

    /**
     * @return retorna el valor del atributo nro_tarjeta_salida
     */
    public String getNro_tarjeta_salida() {
        return nro_tarjeta_salida;
    }

    /**
     * @param nro_tarjeta_salida a establecer en el atributo nro_tarjeta_salida.
     */
    public void setNro_tarjeta_salida(String nro_tarjeta_salida) {
        this.nro_tarjeta_salida = nro_tarjeta_salida;
    }

    /**
     * @return retorna el valor del atributo respuesta_medio
     */
    public String getRespuesta_medio() {
        return respuesta_medio;
    }

    /**
     * @param respuesta_medio a establecer en el atributo respuesta_medio.
     */
    public void setRespuesta_medio(String respuesta_medio) {
        this.respuesta_medio = respuesta_medio;
    }

    /**
     * @return retorna el valor del atributo fecha_creacion
     */
    public Date getFecha_creacion() {
        return fecha_creacion;
    }

    /**
     * @param fecha_creacion a establecer en el atributo fecha_creacion.
     */
    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    /**
     * @return retorna el valor del atributo fecha_modificacion
     */
    public Date getFecha_modificacion() {
        return fecha_modificacion;
    }

    /**
     * @param fecha_modificacion a establecer en el atributo fecha_modificacion.
     */
    public void setFecha_modificacion(Date fecha_modificacion) {
        this.fecha_modificacion = fecha_modificacion;
    }

    /**
     * @return retorna el valor del atributo id_pais
     */
    public int getId_pais() {
        return id_pais;
    }

    /**
     * @param id_pais a establecer en el atributo id_pais.
     */
    public void setId_pais(int id_pais) {
        this.id_pais = id_pais;
    }

    /**
     * @return retorna el valor del atributo rut_cliente
     */
    public long getRut_cliente() {
        return rut_cliente;
    }

    /**
     * @param rut_cliente a establecer en el atributo rut_cliente.
     */
    public void setRut_cliente(long rut_cliente) {
        this.rut_cliente = rut_cliente;
    }

}
