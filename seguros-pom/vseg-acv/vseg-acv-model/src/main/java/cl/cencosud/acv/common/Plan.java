package cl.cencosud.acv.common;

import java.io.Serializable;
import java.util.Comparator;

public class Plan implements Serializable, Comparable < Plan > {

    /**
     * Identificador de la clase para serializci�n.
     */
    private static final long serialVersionUID = 47179529356258678L;
    /**
     * Identificador del plan.
     */
    private String idPlan;
    /**
     * Identificador del plan legacy.
     */
    private String idPlanLegacy;
    /**
     * Nombre de la compa�ia.
     */
    private String compannia;
    /**
     * C�digo de la compa��a que inspecciona.
     */
    private String codigoCompanniaInspeccion;
    /**
     * Nombre del plan.
     */
    private String nombrePlan;
    /**
     * Prima mensual en UF.
     */
    private float primaMensualUF;
    /**
     * Prima mensual en pesos.
     */
    private long primaMensualPesos;
    /**
     * Prima anual en UF.
     */
    private float primaAnualUF;
    /**
     * Prima anual en pesos.
     */
    private long primaAnualPesos;
    /**
     * Identificador de la clase para serializci�n.
     */
    private Cobertura[] coberturas;
    /**
     * Identificador que indica si a plan le corresponde
     * inspeccion vehicular.
     */
    private int inspeccionVehi;
    /**
     * Identificador que indica el dia maximo de activacion
     * del seguro.
     */
    private int maxDiaIniVig;

    private Integer solicitaAutoNuevo;
    private Integer solicitaFactura;
    private String formaPago;
    
    private long idProducto;
    
    private String imagenPP;
    
    private String imagenPS;
    
    private String hiddProducto;
     
    
    

    /**
     * @return the idPlan
     */
    public String getIdPlan() {
        return idPlan;
    }

    /**
     * @param idPlan
     *            the idPlan to set
     */
    public void setIdPlan(String idPlan) {
        this.idPlan = idPlan;
    }

    /**
     * @return the idPlanLegacy
     */
    public String getIdPlanLegacy() {
        return idPlanLegacy;
    }

    /**
     * @param idPlanLegacy
     *            the idPlanLegacy to set
     */
    public void setIdPlanLegacy(String idPlanLegacy) {
        this.idPlanLegacy = idPlanLegacy;
    }

    /**
     * @return the compannia
     */
    public String getCompannia() {
        return compannia;
    }

    /**
     * @param compannia
     *            the compannia to set
     */
    public void setCompannia(String compannia) {
        this.compannia = compannia;
    }

    /**
     * @return the codigoCompanniaInspeccion
     */
    public String getCodigoCompanniaInspeccion() {
        return codigoCompanniaInspeccion;
    }

    /**
     * @param codigoCompanniaInspeccion
     *            the codigoCompanniaInspeccion to set
     */
    public void setCodigoCompanniaInspeccion(String codigoCompanniaInspeccion) {
        this.codigoCompanniaInspeccion = codigoCompanniaInspeccion;
    }

    /**
     * @return the nombrePlan
     */
    public String getNombrePlan() {
        return nombrePlan;
    }

    /**
     * @param nombrePlan
     *            the nombrePlan to set
     */
    public void setNombrePlan(String nombrePlan) {
        this.nombrePlan = nombrePlan;
    }

    /**
     * @return the primaMensualUF
     */
    public float getPrimaMensualUF() {
        return primaMensualUF;
    }

    /**
     * @param primaMensualUF
     *            the primaMensualUF to set
     */
    public void setPrimaMensualUF(float primaMensualUF) {
        this.primaMensualUF = primaMensualUF;
    }

    /**
     * @return the primaMensualPesos
     */
    public long getPrimaMensualPesos() {
        return primaMensualPesos;
    }

    /**
     * @param primaMensualPesos
     *            the primaMensualPesos to set
     */
    public void setPrimaMensualPesos(long primaMensualPesos) {
        this.primaMensualPesos = primaMensualPesos;
    }

    /**
     * @return the primaAnualUF
     */
    public float getPrimaAnualUF() {
        return primaAnualUF;
    }

    /**
     * @param primaAnualUF
     *            the primaAnualUF to set
     */
    public void setPrimaAnualUF(float primaAnualUF) {
        this.primaAnualUF = primaAnualUF;
    }

    /**
     * @return the primaAnualPesos
     */
    public long getPrimaAnualPesos() {
        return primaAnualPesos;
    }

    /**
     * @param primaAnualPesos
     *            the primaAnualPesos to set
     */
    public void setPrimaAnualPesos(long primaAnualPesos) {
        this.primaAnualPesos = primaAnualPesos;
    }

    /**
     * @return the coberturas
     */
    public Cobertura[] getCoberturas() {
        return coberturas;
    }

    /**
     * @param coberturas
     *            the coberturas to set
     */
    public void setCoberturas(Cobertura[] coberturas) {
        this.coberturas = coberturas;
    }

    /**
     * @return the inspeccionVehi
     */
    public int getInspeccionVehi() {
        return inspeccionVehi;
    }

    /**
     * @param inspeccionVehi
     *            the inspeccionVehi to set
     */
    public void setInspeccionVehi(int inspeccionVehi) {
        this.inspeccionVehi = inspeccionVehi;
    }

    /**
     * @return the maxDiaIniVig
     */
    public int getMaxDiaIniVig() {
        return maxDiaIniVig;
    }

    /**
     * @param maxDiaIniVig
     *            the maxDiaIniVig to set
     */
    public void setMaxDiaIniVig(int maxDiaIniVig) {
        this.maxDiaIniVig = maxDiaIniVig;
    }


    /**
     * @return retorna el valor del atributo solicitaAutoNuevo
     */
    public Integer getSolicitaAutoNuevo() {
        return solicitaAutoNuevo;
    }

    /**
     * @param solicitaAutoNuevo a establecer en el atributo solicitaAutoNuevo.
     */
    public void setSolicitaAutoNuevo(Integer solicitaAutoNuevo) {
        this.solicitaAutoNuevo = solicitaAutoNuevo;
    }

    /**
     * @return retorna el valor del atributo solicitaFactura
     */
    public Integer getSolicitaFactura() {
        return solicitaFactura;
    }

    /**
     * @param solicitaFactura a establecer en el atributo solicitaFactura.
     */
    public void setSolicitaFactura(Integer solicitaFactura) {
        this.solicitaFactura = solicitaFactura;
    }
    
    /**
     * @return retorna el valor del atributo formaPago
     */
    public String getFormaPago() {
        return formaPago;
    }

    /**
     * @param formaPago a establecer en el atributo formaPago.
     */
    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

    @Override
    public String toString() {
        String NEW_LINE = System.getProperty("line.separator");
        StringBuffer out = new StringBuffer();
        out.append("[[Plan]");
        out.append("[coberturas=" + this.coberturas + "]");
        out.append("[codigoCompanniaInspeccion="
            + this.codigoCompanniaInspeccion + "]");
        out.append("[compannia=" + this.compannia + "]");
        out.append("[idPlan=" + this.idPlan + "]");
        out.append("[idPlanLegacy=" + this.idPlanLegacy + "]");
        out.append("[idPlanLegacy=" + this.idPlanLegacy + "]");
        out.append("[inspeccionVehi=" + this.inspeccionVehi + "]");
        out.append("[maxDiaIniVig=" + this.maxDiaIniVig + "]");
        out.append("[solicitaAutoNuevo=" + this.solicitaAutoNuevo + "]");
        out.append("[solicitaFactura=" + this.solicitaFactura + "]");
        out.append("[formaPago=" + this.formaPago + "]");
        out.append("[nombrePlan=" + this.nombrePlan + "]");
        out.append("[primaAnualPesos=" + this.primaAnualPesos + "]");
        out.append("[primaAnualUF=" + this.primaAnualUF + "]");
        out.append("[primaMensualPesos=" + this.primaMensualPesos + "]");
        out.append("[primaMensualUF=" + this.primaMensualUF + "]");
        out.append("]" + NEW_LINE);

        return out.toString();
    }

    /**
     * Genera el hashCode del objeto.
     * @return Hashcode generado
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result =
            prime
                * result
                + ((codigoCompanniaInspeccion == null) ? 0
                    : codigoCompanniaInspeccion.hashCode());
        result =
            prime * result + ((compannia == null) ? 0 : compannia.hashCode());
        result = prime * result + ((idPlan == null) ? 0 : idPlan.hashCode());
        result =
            prime * result
                + ((idPlanLegacy == null) ? 0 : idPlanLegacy.hashCode());
        result = prime * result + inspeccionVehi;
        result = prime * result + maxDiaIniVig;
        result = prime * result + solicitaAutoNuevo;
        result = prime * result + solicitaFactura;
        result =
            prime * result + ((formaPago == null) ? 0 : formaPago.hashCode());
        result =
            prime * result + ((nombrePlan == null) ? 0 : nombrePlan.hashCode());
        result =
            prime * result + (int) (primaAnualPesos ^ (primaAnualPesos >>> 32));
        result = prime * result + Float.floatToIntBits(primaAnualUF);
        result =
            prime * result
                + (int) (primaMensualPesos ^ (primaMensualPesos >>> 32));
        result = prime * result + Float.floatToIntBits(primaMensualUF);
        return result;
    }

    /**
     * Valida si un objeto es igual a otro.
     * @param obj Objeto a ser validado.
     * @return verdadero si el objeto es igual
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Plan))
            return false;
        Plan other = (Plan) obj;
        if (codigoCompanniaInspeccion == null) {
            if (other.codigoCompanniaInspeccion != null)
                return false;
        } else if (!codigoCompanniaInspeccion
            .equals(other.codigoCompanniaInspeccion))
            return false;
        if (compannia == null) {
            if (other.compannia != null)
                return false;
        } else if (!compannia.equals(other.compannia))
            return false;
        if (idPlan == null) {
            if (other.idPlan != null)
                return false;
        } else if (!idPlan.equals(other.idPlan))
            return false;
        if (idPlanLegacy == null) {
            if (other.idPlanLegacy != null)
                return false;
        } else if (!idPlanLegacy.equals(other.idPlanLegacy))
            return false;
        if (inspeccionVehi != other.inspeccionVehi)
            return false;
        if (maxDiaIniVig != other.maxDiaIniVig)
            return false;
        if (solicitaAutoNuevo != other.solicitaAutoNuevo)
            return false;
        if (solicitaFactura != other.solicitaFactura)
            return false;
        if (formaPago == null) {
            if (other.formaPago != null)
                return false;
        } else if (!formaPago.equals(other.formaPago))
            return false;
        if (nombrePlan == null) {
            if (other.nombrePlan != null)
                return false;
        } else if (!nombrePlan.equals(other.nombrePlan))
            return false;
        if (primaAnualPesos != other.primaAnualPesos)
            return false;
        if (Float.floatToIntBits(primaAnualUF) != Float
            .floatToIntBits(other.primaAnualUF))
            return false;
        if (primaMensualPesos != other.primaMensualPesos)
            return false;
        if (Float.floatToIntBits(primaMensualUF) != Float
            .floatToIntBits(other.primaMensualUF))
            return false;
        return true;
    }

    /**
     * Compara el objeto con otro.
     * @param plan Objeto a ser comparado
     * @return cero si son iguales 1 si es mayor y 
     * menor que cero es menor.
     */
    public int compareTo(Plan plan) {
        int cmp = 0;
        if (plan != null) {
        	if (this.idProducto == 0.0 && this.compannia.equalsIgnoreCase("")){
            cmp =
                new Long(this.primaMensualPesos).compareTo(new Long(plan
                    .getPrimaMensualPesos()));
        	}
        	else if (this.idProducto != 0.0){
        		cmp =
                    new Long(this.idProducto).compareTo(new Long(plan
                        .getIdProducto()));
        	} else if (!this.compannia.equalsIgnoreCase("")){
        		cmp =
                    new Long(this.compannia).compareTo(new Long(plan
                        .getCompannia()));
        	}
        }
        return cmp;
    }
//
//    public static final Comparator<Plan> ORDER_PRECIO = new Comparator<Plan>() {
//
//		public int compare(Plan o1, Plan o2) {
//			return (o1.getPrimaMensualPesos() < o2.getPrimaMensualPesos() ? 1: (o1.getPrimaMensualPesos()==o2.getPrimaMensualPesos()?0:-1));
//		}
//	};  
//	
//	public static final Comparator<Plan> ORDER_DEDUCIBLE = new Comparator<Plan>() {
//
//		public int compare(Plan o1, Plan o2) {
//			return (o1.getIdProducto() < o2.getIdProducto() ? 1: (o1.getIdProducto()==o2.getIdProducto()?0:-1));
//		}
//	};   
//	
//	public static final Comparator<Plan> ORDER_NOMBRE_COMPANNIA = new Comparator<Plan>() {
//
//		public int compare(Plan o1, Plan o2) {
//			return o1.getCompannia().compareToIgnoreCase(o2.getCompannia());
//		}
//	};  
    
	public void setIdProducto(long idProducto) {
		this.idProducto = idProducto;
	}

	public long getIdProducto() {
		return idProducto;
	}

	public void setImagenPP(String imagenPP) {
		this.imagenPP = imagenPP;
	}

	public String getImagenPP() {
		return imagenPP;
	}

	public void setImagenPS(String imagenPS) {
		this.imagenPS = imagenPS;
	}

	public String getImagenPS() {
		return imagenPS;
	}

	public void setHiddProducto(String hiddProducto) {
		this.hiddProducto = hiddProducto;
	}

	public String getHiddProducto() {
		return hiddProducto;
	}

	public Object get(String string) {
		// TODO Auto-generated method stub
		return null;
	}


}
