package cl.cencosud.acv.common;

import java.io.Serializable;

public class SolicitudInspeccion implements Serializable {

	private static final long serialVersionUID = -8706110848453414812L;
	
	private String id_solicitud;
	private String ramo;
	private String cod_compannia_inspeccion;
	private String nombre_asegurado;
	private String rut_asegurado;
	private String nombre_contacto;
	private String nombre_corredor;
	private String direccion;
	private String comuna;
	private String telefono_contacto;
	private String usuario_interno_bsp;
	private String tipo_inspeccion;
	private String observaciones;
	private String id_solicitud_inspeccion;
	private String numero_patente;
	private String referencia_interna;
	private String celular;
	//hs agrego atributo factura
	private byte[] factura;
	
	public String getNombre_corredor() {
		return nombre_corredor;
	}
	public void setNombre_corredor(String nombreCorredor) {
		nombre_corredor = nombreCorredor;
	}
	public String getRamo() {
		return ramo;
	}
	public void setRamo(String ramo) {
		this.ramo = ramo;
	}
	public String getCod_compannia_inspeccion() {
		return cod_compannia_inspeccion;
	}
	public void setCod_compannia_inspeccion(String codCompanniaInspeccion) {
		cod_compannia_inspeccion = codCompanniaInspeccion;
	}
	public String getId_solicitud() {
		return id_solicitud;
	}
	public void setId_solicitud(String idSolicitud) {
		id_solicitud = idSolicitud;
	}
	public String getId_solicitud_inspeccion() {
		return id_solicitud_inspeccion;
	}
	public void setId_solicitud_inspeccion(String idSolicitudInspeccion) {
		id_solicitud_inspeccion = idSolicitudInspeccion;
	}
	public String getNombre_asegurado() {
		return nombre_asegurado;
	}
	public void setNombre_asegurado(String nombreAsegurado) {
		nombre_asegurado = nombreAsegurado;
	}
	public String getRut_asegurado() {
		return rut_asegurado;
	}
	public void setRut_asegurado(String rutAsegurado) {
		rut_asegurado = rutAsegurado;
	}
	public String getNombre_contacto() {
		return nombre_contacto;
	}
	public void setNombre_contacto(String nombreContacto) {
		nombre_contacto = nombreContacto;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getComuna() {
		return comuna;
	}
	public void setComuna(String comuna) {
		this.comuna = comuna;
	}
	public String getTelefono_contacto() {
		return telefono_contacto;
	}
	public void setTelefono_contacto(String telefonoContacto) {
		telefono_contacto = telefonoContacto;
	}
	public String getUsuario_interno_bsp() {
		return usuario_interno_bsp;
	}
	public void setUsuario_interno_bsp(String usuarioInternoBsp) {
		usuario_interno_bsp = usuarioInternoBsp;
	}
	public String getTipo_inspeccion() {
		return tipo_inspeccion;
	}
	public void setTipo_inspeccion(String tipoInspeccion) {
		tipo_inspeccion = tipoInspeccion;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
    /**
     * @return retorna el valor del atributo numero_patente
     */
    public String getNumero_patente() {
        return numero_patente;
    }
    /**
     * @param numero_patente a establecer en el atributo numero_patente.
     */
    public void setNumero_patente(String numero_patente) {
        this.numero_patente = numero_patente;
    }
    /**
     * @return retorna el valor del atributo referencia_interna
     */
    public String getReferencia_interna() {
        return referencia_interna;
    }
    /**
     * @param referencia_interna a establecer en el atributo referencia_interna.
     */
    public void setReferencia_interna(String referencia_interna) {
        this.referencia_interna = referencia_interna;
    }
    /**
     * @return retorna el valor del atributo celular
     */
    public String getCelular() {
        return celular;
    }
    /**
     * @param celular a establecer en el atributo celular.
     */
    public void setCelular(String celular) {
        this.celular = celular;
    }
	public byte[] getFactura() {
		return factura;
	}
	public void setFactura(byte[] factura) {
		this.factura = factura;
	}
	
}
