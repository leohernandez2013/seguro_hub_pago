package cl.cencosud.acv.common.exception;

import cl.cencosud.acv.common.config.ACVErrorsConfig;
import cl.tinet.common.config.AbstractConfigurator;
import cl.tinet.common.model.exception.BusinessException;

public abstract class ACVException extends BusinessException {

    public ACVException(String messageKey, Object[] arguments) {
        super(messageKey, arguments);
    }

    public ACVException(BusinessException[] exceptions) {
        super(exceptions);
    }

    @Override
    public AbstractConfigurator loadConfigurator() {
        return ACVErrorsConfig.getInstance();
    }

}