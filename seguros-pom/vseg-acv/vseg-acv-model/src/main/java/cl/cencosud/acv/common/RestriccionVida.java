package cl.cencosud.acv.common;

import java.io.Serializable;

public class RestriccionVida implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -7259449514066786629L;

    private long idRestriccion;

    private long idPlan;

    private int requiereGrupoFamiliar;

    private boolean requiereBeneficiarios;
    
    private int requiereCapital;

    private int edadMinimaIngreso;

    private int edadMaximaHijo;

    private int requiereValidarPlan;

    private int requierePreguntaScoring;

    private ParentescoAsegurado[] parentescos;

    public long getIdRestriccion() {
        return idRestriccion;
    }

    public void setIdRestriccion(long idRestriccion) {
        this.idRestriccion = idRestriccion;
    }

    public long getIdPlan() {
        return idPlan;
    }

    public void setIdPlan(long idPlan) {
        this.idPlan = idPlan;
    }

    public int getRequiereGrupoFamiliar() {
        return requiereGrupoFamiliar;
    }

    public void setRequiereGrupoFamiliar(int requiereGrupoFamiliar) {
        this.requiereGrupoFamiliar = requiereGrupoFamiliar;
    }

    public boolean isRequiereBeneficiarios() {
		return requiereBeneficiarios;
	}

	public void setRequiereBeneficiarios(boolean requiereBeneficiarios) {
		this.requiereBeneficiarios = requiereBeneficiarios;
	}

	public int getRequiereCapital() {
        return requiereCapital;
    }

    public void setRequiereCapital(int requiereCapital) {
        this.requiereCapital = requiereCapital;
    }

    public int getEdadMinimaIngreso() {
        return edadMinimaIngreso;
    }

    public void setEdadMinimaIngreso(int edadMinimaIngreso) {
        this.edadMinimaIngreso = edadMinimaIngreso;
    }

    public int getEdadMaximaHijo() {
        return edadMaximaHijo;
    }

    public void setEdadMaximaHijo(int edadMaximaHijo) {
        this.edadMaximaHijo = edadMaximaHijo;
    }

    public int getRequiereValidarPlan() {
        return requiereValidarPlan;
    }

    public void setRequiereValidarPlan(int requiereValidarPlan) {
        this.requiereValidarPlan = requiereValidarPlan;
    }

    public int getRequierePreguntaScoring() {
        return requierePreguntaScoring;
    }

    public void setRequierePreguntaScoring(int requierePreguntaScoring) {
        this.requierePreguntaScoring = requierePreguntaScoring;
    }

    public ParentescoAsegurado[] getParentescos() {
        return parentescos;
    }

    public void setParentescos(ParentescoAsegurado[] parentescos) {
        this.parentescos = parentescos;
    }

}
