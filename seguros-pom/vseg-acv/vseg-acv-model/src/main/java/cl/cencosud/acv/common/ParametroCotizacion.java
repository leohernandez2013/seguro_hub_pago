package cl.cencosud.acv.common;

import java.io.Serializable;

/**
 * Clase encargada de transportar valores de listados en el proceso de
 * cotizacion.
 * 
 * @author Miguel Garc�a (TInet).
 * 
 */
public class ParametroCotizacion implements Serializable {

	/**
	 * Identificador de la Clase para Serializaci�n.
	 */
	private static final long serialVersionUID = -8636502787055467934L;
	/**
	 * Identificador del parametro.
	 */
	private String id;
	/**
	 * Descripci�n del parametro.
	 */
	private String descripcion;
	/**
	 * Identificador del legacy.
	 */
	private String idLegacy;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the idLegacy
	 */
	public String getIdLegacy() {
		return idLegacy;
	}

	/**
	 * @param idLegacy
	 *            the idLegacy to set
	 */
	public void setIdLegacy(String idLegacy) {
		this.idLegacy = idLegacy;
	}

	public String toString() {
		String result = "";
		result += "(id=" + this.id + ")";
		result += "(descripcion=" + this.descripcion + ")";
		result += "(idLegacy=" + this.idLegacy + ")";
		return result;
	}

}
