package cl.cencosud.acv.common;

public class DuenyoVehiculo extends Contratante {

	/**
	 * Identificador de la clase para Serializacion.
	 */
	private static final long serialVersionUID = -5782172639480965476L;
	/**
	 * Comuna de residencia.
	 */
	private String comunaResidencia;

	/**
	 * @return the comunaResidencia
	 */
	public String getComunaResidencia() {
		return comunaResidencia;
	}

	/**
	 * @param comunaResidencia
	 *            the comunaResidencia to set
	 */
	public void setComunaResidencia(String comunaResidencia) {
		this.comunaResidencia = comunaResidencia;
	}

}
