package cl.cencosud.asesorcotizador.interfaces;

import java.rmi.RemoteException;

import com.ibm.ejs.container.*;

/**
 * EJSRemoteStatelessPagos_0da9f0f8
 */
public class EJSRemoteStatelessPagos_0da9f0f8 extends EJSWrapper implements Pagos {
	/**
	 * EJSRemoteStatelessPagos_0da9f0f8
	 */
	public EJSRemoteStatelessPagos_0da9f0f8() throws java.rmi.RemoteException {
		super();	}
	/**
	 * aceptaTransaccion
	 */
	public boolean aceptaTransaccion(cl.cencosud.acv.common.Transaccion transaccion) throws cl.tinet.common.model.exception.BusinessException, java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		boolean _EJS_result = false;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = transaccion;
			}
	cl.cencosud.asesorcotizador.ejb.PagosBean beanRef = (cl.cencosud.asesorcotizador.ejb.PagosBean)container.preInvoke(this, 0, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.aceptaTransaccion(transaccion);
		}
		catch (cl.tinet.common.model.exception.BusinessException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 0, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * pagarCencosud
	 */
	public boolean pagarCencosud(cl.cencosud.acv.common.Transaccion transaccion) throws cl.tinet.common.model.exception.BusinessException, java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		boolean _EJS_result = false;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = transaccion;
			}
	cl.cencosud.asesorcotizador.ejb.PagosBean beanRef = (cl.cencosud.asesorcotizador.ejb.PagosBean)container.preInvoke(this, 1, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.pagarCencosud(transaccion);
		}
		catch (cl.tinet.common.model.exception.BusinessException ex) {
			_EJS_s.setCheckedException(ex);
			throw ex;
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 1, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * validarDigitosTarjeta
	 */
	public boolean validarDigitosTarjeta(int idSolicitud, java.lang.String tarjeta) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		boolean _EJS_result = false;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[2];
				_jacc_parms[0] = new java.lang.Integer(idSolicitud);
				_jacc_parms[1] = tarjeta;
			}
	cl.cencosud.asesorcotizador.ejb.PagosBean beanRef = (cl.cencosud.asesorcotizador.ejb.PagosBean)container.preInvoke(this, 2, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.validarDigitosTarjeta(idSolicitud, tarjeta);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 2, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerDatosRespuesta
	 */
	public cl.cencosud.acv.common.RespuestaTBK obtenerDatosRespuesta(long idSolicitud) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.RespuestaTBK _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = new java.lang.Long(idSolicitud);
			}
	cl.cencosud.asesorcotizador.ejb.PagosBean beanRef = (cl.cencosud.asesorcotizador.ejb.PagosBean)container.preInvoke(this, 3, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerDatosRespuesta(idSolicitud);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 3, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerDatosTransaccion
	 */
	public cl.cencosud.acv.common.Transaccion obtenerDatosTransaccion(cl.cencosud.acv.common.Transaccion transaccion) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.Transaccion _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = transaccion;
			}
	cl.cencosud.asesorcotizador.ejb.PagosBean beanRef = (cl.cencosud.asesorcotizador.ejb.PagosBean)container.preInvoke(this, 4, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerDatosTransaccion(transaccion);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 4, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * obtenerDatosTransaccion
	 */
	public cl.cencosud.acv.common.Transaccion obtenerDatosTransaccion(int idSolicitud) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		cl.cencosud.acv.common.Transaccion _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = new java.lang.Integer(idSolicitud);
			}
	cl.cencosud.asesorcotizador.ejb.PagosBean beanRef = (cl.cencosud.asesorcotizador.ejb.PagosBean)container.preInvoke(this, 5, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerDatosTransaccion(idSolicitud);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 5, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * solicitarInspeccion
	 */
	public java.lang.String solicitarInspeccion(cl.cencosud.acv.common.SolicitudInspeccion solicitudInspeccion) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.lang.String _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = solicitudInspeccion;
			}
	cl.cencosud.asesorcotizador.ejb.PagosBean beanRef = (cl.cencosud.asesorcotizador.ejb.PagosBean)container.preInvoke(this, 6, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.solicitarInspeccion(solicitudInspeccion);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 6, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * grabarDatosRespuesta
	 */
	public long grabarDatosRespuesta(cl.cencosud.acv.common.RespuestaTBK respuesta) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		long _EJS_result = 0;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = respuesta;
			}
	cl.cencosud.asesorcotizador.ejb.PagosBean beanRef = (cl.cencosud.asesorcotizador.ejb.PagosBean)container.preInvoke(this, 7, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.grabarDatosRespuesta(respuesta);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 7, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
	/**
	 * actualizarEstadoTransaccion
	 */
	public void actualizarEstadoTransaccion(cl.cencosud.acv.common.Transaccion transaccion) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = transaccion;
			}
	cl.cencosud.asesorcotizador.ejb.PagosBean beanRef = (cl.cencosud.asesorcotizador.ejb.PagosBean)container.preInvoke(this, 8, _EJS_s, _jacc_parms);
			beanRef.actualizarEstadoTransaccion(transaccion);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 8, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * actualizarTransaccion
	 */
	public void actualizarTransaccion(cl.cencosud.acv.common.Transaccion transaccion) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = transaccion;
			}
	cl.cencosud.asesorcotizador.ejb.PagosBean beanRef = (cl.cencosud.asesorcotizador.ejb.PagosBean)container.preInvoke(this, 9, _EJS_s, _jacc_parms);
			beanRef.actualizarTransaccion(transaccion);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 9, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * enviarEmail
	 */
	public void enviarEmail(cl.cencosud.acv.common.valorizacion.CotizacionSeguro datosSeguro, 
			java.lang.String numeroOrdenCompra, java.lang.String idRama) 
			throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[3];
				_jacc_parms[0] = datosSeguro;
				_jacc_parms[1] = numeroOrdenCompra;
				_jacc_parms[2] = idRama;
			}
	cl.cencosud.asesorcotizador.ejb.PagosBean beanRef = (cl.cencosud.asesorcotizador.ejb.PagosBean)container.preInvoke(this, 10, _EJS_s, _jacc_parms);
			beanRef.enviarEmail(datosSeguro, numeroOrdenCompra, idRama);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 10, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * generarTransaccion
	 */
	public void generarTransaccion(cl.cencosud.acv.common.Transaccion transaccion) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = transaccion;
			}
	cl.cencosud.asesorcotizador.ejb.PagosBean beanRef = (cl.cencosud.asesorcotizador.ejb.PagosBean)container.preInvoke(this, 11, _EJS_s, _jacc_parms);
			beanRef.generarTransaccion(transaccion);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 11, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * enviarEmail
	 */
	public void guardarNoRegistrado(java.lang.String rutCliente, java.lang.String nombreCliente, java.lang.String mailCliente, java.lang.String tipoSeguro) throws java.rmi.RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[4];
				_jacc_parms[0] = rutCliente;
				_jacc_parms[1] = nombreCliente;
				_jacc_parms[2] = mailCliente;
				_jacc_parms[3] = tipoSeguro;
			}
	cl.cencosud.asesorcotizador.ejb.PagosBean beanRef = (cl.cencosud.asesorcotizador.ejb.PagosBean)container.preInvoke(this, 12, _EJS_s, _jacc_parms);
			beanRef.guardarNoRegistrado(rutCliente, nombreCliente, mailCliente, tipoSeguro);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 12, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	
	public String obtenerParametroSistema(String grupo, String nombre)
			throws RemoteException {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		java.lang.String _EJS_result = null;
		try {
			if (container.doesJaccNeedsEJBArguments( this ))
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = grupo;
				_jacc_parms[1] = nombre;
			}
	cl.cencosud.asesorcotizador.ejb.PagosBean beanRef = (cl.cencosud.asesorcotizador.ejb.PagosBean)container.preInvoke(this, 6, _EJS_s, _jacc_parms);
			_EJS_result = beanRef.obtenerParametroSistema(grupo, nombre);
		}
		catch (java.rmi.RemoteException ex) {
			_EJS_s.setUncheckedException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedException(ex);
			throw new java.rmi.RemoteException("bean method raised unchecked exception", ex);
		}

		finally {
			try{
				container.postInvoke(this, 6, _EJS_s);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return _EJS_result;
	}
}
