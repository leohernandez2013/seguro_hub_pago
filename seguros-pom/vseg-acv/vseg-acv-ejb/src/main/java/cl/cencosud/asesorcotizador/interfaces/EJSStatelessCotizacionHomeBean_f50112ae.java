package cl.cencosud.asesorcotizador.interfaces;

import com.ibm.ejs.container.*;

/**
 * EJSStatelessCotizacionHomeBean_f50112ae
 */
public class EJSStatelessCotizacionHomeBean_f50112ae extends EJSHome {
	static final long serialVersionUID = 61;
	/**
	 * EJSStatelessCotizacionHomeBean_f50112ae
	 */
	public EJSStatelessCotizacionHomeBean_f50112ae() throws java.rmi.RemoteException {
		super();	}
	/**
	 * create
	 */
	public cl.cencosud.asesorcotizador.interfaces.Cotizacion create() throws javax.ejb.CreateException, java.rmi.RemoteException {
BeanO beanO = null;
cl.cencosud.asesorcotizador.interfaces.Cotizacion result = null;
boolean createFailed = false;
try {
	result = (cl.cencosud.asesorcotizador.interfaces.Cotizacion) super.createWrapper(null);
}
catch (javax.ejb.CreateException ex) {
	createFailed = true;
	throw ex;
} catch (java.rmi.RemoteException ex) {
	createFailed = true;
	throw ex;
} catch (Throwable ex) {
	createFailed = true;
	throw new CreateFailureException(ex);
} finally {
	if (createFailed) {
		super.createFailure(beanO);
	}
}
return result;	}
}
