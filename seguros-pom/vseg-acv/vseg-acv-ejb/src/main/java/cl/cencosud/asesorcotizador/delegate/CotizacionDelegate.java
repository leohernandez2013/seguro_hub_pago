package cl.cencosud.asesorcotizador.delegate;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import java.util.logging.Logger;
import javax.ejb.CreateException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.cencosud.acv.common.Actividad;
import cl.cencosud.acv.common.Cobertura;
import cl.cencosud.acv.common.CoberturaSeg;
import cl.cencosud.acv.common.ColorVehiculo;
import cl.cencosud.acv.common.Contratante;
import cl.cencosud.acv.common.EstadoCivil;
import cl.cencosud.acv.common.ParametroCotizacion;
import cl.cencosud.acv.common.PatenteVehiculo;
import cl.cencosud.acv.common.Plan;
import cl.cencosud.acv.common.Producto;
import cl.cencosud.acv.common.Rama;
import cl.cencosud.acv.common.RestriccionVida;
import cl.cencosud.acv.common.Solicitud;
import cl.cencosud.acv.common.Subcategoria;
import cl.cencosud.acv.common.Vehiculo;
import cl.cencosud.acv.common.exception.ProcesoCotizacionException;
import cl.cencosud.acv.common.exception.ValorizacionException;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida;
import cl.cencosud.acv.common.valorizacion.ValorizacionSeguro;
import cl.cencosud.asesorcotizador.interfaces.Cotizacion;
import cl.cencosud.asesorcotizador.interfaces.CotizacionHome;
import cl.cencosud.ventaseguros.common.Parametro;
import cl.cencosud.ventaseguros.comparador.model.Caracteristica;
import cl.cencosud.ventaseguros.interfaces.Comparador;
import cl.cencosud.ventaseguros.interfaces.ComparadorHome;
import cl.tinet.common.seguridad.model.Usuario;

import com.tinet.comun.jndi.ServiceLocator;
import com.tinet.exceptions.system.SystemException;

/**
 * Delegate para los servicios del m�dulo Cotizaci�n.
 * <br/>
 * @author miguelgarcia
 * @version 1.0
 * @created 30/09/2010
 */
public class CotizacionDelegate {

    private Cotizacion cotizacion;

    private CotizacionHome cotizacionHome; 
    
    private static final Log logger = LogFactory.getLog(CotizacionDelegate.class);

    /**
     * Constructor de la clase.
     */
    public CotizacionDelegate() {
        try {
            this.cotizacionHome =
                (CotizacionHome) ServiceLocator.singleton().getRemoteHome(
                    CotizacionHome.JNDI_NAME, CotizacionHome.class);

            this.cotizacion = this.cotizacionHome.create();
        } catch (Exception e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene los datos b�sicos de un Contratante.
     * @param rut rut del cliente
     * @param dv digito verificador.
     * @return datos del contratante.
     */
    public Contratante obtenerDatosContratante(long idSolicitud) {
        Contratante resultado = null;
        try {
            resultado = this.cotizacion.obtenerDatosContratante(idSolicitud);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return resultado;
    }

    /**
     * Metodo encargado de valorizar el producto seleccionado por el Cliente.
     * @param datosContratante datos del contratante.
     */
    public void cotizarProducto(HashMap < String, String > datosContratante) {
        try {
            this.cotizacion.cotizarProducto(datosContratante);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene los Tipos de Vehiculos disponibles.
     * @return listado de Tipos de Vehiculos.
     */
    public Vehiculo[] obtenerTiposVehiculos() {
        Vehiculo[] tiposVehiculos = new Vehiculo[] {};
        try {
            tiposVehiculos = this.cotizacion.obtenerTiposVehiculo();
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return tiposVehiculos;
    }
    
    /**
     * Obtiene los Tipos de Vehiculos disponibles por tipo de Rama.
     * @return listado de Tipos de Vehiculos por Tipo de Rama.
     */
    public Vehiculo[] obtenerTiposVehiculos(int idTipo) {
        Vehiculo[] tiposVehiculos = new Vehiculo[] {};
        try {
            tiposVehiculos = this.cotizacion.obtenerTiposVehiculo(idTipo);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
        return tiposVehiculos;
    }

    /**
     * Obtiene las Marcas de Vehiculos disponibles.
     * 
     * @return listado de Marcas de Vehiculos.
     */
    public Vehiculo[] obtenerMarcasVehiculos() {
        try {
            return this.cotizacion.obtenerMarcasVehiculos();
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene los Modelos de Vehiculo disponibles.
     * 
     * @param idTipo
     *            Codigo Tipo de Vehiculo.
     * @param idMarca
     *            Codigo Marca de Vehiculo.
     * @return Listado de Modelos de Vehiculos.
     */
    public Vehiculo[] obtenerModelosVehiculos(String idTipo, String idMarca) {
        try {
            return this.cotizacion.obtenerModelosVehiculo(idTipo, idMarca);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene los planes asociados a un producto.
     * @param idProducto id del producto
     * @param cotizacionSeguro datos de la cotizaci�n.
     * @return listado de planes.
     * @throws ValorizacionException 
     * @throws ProcesoCotizacionException 
     */
    public Plan[] obtenerPlanesCotizacion(String idProducto,
        CotizacionSeguro cotizacionSeguro, String idPlanPromocion)
        throws ProcesoCotizacionException, ValorizacionException {
        try {
            return this.cotizacion.obtenerPlanesCotizacion(idProducto,
                cotizacionSeguro, idPlanPromocion);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene listado de productos.
     * @param idSubcategoria identificador de subcategoria.
     * @param idRama identificador de rama.
     * @return listado de productos.
     * @throws ProcesoCotizacionException 
     */
    public Producto[] obtenerProductos(String idSubcategoria, String idRama,
        String idPlan) throws ProcesoCotizacionException {
        try {
            return this.cotizacion.obtenerProductos(idSubcategoria, idRama,
                idPlan);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene listado de coberturas.
     * @param idPlan identificador del plan.
     * @return listado de coberturas.
     */
    public Cobertura[] obtenerCoberturas(String idPlan) {
        try {
            return this.cotizacion.obtenerCoberturas(idPlan);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene listado de actividades.
     * @return listado de actividades.
     */
    public Actividad[] obtenerActividades() {
        try {
            return this.cotizacion.obtenerActividades();
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene listado de estados civiles.
     * @return listado de estados civiles.
     */
    public EstadoCivil[] obtenerEstadosCiviles() {
        try {
            return this.cotizacion.obtenerEstadosCiviles();
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene listado de regiones.
     * @return listado de regiones.
     */
    public ParametroCotizacion[] obtenerRegiones() {
        try {
            return this.cotizacion.obtenerRegiones();
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene listado de comunas.
     * @param idRegion identificador de region.
     * @return listado de comunas.
     */
    public ParametroCotizacion[] obtenerComunas(String idRegion) {
        try {
            return this.cotizacion.obtenerComunas(idRegion);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene listado de ciudades.
     * @param idComuna identificador de comuna.
     * @return listado de ciudades.
     */
    public ParametroCotizacion[] obtenerCiudades(String idComuna) {
        try {
            return this.cotizacion.obtenerCiudades(idComuna);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene la valorizacion para cotizaci�n de tipo Hogar.
     * @param primaHogar Datos de la cotizaci�n.
     * @return valorizaci�n de la cotizaci�n.
     */
    public ValorizacionSeguro obtenerValorizacionHogar(
        CotizacionSeguroHogar primaHogar) throws ValorizacionException {
        try {
            return this.cotizacion.obtenerValorizacionHogar(primaHogar);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene la valorizaci�n para cotizacion de tipo Vida.
     * @param cotizacionVida Datos de la cotizaci�n.
     * @return valorizaci�n de la cotizaci�n.
     */
    public ValorizacionSeguro obtenerValorizacionVida(
        CotizacionSeguroVida cotizacionVida) throws ValorizacionException {
        try {
            return this.cotizacion.obtenerValorizacionVida(cotizacionVida);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene la valorizaci�n para cotizaci�n de tipo Vehiculo.
     * @param cotizacionVehiculo datos de la cotizaci�n.
     * @return valorizaci�n de la cotizaci�n.
     * @throws ValorizacionException 
     */
    public ValorizacionSeguro obtenerValorizacionVehiculo(
        CotizacionSeguroVehiculo cotizacionVehiculo)
        throws ValorizacionException {
        try {
            return this.cotizacion
                .obtenerValorizacionVehiculo(cotizacionVehiculo);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene listado de parametros seg�n un grupo.
     * @param idGrupo identificador del grupo.
     * @return listado de parametros.
     */
    public List < Map < String, ? > > obtenerParametro(String idGrupo) {
        try {
            return this.cotizacion.obtenerParametro(idGrupo);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene un parametro seg�n el grupo y el parametro solicitado.
     * @param idGrupo identificador del grupo.
     * @return listado de parametros.
     */
    public Map < String, ? > obtenerParametro(String idGrupo, String idParametro) {
        try {
            return this.cotizacion.obtenerParametro(idGrupo, idParametro);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    public long ingresarCotizacionVehiculo(CotizacionSeguroVehiculo cotizacion,
        String idRama, String idProducto, Plan plan, String idVitrineo) {
        try {
            return this.cotizacion.ingresarCotizacionVehiculo(cotizacion,
                idRama, idProducto, plan, idVitrineo);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    public long ingresarCotizacionVida(CotizacionSeguroVida cotizacion,
        String idRama, String idProducto, Plan plan, String idVitrineo) {
        try {
            return this.cotizacion.ingresarCotizacionVida(cotizacion, idRama,
                idProducto, plan, idVitrineo);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    public long ingresarCotizacionHogar(CotizacionSeguroHogar cotizacion,
        String idRama, String idProducto, Plan plan, String idVitrineo) {
        try {
            return this.cotizacion.ingresarCotizacionHogar(cotizacion, idRama,
                idProducto, plan, idVitrineo);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    public int ingresarDatosAdicionalesVehiculo(long idSolicitud,
        CotizacionSeguroVehiculo cotizacion) {
        try {
        	logger.info("idSolicitud:" + idSolicitud);
        	logger.info("cotizacion:" + cotizacion);
            return this.cotizacion.ingresarDatosAdicionalesVehiculo(
                idSolicitud, cotizacion);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    public int ingresarDatosAdicionalesVida(long idSolicitud,
        CotizacionSeguroVida cotizacion) {
        try {
            return this.cotizacion.ingresarDatosAdicionalesVida(idSolicitud,
                cotizacion);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    public int ingresarDatosAdicionalesHogar(long idSolicitud,
        CotizacionSeguroHogar cotizacion) {
        try {
            return this.cotizacion.ingresarDatosAdicionalesHogar(idSolicitud,
                cotizacion);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    public ColorVehiculo[] obtenerColoresVehiculo() {
        try {
            return this.cotizacion.obtenerColoresVehiculo();
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    public RestriccionVida obtenerRestriccionVida(long idPlan) {
        try {
            return this.cotizacion.obtenerRestriccionVida(idPlan);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    public Rama obtenerRamaPorId(long idRama) {
        try {
            return this.cotizacion.obtenerRamaPorId(idRama);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    public Solicitud obtenerDatosSolicitud(long idSolicitud) {
        try {
            return this.cotizacion.obtenerDatosSolicitud(idSolicitud);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir m�todo obtenerDatosPlan.
     * @param idPlan
     * @return
     */
    public Plan obtenerDatosPlan(long idPlan) {
        try {
            return this.cotizacion.obtenerDatosPlan(idPlan);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir m�todo obtenerCotizacionHogarPDF.
     * @param cotHogar
     * @param solicitud
     */
    public void enviarCotizacionHogarPDF(CotizacionSeguroHogar cotHogar,
        Solicitud solicitud, Usuario usuario) {
        try {
            this.cotizacion.enviarCotizacionHogarPDF(cotHogar, solicitud,
                usuario);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir m�todo obtenerCotizacionVehiculoPDF.
     * @param vehiculo
     * @param solicitud
     */
    public void enviarCotizacionVehiculoPDF(CotizacionSeguroVehiculo vehiculo,
        Solicitud solicitud, Usuario usuario) {
        try {
            this.cotizacion.enviarCotizacionVehiculoPDF(vehiculo, solicitud,
                usuario);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir m�todo enviarCotizacionVidaPDF.
     * @param cotHogar
     * @param solicitud
     */
    public void enviarCotizacionVidaPDF(CotizacionSeguroVida cotVida,
        Solicitud solicitud, Usuario usuario) {
        try {
            this.cotizacion
                .enviarCotizacionVidaPDF(cotVida, solicitud, usuario);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    public boolean existePrimaUnica(long idPlan) {
        try {
            return this.cotizacion.existePrimaUnica(idPlan);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    public byte[] obtenerCotizacionVehiculoPDF(
        CotizacionSeguroVehiculo vehiculo, Solicitud solicitud, Usuario usuario) {
        try {
            return this.cotizacion.obtenerCotizacionVehiculoPDF(vehiculo,
                solicitud, usuario);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    public byte[] obtenerCotizacionHogarPDF(CotizacionSeguroHogar cotHogar,
        Solicitud solicitud, Usuario usuario) {
        try {
            return this.cotizacion.obtenerCotizacionHogarPDF(cotHogar,
                solicitud, usuario);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    public byte[] obtenerCotizacionVidaPDF(CotizacionSeguroVida cotVida,
        Solicitud solicitud, Usuario usuario) {
        try {
            return this.cotizacion.obtenerCotizacionVidaPDF(cotVida, solicitud,
                usuario);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    public long obtenerSubcategoriaAsociada(String idProducto, String idRama) {
        try {
            return this.cotizacion.obtenerSubcategoriaAsociada(idProducto,
                idRama);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    public CotizacionSeguroVida obtenerDatosMateriaVida(long idSolicitud) {
        try {
            return this.cotizacion.obtenerDatosMateriaVida(idSolicitud);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    public CotizacionSeguroVehiculo obtenerDatosMateriaVehiculo(long idSolicitud) {
        try {
            return this.cotizacion.obtenerDatosMateriaVehiculo(idSolicitud);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    public CotizacionSeguroHogar obtenerDatosMateriaHogar(long idSolicitud) {
        try {
            return this.cotizacion.obtenerDatosMateriaHogar(idSolicitud);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene una region por el identificador.
     * @param idRegion id.
     * @return Region solicitada.
     */
    public Map < String, Object > obtenerRegionPorId(String idRegion) {
        try {
            return this.cotizacion.obtenerRegionPorId(idRegion);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir m�todo obtenerComunaPorId.
     * @param idComuna
     * @return
     */
    public Map < String, Object > obtenerComunaPorId(String idComuna) {
        try {
            return this.cotizacion.obtenerComunaPorId(idComuna);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Ibtiene ciudad por el identificador.
     * @param idCiudad identificador de ciudad
     * @return Ciudad solicitada
     */
    public Map < String, Object > obtenerCiudadPorId(String idCiudad) {
        try {
            return this.cotizacion.obtenerCiudadPorId(idCiudad);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene subcategoria por el id.
     * @param idSubcategoria id de la subcategoria.
     * @return subcategoria solicitada.
     */
    public Subcategoria obtenerSubcategoriaPorId(long idSubcategoria) {
        try {
            return this.cotizacion.obtenerSubcategoriaPorId(idSubcategoria);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    public Parametro[] obtenerComunaPorCiudad(String idCiudad) {
        try {
            return this.cotizacion.obtenerComunaPorCiudad(idCiudad);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    public Parametro[] obtenerRegionPorComuna(String idComuna) {
        try {
            return this.cotizacion.obtenerRegionPorComuna(idComuna);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir m�todo actualizarEstadoSolicitud.
     * @param idSolicitud
     * @param idEstado
     */
    public void actualizarEstadoSolicitud(long idSolicitud, String idEstado) {
        try {
            this.cotizacion.actualizarEstadoSolicitud(idSolicitud, idEstado);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }
    
    /**
     * Obtiene las Marcas por tipo de Vehiculos disponibles.
     * 
     * @return listado de Marcas por tipo de Vehiculos.
     */
    public Vehiculo[] obtenerMarcasPorTipoVehiculos(String idTipo) {
        try {
            return this.cotizacion.obtenerMarcasPorTipoVehiculos(idTipo);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }
    
    /**
     * Obtiene una poliza html desde el WebService.
     * @param rutCliente
     * @param numeroSolicitud
     * @return
     */
    public byte[] obtenerPolizaHtml(int rutCliente, int numeroSolicitud){
        try {
            return this.cotizacion.obtenerPolizaHtml(rutCliente, numeroSolicitud);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }        
    }

    /**
     * Obtiene los planes asociados a un producto.
     * @param idProducto Identificador del producto.
     * @param idPlanPromocion Indetificador de plan en promocion.
     * @return Listado de planes asociados.
     * @throws ProcesoCotizacionException
     * @throws ValorizacionException
     */
    public Plan[] obtenerPlanes(String idProducto, String idPlanPromocion)
    throws ProcesoCotizacionException, ValorizacionException {
        try {
            return this.cotizacion.obtenerPlanes(idProducto, idPlanPromocion);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }        
    }
    
    /**
     * TODO Describir m�todo valorizarPlan.
     * @param idProducto
     * @param cotizacionSeguro
     * @param idPlan
     * @return
     * @throws ProcesoCotizacionException
     * @throws ValorizacionException
     */
    public Plan valorizarPlan(String idProducto,
        CotizacionSeguro cotizacionSeguro, String idPlan)
        throws ProcesoCotizacionException, ValorizacionException {
        try {
            return this.cotizacion.valorizarPlan(idProducto, cotizacionSeguro,
                idPlan);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }
    
	/**
	 * Obtiene los rut sin captcha
	 */
	public int obtenerRutSinCaptcha(long rutsincap) throws RemoteException {
		int resultado = 0; 
		try {
			resultado = this.cotizacion.obtenerRutSinCaptcha(rutsincap);
			return resultado;
		} catch (RemoteException e) {
			throw new SystemException(e);
		}
	}
    
        
    /**
     * TODO Describir m�todo valorizarPlan.
     * @param idProducto
     * @param cotizacionSeguro
     * @param idPlan
     * @return
     */
    public boolean isComparadorHabilitado(Long idProducto) {
    	ServiceLocator locator = ServiceLocator.singleton();
        ComparadorHome comparadorHome;
		try {
			comparadorHome = (ComparadorHome) locator.getRemoteHome(
           		 ComparadorHome.JNDI_NAME,
           		 ComparadorHome.class);
			Comparador comparador = comparadorHome.create();

	         return comparador.isProductoHabilitado(idProducto);
			
		} catch (RemoteException e) {
           logger.error("Error al obtener informaci�n si producto tiene habilitado comparador", e);
           throw new SystemException(e);
       } catch (CreateException e) {
           logger.error("Error al crear ejb remoto", e);
           throw new SystemException(e);
       } catch (Exception e) {
           logger.error("Error al obtener home remoto", e);
           throw new SystemException(e);
       }
    }
    
    /**
     * Metodo que obtiene las caracteristicas para el comparador, asociadas a un plan.
     * @param idPlan
     * @param idEstado
     * @return
     */
    public List<Caracteristica> getCaracteristicasComparadorPorPlan(Integer idPlan, Integer idEstado) {
    	ServiceLocator locator = ServiceLocator.singleton();
        ComparadorHome comparadorHome;
		try {
			comparadorHome = (ComparadorHome) locator.getRemoteHome(
           		 ComparadorHome.JNDI_NAME,
           		 ComparadorHome.class);
			Comparador comparador = comparadorHome.create();
	         return comparador.getCaracteristicasPorPlan(idPlan, idEstado);			
		} catch (RemoteException e) {
           logger.error("Error al obtener las caracteristicas por plan del comparador", e);
           throw new SystemException(e);
       } catch (CreateException e) {
           logger.error("Error al crear ejb remoto", e);
           throw new SystemException(e);
       } catch (Exception e) {
           logger.error("Error al obtener home remoto", e);
           throw new SystemException(e);
       }
    }
    
    /**
     * Metodo que obtiene las caracteristicas compartidas para el comparador.
     * @param idPlan
     * @param idEstado
     * @return
     */
    public List<Caracteristica> obtenerCaracteristicasCompartidas(Long idProducto, Integer idEstado) {
    	ServiceLocator locator = ServiceLocator.singleton();
        ComparadorHome comparadorHome;
		try {
			comparadorHome = (ComparadorHome) locator.getRemoteHome(
           		 ComparadorHome.JNDI_NAME,
           		 ComparadorHome.class);
			Comparador comparador = comparadorHome.create();
	        return comparador.obtenerCaracteristicasCompartidasFront(idProducto, idEstado);
		} catch (RemoteException e) {
           logger.error("Error al obtener las caracteristicas compartidas del comprador", e);
           throw new SystemException(e);
       } catch (CreateException e) {
           logger.error("Error al crear ejb remoto", e);
           throw new SystemException(e);
       } catch (Exception e) {
           logger.error("Error al obtener home remoto", e);
           throw new SystemException(e);
       }
	}
    
    public void guardarComparadorFront(long rutCliente, LinkedHashMap mapResultComparador) {
    	ServiceLocator locator = ServiceLocator.singleton();
        ComparadorHome comparadorHome;
		try {
			comparadorHome = (ComparadorHome) locator.getRemoteHome(
           		 ComparadorHome.JNDI_NAME,
           		 ComparadorHome.class);
			Comparador comparador = comparadorHome.create();
	        comparador.guardarComparador(rutCliente, mapResultComparador);
		} catch (RemoteException e) {
           logger.error("Error al obtener las caracteristicas por plan del comparador", e);
           throw new SystemException(e);
       } catch (CreateException e) {
           logger.error("Error al crear ejb remoto", e);
           throw new SystemException(e);
       } catch (Exception e) {
           logger.error("Error al obtener home remoto", e);
           throw new SystemException(e);
       }
    }

	public void enviarComparador(String mailCliente, String html) {
		ServiceLocator locator = ServiceLocator.singleton();
        ComparadorHome comparadorHome;
		try {
			comparadorHome = (ComparadorHome) locator.getRemoteHome(
           		 ComparadorHome.JNDI_NAME,
           		 ComparadorHome.class);
			Comparador comparador = comparadorHome.create();
	        comparador.enviarComparador(mailCliente, html);
		} catch (RemoteException e) {
           logger.error("Error al obtener las caracteristicas por plan del comparador", e);
           throw new SystemException(e);
       } catch (CreateException e) {
           logger.error("Error al crear ejb remoto", e);
           throw new SystemException(e);
       } catch (Exception e) {
           logger.error("Error al obtener home remoto", e);
           throw new SystemException(e);
       }
	}
	

	
	

	public List < HashMap < String, Object > > obtenerVehiculos(String rut) {
		
		try {
			logger.info("Rut en Delegate:" + rut);
			return this.cotizacion.obtenerVehiculos(rut);
			
		} catch (RemoteException e) {
			throw new SystemException(e);
		}
	}

	public Map<String, Object> obtenerVehiculosCompleto(String rut, String modelo) {
		try {
			return this.cotizacion.obtenerVehiculosCompleto(rut,modelo);
			
		} catch (RemoteException e) {
			throw new SystemException(e);
		}
	}

	public Map<String, Object> obtenerDatosVehiculosCompleto(String tipo,
			String marca, String modelo) {
		try {
			return this.cotizacion.obtenerDatosVehiculosCompleto(tipo,marca,modelo);
			
		} catch (RemoteException e) {
			throw new SystemException(e);
		}
	}
	
	public long grabarVitrineoGeneral(String pswd, String rutVitrineo,
			String dvVitrineo, String nombre, String apellidoPaterno,
			String apellidoMaterno, String tipoTelefono, String codTelefono,
			String telefono, String email, int region, int comuna,
			String diaNac, String mesNac, String anyoNac, String estadoCivil,
			String sexo, long edadConductor, int tipoVehiculo, int idMarcaVitrineo,
			int idModeloVitrineo, String idAnyoVitrineo, int numPuertas,
			String esDuenyo, String ciudadHogar, String direccionHogar,
			String numeroDireccionHogar, String numeroDeptoHogar,
			String anyoHogar, int rama, int subcategoria, int productoVitrineo,
			String rutDuenyo, String dvDuenyo, String nombreDuenyo,
			String apellidoPaternoDuenyo, String apellidoMaternoDuenyo,
			String diaNacimientoDuenyo, String mesNacimientoDuenyo,
			String anyoNacimientoDuenyo, String estadoCivilDuenyo,
			String regionResidenciaDuenyo, String comunaResidenciaDuenyo,
			String valorComercial) {
		try {
            return this.cotizacion.grabarVitrineoGeneral( pswd, rutVitrineo, dvVitrineo, nombre, apellidoPaterno, apellidoMaterno, tipoTelefono, codTelefono,
        			telefono, email, region, comuna, diaNac, mesNac, anyoNac, estadoCivil, sexo, edadConductor, tipoVehiculo, idMarcaVitrineo,
        			idModeloVitrineo, idAnyoVitrineo, numPuertas, esDuenyo, ciudadHogar, direccionHogar, numeroDireccionHogar, numeroDeptoHogar,
        			anyoHogar, rama, subcategoria, productoVitrineo, rutDuenyo, dvDuenyo, nombreDuenyo, apellidoPaternoDuenyo, apellidoMaternoDuenyo,
        			diaNacimientoDuenyo, mesNacimientoDuenyo, anyoNacimientoDuenyo, estadoCivilDuenyo, regionResidenciaDuenyo, comunaResidenciaDuenyo,
        			valorComercial);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
		
	}

	public Map<String, Object> obtenerDatosVitrineo(String claveVitrineo) {
		try {
            return this.cotizacion.obtenerDatosVitrineo( claveVitrineo);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
		
	}

	public Plan[] obtenerPlanesDeducibles(String idProducto2,
			String idProducto3, String idProducto4, String idProducto5,
			String idProducto6, String idProducto7, String idPlanPromocion) throws ProcesoCotizacionException, ValorizacionException {
		 try {
	            return this.cotizacion.obtenerPlanesDeducibles(idProducto2, idProducto3, idProducto4, idProducto5, idProducto6, idProducto7,idPlanPromocion);
	        } catch (RemoteException e) {
	            throw new SystemException(e);
	        } 
	}

	public Plan valorizarPlanDeducibles(String idProducto2, String idProducto3,
			String idProducto4, String idProducto5, String idProducto6,
			String idProducto7, CotizacionSeguro cotizacion, String idPlan) throws ProcesoCotizacionException, ValorizacionException {
        try {
            return this.cotizacion.valorizarPlanDeducibles(idProducto2, idProducto3, idProducto4, idProducto5,
            		idProducto6, idProducto7,cotizacion,
                idPlan);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
	}
	
    /**
     * Identifica si el cliente es preferente
     * @param rut del cliente
     * @return mensaje
     */
    public String consultarDescuento(int rut) {
        try {
            return this.cotizacion.consultarDescuento(rut);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }
    
    /**
     * Graba los datos de la mascota
     * @param rut y dv del cliente
     * @param nombre de la mascota
     * @param tipo de mascota
     * @param raza de la mascota
     * @param color de la mascota
     * @param edad de la mascota
     */
    public void guardarMascota(String rutDuenoMascota, String nombreMascota, String tipoMascota,
    		String razaMascota, String colorMascota, String edadMascota, String sexoMascota, String direccionMascota) {
        try {
        	this.cotizacion.guardarMascota(rutDuenoMascota, nombreMascota, tipoMascota,
        			razaMascota, colorMascota, edadMascota, sexoMascota, direccionMascota);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
    }
    
	public String obtenerIdFicha(int idSubcategoria) {
		try {
            return this.cotizacion.obtenerIdFicha(idSubcategoria);
        } catch (RemoteException e) {
            throw new SystemException(e);
        }
		
	}
	
	public CoberturaSeg[] consultarrCoberturas(int idPlan) {
        try {
        	logger.info("idPlan:" + idPlan);
            return this.cotizacion.consultarCoberturas(idPlan);
        } catch (RemoteException e) {
        	  logger.error("Error al obtener informaci�n de coberturas", e);
            throw new SystemException(e);
        }
    }
	
	public PatenteVehiculo obtenerDatosPatente(String idPatente){
		try {
        	logger.info("idPatente:" + idPatente);
            return this.cotizacion.obtenerDatosPatente(idPatente);
        } catch (RemoteException e) {
        	  logger.error("Error al obtener informaci�n de Patente", e);
            throw new SystemException(e);
        }
	}
	
	public long guardarDatosPatente(PatenteVehiculo patente){
		try {
        	logger.info("idPatente:" + patente.getPatente());
        	long var = this.cotizacion.guardarDatosPatente(patente);
        	System.out.println("***** "+var+" *****");
            return var;
        } catch (RemoteException e) {
        	  logger.error("Error al obtener informaci�n de Patente", e);
            throw new SystemException(e);
        }
	}
}
