package cl.cencosud.asesorcotizador.ejb;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.print.attribute.standard.DateTimeAtCompleted;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.cencosud.acv.common.Actividad;
import cl.cencosud.acv.common.Beneficiario;
import cl.cencosud.acv.common.Cobertura;
import cl.cencosud.acv.common.CoberturaSeg;
import cl.cencosud.acv.common.ColorVehiculo;
import cl.cencosud.acv.common.Contratante;
import cl.cencosud.acv.common.EstadoCivil;
import cl.cencosud.acv.common.ParametroCotizacion;
import cl.cencosud.acv.common.ParentescoAsegurado;
import cl.cencosud.acv.common.PatenteVehiculo;
import cl.cencosud.acv.common.Plan;
import cl.cencosud.acv.common.Producto;
import cl.cencosud.acv.common.Rama;
import cl.cencosud.acv.common.RestriccionVida;
import cl.cencosud.acv.common.Solicitud;
import cl.cencosud.acv.common.Subcategoria;
import cl.cencosud.acv.common.Vehiculo;
import cl.cencosud.acv.common.exception.ProcesoCotizacionException;
import cl.cencosud.acv.common.exception.ValorizacionException;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguro;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroHogar;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVehiculo;
import cl.cencosud.acv.common.valorizacion.CotizacionSeguroVida;
import cl.cencosud.acv.common.valorizacion.ValorizacionSeguro;
import cl.cencosud.asesorcotizador.dao.ACVDAOFactory;
import cl.cencosud.asesorcotizador.dao.ClientePreferenteDAO;
import cl.cencosud.asesorcotizador.dao.ConsultarCoberturasDAO;
import cl.cencosud.asesorcotizador.dao.CotizacionDAO;
import cl.cencosud.asesorcotizador.dao.CotizacionPDFDAO;
import cl.cencosud.asesorcotizador.dao.ObtenerCoberturasDAO;
import cl.cencosud.asesorcotizador.dao.PagosDAO;
import cl.cencosud.asesorcotizador.dao.RegistrarCotizacionDAO;
import cl.cencosud.asesorcotizador.dao.ValorizacionDAO;
import cl.cencosud.asesorcotizador.dao.ws.ConsultarCoberturaDAOWS;
import cl.cencosud.ventaseguros.common.Parametro;
import cl.cencosud.ventaseguros.interfaces.MantenedorClientes;
import cl.cencosud.ventaseguros.interfaces.MantenedorClientesHome;
import cl.tinet.common.mail.Mail;
import cl.tinet.common.seguridad.model.Usuario;

import com.tinet.comun.jndi.ServiceLocator;
import com.tinet.exceptions.system.SystemException;

/**
 * @author Miguel Garc�a H.(TInet Soluciones Inform�ticas).
 * @version 1.0
 * @created 26-Jul-2010 15:32:03
 * 
 * 
 * @ejb.bean name="Cotizacion"
 *           display-name="Cotizacion"
 *           description="Servicios relacionados al proceso de cotizaci�n."
 *           jndi-name="ejb/ventaseguros/Cotizacion"
 *           type="Stateless"
 *           view-type="remote"
 *
 * @ejb.transaction type="Supports"
 *
 * @ejb.resource-ref res-ref-name="jdbc/AsesorCotizadorDS" 
 *           res-type="javax.sql.DataSource"
 *           res-sharing-scope="Shareable"
 *           res-auth="Container"
 *           jndi-name="jdbc/AsesorCotizadorDS"
 *           
 * @ejb.resource-ref res-ref-name="jdbc/vehiculosDS" 
 *           res-type="javax.sql.DataSource"
 *           res-sharing-scope="Shareable"
 *           res-auth="Container"
 *           jndi-name="jdbc/vehiculosDS"
 * 
 * @ejb.ejb-service-ref name="service/WsBigsaCalculoPrimaHogar"
 *           interface="cl.cencosud.bigsa.cotizacion.webservice.client.was.WsBigsaCalculoPrimaHogar"
 *           jaxrpc-mapping-file="META-INF/WsBigsaCalculoPrimaHogarImplPort_mapping.xml"
 * 
 * @ejb.ejb-service-ref name="service/WsBigsaCalculoPrimaVehiculo"
 *           interface="cl.cencosud.bigsa.cotizacion.webservice.client.was.WsBigsaCalculoPrimaVehiculo"
 *           jaxrpc-mapping-file="META-INF/WsBigsaCalculoPrimaVehiculoImplPort_mapping.xml"
 * 
 * @ejb.ejb-service-ref name="service/WsBigsaCalculoPrimaVida"
 *           interface="cl.cencosud.bigsa.cotizacion.webservice.client.was.WsBigsaCalculoPrimaVida"
 *           jaxrpc-mapping-file="META-INF/WsBigsaCalculoPrimaVidaPort_mapping.xml"
 * 
 * @ejb.ejb-service-ref name="service/WsBigsaCotizacionPDF"
 *           interface="cl.cencosud.bigsa.cotizacion.consulta.webservice.client.was.WsBigsaCotizacionPDF"
 *           jaxrpc-mapping-file="META-INF/WsBigsaCotizacionPDFPort_mapping.xml"
 * 
 * @ejb.ejb-service-ref name="service/WsBigsaCargaCotizacion"
 *           interface="cl.cencosud.bigsa.cotizacion.grabar.webservice.client.was.WsBigsaCargaCotizacion"
 *           jaxrpc-mapping-file="META-INF/WsBigsaCargaCotizacionImplPort_mapping.xml"
 * 
 * @ejb.ejb-service-ref name="service/WsBigsaCoberturasHogar"
 *           interface="cl.cencosud.bigsa.coberturas.hogar.webservice.client.was.WsBigsaCoberturasHogar"
 *           jaxrpc-mapping-file="META-INF/WsBigsaCoberturasHogarImplPort_mapping.xml"
 * 
 * @ejb.ejb-service-ref name="service/WsBigsaCoberturasVehiculos"
 *           interface="cl.cencosud.bigsa.coberturas.vehiculo.webservice.client.was.WsBigsaCoberturasVehiculos"
 *           jaxrpc-mapping-file="META-INF/WsBigsaCoberturasVehiculosImplPort_mapping.xml"
 * 
 * @ejb.ejb-service-ref name="service/WsBigsaCoberturasVida"
 *           interface="cl.cencosud.bigsa.coberturas.vida.webservice.client.was.WsBigsaCoberturasVida"
 *           jaxrpc-mapping-file="META-INF/WsBigsaCoberturasVidaImplPort_mapping.xml"
 */
public class CotizacionBean implements SessionBean {

    /**
     * Identificador de la clase para serializaci�n.
     */
    private static final long serialVersionUID = 311503931615276761L;

    /**
     * Logger de la clase.
     */
    private static final Log logger = LogFactory.getLog(CotizacionBean.class);

    private static final Object TIPO_SEGURO_VEHICULO = "vehiculo";

    private SessionContext sessionContext;

    public void ejbActivate() throws EJBException, RemoteException {
        logger.debug("Activando instancia del EJB Cotizacion");

    }

    public void ejbPassivate() throws EJBException, RemoteException {
        logger.debug("Pasivando instancia del EJB Cotizacion");

    }

    public void ejbRemove() throws EJBException, RemoteException {
        logger.debug("Eliminando instancia del EJB Cotizacion");

    }

    public void setSessionContext(SessionContext context) throws EJBException,
        RemoteException {
        logger.debug("Estableciendo contexto de sesi�n.");
        this.sessionContext = context;

    }

    public void ejbCreate() throws CreateException {
        logger.debug("Creando instancia del EJB Cotizacion");
    }

    /**
     * Obtiene los datos b�sicos de un Contratante.
     * @ejb.interface-method "remote"
     */
    public Contratante obtenerDatosContratante(long idSolicitud) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            logger.debug("Consultar Datos Contratante al nivel DAO...");
            return dao.obtenerDatosContratante(idSolicitud);
        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }
    }
    
    

    /**
     * Obtiene los datos b�sicos de un Contratante.
     * @ejb.interface-method "remote"
     */
    public void cotizarProducto(HashMap < String, String > datosContratante) {

        logger.debug("Inicio Cotizacion Seguro EJB...");
        String tipoSeguro = datosContratante.get("tipoSeguro");

        if (tipoSeguro.equals(TIPO_SEGURO_VEHICULO)) {
            logger.debug("Cotizando Seguro Vehiculo...");
        }
    }

    /**
     * Obtiene los tipos de vehiculos.
     * @ejb.interface-method "remote"
     */
    public Vehiculo[] obtenerTiposVehiculo() {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {

            return dao.obtenerTiposVehiculos();
        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }

    }
    
    /**
     * Obtiene los tipos de vehiculos seg�n los tipos de Rama.
     * @ejb.interface-method "remote"
     */
    public Vehiculo[] obtenerTiposVehiculo( int idTipo ) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {

            return dao.obtenerTiposVehiculos( idTipo );
        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }

    }

    /**
     * Obtiene las Marcas de Vehiculos.
     * @ejb.interface-method "remote"
     */
    public Vehiculo[] obtenerMarcasVehiculos() {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            return dao.obtenerMarcasVehiculos();
        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene los Modelos de Vehiculos.
     * @ejb.interface-method "remote"
     */
    public Vehiculo[] obtenerModelosVehiculo(String idTipo, String idMarca) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            return dao.obtenerModelosVehiculos(idTipo, idMarca);
        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene los Planes para Cotizarn.
     * @throws ProcesoCotizacionException 
     * @throws ValorizacionException 
     * @ejb.interface-method "remote"
     */
    public Plan[] obtenerPlanes(String idProducto, String idPlanPromocion)
        throws ProcesoCotizacionException, ValorizacionException {

        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);

        try {
            logger.info("Obteniendo Planes");
            Plan[] planes = dao.obtenerPlanesCotizacion(idProducto, idPlanPromocion);
            
            if(planes == null || planes.length == 0) {
                logger.info("No existen planes asociados");
                logger.info("idProducto: " + idProducto);
                /*throw new ProcesoCotizacionException(
                ProcesoCotizacionException.PLAN_NO_ENCONTRADO,
                new Object[] {});*/
            }
            return planes;
        } finally {
            dao.close();
        }
    }
    
    /**
     * Obtiene la valorizacion de un plan.
     * @throws ProcesoCotizacionException 
     * @throws ValorizacionException 
     * @ejb.interface-method "remote"
     */
    public Plan valorizarPlan(String idProducto,
        CotizacionSeguro cotizacionSeguro, String idPlan)
        throws ProcesoCotizacionException, ValorizacionException {

        Plan planValorizado = new Plan();

        ValorizacionSeguro valorizacion = null;

        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        ObtenerCoberturasDAO daoCoberturas =
            factory.getVentaSegurosDAO(ObtenerCoberturasDAO.class);

        try {
            if (cotizacionSeguro instanceof CotizacionSeguroHogar) {
                logger.info("Valorizacion Tipo Hogar");
                CotizacionSeguroHogar cotizacionHogar =
                    (CotizacionSeguroHogar) cotizacionSeguro;

                // Obtiene codigo plan Bigsa.
                Plan[] plan = dao.obtenerPlanCotizacion(idProducto, idPlan);

                if (plan != null && plan.length > 0) {
                    cotizacionHogar.setCodigoPlan(Long.parseLong(plan[0]
                        .getIdPlanLegacy()));

                    planValorizado = plan[0];
                }
                
                // Seteo de datos legacy en la cotizacion.
                CotizacionSeguroHogar oCotizacion = new CotizacionSeguroHogar();
                oCotizacion.setCodigoPlan(cotizacionHogar.getCodigoPlan());
                oCotizacion.setRutCliente(cotizacionHogar.getRutCliente());
                
    			String numSubcategoria = oCotizacion.getNumSubcategoria() == null ? ""
    					: oCotizacion.getNumSubcategoria();
    			// Datos no necesarios para valorizacion de mascota
                if(!numSubcategoria.equals("140")){
                    oCotizacion.setMontoAseguradoContenido(cotizacionHogar
                            .getMontoAseguradoContenido());
                        oCotizacion.setMontoAseguradoEdificio(cotizacionHogar
                            .getMontoAseguradoEdificio());
                        oCotizacion.setMontoAseguradoRobo(cotizacionHogar
                            .getMontoAseguradoRobo());
                        oCotizacion.setComuna(this.obtenerComunaLeg(cotizacionHogar
                            .getComuna()));
                        oCotizacion.setAntiguedadVivienda(cotizacionHogar
                            .getAntiguedadVivienda());
                }
               
                //Inicio hogar vacaciones
                
                if( cotizacionHogar.getNumSubcategoria().equals("220")){	
             	
                	oCotizacion.setTramoDias(cotizacionHogar.getTramoDias());
           	
                }

                // Fin hogar vacaciones
                
                // Obtiene datos de Valorizaci�n por plan.
                try {
                	
                	
                    valorizacion = this.obtenerValorizacionHogar(oCotizacion);

                    if (valorizacion != null) {
                        planValorizado.setPrimaMensualUF(valorizacion
                            .getPrimaMensual());
                        planValorizado.setPrimaMensualPesos(valorizacion
                            .getPrimaMensualPesos());
                        planValorizado.setPrimaAnualPesos(valorizacion
                            .getPrimaAnualPesos());
                        planValorizado.setPrimaAnualUF(valorizacion
                            .getPrimaAnual());
                    }
                } catch (ValorizacionException ve) {
                    logger.error("Error al Valorizar Plan: "
                        + planValorizado.getIdPlan());
                    planValorizado.setPrimaMensualUF(0);
                    planValorizado.setPrimaMensualPesos(0);
                    planValorizado.setPrimaAnualPesos(0);
                    planValorizado.setPrimaAnualUF(0);
                }
                
                planValorizado.setCoberturas(new Cobertura[] { new Cobertura() });

             /*   try {
                    planValorizado.setCoberturas(daoCoberturas
                        .obtenerCoberturasHogar(oCotizacion));
                } catch (SystemException se) {
                    planValorizado
                        .setCoberturas(new Cobertura[] { new Cobertura() });
                } */

            } else if (cotizacionSeguro instanceof CotizacionSeguroVida) {
                logger.info("Valorizacion Tipo Vida");
                CotizacionSeguroVida cotizacionVida =
                    (CotizacionSeguroVida) cotizacionSeguro;

                // Obtiene codigo plan Bigsa.
                Plan[] plan = dao.obtenerPlanCotizacion(idProducto, idPlan);

                if (plan != null && plan.length > 0) {
                    cotizacionVida.setCodigoPlan(Long.parseLong(plan[0]
                        .getIdPlanLegacy()));
                    planValorizado = plan[0];
                }

                try {
                    valorizacion = this.obtenerValorizacionVida(cotizacionVida);

                    if (valorizacion != null) {
                        planValorizado.setPrimaMensualUF(valorizacion
                            .getPrimaMensual());
                        planValorizado.setPrimaMensualPesos(valorizacion
                            .getPrimaMensualPesos());
                        planValorizado.setPrimaAnualPesos(valorizacion
                            .getPrimaAnualPesos());
                        planValorizado.setPrimaAnualUF(valorizacion
                            .getPrimaAnual());
                    }
                } catch (ValorizacionException ve) {
                    logger.error("Error al Valorizar Plan: "
                        + planValorizado.getIdPlan());
                    planValorizado.setPrimaMensualUF(0);
                    planValorizado.setPrimaMensualPesos(0);
                    planValorizado.setPrimaAnualPesos(0);
                    planValorizado.setPrimaAnualUF(0);
                }
                
                planValorizado.setCoberturas(new Cobertura[] { new Cobertura() });

             /*   try {
                    planValorizado.setCoberturas(daoCoberturas
                        .obtenerCoberturasVida(cotizacionVida));
                } catch (SystemException se) {
                    planValorizado
                        .setCoberturas(new Cobertura[] { new Cobertura() });
                } */

            } else if (cotizacionSeguro instanceof CotizacionSeguroVehiculo) {
                logger.info("Valorizacion Tipo Vehiculo");
                HttpServletRequest request = null;
                CotizacionSeguroVehiculo cotizacionVehiculo = (CotizacionSeguroVehiculo) cotizacionSeguro;

                // Obtiene codigo plan bigsa.
                Plan[] plan = null;
                
                plan = dao.obtenerPlanCotizacion(idProducto, idPlan);
                if (plan != null && plan.length > 0) {
                    cotizacionVehiculo.setCodigoPlan(Long.parseLong(plan[0].getIdPlanLegacy()));
                    cotizacionVehiculo.setCodigoProducto(plan[0].getIdProducto());
                    planValorizado = plan[0];
                }

                // Obtiene codigos Legacy de vehiculo.

                String idTipo = String.valueOf(cotizacionVehiculo.getCodigoTipoVehiculo());
                String idMarca = String.valueOf(cotizacionVehiculo.getCodigoMarca());
                String idModelo = String.valueOf(cotizacionVehiculo.getCodigoModelo());

                Vehiculo vehiculo = dao.obtenerVehiculoLegacy(idTipo, idMarca, idModelo);

                CotizacionSeguroVehiculo nuevaCotizacionVehiculo = new CotizacionSeguroVehiculo();

                try {
                    BeanUtils.copyProperties(nuevaCotizacionVehiculo,
                        cotizacionVehiculo);

                    Contratante asegurado = new Contratante();
                    Contratante contratante = new Contratante();
                    BeanUtils.copyProperties(asegurado, cotizacionVehiculo.getAsegurado());
                    BeanUtils.copyProperties(contratante, cotizacionVehiculo.getContratante());

                    nuevaCotizacionVehiculo.setAsegurado(asegurado);
                    nuevaCotizacionVehiculo.setContratante(contratante);
                } catch (IllegalAccessException e) {
                    logger.error("Error ilegal al copiar propiedades", e);
                } catch (InvocationTargetException e) {
                    logger
                        .error("Error de invocacion al copiar propiedades", e);
                }

                if (vehiculo != null) {
                    nuevaCotizacionVehiculo.setCodigoMarca(Integer.parseInt(vehiculo.getIdMarcaVehiculo()));
                    nuevaCotizacionVehiculo.setCodigoTipoVehiculo(Integer.parseInt(vehiculo.getIdTipoVehiculo()));
                    nuevaCotizacionVehiculo.setCodigoModelo(Integer.parseInt(vehiculo.getIdModeloVehiculo()));
                } else {
                    throw new ProcesoCotizacionException(
                        ProcesoCotizacionException.VEHICULO_NO_ENCONTRADO,
                        new Object[] {});
                }

                // Obtiene datos de Valorizaci�n por plan.

                try {
                    valorizacion =
                        this
                            .obtenerValorizacionVehiculo(nuevaCotizacionVehiculo);

                    if (valorizacion != null) {

                        planValorizado.setPrimaMensualUF(valorizacion
                            .getPrimaMensual());
                        planValorizado.setPrimaMensualPesos(valorizacion
                            .getPrimaMensualPesos());
                        planValorizado.setPrimaAnualUF(valorizacion
                            .getPrimaAnual());
                        planValorizado.setPrimaAnualPesos(valorizacion
                            .getPrimaAnualPesos());
                        planValorizado.setIdProducto(valorizacion.getIdProducto());

                    } else {
                        throw new ProcesoCotizacionException(
                            ProcesoCotizacionException.ERROR_VALORIZACION,
                            new Object[] {});
                    }
                } catch (ValorizacionException ve) {
                    logger.error("Error al Valorizar Plan: "
                        + planValorizado.getIdPlan());
                    logger.error("Error al Valorizar Plan: "
                            + ve);
                    planValorizado.setPrimaMensualUF(0);
                    planValorizado.setPrimaMensualPesos(0);
                    planValorizado.setPrimaAnualUF(0);
                    planValorizado.setPrimaAnualPesos(0);
                }

                planValorizado.setCoberturas(new Cobertura[] { new Cobertura() });
               /* try {
                   planValorizado.setCoberturas(daoCoberturas.obtenerCoberturasVehiculo(nuevaCotizacionVehiculo));
                } catch (SystemException se) {
                    planValorizado.setCoberturas(new Cobertura[] { new Cobertura() });
                }*/
            }

        } finally {
            dao.close();
            daoCoberturas.close();
        }
        return planValorizado;
    }
    
    
    /**
     * Obtiene los Planes de una Cotizacion.
     * @throws ProcesoCotizacionException 
     * @throws ValorizacionException 
     * @ejb.interface-method "remote"
     * @deprecated @see {@link CotizacionBean#obtenerPlanes(String, String)} 
     * and {@link CotizacionBean#valorizarPlan(String, CotizacionSeguro, String)}}
     */
    public Plan[] obtenerPlanesCotizacion(String idProducto,
        CotizacionSeguro cotizacionSeguro, String idPlanPromocion)
        throws ProcesoCotizacionException, ValorizacionException {

        logger.info("En obtenerPlanes");
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        logger.info("En factory");
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        logger.info("En dao1");
        ObtenerCoberturasDAO daoCoberturas =
            factory.getVentaSegurosDAO(ObtenerCoberturasDAO.class);
        logger.info("En dao2");

        try {
            logger.debug("Obteniendo Planes por Producto...");
            Plan[] planes =
                dao.obtenerPlanesCotizacion(idProducto, idPlanPromocion);

            // Obtener Valorizacion por cada Plan.

            if (planes != null && planes.length > 0) {

                for (int i = 0; i < planes.length; i++) {

                    ValorizacionSeguro valorizacion = null;

                    if (cotizacionSeguro instanceof CotizacionSeguroHogar) {
                        logger.debug("Valorizacion Tipo Hogar...");
                        CotizacionSeguroHogar cotizacionHogar =
                            (CotizacionSeguroHogar) cotizacionSeguro;

                        // Obtiene codigo plan Bigsa.
                        String idPlan = String.valueOf(planes[i].getIdPlan());
                        Plan[] plan =
                            dao.obtenerPlanCotizacion(idProducto, idPlan);

                        if (plan != null && plan.length > 0) {
                            cotizacionHogar.setCodigoPlan(Long
                                .parseLong(plan[0].getIdPlanLegacy()));
                        }

                        // Seteo de datos legacy en la cotizacion.
                        CotizacionSeguroHogar oCotizacion =
                            new CotizacionSeguroHogar();
                        oCotizacion.setCodigoPlan(cotizacionHogar
                            .getCodigoPlan());
                        oCotizacion.setRutCliente(cotizacionHogar
                            .getRutCliente());
                        oCotizacion.setMontoAseguradoContenido(cotizacionHogar
                            .getMontoAseguradoContenido());
                        oCotizacion.setMontoAseguradoEdificio(cotizacionHogar
                            .getMontoAseguradoEdificio());
                        oCotizacion.setMontoAseguradoRobo(cotizacionHogar
                            .getMontoAseguradoRobo());
                        oCotizacion.setComuna(this
                            .obtenerComunaLeg(cotizacionHogar.getComuna()));
                        oCotizacion.setAntiguedadVivienda(cotizacionHogar.getAntiguedadVivienda());

                        // Obtiene datos de Valorizaci�n por plan.
                        try {
                            valorizacion =
                                this.obtenerValorizacionHogar(oCotizacion);

                            if (valorizacion != null) {
                                planes[i].setPrimaMensualUF(valorizacion
                                    .getPrimaMensual());
                                planes[i].setPrimaMensualPesos(valorizacion
                                    .getPrimaMensualPesos());
                                planes[i].setPrimaAnualPesos(valorizacion
                                    .getPrimaAnualPesos());
                                planes[i].setPrimaAnualUF(valorizacion
                                    .getPrimaAnual());
                            }
                        } catch (ValorizacionException ve) {
                            logger.error("Error al Valorizar Plan: "
                                + planes[i].getIdPlan());
                            planes[i].setPrimaMensualUF(0);
                            planes[i].setPrimaMensualPesos(0);
                            planes[i].setPrimaAnualPesos(0);
                            planes[i].setPrimaAnualUF(0);
                        }

                        try {
                            planes[i].setCoberturas(daoCoberturas
                                .obtenerCoberturasHogar(oCotizacion));
                        } catch (SystemException se) {
                            planes[i]
                                .setCoberturas(new Cobertura[] { new Cobertura() });
                        }

                    } else if (cotizacionSeguro instanceof CotizacionSeguroVida) {
                        logger.debug("Valorizacion Tipo Vida...");
                        CotizacionSeguroVida cotizacionVida =
                            (CotizacionSeguroVida) cotizacionSeguro;

                        // Obtiene codigo plan Bigsa.
                        String idPlan = String.valueOf(planes[i].getIdPlan());
                        Plan[] plan =
                            dao.obtenerPlanCotizacion(idProducto, idPlan);

                        if (plan != null && plan.length > 0) {
                            cotizacionVida.setCodigoPlan(Long.parseLong(plan[0]
                                .getIdPlanLegacy()));
                        }

                        try {
                            valorizacion =
                                this.obtenerValorizacionVida(cotizacionVida);

                            if (valorizacion != null) {
                                planes[i].setPrimaMensualUF(valorizacion
                                    .getPrimaMensual());
                                planes[i].setPrimaMensualPesos(valorizacion
                                    .getPrimaMensualPesos());
                                planes[i].setPrimaAnualPesos(valorizacion
                                    .getPrimaAnualPesos());
                                planes[i].setPrimaAnualUF(valorizacion
                                    .getPrimaAnual());
                            }
                        } catch (ValorizacionException ve) {
                            logger.error("Error al Valorizar Plan: "
                                + planes[i].getIdPlan());
                            planes[i].setPrimaMensualUF(0);
                            planes[i].setPrimaMensualPesos(0);
                            planes[i].setPrimaAnualPesos(0);
                            planes[i].setPrimaAnualUF(0);
                        }

                        try {
                            planes[i].setCoberturas(daoCoberturas
                                .obtenerCoberturasVida(cotizacionVida));
                        } catch (SystemException se) {
                            planes[i]
                                .setCoberturas(new Cobertura[] { new Cobertura() });
                        }

                    } else if (cotizacionSeguro instanceof CotizacionSeguroVehiculo) {
                        logger.debug("Valorizacion Tipo Vehiculo...");
                        CotizacionSeguroVehiculo cotizacionVehiculo =
                            (CotizacionSeguroVehiculo) cotizacionSeguro;

                        // Obtiene codigo plan bigsa.
                        String idPlan = String.valueOf(planes[i].getIdPlan());
                        Plan[] plan =
                            dao.obtenerPlanCotizacion(idProducto, idPlan);

                        if (plan != null && plan.length > 0) {
                            cotizacionVehiculo.setCodigoPlan(Long
                                .parseLong(plan[0].getIdPlanLegacy()));
                        }

                        // Obtiene codigos Legacy de vehiculo.

                        String idTipo =
                            String.valueOf(cotizacionVehiculo
                                .getCodigoTipoVehiculo());
                        String idMarca =
                            String.valueOf(cotizacionVehiculo.getCodigoMarca());
                        String idModelo =
                            String
                                .valueOf(cotizacionVehiculo.getCodigoModelo());

                        Vehiculo vehiculo =
                            dao
                                .obtenerVehiculoLegacy(idTipo, idMarca,
                                    idModelo);

                        CotizacionSeguroVehiculo nuevaCotizacionVehiculo =
                            new CotizacionSeguroVehiculo();

                        try {
                            BeanUtils.copyProperties(nuevaCotizacionVehiculo,
                                cotizacionVehiculo);

                            Contratante asegurado = new Contratante();
                            Contratante contratante = new Contratante();
                            BeanUtils.copyProperties(asegurado,
                                cotizacionVehiculo.getAsegurado());
                            BeanUtils.copyProperties(contratante,
                                cotizacionVehiculo.getContratante());

                            nuevaCotizacionVehiculo.setAsegurado(asegurado);
                            nuevaCotizacionVehiculo.setContratante(contratante);
                        } catch (IllegalAccessException e) {
                            logger.error("Error ilegal al copiar propiedades",
                                e);
                        } catch (InvocationTargetException e) {
                            logger.error(
                                "Error de invocacion al copiar propiedades", e);
                        }

                        if (vehiculo != null) {
                            nuevaCotizacionVehiculo.setCodigoMarca(Integer
                                .parseInt(vehiculo.getIdMarcaVehiculo()));
                            nuevaCotizacionVehiculo
                                .setCodigoTipoVehiculo(Integer
                                    .parseInt(vehiculo.getIdTipoVehiculo()));
                            nuevaCotizacionVehiculo.setCodigoModelo(Integer
                                .parseInt(vehiculo.getIdModeloVehiculo()));
                        } else {
                            throw new ProcesoCotizacionException(
                                ProcesoCotizacionException.VEHICULO_NO_ENCONTRADO,
                                new Object[] {});
                        }

                        if (nuevaCotizacionVehiculo.isClienteEsAsegurado()) {
                            Contratante contratante =
                                nuevaCotizacionVehiculo.getContratante();
                            String idComuna =
                                cotizacionVehiculo.getContratante()
                                    .getIdComuna();
                            String idRegion =
                                cotizacionVehiculo.getContratante()
                                    .getIdRegion();

                            contratante.setIdComuna(this
                                .obtenerComunaLeg(idComuna));

                            contratante.setIdRegion(this
                                .obtenerRegionLeg(idRegion));
                        } else {
                            Contratante asegurado =
                                nuevaCotizacionVehiculo.getAsegurado();
                            String idComuna =
                                cotizacionVehiculo.getAsegurado().getIdComuna();
                            String idRegion =
                                cotizacionVehiculo.getAsegurado().getIdRegion();
                            asegurado.setIdComuna(this
                                .obtenerComunaLeg(idComuna));
                            asegurado.setIdRegion(this
                                .obtenerRegionLeg(idRegion));
                        }

                        // Obtiene datos de Valorizaci�n por plan.

                        try {
                            valorizacion =
                                this
                                    .obtenerValorizacionVehiculo(nuevaCotizacionVehiculo);

                            if (valorizacion != null) {

                                planes[i].setPrimaMensualUF(valorizacion
                                    .getPrimaMensual());
                                planes[i].setPrimaMensualPesos(valorizacion
                                    .getPrimaMensualPesos());
                                planes[i].setPrimaAnualUF(valorizacion
                                    .getPrimaAnual());
                                planes[i].setPrimaAnualPesos(valorizacion
                                    .getPrimaAnualPesos());

                            } else {
                                throw new ProcesoCotizacionException(
                                    ProcesoCotizacionException.ERROR_VALORIZACION,
                                    new Object[] {});
                            }
                        } catch (ValorizacionException ve) {
                            logger.error("Error al Valorizar Plan: "
                                + planes[i].getIdPlan());
                            planes[i].setPrimaMensualUF(0);
                            planes[i].setPrimaMensualPesos(0);
                            planes[i].setPrimaAnualUF(0);
                            planes[i].setPrimaAnualPesos(0);
                        }

                        try {
                            planes[i]
                                .setCoberturas(daoCoberturas
                                    .obtenerCoberturasVehiculo(nuevaCotizacionVehiculo));
                        } catch (SystemException se) {
                            planes[i]
                                .setCoberturas(new Cobertura[] { new Cobertura() });
                        }

                    }

                }
            } else {
                logger.error("No Existen PLANES asociados.");
                logger.error("idProducto: " + idProducto);
                /*throw new ProcesoCotizacionException(
                    ProcesoCotizacionException.PLAN_NO_ENCONTRADO,
                    new Object[] {});*/
            }

            return planes;
        } finally {
            dao.close();
            daoCoberturas.close();
        }
    }

    /**
     * Obtiene los Productos de una Subcategoria.
     * @throws ProcesoCotizacionException 
     * @ejb.interface-method "remote"
     */
    public Producto[] obtenerProductos(String idSubcategoria, String idRama,
        String idPlan) throws ProcesoCotizacionException {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            Producto[] result =
                dao.obtenerProductos(idSubcategoria, idRama, idPlan);
            if (result == null || result.length == 0) {
                logger.error("No existen PRODUCTOS asociados.");
                logger.error("idRama: " + idRama);
                logger.error("idSubcategoria: " + idSubcategoria);
                /*throw new ProcesoCotizacionException(
                    ProcesoCotizacionException.PRODUCTO_NO_ENCONTRADO,
                    new Object[] {});*/
            }
            return result;
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene las Coberturas asociadas a un plan.
     * @ejb.interface-method "remote"
     */
    public Cobertura[] obtenerCoberturas(String idPlan) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            return dao.obtenerCoberturas(idPlan);
        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene listado de Actividades disponibles.
     * @ejb.interface-method "remote"
     */
    public Actividad[] obtenerActividades() {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            return dao.obtenerActividades();
        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }

    }

    /**
     * Obtiene listado de Estados Civiles disponibles.
     * @ejb.interface-method "remote"
     */
    public EstadoCivil[] obtenerEstadosCiviles() {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            return dao.obtenerEstadosCiviles();
        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }

    }

    /**
     * Obtiene listdo de Regiones dispobiles.
     * @ejb.interface-method "remote"
     */
    public ParametroCotizacion[] obtenerRegiones() {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            return dao.obtenerRegiones();
        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene listdo de Comunas dispobiles.
     * @ejb.interface-method "remote"
     */
    public ParametroCotizacion[] obtenerComunas(String idRegion) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            return dao.obtenerComunas(idRegion);
        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene listdo de Comunas dispobiles.
     * @ejb.interface-method "remote"
     */
    public ParametroCotizacion[] obtenerCiudades(String idComuna) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            return dao.obtenerCiudades(idComuna);
        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene Valorizacion para una Cotizacion de Seguro Hogar.
     * @ejb.interface-method "remote"
     */
    public ValorizacionSeguro obtenerValorizacionHogar(
        CotizacionSeguroHogar primaHogar) throws ValorizacionException {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        ValorizacionDAO dao = factory.getVentaSegurosDAO(ValorizacionDAO.class);
        try {
            ValorizacionSeguro result = dao.valorizarPlanHogar(primaHogar);
            return result;
        } finally {
            dao.close();
        }

    }

    /**
     * Obtiene Valorizacion para una Cotizacion de Seguro Hogar.
     * @throws ValorizacionException 
     * @ejb.interface-method "remote"
     */
    public ValorizacionSeguro obtenerValorizacionVehiculo(
        CotizacionSeguroVehiculo cotizacionVehiculo)
        throws ValorizacionException {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        ValorizacionDAO dao = factory.getVentaSegurosDAO(ValorizacionDAO.class);
        try {

            ValorizacionSeguro result =
                dao.valorizarPlanVehiculo(cotizacionVehiculo);
            return result;
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene Valorizacion para una Cotizacion de Seguro Hogar.
     * @ejb.interface-method "remote"
     */
    public ValorizacionSeguro obtenerValorizacionVida(
        CotizacionSeguroVida cotizacionVida) throws ValorizacionException {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        ValorizacionDAO dao = factory.getVentaSegurosDAO(ValorizacionDAO.class);
        try {
            ValorizacionSeguro result = dao.valorizarPlanVida(cotizacionVida);
            return result;
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene Parametros segrun grupo.
     * @ejb.interface-method "remote"
     */
    public List < Map < String, ? >> obtenerParametro(String idGrupo) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            List < Map < String, ? >> result = dao.obtenerParametro(idGrupo);
            return result;
        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene Parametros segrun grupo.
     * @ejb.interface-method "remote"
     */
    public Map < String, ? > obtenerParametro(String idGrupo, String idParametro) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            return dao.obtenerParametro(idGrupo, idParametro);
        } catch (Exception e) {
            logger.error(e);
            throw new SystemException(e);
        } finally {
            dao.close();
        }
    }

    /**
     * Almacena los datos de una cotizacion para PASO 1.
     * @param cotizacion
     * @ejb.interface-method "remote"
     */
    public long ingresarCotizacionVehiculo(CotizacionSeguroVehiculo cotizacion,
        String idRama, String idProducto, Plan plan, String idVitrineo) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {

            long rut = cotizacion.getRutCliente();
            int esAsegurado = cotizacion.isClienteEsAsegurado() ? 1 : 0;
            String estadoSolicitud = String.valueOf(dao.obtenerParametro("ESTADO_SOLICITUD", "EN_PASO_1").get("VALOR"));

            long idSolicitud = dao.agregarDatosSolicitud(plan, esAsegurado, idRama,idProducto, estadoSolicitud, null, rut, idVitrineo);
            logger.info("GUARDAR DATOS DEL CONTRATANTE-ASEGURADO ");
            dao.agregarDatosContratante(idSolicitud, cotizacion);
            dao.agregarDatosAsegurado(idSolicitud, cotizacion.getAsegurado());
            dao.agregarDatosMateriaVehiculo(cotizacion, idSolicitud);

            return idSolicitud;

        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }
    }

    /**
     * Almacena los datos de una cotizacion para PASO 1.
     * @param cotizacion
     * @ejb.interface-method "remote"
     */
    public long ingresarCotizacionVida(CotizacionSeguroVida cotizacion,
        String idRama, String idProducto, Plan plan, String idVitrineo) {

        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {

            long rut = cotizacion.getRutCliente();
            int esAsegurado = cotizacion.isClienteEsAsegurado() ? 0 : 1;
            String estadoSolicitud =
                String.valueOf(dao.obtenerParametro("ESTADO_SOLICITUD",
                    "EN_PASO_1").get("VALOR"));

            long idSolicitud =
                dao.agregarDatosSolicitud(plan, esAsegurado, idRama,
                    idProducto, estadoSolicitud, null, rut, idVitrineo);
            long idContratante =
                dao.agregarDatosContratante(idSolicitud, cotizacion);
            long idAsegurado =
                dao.agregarDatosAsegurado(idSolicitud, cotizacion
                    .getAsegurado());
            long idMateria =
                dao.agregarDatosMateriaVida(cotizacion, idSolicitud);

            logger.debug("Ingreso Cotizacion Vida");
            logger.debug("id solicitud: " + idSolicitud);
            logger.debug("id contratante: " + idContratante);
            logger.debug("id asegurado: " + idAsegurado);
            logger.debug("id materia vida: " + idMateria);
            return idSolicitud;

        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }
    }

    /**
     * Almacena los datos de una cotizacion para PASO 1.
     * @param cotizacion
     * @ejb.interface-method "remote"
     */
    public long ingresarCotizacionHogar(CotizacionSeguroHogar cotizacion,
        String idRama, String idProducto, Plan plan, String idVitrineo) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {

            long rut = cotizacion.getRutCliente();
            int esAsegurado = cotizacion.isClienteEsAsegurado() ? 1 : 0;
            String estadoSolicitud =
                String.valueOf(dao.obtenerParametro("ESTADO_SOLICITUD",
                    "EN_PASO_1").get("VALOR"));

            long idSolicitud =
                dao.agregarDatosSolicitud(plan, esAsegurado, idRama,
                    idProducto, estadoSolicitud, null, rut, idVitrineo);
            long idContratante =
                dao.agregarDatosContratante(idSolicitud, cotizacion);

            long idAsegurado = 0;
            if (cotizacion.isClienteEsAsegurado()) {
                idAsegurado =
                    dao.agregarDatosAsegurado(idSolicitud, cotizacion
                        .getContratante());
            } else {
                idAsegurado =
                    dao.agregarDatosAsegurado(idSolicitud, cotizacion
                        .getAsegurado());
            }

            long idMAteria =
                dao.agregarDatosMateriaHogar(cotizacion, idSolicitud);
            //long idMateria = dao.agregarDatosMateriaVida(cotizacion, idSolicitud);

            logger.debug("Ingreso Cotizacion Vida");
            logger.debug("id solicitud: " + idSolicitud);
            logger.debug("id contratante: " + idContratante);
            logger.debug("id asegurado: " + idAsegurado);
            //logger.debug("id materia vida: " + idMateria);
            return idSolicitud;

        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }
    }

    /**
     * Almacena los datos de una cotizacion para PASO 1.
     * @param cotizacion
     * @ejb.interface-method "remote"
     */
    public int ingresarDatosAdicionalesVehiculo(long idSolicitud,CotizacionSeguroVehiculo cotizacion) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);

        int resultNroBigsa = 0;
        try {

            String estadoSolicitud = String.valueOf(dao.obtenerParametro("ESTADO_SOLICITUD", "EN_PASO_2").get("VALOR"));
            dao.actualizarEstadoSolicitud(idSolicitud, estadoSolicitud);
            dao.actualizarContratante(idSolicitud, cotizacion);
            dao.actualizarMateriaVehiculo(idSolicitud, cotizacion);

            //Actualizar datos de asegurado.
            dao.actualizarAsegurado(idSolicitud, cotizacion);

            // Ingresar Factura.
            CotizacionSeguroVehiculo materia = dao.obtenerDatosMateriaVehiculo(idSolicitud);
            dao.ingresarFacturaVehiculo(materia.getIdMateriaVehiculo(),cotizacion.getFactura());

            // Registro Cotizacion en Bigsa.
            Solicitud solicitud = dao.obtenerDatosSolicitud(idSolicitud);

            Contratante contratante = dao.obtenerDatosContratante(solicitud.getId_solicitud());

            Contratante asegurado = dao.obtenerDatosAsegurado(solicitud.getId_solicitud());

            //INICIO REDUCCION DE CAMPOS TF
            contratante.setApellidoPaterno(cotizacion.getApellidoPaterno());
            contratante.setApellidoMaterno(cotizacion.getApellidoMaterno());
            contratante.setEmail(cotizacion.getEmail());
            if(cotizacion.isClienteEsAsegurado()){
            	asegurado.setApellidoPaterno(cotizacion.getApellidoPaterno());
            	asegurado.setApellidoMaterno(cotizacion.getApellidoMaterno());
            }
            //FIN REDUCCION DE CAMPOS TF
            
            contratante.setEstadoCivil(cotizacion.getContratante().getEstadoCivil());
            asegurado.setEstadoCivil(cotizacion.getAsegurado().getEstadoCivil());

            materia.setFecIniVig(cotizacion.getFecIniVig());
            materia.setFecTerVig(cotizacion.getFecTerVig());
            materia.setNumDiaMaxIniVig(cotizacion.getNumDiaMaxIniVig());
            materia.setEstadoCivil(cotizacion.getEstadoCivil());
            materia.setCodigoPlan(solicitud.getId_plan());
            if(cotizacion.getNumSubcategoria() != null){
            	materia.setNumSubcategoria(cotizacion.getNumSubcategoria());
            }
            
            //
            materia.setMontoAsegurado(solicitud.getPrima_anual_uf());
            materia.setAsegurado(asegurado);
            materia.setContratante(contratante);

            resultNroBigsa = this.registrarCotizacionVehiculo(materia, solicitud);

        } catch (Exception e) {
            logger.error("ERROR AL INGRESAR DATOS ADICIONALES VEHICULO " , e);
            throw new SystemException(e);
        } finally {
            dao.close();
        }
        return resultNroBigsa;
    }

    /**
     * Almacena los datos de una cotizacion para PASO 1.
     * @param cotizacion
     * @ejb.interface-method "remote"
     */
    public int ingresarDatosAdicionalesVida(long idSolicitud,
        CotizacionSeguroVida cotizacion) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {

            String estadoSolicitud =
                String.valueOf(dao.obtenerParametro("ESTADO_SOLICITUD",
                    "EN_PASO_2").get("VALOR"));
            dao.actualizarEstadoSolicitud(idSolicitud, estadoSolicitud);
            dao.actualizarContratante(idSolicitud, cotizacion);
            dao.actualizarMateriaVida(idSolicitud, cotizacion);

            CotizacionSeguroVida materiaVida = dao.obtenerDatosMateriaVida(idSolicitud);

            Beneficiario[] beneficiarios = cotizacion.getBeneficiarios();

            if (beneficiarios != null) {
                for (int i = 0; i < beneficiarios.length; i++) {
                    dao.agregarDatosVidaBeneficiario(materiaVida
                        .getIdMateriaVida(), beneficiarios[i]);
                }
            }

            Solicitud solicitud = this.obtenerDatosSolicitud(idSolicitud);
            cotizacion.setIdMateriaVida(materiaVida.getIdMateriaVida());
            return this.registrarCotizacionVida(cotizacion, solicitud);

        } catch (Exception e) {
            logger.error("ERROR VIDA " , e);
            throw new SystemException(e);
        } finally {
            dao.close();
        }
    }

    /**
     * Almacena los datos de una cotizacion para PASO 1.
     * @param cotizacion
     * @ejb.interface-method "remote"
     */
    public int ingresarDatosAdicionalesHogar(long idSolicitud,
        CotizacionSeguroHogar cotizacion) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);

        try {

            String estadoSolicitud =
                String.valueOf(dao.obtenerParametro("ESTADO_SOLICITUD",
                    "EN_PASO_2").get("VALOR"));

            dao.actualizarEstadoSolicitud(idSolicitud, estadoSolicitud);

            Solicitud solicitud = dao.obtenerDatosSolicitud(idSolicitud);

            dao.actualizarContratante(idSolicitud, cotizacion);

            if (cotizacion.isClienteEsAsegurado()) {
                dao.actualizarAsegurado(idSolicitud, cotizacion.getContratante());
            } else {
                dao.actualizarAsegurado(idSolicitud, cotizacion.getAsegurado());
            }

            dao.actualizarMateriaHogar(idSolicitud, cotizacion);

            CotizacionSeguroHogar cotHogar =
                dao.obtenerDatosMateriaHogar(idSolicitud);
            
            cotHogar.setRegion(obtenerRegionLeg(cotizacion.getRegion()));
            cotHogar.setComuna(obtenerComunaLeg(cotizacion.getComuna()));
            cotHogar.setCiudad(obtenerCiudadLeg(cotizacion.getCiudad()));

            cotHogar.setEstadoCivil(cotizacion.getContratante()
                .getEstadoCivil());
            cotHogar.setClienteEsAsegurado(cotizacion.isClienteEsAsegurado());
            
            // INICIO hogar vacaciones
            
            if( cotizacion.getNumSubcategoria().equals("220")){	
            	
            	 cotHogar.setFecIniVig(cotizacion.getFecIniVig());
                 cotHogar.setTramoDias(cotizacion.getTramoDias());
            	
            }

            
            //Fin Hogar Vacaciones
            
            
            cotHogar.setAntiguedadVivienda(cotizacion.getAntiguedadVivienda());
            
            // REGISTRO EN EL WEB SERVICE.
            return this.registrarCotizacionHogar(cotHogar, solicitud);

        } catch (Exception e) {
            e.printStackTrace();
            throw new SystemException(e);
        } finally {
            dao.close();
        }
    }

    /**
     * TODO Describir m�todo registrarCotizacionVehiculo.
     * @param vehiculo
     * @param solicitud
     * @return
     */
    public int registrarCotizacionVehiculo(CotizacionSeguroVehiculo vehiculo,
        Solicitud solicitud) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        RegistrarCotizacionDAO dao =
            factory.getVentaSegurosDAO(RegistrarCotizacionDAO.class);
        CotizacionDAO cotizacionDao =
            factory.getVentaSegurosDAO(CotizacionDAO.class);

        int resultNroBigsa = 0;
        try {
            //Obtener valores LEG
            Plan plan =
                cotizacionDao.obtenerDatosPlan(vehiculo.getCodigoPlan());
            vehiculo.setCodigoPlan(Long.parseLong(plan.getIdPlanLegacy()));

            //REGION, COMUNA y CIUDAD.
            Contratante contratante = vehiculo.getContratante();
            if (contratante != null) {
                String idRegion = contratante.getIdRegion();
                String idComuna = contratante.getIdComuna();
                String idCiudad = contratante.getIdCiudad();

                contratante.setIdRegion(obtenerRegionLeg(idRegion));
                contratante.setIdComuna(obtenerComunaLeg(idComuna));
                contratante.setIdCiudad(obtenerCiudadLeg(idCiudad));
            }

            Contratante asegurado = vehiculo.getAsegurado();
            if (asegurado != null) {
                String idRegion = asegurado.getIdRegion();
                String idComuna = asegurado.getIdComuna();
                String idCiudad = asegurado.getIdCiudad();

                asegurado.setIdRegion(obtenerRegionLeg(idRegion));
                asegurado.setIdComuna(obtenerComunaLeg(idComuna));
                asegurado.setIdCiudad(obtenerCiudadLeg(idCiudad));
            }

            String idTipo = String.valueOf(vehiculo.getCodigoTipoVehiculo());
            String idMarca = String.valueOf(vehiculo.getCodigoMarca());
            String idModelo = String.valueOf(vehiculo.getCodigoModelo());

            Vehiculo vehiculoLeg =
                cotizacionDao.obtenerVehiculoLegacy(idTipo, idMarca, idModelo);
            if (vehiculoLeg == null) {
                throw new ProcesoCotizacionException(
                    ProcesoCotizacionException.VEHICULO_NO_ENCONTRADO,
                    new Object[] {});
            }

            vehiculo.setCodigoMarca(Integer.valueOf(vehiculoLeg
                .getIdMarcaVehiculo()));
            vehiculo.setCodigoTipoVehiculo(Integer.valueOf(vehiculoLeg
                .getIdTipoVehiculo()));
            vehiculo.setCodigoModelo(Integer.valueOf(vehiculoLeg
                .getIdModeloVehiculo()));
            
            setColorVehiculoLeg(vehiculo);

            resultNroBigsa =
                dao.registrarCotizacionVehiculo(vehiculo, solicitud);
        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
            cotizacionDao.close();
        }
        return resultNroBigsa;
    }

    public int registrarCotizacionVida(CotizacionSeguroVida cotVida,
        Solicitud solicitud) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        RegistrarCotizacionDAO dao =
            factory.getVentaSegurosDAO(RegistrarCotizacionDAO.class);

        CotizacionDAO cotizacionDao =
            factory.getVentaSegurosDAO(CotizacionDAO.class);

        try {

            long codigoPlan = cotVida.getCodigoPlan();

            //Obtener valores LEG
            Plan plan = cotizacionDao.obtenerDatosPlan(cotVida.getCodigoPlan());
            cotVida.setCodigoPlan(Long.parseLong(plan.getIdPlanLegacy()));

            //REGION, COMUNA y CIUDAD.
            Contratante contratante = cotVida.getContratante();
            if (contratante != null) {
                String idRegion = contratante.getIdRegion();
                String idComuna = contratante.getIdComuna();
                String idCiudad = contratante.getIdCiudad();

                contratante.setIdRegion(obtenerRegionLeg(idRegion));
                contratante.setIdComuna(obtenerComunaLeg(idComuna));
                contratante.setIdCiudad(obtenerCiudadLeg(idCiudad));
            }

            Contratante asegurado = cotVida.getAsegurado();
            if (asegurado != null) {
                String idRegion = asegurado.getIdRegion();
                String idComuna = asegurado.getIdComuna();
                String idCiudad = asegurado.getIdCiudad();

                asegurado.setIdRegion(obtenerRegionLeg(idRegion));
                asegurado.setIdComuna(obtenerComunaLeg(idComuna));
                asegurado.setIdCiudad(obtenerCiudadLeg(idCiudad));
            }

            int codigoBigsa = dao.registrarCotizacionVida(cotVida, solicitud);

            cotVida.setCodigoPlan(codigoPlan);

            return codigoBigsa;

        } catch (Exception e) {
            logger.error("ERROR REGISTRAR COTIZACION ", e);
            throw new SystemException(e);
        } finally {
            dao.close();
            cotizacionDao.close();
        }
    }

    public int registrarCotizacionHogar(CotizacionSeguroHogar cotHogar,
        Solicitud solicitud) {

        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        RegistrarCotizacionDAO dao =
            factory.getVentaSegurosDAO(RegistrarCotizacionDAO.class);
        CotizacionDAO cotizacionDao =
            factory.getVentaSegurosDAO(CotizacionDAO.class);

        try {

            //Obtener valores LEG
            Plan plan = cotizacionDao.obtenerDatosPlan(solicitud.getId_plan());
            cotHogar.setCodigoPlan(Long.parseLong(plan.getIdPlanLegacy()));

            cotHogar.setContratante(cotizacionDao
                .obtenerDatosContratante(solicitud.getId_solicitud()));
            cotHogar.setAsegurado(cotizacionDao.obtenerDatosAsegurado(solicitud
                .getId_solicitud()));

            //REGION, COMUNA y CIUDAD.
            Contratante contratante = cotHogar.getContratante();
            if (contratante != null) {
                String idRegion = contratante.getIdRegion();
                String idComuna = contratante.getIdComuna();
                String idCiudad = contratante.getIdCiudad();

                contratante.setIdRegion(obtenerRegionLeg(idRegion));
                contratante.setIdComuna(obtenerComunaLeg(idComuna));
                contratante.setIdCiudad(obtenerCiudadLeg(idCiudad));
            }

            Contratante asegurado = cotHogar.getAsegurado();
            if (asegurado != null) {
                String idRegion = asegurado.getIdRegion();
                String idComuna = asegurado.getIdComuna();
                String idCiudad = asegurado.getIdCiudad();

                asegurado.setIdRegion(obtenerRegionLeg(idRegion));
                asegurado.setIdComuna(obtenerComunaLeg(idComuna));
                asegurado.setIdCiudad(obtenerCiudadLeg(idCiudad));
            }

            return dao.registrarCotizacionHogar(cotHogar, solicitud);

        } catch (Exception e) {
            e.printStackTrace();
            throw new SystemException(e);
        } finally {
            dao.close();
            cotizacionDao.close();
        }
    }

    /**
     * Obtiene los colores para vehiculo.
     * @param cotizacion
     * @ejb.interface-method "remote"
     */
    public ColorVehiculo[] obtenerColoresVehiculo() {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            return dao.obtenerColoresVehiculo();
        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene las restricciones de un plan.
     * @param cotizacion
     * @ejb.interface-method "remote"
     */
    public RestriccionVida obtenerRestriccionVida(long idPlan) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            RestriccionVida result = dao.obtenerRestriccionVida(idPlan);
            ParentescoAsegurado[] parentescos = null;
            if (result != null) {
                parentescos =
                    dao.obtenerParentescoRestriccion(result.getIdRestriccion());
                result.setParentescos(parentescos);
            }
            return result;

        } catch (Exception e) {
            throw new SystemException(e);
        } finally {
            dao.close();
        }
    }

    /**
     * TODO Describir m�todo obtenerRamaPorId.
     * @param idRama
     * @return
     * @ejb.interface-method "remote"
     */
    public Rama obtenerRamaPorId(long idRama) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            return dao.obtenerRamaPorId(idRama);

        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene los datos de una solicitud.
     * @param idRama
     * @return
     * @ejb.interface-method "remote"
     */
    public Solicitud obtenerDatosSolicitud(long idSolicitud) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            Solicitud solicitud = dao.obtenerDatosSolicitud(idSolicitud);

            if (solicitud.getId_rama().equals("1")) {
                CotizacionSeguroVehiculo vehiculo =
                    new CotizacionSeguroVehiculo();
                vehiculo = dao.obtenerDatosMateriaVehiculo(idSolicitud);
                solicitud.setDatosCotizacion(vehiculo);

            } else if (solicitud.getId_rama().equals("2")) {
                CotizacionSeguroHogar hogar = new CotizacionSeguroHogar();
                hogar = dao.obtenerDatosMateriaHogar(idSolicitud);
                solicitud.setDatosCotizacion(hogar);

            } else {
                CotizacionSeguro vida = new CotizacionSeguroVida();
                vida = dao.obtenerDatosMateriaVida(idSolicitud);
                solicitud.setDatosCotizacion(vida);
            }

            return solicitud;
        } finally {
            dao.close();
        }
    }

    /**
     * TODO Describir m�todo obtenerDatosPlan.
     * @param idPlan
     * @return
     * @ejb.interface-method "remote"
     */
    public Plan obtenerDatosPlan(long idPlan) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            return dao.obtenerDatosPlan(idPlan);
        } finally {
            dao.close();
        }
    }

    /**
     * TODO Describir m�todo obtenerCotizacionHogarPDF.
     * @param cotHogar
     * @param solicitud
     * @return
     * @ejb.interface-method "remote"
     */
    public void enviarCotizacionHogarPDF(CotizacionSeguroHogar cotHogar,
        Solicitud solicitud, Usuario usuario) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO cotizacionDao =
            factory.getVentaSegurosDAO(CotizacionDAO.class);

        CotizacionPDFDAO dao =
            factory.getVentaSegurosDAO(CotizacionPDFDAO.class);
        PagosDAO pagosDao = factory.getVentaSegurosDAO(PagosDAO.class);
        try {
            Contratante contratante =
                cotizacionDao.obtenerDatosContratante(solicitud
                    .getId_solicitud());
            Contratante asegurado =
                cotizacionDao
                    .obtenerDatosAsegurado(solicitud.getId_solicitud());

            Plan plan =
                cotizacionDao.obtenerDatosPlan(cotHogar.getCodigoPlan());

            cotHogar.setCodigoPlan(Long.parseLong(plan.getIdPlanLegacy()));

            cotHogar.setAsegurado(asegurado);
            cotHogar.setContratante(contratante);

            //TODO: Modificar datos de region, comuna, ciudad para entregar datos LEG.
            //Aunque no sirva de mucho en algunos pdf's.

            byte[] pdf = dao.obtenerCotizacionHogarPDF(cotHogar, solicitud);

            String mensaje = "";
            String email = cotHogar.getEmail();
            String nombre = cotHogar.getNombre();
            String server = pagosDao.obtenerDatosEmail("SERVER_CORREO");
            String from = pagosDao.obtenerDatosEmail("MAIL_FROM");
            String asunto =
                pagosDao.obtenerDatosEmail("MAIL_SUBJECT_COTIZACION");

            Mail mail = new Mail(from, email, asunto, "", server);
            try {

                String cuerpo = "";
                if (usuario != null) {
                    cuerpo = "HTML_COTIZACION_AUTENTIFICADO";
                } else {
                    cuerpo = "HTML_COTIZACION_NO_AUTENTIFICADO";
                }

                InputStream is = pagosDao.obtenerCuerpoEmail(cuerpo);

                String archivo = mail.convertStreamToString(is);

                //Reemplazar nombre y clave.
                mensaje = archivo.replace("{0}", nombre);

                mail.addContenido(mensaje);
                mail.addContenido(pdf, "application/pdf", "cotizacion.pdf");
                mail.sendMail();
            } catch (Exception e) {
                logger.error("error al enviar mail", e);
            }
        } finally {
            dao.close();
            cotizacionDao.close();
            pagosDao.close();
        }
    }

    /**
     * TODO Describir m�todo obtenerCotizacionVehiculoPDF.
     * @param vehiculo
     * @param solicitud
     * @return
     * @ejb.interface-method "remote"
     */
    public void enviarCotizacionVehiculoPDF(CotizacionSeguroVehiculo vehiculo,
        Solicitud solicitud, Usuario usuario) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO cotizacionDao =
            factory.getVentaSegurosDAO(CotizacionDAO.class);

        CotizacionPDFDAO dao =
            factory.getVentaSegurosDAO(CotizacionPDFDAO.class);

        PagosDAO pagosDao = factory.getVentaSegurosDAO(PagosDAO.class);
        try {
            Contratante contratante =
                cotizacionDao.obtenerDatosContratante(solicitud
                    .getId_solicitud());
            Contratante asegurado =
                cotizacionDao
                    .obtenerDatosAsegurado(solicitud.getId_solicitud());

            contratante.setEstadoCivil(vehiculo.getContratante()
                .getEstadoCivil());
            asegurado.setEstadoCivil(vehiculo.getAsegurado().getEstadoCivil());

            Plan plan =
                cotizacionDao.obtenerDatosPlan(vehiculo.getCodigoPlan());

            vehiculo.setCodigoPlan(Long.parseLong(plan.getIdPlanLegacy()));
            vehiculo.setAsegurado(asegurado);
            vehiculo.setContratante(contratante);

            int idTipo = vehiculo.getCodigoTipoVehiculo();
            int idMarca = vehiculo.getCodigoMarca();
            int idModelo = vehiculo.getCodigoModelo();

            Vehiculo vehiculoLeg =
                cotizacionDao.obtenerVehiculoLegacy(String.valueOf(idTipo),
                    String.valueOf(idMarca), String.valueOf(idModelo));

            vehiculo.setCodigoMarca(Integer.valueOf(vehiculoLeg
                .getIdMarcaVehiculo()));
            vehiculo.setCodigoTipoVehiculo(Integer.valueOf(vehiculoLeg
                .getIdTipoVehiculo()));
            vehiculo.setCodigoModelo(Integer.valueOf(vehiculoLeg
                .getIdModeloVehiculo()));
            vehiculo.setPatente("ET0000");
            
            setColorVehiculoLeg(vehiculo);


            byte[] pdf = dao.obtenerCotizacionVehiculoPDF(vehiculo, solicitud);

            String mensaje = "";
            String email = vehiculo.getEmail();
            String nombre = vehiculo.getNombre();
            String server = pagosDao.obtenerDatosEmail("SERVER_CORREO");
            String from = pagosDao.obtenerDatosEmail("MAIL_FROM");
            String asunto =
                pagosDao.obtenerDatosEmail("MAIL_SUBJECT_COTIZACION");

            Mail mail = new Mail(from, email, asunto, "", server);
            try {

                String cuerpo = "";
                if (usuario != null) {
                    cuerpo = "HTML_COTIZACION_AUTENTIFICADO";
                } else {
                    cuerpo = "HTML_COTIZACION_NO_AUTENTIFICADO";
                }

                logger.info("BUSCAR EMAIL: " + cuerpo);

                InputStream is = pagosDao.obtenerCuerpoEmail(cuerpo);

                String archivo = mail.convertStreamToString(is);
                logger.info(archivo);

                //Reemplazar nombre y clave.
                mensaje = archivo.replace("{0}", nombre);

                mail.addContenido(mensaje);
                mail.addContenido(pdf, "application/pdf", "cotizacion.pdf");
                mail.sendMail();
            } catch (Exception e) {
                logger.error("Error al enviar el e-mail", e);
            }

        } finally {
            dao.close();
            cotizacionDao.close();
            pagosDao.close();
        }
    }

    /**
     * TODO Describir m�todo enviarCotizacionVidaPDF.
     * @param cotHogar
     * @param solicitud
     * @return
     * @ejb.interface-method "remote"
     */
    public void enviarCotizacionVidaPDF(CotizacionSeguroVida cotVida,
        Solicitud solicitud, Usuario usuario) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO cotizacionDao =
            factory.getVentaSegurosDAO(CotizacionDAO.class);

        CotizacionPDFDAO dao =
            factory.getVentaSegurosDAO(CotizacionPDFDAO.class);
        PagosDAO pagosDao = factory.getVentaSegurosDAO(PagosDAO.class);
        try {
            Contratante contratante =
                cotizacionDao.obtenerDatosContratante(solicitud
                    .getId_solicitud());
            Contratante asegurado =
                cotizacionDao
                    .obtenerDatosAsegurado(solicitud.getId_solicitud());

            Plan plan = cotizacionDao.obtenerDatosPlan(cotVida.getCodigoPlan());

            cotVida.setCodigoPlan(Long.parseLong(plan.getIdPlanLegacy()));

            cotVida.setAsegurado(asegurado);
            cotVida.setContratante(contratante);

            byte[] pdf = dao.obtenerCotizacionVidaPDF(cotVida, solicitud);

            String mensaje = "";
            String email = cotVida.getEmail();
            String nombre = cotVida.getNombre();
            String server = pagosDao.obtenerDatosEmail("SERVER_CORREO");
            String from = pagosDao.obtenerDatosEmail("MAIL_FROM");
            String asunto =
                pagosDao.obtenerDatosEmail("MAIL_SUBJECT_COTIZACION");

            Mail mail = new Mail(from, email, asunto, "", server);
            try {

                String cuerpo = "";
                if (usuario != null) {
                    cuerpo = "HTML_COTIZACION_AUTENTIFICADO";
                } else {
                    cuerpo = "HTML_COTIZACION_NO_AUTENTIFICADO";
                }

                InputStream is = pagosDao.obtenerCuerpoEmail(cuerpo);

                String archivo = mail.convertStreamToString(is);

                //Reemplazar nombre y clave.
                mensaje = archivo.replace("{0}", nombre);

                mail.addContenido(mensaje);
                mail.addContenido(pdf, "application/pdf", "cotizacion.pdf");
                mail.sendMail();
            } catch (Exception e) {
                logger.error("Error al enviar mail.", e);
            }
        } finally {
            dao.close();
            cotizacionDao.close();
            pagosDao.close();
        }
    }

    /**
     * Identifica si tiene prima unica un determinado plan.
     * @param idRama
     * @return
     * @ejb.interface-method "remote"
     */
    public boolean existePrimaUnica(long idPlan) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            return dao.existePrimaUnica(idPlan);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene el pdf de una cotizacion de vehiculo.
     * @param vehiculo Cotizacion de tipo vehiulo.
     * @param solicutd solicitud asociada.
     * @return
     * @ejb.interface-method "remote"
     */
    public byte[] obtenerCotizacionVehiculoPDF(
        CotizacionSeguroVehiculo vehiculo, Solicitud solicitud, Usuario usuario) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionPDFDAO dao =
            factory.getVentaSegurosDAO(CotizacionPDFDAO.class);
        CotizacionDAO cotizacionDao =
            factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {

            Contratante contratante =
                cotizacionDao.obtenerDatosContratante(solicitud
                    .getId_solicitud());
            Contratante asegurado =
                cotizacionDao
                    .obtenerDatosAsegurado(solicitud.getId_solicitud());

            contratante.setEstadoCivil(vehiculo.getContratante()
                .getEstadoCivil());
            asegurado.setEstadoCivil(vehiculo.getAsegurado().getEstadoCivil());

            Plan plan =
                cotizacionDao.obtenerDatosPlan(vehiculo.getCodigoPlan());

            vehiculo.setCodigoPlan(Long.parseLong(plan.getIdPlanLegacy()));
            vehiculo.setAsegurado(asegurado);
            vehiculo.setContratante(contratante);

            int idTipo = vehiculo.getCodigoTipoVehiculo();
            int idMarca = vehiculo.getCodigoMarca();
            int idModelo = vehiculo.getCodigoModelo();

            Vehiculo vehiculoLeg =
                cotizacionDao.obtenerVehiculoLegacy(String.valueOf(idTipo),
                    String.valueOf(idMarca), String.valueOf(idModelo));

            vehiculo.setCodigoMarca(Integer.valueOf(vehiculoLeg
                .getIdMarcaVehiculo()));
            vehiculo.setCodigoTipoVehiculo(Integer.valueOf(vehiculoLeg
                .getIdTipoVehiculo()));
            vehiculo.setCodigoModelo(Integer.valueOf(vehiculoLeg
                .getIdModeloVehiculo()));
            vehiculo.setPatente("ET0000");
            
            setColorVehiculoLeg(vehiculo);

            return dao.obtenerCotizacionVehiculoPDF(vehiculo, solicitud);
        } finally {
            dao.close();
            cotizacionDao.close();
        }
    }

    /**
     * TODO Describir m�todo setColorVehiculoLeg.
     * @param vehiculo
     */
    private void setColorVehiculoLeg(CotizacionSeguroVehiculo vehiculo) {
        //Color
        ColorVehiculo[] colores = this.obtenerColoresVehiculo();
        String idColorLeg = "-1";
        for (int i = 0; i < colores.length; i++) {
            if(colores[i].getIdColor() == vehiculo.getIdColor()) {
                idColorLeg = colores[i].getIdColorLeg();
            }
        }
        vehiculo.setIdColor(Long.parseLong(idColorLeg));
    }

    /**
     * Obtiene el pdf de una cotizacion de hogar.
     * @param vehiculo Cotizacion de tipo vehiulo.
     * @param solicutd solicitud asociada.
     * @return
     * @ejb.interface-method "remote"
     */
    public byte[] obtenerCotizacionHogarPDF(CotizacionSeguroHogar cotHogar,
        Solicitud solicitud, Usuario usuario) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionPDFDAO dao =
            factory.getVentaSegurosDAO(CotizacionPDFDAO.class);
        CotizacionDAO cotizacionDao =
            factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            Contratante contratante =
                cotizacionDao.obtenerDatosContratante(solicitud
                    .getId_solicitud());
            Contratante asegurado =
                cotizacionDao
                    .obtenerDatosAsegurado(solicitud.getId_solicitud());

            Plan plan =
                cotizacionDao.obtenerDatosPlan(cotHogar.getCodigoPlan());

            cotHogar.setCodigoPlan(Long.parseLong(plan.getIdPlanLegacy()));

            cotHogar.setAsegurado(asegurado);
            cotHogar.setContratante(contratante);

            //TODO: Modificar datos de region, comuna, ciudad para entregar datos LEG.
            //Aunque no sirva de mucho en algunos pdf's.

            return dao.obtenerCotizacionHogarPDF(cotHogar, solicitud);
        } finally {
            dao.close();
            cotizacionDao.close();
        }
    }

    /**
     * Obtiene el pdf de una cotizacion de vida.
     * @param vehiculo Cotizacion de tipo vehiulo.
     * @param solicutd solicitud asociada.
     * @return
     * @ejb.interface-method "remote"
     */
    public byte[] obtenerCotizacionVidaPDF(CotizacionSeguroVida cotVida,
        Solicitud solicitud, Usuario usuario) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionPDFDAO dao =
            factory.getVentaSegurosDAO(CotizacionPDFDAO.class);
        CotizacionDAO cotizacionDao =
            factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            Contratante contratante =
                cotizacionDao.obtenerDatosContratante(solicitud
                    .getId_solicitud());
            Contratante asegurado =
                cotizacionDao
                    .obtenerDatosAsegurado(solicitud.getId_solicitud());

            Plan plan = cotizacionDao.obtenerDatosPlan(cotVida.getCodigoPlan());

            cotVida.setCodigoPlan(Long.parseLong(plan.getIdPlanLegacy()));

            cotVida.setAsegurado(asegurado);
            cotVida.setContratante(contratante);

            return dao.obtenerCotizacionVidaPDF(cotVida, solicitud);
        } finally {
            dao.close();
            cotizacionDao.close();
        }
    }

    /**
     * TODO Describir m�todo obtenerRegionLeg.
     * @param idRegion
     * @return
     */
    private String obtenerRegionLeg(String idRegion) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        String codigoRegionLeg = "";
        try {
            Map < String, Object > region = dao.obtenerRegionPorId(idRegion);
            codigoRegionLeg = (String) region.get("CODIGO_REGION_LEG");
        } finally {
            dao.close();
        }
        return codigoRegionLeg;
    }

    /**
     * TODO Describir m�todo obtenerRegionPorId.
     * @param idRegion
     * @return
     * @ejb.interface-method "remote"
     */
    public Map < String, Object > obtenerRegionPorId(String idRegion) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            return dao.obtenerRegionPorId(idRegion);
        } finally {
            dao.close();
        }
    }

    /**
     * TODO Describir m�todo obtenerComunaLeg.
     * @param idComuna
     * @return
     */
    private String obtenerComunaLeg(String idComuna) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        String codigoComunaLeg = "";
        try {
            Map < String, Object > region = dao.obtenerComunaPorId(idComuna);
            codigoComunaLeg = (String) region.get("CODIGO_COMUNA_LEG");
        } finally {
            dao.close();
        }
        return codigoComunaLeg;
    }

    /**
     * TODO Describir m�todo obtenerComunaPorId.
     * @param idComuna
     * @return
     * @ejb.interface-method "remote"
     */
    public Map < String, Object > obtenerComunaPorId(String idComuna) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            return dao.obtenerComunaPorId(idComuna);
        } finally {
            dao.close();
        }
    }

    /**
     * TODO Describir m�todo obtenerCiudadLeg.
     * @param idCiudad
     * @return
     */
    private String obtenerCiudadLeg(String idCiudad) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        String codigoCiudadLeg = "";
        try {
            Map < String, Object > ciudad = dao.obtenerCiudadPorId(idCiudad);
            codigoCiudadLeg = (String) ciudad.get("CODIGO_CIUDAD_LEG");
        } finally {
            dao.close();
        }
        return codigoCiudadLeg;
    }

    /**
     * TODO Describir m�todo obtenerCiudadPorId.
     * @param idCiudad
     * @return
     * @ejb.interface-method "remote"
     */
    public Map < String, Object > obtenerCiudadPorId(String idCiudad) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            return dao.obtenerCiudadPorId(idCiudad);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene el id de una subcategoria seg�n producto y rama.
     * @param idProducto producto asociado.
     * @param idRama rama asociada.
     * @return
     * @ejb.interface-method "remote"
     */
    public long obtenerSubcategoriaAsociada(String idProducto, String idRama) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            return dao.obtenerSubcategoriaAsociada(idProducto, idRama);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene Datos de vehiculo asociado a una solicitud.
     * @param idSolicitud identificador de solicitud.
     * @return
     * @ejb.interface-method "remote"
     */
    public CotizacionSeguroVehiculo obtenerDatosMateriaVehiculo(long idSolicitud) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            return dao.obtenerDatosMateriaVehiculo(idSolicitud);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene Datos de hogar asociado a una solicitud.
     * @param idSolicitud identificador de solicitud.
     * @return
     * @ejb.interface-method "remote"
     */
    public CotizacionSeguroHogar obtenerDatosMateriaHogar(long idSolicitud) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            return dao.obtenerDatosMateriaHogar(idSolicitud);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene Datos de vida asociado a una solicitud.
     * @param idSolicitud identificador de solicitud.
     * @return
     * @ejb.interface-method "remote"
     */
    public CotizacionSeguroVida obtenerDatosMateriaVida(long idSolicitud) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            return dao.obtenerDatosMateriaVida(idSolicitud);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene Subcategoria por el ID.
     * @param idSolicitud identificador de solicitud.
     * @return
     * @ejb.interface-method "remote"
     */
    public Subcategoria obtenerSubcategoriaPorId(long idSubcategoria) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            return dao.obtenerSubcategoriaPorId(idSubcategoria);
        } finally {
            dao.close();
        }
    }

    /**
     * Obtiene una comuna con un id de ciudad.
     * @param idSolicitud identificador de solicitud.
     * @return
     * @ejb.interface-method "remote"
     */
   public Parametro[] obtenerComunaPorCiudad(String idCiudad)
        throws SystemException {
        try {
            ServiceLocator locator = ServiceLocator.singleton();
            MantenedorClientesHome clientesHome;
            clientesHome =
                (MantenedorClientesHome) locator.getRemoteHome(
                    MantenedorClientesHome.JNDI_NAME,
                    MantenedorClientesHome.class);

            MantenedorClientes mantenedorClientes = clientesHome.create();
            return mantenedorClientes.obtenerComunaPorCiudad(idCiudad);

        } catch (Exception e) {
            logger.debug("Error al obtener comuna por ciudad.");
            throw new SystemException(e);
        }
    }

    /**
     * Obtiene una region con id de comuna.
     * @param idSolicitud identificador de solicitud.
     * @return
     * @ejb.interface-method "remote"
     */
   public Parametro[] obtenerRegionPorComuna(String idComuna)
        throws SystemException {
        try {
            ServiceLocator locator = ServiceLocator.singleton();
            MantenedorClientesHome clientesHome;
            clientesHome =
                (MantenedorClientesHome) locator.getRemoteHome(
                    MantenedorClientesHome.JNDI_NAME,
                    MantenedorClientesHome.class);

            MantenedorClientes mantenedorClientes = clientesHome.create();
            return mantenedorClientes.obtenerRegionPorComuna(idComuna);

        } catch (Exception e) {
            logger.debug("Error al obtener comuna por ciudad.");
            throw new SystemException(e);
        }
    }

    /**
     * TODO Describir m�todo actualizarEstadoSolicitud.
     * @param idSolicitud
     * @param idEstado
     * @ejb.interface-method "remote"
     */
    public void actualizarEstadoSolicitud(long idSolicitud, String idEstado) {
        ACVDAOFactory factory = ACVDAOFactory.getInstance();
        CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
        try {
            String estadoSolicitud =
                String.valueOf(dao.obtenerParametro("ESTADO_SOLICITUD",
                    idEstado).get("VALOR"));
            dao.actualizarEstadoSolicitud(idSolicitud, estadoSolicitud);
        } finally {
            dao.close();
        }
    }
    
     /**
      * Obtiene las Marcas por tipo de Vehiculos.
      * @ejb.interface-method "remote"
      */
     public Vehiculo[] obtenerMarcasPorTipoVehiculos(String idTipo) {
         ACVDAOFactory factory = ACVDAOFactory.getInstance();
         CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
         try {
             return dao.obtenerMarcasPorTipoVehiculos(idTipo);
         } catch (Exception e) {
             throw new SystemException(e);
         } finally {
             dao.close();
         }
     }

     
     
     /**
      * TODO Describir m�todo obtenerPolizaHtml.
      * @param rutCliente
      * @param numeroSolicitud
      * @return
      * @ejb.interface-method "remote"
      */
     public byte[] obtenerPolizaHtml(int rutCliente, int numeroSolicitud) {
         try {
             ServiceLocator locator = ServiceLocator.singleton();
             MantenedorClientesHome clientesHome =
                 (MantenedorClientesHome) locator.getRemoteHome(
                     MantenedorClientesHome.JNDI_NAME,
                     MantenedorClientesHome.class);
             MantenedorClientes mantenedorClientes = clientesHome.create();

             return mantenedorClientes.obtenerPolizaHtml(rutCliente, numeroSolicitud);
         } catch (RemoteException e) {
             logger.error("Error al obtener poliza html", e);
             throw new SystemException(e);
         } catch (CreateException e) {
             logger.error("Error al crear ejb remoto", e);
             throw new SystemException(e);
         } catch (Exception e) {
             logger.error("Error al obtener home remoto", e);
             throw new SystemException(e);
         }
     }
     
     /**
      * TODO Describir m�todo obtenerRutSinCaptcha.
      * @param rutsincap
      * @return
      * 
      * @ejb.interface-method "remote"
      */     
     public int obtenerRutSinCaptcha(long rutsincap) {
         ACVDAOFactory factory = ACVDAOFactory.getInstance();
         CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
         try {
             logger.debug("Consultar Rut sin Captcha..."); 
             return dao.obtenerRutSinCaptcha(rutsincap);
         } catch (Exception e) {
             throw new SystemException(e);
         } finally {
             dao.close();
         }
     }
     /**
      * TODO Describir m�todo grabarVitrineoGeneral.
      * @param pswd
	  * @param rutVitrineo
	  * @param dvVitrineo
	  * @param nombre
	  * @param apellidoPaterno
	  * @param apellidoMaterno
	  * @param tipoTelefono
	  * @param codTelefono
	  * @param telefono
	  * @param email
	  * @param region
	  * @param comuna
	  * @param diaNac
	  * @param mesNac
	  * @param anyoNac
	  * @param estadoCivil
	  * @param sexo
	  * @param tipoVehiculo
	  * @param idMarcaVitrineo
	  * @param idModeloVitrineo
	  * @param idAnyoVitrineo
	  * @param numPuertas
	  * @param esDuenyo
	  * @param ciudadHogar
	  * @param direccionHogar
	  * @param numeroDireccionHogar
	  * @param numeroDeptoHogar
	  * @param anyoHogar
	  * @param rama
	  * @param subcategoria
	  * @param productoVitrineo
	  * @param rutDuenyo
	  * @param dvDuenyo
	  * @param nombreDuenyo
	  * @param apellidoPaternoDuenyo
	  * @param apellidoMaternoDuenyo
	  * @param diaNacimientoDuenyo
	  * @param mesNacimientoDuenyo
	  * @param anyoNacimientoDuenyo
	  * @param estadoCivilDuenyo
	  * @param regionResidenciaDuenyo
	  * @param comunaResidenciaDuenyo
	  * @param valorComercial      
      * @ejb.interface-method "remote"
      */   
     public long grabarVitrineoGeneral(String pswd, String rutVitrineo,
			String dvVitrineo, String nombre, String apellidoPaterno,
			String apellidoMaterno, String tipoTelefono, String codTelefono,
			String telefono, String email, int region, int comuna,
			String diaNac, String mesNac, String anyoNac, String estadoCivil,
			String sexo, long edadConductor, int tipoVehiculo, int idMarcaVitrineo,
			int idModeloVitrineo, String idAnyoVitrineo, int numPuertas,
			String esDuenyo, String ciudadHogar, String direccionHogar,
			String numeroDireccionHogar, String numeroDeptoHogar,
			String anyoHogar, int rama, int subcategoria, int productoVitrineo,
			String rutDuenyo, String dvDuenyo, String nombreDuenyo,
			String apellidoPaternoDuenyo, String apellidoMaternoDuenyo,
			String diaNacimientoDuenyo, String mesNacimientoDuenyo,
			String anyoNacimientoDuenyo, String estadoCivilDuenyo,
			String regionResidenciaDuenyo, String comunaResidenciaDuenyo,
			String valorComercial) {
         ACVDAOFactory factory = ACVDAOFactory.getInstance();
         CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
         try {
             logger.debug("Grabando datos de vitrineo...");
             return dao.grabarVitrineoGeneral(pswd, rutVitrineo, dvVitrineo, nombre, apellidoPaterno, apellidoMaterno, tipoTelefono, codTelefono,
        			telefono, email, region, comuna, diaNac, mesNac, anyoNac, estadoCivil, sexo, edadConductor, tipoVehiculo, idMarcaVitrineo,
        			idModeloVitrineo, idAnyoVitrineo, numPuertas, esDuenyo, ciudadHogar, direccionHogar, numeroDireccionHogar, numeroDeptoHogar,
        			anyoHogar, rama, subcategoria, productoVitrineo, rutDuenyo, dvDuenyo, nombreDuenyo, apellidoPaternoDuenyo, apellidoMaternoDuenyo,
        			diaNacimientoDuenyo, mesNacimientoDuenyo, anyoNacimientoDuenyo, estadoCivilDuenyo, regionResidenciaDuenyo, comunaResidenciaDuenyo,
        			valorComercial);
         } catch (Exception e) {
             throw new SystemException(e);
         } finally {
             dao.close();
         }
     }
     
     
     
     /**
      * Obtiene los Vehiculos registrados a cada cliente.
      * @ejb.interface-method "remote"
      */
     public List < HashMap < String, String > > obtenerVehiculos(String rut) {
    	 logger.info("Rut en BEAN:" + rut);
         ACVDAOFactory factory = ACVDAOFactory.getInstance();
         CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
         try {
             return dao.obtenerVehiculos(rut);
         } catch (Exception e) {
             throw new SystemException(e);
         } finally {
             dao.close();
         }
     }
     
     /**
      * Obtiene los datos de los Vehiculos registrados a cada cliente.
      * @ejb.interface-method "remote"
      */
     public Map<String, Object> obtenerVehiculosCompleto(String rut,String modelo) {
         ACVDAOFactory factory = ACVDAOFactory.getInstance();
         CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
         try {
             return dao.obtenerVehiculosCompleto(rut,modelo);
         } catch (Exception e) {
             throw new SystemException(e);
         } finally {
             dao.close();
         }
     }
     
     /**
      * Obtiene los datos de los Vehiculos que se encuentran en la BD local.
      * @ejb.interface-method "remote"
      */
     public Map<String, Object> obtenerDatosVehiculosCompleto(String tipo,String marca, String modelo) {
         ACVDAOFactory factory = ACVDAOFactory.getInstance();
         CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
         try {
             return dao.obtenerDatosVehiculosCompleto(tipo,marca,modelo);
         } catch (Exception e) {
             throw new SystemException(e);
         } finally {
             dao.close();
         }
     }
     
     public Map<String, Object> obtenerDatosVitrineo(String claveVitrineo) {
         ACVDAOFactory factory = ACVDAOFactory.getInstance();
         CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
         try {
             return dao.obtenerDatosVitrineo(claveVitrineo);
         } catch (Exception e) {
             throw new SystemException(e);
         } finally {
             dao.close();
         }
     }
     
     /**
      * Obtiene los Planes para Cotizar con diferentes deducibles.
      * @throws ProcesoCotizacionException 
      * @throws ValorizacionException 
      * @ejb.interface-method "remote"
      */
     public Plan[] obtenerPlanesDeducibles(String idProducto2,String idProducto3, String idProducto4, 
    		 String idProducto5, String idProducto6, String idProducto7,String idPlanPromocion)
         throws ProcesoCotizacionException, ValorizacionException {

         logger.info("En obtenerPlanesDeducibles");
         ACVDAOFactory factory = ACVDAOFactory.getInstance();
         logger.info("Obteniendo factory");
         CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
         logger.info("Obteniendo dao");

         try {
             logger.debug("Obteniendo Planes...");
             Plan[] planes =
                 dao.obtenerPlanesDeducibles(idProducto2, idProducto3, idProducto4, idProducto5, idProducto6, idProducto7,idPlanPromocion);
             
             if(planes == null || planes.length == 0) {
                 logger.error("No Existen PLANES asociados.");
                 logger.error("idProducto: " + idProducto2);
                 /*throw new ProcesoCotizacionException(
                     ProcesoCotizacionException.PLAN_NO_ENCONTRADO,
                     new Object[] {});*/
             }

             logger.debug("Planes obtenidos: " + planes);
             return planes;
         } finally {
             dao.close();
         }
     }
     
     /**
      * Obtiene la valorizacion de un plan deducibles.
      * @throws ProcesoCotizacionException 
      * @throws ValorizacionException 
      * @ejb.interface-method "remote"
      */
     public Plan valorizarPlanDeducibles(String idProducto2, String idProducto3, String idProducto4,
    		 String idProducto5, String idProducto6, String idProducto7,	 
         CotizacionSeguro cotizacionSeguro, String idPlan)
         throws ProcesoCotizacionException, ValorizacionException {

         Plan planValorizado = new Plan();

         ValorizacionSeguro valorizacion = null;

         ACVDAOFactory factory = ACVDAOFactory.getInstance();
         CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
         ObtenerCoberturasDAO daoCoberturas = factory.getVentaSegurosDAO(ObtenerCoberturasDAO.class);
         if (cotizacionSeguro instanceof CotizacionSeguroVehiculo) {
             logger.info("Valorizacion Tipo Vehiculo...");
             HttpServletRequest request = null;
             CotizacionSeguroVehiculo cotizacionVehiculo = (CotizacionSeguroVehiculo) cotizacionSeguro;

             // Obtiene codigo plan bigsa.
             Plan[] plan = null;
             
             plan = dao.obtenerPlanCotizacionDeducibles(idProducto2,idProducto3, idProducto4, idProducto5, idProducto6, idProducto7, idPlan);
             if (plan != null && plan.length > 0) {
                 cotizacionVehiculo.setCodigoPlan(Long.parseLong(plan[0].getIdPlanLegacy()));
                 cotizacionVehiculo.setCodigoProducto(plan[0].getIdProducto());
                 planValorizado = plan[0];
             }

             // Obtiene codigos Legacy de vehiculo.

             String idTipo = String.valueOf(cotizacionVehiculo.getCodigoTipoVehiculo());
             String idMarca = String.valueOf(cotizacionVehiculo.getCodigoMarca());
             String idModelo = String.valueOf(cotizacionVehiculo.getCodigoModelo());

             Vehiculo vehiculo = dao.obtenerVehiculoLegacy(idTipo, idMarca, idModelo);

             CotizacionSeguroVehiculo nuevaCotizacionVehiculo = new CotizacionSeguroVehiculo();

             try {
                 BeanUtils.copyProperties(nuevaCotizacionVehiculo,cotizacionVehiculo);

                 Contratante asegurado = new Contratante();
                 Contratante contratante = new Contratante();
                 BeanUtils.copyProperties(asegurado, cotizacionVehiculo.getAsegurado());
                 BeanUtils.copyProperties(contratante, cotizacionVehiculo.getContratante());

                 nuevaCotizacionVehiculo.setAsegurado(asegurado);
                 nuevaCotizacionVehiculo.setContratante(contratante);
             } catch (IllegalAccessException e) {
                 logger.error("Error ilegal al copiar propiedades", e);
             } catch (InvocationTargetException e) {
                 logger.error("Error de invocacion al copiar propiedades", e);
             }

             if (vehiculo != null) {
                 nuevaCotizacionVehiculo.setCodigoMarca(Integer.parseInt(vehiculo.getIdMarcaVehiculo()));
                 nuevaCotizacionVehiculo.setCodigoTipoVehiculo(Integer.parseInt(vehiculo.getIdTipoVehiculo()));
                 nuevaCotizacionVehiculo.setCodigoModelo(Integer.parseInt(vehiculo.getIdModeloVehiculo()));
             } else {
                 throw new ProcesoCotizacionException(
                     ProcesoCotizacionException.VEHICULO_NO_ENCONTRADO,new Object[] {});
             }

             // Obtiene datos de Valorizaci�n por plan.

             try {
                 valorizacion =this.obtenerValorizacionVehiculo(nuevaCotizacionVehiculo);

                 if (valorizacion != null) {
                     planValorizado.setPrimaMensualUF(valorizacion.getPrimaMensual());
                     planValorizado.setPrimaMensualPesos(valorizacion.getPrimaMensualPesos());
                     planValorizado.setPrimaAnualUF(valorizacion.getPrimaAnual());
                     planValorizado.setPrimaAnualPesos(valorizacion.getPrimaAnualPesos());
                     planValorizado.setIdProducto(valorizacion.getIdProducto());

                 } else {
                     throw new ProcesoCotizacionException(ProcesoCotizacionException.ERROR_VALORIZACION,new Object[] {});
                 }
             } catch (ValorizacionException ve) {
                 logger.error("Error al Valorizar Plan: "+ planValorizado.getIdPlan());
                 logger.error("Error al Valorizar Plan: "+ ve);
                 planValorizado.setPrimaMensualUF(0);
                 planValorizado.setPrimaMensualPesos(0);
                 planValorizado.setPrimaAnualUF(0);
                 planValorizado.setPrimaAnualPesos(0);
             }

             planValorizado.setCoberturas(new Cobertura[] { new Cobertura() });
            /* try {
                planValorizado.setCoberturas(daoCoberturas.obtenerCoberturasVehiculo(nuevaCotizacionVehiculo));
             } catch (SystemException se) {
                 planValorizado.setCoberturas(new Cobertura[] { new Cobertura() });
             }*/
         }  
     return planValorizado;
     }
     
     /**
      * Identifica si el cliente es preferente
      * @param rut del cliente
      * @return mensaje
      */
     public String consultarDescuento(int rut){
         ACVDAOFactory factory = ACVDAOFactory.getInstance();
         ClientePreferenteDAO clientePrefe = factory.getVentaSegurosDAO(ClientePreferenteDAO.class);
    	 String glosaDescuento=null;
    	 
		try {
			glosaDescuento = clientePrefe.consultarDescuento(rut);
		} catch (RemoteException e) {
			e.printStackTrace();
		}finally{
			clientePrefe.close();
		}
    	 return glosaDescuento;
     }
     
     /**
      * Graba los datos de la mascota
      * @param rut y dv del cliente
      * @param nombre de la mascota
      * @param tipo de mascota
      * @param raza de la mascota
      * @param color de la mascota
      * @param edad de la mascota
      */
     public void guardarMascota(String rutDuenoMascota, String nombreMascota, String tipoMascota,
     		String razaMascota, String colorMascota, String edadMascota, String sexoMascota, String direccionMascota) {
         ACVDAOFactory factory = ACVDAOFactory.getInstance();
         CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
         try {
             dao.guardarMascota(rutDuenoMascota, nombreMascota, tipoMascota,
         			razaMascota, colorMascota, edadMascota, sexoMascota, direccionMascota);
         } finally {
             dao.close();
         }
     }
     
     public String obtenerIdFicha(int idSubcategoria){
         ACVDAOFactory factory = ACVDAOFactory.getInstance();
         CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
    	 String idFicha = null;
    	 
		try {
			idFicha = dao.obtenerIdFicha(idSubcategoria);
		}finally{
			dao.close();
		}
    	 return idFicha;
     }
     
     /**
      * Obtiene las Coberturas asociadas a un plan.
      * @ejb.interface-method "remote"
      */
     public CoberturaSeg[] consultarCoberturas(int idPlan) {
         ACVDAOFactory factory = ACVDAOFactory.getInstance();
         ConsultarCoberturaDAOWS dao = factory.getVentaSegurosDAO(ConsultarCoberturaDAOWS.class);
         try {
             return dao.obtenerCoberturas(new Integer(idPlan));
         } catch (Exception e) {
             throw new SystemException(e);
         } finally {
             dao.close();
         }
     }
     
     /**
      * Obtiene las caracteristicas del vehiculo por su patente.
      * @ejb.interface-method "remote"
      */
     //Pleyasoft - Patente Vehiculo
     public PatenteVehiculo obtenerDatosPatente(String idPatente) {
         ACVDAOFactory factory = ACVDAOFactory.getInstance();
         CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
         try {
             logger.debug("Consultar Datos Pantente DAO...");
             return dao.obtenerDatosPatente(idPatente);
         } catch (Exception e) {
             throw new SystemException(e);
         } finally {
             dao.close();
         }
     }
     /**
      * Obtiene el id de una subcategoria seg�n producto y rama.
      * @param idProducto producto asociado.
      * @param idRama rama asociada.
      * @return
      * @ejb.interface-method "remote"
      */
     public long guardarDatosPatente(PatenteVehiculo p) {
         ACVDAOFactory factory = ACVDAOFactory.getInstance();
         CotizacionDAO dao = factory.getVentaSegurosDAO(CotizacionDAO.class);
         SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         Date date = new Date();
         
         //System.out.println(df.format(date));
         
         long update = 0;
         try {
        	 p.setFechaModificacion(df.format(date));
             update = dao.actualizarDatosPatente(p);
             if(update > 0){
            	 logger.debug("Actualizar Datos Pantente DAO...");
            	 return update;
             }else{
            	 logger.debug("Insertar Datos Pantente DAO...");
            	 //System.out.println(p.toString());
            	 
            	 return dao.agregarDatosPatente(p);
             }
         } catch (Exception e) {
             throw new SystemException(e);
         } finally {
             dao.close();
         }
     }
     
     //Pleyasoft - Patente Vehiculo
}