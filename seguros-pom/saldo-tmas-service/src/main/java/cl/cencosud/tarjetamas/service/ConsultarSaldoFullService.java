package cl.cencosud.tarjetamas.service;

import cl.cencosud.tarjetamas.exception.ApplicationException;
import cl.cencosud.tarjetamas.exception.BusinessException;
import cl.cencosud.tarjetamas.pojo.ConsultarSaldoFullRSP;


public interface ConsultarSaldoFullService {
	
	public ConsultarSaldoFullRSP consultarSaldoFull(String arg0) throws ApplicationException, BusinessException;	

}
