/**
 * 
 */
package cl.cencosud.tarjetamas.exception;

/**
 * Excepci�n de Aplicaci�n.
 * @author Reinaldo Alvarez
 * @version 1.0
 *
 */
public class ApplicationException extends Exception{

	private static final long serialVersionUID = 7749688205979641010L;
	private String  mensaje;
	
	public ApplicationException(){
		mensaje ="error.generico";
    }

    public ApplicationException(Exception e){
        setStackTrace(e.getStackTrace());
    }

	public ApplicationException(String string) {
		mensaje = string;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	

}
