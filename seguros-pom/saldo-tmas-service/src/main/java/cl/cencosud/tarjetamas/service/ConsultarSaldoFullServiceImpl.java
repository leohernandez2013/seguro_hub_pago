package cl.cencosud.tarjetamas.service;

import java.io.Serializable;

import javax.jms.JMSException;

import org.apache.log4j.Logger;

import com.ibm.mq.MQException;

import cl.acl.frameworkjms.exceptions.ConvertObjectException;
import cl.acl.frameworkjms.exceptions.InitException;
import cl.acl.frameworkjms.exceptions.TimeOutException;
import cl.acl.frameworkjms.service.MessagingProcess;
import cl.cencosud.tarjetamas.exception.ApplicationException;
import cl.cencosud.tarjetamas.exception.BusinessException;
import cl.cencosud.tarjetamas.pojo.ConsultarSaldoFullREQ;
import cl.cencosud.tarjetamas.pojo.ConsultarSaldoFullRSP;

public class ConsultarSaldoFullServiceImpl implements Serializable, ConsultarSaldoFullService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1979938313318350936L;
	
	private MessagingProcess consultarSaldoFullProcessREQ;
	private MessagingProcess consultarSaldoFullProcessRSP;
	
	public ConsultarSaldoFullServiceImpl() throws InitException {
		consultarSaldoFullProcessREQ = new MessagingProcess("REQ", 15, "conf/ConsultarSaldoFullREQ.xml");
		consultarSaldoFullProcessRSP = new MessagingProcess("RSP", 15, "conf/ConsultarSaldoFullRSP.xml");
	}
	
	protected final static Logger logger = Logger.getLogger(ConsultarSaldoFullServiceImpl.class); 

	public ConsultarSaldoFullRSP consultarSaldoFull(String cuenta) throws ApplicationException, BusinessException {
		ConsultarSaldoFullREQ requestJMS = new ConsultarSaldoFullREQ();
		
		requestJMS.setCanal("INTERNETCL");
		requestJMS.setNumeroCuenta(cuenta);
		requestJMS.setNumeroSecuencia("0001");
		if (cuenta == null || cuenta.trim().length() < 19) {
			throw new ApplicationException("error.consultar.saldo.parametros.entrada");
		}
		
		String org = null;
		
		if (cuenta.startsWith("00061528031") || cuenta.startsWith("00061528032") || cuenta.startsWith("00061528033") || cuenta.startsWith("00061528034")) {
			org = "603";
		}else{
			org = "601";
		}
		
		requestJMS.setOrganizacion(org);
		
		ConsultarSaldoFullRSP responseJMS = null;
		
		try {
			String messageId = consultarSaldoFullProcessREQ.doSendMessage(requestJMS);
			responseJMS = (ConsultarSaldoFullRSP) consultarSaldoFullProcessRSP.getSingleMessage(messageId);
		} catch (JMSException e) {
			logger.error("consultarSaldoFullProcess JMSException ", e);
			if (e.getLinkedException() instanceof MQException) {
				MQException mqe = (MQException) e.getLinkedException();
				logger.error("consultarSaldoFullProcessREQ JMSLinkedException "+mqe.getMessage());
			}
			throw new ApplicationException("error.consultar.saldo.full");
		} catch (ConvertObjectException e) {
			logger.error("consultarSaldoFullProcess ConvertObjectException ", e);
			throw new ApplicationException("error.consultar.saldo.full");
		} catch (TimeOutException e) {
			logger.error("consultaSaldoProcessRSP TimeOutException ", e);
			throw new ApplicationException("error.consulta.saldo.full.mq");
		} catch (Exception e) {
			logger.error("consultaSaldoProcessRSP Exception "+e.getMessage());
			throw new ApplicationException("error.consultar.saldo.full");
		}
		
		return responseJMS;
	}

	public void setConsultarSaldoFullProcessREQ(
			MessagingProcess consultarSaldoFullProcessREQ) {
		this.consultarSaldoFullProcessREQ = consultarSaldoFullProcessREQ;
	}

	public void setConsultarSaldoFullProcessRSP(
			MessagingProcess consultarSaldoFullProcessRSP) {
		this.consultarSaldoFullProcessRSP = consultarSaldoFullProcessRSP;
	}

}
