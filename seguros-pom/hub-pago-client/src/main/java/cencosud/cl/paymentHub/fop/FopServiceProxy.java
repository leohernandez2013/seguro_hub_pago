package cencosud.cl.paymentHub.fop;

public class FopServiceProxy implements cencosud.cl.paymentHub.fop.FopService {
  private String _endpoint = null;
  private cencosud.cl.paymentHub.fop.FopService fopService = null;
  
  public FopServiceProxy() {
    _initFopServiceProxy();
  }
  
  public FopServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initFopServiceProxy();
  }
  
  private void _initFopServiceProxy() {
    try {
      fopService = (new cencosud.cl.paymentHub.fop.FopLocator()).getFopServicePort();
      if (fopService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)fopService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)fopService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (fopService != null)
      ((javax.xml.rpc.Stub)fopService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public cencosud.cl.paymentHub.fop.FopService getFopService() {
    if (fopService == null)
      _initFopServiceProxy();
    return fopService;
  }
  
  public java.lang.String get(java.lang.String request) throws java.rmi.RemoteException, cencosud.cl.paymentHub.fop.WebServiceFaultWrapper{
    if (fopService == null)
      _initFopServiceProxy();
    return fopService.get(request);
  }
  
  public java.lang.String getFopTypeInfo(java.lang.String name) throws java.rmi.RemoteException, cencosud.cl.paymentHub.fop.WebServiceFaultWrapper{
    if (fopService == null)
      _initFopServiceProxy();
    return fopService.getFopTypeInfo(name);
  }
  
  public java.lang.String getFopTypes() throws java.rmi.RemoteException, cencosud.cl.paymentHub.fop.WebServiceFaultWrapper{
    if (fopService == null)
      _initFopServiceProxy();
    return fopService.getFopTypes();
  }
  
  
}