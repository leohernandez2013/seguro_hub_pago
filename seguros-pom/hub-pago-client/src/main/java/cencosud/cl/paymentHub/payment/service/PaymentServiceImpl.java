package cencosud.cl.paymentHub.payment.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.axis.AxisFault;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.SAXException;

import cencosud.cl.paymentHub.payment.Payment;
import cencosud.cl.paymentHub.payment.PaymentLocator;
import cencosud.cl.paymentHub.payment.PaymentService;
import cencosud.cl.paymentHub.payment.PaymentServicePortBindingStub;
import cencosud.cl.paymentHub.payment.mapper.PaymentMapper;
import cencosud.cl.paymentHub.payment.model.RequestGetPayment;
import cencosud.cl.paymentHub.payment.model.RequestInitPayment;
import cencosud.cl.paymentHub.payment.model.ResponseGetPayment;
import cencosud.cl.paymentHub.payment.model.ResponseInitPayment;

/**
 * 
 * @author Virtual
 * 
 */
public class PaymentServiceImpl implements IPaymentService {

	private static Log logger = LogFactory.getLog(PaymentServiceImpl.class);
	private static final String APPLICATION_ID = "7002";
	private static final String COMMERCE_SESSION_ID = "7002";
	private static final String CURRENCY = "CLP";
	private static final String DISCOUNTED_AMOUNT = "0";

	
	/**
	 * 
	 */
	public ResponseInitPayment initPayment(String urlWsdl, RequestInitPayment request) {

		ResponseInitPayment responsePayment = null;

		try {
			PaymentMapper mapper = new PaymentMapper();
			Payment paymentlocator = new PaymentLocator();
			PaymentService payment = new PaymentServicePortBindingStub(new URL(
					urlWsdl),
					paymentlocator);
			
			
			// SE SETEAN PARAMETROS POR DEFECTO
			if (request != null) {
				request.setApplicationId(APPLICATION_ID);
				request.setCommerceSessionId(COMMERCE_SESSION_ID);
				request.setCurrency(CURRENCY);
				request.setDiscountedAmount(DISCOUNTED_AMOUNT);
			}

			String requestMessage = getInitRequest(request);
			String responseMessage = payment.init(requestMessage);
			responsePayment = mapper.mapInitResponse(responseMessage);

		} catch (AxisFault e) {
			logger.error(e.getMessage(), e);
			e.printStackTrace();
		} catch (MalformedURLException e) {
			logger.error(e.getMessage(), e);
		} catch (RemoteException e) {
			logger.error(e.getMessage(), e);
		} catch (SAXException e) {
			logger.error(e.getMessage(), e);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		} catch (ParserConfigurationException e) {
			logger.error(e.getMessage(), e);
		}

		return responsePayment;
	}


	


	/**
	 * Metodo que consulta el estado de un pago
	 */
	public ResponseGetPayment getPaymentStatusByPaymentId(String urlWsdl, String paymentId) {
		ResponseGetPayment response = null;

		try {
			
			PaymentService payment;
			PaymentMapper mapper = new PaymentMapper();
			Payment paymentlocator = new PaymentLocator();
			payment = new PaymentServicePortBindingStub(new URL(
					urlWsdl),
					paymentlocator);
			RequestGetPayment request = new RequestGetPayment(paymentId);
			String requestMessage = getRequestGetPayment(request);
			String responseMessage = payment.get(requestMessage);
			response = mapper.mapGetPayment(responseMessage);
			response.setPaymentId(paymentId);
		} catch (AxisFault e) {
			logger.error(e.getMessage(), e);
		} catch (MalformedURLException e) {
			logger.error(e.getMessage(), e);
		} catch (RemoteException e) {
			logger.error(e.getMessage(), e);
		}

		return response;
	}


	/**
	 * 
	 * @param request
	 * @return
	 */
	private String  getRequestGetPayment(RequestGetPayment request) {
		String requestMessage = "<message><header>"+
		"<action>/PAYMENT/Get</action>"+
		"<dateTime>2012.10.10 23:23:23.539</dateTime>"+
		"<messageId>fdde-13dr-ewe2-jjde-12u4</messageId>"+
		"</header>"+
		"<body action=\"request\">"+
		"<paymentId>"+request.getPaymentId()+"</paymentId>"+
		"</body></message>";
		
		return requestMessage;
	}

	

	/**
	 * 
	 * @param requestPayment
	 * @return
	 */
	private String getInitRequest(RequestInitPayment requestPayment) {
		String request = "<message>" + "<header>"
				+ "<action>/PAYMENT/Init</action>"
				+ "<dateTime>YYYY.MM.DD HH:MM:SS.SSSSSS</dateTime>"
				+ "<messageId>fdde-13dr-ewe2-jjde-12u4</messageId>"
				+ "</header>" + "<body action=\"request\">"
				+ "<applicationId>"
				+ requestPayment.getApplicationId()
				+ "</applicationId>"
				+ "<commerceSessionId>"
				+ requestPayment.getCommerceSessionId()
				+ "</commerceSessionId>"
				+ "<orderId>"
				+ requestPayment.getOrderId()
				+ "</orderId>"
				+ "<returnUrl>"
				+ requestPayment.getReturnUrl()
				+ "</returnUrl>"
				+ "<clientName>"
				+ "<first>"
				+ requestPayment.getClientName().getFirstName()
				+ "</first>"
				+ "<last>"
				+ requestPayment.getClientName().getLastName()
				+ "</last>"
				+ "</clientName>"
				+ "<amount>"
				+ requestPayment.getAmount()
				+ "</amount>"
				+ "<currency>"
				+ requestPayment.getCurrency()
				+ "</currency>"
				+ "<discountedAmount>"
				+ requestPayment.getDiscountedAmount()
				+ "</discountedAmount>"
				+ "<fopId>11</fopId>"
				+ "</body>"
				+ "</message>";
		return request;
	}
	
	

}
