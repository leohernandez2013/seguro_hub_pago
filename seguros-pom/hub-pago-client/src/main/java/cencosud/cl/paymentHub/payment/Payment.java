/**
 * Payment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cencosud.cl.paymentHub.payment;

public interface Payment extends javax.xml.rpc.Service {
    public java.lang.String getPaymentServicePortAddress();

    public cencosud.cl.paymentHub.payment.PaymentService getPaymentServicePort() throws javax.xml.rpc.ServiceException;

    public cencosud.cl.paymentHub.payment.PaymentService getPaymentServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
