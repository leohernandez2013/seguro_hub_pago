/**
 * PaymentLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cencosud.cl.paymentHub.payment;

public class PaymentLocator extends org.apache.axis.client.Service implements cencosud.cl.paymentHub.payment.Payment {

    public PaymentLocator() {
    }


    public PaymentLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public PaymentLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for PaymentServicePort
    private java.lang.String PaymentServicePort_address = "http://pagos.qa.cencosud.com:80/payment_hub/services/soap/v2/PaymentService";

    public java.lang.String getPaymentServicePortAddress() {
        return PaymentServicePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String PaymentServicePortWSDDServiceName = "PaymentServicePort";

    public java.lang.String getPaymentServicePortWSDDServiceName() {
        return PaymentServicePortWSDDServiceName;
    }

    public void setPaymentServicePortWSDDServiceName(java.lang.String name) {
        PaymentServicePortWSDDServiceName = name;
    }

    public cencosud.cl.paymentHub.payment.PaymentService getPaymentServicePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(PaymentServicePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getPaymentServicePort(endpoint);
    }

    public cencosud.cl.paymentHub.payment.PaymentService getPaymentServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            cencosud.cl.paymentHub.payment.PaymentServicePortBindingStub _stub = new cencosud.cl.paymentHub.payment.PaymentServicePortBindingStub(portAddress, this);
            _stub.setPortName(getPaymentServicePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setPaymentServicePortEndpointAddress(java.lang.String address) {
        PaymentServicePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (cencosud.cl.paymentHub.payment.PaymentService.class.isAssignableFrom(serviceEndpointInterface)) {
                cencosud.cl.paymentHub.payment.PaymentServicePortBindingStub _stub = new cencosud.cl.paymentHub.payment.PaymentServicePortBindingStub(new java.net.URL(PaymentServicePort_address), this);
                _stub.setPortName(getPaymentServicePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("PaymentServicePort".equals(inputPortName)) {
            return getPaymentServicePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://cl.cencosud/paymentHub/services", "Payment");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://cl.cencosud/paymentHub/services", "PaymentServicePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("PaymentServicePort".equals(portName)) {
            setPaymentServicePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
