/**
 * FopService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package cencosud.cl.paymentHub.fop;

public interface FopService extends java.rmi.Remote {
    public java.lang.String get(java.lang.String request) throws java.rmi.RemoteException, cencosud.cl.paymentHub.fop.WebServiceFaultWrapper;
    public java.lang.String getFopTypeInfo(java.lang.String name) throws java.rmi.RemoteException, cencosud.cl.paymentHub.fop.WebServiceFaultWrapper;
    public java.lang.String getFopTypes() throws java.rmi.RemoteException, cencosud.cl.paymentHub.fop.WebServiceFaultWrapper;
}
