package cl.tinet.common.sessioncontrol.util;

import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Utilitario del servicio de sesi�n persistente.
 * <br/>
 * @author Roberto San Mart�n
 * @version 1.0
 * @created 26-Jul-2010 15:32:02
 */
public class ControlDeSesionUtil {

    /**
     * ID de la cookie con el identificador de sesi�n persistente.
     */
    public static final String SESSION_ID_KEY = "PSESSIONID";

    /**
     * Variable de acceso a log.
     */
    private static Log logger = LogFactory.getLog(ControlDeSesionUtil.class);

    /**
     * Constructor de la clase. Se define privado para evitar instanciaci�n.
     */
    private ControlDeSesionUtil() {
    }

    /**
     * Recupera el identificador de sesi�n persistente. En caso de que
     * no exista, se crea un nuevo identificador, se establece en la cookie
     * y se retorna.
     * <br/>
     * @param aplicacion nombre de la aplicacion a la que pertenece la sesion.
     *        Permite diferenciar entre sesiones de aplicaciones diferentes.
     * @param request datos de la solicitud.
     * @param response datos de la respuesta al usuario.
     * @param contextoBase ruta de contexto base para obtener el ID. En caso de que
     *        se especifique <code>null</code> se asume que es el contexto
     *        de la aplicaci�n actual.
     * @return identificador de sesi�n.
     */
    public static String obtenerSID(String aplicacion,
        HttpServletRequest request, HttpServletResponse response,
        String contextoBase) {
        String path = contextoBase;
        String cookieName = SESSION_ID_KEY;
        if (path == null) {
            path = request.getContextPath();
        }
        if (aplicacion != null) {
            cookieName = aplicacion + "_" + SESSION_ID_KEY;
        }
        String sessionID = null;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                Cookie cookie = cookies[i];
                if (cookieName.equals(cookie.getName())) {
                    sessionID = cookie.getValue();
                }
            }
        }
        if (sessionID == null) {
            sessionID = generarSID(request);
            Cookie cookie = new Cookie(cookieName, sessionID);
            cookie.setMaxAge(Integer.MIN_VALUE);
            cookie.setPath(path);
            response.addCookie(cookie);
        } else if (!isSIDValido(sessionID)) {
            throw new IllegalStateException(
                "El ID de sesion encontrado no es v�lido: " + sessionID);
        }
        return sessionID;
    }

    /**
     * Recupera el identificador de sesi�n persistente. En caso de que
     * no exista, se crea un nuevo identificador, se establece en la cookie
     * y se retorna.
     * <br/>
     * @param request datos de la solicitud.
     * @param response datos de la respuesta al usuario.
     * @param contextoBase ruta de contexto base para obtener el ID. En caso de que
     *        se especifique <code>null</code> se asume que es el contexto
     *        de la aplicaci�n actual.
     * @return identificador de sesi�n.
     */
    public static String obtenerSID(HttpServletRequest request,
        HttpServletResponse response, String contextoBase) {
        return obtenerSID(null, request, response, contextoBase);
    }

    /**
     * Recupera el identificador de sesi�n persistente. En caso de que
     * no exista, se crea un nuevo identificador, se establece en la cookie
     * y se retorna.
     * <br/>
     * @param request datos de la solicitud.
     * @param response datos de la respuesta al usuario.
     * @return identificador de sesi�n.
     */
    public static String obtenerSID(HttpServletRequest request,
        HttpServletResponse response) {
        return obtenerSID(null, request, response, null);
    }

    /**
     * Crea un nuevo identificador de sesi�n v�lido.
     * <br/>
     * @param request datos de la solicitud.
     * @return nuevo identificador.
     */
    public static String generarSID(HttpServletRequest request) {
        return UUID.randomUUID().toString();
    }

    /**
     * Valida si es que el identificador especificado es o no valido.
     * @param sid identificador a validar.
     * @return <code>true</code> si es que el identificador es v�lido.
     */
    public static boolean isSIDValido(String sid) {
        try {
            UUID.fromString(sid);
            return true;
        } catch (NumberFormatException nfe) {
            logger.trace("Error validando formato de ID de sesi�n.", nfe);
        } catch (IllegalArgumentException iae) {
            logger.trace("Error validando formato de ID de sesi�n.", iae);
        }
        return false;
    }
}
