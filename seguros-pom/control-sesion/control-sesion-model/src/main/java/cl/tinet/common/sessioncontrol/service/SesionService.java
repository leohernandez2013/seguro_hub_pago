package cl.tinet.common.sessioncontrol.service;

import java.util.Date;
import java.util.Map;

import cl.tinet.common.util.resource.Closeable;

/**
 * Interfaz de servicio de acceso a datos de sesi�n persistente.
 * <br/>
 * @author Roberto San Mart�n.
 * @version 1.0
 * @created Aug 13, 2010
 */
public interface SesionService extends Closeable {

    /**
     * Retorna el valor del atributo especificado.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     * @param nombre identificador del atributo a obtener.
     * @return valor del atributo asociado al nombre especificado.
     */
    public abstract Object getAttribute(String aplicacion, String sid,
        String nombre);

    /**
     * Retorna los nombres de los atributos en la sesi�n.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     * @return todos los nombres de los atributos en sesi�n.
     */
    public abstract String[] getAttributeNames(String aplicacion, String sid);

    /**
     * Retorna todos los atributos establecidos en la sesi�n.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     * @return {@link Map} con los datos en sesi�n indexados por
     *          nombre.
     */
    public abstract Map < String, ? > getAttributesMap(String aplicacion,
        String sid);

    /**
     * Permite establecer un valor en sesi�n.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     * @param nombre identificador del atributo a establecer.
     * @param valor valor a establecer.
     */
    public abstract void setAttribute(String aplicacion, String sid,
        String nombre, Object valor);

    /**
     * Permite eliminar un elemento de la sesi�n.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     * @param nombre identificador del atributo a eliminar.
     */
    public abstract void delAttribute(String aplicacion, String sid,
        String nombre);

    /**
     * Retorna la fecha de ultimo acceso a la sesi�n.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     * @return fecha de ultimo acceso a la sesi�n.
     */
    public abstract Date getLastAccess(String aplicacion, String sid);

    /**
     * Retorna la fecha de creaci�n de la sesi�n.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     * @return fecha de creaci�n de la sesi�n.
     */
    public abstract Date getCreation(String aplicacion, String sid);

    /**
     * Verifica que el SID especificado no existe, en cuyo caso crea el registro
     * en la base de datos. En caso contrario, actualiza la fecha de ultimo
     * acceso para as� evitar que la sesi�n expire.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     */
    public abstract void createSession(String aplicacion, String sid);

    /**
     * Elimina la sesi�n especificada.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     */
    public abstract void invalidate(String aplicacion, String sid);

    /**
     * Elimina las sesiones que se encuentran expiradas.
     * <br/>
     * @return cantidad de sesiones eliminadas.
     */
    public abstract int deleteExpiredSessions();
}
