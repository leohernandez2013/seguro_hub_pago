package cl.tinet.common.sessioncontrol.config;

import java.util.Locale;
import java.util.ResourceBundle;

import cl.tinet.common.config.AbstractConfigurator;

/**
 * Utilitario de configuración del servicio de sesión persistente.
 * <br/>
 * @author Roberto San Martín
 * @version 1.0
 * @created Jul 30, 2010
 */
public class SessionControlConfig extends AbstractConfigurator {

    /**
     * Única instancia del utilitario.
     */
    private static SessionControlConfig instance = new SessionControlConfig();

    /**
     * Archivo de configuración asociado al utilitario.
     */
    private static final String SESSION_CONTROL_BUNDLE =
        "cl.tinet.common.sessioncontrol.resource.SessionControl";

    /**
     * Constructor del utilitario. Se define privado para evitar
     * instanciación directa.
     */
    private SessionControlConfig() {
    }

    /**
     * Retorna la única instancia del utilitario de configuración.
     * <br/>
     * @return utilitario de configuración.
     */
    public static SessionControlConfig getInstance() {
        return instance;
    }

    /**
     * Carga el archivo de configuración asociado al utilitario.
     * <br/>
     * @param locale información de localización.
     * @return archivo de propiedades asociado al utilitario.
     */
    @Override
    public ResourceBundle loadBundle(Locale locale) {
        return ResourceBundle.getBundle(SESSION_CONTROL_BUNDLE, locale);
    }
}
