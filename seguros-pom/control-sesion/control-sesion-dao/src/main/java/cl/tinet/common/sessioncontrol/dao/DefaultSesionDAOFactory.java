package cl.tinet.common.sessioncontrol.dao;

import java.util.Locale;

import cl.tinet.common.dao.AbstractDAOFactory;
import cl.tinet.common.sessioncontrol.config.SessionControlConfig;
import cl.tinet.common.sessioncontrol.dao.jdbc.SesionDAOJDBC;

import com.tinet.exceptions.system.SystemException;

/**
 * DAO Facotry para clases de control de sesión.
 * 
 * @author Roberto San Martín.
 * @version 1.0
 * @created 26-Jul-2010 15:32:02
 */
public class DefaultSesionDAOFactory extends AbstractDAOFactory {

    /**
     * Llave de configuración para implementación concreta de DAO Factory.
     */
    public static final String DEFAULT_KEY =
        "cl.tinet.common.sessioncontrol.DAOFACTORY";

    /**
     * Llave de configuración para implementación concreta de DAO de sesión.
     */
    public static final String SESION_DAO_CLASS_KEY =
        "cl.tinet.common.sessioncontrol.SESION_DAO_CLASS";

    /**
     * Clase DAO concreta de sesión.
     */
    private Class < ? extends SesionDAOJDBC > claseDAO;

    /**
     * Retorna una instancia concreta del DAO Factory.
     * <br/>
     * @param locale información de localización.
     * @return DAO Factory concreto.
     */
    public static DefaultSesionDAOFactory getInstance(Locale locale) {
        return (DefaultSesionDAOFactory) loadFactory(
            DefaultSesionDAOFactory.class, DEFAULT_KEY, SessionControlConfig
                .getInstance(), locale);
    }

    /**
     * Retorna una instancia concreta del DAO Factory.
     * <br/>
     * @return DAO Factory concreto.
     */
    public static DefaultSesionDAOFactory getInstance() {
        return (DefaultSesionDAOFactory) loadFactory(
            DefaultSesionDAOFactory.class, DEFAULT_KEY, SessionControlConfig
                .getInstance());
    }

    /**
     * Retorna una instancia del DAO de sesión.
     * <br/>
     * @return instancia concreta de DAO de sesión.
     */
    @SuppressWarnings("unchecked")
    public SesionDAO getSesionDAO() {
        if (claseDAO == null) {
            claseDAO = SesionDAOJDBC.class;
            String nombre =
                this.getConfigurator().getString(SESION_DAO_CLASS_KEY,
                    this.getLocale());
            if (nombre != null) {
                try {
                    Class clase = Class.forName(nombre);
                    claseDAO = clase;
                } catch (ClassNotFoundException cnfe) {
                    throw new SystemException(cnfe);
                }
            }
        }
        return (SesionDAO) getDAO(SesionDAO.class, claseDAO);
    }
}
