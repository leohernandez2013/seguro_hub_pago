package cl.tinet.common.sessioncontrol.wrapper;

import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpSession;

import cl.tinet.common.sessioncontrol.service.SesionService;
import cl.tinet.common.sessioncontrol.service.SesionServiceFactory;
import cl.tinet.common.util.resource.ResourceLeakUtil;

/**
 * Implementaci�n concreta de request persistente.
 * <p>
 * Se encarga de retornar una instancia de sesi�n persistente
 * implementando el m�todo {@link #getSession(boolean)}.
 * </p>
 * <br/>
 * @author Roberto San Mart�n
 * @version 1.0
 * @created Aug 2, 2010
 */
public class HttpServletRequestPersistente extends HttpServletRequestWrapper {

    /**
     * Identificador de la sesi�n persistente.
     */
    private String sesID;

    /**
     * Nombre de la aplicaci�n.
     */
    private String appName;

    /**
     * Listado de excepciones.
     */
    private List < String > excepciones;

    /**
     * Referencia a la sesi�n persistente asociada a la solicitud.
     */
    private HttpSessionPersistente sp;

    /**
     * Constructor del request de sesi�n persistente.
     * <p>
     * Inicializa el nombre de la aplicaci�n, identificador de sesi�n y
     * listado de excepciones a la persistencia de la sesi�n que se
     * crear�.
     * </p>
     * @param request datos de la solicitud.
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     * @param listado excepciones a la persistencia.
     */
    public HttpServletRequestPersistente(HttpServletRequest request,
        String aplicacion, String sid, List < String > listado) {
        super(request);
        this.sesID = sid;
        this.appName = aplicacion;
        this.excepciones = listado;
    }

    /**
     * Retorna la sesi�n persistente asociada a la solicitud actual.
     * <br/>
     * @param create indicador de si la sesi�n persistente debe ser creada
     *          como una sesi�n nueva en caso de que la sesi�n no exista o
     *          si solo se retornar� <code>null</code>.
     * @return sesi�n persistente.
     */
    public synchronized HttpSession getSession(boolean create) {
    	  HttpSession session;
    	if (sp == null) {
             session = super.getSession(create);
            if (session != null) {
                sp =
                    new HttpSessionPersistente(appName, sesID, excepciones,
                        session);
            } else {
                session = super.getSession(true);
             
                long duration = session.getMaxInactiveInterval() * 1000L;
                   // Date lastAccess = svc.getLastAccess(appName, sesID);
                  
                HttpSessionPersistente  hsp = new HttpSessionPersistente(appName, sesID,
                                excepciones, session);
                 long lastAccess = hsp.getLastAccessedTime();
                                       
                   if (!isSessionExpired(lastAccess, duration) || hsp.getAttribute("cl.tinet.common.seguridad.USUARIO_CONECTADO")==null) {
                       sp = hsp;
                            
                        } else {
                        	hsp.invalidate();
                    	
                   }


                }
        }
        return sp;
    }

    /**
     * Retorna la sesi�n del usuario. En caso de que la sesi�n
     * no exista, crea una nueva excepci�n.
     * <br/>
     * @return sesi�n persistente.
     */
    public synchronized HttpSession getSession() {
        return getSession(true);
    }

    /**
     * Determina si la sesi�n se encuentra expirada o no.
     * <br/>
     * @param lastAccess fecha de ultimo acceso a la aplicaci�n.
     * @param duration duraci�n de la sesi�n en milisegundos.
     * @return <code>true</code> si es que la sesi�n expir�.
     */
    private boolean isSessionExpired(Long lastAccess, long duration) {
        if (lastAccess != null) {
            long accessInterval =
                System.currentTimeMillis() - lastAccess.intValue();
            return duration <= accessInterval;
        }
        return true;
    }
}