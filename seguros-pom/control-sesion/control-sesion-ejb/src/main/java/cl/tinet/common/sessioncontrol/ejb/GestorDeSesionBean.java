package cl.tinet.common.sessioncontrol.ejb;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.ejb.TimedObject;
import javax.ejb.Timer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.tinet.common.sessioncontrol.dao.DefaultSesionDAOFactory;
import cl.tinet.common.sessioncontrol.dao.SesionDAO;
import cl.tinet.common.sessioncontrol.interfaces.GestorDeSesion;

/**
 * EJB con servicios de persistencia de sesi�n.
 * <br/>
 * @author Roberto San Mart�n (TInet Soluciones Inform�ticas).
 * @version 1.0
 * @created 26-Jul-2010 15:32:03
 * 
 * 
 * @ejb.bean name="GestorDeSesion"
 *           display-name="GestorDeSession"
 *           description="Servicios relacionados a gestion de sesion."
 *           jndi-name="ejb/GestorDeSession"
 *           type="Stateless"
 *           view-type="remote"
 *
 * @ejb.transaction type="Supports"
 *
 * @ejb.resource-ref res-ref-name="jdbc/sessionDS" 
 *           res-type="javax.sql.DataSource"
 *           res-sharing-scope="Shareable"
 *           res-auth="Container"
 *           jndi-name="jdbc/sessionDS"
 */
public class GestorDeSesionBean implements SessionBean, TimedObject {

    /**
     * Versi�n de la clase para serializaci�n.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Variable de acceso a log.
     */
    private static final Log LOGGER =
        LogFactory.getLog(GestorDeSesionBean.class);

    /**
     * Contexto de sesi�n del EJB.
     */
    private SessionContext sessionContext;

    /**
     * Permite establecer un valor en sesi�n.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     * @param nombre identificador del atributo a establecer.
     * @param valor valor a establecer.
     * 
     * @ejb.interface-method "remote"
     */
    public void setAttribute(String aplicacion, String sid, String nombre,
        Object valor) {
        SesionDAO sesionDAO =
            DefaultSesionDAOFactory.getInstance().getSesionDAO();
        try {
            sesionDAO.setAttribute(aplicacion, sid, nombre, valor);
        } finally {
            sesionDAO.close();
        }
    }

    /**
     * Retorna el valor del atributo especificado.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     * @param nombre identificador del atributo a obtener.
     * @return valor del atributo asociado al nombre especificado.
     * 
     * @ejb.interface-method "remote"
     */
    public Object getAttribute(String aplicacion, String sid, String nombre) {
        SesionDAO sesionDAO =
            DefaultSesionDAOFactory.getInstance().getSesionDAO();
        try {
            return sesionDAO.getAttribute(aplicacion, sid, nombre);
        } finally {
            sesionDAO.close();
        }
    }

    /**
     * Permite eliminar un elemento de la sesi�n.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     * @param nombre identificador del atributo a eliminar.
     * 
     * @ejb.interface-method "remote"
     */
    public void delAttribute(String aplicacion, String sid, String nombre) {
        SesionDAO sesionDAO =
            DefaultSesionDAOFactory.getInstance().getSesionDAO();
        try {
            sesionDAO.delAttribute(aplicacion, sid, nombre);
        } finally {
            sesionDAO.close();
        }

    }

    /**
     * Permite establecer el contexto de sesi�n del ejb.
     * <br/>
     * @param context contexto de sesi�n a establecer.
     */
    public void setSessionContext(SessionContext context) {
        LOGGER.debug("Estableciendo contexto de sesi�n.");
        this.sessionContext = context;
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Contexto establecido: " + this.sessionContext);
        }
    }

    /**
     * Elimina la sesi�n especificada.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     * 
     * @ejb.interface-method "remote"
     */
    public void invalidate(String aplicacion, String sid) {
        SesionDAO sesionDAO =
            DefaultSesionDAOFactory.getInstance().getSesionDAO();
        try {
            sesionDAO.invalidate(aplicacion, sid);
        } finally {
            sesionDAO.close();
        }
    }

    /**
     * Elimina las sesiones que se encuentran expiradas.
     * <br/>
     * @return cantidad de sesiones eliminadas.
     * 
     * @ejb.interface-method "remote"
     */
    public int deleteExpiredSessions() {
        SesionDAO sesionDAO =
            DefaultSesionDAOFactory.getInstance().getSesionDAO();
        try {
            return sesionDAO.deleteExpiredSessions();
        } finally {
            sesionDAO.close();
        }
    }

    /**
     * Retorna la fecha de ultimo acceso a la sesi�n.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     * @return fecha de ultimo acceso a la sesi�n.
     * 
     * @ejb.interface-method "remote"
     */
    public Date getLastAccess(String aplicacion, String sid) {
        SesionDAO sesionDAO =
            DefaultSesionDAOFactory.getInstance().getSesionDAO();
        try {
            return sesionDAO.getLastAccess(aplicacion, sid);
        } finally {
            sesionDAO.close();
        }
    }

    /**
     * Retorna la fecha de creaci�n de la sesi�n.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     * @return fecha de creaci�n de la sesi�n.
     * 
     * @ejb.interface-method "remote"
     */
    public Date getCreation(String aplicacion, String sid) {
        SesionDAO sesionDAO =
            DefaultSesionDAOFactory.getInstance().getSesionDAO();
        try {
            return sesionDAO.getCreation(aplicacion, sid);
        } finally {
            sesionDAO.close();
        }
    }

    /**
     * Retorna los nombres de los atributos en la sesi�n.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     * @return todos los nombres de los atributos en sesi�n.
     * 
     * @ejb.interface-method "remote"
     */
    public String[] getAttributeNames(String aplicacion, String sid) {
        SesionDAO sesionDAO =
            DefaultSesionDAOFactory.getInstance().getSesionDAO();
        try {
            return sesionDAO.getAttributeNames(aplicacion, sid);
        } finally {
            sesionDAO.close();
        }
    }

    /**
     * Retorna todos los atributos establecidos en la sesi�n.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     * @return {@link Map} con los datos en sesi�n indexados por
     *          nombre.
     * 
     * @ejb.interface-method "remote"
     */
    public Map < String, ? > getAttributesMap(String aplicacion, String sid) {
        SesionDAO sesionDAO =
            DefaultSesionDAOFactory.getInstance().getSesionDAO();
        try {
            return sesionDAO.getAttributesMap(aplicacion, sid);
        } finally {
            sesionDAO.close();
        }
    }

    /**
     * Verifica que el SID especificado no existe, en cuyo caso crea el registro
     * en la base de datos. En caso contrario, actualiza la fecha de ultimo
     * acceso para as� evitar que la sesi�n expire.
     * <br/>
     * @param aplicacion nombre de la aplicaci�n.
     * @param sid identificador de la sesi�n.
     * 
     * @ejb.interface-method "remote"
     */
    public void createSession(String aplicacion, String sid) {
        SesionDAO sesionDAO =
            DefaultSesionDAOFactory.getInstance().getSesionDAO();
        try {
            sesionDAO.createSession(aplicacion, sid);
        } finally {
            sesionDAO.close();
        }
    }

    /**
     * Inicializaci�n del EJB antes de ser utilizado por primera vez.
     * <br/>
     * @throws CreateException en caso de ocurrir un error durante la creaci�n.
     */
    public void ejbCreate() throws CreateException {
        LOGGER.debug("Creando instancia de EJB de gesti�n de sesi�n.");
    }

    /**
     * Libera los recursos del EJB antes de ser descartado.
     */
    public void ejbRemove() {
        LOGGER.debug("Eliminando instancia de EJB de gesti�n de sesi�n.");
    }

    /**
     * Libera recursos antes de pasivaci�n del EJB.
     */
    public void ejbPassivate() {
        LOGGER.debug("Pasivando instancia de EJB de gesti�n de sesi�n.");
    }

    /**
     * Restablece recursos luego de activaci�n del EJB.
     */
    public void ejbActivate() {
        LOGGER.debug("Activando instancia de EJB de gesti�n de sesi�n.");
    }

    /**
     * Realiza la limpieza peri�dica de sesiones expiradas.
     * <br/>
     * @param timer datos del servicio temporizador del contenedor.
     */
    public void ejbTimeout(Timer timer) {
        GestorDeSesion gestor = (GestorDeSesion) sessionContext.getEJBObject();
        try {
            gestor.deleteExpiredSessions();
        } catch (RemoteException e) {
            LOGGER.error("Error eliminando sesiones expiradas.", e);
            throw new EJBException(e);
        }
    }
}
