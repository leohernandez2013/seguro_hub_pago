package cl.tinet.common.captcha.servlet.util;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.octo.captcha.service.CaptchaServiceException;

public class ImageCaptchaServlet extends HttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public void init(ServletConfig servletConfig) throws ServletException {

        super.init(servletConfig);

    }

    protected void doGet(HttpServletRequest httpServletRequest,
        HttpServletResponse httpServletResponse) throws ServletException,
        IOException {

        //        byte[] captchaChallengeAsJpeg = null;
        //        // the output stream to render the captcha image as jpeg into
        //        ByteArrayOutputStream jpegOutputStream = new ByteArrayOutputStream();
        try {
            // get the session id that will identify the generated captcha.
            //the same id must be used to validate the response, the session id is a good candidate!
            String captchaId = httpServletRequest.getSession().getId();
            // call the ImageCaptchaService getChallenge method
            BufferedImage challenge =
                CaptchaServiceSingleton.getInstance().getImageChallengeForID(
                    captchaId, httpServletRequest.getLocale());

            ServletOutputStream responseOutputStream =
                httpServletResponse.getOutputStream();

            httpServletResponse.setHeader("Cache-Control", "no-store");
            httpServletResponse.setHeader("Pragma", "no-cache");
            httpServletResponse.setDateHeader("Expires", 0);
            httpServletResponse.setContentType("image/png");

            // flush it in the response
            ImageIO.write(challenge, "png", responseOutputStream);
            responseOutputStream.flush();
            responseOutputStream.close();

            // a jpeg encoder
            //            JPEGImageEncoder jpegEncoder =
            //                    JPEGCodec.createJPEGEncoder(jpegOutputStream);
            //            jpegEncoder.encode(challenge);
            //            
            //            captchaChallengeAsJpeg = jpegOutputStream.toByteArray();

            //            httpServletResponse.setContentType("image/jpeg");
            //            responseOutputStream.write(captchaChallengeAsJpeg);
        } catch (IllegalArgumentException e) {
            httpServletResponse.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        } catch (CaptchaServiceException e) {
            httpServletResponse
                .sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return;
        }
    }

}
