package cl.tinet.common.struts.exception;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ExceptionHandler;

/**
 * Manejador de excepciones de negocio para la aplicacion.
 *
 * @author Roberto San Mart�n
 * @version 1.0
 * @created Nov 9, 2008
 */
public class BusinessExceptionHandler extends ExceptionHandler {

    private static final Log LOGGER =
        LogFactory.getLog(BusinessExceptionHandler.class);

    /**
     * Se encarga de registrar la excepcion en el log de la aplicacion.
     *
     * @param e excepcion que se registrar�.
     */
    @Override
    protected void logException(Exception e) {
        LOGGER.debug("Se ha encontrado una excepci�n de negocio.", e);
    }
    
}
