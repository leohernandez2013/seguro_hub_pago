package cl.tinet.common.struts.form;

import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.RequestUtils;

import com.tinet.validator.struts.StrutsTinetValidator;

/**
 * TODO Falta descripcion de clase ValidableActionForm.
 * <br/>
 * @author chespi81
 * @version 1.0
 * @created Aug 21, 2010
 */
public abstract class ValidableActionForm extends ActionForm {

    public static final String PROPERTY_NAMES =
        "cl.tinet.common.struts.validator.PROPERTY_NAMES";

    /**
     * M�todo validate.
     * 
     * @param mapping
     *            Atributo con las posibles solicitudes de salida
     * @param request
     *            Atributo con los objetos que pueden ser visibles en un JSP
     * @return Atributo resultado
     */
    public ActionErrors validate(ActionMapping mapping,
        HttpServletRequest request) {

        InputStream validators =
            ValidableActionForm.class
                .getResourceAsStream("/com/tinet/validator/resource/validation-base.xml");
        InputStream formRules = getValidationRules(request);

        /*
         * Se crea un a instancia de validador espec�fico para struts, notar
         * que los par�metros que necesita el constructor son las reglas de
         * validaci�n y la localizaci�n. Esto es porque es posible definir
         * validaciones localizadas. Notar adem�s que la localizaci�n es
         * obtenida mediante la variable de sesi�n de struts definida para este
         * prop�sito
         */
        StrutsTinetValidator validator =
            new StrutsTinetValidator(
                new InputStream[] { validators, formRules }, RequestUtils
                    .getUserLocale(request, null));

        // Ahora validamos el objeto
        validator.validateTO(this);

        // Se retorna resultado de validaci�n.
        ActionErrors results =
            (ActionErrors) validator.getResults(getServlet());
        request.setAttribute(PROPERTY_NAMES, results.properties());

        return results;
    }

    /**
     * Cargamos el archivo de validaci�n generado para esta capa. Notar que ac�
     * se hace necesario que los campos, en este caso las fechas, tengan un
     * formato espec�fico para poder interpretarlas.
     * 
     * @return 
     */
    public abstract InputStream getValidationRules(HttpServletRequest request);

    /**
     * TODO Describir m�todo obtenerClase.
     * @return
     */
    protected Class < ? extends ValidableActionForm > obtenerClase() {
        return this.getClass();
    }
}
