package com.tinet.validator;

import java.util.regex.Pattern;

/**
 * Validador especializado para caracteres esperables en nombres y apellidos
 * de clientes.
 * <br/>
 * @author Roberto San Mart�n
 * @version 1.0
 * @created 06/12/2010
 */
public class CENCOTinetValidator extends TinetValidator {

    /**
     * Realiza la validaci�n de caracteres esperables para nombres y apellidos
     * de clientes.
     * 
     * @param bean objeto que se va a validar.
     * @param va accion de validaci�n.
     * @param field campo a validar.
     * @param validator datos del validador.
     * @return <code>true</code> en caso de que la validaci�n sea exitosa,
     *          <code>false</code> en caso contrario.
     */
    public static boolean validateAlphaSpaceSpecial(java.lang.Object bean,
        org.apache.commons.validator.ValidatorAction va,
        org.apache.commons.validator.Field field,
        org.apache.commons.validator.Validator validator) {
        String value = null;
        value = evaluateBean(bean, field);
        String mask =
            "[A-Za-z\\s'\\-��������������������������������������������]+";
        return validatePattern(value, mask);
    }

    /**
     * M�todo utilitario que valida el valor especificado utilizando la
     * expresi�n regular especificada.
     * 
     * @param value contenido que se desea validar.
     * @param mask patr�n de la expresi�n regular contra la que se validar�.
     * @return <code>true</code> si el valor cumple con el patr�n de la
     *          expresi�n, <code>false</code> en caso contrario.
     */
    public static boolean validatePattern(String value, String mask) {
        Pattern pattern = Pattern.compile(mask);
        if (value != null && value.length() > 0
            && !pattern.matcher(value).matches()) {
            return false;
        } else {
            return true;
        }
    }
}
