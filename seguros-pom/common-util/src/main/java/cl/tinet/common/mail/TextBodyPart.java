package cl.tinet.common.mail;

import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

public class TextBodyPart extends MimeBodyPart
{
  public TextBodyPart(String content)
    throws MessagingException
  {
    super.setText(content);
  }
}
