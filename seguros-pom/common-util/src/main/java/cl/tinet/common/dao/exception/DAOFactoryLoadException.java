package cl.tinet.common.dao.exception;


/**
 * Representa un error durante la carga de la clase DAO Factory.
 * @author Tinet
 * @version 1.0
 * @created 27-Jul-2010 0:49:06
 */
public class DAOFactoryLoadException extends RuntimeException {

    /**
     * Versión de la clase para serialización.
     */
    private static final long serialVersionUID = -437824074723275032L;

    private String className;

    /**
     * Constructor que recibe el nombre de la clase que se intentó cargar y el
     * motivo del error durante la carga.
     *
     * @param className Nombre de clase con problemas de carga.
     * @param throwable Motivo del error.
     */
    public DAOFactoryLoadException(String className, Throwable throwable) {
        super("Problemas cargando la clase especificada.", throwable);
        this.className = className;
    }

    /**
     * Retorna el nombre de la clase con problemas.
     * @return nombre de clase.
     */
    public String getClassName() {
        return className;
    }
}
