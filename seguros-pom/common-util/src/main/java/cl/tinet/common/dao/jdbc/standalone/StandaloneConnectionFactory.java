package cl.tinet.common.dao.jdbc.standalone;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.tinet.common.config.AbstractConfigurator;
import cl.tinet.common.config.BaseConfigurable;
import cl.tinet.common.config.Configurable;
import cl.tinet.common.dao.jdbc.connection.ConnectionFactory;
import cl.tinet.common.dao.jdbc.connection.ConnectionPoolSynchronizer;

import com.tinet.exceptions.system.SystemException;

/**
 * Clase que permite a una apliaci�n obtener conexi�n a la base de datos en un
 * ambiente no gestionado.
 * 
 * @author Tinet
 * @version 1.0
 * @created 27-Jul-2010 0:49:08
 */
public class StandaloneConnectionFactory extends BaseConfigurable implements
    ConnectionFactory {

    /**
     * Sufijo de llave de b�squeda para driver JDBC a utilizar.
     */
    public static final String JDBC_DRIVER_CLASS = "driverClass";

    /**
     * Prefijo predeterminado de llave a utilizar para datos de conexi�n.
     */
    public static final String DEFAULT_CONNECTION_KEY =
        "cl.unimarc.common.dao.jdbc.standalone.StandaloneConnectionFactory";

    /**
     * Sufijo de llave de b�squeda para URL de conexi�n a la BD.
     */
    public static final String JDBC_URL_KEY = "url";

    /**
     * Sufijo de llave de b�squeda para nombre de usuario de la conexi�n.
     */
    public static final String JDBC_USERNAME_KEY = "username";

    /**
     * Sufijo de llave de b�squeda para clave de conexi�n a la BD.
     */
    public static final String JDBC_PASSWORD_KEY = "password";

    /**
     * Sufijo de llave de b�squeda para tiempo de espera de una conexi�n
     * disponible en el pool.
     */
    public static final String JDBC_WAIT_TIME = "waitTime";

    /**
     * Sufijo de llave de b�squeda para tama�o del pool de conexiones.
     */
    public static final String JDBC_POOL_SIZE = "poolSize";

    /**
     * Sufijo de llave de b�squeda para boolean de test de conexiones.
     */
    public static final String POOL_TEST_CONNECTIONS = "testConnections";

    /**
     * Sufijo de llave de b�squeda para SQL de testeo de conexiones.
     */
    public static final String POOL_TEST_CONNECTION_SQL = "testSQL";

    private static Log logger =
        LogFactory.getLog(StandaloneConnectionFactory.class);

    private static Map poolMap = new HashMap();

    private long waitTime;

    /**
     * Constructor de la clase que recibe par�metros de configuraci�n.
     *
     * @param config Instancia concreta de utilitario de configuraci�n.
     * @param locale localizaci�n de configuraci�n valida.
     */
    public StandaloneConnectionFactory(AbstractConfigurator config,
        Locale locale) {
        this.configure(config, locale);
    }

    /**
     * Constructor de la clase que recibe utilitario de configuraci�n. Asume
     * localizaci�n de configuraci�n por defecto de la maquina virtual.
     * 
     * @param config Instancia concreta de utilitario de configuraci�n.
    */
    public StandaloneConnectionFactory(AbstractConfigurator config) {
        this(config, Locale.getDefault());
    }

    /**
     * Constructor de la clase que recibe un objeto configurable, desde donde
     * se obtiene la informaci�n de configuraci�n.
     *
     * @param configurable desde el que se copia la configuraci�n.
     */
    public StandaloneConnectionFactory(Configurable configurable) {
        this.configure(configurable);
    }

    /**
     * Metodo encargado de inicializar la configuraci�n de la instancia del
     * {@link StandaloneConnectionFactory} en base a la llave especificada.
     * 
     * @param baseKey llave de b�squeda de datos de configuraci�n (prefijo).
     * @throws SQLException en caso de error durante la inicializaci�n.
     */
    private void initMe(String baseKey) throws SQLException {
        if (logger.isDebugEnabled()) {
            logger.debug("Inicializando pool para " + baseKey);
        }
        AbstractConfigurator config = getConfigurator();
        Locale local = getLocale();
        try {
            logger.debug("Obteniendo parametros de conficuracion.");
            String driverClassName =
                config.getString(createKey(baseKey, JDBC_DRIVER_CLASS), local);
            String jdbcURL =
                config.getString(createKey(baseKey, JDBC_URL_KEY), local);
            String jdbcUsername =
                config.getString(createKey(baseKey, JDBC_USERNAME_KEY), local);
            String jdbcPassword =
                config.getString(createKey(baseKey, JDBC_PASSWORD_KEY), local);
            this.waitTime =
                config.getLong(createKey(baseKey, JDBC_WAIT_TIME), local);
            int poolSize =
                config.getInt(createKey(baseKey, JDBC_POOL_SIZE), local);
            logger.debug("Leyendo todos los parametros de configuracion.");
            Map data = new HashMap();
            Enumeration keys = config.getBundle(local).getKeys();
            while (keys.hasMoreElements()) {
                String key = (String) keys.nextElement();
                data.put(key, config.getString(key, local));
            }
            if (logger.isDebugEnabled()) {
                logger.debug("Parametros leidos: " + data);
            }
            try {
                if (logger.isDebugEnabled()) {
                    logger.debug("Cargando driver: " + driverClassName);
                }
                Class.forName(driverClassName);
                logger.debug("Driver cargado");
            } catch (ClassNotFoundException cnfe) {
                throw new SystemException(cnfe);
            }
            logger.debug("Creando instancia de pool de conexiones.");
            ConnectionPoolSynchronizer pool =
                ConnectionPoolSynchronizer.newInstance(jdbcURL, jdbcUsername,
                    jdbcPassword, data, poolSize);
            pool.setTestConnections(config.isTrue(createKey(baseKey,
                POOL_TEST_CONNECTIONS), local));
            pool.setTestSQL(config.getString(createKey(baseKey,
                POOL_TEST_CONNECTION_SQL), local));
            logger.debug("Creada e inicializada.");
            poolMap.put(getPoolMapKey(baseKey), pool);
        } catch (NullPointerException npe) {
            throw new SystemException(npe);
        } catch (NumberFormatException nfe) {
            throw new SystemException(nfe);
        }
    }

    /**
     * Retorna la llave interna de b�squeda del pool de conexiones en funci�n
     * de la llave base especificada y la instancia de configuraci�n utilizada.
     *
     * @param baseKey prefijo de configuraci�n utilizado.
     * @return llave de b�squeda interna en el Map de pools.
     */
    protected String getPoolMapKey(String baseKey) {
        String newKey =
            baseKey + '_' + getLocale() + '_'
                + getConfigurator().getClass().getName();
        if (logger.isDebugEnabled()) {
            logger.debug("Nueva llave de pool generada: " + newKey);
        }
        return newKey;
    }

    /**
     * Retorna una conexi�n, utilizando la llave de b�squeda por defecto.
     * @return nueva conexi�n a la base de datos.
     * @throws SQLException en caso de ocurrir un error durante la obtenci�n
     *          de la conexi�n.
     */
    public Connection getConnection() throws SQLException {
        return this.getConnection(DEFAULT_CONNECTION_KEY);
    }

    /**
     * Retorna una conexi�n, utilizando la llave de b�squeda especificada.
     * @param baseKey llave de b�squeda de la conexi�n.
     * @return nueva conexi�n a la base de datos.
     * @throws SQLException en caso de ocurrir un error durante la obtenci�n
     *          de la conexi�n.
     */
    public Connection getConnection(String baseKey) throws SQLException {
        Map pm = poolMap;
        String internalKey = getPoolMapKey(baseKey);
        if (!poolMap.containsKey(internalKey)) {
            if (logger.isDebugEnabled()) {
                logger.debug("Inicializando pool para " + baseKey);
            }
            synchronized (poolMap) {
                if (!pm.containsKey(internalKey)) {
                    try {
                        if (logger.isDebugEnabled()) {
                            logger.debug("Intentando con llave " + baseKey);
                        }
                        this.initMe(baseKey);
                    } catch (SystemException se) {
                        if (DEFAULT_CONNECTION_KEY.equals(baseKey)) {
                            throw se;
                        }
                        logger.debug("Intentando con llave por defecto.");
                        this.initMe(DEFAULT_CONNECTION_KEY);
                        poolMap.put(internalKey, poolMap
                            .get(getPoolMapKey(DEFAULT_CONNECTION_KEY)));
                    }
                }
            }
            if (logger.isDebugEnabled()) {
                logger.debug("Pool de conexiones inicializado para " + baseKey);
            }
        }
        logger.debug("Recuperando pool de conexiones.");
        ConnectionPoolSynchronizer pool =
            (ConnectionPoolSynchronizer) poolMap.get(internalKey);
        if (logger.isDebugEnabled()) {
            logger.debug("Recuperando conexi�n a la BD. " + waitTime
                + ". Instancia: " + this);
        }

        Connection connection = pool.getConnection(this.waitTime);
        if (connection == null) {
            throw new SQLException("No hay conexiones disponibles.");
        }
        logger.debug("Conexi�n recuperada.");
        return connection;
    }

    /**
     * Permite realiza la creaci�n de la llave completa de b�squeda concatenando
     * la base (prefijo) y el detalle (sufijo).
     * @param baseKey prefijo de llave generada.
     * @param detailKey sufijo de llave generada.
     * @return llave compuesta.
     */
    protected String createKey(String baseKey, String detailKey) {
        return baseKey + "." + detailKey;
    }
}
