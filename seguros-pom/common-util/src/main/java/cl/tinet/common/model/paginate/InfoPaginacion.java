package cl.tinet.common.model.paginate;

import java.io.Serializable;
import java.text.MessageFormat;

/**
 * Clase que encapsula la informaci�n referente a la paginaci�n de resultados de
 * listados.
 *
 * @author Roberto San Mart�n
 * @version 1.0
 * @created 27-Jul-2010 0:49:06
 */
public class InfoPaginacion implements Serializable {

    /**
     * Cantidad predeterminada de registros por p�gina.
     */
    public static final int DEFAULT_REGISTROS_PAGINA = 10;

    /**
     * Versi�n de la clase para serializaci�n.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Patr�n para generaci�n de String representativo de datos de paginaci�n.
     */
    private static final String TO_STRING_PATTERN =
        "{0}:[paginaActual=''{1}'',totalRegistros=''{2}'',"
            + "registrosPorPagina=''{3}'']";

    /**
     * P�gina actual de los datos que se est�n paginando.
     */
    private int paginaActual;

    /**
     * Total de los registros que se paginar�n.
     */
    private int totalRegistros;

    /**
     * Tama�o de la p�gina, cantidad de elementos.
     */
    private int registrosPorPagina;

    /**
     * Constructor de la clase por defecto. Inicializa la cantidad de registros
     * en <code>0</code>, la pagina actual en <code>1</code> y la cantidad de
     * registros por p�gina en <code>10</code>.
     */
    public InfoPaginacion() {
        this(1, 0, DEFAULT_REGISTROS_PAGINA);
    }

    /**
     * Constructor de la clase que establece los datos de paginaci�n seg�n los
     * datos especificados por argumento.
     *
     * @param paginaActual p�gina solicitada del listado.
     * @param totalRegistros total de registros del listado.
     * @param registrosPorPagina cantidad de registros de cada pagina.
     */
    public InfoPaginacion(int paginaActual, int totalRegistros,
        int registrosPorPagina) {
        super();
        this.paginaActual = paginaActual;
        this.totalRegistros = totalRegistros;
        this.registrosPorPagina = registrosPorPagina;
    }

    /**
     * @return retorna el valor del atributo paginaActual
     */
    public int getPaginaActual() {
        return paginaActual;
    }

    /**
     * @param paginaActual a establecer en el atributo paginaActual.
     */
    public void setPaginaActual(int paginaActual) {
        this.paginaActual = paginaActual;
    }

    /**
     * @return retorna el valor del atributo totalRegistros
     */
    public int getTotalRegistros() {
        return totalRegistros;
    }

    /**
     * @param totalRegistros a establecer en el atributo totalRegistros.
     */
    public void setTotalRegistros(int totalRegistros) {
        this.totalRegistros = totalRegistros;
    }

    /**
     * @return retorna el valor del atributo registrosPorPagina
     */
    public int getRegistrosPorPagina() {
        return registrosPorPagina;
    }

    /**
     * @param registrosPorPagina a establecer en el atributo registrosPorPagina.
     */
    public void setRegistrosPorPagina(int registrosPorPagina) {
        this.registrosPorPagina = registrosPorPagina;
    }

    /**
     * Retorna la cantidad total p�ginas del listado. El total se calcula en
     * base a la cantidad de registros del listado y a la cantidad de registros
     * por p�gina.
     *
     * @return cantidad total de p�ginas del listado.
     */
    public int getTotalPaginas() {
        if (this.registrosPorPagina < 1) {
            return 0;
        }
        if (this.totalRegistros < 1) {
            return 1;
        }
        return 1 + (this.totalRegistros - 1) / this.registrosPorPagina;
    }

    /**
     * Retorna la representaci�n en String de la instancia del objeto de
     * informaci�n de paginaci�n.
     * @return representaci�n string del objeto.
     */
    public String toString() {
        return MessageFormat.format(TO_STRING_PATTERN, new Object[] {
            InfoPaginacion.class.getName(), String.valueOf(this.paginaActual),
            String.valueOf(this.registrosPorPagina),
            String.valueOf(this.totalRegistros) });
    }
}
