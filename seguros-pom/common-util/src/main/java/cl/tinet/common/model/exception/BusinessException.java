package cl.tinet.common.model.exception;

import java.text.MessageFormat;
import java.util.Locale;

import cl.tinet.common.config.AbstractConfigurator;

/**
 * Excepci�n base para las excepciones de negocio de las aplicaciones. Permite
 * almacenar el contenido de una situaci�n de negocio an�mala, o de muchas
 * situaciones an�malas a trav�s de su atributo {@link #exceptions}.
 * 
 * @see #BusinessException(String, Object[])
 * @see #BusinessException(BusinessException[])
 *
 * @author Roberto San Mart�n
 * @version 1.0
 * @created Jul 27, 2010
 */
public abstract class BusinessException extends Exception {

    /**
     * Constante que representa la una lista de argumentos vac�a.
     */
    public static final Object[] SIN_ARGUMENTOS = {};

    /**
     * Constante que representa una lista de excepciones vac�a.
     */
    public static final BusinessException[] SIN_CONTENIDO = {};

    /**
     * Versi�n de la clase para serializaci�n.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Llave de configuraci�n del mensaje de error. El mensaje configurado
     * puede corresponder a un texto con patrones de reemplazo.
     * 
     * @see MessageFormat#format(String, Object[])
     */
    private String messageKey;

    /**
     * Argumentos que se pasar�n al mensaje de error como informaci�n
     * adicional.
     */
    private Object[] arguments;

    /**
     * Arreglo de errores de negocio contenidos dentro de la instancia
     * de error.
     */
    private BusinessException[] exceptions;

    /**
     * Constructor de la clase.
     * <p>
     * Recibe como argumentos la llave del mensaje de
     * error desde el utilitario de configuraci�n {@link AbstractConfigurator}
     * que retorna el m�todo {@link BusinessException#loadConfigurator()}.
     * </p><p>
     * Recibe adem�s los par�metros del mensaje de error.
     * </p>  
     * @param messageKey llave de b�squeda del mensaje de error.
     * @param arguments argumentos del mensaje de error.
     */
    public BusinessException(String messageKey, Object[] arguments) {
        super(messageKey);
        if (messageKey == null) {
            throw new NullPointerException(
                "La llave de mensaje no debe ser nula.");
        }
        this.messageKey = messageKey;
        this.arguments = SIN_ARGUMENTOS;
        if (arguments != null) {
            this.arguments = (Object[]) arguments.clone();
        }
        this.exceptions = SIN_CONTENIDO;
    }

    /**
     * Constructor que recibe como argumento una colecci�n de excepciones
     * de negocio.
     *
     * @param exceptions arreglo de excepciones contenidas.
     */
    public BusinessException(BusinessException[] exceptions) {
        if (exceptions == null) {
            throw new NullPointerException(
                "El contenido de excepciones no debe ser nulo.");
        }
        this.exceptions = (BusinessException[]) exceptions.clone();
        this.arguments = SIN_ARGUMENTOS;
    }

    /**
     * M�todo encargado de retorna la instancia concreta de
     * {@link AbstractConfigurator} que debe utilizar las excepciones de la
     * aplicaci�n donde se utilice este esquema de excepciones.
     *
     * @return Instancia concreta de {@link AbstractConfigurator} que permite
     *          recuperar los mensajes de error de la aplicaci�n.
     */
    public abstract AbstractConfigurator loadConfigurator();

    /**
     * Retorna el mensaje de error correspondiente a la localizaci�n por
     * defecto de la m�quina virtual. Obtiene el mensaje desde la instancia
     * concreta de {@link AbstractConfigurator} que le corresponde a la
     * excepci�n.
     *
     * @return Mensaje de error con los par�metros establecidos.
     */
    public String getMessage() {
        return this.getMessage(Locale.getDefault());
    }

    /**
     * Retorna el mensaje de error correspondiente a la localizaci�n
     * especificada como argumento. El mensaje es obtenido desde la
     * instancia concreta de {@link AbstractConfigurator} que le corresponde
     * a la excepci�n.
     *
     * @param locale bajo el cual se debe buscar el mensaje de error en la
     *          instancia de {@link AbstractConfigurator} que corresponde a
     *          la excepci�n.
     * @return mensaje de error con los par�metros establecidos.
     */
    public String getMessage(Locale locale) {
        if ((this.exceptions == null) || (this.exceptions == SIN_CONTENIDO)
            || (this.exceptions.length == 0)) {
            String pattern =
                loadConfigurator().getString(this.messageKey, locale);
            if (pattern != null) {
                return MessageFormat.format(pattern, arguments);
            }
        } else {
            StringBuffer buffer = new StringBuffer();
            for (int i = 0; i < this.exceptions.length; i++) {
                buffer.append(this.exceptions[i].getMessage(locale)).append(
                    "\n");
            }
            return buffer.toString();
        }
        return super.getMessage();
    }

    /**
     * Indicador de si la instancia actual corresponde a un contenedor de
     * excepciones de negocio ({@link #exceptions}) o a una excepci�n �nica
     * ({@link #messageKey}).
     * 
     * @return <code>true</code> si es que corresponde a un contenedor de
     *          excepciones.
     */
    public boolean isContenedor() {
        return (this.exceptions != null);
    }

    /**
     * Retorna el arreglo de excepciones causantes contenidas en la
     * excepci�n de negocio.
     *
     * @return arreglo de excepciones contenidas.
     */
    public BusinessException[] getExceptions() {
        return (BusinessException[]) exceptions.clone();
    }
}
