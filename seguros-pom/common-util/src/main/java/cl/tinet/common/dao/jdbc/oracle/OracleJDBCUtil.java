package cl.tinet.common.dao.jdbc.oracle;

import java.sql.ResultSet;
import java.util.Map;

import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.RowProcessor;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;

/**
 * Utilitario para uso de driver JDBC de Oracle.
 * <p>
 * Incluye rutinas relacionadas a la biblioteca commons-dbutils en m�todos
 * �tiles est�ticos.
 * </p>
 * <br/>
 * @author Roberto San Mart�n
 * @version 1.0
 * @created Aug 20, 2010
 */
public class OracleJDBCUtil {

    /**
     * Constructor de la clase que se define privado para evitar instanciaci�n
     * directa.
     */
    private OracleJDBCUtil() {
    }

    /**
     * Retorna un {@link ResultSetHandler} especializado para conexi�n a base
     * de datos Oracle, gracias a que el {@link RowProcessor} contenido en su
     * interior permite recuperar datos de tipo de dato especializados para
     * Oracle.
     * 
     * @see OracleRowProcessor
     * 
     * @param resultClass tipo de dato esperado para cada row del
     *          {@link ResultSet} que se procesar�.
     * @return Manejador de {@link ResultSet} especializado para Oracle.
     */
    public static ResultSetHandler getResultSetHandler(Class resultClass) {
        if (resultClass.getName().equals(Map.class.getName())) {
            return new MapListHandler(new OracleRowProcessor());
        } else {
            return new BeanListHandler(resultClass, new OracleRowProcessor());
        }
    }
}
