package cl.tinet.common.bean;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.beanutils.locale.LocaleBeanUtilsBean;
import org.apache.commons.beanutils.locale.LocaleConvertUtilsBean;
import org.apache.commons.beanutils.locale.converters.DateLocaleConverter;
import org.apache.commons.beanutils.locale.converters.DoubleLocaleConverter;

import cl.tinet.common.config.AbstractConfigurator;

import com.tinet.exceptions.system.SystemException;

/**
 * Utilitario que encapsula rutinas relacionadas a la conversión
 * de datos entre objetos javabeans y {@link Map}.
 * 
 * @author Roberto San Martín
 * @version 1.0
 * @created 21-Ago-2010
 */
public class PopulateUtil {

    /**
     * Llave de configuración para obtener el patron de conversión de fechas
     * de la aplicación.
     */
    public static final String CONVERTER_DATE_PATTERN_KEY =
        "cl.tinet.common.bean.CONVERTER_DATE_PATTERN";

    /**
     * Constructor sin argumentos de la clase. Se define privado para evitar
     * que la clase sea instanciada.
     */
    private PopulateUtil() {
    }

    /**
     * Establece los atributos del javabean especificado utilizando como origen
     * de los datos las entradas en el objeto {@link Map} especificado.
     * <p>
     * Para realizar la operación se utiliza la API commons-beanutils.
     * </p>
     * @param javabean objeto donde se establecerán las propiedades.
     * @param datos origen de los datos a establecer en el {@link Map}.
     * @param config datos de configuración.
     */
    public void populate(Object javabean, Map datos,
        AbstractConfigurator config) {
        populate(javabean, datos, config, Locale.getDefault());
    }

    /**
     * Establece los atributos del javabean especificado utilizando como origen
     * de los datos las entradas en el objeto {@link Map} especificado.
     * <p>
     * Para realizar la operación se utiliza la API commons-beanutils.
     * </p>
     * @param javabean objeto donde se establecerán las propiedades.
     * @param datos origen de los datos a establecer en el {@link Map}.
     * @param config datos de configuración.
     * @param locale datos de localización.
     */
    public static void populate(Object javabean, Map datos,
        AbstractConfigurator config, Locale locale) {
        if (javabean == null) {
            throw new NullPointerException(
                "El javabean especificado no debe ser nulo.");
        }
        if (datos == null) {
            throw new NullPointerException(
                "Los datos especificados no deben ser nulos.");
        }
        try {
            getBeanUtils(config, locale).populate(javabean, datos);
        } catch (IllegalAccessException e) {
            throw new SystemException(e);
        } catch (InvocationTargetException e) {
            throw new SystemException(e);
        }
    }

    /**
     * Permite obtener la representación {@link Map} del javabean especificado.
     * <br/>
     * @param javabean clase que se desea describir.
     * @param config datos de configuración.
     * @return representación {@link Map} del javabean especificado.
     */
    public static Map describe(Object javabean, AbstractConfigurator config) {
        return describe(javabean, config, Locale.getDefault());
    }

    /**
     * Permite obtener la representación {@link Map} del javabean especificado.
     * <br/>
     * @param javabean clase que se desea describir.
     * @param config datos de configuración.
     * @param locale información de localización.
     * @return representación {@link Map} del javabean especificado.
     */
    public static Map describe(Object javabean, AbstractConfigurator config,
        Locale locale) {
        try {
            return getBeanUtils(config, locale).describe(javabean);
        } catch (IllegalAccessException iae) {
            throw new SystemException(iae);
        } catch (InvocationTargetException ite) {
            throw new SystemException(ite);
        } catch (NoSuchMethodException nsme) {
            throw new SystemException(nsme);
        }
    }

    /**
     * Construye el objeto de transformación entre javabean y {@link Map}.
     * <br/>
     * @param config datos de configuración.
     * @param locale datos de localización.
     * @return utilitario de transformación.
     */
    private static LocaleBeanUtilsBean getBeanUtils(
        AbstractConfigurator config, Locale locale) {
        if (locale == null) {
            throw new NullPointerException(
                "El locale especificado no debe ser nulo.");
        }
        String datePattern =
            config.getString(CONVERTER_DATE_PATTERN_KEY, locale);
        if (datePattern == null) {
            datePattern = "dd/MM/yyyy";
        }
        LocaleConvertUtilsBean cub = new LocaleConvertUtilsBean();
        cub.register(new DateLocaleConverter(null, locale, datePattern),
            Date.class, locale);
        cub.register(new CharacterLocaleConverter(), char.class, locale);
        cub.register(new DoubleLocaleConverter(Double.valueOf("0"), locale,
            true), double.class, locale);
        LocaleBeanUtilsBean lbub = new LocaleBeanUtilsBean(cub);
        lbub.setDefaultLocale(locale);
        return lbub;
    }
}
