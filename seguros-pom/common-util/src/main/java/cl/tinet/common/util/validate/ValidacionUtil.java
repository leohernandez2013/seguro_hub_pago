package cl.tinet.common.util.validate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.EmailValidator;

/**
 * Utilitario para realizar validaciones que implican c�lculos complejos.
 * @author Roberto San Mart�n
 * @version 1.0
 * @created 27-Jul-2010 0:49:09
 */
public class ValidacionUtil {

    /**
     * Valor del caracter ASCII <code>0</code>. Utilizado en generacion de
     * d�gito verificador de RUT.
     */
    private static final int CODIGO_ASCII_0 = 47;

    /**
     * Valor de c�digo ASCII <code>K</code>. Utilizado en generaci�n de 
     */
    private static final int CODIGO_ASCII_K = 75;

    /**
     * Divisor decimal.
     */
    private static final int DIVISOR_DECIMAL = 10;

    /**
     * Divisor para c�lculo del m�dulo 11.
     */
    private static final int DIVISOR_MOD11 = 11;

    private static final int DIVISOR_MODULO6 = 6;

    private static final int LIMITE_SUPERIOR_MULTIPLICADOR = 9;

    private static final int LONGITUD_CODIGO_EAN13 = 12;

    private static final int MULTIPLICADOR_PARES_EAN13 = 3;

    /**
     * Retorna el d�gito verificador del c�digo en formato EAN13 especificado.
     * <p>Recibe como argumento el c�digo EAN13 sin el d�gito de verificaci�n,
     * por lo que debe contar con a lo m�s 12 d�gitos. Ver referencia en 
     * <a href=http://es.wikipedia.org/wiki/EAN>
     * http://es.wikipedia.org/wiki/EAN</a>.
     *
     * @param codigoEAN recibe un long de largo m�ximo 12 (en formato
     *          decimal, claro).
     * @return retorna el valor del d�gito verificador en EAN13.
     */
    public static int getDigitoEAN13(long codigoEAN) {
        String digitos = String.valueOf(codigoEAN);
        if (digitos.length() > LONGITUD_CODIGO_EAN13) {
            return -1;
        } else if (digitos.length() < LONGITUD_CODIGO_EAN13) {
            digitos = StringUtils.leftPad(digitos, LONGITUD_CODIGO_EAN13, '0');
        }
        int sumaImpar = 0;
        int sumaPar = 0;
        for (int i = 0; i < digitos.length(); i++) {
            int digito = Integer.parseInt(String.valueOf(digitos.charAt(i)));
            if ((i % 2) == 0) {
                sumaPar += digito;
            } else {
                sumaImpar += digito;
            }
        }
        int sumaTotal = sumaImpar * MULTIPLICADOR_PARES_EAN13 + sumaPar;
        int decenaSuperior = sumaTotal;
        while (decenaSuperior % DIVISOR_DECIMAL != 0) {
            decenaSuperior++;
        }
        int calculado = decenaSuperior - sumaTotal;
        return calculado;
    }

    /**
     * Determina el valor del d�gito verificador para la parte num�rica del RUT
     * especificada. El c�lculo del d�gito se realiza a trav�s del m�dulo 11.
     *
     * @see ValidacionUtil#getDigitoVerificador(long)
     *
     * @param numRUT parte num�rica del RUT, con la que se calcular� el d�gito
     *          verificador. 
     * @return d�gito verificador del RUT especificado, calculado a trav�s del
     *          m�dulo 11. Los valores pueden ser: d�gitos 0 a 9, y car�cter K.
     */
    public static char getDigitoVerificador(long numRUT) {
        if (numRUT < 1) {
            throw new IllegalArgumentException(
                "El numRUT especificado debe ser mayor a 1");
        }
        int m = 0;
        long s = 1;
        long t = numRUT;
        int multiplicador;
        for (; t != 0; t /= DIVISOR_DECIMAL) {
            multiplicador =
                LIMITE_SUPERIOR_MULTIPLICADOR - m++ % DIVISOR_MODULO6;
            s = (s + t % DIVISOR_DECIMAL * multiplicador) % DIVISOR_MOD11;
        }
        char dig = (char) (s + CODIGO_ASCII_0);
        if (s == 0) {
            dig = CODIGO_ASCII_K;
        }
        return dig;
    }

    /**
     * Determina la validez del c�digo de barras en formato EAN13 especificado.
     * Compara el c�digo generado internamente con el �ltimo d�gito del c�digo
     * EAN13 especificado.
     *
     * @see ValidacionUtil#getDigitoEAN13(long)
     *
     * @param codigoEAN13 c�digo EAN13 a validar. La longitud del c�digo
     *          debe ser de a lo m�s 13 d�gitos (en formato decimal).
     * @return <code>true</code> si es que el d�gito calculado internamente
     *          corresponde con el �ltimo d�gito del c�digo especificado.
     * @throws CodigoDeFormularioInvalidoException
     */
    public static boolean isValidoEAN13(long codigoEAN13) {
        int verificador = (int) (codigoEAN13 % DIVISOR_DECIMAL);
        int calculado = getDigitoEAN13(codigoEAN13 / DIVISOR_DECIMAL);
        return verificador == calculado;
    }

    /**
     * Determina la validez del RUT del cliente utilizando la validaci�n
     * mediante el m�dulo 11 del d�gito verificador.
     *
     * @param numRUT Parte num�rica del RUT de a validar.
     * @param digitoVerificador d�gito de verificaci�n del RUT.
     * @return <code>true</code> si es que el d�gito verificador especificado
     *          corresponde con el d�gito verificador calculado internamente.
     *          <code>false</code> en caso contrario.
     */
    public static boolean isValidoRUT(long numRUT, char digitoVerificador) {
        char dig = getDigitoVerificador(numRUT);
        return Character.toUpperCase(digitoVerificador) == dig;
    }

    /**
     * M�todo que valida un email.
     * @param email String de correo
     * @return Si es valido el correo
     */
    public static boolean isEmailValido(String email) {
        EmailValidator oValidador = EmailValidator.getInstance();
        return oValidador.isValid(email);
    }

    /**
     * Constructor privado de clase utilitaria. Es privada para evitar que la
     * clase sea instanciada.
     */
    private ValidacionUtil() {
    }
    
    
    public static boolean isFechaValida(String date, String dateFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        }

        catch (ParseException e) {
            return false;
        }

        if (!sdf.format(testDate).equals(date)) {
            return false;
        }
        return true;

    }
}
