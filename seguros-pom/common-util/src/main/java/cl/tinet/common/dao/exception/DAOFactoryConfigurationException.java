package cl.tinet.common.dao.exception;

import cl.tinet.common.config.AbstractConfigurator;

/**
 * Representa un error de configuración del DAO Factory ocurrido durante
 * la inicialización del mismo.
 *
 * @author Tinet
 * @version 1.0
 * @created 02-04-2009
 */
public class DAOFactoryConfigurationException extends RuntimeException {

    /**
     * Versión de la clase para serialización.
     */
    private static final long serialVersionUID = -6089882352190436077L;

    private String factoryKey;

    private String configClass;

    /**
     * Constructor que recibe el mensaje, la llave del factory y el utilitario
     * de configuración con el problema.
     * @param message mensaje de error.
     * @param factoryKey llave de configuración con error.
     * @param config utilitario de configuración utilizado.
     */
    public DAOFactoryConfigurationException(String message, String factoryKey,
        AbstractConfigurator config) {
        super(message);
        this.factoryKey = factoryKey;
        this.configClass = config.getClass().getName();
    }

    /**
     * Retorna la llave del factory con problemas.
     * @return llave de factory con problemas.
     */
    public String getFactoryKey() {
        return factoryKey;
    }

    /**
     * Retorna la clase del configuración que provocó el problema.
     * @return clase de configuración.
     */
    public String getConfigClass() {
        return configClass;
    }
}
