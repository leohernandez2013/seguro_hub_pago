package cl.tinet.common.dao.jdbc.managed;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Locale;

import javax.sql.DataSource;

import cl.tinet.common.config.AbstractConfigurator;
import cl.tinet.common.config.BaseConfigurable;
import cl.tinet.common.config.Configurable;
import cl.tinet.common.dao.jdbc.connection.ConnectionFactory;

import com.tinet.comun.jndi.ServiceLocator;
import com.tinet.exceptions.system.SystemException;

/**
 * Clase concreta que implementa {@link ConnectionFactory} y que permite a una
 * aplicaci�n obtener la conexi�n a la base de datos en un ambiente gestionado.
 *
 * @author Roberto San Mart�n
 * @version 1.0
 * @created Nov 4, 2008
 */
public class ManagedConnectionFactory extends BaseConfigurable implements
    ConnectionFactory {
    /**
     * Llave del origen de datos predeterminado para la aplicaci�n en el
     * utilitario de configuraci�n.
     */
    public static final String DEFAULT_DATASOURCE_KEY =
        "cl.tinet.common.dao.jdbc.managed.ManagedConnectionFactory.dataSource";

    /**
     * Constructor de la clase que recibe el utilitario de configuraci�n y
     * el datos de localizaci�n que corresponde a la configuraci�n.
     *
     * @param config Utilitario de configuraci�n.
     * @param locale datos de localizaci�n de configuraci�n.
     */
    public ManagedConnectionFactory(AbstractConfigurator config,
        Locale locale) {
        this.configure(config, locale);
    }

    /**
     * Constructor de la clase que recibe el utilitario de configuraci�n y
     * asume la localizaci�n por defecto de la m�quina virtual.
     *
     * @param config Utilitario de configuraci�n.
     */
    public ManagedConnectionFactory(AbstractConfigurator config) {
        this(config, Locale.getDefault());
    }

    /**
     * Constructor de la clase que copia par�metros de configuraci�n desde
     * la clase configurable especificada.
     *
     * @param configurable desde los que se obtienen los datos de configuraci�n.
     */
    public ManagedConnectionFactory(Configurable configurable) {
        this.configure(configurable);
    }

    /**
     * Retorna la conexi�n utilizando el origen de datos predeterminado para la
     * aplicaci�n.
     * 
     * @return conexi�n obtenida desde el origen de datos predeterminado.
     * @throws SQLException en caso de error obteniendo la conexi�n.
     */
    public Connection getConnection() throws SQLException {
        return getConnection(DEFAULT_DATASOURCE_KEY);
    }

    /**
     * Retorna la conexi�n utilizando el origen de datos asociado al JNDI
     * bajo la llave de configuraci�n especificada.
     *
     * @param dsKey llave de b�squeda del JNDI del origen de datos.
     * @return conexi�n obtenida desde el origen de datos especificado.
     * @throws SQLException en caso de error obteniendo la conexi�n.
     */
    public Connection getConnection(String dsKey) throws SQLException {
        return getDataSource(dsKey).getConnection();
    }

    /**
     * Obtiene el origen de datos asociado al JNDI de la llave especificada.
     * @param dsKey llave de b�squeda del JNDI del origen de datos.
     * @return DataSource especificado.
     */
    protected DataSource getDataSource(String dsKey) {
        try {
            String dsJNDI =
                this.getConfigurator().getString(dsKey, this.getLocale());
            return ServiceLocator.singleton().getDataSource(dsJNDI);
            // Se incluye catch de java.lang.Exception por uso de API
            // base que la lanza.
        } catch (Exception e) {
            throw new SystemException(e);
        }
    }
}
