package cl.tinet.common.dao;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.tinet.common.config.AbstractConfigurator;
import cl.tinet.common.config.BaseConfigurable;
import cl.tinet.common.config.Configurable;
import cl.tinet.common.dao.exception.DAOFactoryConfigurationException;
import cl.tinet.common.dao.exception.DAOFactoryLoadException;
import cl.tinet.common.dao.jdbc.BaseDAO;

import com.tinet.exceptions.system.SystemException;

/**
 * DAO Factory abstracto base para las implementaciones concretas de DAOFactory
 * de las aplicaciones.
 *
 * @author Roberto San Mart�n
 * @version 1.0
 * @created 27-Jul-2010 0:49:03
 */
public abstract class AbstractDAOFactory extends BaseConfigurable implements
    Configurable {

    /**
     * Variable de acceso a log.
     */
    private static Log logger = LogFactory.getLog(AbstractDAOFactory.class);

    /**
     * Map de fabricas de DAO del sistema.
     */
    private static Map factoryMap = new HashMap();

    /**
     * Retorna la instancia concreta de {@link AbstractDAOFactory} que
     * corresponde a la clase invocadora, la llave por defecto y seg�n est� 
     * especificado en el utilitario de configuraci�n especificado. En caso de
     * no encontrar la clase concreta del DAOFactory a retornar, se lanza una
     * {@link DAOFactoryConfigurationException} o
     * {@link DAOFactoryLoadException} en caso de error durante la
     * instanciaci�n.
     *
     * @param callerClass Clase invocadora del m�todo. Quien solicita la
     *          instancia concreta de {@link AbstractDAOFactory}.
     * @param defaultKey Llave por defecto de b�squeda en el utilitario de
     *          configuraci�n. En caso de no encontrar un DAOFactory para
     *          la clase invocadora concreta, se utiliza la llave por defecto
     *          especificada.
     * @param config utilitario de configuraci�n donde se buscar�n los datos.
     * @return Instancia concreta de {@link AbstractDAOFactory}.
     */
    public static AbstractDAOFactory loadFactory(final Class callerClass,
        final String defaultKey, final AbstractConfigurator config) {
        return loadFactory(createKey(callerClass), defaultKey, config, Locale
            .getDefault(), callerClass.getClassLoader());
    }

    /**
     * Retorna la instancia concreta de {@link AbstractDAOFactory} que
     * corresponde a la clase invocadora, la llave por defecto y seg�n est� 
     * especificado en el utilitario de configuraci�n especificado. En caso de
     * no encontrar la clase concreta del DAOFactory a retornar, se lanza una
     * {@link DAOFactoryConfigurationException} o
     * {@link DAOFactoryLoadException} en caso de error durante la
     * instanciaci�n.
     *
     * @param callerClass Clase invocadora del m�todo. Quien solicita la
     *          instancia concreta de {@link AbstractDAOFactory}.
     * @param defaultKey Llave por defecto de b�squeda en el utilitario de
     *          configuraci�n. En caso de no encontrar un DAOFactory para
     *          la clase invocadora concreta, se utiliza la llave por defecto
     *          especificada.
     * @param config utilitario de configuraci�n donde se buscar�n los datos.
     * @param locale utilizado para identificar la configuraci�n
     *          correspondiente.
     * @return Instancia concreta de {@link AbstractDAOFactory}.
     */
    public static AbstractDAOFactory loadFactory(final Class callerClass,
        final String defaultKey, final AbstractConfigurator config,
        final Locale locale) {
        return loadFactory(createKey(callerClass), defaultKey, config, locale,
            callerClass.getClassLoader());
    }

    /**
     * Retorna la instancia concreta de {@link AbstractDAOFactory} que
     * corresponde a la clase invocadora, la llave por defecto y seg�n est� 
     * especificado en el utilitario de configuraci�n especificado. En caso de
     * no encontrar la clase concreta del DAOFactory a retornar, se lanza una
     * {@link DAOFactoryConfigurationException} o
     * {@link DAOFactoryLoadException} en caso de error durante la
     * instanciaci�n.
     *
     * @param factoryKey Llave de b�squeda del nombre de la clase concreta que
     *          ser� retornada como {@link AbstractDAOFactory}.
     * @param defaultKey Llave por defecto de b�squeda en el utilitario de
     *          configuraci�n. En caso de no encontrar un DAOFactory para
     *          la clase invocadora concreta, se utiliza la llave por defecto
     *          especificada.
     * @param config utilitario de configuraci�n donde se buscar�n los datos.
     * @param loader cargador de clases para b�squeda de la clase.
     * @return Instancia concreta de {@link AbstractDAOFactory}.
     */
    public static AbstractDAOFactory loadFactory(final String factoryKey,
        final String defaultKey, final AbstractConfigurator config,
        final ClassLoader loader) {
        return loadFactory(factoryKey, defaultKey, config, Locale.getDefault(),
            loader);
    }

    /**
     * Retorna la instancia concreta de {@link AbstractDAOFactory} que
     * corresponde a la clase invocadora, la llave por defecto y seg�n est� 
     * especificado en el utilitario de configuraci�n especificado. En caso de
     * no encontrar la clase concreta del DAOFactory a retornar, se lanza una
     * {@link DAOFactoryConfigurationException} o
     * {@link DAOFactoryLoadException} en caso de error durante la
     * instanciaci�n.
     *
     * @param factoryKey Llave de b�squeda del nombre de la clase concreta que
     *          ser� retornada como {@link AbstractDAOFactory}.
     * @param defaultKey Llave por defecto de b�squeda en el utilitario de
     *          configuraci�n. En caso de no encontrar un DAOFactory para
     *          la clase invocadora concreta, se utiliza la llave por defecto
     *          especificada.
     * @param config utilitario de configuraci�n donde se buscar�n los datos.
     * @param locale utilizado para identificar la configuraci�n
     *          correspondiente.
     * @param loader cargador de clases para b�squeda de la clase.
     * @return Instancia concreta de {@link AbstractDAOFactory}.
     */
    public static AbstractDAOFactory loadFactory(final String factoryKey,
        final String defaultKey, final AbstractConfigurator config,
        final Locale locale, final ClassLoader loader) {
        logger.trace("Iniciando m�todo loadFactory.");
        if (factoryKey == null) {
            logger.debug("No especific� llave de b�squeda. Lanzando NPE.");
            throw new NullPointerException(
                "Debe especificar la llave del DAO Factory.");
        }
        if (locale == null) {
            logger.debug("No especific� locale de b�squeda. Lanzando NPE.");
            throw new NullPointerException(
                "Debe especificar el locale de b�squeda.");
        }
        String internalKey =
            createKey(factoryKey, locale, config.getClass().getName());
        if (logger.isTraceEnabled()) {
            logger.trace("Llave de b�squeda: " + factoryKey);
            logger
                .trace("Llave interna de b�squeda de Factory: " + internalKey);
        }
        if (!factoryMap.containsKey(internalKey)) {
            createDAOFactory(factoryKey, defaultKey, config, locale, loader,
                internalKey);
        }
        logger.trace("Retornando instancia de DAO Factory.");
        return (AbstractDAOFactory) factoryMap.get(internalKey);
    }

    /**
     * Crea la instancia concreta de {@link AbstractDAOFactory} que
     * corresponde a la clase invocadora, la llave por defecto y seg�n est� 
     * especificado en el utilitario de configuraci�n especificado. En caso de
     * no encontrar la clase concreta del DAOFactory a retornar, se lanza una
     * {@link DAOFactoryConfigurationException} o
     * {@link DAOFactoryLoadException} en caso de error durante la
     * instanciaci�n.
     * La instancia creada es insertada en el Map de instancias del DAO Factory.
     *
     * @param factoryKey Llave de b�squeda del nombre de la clase concreta que
     *          ser� retornada como {@link AbstractDAOFactory}.
     * @param defaultKey Llave por defecto de b�squeda en el utilitario de
     *          configuraci�n. En caso de no encontrar un DAOFactory para
     *          la clase invocadora concreta, se utiliza la llave por defecto
     *          especificada.
     * @param config utilitario de configuraci�n donde se buscar�n los datos.
     * @param locale utilizado para identificar la configuraci�n
     *          correspondiente.
     * @param loader cargador de clases para b�squeda de la clase.
     * @param internalKey llave interna de b�squeda de la instancia en Map
     *          de factory de DAOs.
     */
    protected static void createDAOFactory(final String factoryKey,
        final String defaultKey, final AbstractConfigurator config,
        final Locale locale, final ClassLoader loader, String internalKey) {
        logger.info("No se encontr� el DAO Factory asociado llave interna "
            + "de b�squeda, inicializando.");
        synchronized (factoryMap) {
            if (!factoryMap.containsKey(internalKey)) {
                String className = config.getString(factoryKey, locale);
                if (className == null) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("No se encontr� configuracion para "
                            + factoryKey + ". Intentando con " + defaultKey);
                    }
                    className = config.getString(defaultKey, locale);
                }
                if (className == null) {
                    logger.debug("Nombre de clase factory no especificado, "
                        + "lanzando DAOFactoryConfigurationException.");
                    throw new DAOFactoryConfigurationException(
                        "No se encontr� la llave de configuracion.",
                        factoryKey, config);
                }
                try {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Nombre de clase factory encontrado: "
                            + className);
                    }
                    logger.trace("Intentando cargar clase.");
                    if (loader == null) {
                        logger.debug("No se especific� el classloader de "
                            + "b�squeda. Lanzazndo NPE.");
                        throw new NullPointerException(
                            "Debe especificar un classloader no nulo.");
                    }
                    Class factoryClass = loader.loadClass(className);
                    logger.trace("Carga exitosa. Intentando instanciar clase.");
                    AbstractDAOFactory factory =
                        (AbstractDAOFactory) factoryClass.newInstance();
                    factory.configure(config, locale);
                    factoryMap.put(internalKey, factory);
                    logger.trace("Instanciaci�n exitosa. Retornando.");
                } catch (ClassNotFoundException cnfe) {
                    logger.info("Clase DAOFactory no encontrada. "
                        + "Lanzando error.");
                    throw new DAOFactoryLoadException(className, cnfe);
                } catch (InstantiationException ie) {
                    logger.info("Clase DAOFactory no se logr� instanciar. "
                        + "Lanzando error.");
                    throw new DAOFactoryLoadException(className, ie);
                } catch (IllegalAccessException iae) {
                    logger
                        .info("Sin acceso a constructor de clase DAOFactory. "
                            + "Lanzando error.");
                    throw new DAOFactoryLoadException(className, iae);
                }
            }
        }
    }

    /**
     * Retorna la llave interna de b�squeda del DAOFactory en el Map de
     * fabricas de DAO.
     *
     * @param factoryKey llave del factory buscado.
     * @param locale informaci�n de localizaci�n de la configuraci�n a utilizar.
     * @param configClassName Nombre de la clase de de configuraci�n.
     * @return llave interna generada en base a los par�metros especificados.
     */
    protected static String createKey(final String factoryKey,
        final Locale locale, String configClassName) {
        return factoryKey + "_" + locale.toString() + "_" + configClassName;
    }

    /**
     * Crea una llave de b�squeda para la clase especificada. En caso de que
     * la llave especificada sea null, lanza una {@link NullPointerException}.
     *
     * @param caller Clase que realiza la solicitud del DAOFactory.
     * @return llave generada en base a la clase invocadora.
     */
    protected static String createKey(final Class caller) {
        if (caller == null) {
            throw new NullPointerException(
                "Debe especificar un caller no nulo.");
        }
        return caller.getName() + ".daoFactory";
    }

    /**
     * Retorna una nueva instancia de {@link BaseDAO} utilizando la clase DAO
     * concreta especificada como argumento. La instancia es inicializada antes
     * de ser retornada.
     * 
     * @param interfazDAO Interfaz DAO que se desea retornar.
     * @param claseDAO Clase DAO concreta que implementa la interfaz
     *          especificada.
     * @return Instancia concreta de la clase DAO especificada inicialzada.
     */
    public BaseDAO getDAO(final Class interfazDAO, final Class claseDAO) {
        try {
            BaseDAO dao = (BaseDAO) claseDAO.newInstance();
            dao.configure(this);
            dao.setDAOInterface(interfazDAO);
            return dao;
        } catch (InstantiationException ie) {
            throw new SystemException(ie);
        } catch (IllegalAccessException iae) {
            throw new SystemException(iae);
        }
    }
}
