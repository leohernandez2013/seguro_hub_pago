package cl.tinet.common.dao.jdbc.configurable;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import cl.tinet.common.config.AbstractConfigurator;
import cl.tinet.common.dao.jdbc.BaseDAOJDBC;
import cl.tinet.common.dao.jdbc.connection.ConnectionFactory;
import cl.tinet.common.dao.jdbc.managed.ManagedConnectionFactory;
import cl.tinet.common.dao.jdbc.standalone.StandaloneConnectionFactory;

import com.tinet.exceptions.system.SystemException;

/**
 * Clase abstracta DAO JDBC que deben implementar las clases que deseen tener
 * libertad de conectarse a la base de datos en funci�n de la configuraci�n
 * concreta de la instancia del DAO.
 *
 * @author Tinet
 * @version 1.0
 * @created 27-Jul-2010 0:49:05
 */
public abstract class ConfigurableBaseDAOJDBC extends BaseDAOJDBC {

    /**
     * Llave de configuraci�n del flag de selecci�n de tipo de conexi�n.
     */
    public static final String CF_IMPL_KEY =
        "cl.unimarc.common.dao.jdbc.configurable"
        + ".ConfigurableBaseDAOJDBC.connectionFactory.cfTypeFlag";

    /**
     * Map est�tico con las f�bricas de conexiones cargadas en el sistema.
     */
    private static Map cfMap = new HashMap();

    /**
     * Retorna la instancia de {@link ConnectionFactory} que corresponde
     * retornar seg�n configuraci�n.
     * @return {@link ConnectionFactory} valido seg�n configuraci�n.
     */
    public ConnectionFactory getConnectionFactory() {
        String key = createKey();
        if (!cfMap.containsKey(key)) {
            initConnectionFactory(key);
        }
        return (ConnectionFactory) cfMap.get(key);
    }

    /**
     * Construye la llave bajo la cual se debe encontrar el connection factory.
     * @return nueva llave de registro del connection factory.
     */
    public String createKey() {
        return this.getConfigurator().getClass().getName() + "_"
            + this.getLocale();
    }

    /**
     * Inicializa el {@link ConnectionFactory} utilizando la configuraci�n
     * actual de la instancia del DAO actual.
     * @param key llave de registro del connection factory.
     */
    private void initConnectionFactory(String key) {
        synchronized (cfMap) {
            if (!cfMap.containsKey(key)) {
                AbstractConfigurator config = this.getConfigurator();
                Locale locale = this.getLocale();
                String cfTypeFlag = config.getString(CF_IMPL_KEY, locale);
                if ("MANAGED".equalsIgnoreCase(cfTypeFlag)) {
                    cfMap
                        .put(key, new ManagedConnectionFactory(config, locale));
                } else if ("STANDALONE".equalsIgnoreCase(cfTypeFlag)) {
                    cfMap.put(key, new StandaloneConnectionFactory(config,
                        locale));
                }
            }
        }
        throw new SystemException(new Exception(
            "Tipo de connectionFactory no v�lido."));
    }
}
