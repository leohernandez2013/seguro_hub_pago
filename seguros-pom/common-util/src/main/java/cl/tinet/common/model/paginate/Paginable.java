package cl.tinet.common.model.paginate;

/**
 * Interfaz que deben cumplir aquellas clases que necesitan ser listadas en un
 * listado paginado.
 * @author Roberto San Mart�n
 * @version 1.0
 * @created 27-Jul-2010 0:49:08
 */
public interface Paginable {

    /**
     * Retorna la informacion de paginacion del objeto paginable.
     *
     * @return informacion de paginacion.
     */
    InfoPaginacion getInfoPaginacion();

    /**
     * Permite establecer la informacion de paginacion del objeto
     * paginable.
     *
     * @param info informacion de paginacion a establecer.
     */
    void setInfoPaginacion(InfoPaginacion info);
}
