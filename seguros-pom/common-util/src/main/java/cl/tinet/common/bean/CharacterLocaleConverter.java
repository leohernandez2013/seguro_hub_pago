package cl.tinet.common.bean;

import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.locale.LocaleConverter;

/**
 * Convertidor de datos de tipo <code>char</code>.
 * <br/>
 * @author Roberto San Mart�n
 * @version 1.0
 * @created Aug 21, 2010
 */
public class CharacterLocaleConverter implements LocaleConverter {

    /**
     * Convierte a char el valor especificado. El patr�n especificado
     * no es utilizado.
     * <br/>
     * @param type tipo de dato esperado.
     * @param value valor del dato original.
     * @param pattern patron de transformaci�n no utilizado.
     * @return objeto transformado.
     */
    public Object convert(Class type, Object value, String pattern) {
        if (value == null) {
            return new Character((char) 0);
        }
        String message = "No se puede convertir '" + value + "' a " + type;
        try {
            if (type.equals(Character.class)) {
                return new Character(value.toString().charAt(0));
            }
        } catch (ClassCastException cce) {
            throw new ConversionException(message, cce);
        }
        throw new ConversionException(message);
    }

    /**
     * Convierte a char el valor especificado.
     * <br/>
     * @param type tipo de dato esperado.
     * @param value valor del dato original.
     * @return objeto transformado.
     */
    public Object convert(Class type, Object value) {
        return convert(type, value, null);
    }
}
