package cl.tinet.common.util.paginate;

import java.util.Locale;

import cl.tinet.common.config.AbstractConfigurator;
import cl.tinet.common.model.paginate.InfoPaginacion;

/**
 * Utilitario que permite instanciar el objeto {@link InfoPaginacion} utilizando
 * la información de configuración de la aplicación.
 *
 * @author Tinet
 * @version 1.0
 * @created 27-Jul-2010 0:49:07
 */
public class InfoPaginacionUtil {

    /**
     * Llave de configuración para búsqueda de cantidad de registros por
     * pagina por defecto.
     */
    public static final String PAGE_INFO_RPP_KEY =
        "cl.unimarc.common.util.paginate.InfoPaginacionUtil.PAGE_INFO_RPP";

    /**
     * Crear una nueva instancia del objeto {@link InfoPaginacion} utilizando
     * los datos de configuración del utilitario especificado.
     *
     * @param config utilitario de configuración.
     * @return instancia de {@link InfoPaginacion} inicializada.
     */
    public static InfoPaginacion createDefaultPageInfo(
        AbstractConfigurator config) {
        return createDefaultPageInfo(config, Locale.getDefault());
    }

    /**
     * Crear una nueva instancia del objeto {@link InfoPaginacion} utilizando
     * los datos de configuración del utilitario especificado y el locale de
     * configuración especificado.
     *
     * @param config utilitario de configuración.
     * @param locale localización de configuración.
     * @return instancia de {@link InfoPaginacion} inicializada.
     */
    public static InfoPaginacion createDefaultPageInfo(
        AbstractConfigurator config, Locale locale) {
        if (config == null) {
            throw new NullPointerException(
                "Debe especificar un configurador válido.");
        }
        return new InfoPaginacion(1, 0, config
            .getInt(PAGE_INFO_RPP_KEY, locale));
    }

    /**
     * Constructor privado para evitar instanciación.
     */
    private InfoPaginacionUtil() {
    }
}
