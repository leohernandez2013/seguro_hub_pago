package cl.tinet.common.dao.jdbc.connection;

import java.sql.Connection;
import java.sql.SQLException;

import cl.tinet.common.config.Configurable;

/**
 * Interfaz de fabrica de conexiones. Permite obtener una conexi�n del medio en
 * donde se encuentre corriendo la aplicaci�n.
 *
 * @author Tinet
 * @version 1.0
 * @created 27-Jul-2010 0:49:05
 */
public interface ConnectionFactory extends Configurable {

    /**
     * Retorna la conexi�n utilizando los datos de configuraci�n por defecto.
     * @return conexi�n a la base de datos.
     * @throws SQLException en caso de no conseguir obtener la conexi�n.
     */
    Connection getConnection() throws SQLException;

    /**
     * Retorna la conexi�n asociada a la llave de configuraci�n especificada.
     * @param connectionKey llave de conexi�n a utilizar.
     * @return conexi�n a la base de datos.
     * @throws SQLException en caso de no lograr crear la conexi�n.
     */
    Connection getConnection(String connectionKey) throws SQLException;
}
