package cl.tinet.common.dao.jdbc;

import cl.tinet.common.config.Configurable;
import cl.tinet.common.util.resource.Closeable;

/**
 * Interfaz que debe cumplir cualquier clase de acceso a datos.
 * 
 * @author Tinet
 * @version 1.0
 * @created 27-Jul-2010 0:49:04
 */
public interface BaseDAO extends Configurable, Closeable {

    /**
     * Permite a la instancia del DAO cerrar todos los recursos utilizados
     * durante su vida. Este m�todo debe ser llamado antes de descartar la
     * instancia con el fin de liberar los recursos utilizados.
     */
    void close();

    /**
     * Permite establecer la interfaz DAO que cumple la clase concreta.
     *
     * @param interfazDAO interfaz cumplida por la clase concreta.
     */
    void setDAOInterface(Class interfazDAO);

    /**
     * Retorna la interfaz DAO cumplida por la clase concreta.
     *
     * @return interfaz DAO cumplida.
     */
    Class getDAOInterface();
}
