package cl.tinet.common.dao.jdbc;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cl.tinet.common.config.BaseConfigurable;
import cl.tinet.common.dao.jdbc.connection.ConnectionFactory;

import com.tinet.exceptions.system.SystemException;

/**
 * <p>
 * Clase DAO base abstracta, que implementa los servicios requeridos por la
 * interfaz {@link BaseDAO} con el fin de servir de plataforma para cualquier
 * implementaci�n especial de DAO JDBC. Provee m�todos para la obtenci�n de la
 * conexi�n, para el cierre de sentencias y cursores, as� como para realizar
 * inserciones, b�squedas, actualizaciones y consultas utilizando la API JDBC.
 * </p>
 * <p>
 * Permite la integraci�n con el esquema de configuraci�n especial de
 * aplicaciones (extiende de la clase {@link BaseConfigurable}).
 * </p>
 * 
 * @author Roberto San Mart�n
 * @version 1.0
 * @created 27-Jul-2010 0:49:04
 */
public abstract class BaseDAOJDBC extends BaseConfigurable implements BaseDAO {

    /**
     * Referencia para acceso al log de la aplicaci�n.
     */
    private static Log logger = LogFactory.getLog(BaseDAOJDBC.class);

    /**
     * {@link Map} de conexiones abiertas durante la vida de la instancia
     * concreta del DAO.
     */
    private Map connectionsMap;

    /**
     * Clase que representa la interfaz DAO implementada por la clase concreta.
     */
    private Class daoInterface;

    /**
     * Constructor sin argumentos de la clase. Inicializa el {@link Map} de
     * conexiones como un nuevo {@link HashMap} vac�o.
     */
    public BaseDAOJDBC() {
        logger.debug("Inicializando Map de conexiones.");
        this.connectionsMap = new HashMap();
    }

    /**
     * Cierra todos los recursos abiertos durante el tiempo de vida del DAO. Es
     * responsabilidad del invocador el realizar la llamada al m�todo close al
     * finalizar uso del DAO.
     * 
     * @see cl.unimarc.common.dao.BaseDAO#close()
     */
    public void close() {
        Collection connections = this.connectionsMap.values();
        for (Iterator iterator = connections.iterator(); iterator.hasNext();) {
            Connection connection = (Connection) iterator.next();
            this.close(connection);
        }
        this.connectionsMap.clear();
    }

    /**
     * Ejecuta la consulta SQL especificada con los argumentos indicados y
     * retorna una instancia de la clase especificada llena con los datos
     * retornados por la consulta. Utiliza la conexi�n por defecto para el DAO
     * concreto.
     * 
     * @param resultClass
     *            Clase del objeto retornado.
     * @param sql
     *            consulta SQL a ejecutar.
     * @param arguments
     *            arreglo de argumentos a la consulta.
     * @return instancia encontrada. <code>null</code> en caso de que la
     *         consulta no entregue resultados.
     */
    public Object find(Class resultClass, String sql, Object[] arguments) {
        return this.find(getConnection(), resultClass, sql, arguments);
    }

    /**
     * Ejecuta la consulta SQL especificada con los argumentos indicados y
     * retorna una instancia de la clase especificada llena con los datos
     * retornados por la consulta. Utiliza la conexi�n especificada para
     * ejecutar la consulta.
     * 
     * @param connection
     *            conexi�n a la base de datos que se utilizar�.
     * @param resultClass
     *            Clase del objeto retornado.
     * @param sql
     *            consulta SQL a ejecutar.
     * @param arguments
     *            arreglo de argumentos a la consulta.
     * @return instancia encontrada. <code>null</code> en caso de que la
     *         consulta no entregue resultados.
     */
    public Object find(Connection connection, Class resultClass, String sql,
        Object[] arguments) {
        List result = this.query(connection, resultClass, sql, arguments);
        if (result.size() == 0) {
            return null;
        }
        if (result.size() > 1) {
            logger.warn("Una consulta por un registro retorn� m�s de un "
                + "elemento. Se recuperar� solo el primer elemento.");
            if (logger.isDebugEnabled()) {
                logger.debug("Una consulta por un registro retorn� m�s "
                    + "de un elemento. Consulta: " + sql, new Exception(
                        "Se esperaba solo un resultado."));
            }
        }
        return result.get(0);
    }

    /**
     * <p>
     * Permite obtener una nueva conexi�n utilizando la configuraci�n por
     * defecto del DAO concreto que la solicita. En caso de no encontrar la
     * conexi�n para el DAO particular, intenta recuperar la conexi�n
     * establecida por defecto para la aplicaci�n completa.
     * </p>
     * <p>
     * En caso de no lograr crear la conexi�n se lanza una excepci�n de tipo
     * {@link SystemException}.
     * </p>
     * 
     * @return Conexi�n por defecto para el DAO concreto.
     */
    public Connection getConnection() {
        String defaultCofigurationKey = createDefaultConfigurationKey();
        if (!this.connectionsMap.containsKey(defaultCofigurationKey)) {
            Connection connection = null;
            ConnectionFactory cf = getConnectionFactory();
            try {
                connection = cf.getConnection(defaultCofigurationKey);
            } catch (SQLException sqle) {
                logger.debug("No se encontr� conexi�n bajo "
                    + defaultCofigurationKey);
            } catch (SystemException se) {
                logger.debug("No se encontr� conexi�n bajo "
                    + defaultCofigurationKey);
            }
            if (connection == null) {
                logger.debug("Intentando obtener conexi�n para aplicaci�n.");
                try {
                    connection = cf.getConnection();
                } catch (SQLException sqle) {
                    logger.error("No se obtuvo la conexi�n. Lanzando error.");
                    throw new SystemException(sqle);
                }
            }
            this.connectionsMap.put(defaultCofigurationKey, connection);
        }
        return (Connection) this.connectionsMap.get(defaultCofigurationKey);
    }

    /**
     * <p>
     * Permite obtener una nueva conexi�n a la base de datos utilizando la llave
     * de configuraci�n especificada. En caso de que no exista configuraci�n
     * asociada a la llave de configuraci�n especificada, se busca la
     * configuraci�n predeterminada para el DAO solicitante. En caso de que la
     * configuraci�n predeterminada no exista, se utiliza la configuraci�n
     * establecida para la aplicaci�n completa.
     * </p>
     * <p>
     * En caso de no lograr crear la conexi�n, se lanza una
     * {@link SystemException} con la causa del problema.
     * </p>
     * 
     * @param configurationKey
     *            llave de configuraci�n especial de la aplicaci�n.
     * @return nueva conexi�n a la base de datos utilizando la configuraci�n
     *         especificada.
     */
    public Connection getConnection(final String configurationKey) {
        if (!connectionsMap.containsKey(configurationKey)) {
            if (logger.isDebugEnabled()) {
                logger
                    .debug("Obteniendo conexi�n especial: configurationKey = "
                        + configurationKey);
            }
            Connection connection = null;
            ConnectionFactory cf = getConnectionFactory();
            try {
                connection = cf.getConnection(configurationKey);
            } catch (SQLException sqle) {
                logger.debug("No se logr� obtener conexi�n epsecial.");
                connection = getConnection();
            }
            this.connectionsMap.put(configurationKey, connection);
        }
        return (Connection) connectionsMap.get(configurationKey);
    }

    /**
     * <p>
     * M�todo abstracto que permite obtener la fabrica de conexiones para el DAO
     * concreto. Este m�todo debe ser implementado en funci�n de las necesidades
     * y capacidades del ambiente de ejecuci�n y el DAO concreto.
     * </p>
     * 
     * @return Fabrica de conexiones para el DAO concreto.
     */
    public abstract ConnectionFactory getConnectionFactory();

    /**
     * Permite retornar la clase de la interfaz DAO implementada por la clase
     * DAO concreta instanciada.
     * 
     * @return Clase que representa la interfaz DAO implementada por la clase
     *         concreta.
     */
    public Class getDAOInterface() {
        return this.daoInterface;
    }

    /**
     * Realiza la secuencia SQL especificada retornando el identificador
     * generado como resultado de la ejecuci�n. Utiliza la conexi�n especificada
     * para ejecutar la sentencia.
     * 
     * @param connection
     *            conexi�n a utilizar.
     * @param sql
     *            sentencia SQL a ejecutar.
     * @param arguments
     *            argumentos de la sentencia.
     * @return identificador generado.
     */
    public long insert(Connection connection, String sql, Object[] arguments) {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            logger.trace("Inicializando sentencia SQL.");
            String generatedColumn = getGeneratedColumn(sql);
            if (generatedColumn != null) {
                statement =
                    connection.prepareStatement(sql,
                        new String[] { generatedColumn });
            } else {
                statement =
                    connection.prepareStatement(sql,
                        Statement.RETURN_GENERATED_KEYS);
            }
            logger.trace("Estableciendo los argumentos.");
            this.setArguments(statement, arguments);
            logger.trace("Ejecutando sentencia.");
            long key = -1;
            statement.execute();
            logger.trace("Recuperando llaves autogeneradas.");
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                logger.trace("Llave autogenerada encontrada.");
                key = resultSet.getLong(1);
            }
            return key;
        } catch (SQLException sqle) {
            this.handleException("Error realizando operacion insert.", sqle,
                sql, arguments);
            throw new SystemException(sqle);
        } finally {
            this.close(statement, resultSet);
            statement = null;
            resultSet = null;
        }
    }

    /**
     * Realiza la secuencia SQL especificada retornando el identificador
     * generado como resultado de la ejecuci�n. Utiliza la conexi�n
     * predeterminada para ejecutar la sentencia.
     * 
     * @param sql
     *            sentencia SQL a ejecutar.
     * @param arguments
     *            argumentos de la sentencia.
     * @return identificador generado.
     */
    public long insert(String sql, Object[] arguments) {
        return this.insert(getConnection(), sql, arguments);
    }
    
    /**
     * Realiza la secuencia SQL para cargar rut
     * 
     * @param sql
     *            sentencia SQL a ejecutar.
     * @param arguments
     *            argumentos de la sentencia.
     * @return identificador generado.
     */
    public int insertrut(String sql, Object[] arguments) {
        return this.insertrut(getConnection(), sql, arguments);
    }
   
    /**
     * Realiza la secuencia SQL especificada retornando el identificador
     * generado como resultado de la ejecuci�n. Utiliza la conexi�n especificada
     * para ejecutar la sentencia.
     * 
     * @param connection
     *            conexi�n a utilizar.
     * @param sql
     *            sentencia SQL a ejecutar.
     * @param arguments
     *            argumentos de la sentencia.
     * @return identificador generado.
     */
    private int insertrut(Connection connection, String sql, Object[] arguments) {
    	PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            logger.trace("Inicializando sentencia SQL.");
            String generatedColumn = getGeneratedColumn(sql);
            if (generatedColumn != null) {
                statement =
                    connection.prepareStatement(sql,
                        new String[] { generatedColumn });
            } else {
                statement =
                    connection.prepareStatement(sql,
                        Statement.RETURN_GENERATED_KEYS);
            }
            logger.trace("Estableciendo los argumentos.");
            this.setArguments(statement, arguments);
            logger.trace("Ejecutando sentencia.");
            int key = -1;
            statement.execute();
            logger.trace("Recuperando llaves autogeneradas.");
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                logger.trace("Llave autogenerada encontrada.");
                key = 1;
            }
            return key;
        } catch (SQLException sqle) {
            this.handleException("Error realizando operacion insert.", sqle,
                sql, arguments);
            throw new SystemException(sqle);
        } finally {
            this.close(statement, resultSet);
            statement = null;
            resultSet = null;
        }
	}

	/**
     * Metodo para insertar sin obtener el id generado.
     * 
     * @param connection
     *            conexi�n
     * @param sql
     *            sql ingresado
     * @param arguments
     *            argumentos
     */
    public void insertWithoutGeneratedKey(Connection connection, String sql,
        Object[] arguments) {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            logger.trace("Inicializando sentencia SQL.");
            statement =
                connection.prepareStatement(sql,
                    Statement.RETURN_GENERATED_KEYS);
            logger.trace("Estableciendo los argumentos.");
            this.setArguments(statement, arguments);
            logger.trace("Ejecutando sentencia.");
            statement.execute();
        } catch (SQLException sqle) {
            this.handleException("Error realizando operacion insert.", sqle,
                sql, arguments);
            throw new SystemException(sqle);
        } finally {
            this.close(statement, resultSet);
        }
    }

    /**
     * Metodo para insertar sin obtener el id generado.
     * 
     * @param sql
     *            sentencia sql
     * @param arguments
     *            argumentos
     */
    public void insertWithoutGeneratedKey(String sql, Object[] arguments) {
        this.insertWithoutGeneratedKey(getConnection(), sql, arguments);
    }

    /**
     * Permite realizar una consulta SQL con los argumentos especificados
     * utilizando la conexi�n del DAO por defecto. Retorna un List con objetos
     * de la clase especificada por resultClass.
     * 
     * @param resultClass
     *            clase de los objetos retornados en el listado.
     * @param sql
     *            sentencia SQL a ejecutar.
     * @param arguments
     *            arreglo de argumentos a la consulta.
     * @return List de instancias de resultClass con los valores de la consulta.
     */
    public List query(Class resultClass, String sql, Object[] arguments) {
        return this.query(getConnection(), resultClass, sql, arguments);
    }

    /**
     * Permite realizar una consulta SQL con los argumentos especificados
     * utilizando la conexi�n especificada. Retorna un List con objetos de la
     * clase especificada por resultClass.
     * 
     * @param connection
     *            conexi�n a la base de datos a utilizar.
     * @param resultClass
     *            clase de los objetos retornados en el listado.
     * @param sql
     *            sentencia SQL a ejecutar.
     * @param arguments
     *            arreglo de argumentos a la consulta.
     * @return List de instancias de resultClass con los valores de la consulta.
     */
    public List query(Connection connection, Class resultClass, String sql,
        Object[] arguments) {
        if (connection == null) {
            throw new NullPointerException(
                "La conexi�n especificada no debe ser null.");
        }
        if (resultClass == null) {
            throw new NullPointerException(
                "La clase de resultado especificada no debe ser null.");
        }
        if (sql == null) {
            throw new NullPointerException(
                "La sentencia SQL a ejecutar no debe ser null.");
        }
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            logger.trace("Inicializando sentencia SQL.");
            statement = connection.prepareStatement(sql);
            logger.trace("Estableciendo los argumentos.");
            this.setArguments(statement, arguments);
            logger.trace("Ejecutando consulta.");
            resultSet = statement.executeQuery();
            logger.trace("Construyendo respuesta.");
            List response = this.buildResponse(resultSet, resultClass);
            logger.trace("Retornando resultados.");
            return response;
        } catch (SQLException sqle) {
            this.handleException("Error ejecutando consulta SQL.", sqle, sql,
                arguments);
            throw new SystemException(sqle);
        } finally {
            this.close(statement, resultSet);
            statement = null;
            resultSet = null;
        }
    }
    
    
    
    public int queryCaptcha( String sql, Object[] arguments) {
    	
    	Connection connection= getConnection();
            if (connection == null) {
                throw new NullPointerException(
                    "La conexi�n especificada no debe ser null.");
            }
            if (sql == null) {
                throw new NullPointerException(
                    "La sentencia SQL a ejecutar no debe ser null.");
            }
            PreparedStatement statement = null;
            ResultSet resultSet = null;
            try {
                logger.trace("Inicializando sentencia SQL.");
                statement = connection.prepareStatement(sql);
                logger.trace("Estableciendo los argumentos.");
                this.setArguments(statement, arguments);
                logger.trace("Ejecutando consulta.");
                resultSet = statement.executeQuery();
                logger.trace("Construyendo respuesta.");
                logger.trace("Retornando resultados.");
                if(resultSet.next())
                	return resultSet.getInt("existe");
                return 0;
            } catch (SQLException sqle) {
                this.handleException("Error ejecutando consulta SQL.", sqle, sql,
                    arguments);
                throw new SystemException(sqle);
            } finally {
                this.close(statement, resultSet);
                statement = null;
                resultSet = null;
            }
        }

    /**
     * Permite establecer la interfaz DAO asociada al DAO concreto.
     * 
     * @param daoInt
     *            interfaz DAO implementada por el DAO concreto.
     */
    public void setDAOInterface(final Class daoInt) {
        if (daoInt == null) {
            logger.error("Interfaz DAO no debe ser null. Lanzando excepci�n.");
            throw new NullPointerException(
                "La interfaz DAO especificada no debe ser null.");
        }
        this.daoInterface = daoInt;
    }

    /**
     * Permite realizar la sentencia SQL especificada indicando la cantidad de
     * registros afectados por la misma. Utiliza la conexi�n especificada.
     * 
     * @param connection
     *            conexi�n a la base de datos que se utilizar�.
     * @param sql
     *            sentencia SQL a ejecutar.
     * @param arguments
     *            arreglo de argumentos de la sentencia.
     * @return cantidad de filas afectadas.
     */
    public int update(Connection connection, String sql, Object[] arguments) {
        PreparedStatement statement = null;
        try {
            logger.trace("Inicializando sentencia SQL.");
            statement = connection.prepareStatement(sql);
            logger.trace("Estableciendo los argumentos.");
            this.setArguments(statement, arguments);
            logger.trace("Ejecutando sentencia.");
            return statement.executeUpdate();
        } catch (SQLException sqle) {
            this.handleException("No se logr� ejecutar la sentencia.", sqle,
                sql, arguments);
            throw new SystemException(sqle);
        } finally {
            this.close(statement);
            statement = null;
        }
    }

    /**
     * Permite realizar la sentencia SQL especificada indicando la cantidad de
     * registros afectados por la misma. Utiliza la conexi�n por defecto para el
     * DAO concreto.
     * 
     * @param sql
     *            sentencia SQL a ejecutar.
     * @param arguments
     *            arreglo de argumentos de la sentencia.
     * @return cantidad de filas afectadas.
     */
    public int update(String sql, Object[] arguments) {
        return this.update(getConnection(), sql, arguments);
    }

    /**
     * Permite construir la respuesta en base a los datos recuperados en el
     * ResultSet especificado.
     * 
     * @param resultSet
     *            ResultSet con los datos retornados por la consulta.
     * @param resultClass
     *            Clase de las instancias que llenaran el listado de elementos
     *            retornados.
     * @return List con las instancias llenas con los datos del ResultSet.
     * @throws SQLException
     *             en caso de error durante el procesamiento.
     */
    protected List buildResponse(ResultSet resultSet, Class resultClass)
        throws SQLException {
        logger.trace("Manejando resultados...");
        return (List) getResultSetHandler(resultClass).handle(resultSet);
    }

    /**
     * Permite cerrar la conexi�n especificada. En caso de ocurrir alg�n
     * problema durante el cierre de la conexi�n, el problema se registra en el
     * log de la aplicaci�n, y se continua el flujo normalmente.
     * 
     * @param connection
     *            referencia a la conexi�n que se desea cerrar.
     */
    protected void close(final Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException sqle) {
                logger.warn(
                    "La conexi�n a la base de datos no logr� cerrarse.", sqle);
            }
        }
    }

    /**
     * Cierra el ResultSet especificado.
     * 
     * @param resultSet
     *            resultSet a cerrar.
     */
    protected void close(ResultSet resultSet) {
        if (resultSet != null) {
            logger.trace("Cerrando cursor.");
            try {
                resultSet.close();
            } catch (SQLException sqle) {
                logger.warn("El cursor especificado no logr� cerrarse.", sqle);
            }
        }
    }

    /**
     * Cierra la sentencia SQL especificada.
     * 
     * @param statement
     *            Sentencia a cerrar.
     */
    protected void close(Statement statement) {
        if (statement != null) {
            logger.trace("Cerrando sentencia SQL.");
            try {
                statement.close();
            } catch (SQLException sqle) {
                logger.warn("La sentencia especificada no logr� cerrarse.",
                    sqle);
            }
        }
    }

    /**
     * Cierra el resultset y la sentencia especificadas.
     * 
     * @param statement
     *            sentencia a cerrar.
     * @param resultSet
     *            resultset a cerrar.
     */
    protected void close(Statement statement, ResultSet resultSet) {
        this.close(resultSet);
        this.close(statement);
    }

    /**
     * Crea la llave de configuraci�n por defecto para el DAO concreto actual
     * bas�ndose el el nombre de la interfaz DAO implementada.
     * 
     * @return llave de configuraci�n creada en base a la interfaz DAO
     *         implementada.
     */
    protected String createDefaultConfigurationKey() {
        String connectionKey = this.daoInterface.getName();
        if (logger.isDebugEnabled()) {
            logger.debug("Llave por defecto: " + connectionKey);
        }
        return connectionKey;
    }

    /**
     * Retorna el {@link ResultSetHandler} que se utilizar� para manejar la
     * traducci�n desde el ResultSet a la clase especificada.
     * 
     * @param resultClass
     *            Clase que debe ser manejada pro el {@link ResultSetHandler}.
     * @return manejador de ResultSet.
     */
    protected ResultSetHandler getResultSetHandler(Class resultClass) {
        logger.trace("Retornando manejador...");
        if (resultClass.getName().equals(Map.class.getName())) {
            return new MapListHandler();
        } else {
            return new BeanListHandler(resultClass);
        }
    }

    /**
     * Se encarga de manejar una excepci�n durante la ejecuci�n de alg�n m�todo
     * del DAO.
     * 
     * @param message
     *            mensaje de error a registrar en el log.
     * @param throwable
     *            motivo del error generado. No debe ser null.
     * @param sql
     *            sentencia SQL que se estaba intentando ejecutar.
     * @param arguments
     *            par�metros de la sentencia.
     */
    protected void handleException(String message, Throwable throwable,
        String sql, Object[] arguments) {
        if (logger.isDebugEnabled()) {
            StringBuilder builder = new StringBuilder();
            String separator = "";
            for (int i = 0; i < arguments.length; i++) {
                builder.append(separator).append(arguments[i]);
                separator = " - ";
            }
            logger.debug("SQL       : " + sql);
            logger.debug("Argumentos: " + builder);
        }
        logger.error(message, throwable);
    }

    /**
     * Permite establecer el valor especificado en la posici�n especificada de
     * la sentencia.
     * 
     * @param statement
     *            sentencia SQL donde se establecer� el argumento.
     * @param object
     *            valor a establecer.
     * @param i
     *            �ndice de posici�n del valor.
     * @throws SQLException
     *             en caso de error.
     * @return nuevo �ndice de posici�n donde realizar la siguiente inserci�n.
     */
    protected int setArgument(PreparedStatement statement, Object object, int i)
        throws SQLException {
        int j = i;
        if (object instanceof Blob) {
            statement.setBlob(j++, (Blob) object);
        } else if (object instanceof Date) {
            Date date = (Date) object;
            statement.setTimestamp(j++, new Timestamp(date.getTime()));
        } else if (object instanceof InputStream) {
            ByteArrayInputStream is = (ByteArrayInputStream) object;
            statement.setBinaryStream(j++, is, is.available());
        } else {
            statement.setObject(j++, object);
        }
        return j;
    }

    /**
     * Permite establecer el listado de argumentos especificados en la sentencia
     * SQL especificada.
     * 
     * @param statement
     *            sentencia donde se establecer�n los par�metros.
     * @param arguments
     *            argumentos de la sentencia.
     * @throws SQLException
     *             en caso de error.
     */
    protected void setArguments(PreparedStatement statement, Object[] arguments)
        throws SQLException {
        if (arguments != null) {
            logger.trace("Estableciendo arguemntos de la sentencia.");
            int j = 1;
            for (int i = 0; i < arguments.length; i++) {
                j = this.setArgument(statement, arguments[i], j);
            }
            logger.trace("Argumentos establecidos.");
        }
    }

    /**
     * Retorna el nombre de la llave generada en la sentencia de inserci�n
     * especificada.
     *
     * @param sql sentencia SQL de inserci�n.
     * @return nombre de la llave que se generar� en la sentencia.
     */
    protected String getGeneratedColumn(String sql) {
        String generatedKeys = null;
        if (sql.indexOf(".nextval") > 0) {
            int findex = sql.indexOf("(") + 1;

            int punto = sql.indexOf(".");
            int cerrar = sql.indexOf(")");
            if (punto != -1 && punto < cerrar) {
                findex = punto + 1;
            }
            int lindex = sql.indexOf(",");
            generatedKeys = sql.substring(findex, lindex).trim().toUpperCase();
        }
        return generatedKeys;
    }
    
    public String obtieneSecuencia(String sql){
       	
   	long id= 0;
   	String retorno="NOK";
   	
   	Connection connection= getConnection();
       if (connection == null) {
           throw new NullPointerException(
               "La conexi�n especificada no debe ser null.");
       }
       if (sql == null) {
           throw new NullPointerException(
               "La sentencia SQL a ejecutar no debe ser null.");
       }
       PreparedStatement statement = null;
       ResultSet resultSet = null;
   	
       try {
       
       logger.trace("Inicializando sentencia SQL.");
       statement = connection.prepareStatement(sql);
       logger.trace("Estableciendo los argumentos.");
       synchronized( this ) {
      ResultSet rs = statement.executeQuery();
      if(rs.next()){
         id = rs.getLong(1);
         retorno="OK";
      }
       }  
       } catch (SQLException sqle) {
               //this.handleException("Error ejecutando consulta SQL.",sqle,sql);
               throw new SystemException(sqle);
           } finally {
               this.close(statement, resultSet);
               statement = null;
               resultSet = null;
           }
       if(retorno.equals("OK")){
       	return String.valueOf(id);
       }else{
       	return retorno;
       }      
   }
}
